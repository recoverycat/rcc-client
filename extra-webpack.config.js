import path from 'path'

export default {
	module: {
		rules: [
			{
				test: /\.(pdf)$/i,
				type: 'asset/resource',
				generator: {
					filename: '[name]-[hash][ext]',
				}
			},
			{
				test: /.(md)$/i,
				use: ['html-loader', path.resolve('loaders/markdown-loader.js')]
			}
		],
	},
}
