# Periodic assessment of SOUP
Regulatory file: [SOP_3.11](https://drive.google.com/drive/folders/1n2cmIIvlFkex7dsS1CQYwn93Mx8FalKV)

Create new GitLab issue and branch. Keep note of all steps taken (console commands).

Example: https://git.recoverycat.de/recoverycat/rcc-client/-/issues/1275

## Vulnerability scan
1. `yarn upgrade` (fixes many vulnerabilities already)
1. `yarn audit`, save result in `yarn-audit.md`. Don't use `npm audit` since npm is not used for build.
1. Go through vulnerabilities and eliminate as many as possible:
    1. Look at top-level packages and upgrade if possible.
    1. Look at top-level packages and remove if possible, using `yarn remove`.
1. Handle leftover vulnerabilities:
    1. Keep justifiable vulnerabilities and note the reasoning in the assessment summary:
        1. Severity "low", according to GitHub Advisories.
        1. Severity "moderate" or higher, but not applicable to our use of the package.
    1. Escalate unjustifiable vulnerabilities with process owner.
1. Run tests.
1. Add final `yarn audit` output to `yarn-audit.md`.
1. Commit all changes. Commit message includes the list of console commands.

## SOUP assessment
Regulatory file: [RC1_SW_04](https://drive.google.com/drive/folders/1fGmG2yGAM_TZ6jSc1LFRpicz4sdG8wbx)

1. Use `npm ls --depth 0 --json > soup.json` to extract all direct dependencies.
1. Fix any errors that might have occurred (usually malformed package.json).
1. Feed that file to `rcc_create_soup_list.js` to generate a list of all direct dependencies and their versions and update the Google Sheets file with that.
1. If multiple versions of the same package are installed,
use `yarn why` to determine whether they are all used/relevant to the SOUP list.
1. Review the RC1_SW_04 to see if all packages are still needed and fit our requirements. Uninstall unused packages and delete them from the list.
1. Export Google Sheets file as CSV and save in this directory.

Put list of console commands and a summary of what was done in the merge request description.
