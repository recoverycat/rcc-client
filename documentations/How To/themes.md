# Themes

Everything related to themes lies in `./lib/themes` or `@rcc/themes`. ( There is
one exception and that is the SharedModule, which lies in `./lib/common` and
imports and re-exports the common theme; in the future every module, that makes
use of the theme, should import the active theme by itself and not through
SharedModule).

## Colors

see. [Skins](/additional-documentation/skins.html)

When working on a theme, make use of the css color variables and/or
{@link RccColorService}.

## General setup


Every theme consists of a set of components and an NgModule exporting all those
components. The set of components is determined by the files in
`./lib/themes/theming-mechanics/components`. Each component file in this
directory holds an (maybe abstract) component, that defines an interface for a component
and a selector. For each of those abstract components a theme has to provide
an actual component that extends one of the abstract components and uses the
same selector as this abstract component.

In this sense every theme provides the same set of components; the actual
implementation beyond selector and component interface can vary between the
themes.

The active theme is determined by `./lib/themes/active.ts`. By default it exports
everything from `./lib/themes/default`: The default theme. Exchanging this
file for another by a fileReplacement configuration in angular.json allows to
export any other theme module as `RccThemeModule`.
```
//theme.ts replacing ./lib/themes/active.ts

export * from '@rcc/themes/bengal'

```




## Creating a new component from scratch

### Create a base component

The first step is to lay down the interface for the new component. The new component
file is meant to be extended by the real component later on. This way components
of every theme will have to conform to the same interface and will not cause
errors when one theme replaces another.

Go to `lib/themes/theme-mechanics/components` an create
`RccSomethingBaseComponent` in `something-base.component.ts`. You can make it
an abstract class and define Inputs and Outputs and a selector. (Abstract
classes, properties and methods can be decorated just like in non-abstract
components.) If there is some common logic the variants of all themes should
share, put it here.

Add a Description on top of it telling what the actual component is supposed
to do. (The description will be the basis for the unit test, see below)

__Note:__ Whenever you import from `@rcc/common`, import from a deeper entry
point like `@rcc/common/action` to avoid circular dependencies (they might not
show up when serving the app, but can still cause trouble in the unit test)

### Create a default component

This is where the actual implementation starts. The default component should do
everything listed in the description of the base component. The default
component needs no fancy styling, it should just do its job with as little
expense as possible – sometimes this means you only add a template to the base
component.

Go to `lib/themes/default/something` and create
`something.component.ts`. Add files for styles and templates as needed.

Make the default component standalone (see
https://angular.io/guide/standalone-components). This way it is a lot easier for
other themes to build on the new component.

Create `RccSomethingComponent` class extending  `RccSomethingBaseComponent`.

Add the new component to `lib/themes/default/theme.module.ts`

__Note__: Since the default theme is probably not the one the current variants
use, it is a bit cumbersome to develop. There are two ways to temporarily work with the
default components (will improve this over time), see below.

### Add a unit test

When the component works as intended add a unit test, testing everything
included in the description of the base class. Save the test into
`lib/themes/theming-mechanics/components/somethig.component.spec` next to the
base component. Export it using the  `ComponentUnitTests` from
`lib/themes/theming-mechanics/components/components.commons.spec.ts`:

```
// something.component.spec.ts

import	{	Type 							}	from '@angular/core'
import	{	RccActionButtonBaseComponent	}	from '@rcc/themes'
import	{	ComponentUnitTests				}	from './components.commons.spec'

// All Units test for RccSomethingComponent:
function rccSomethingComponentTests(componentClass: Type<unknown>) : void {


	it('does something)', () => {
		// ...
	})

	//..
}


export const RccSomethingComponentTests : ComponentUnitTests = {

	baseComponentClass: 	RccSomethingBaseComponent,
	unitTests:				rccSomethingComponentTests

}

```

Then import it in 'lib/themes/theming-mechanics/components/components.spec' and
add it to the `componentsTestEntries` array:

```
// components.spec.ts
// ...

const componentsTestEntries	: ComponentUnitTests[]	= [
	RccActionButtonComponentTests,
	RccSomethingComponentTests,
	//...
]

//...

```

Every theme is supposed to run all these unit tests against it's own exports; e.g. bengal:

```
// bengal/theme.spec.ts

import 	* as themeExports 	from './index'
import	{ testComponents }	from '@rcc/themes/spec'


describe( 'Default theme components', () => {
	testComponents(themeExports)
})

```

These tests will also check if all components are present.


### Create the actual component for a specific theme

Goto `lib/themes/bengal` (or your theme folder). Create
`RccSomethingComponent` in `lib/themes/bengal/something.ts`. Add styles and
a template as needed, no restrictions just make sure the new Component passes
the unit test. This is where all the fancy design templates ought to be applied :)

You can start by extending `RccSomethingBaseComponent` or reuse the code from
the default theme component by extending `RccSomethingComponent` from
`@rcc/themes/default`:


```
import { RccSomethingComponent as DefaultSomethingComponent } from '@rcc/themes/default'

@Component({
	//...
})
export class RccSomethingComponent extend DefaultSomethingComponent {
	//...
}

```

Make the actual component standalone too (see
https://angular.io/guide/standalone-components). This way it is a lot easier for
other themes to build on the new component.

Add the component to `lib/themes/bengal/theme.module`.

The app should now be able to pass all the unit tests.



## Temporarily switching theme during development

I can think of three ways to do this (it is still
to be determined which one of them is the least annoying)

* Go to `app/generic/src` and then into the current variant sub folder. Edit
`theme.ts` so it points to the theme you want to work with. (Change it back when
you are done)
* Edit angular.json and comment out the fileReplacement configuration for the
variant you want to test. This will load the default theme. (Change it back when
you are done)
* While working on a specific component, have the current theme re-export the
corresponding default component as it's own. This way, as you can work on the
default theme component while another custom theme is loaded.



