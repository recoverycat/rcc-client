# How to deal with circular dependencies in Angular

**⚠️ @rcc/common must never import from @rcc/themes**

While Angular may be able to deal with certain ways of importing and the app will run with no problems, Jasmine (our unit testing suite) still could throw some errors.

- Regularly run `ng test` during development
	- Every time you add a new import
	- If you encountered a CD, everytime you remove an import
- If in doubt, be as specific as possible when importing:
	- Instead of importing 4 components/modules/services from @rcc/common, import them each on their own with more precise routes (for example @rcc/common/src/actions)
	- Instead of importing the `SharedModule` or the `UiComponentsModule` which both consist of a wide variety of components/modules/services, import each one individually

We have dealt with many instances of circular dependency already.

If you run into problems, try orientating yourself by other components that are built and work in a very similar way. This could mean:

- Setting up proper base components and extending them to the @rcc/themes/bengal and @rcc/themes/default folders
- Comparing how other components/modules/service handle their imports
- **Last resort**: If nothing else helps, start anew by rebuilding your code on a new branch with a stern focus on not causing a CD

## How does a circular dependency show itself?

Mostly you will stumble across a CD when running `ng test`, either manually or during the GitLab pipeline.

This is what it could look like:

```
 An error was thrown in afterAll
  Uncaught TypeError: Cannot read properties of undefined (reading 'provideTranslationMap')
  TypeError: Cannot read properties of undefined (reading 'provideTranslationMap')
      at Module.provideTranslationMap (http://localhost:9876/_karma_webpack_/main.js:7695:109)
      at Module.3983 (http://localhost:9876/_karma_webpack_/webpack:/lib/common/src/notifications/notification.module.ts:25:3)
      at __webpack_require__ (http://localhost:9876/_karma_webpack_/webpack:/webpack/bootstrap:19:1)
      at Module.74212 (http://localhost:9876/_karma_webpack_/main.js:4622:78)
      at __webpack_require__ (http://localhost:9876/_karma_webpack_/webpack:/webpack/bootstrap:19:1)
      at Module.7801 (http://localhost:9876/_karma_webpack_/main.js:3610:72)
      at __webpack_require__ (http://localhost:9876/_karma_webpack_/webpack:/webpack/bootstrap:19:1)
      at Module.82875 (http://localhost:9876/_karma_webpack_/main.js:3553:75)
      at __webpack_require__ (http://localhost:9876/_karma_webpack_/webpack:/webpack/bootstrap:19:1)
      at Module.53666 (http://localhost:9876/_karma_webpack_/main.js:23305:79)

Chrome Headless 113.0.5672.126 (Mac OS 10.15.7): Executed 10 of 10 ERROR (0.039 secs / 0.036 secs)


Chrome Headless 113.0.5672.126 (Mac OS 10.15.7): Executed 10 of 10 ERROR (0.04 secs / 0.036 secs)

```

1. One circular dependency indicator is the error being `thrown in afterAll`, so it doesn’t have anything to do with the unit tests themselves.
2. The next thing is
`Uncaught TypeError: Cannot read properties of undefined (reading 'provideTranslationMap')`
This gives us a hint which components/module/service might be causing problems. Very common is also the `RccActionButtonComponent` or the `TranslationsModule`.

## Any further ideas?

If you have more ideas or techniques you use, that could be helpful for other developers, feel free to add them to this guide. *Thanks!*
