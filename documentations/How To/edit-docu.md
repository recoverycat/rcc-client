# Edit documentation

**Documentation lives at [docs.recoverycat.de](docs.recoverycat.de)**

We use [compodoc](https://compodoc.app/) to automatically generate documentation from in-line comments.

For longer explanations like this one, you can create custom pages (we call them Guides) in [Markdown](https://www.markdownguide.org/basic-syntax/). Guides are currently in ```documentations/guides```. Images are in ```Screenshots```.

It is recommended to run compodoc locally to preview changes before pushing them.

You could also use VS Code's preview function:
![image](../../Screenshots/edit-docu-preview.PNG)

1. Install the compodoc CLI: ```npm install -g @compodoc/compodoc```

2. Navigate into the RCC directory in your shell and run compodoc: ```compodoc -sw```. This takes a minute. Docu is served locally on [http://127.0.0.1:8080](http://127.0.0.1:8080) and watches for changes so you don't have to restart the server, only save and wait for the page to reload.

3. When creating a new page, you have to add it to ```documentations/guides/summary.json``` before it will appear in the Guides menu.
