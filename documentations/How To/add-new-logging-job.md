## Adding a new logging job

### 1. Create a new Module

- The name should describe what the job should log
- The module should provide a logging job using the `provideRccUsageLoggingJob` function from the `UsageLoggingCommons`
- Import the new module in the feature module of the desired flavor

### 2. Defining a logging Job

-When providing a logging job, you have to define a `RccUsageLoggingJob` object
- tick$
  - The tick observable will be listened to by the `UsageLoggingService` and will be used to trigger the handover to the actual logging service which will log the data
  - The tick observable emits a 'DynamicLoggingData' object which contains the data that should be logged and collected at runtime
- The rest of the data is defined when the `RccUsageLoggingJob` object is created
  - `description` and `label`will be used to display the loggingJob in the UI so the associated language strings should convey the purpose of the logging job
  - `requiredUserConsent` defines if the user has to give consent to the logging job, this should be inferred from the `label`, `description`and the `userId`entries
  - `userId`defines if and what kind of rotating Id should be sent

