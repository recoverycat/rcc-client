# Use Git and GitLab

This guide shows the basic workflows of Git and GitLab and how to get started with the repository.
Git is a version control tool that keeps track of changes made over time,
so it's easy to revert back to an older version of a file when necessary.
GitLab is a sort of online version of Git that allows us to work on the same files simultaneously
without getting in each other's way.

This is the Recovery Cat GitLab repository:
[https://gitlab.com/recoverycat/rcc-client](https://gitlab.com/recoverycat/rcc-client)

Basic knowledge of the command line or terminal (how to open it and change directories) is assumed.
If you don't know how to do this, there are guides for [Windows](https://www.minitool.com/lib/cmd.html)
and [macOS](https://terminalcheatsheet.com/) on the web.

## Setup

1. [Download](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) and install Git.
If you are using Windows, make sure to use the standalone installer instead of the portable version.
1. Verify that Git is installed correctly by typing `git version` in the terminal.
Hit <kbd>Enter</kbd> and you should see output similar to `git version 2.27.0.1`
1. You can now use Git locally. For more info, see the [Pro Git book](https://git-scm.com/book/en/v2).
1. Create an account on [gitlab.com](https://gitlab.com/users/sign_in?redirect_to_referer=yes)
and verify your email address.
1. Ask a project owner to add you to the RC project. We need your GitLab username for that.

## Connecting Git to GitLab

In order to push from your command line to the GitLab server, you need to generate an SSH key pair.

1. In the terminal, generate a new key pair with `ssh-keygen -t ed25519`
    1. Choose a filename when prompted, we recommend `id_ed25519_rc`
    1. A passphrase is recommended, but optional.
    You will be asked to enter this passphrase anytime you want to interact with GitLab through the terminal.
1. Copy the public key
    1. On Mac: `tr -d '\n' < ~/.ssh/id_ed25519_rc.pub | pbcopy`
    1. On Windows, it is easiest to go to `C:/Users/<YourName>/.ssh`,
    open the .pub file you just created in the Editor and copy the contents manually.
1. Go to your [GitLab preferences](https://gitlab.com/-/profile/keys).
Paste the key you just copies in the "Key" field. Give it a descriptive title and click "Add key".
1. Set up your local Git username and email. **These must be the same as your GitLab username and email!**
    ```console
    git config --global user.name "Your Name"
    git config --global user.email "youremail@example.com"
    ```

## Downloading the source code

1. Create a folder `recoverycat` on your computer. This is where the source code will be downloaded to.
1. Navigate to this directory in the terminal with `cd`
1. Clone the repository with `git clone git@gitlab.com:recoverycat/rcc-client.git`.
All files are now downloaded from GitLab to your computer.

## Working with the code

Please always work on your own branch and don't push directly to the main branch!

1. Download changes from the remote server with `git pull` to make sure you are working on the
latest version of a file.
1. Create a new branch with `git checkout -b <branchname>`.
Git automatically switches to that branch.
1. To switch between branches, use `git checkout <branchname>`

To learn how to install the Recovery Cat app and run it locally, read the [README](https://docs.recoverycat.de/).

## Uploading changes to GitLab

After completing a task, you can push your local changes to GitLab so others can see it
and it becomes part of the app.

1. You can see which files have been modified (and more!) with `git status`
Make sure you are on the correct branch!
1. Mark the files you want to push with `git add <filename>`. This is called *staging*.
You can mark all modified files at once with `git add .`
To add all files in a directory, use `git add <directory>/.`
1. After adding all the files you want to push, make a commit with `git commit -m"Your message"`
The message should be brief, yet descriptive and informative to others.
1. Finally, upload your changes to the remote server with `git push`
    - When creating a new branch, the first push to that branch must be `git push -u origin <branchname>`.
    This creates the same branch in the remote repository.
