import { defineConfig } from 'cypress'
import installLogsPrinter from 'cypress-terminal-report/src/installLogsPrinter'
import {getMostRecentFile} from "./cypress/e2e/utils/fileUtils";

export default defineConfig({
	e2e: {
		baseUrl: 'https://localhost:4202/',
		setupNodeEvents(on, config) {
			console.log('setupNodeEvents')
			const options : installLogsPrinter.PluginOptions = {
				printLogsToConsole: 'always',

			}
			installLogsPrinter(on, options);
			// implement node event listeners here

			on('task', {
				getMostRecentFile
			})
		},
	},
	waitForAnimations: true,
	chromeWebSecurity: false,
	includeShadowDom: true,
	env: {language: 'de'},
})
