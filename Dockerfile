FROM nginx:alpine
ENV FLAVORS repose hcp pat

COPY /dist/app /app

# Scripts in this dir will be executed by the nginx entrypoint.sh
COPY docker/select-flavor.sh /docker-entrypoint.d/09-recoverycat-select-flavor.sh
COPY ./docker/default.conf /etc/nginx/conf.d/default.conf
