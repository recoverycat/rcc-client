// Helper script to generate SOUP list from SBOM
// Input: List of direct dependencies (can be generated with npm ls --json)
// Output: TSV with <package name\tversion> for all direct dependencies
// TSV format can be copy-pasted into Google Sheets

import yargs from 'yargs'
import { hideBin } from 'yargs/helpers'
import { writeFile } from 'fs/promises'
import { readFileSync } from 'fs'

const argv	=	yargs(hideBin(process.argv))
				.options({
					input: {
						describe:		'Path to output of npm ls --json',
						demandOption:	true,
					},
					output: {
						describe:		'Target path of the generated TSV file',
						default:		'./soup.tsv'
					},
				})
                .check(argv => {
                    if (!argv.input.endsWith('.json'))  throw new Error('Input path must end with .json')
                    if (!argv.output.endsWith('.tsv'))  throw new Error('Output path must end with .tsv')
                    return true
                })
				.argv

const targetPath = argv.output

function findDeps(sbom, content=[]) {
    if (sbom.problems) {
        console.group()
        console.warn(`WARNING: npm found ${sbom.problems.length} problem(s):`)
        console.group()
        for (const p of sbom.problems)
            console.warn(p)
        console.groupEnd()
        console.groupEnd()
    }
    
    for (const depName of Object.keys(sbom.dependencies)) {
        const dep = sbom.dependencies[depName]
        content.push(`${depName}\t${dep.version}`)

        if (dep.dependencies)
            findDeps(dep, content)
    }

    return content
}

function createRows(deps) {
    const deduplicated = [...new Set(deps)].sort()
    
    // Deps to exclude from final list
    // = Recovery Cat packages (incl. charts) + types (not security relevant)
    const exclude = ['@rcc/', 'rc_charts_vanilla', '@types/']
    const final = deduplicated.filter(dep => !exclude.some(prefix => dep.startsWith(prefix)))
    
    return final.join('\n')
}

async function createSoupList(targetPath) {
    const sbom = JSON.parse(readFileSync(argv.input, 'utf8'))

    const deps = findDeps(sbom)
    const data = createRows(deps)

    await writeFile(targetPath, data, 'utf8')
    console.log(`SOUP list created at ${targetPath}`)
}

await createSoupList(targetPath)
