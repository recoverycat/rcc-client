# requiring the environment of NodeJS
image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/node:20

stages:
  - regulatory
  - prebuild
  - test
  - build
  - docker
  - tagging
  - deploy
  - release
  - e2e
  - triggers

variables:
  # see https://docs.gitlab.com/runner/configuration/feature-flags.html
  FF_USE_FASTZIP: "true"
  # see https://docs.gitlab.com/ee/ci/runners/configure_runners.html
  ARTIFACT_COMPRESSION_LEVEL: "fast"
  CACHE_COMPRESSION_LEVEL: "fast"
  # see https://yarnpkg.com/lang/en/docs/cli/cache/
  YARN_CACHE_FOLDER: "$CI_PROJECT_DIR/.cache/yarn"
  # see https://on.cypress.io/caching
  CYPRESS_CACHE_FOLDER: "$CI_PROJECT_DIR/.cache/Cypress"
  DOCKER_VERSION: "20.10"
  # https://hub.docker.com/r/cypress/browsers/tags?page=1&page_size=&name=&ordering=
  NODE_WITH_BROWSERS_TAG: node-20.14.0-chrome-126.0.6478.114-1-ff-127.0.1-edge-126.0.2592.61-1
  RUN_E2E: "true"

# add 'node_modules' to cache for speeding up builds
# but only until yarn.lock changes
cache: &node_cache
  key:
    files:
      - yarn.lock
  paths:
    - node_modules/
    - lib/**/node_modules/
    - .cache/
    - .cache/Cypress

.install_dependencies: &install_dependencies
  - yarn config set compressionLevel 0
  - yarn install --cache-folder .cache --frozen-lockfile --no-progress --immutable --immutable-cache --non-interactive
  - yarn cypress install

# ==========================================
#region Regulatory stage
# ==========================================
vulnerability-scanning:
  image:
    name: docker.io/aquasec/trivy:0.58.0
    entrypoint: [""]
  stage: regulatory
  variables:
    TRIVY_NO_PROGRESS: "true"
    TRIVY_CACHE_DIR: ".trivycache/"
    RUN_VULN: "true"
    SEVERITY_THRESHOLD: "HIGH"
  script:
    # Update vulnerabilities database
    - trivy sbom --download-db-only
    # Generate SBOM including vulnerabilities
    - trivy filesystem . --format cyclonedx --output "./sbom.json" --scanners vuln --include-dev-deps
    # Generate vulnerability report from SBOM
    - trivy sbom "./sbom.json" --output "./vulnerability-report.txt"
    # Exit with code 1 if vulnerabilities of a certain severity are detected
    - trivy sbom "./sbom.json" --exit-code 1 --severity $SEVERITY_THRESHOLD
  cache:
    key: trivy-cache
    paths:
      - .trivycache/
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule" && $RUN_VULN == "true"
      allow_failure: false
  artifacts:
    when: always
    paths:
      - "./sbom.json"
      - "./vulnerability-report.txt"

# ==========================================
#region Prebuild stage
# ==========================================
# Loads deps
deps:
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/cypress/browsers:$NODE_WITH_BROWSERS_TAG
  stage: prebuild
  script:
    - *install_dependencies
  cache:
    <<: *node_cache
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_TAG
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH == "main"

# ==========================================
#region Test stage
# ==========================================
# lint all typescript files which are different in target branch
lint:
  stage: test
  script:
    - *install_dependencies
    - git fetch origin $CI_MERGE_REQUEST_TARGET_BRANCH_NAME
    - git diff --name-only --diff-filter=M --diff-filter=A origin/$CI_MERGE_REQUEST_TARGET_BRANCH_NAME -- "lib/**/*.ts" "app/**/*.ts" | xargs -t npx eslint
  cache:
    <<: *node_cache
    policy: pull
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
  dependencies:
    - deps

# run unit tests
unittests:
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/cypress/browsers:$NODE_WITH_BROWSERS_TAG
  stage: test
  script:
    - *install_dependencies
    - git fetch --prune --unshallow --tags
    - npm run unittests
  cache:
    <<: *node_cache
    policy: pull
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_TAG
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH == "main"
  dependencies:
    - deps
  artifacts:
    paths:
      - coverage
    reports:
      junit:
        - unit-tests-results.xml
      coverage_report:
        coverage_format: cobertura
        path: coverage/cobertura-coverage.xml

# ==========================================
#region Build stage
# ==========================================
.build: &build
  stage: build
  dependencies: [ ]
  script:
    - *install_dependencies
    # - git fetch --prune --unshallow --tags
    - yarn build:${FLAVOR}
  cache:
    <<: *node_cache
    policy: pull
  artifacts:
    name: recoverycat/${CI_COMMIT_SHORT_SHA}
    paths:
      - dist/app
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_TAG
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH == "main"

build docs:
  <<: *build
  script:
    - *install_dependencies
    - npx compodoc -c .compodocrc.json # build to public path

build hcp:
  variables:
    FLAVOR: hcp
  <<: *build

build pat:
  variables:
    FLAVOR: pat
  <<: *build

build repose:
  variables:
    FLAVOR: repose
  <<: *build

build hcp-med:
  variables:
    FLAVOR: hcp-med
  <<: *build

build pat-med:
  variables:
    FLAVOR: pat-med
  <<: *build

# ==========================================
#region Docker stage
# ==========================================
docker build:
  variables:
    DOCKER_TLS_CERTDIR: ""
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/docker:$DOCKER_VERSION
  services:
    - docker:${DOCKER_VERSION}-dind
  stage: docker
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker login -u $HARBOR_USER -p $HARBOR_PASSWORD $HARBOR_REGISTRY
  script:
    - DOCKER_BUILDKIT=1 docker build --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA --tag $CI_REGISTRY_IMAGE:latest .
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    - docker push $CI_REGISTRY_IMAGE:latest
    - docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $HARBOR_REGISTRY_IMAGE:$CI_COMMIT_SHA
    - docker tag $CI_REGISTRY_IMAGE:latest $HARBOR_REGISTRY_IMAGE:latest
    - docker push $HARBOR_REGISTRY_IMAGE:$CI_COMMIT_SHA
    - docker push $HARBOR_REGISTRY_IMAGE:latest
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_TAG
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TITLE =~ /^(\[Draft\]|\(Draft\)|Draft:)/
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH == "main"
  dependencies:
    - "build docs"
    - "build hcp"
    - "build pat"
    - "build repose"
    - "build hcp-med"
    - "build pat-med"
  tags:
    - docker
  cache:
    policy: pull

# ==========================================
#region Tagging stage
# ==========================================
docker tag development:
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/docker:$DOCKER_VERSION
  variables:
    DOCKER_TLS_CERTDIR: ""
  services:
    - docker:${DOCKER_VERSION}-dind
  stage: tagging
  needs: [ "docker build" ]
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker login -u $HARBOR_USER -p $HARBOR_PASSWORD $HARBOR_REGISTRY
  script:
    - docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    - docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $CI_REGISTRY_IMAGE:development
    - docker push $CI_REGISTRY_IMAGE:development
    - export SORTABLE_TAG="development-${CI_COMMIT_SHA:0:7}-$(date +%s)"
    - echo $SORTABLE_TAG
    - docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $CI_REGISTRY_IMAGE:$SORTABLE_TAG
    - docker push $CI_REGISTRY_IMAGE:$SORTABLE_TAG
    - echo "Also push images to https://harbor.recoverycat.de"
    - docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $HARBOR_REGISTRY_IMAGE:development
    - docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $HARBOR_REGISTRY_IMAGE:$SORTABLE_TAG
    - docker push $HARBOR_REGISTRY_IMAGE:development
    - docker push $HARBOR_REGISTRY_IMAGE:$SORTABLE_TAG
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == "main"
  tags:
    - docker
  cache:
    policy: pull

docker tag release:
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/docker:$DOCKER_VERSION
  variables:
    DOCKER_TLS_CERTDIR: ""
  services:
    - docker:${DOCKER_VERSION}-dind
  stage: tagging
  # we need dev tagging so that dev image recognition can be triggered after a tag release
  needs: [ "docker tag development", "docker build" ]
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker login -u $HARBOR_USER -p $HARBOR_PASSWORD $HARBOR_REGISTRY
  script:
    - docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    - docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
    - echo "Also push images to https://harbor.recoverycat.de"
    - docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $HARBOR_REGISTRY_IMAGE:$CI_COMMIT_TAG
    - docker push $HARBOR_REGISTRY_IMAGE:$CI_COMMIT_TAG
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_TAG
  tags:
    - docker
  cache:
    policy: pull

# ==========================================
#region Scheduled E2E stage
# ==========================================
generate-e2e-tests:
  stage: e2e
  variables:
    HCP_DOMAIN: https://hcp.qa.recoverycat.de/
    PAT_DOMAIN: https://pat.qa.recoverycat.de/
    BROWSER: chrome
    DO_REGULATORY: true
    DO_REGULAR: true
  script:
    - ./cypress/gen_e2e.sh $HCP_DOMAIN $BROWSER $DO_REGULATORY $DO_REGULAR
    - ./cypress/gen_e2e.sh $PAT_DOMAIN $BROWSER $DO_REGULATORY $DO_REGULAR
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule" && $RUN_E2E == "true"
      allow_failure: false
  artifacts:
    paths:
      - e2e.*.hcp.gitlab-ci.yml
      - e2e.*.pat.gitlab-ci.yml
  cache:
    policy: pull

e2e-hcp-pipeline:
  stage: triggers
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule" && $RUN_E2E == "true"
      allow_failure: false
  trigger:
    strategy: depend
    include:
      - artifact: e2e.regulatory.hcp.gitlab-ci.yml
        job: generate-e2e-tests
      - artifact: e2e.regular.hcp.gitlab-ci.yml
        job: generate-e2e-tests

e2e-pat-pipeline:
  stage: triggers
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule" && $RUN_E2E == "true"
      allow_failure: false
  trigger:
    strategy: depend
    include:
      - artifact: e2e.regulatory.pat.gitlab-ci.yml
        job: generate-e2e-tests
      - artifact: e2e.regular.pat.gitlab-ci.yml
        job: generate-e2e-tests

# ==========================================
#region MR E2E stage
# ==========================================
serverless-create:
  image: $HARBOR_SERVERLESS_REGISTRY_IMAGE:latest
  stage: e2e
  needs: [ "docker build" ]
  variables:
    CI_MAX_SCALE: 1
    CI_MIN_SCALE: 0
    CI_MEM_LIMIT: 2048
    CI_CPU_LIMIT: 750
  script:
    - export CI_GUM_SPIN=false
    - _date=$(date +%s)
    - export IMAGE=$HARBOR_REGISTRY_IMAGE:$CI_COMMIT_SHA
    - echo "Deploying $FLAVOR"
    - flavor_uppercase=$(echo "$FLAVOR" | tr '[:lower:]' '[:upper:]')
    - export NAME=$FLAVOR-${CI_COMMIT_SHA:0:7}-$_date
    - echo "$NAME" "$IMAGE" "FLAVOR=$FLAVOR"
    - response=$(bash /deploy_container.sh --create "$NAME" "$IMAGE" "FLAVOR=$FLAVOR")
    - uuid=$(echo "$response" | head -n 1)
    - url=$(echo "$response" | tail -n 1)
    - echo "${flavor_uppercase}_SERVERLESS_CONTAINER_ID=$uuid" > build.${FLAVOR}.env
    - echo "${flavor_uppercase}_SERVERLESS_CONTAINER_URL=$url" >> build.${FLAVOR}.env
    - echo "FLAVOR=$FLAVOR UUID=$uuid URL=$url"
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TITLE =~ /^(\[Draft\]|\(Draft\)|Draft:)/
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      allow_failure: false
  parallel:
    matrix:
      - FLAVOR: [ hcp, pat ]
  artifacts:
    reports:
      dotenv: build.${FLAVOR}.env
  cache:
    policy: pull

generate-e2e-tests-mr:
  stage: e2e
  variables:
    BROWSER: chrome
    DO_REGULATORY: true
    DO_REGULAR: true
  script:
    - ./cypress/gen_e2e.sh $HCP_SERVERLESS_CONTAINER_URL $BROWSER $DO_REGULATORY $DO_REGULAR hcp
    - ./cypress/gen_e2e.sh $PAT_SERVERLESS_CONTAINER_URL $BROWSER $DO_REGULATORY $DO_REGULAR pat
  needs: [ "serverless-create" ]
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TITLE =~ /^(\[Draft\]|\(Draft\)|Draft:)/
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      allow_failure: false
  artifacts:
    paths:
      - e2e.*.hcp.gitlab-ci.yml
      - e2e.*.pat.gitlab-ci.yml
  cache:
    policy: pull

e2e-hcp-pipeline-mr:
  stage: triggers
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TITLE =~ /^(\[Draft\]|\(Draft\)|Draft:)/
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      allow_failure: false
  trigger:
    strategy: depend
    include:
      - artifact: e2e.regulatory.hcp.gitlab-ci.yml
        job: generate-e2e-tests-mr
      - artifact: e2e.regular.hcp.gitlab-ci.yml
        job: generate-e2e-tests-mr

e2e-pat-pipeline-mr:
  stage: triggers
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TITLE =~ /^(\[Draft\]|\(Draft\)|Draft:)/
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      allow_failure: false
  trigger:
    strategy: depend
    include:
      - artifact: e2e.regulatory.pat.gitlab-ci.yml
        job: generate-e2e-tests-mr
      - artifact: e2e.regular.pat.gitlab-ci.yml
        job: generate-e2e-tests-mr

serverless-delete:
  image: $HARBOR_SERVERLESS_REGISTRY_IMAGE:latest
  stage: triggers
  needs: [ "serverless-create", "e2e-hcp-pipeline-mr", "e2e-pat-pipeline-mr" ]
  script:
    - export CI_GUM_SPIN=false
    - flavor_uppercase=$(echo "$FLAVOR" | tr '[:lower:]' '[:upper:]')
    - uuid_key="${flavor_uppercase}_SERVERLESS_CONTAINER_ID"
    - url_key="${flavor_uppercase}_SERVERLESS_CONTAINER_URL"
    - uuid_value="$(eval echo \$$uuid_key)"
    - url_value="$(eval echo \$$url_key)"
    - echo "Deleting ${uuid_value} at ${url_value}"
    - bash /deploy_container.sh --delete ${uuid_value}
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TITLE =~ /^(\[Draft\]|\(Draft\)|Draft:)/
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      allow_failure: false
  parallel:
    matrix:
      - FLAVOR: [ hcp, pat ]
  cache:
    policy: pull
