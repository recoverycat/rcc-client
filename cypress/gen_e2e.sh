#!/bin/bash

# This function extracts different parts of a given URL.
#
# Parameters:
# - $1: The URL to extract parts from.
#
# Returns:
# An array containing the extracted parts of the URL.
# The array elements are in the following order:
# 1. Protocol (e.g., http, https)
# 2. Subdomain
# 3. Domain (e.g., example.com)
# 4. Port (if present)
# 5. Path (if present)
# 6. Query parameters (if present)
# 7. Fragment (if present)
extract_url_parts() {
	local url=$1

	# Extract protocol
	protocol=$(echo $url | grep :// | sed -e 's,^\(.*://\).*,\1,g')
	protocol=${protocol%://}

	# Remove the protocol from URL
	url_no_protocol=$(echo $url | sed -e "s,$protocol://,,g")
	host_port=$(echo $url_no_protocol | cut -d/ -f1)

	# Extract port if available
	port=$(echo $host_port | grep : | cut -d: -f2)

	# Extract host and subdomains
	host=$(echo $host_port | cut -d: -f1)
	IFS='.' read -r -a host_parts <<<"$host"
	len=${#host_parts[@]}

	if [ $len -ge 2 ]; then
		domain="${host_parts[$len - 2]}.${host_parts[$len - 1]}"
		subdomain=$(
			IFS=.
			echo "${host_parts[*]:0:$len-2}"
		)
	else
		subdomain="none"
		domain=$host
	fi

	# Extract the path
	path=$(echo $url_no_protocol | grep / | cut -d/ -f2- | cut -d? -f1)

	# Extract the query
	query=$(echo $url | grep ? | cut -d? -f2- | cut -d# -f1)

	# Extract the fragment
	fragment=$(echo $url | grep \# | cut -d# -f2-)

	# Store components in an array
	url_parts=("$protocol" "$subdomain" "$domain" "${port:-none}" "/${path}" "${query:-none}" "${fragment:-none}")

	# Return the array as a string
	echo "${url_parts[@]}"
}

# This function extracts the first part of a given subdomain.
#
# Parameters:
# - $1: The subdomain to extract the first part from.
#
# Returns:
# The first part of the subdomain. If the subdomain is empty, it returns the subdomain itself.
get_first_part_from_subdomain() {
	local subdomain="$1"
	local first_part=$(echo "$subdomain" | cut -d'.' -f1)

	if [ -z "$first_part" ]; then
		echo "$subdomain"
	else
		echo "$first_part"
	fi
}

# This function generates a Cypress command for running a specific test file.
#
# Parameters:
# - $1: The path to the test file.
# - $2: The base URL for the application under test.
# - $3: The browser to run the tests in.
#
# Returns:
# A string containing the Cypress command to run the test file.
generate_cypress_command() {
	local test_file="$1"
	local base_url="$2"
	local browser="$3"

	echo "yarn cypress run --browser $browser --config baseUrl=$base_url --spec \"$test_file\""

}

# This function generates the header content for the GitLab CI YAML file.
#
# Parameters:
# - None
#
# Returns:
# A string containing the header content for the GitLab CI YAML file.
generate_yaml_header() {

	local yaml_content=$(
		cat <<EOF

stages:
  - triggers

cache: &node_cache
  key:
    files:
      - yarn.lock
  paths:
    - node_modules/
    - lib/**/node_modules/
    - .cache/
    - .cache/Cypress
  policy: pull

.install_dependencies: &install_dependencies
  - yarn config set compressionLevel 0
  - yarn install --cache-folder .cache --frozen-lockfile --no-progress --immutable --immutable-cache --non-interactive
  - yarn cypress install

EOF
	)
	echo "$yaml_content"
}

# This function generates the YAML content for a specific test file.
#
# Parameters:
# - $1: The name of the test file.
# - $2: The Cypress command to run the test file.
# - $3: The flavor of the test (e.g., "hcp").
#
# Returns:
# A string containing the YAML content for the test file.
generate_yaml() {
	local test_name="$1"
	local command="$2"
	local flavor="$3"
	local yaml_content=$(
		cat <<-EOF

${flavor}_$test_name:
  image: \${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/cypress/browsers:\$NODE_WITH_BROWSERS_TAG
  stage: triggers
  retry: 2
  rules:
    - if: \$CI_PIPELINE_SOURCE == "parent_pipeline"
  artifacts:
   when: on_failure
   paths:
    - "cypress/screenshots"
  cache:
    <<: *node_cache
  script:
    - *install_dependencies
    - $command
  tags:
    - docker

EOF
	)
	echo "$yaml_content"

}

# This function generates a simple YAML configuration for a GitLab Job.
#
# Parameters:
#   $1 (string): The name of the test to be included in the YAML configuration.
#
# Returns:
# The generated YAML configuration as a string.
generate_dummy_yaml() {
	local test_name="$1"
	local yaml_content=$(
		cat <<EOF

$test_name:
  stage: triggers
  script:
    - echo "This is only here to make gitlab happy"

EOF
	)
	echo "$yaml_content"

}

# This function generates a file name based on the provided folder type and flavor.
#
# Parameters:
#   $1 (string): The folder type.
#   $2 (string): The flavor.
#
# Returns:
# The generated file name as a string.
file_name() {
	local folder_type="$1"
    local flavor="$2"
	echo e2e."${folder_type}"."${flavor}".gitlab-ci.yml
}

# This function generates the final GitLab CI YAML file for running Cypress tests.
#
# Parameters:
# - $1: The root directory where the test files are located.
# - $2: The base URL for the application under test.
# - $3: The browser to run the tests in.
# - $4: The flavor of the test (e.g., "hcp").
#
# Returns:
# None. The function generates the YAML file directly.
generate() {
	local root_dir="$1"
	local base_url="$2"
	local browser="$3"
	local flavor="$4"
	local folder_type="$5"

	file_name=$(file_name "$folder_type" "$flavor")

	generate_yaml_header >"$file_name"

	# Iterate through each test file
	while IFS= read -r -d '' test_file; do
		# Generate Cypress command for the test file
		test_name=$(basename "$test_file")
		test_command=$(generate_cypress_command "$test_file" "$base_url" "$browser")

		generate_yaml "$test_name" "$test_command" "$flavor" >>"$file_name"
	done < <(find "$root_dir" -type f -name "*.cy.ts" -print0)
}

base_url="$1"
browser="$2"
do_regulatory="$3"
do_regular="$4"
flavor="$5"
root_directory="cypress/e2e"

# Check if base_url parameter exists
if [ -z "$base_url" ]; then
	echo "Error: base_url parameter is missing."
	exit 1
fi

# Check if browser parameter exists
if [ -z "$browser" ]; then
	echo "Error: browser parameter is missing."
	exit 1
fi

# Determine flavor if not provided
if [ -z "$flavor" ]; then
	url_parts_str=$(extract_url_parts "$base_url")
	IFS=' ' read -r -a url_parts <<<"$url_parts_str"
	subdomain="${url_parts[1]}"
	flavor=$(get_first_part_from_subdomain "$subdomain")
fi

flavor_uppercase=$(echo "$flavor" | tr '[:lower:]' '[:upper:]')
flavor_lowercase=$(echo "$flavor" | tr '[:upper:]' '[:lower:]')

# Check if do_regulatory parameter exists
if [ -z "$do_regulatory" ]; then
	echo "Error: do_regulatory parameter is missing."
	exit 1
fi

# Check if do_regular parameter exists
if [ -z "$do_regular" ]; then
	echo "Error: do_regular parameter is missing."
	exit 1
fi

if [ "$do_regulatory" = "true" ]; then
    generate "$root_directory/$flavor_uppercase/regulatory" "$base_url" "$browser" "$flavor_lowercase" "regulatory"
else
	file_name=$(file_name "regulatory" "$flavor_lowercase")
	# Only here to make gitlab happy as it doesn't take empty jobs(which makes sense), but we need it
	# so that the dynamic jobs can't work
	generate_dummy_yaml "regulatory_dummy_job" >"$file_name"
fi

if [ "$do_regular" = "true" ]; then
    generate "$root_directory/$flavor_uppercase/regular" "$base_url" "$browser" "$flavor_lowercase" "regular"
else
	# Only here to make gitlab happy as it doesn't take empty jobs(which makes sense), but we need it
    # so that the dynamic jobs can't work
	file_name=$(file_name "regular" "$flavor_lowercase")
	generate_dummy_yaml "regular_dummy_job" >"$file_name"
fi
