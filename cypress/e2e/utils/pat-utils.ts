import Chainable = Cypress.Chainable;
import { TranslationTable } from './e2e.commons'
import { assertTitle, findElementWithLabel, scrollToElementWithWait, translateName, translateString } from './e2e-utils'

export function enterPATRoot(): Chainable {
	return cy.visit(Cypress.config('baseUrl') as string)
}

export function dontAcceptPushNotifications(initialWait: number = 1000): void {
	cy.wait(initialWait)
	cy.document().then(doc => {
		const alertExists: Element | null = doc.querySelector('ion-alert')
		if (alertExists)
			cy.get('ion-alert').within(() => {
				findElementWithLabel({
					en: 'No',
					de: 'Nein'
				}).should('be.visible').click()
			})
		else
			cy.log('Skipping notifications click')

	})
}

export function assertPATTitle(titleHash: TranslationTable): Chainable<string> {
	const titlePrefixHash: TranslationTable = {
		en: 'Recovery Cat · ',
		de: 'Recovery Cat · '
	}
	const fullTitleHash: TranslationTable
		= {}
	for (const lang in titlePrefixHash)
		if (Object.prototype.hasOwnProperty.call(titlePrefixHash, lang))
			fullTitleHash[lang] = titlePrefixHash[lang] + titleHash[lang]
	return assertTitle(fullTitleHash)
}

export function findSendDataTransmissionPullDown() : Chainable<JQuery> {
	// TODO find a better way to select the pull down
	return cy.get('rcc-pull-down-select').eq(0)
}

export function findReceiveDataTransmissionPullDown() : Chainable<JQuery> {
	// TODO find a better way to select the pull down
	return cy.get('rcc-pull-down-select').eq(1)
}

export function findAnswerOptionValueForPAT(translations: TranslationTable, answerIndex: number): Chainable {
	return cy.get('rcc-widget').within(() => {
		cy.findAllByRole('radio')
			.eq(answerIndex)
			.parent()
			.findByRole('radio', translateName(translations))
	})
}

export function clickNext(position: number): void {
	cy.get('rcc-card').eq(position).within(() => {
		cy.findByRole('button', translateName({
			de: 'Nächste Frage',
			en: 'Next question',
		})).should('be.visible').click().wait(250)
	})
}

export function clickPrevious(position: number): void {
	cy.get('rcc-card').eq(position).within(() => {
		cy.findByRole('button', translateName({
			de: 'Zurück zur letzte Frage',
			en: 'Previous question',
		})).should('be.visible').click().wait(250)
	})
}

export function openDailyQuestions(): void {
	cy.findByRole('link', translateName({
		en: 'Answer questions',
		de: 'Fragen beantworten'
	})).should('be.visible').click()
}

export function assertAnswerSaved(): void {
	findElementWithLabel({
		en: 'Answer saved.',
		de: 'Antwort gespeichert.'
	}).should('be.visible')
}
export function assertNotesSaved(): void {
	findElementWithLabel({
		en: 'Your notes were saved.',
		de: 'Deine Notizen wurden gespeichert.'
	}).should('be.visible')
}
export function assertNotesDeleted(): void {
	findElementWithLabel({
		en: 'Your notes were deleted.',
		de: 'Deine Notizen wurden gelöscht.'
	}).should('be.visible')
}
export function assertAnswerChanged(): void {
	findElementWithLabel({
		en: 'Your answer was changed.',
		de: 'Deine Antwort wurde geändert.'
	}).should('be.visible')
}
export function assertAnswerDeleted(): void {
	findElementWithLabel({
		en: 'Your answer was deleted.',
		de: 'Deine Antwort wurde gelöscht.'
	}).should('be.visible')

	assertAllDoneSlide()

}

export function assertAllDoneSlide(): void {
	cy.get('rcc-card').eq(7).within((card) => {

		scrollToElementWithWait(card)

		cy.findByText(translateString({
			en: 'You answered 0 out of 6 today\'s questions.',
			de: 'Du hast heute 0 von 6 Fragen beantwortet.'
		})).should('be.visible')
	})
}
