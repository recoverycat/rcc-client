import * as path from 'path'
import * as fs from 'fs'
import Chainable = Cypress.Chainable;

export function getMostRecentFile(dir: string): string {
	const files: string[] = orderRecentFiles(dir)
	return path.join(dir, files[0])
}

function orderRecentFiles(dir: string): string[] {
	return fs.readdirSync(dir).filter((file : string)  =>
		fs.lstatSync(path.join(dir, file)).isFile()
	).map(file => ({
		name: file,
		time: fs.statSync(path.join(dir, file)).mtime.getTime()
	})).sort((a, b) => b.time - a.time).map(file => file.name)
}

export function assertNewFileWasDownloadedCorrectly(date: Date, filetype: string) : Chainable<string> {

	return checkIfFileDownloaded().then((filename) => {
		isFileNewerThanDate(filename, date).should('be.true')
		isFiletypeCorrect(filename, filetype).should('be.true')
	})

}

function checkIfFileDownloaded(): Chainable<string> {
	return cy.task<string>('getMostRecentFile', Cypress.config('downloadsFolder')).then((filename) => {

		cy.log('Most recent file:', filename)
		cy.readFile(filename).should('exist')

		cy.wrap(filename)

	})
}

function getCapturingGroups(filename: string): RegExpMatchArray | null {

	//  rcc-backup-\\d{4}-\\d{2}-\\d{2}-(\\d+)(\\.\\w{3})$
	const matches : RegExpMatchArray | null = filename.match(/rcc-backup-\d{4}-\d{2}-\d{2}-(\d+)(\.\w{3})$/)
	if (!matches)
		throw new Error(`Filename ${filename} does not match against regex`)

	return matches
}

function isFileNewerThanDate(filename: string, dateToCompare: Date): Chainable<boolean> {

	const matches : RegExpMatchArray | null = getCapturingGroups(filename)

	if (matches) {
		const fileTimestamp: number = parseInt(matches[1], 10)
		const fileDate: Date = new Date()
		fileDate.setTime(fileTimestamp)

		cy.log(`Comparing ${fileDate.toISOString()} with ${dateToCompare.toISOString()}`)

		return cy.wrap(fileDate > dateToCompare)
	} else return cy.wrap(false)

}

function isFiletypeCorrect(filename: string, filetype: string): Chainable<boolean> {
	const filetypeRegex		: RegExp	= new RegExp(`\\.${filetype}$`)
	const isCorrect 		: boolean	= filetypeRegex.test(filename)

	cy.log(`File ${filename} ends with .${filetype}: ${isCorrect}`)

	return cy.wrap(isCorrect)
}
