export interface TranslationTable{
	[key: string]: string | RegExp
}

export interface KeyValuePair {
	key: string
	value: unknown
}
