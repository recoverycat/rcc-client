import {
			clickButtonWithLabel,
			assertTitle,
			findElementWithLabel,
			findElementWithAriaLabel,
			assertInputValue,
			findElementWithExactLabel,
			checkElementWithAriaLabelIsNotVisible,
			translate,
			translateName
		}												from './e2e-utils'
import	{
			KeyValuePair,
			TranslationTable
		}												from './e2e.commons'
import Chainable = Cypress.Chainable;

/**
 * We're using the variable `states` as a global cypress variable to keep track
 * of the current state of the application. In this first case we are checking
 * the state of the accepted inclusion criteria.If the test or the user have
 * accepted those, we are notifying the user via the cy.log and go on with the
 * next tests.
 *
 * In the cypress UI runner reset the browser cache before each test run.
 * Otherwise the IndexedDB database will have the value true for the
 * inclusionCriteriaConfirmedKey while the `states` variable is still going to
 * be undefined, thus the test run will timeout.
 */
export const states : Record<string,unknown> = {}


export function enterHCPRoot(): Chainable {
	return cy.log('Opening HCP home screen')
		.then(() => cy.visit('/'))
		.then(() => {
			if (confirmHCPVariant('https://localhost:42011/')) acceptInclusionCriteriaIfNeeded() })
}

export function acceptInclusionCriteriaIfNeeded(): Chainable<null> | Chainable<boolean> {
	const criteriaAccepted	:	Chainable<null> | Chainable<boolean>
							=	states.inclusionCriteriaAccepted
								?	cy.log('Already accepted inclusion criteria')
								:	findElementWithLabel({
										de: 'Akzeptieren & starten',
										en: 'Accept & Start'
									})
									.click()
									.then(() => states.inclusionCriteriaAccepted = true)
	return criteriaAccepted
}

export function confirmHCPVariant(baseUrl: string) : boolean {
	cy.log('Checking HCP variant..')
	if (Cypress.config().baseUrl === baseUrl) return true
	else return false
}

export function clickMonitoringSetupButton(): Chainable<JQuery> {
	return findElementWithLabel({
		de: 'Therapie Follow-up',
		en: 'Therapy Follow-up'
	}).click()
}

export function clickScanQRCodeButton(): Chainable<JQuery> {
	return findElementWithLabel({
		de: 'QR-Code scannen',
		en: 'Scan QR-Code'
	}, 'button').click({ force: true })
}

export function clickStatisticsButton(): Chainable<JQuery> {
	return clickButtonWithLabel({
		de: 'Statistiken',
		en: 'Statistics'
	})
}

/**
 * Clicks the button with the arial Label marking it as the cancel button.
 *
 * @returns A Chainable JQuery object
 */
export function clickCancelButton(): Chainable {

	return clickButtonWithLabel({ en: 'Cancel', de: 'Abbrechen' })
}

export function assertHCPTitle(titleHash: TranslationTable): Chainable<string> {
	const titlePrefixHash: TranslationTable = {
		en: 'Recovery Cat · ',
		de: 'Recovery Cat · '
	}
	const fullTitleHash: TranslationTable
		= {}
	for (const lang in titlePrefixHash)
		if (Object.prototype.hasOwnProperty.call(titlePrefixHash, lang))
			fullTitleHash[lang] = titlePrefixHash[lang] + titleHash[lang]
	return assertTitle(fullTitleHash)
}

export function assertNavigationMenuIsOpen(): Chainable {
	findElementWithAriaLabel({
		de: 'Recovery Cat Navigation',
		en: 'Recovery Cat Navigation'
	}).wait(500).should('exist')
	findElementWithLabel({
		de: 'Startseite',
		en: 'Homepage'
	}).wait(500).should('be.visible')
	findElementWithLabel({
		de: 'Einstellungen',
		en: 'Settings'
	}).wait(500).should('be.visible')
	findElementWithLabel({
		de: 'Nach Updates suchen',
		en: 'Check for Updates'
	}).wait(500).should('be.visible')
	return findElementWithLabel({
		de: 'Daten löschen',
		en: 'Delete all data'
	}).wait(500).should('be.visible')
}

export function clickTemplatePullDownSelect(): Chainable {
	return cy.get('rcc-pull-down-select[formcontrolname="template"]')
		.click()
}

export function findTemplatePullDownSelect(): Chainable {
	return cy.get('rcc-pull-down-select[formcontrolname="template"]')
}

// TODO currently finds all back buttons
export function findScrollBackButton(): Chainable {
	return findElementWithAriaLabel({
		en: 'Scroll back',
		de: 'Zurück scrollen'
	})
}

export function findBackButton() : Chainable {
	// TODO wrong aria labels
	return findElementWithAriaLabel({
		en: 'Back',
		de: 'Zurück'
	})
}

// TODO currently finds all forward buttons
export function findScrollForwardButton(): Chainable {
	return findElementWithAriaLabel({
		en: 'Scroll forward',
		de: 'Vorwärts scrollen'
	})
}

export function findCloseButton(): Chainable {
	return findElementWithAriaLabel({
		en: 'Close',
		de: 'Schließen'
	}).last()
}

export function assertDateDisplayIsVisible(): Chainable {
	const dateOptions: Intl.DateTimeFormatOptions = {
		month: '2-digit',
		year: 'numeric',
		day: '2-digit',
		weekday: 'long',
	}
	const todayDe: string = new Date().toLocaleString(
		'de-DE', dateOptions
	)
	const todayEn: string = new Date().toLocaleString(
		'en-GB', dateOptions)

	return findElementWithLabel({
		de: todayDe,
		en: todayEn
	})
}

export function findDeleteButtonOfQuestion(questionHash: TranslationTable): Chainable {
	const deleteButtonPrefixHash: TranslationTable = {
		en: 'Remove: ',
		de: 'Entfernen: '
	}

	const deleteButtonHash: TranslationTable = {}

	for (const lang in deleteButtonPrefixHash)
		if (Object.prototype.hasOwnProperty.call(deleteButtonPrefixHash, lang))
			deleteButtonHash[lang] = deleteButtonPrefixHash[lang] + questionHash[lang]

	return findElementWithAriaLabel(deleteButtonHash)
}

export function clickEditButtonOfQuestion(questionHash: TranslationTable): Chainable {
	const editButtonPrefixHash: TranslationTable = {
		en: 'Edit: ',
		de: 'Bearbeiten: '
	}

	const editButtonHash: TranslationTable = {}

	for (const lang in editButtonPrefixHash)
		if (Object.prototype.hasOwnProperty.call(editButtonPrefixHash, lang))
			editButtonHash[lang] = editButtonPrefixHash[lang] + questionHash[lang]

	return findElementWithAriaLabel(editButtonHash).click()
}

export function findQuestionnaireTitle(): Chainable {
	return cy.findByRole('textbox', translateName({
		de: 'Fragebogentitel',
		en: 'Question title'
	}))
}

// TODO last() is a workaround for the fact that there are two elements with the identifier
export function findLabelInsideMonitoringModal(labelHash: TranslationTable): Chainable {
	// TODO find a better way of identifying the modal
	return cy.get('rcc-monitoring-setup-modal-layout-large').last().within(() => {
		findElementWithLabel(labelHash).should('be.visible')
	})
}

export function checkFilteringInsideMonitoringModal(labelHash: TranslationTable): Chainable {
	return cy.get('rcc-monitoring-setup-modal-layout-large').last().within(() => {
		cy.findByRole('combobox', translateName({
			en: 'Filter',
			de: 'Filter'
		})).as('filterBox')

		cy.get('@filterBox').click()
		findElementWithLabel(labelHash)

		// Find the listbox referenced by the combobox, and check for elements with the
		// role 'option'
		cy.get('@filterBox').invoke('attr', 'aria-controls').then(listboxId => {
			cy.get(`#${listboxId}`).within(() => {
				cy.findAllByRole('option').then($options => {
					expect($options).to.have.length(1)
				})
			})
		})
	})
}

export function findAnswerOptionValueForHCP(translations: TranslationTable, answerIndex: number): Chainable {
	return cy.get('rcc-monitoring-setup-option-editor').within(() => {
		cy.get('rcc-edit-text').eq(answerIndex).within(() => {
			assertInputValue(translations)
		})
	})
}

export function findAnswerOptionInputField(answerIndex: number): Chainable {
	return cy.get('rcc-monitoring-setup-option-editor').within(() => {
		cy.get('rcc-edit-text').eq(answerIndex)
	})
}

export function findQuestionTypePulldown(): Chainable<JQuery> {
	return findElementWithAriaLabel({
		en: 'Choose question type',
		de: 'Fragentyp auswählen'
	})
}

export function findCustomQuestionButton(): Chainable {
	return findElementWithLabel({
		en: 'Custom Question',
		de: 'Eigene Frage'
	})
}

export function findScheduleCheckBoxOfDay(dayHash: TranslationTable): Chainable {
	return findElementWithAriaLabel(dayHash, 'input[type="checkbox"]')
}

export function findToggleButton(): Chainable {
	return cy.get('rcc-toggle')
}

export function clickSaturdayScheduleCheckBox(): Chainable {
	return findElementWithExactLabel({
		en: 'Sat',
		de: 'Sa'
	}).click()
}

export function clickSundayScheduleCheckBox(): Chainable {
	return findElementWithExactLabel({
		en: 'Sun',
		de: 'So'
	}).click()
}

// questionnaire template options

export function findDepressionTemplateOption(): Chainable<JQuery> {
	return findElementWithLabel({
		en: 'Depression',
		de: 'Depression'
	}, 'div[role="option"]')
}

export function findEmptyTemplateOption(): Chainable<JQuery> {
	return findElementWithLabel({
		en: 'Empty template',
		de: 'Leere Vorlage'
	}, 'div[role="option"]')
}

export function findDefaultReminderButton(): Chainable<JQuery> {
	return findElementWithLabel({
		en: 'Reminder, 12:00 PM',
		de: 'Erinnerung, 12:00 Uhr'
	}, 'rcc-quick-action-list')
}

export function findAndIncrementReminder(): Chainable<JQuery> {

	cy.wait(500)
	findDefaultReminderButton()
		.wait(500)
		.click()

	incrementHoursInTimePicker()

	incrementMinutesInTimePicker()

	// TODO find a better way of identifying the submit button (issue created)
	return cy.get('rcc-submit-button')
		.click()
}

export function findDefaultScheduleButton(): Chainable<JQuery> {
	return findElementWithLabel({
		en: 'daily',
		de: 'täglich'
	}, 'rcc-quick-action-list')
}

export function assertAllDaysAreChecked(): Chainable {
	findScheduleCheckBoxOfDay({
		en: 'Monday',
		de: 'Montag'
	}).should('be.checked')

	findScheduleCheckBoxOfDay({
		en: 'Tuesday',
		de: 'Dienstag'
	}).should('be.checked')

	findScheduleCheckBoxOfDay({
		en: 'Wednesday',
		de: 'Mittwoch'
	}).should('be.checked')

	findScheduleCheckBoxOfDay({
		en: 'Thursday',
		de: 'Donnerstag'
	}).should('be.checked')

	findScheduleCheckBoxOfDay({
		en: 'Friday',
		de: 'Freitag'
	}).should('be.checked')

	return findScheduleCheckBoxOfDay({
		en: 'Saturday',
		de: 'Samstag'
	}).should('be.checked')
}

export function findScheduleButtonWithLabel(labelHash: TranslationTable): Chainable<JQuery> {
	return findElementWithLabel(labelHash, 'rcc-quick-action-list')
}

export function findHoursInTimePicker(): Chainable<JQuery> {
	return findElementWithAriaLabel({
		en: 'Hours',
		de: 'Stunden'
	})
}

export function findMinutesInTimePicker(): Chainable<JQuery> {
	return findElementWithAriaLabel({
		en: 'Minutes',
		de: 'Minuten'
	})
}

export function incrementHoursInTimePicker(): Chainable {
	return findHoursInTimePicker()
		.should('be.visible')
		.click()
		.wait(500)
		.type('{downarrow}')
}

export function incrementMinutesInTimePicker(): Chainable {
	return findMinutesInTimePicker()
		.should('be.visible')
		.click()
		.wait(500)
		.type('{downarrow}')
}

// question category buttons

export function findSymptomQuestionCategoryButton(): Chainable<JQuery> {
	return findElementWithLabel({
		en: 'Query Symptoms',
		de: 'Symptom abfragen'
	}).wait(500)
}

export function findMedicationQuestionCategoryButton(): Chainable<JQuery> {
	return findElementWithLabel({
		en: 'Query Medication',
		de: 'Medikation abfragen'
	})
}

export function findResourceQuestionCategoryButton(): Chainable<JQuery> {
	return findElementWithLabel({
		en: 'Query Resources',
		de: 'Ressource abfragen'
	})
}

export function findEarlyWarningSignsQuestionCategoryButton(): Chainable<JQuery> {
	return findElementWithLabel({
		en: 'Query Early Warning Signs',
		de: 'Warnzeichen abfragen'
	})
}

export function findSideEffectsQuestionCategoryButton(): Chainable<JQuery> {
	return findElementWithLabel({
		en: 'Query Side Effects',
		de: 'Nebenwirkung abfragen'
	})
}

export function findWithdrawalSymptomsQuestionCategoryButton(): Chainable<JQuery> {
	return findElementWithLabel({
		en: 'Query Withdrawal Symptoms',
		de: 'Absetzsymptom abfragen'
	})
}

export function findQrCode() : Chainable<JQuery> {
	// TODO replace with a screen reader friendly identifier
	return cy.get('rcc-qr-code')
}

export function findQrCodeScanner() : Chainable<JQuery> {
	// TODO replace with a screen reader friendly identifier
	return cy.get('rcc-qr-code-scanner')
}

export function findNonEditableQuestionWithLabel(labelHash: TranslationTable): Chainable<JQuery> {

	findElementWithLabel(labelHash).should('exist')

	const editLabelHash : TranslationTable = {
		en: 'Edit: ' + labelHash.en,
		de: 'Bearbeiten: ' + labelHash.de
	}

	return checkElementWithAriaLabelIsNotVisible(editLabelHash)
}

export function findDosageInput(): Chainable<JQuery> {
	return cy.findByRole('textbox', translateName({
		de: 'Dosis',
		en: 'Dosage',
	}))
}

export function typeTranslatedText(textHash: TranslationTable, cypressObject: Chainable<JQuery>) : Chainable<string> {
	return translate(textHash).then((translatedText: string) => {
		cypressObject.type(translatedText)
	})
}

export function findMedicationDropDownSelect() : Chainable<JQuery> {

	return cy.get('rcc-pull-down-select[formcontrolname="medication"]')

}

export function findLanguagePullDown() : Chainable<JQuery> {
	// TODO add aria labels to the language pull down
	return cy.get('rcc-pull-down-select')
}


/**
 * This is only needed in the medical product version to skip the inclusion and
 * acceptance criteria modal and can be ignored in other versions since they
 * don't display said modal.
 */
export function setInclusionCriteriaConfirmed(confirmed: boolean) : Chainable {
	return cy.window()
		.then(window => {

			const dbRequest: IDBOpenDBRequest = window.indexedDB.open('rccDatabase', 1)

			dbRequest.onerror = () => {
				throw new Error('Error opening IndexDB database')
			}

			dbRequest.onsuccess = (successEvent: Event) => {

				if (successEvent.target == null)
					return

				const dataBase: IDBDatabase = (successEvent.target as IDBOpenDBRequest).result

				const transaction: IDBTransaction = dataBase.transaction('rccStorage', 'readwrite')

				transaction.onerror = () => {
					throw new Error('Error opening transaction')
				}

				const store: IDBObjectStore = transaction.objectStore('rccStorage')

				const inclusionCriteriaKey: IDBRequest<string> = store.get('rcc-key-value') as IDBRequest<string>

				inclusionCriteriaKey.onerror = () => {
					throw new Error('Error getting settings')
				}

				inclusionCriteriaKey.onsuccess = (inclusionCriteriaSuccessEvent: Event) => {
					if (inclusionCriteriaSuccessEvent.target == null)
						return

					let inclusionCriteriaKeyData: string = (inclusionCriteriaSuccessEvent.target as IDBRequest<string>).result

					if (inclusionCriteriaKeyData == null) inclusionCriteriaKeyData = '[]'

					const inclusionCriteriaObject: KeyValuePair[] = JSON.parse(inclusionCriteriaKeyData) as KeyValuePair[]
					const newInclusionCriteriaObject: KeyValuePair[] = updateInclusionCriteriaConfirmed(inclusionCriteriaObject, confirmed)

					const newInclusionCriteriaKeyData: string = JSON.stringify(newInclusionCriteriaObject)

					const putRequest : IDBRequest = store.put(newInclusionCriteriaKeyData, 'rcc-key-value')

					putRequest.onerror = () => {
						throw new Error('Error putting key value')
					}
				}
			}
		})
}

function updateInclusionCriteriaConfirmed(inclusionCriteriaObject: KeyValuePair[], confirmed: boolean) : KeyValuePair[] {

	let keyFound : boolean = false

	for ( const inclusionCriteria of inclusionCriteriaObject )
		if (inclusionCriteria.key === 'inclusionCriteriaConfirmed') {
			inclusionCriteria.value = confirmed
			keyFound = true
			break
		}

	if (!keyFound)
		inclusionCriteriaObject.push({
			key: 'inclusionCriteriaConfirmed',
			value: confirmed
		})

	return inclusionCriteriaObject
}
