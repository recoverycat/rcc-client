import { clickButtonWithLabel, clickHomeBrandButton, clickMainMenuButton, translateName } from '../../../../utils/e2e-utils'
import { enterHCPRoot } from '../../../../utils/hcp-utils'

describe('G_001 Customer Support Entry & Form', () => {

	beforeEach(() => {

		enterHCPRoot()

	})

	describe('Test feedback form', () => {

		beforeEach(() => {

			clickMainMenuButton()

			clickButtonWithLabel({
				de: 'Hilfe & Benutzerhandbuch',
				en: 'Help & User manual',
			})

			clickButtonWithLabel({ de: 'Feedback senden', en: 'Send Feedback' })

		})

		it('Should open feedback form when button is clicked (G_001.8)', () => {

			cy.findByRole('form', translateName({
				de: 'Kontaktformular',
				en: 'Contact form'
			})).should('be.visible')

		})

		it('Should have a Name text input field (G_001.9)', () => {

			cy.findByRole('textbox', translateName({ de: /Name/, en: /Name/ })).should('be.visible')

		})

		it('Should have an Email text input field (G_001.10)', () => {

			cy.findByRole('textbox', translateName({ de: /E-Mail/, en: /Email/ })).should('be.visible')

		})

		it('Should have a Subject text input field (G_001.11)', () => {

			cy.findByRole('textbox', translateName({ de: /Betreff/, en: /Subject/ })).should('be.visible')

		})

		it('Should have a Category drop down field (G_001.12)', () => {

			cy.findByRole('combobox', translateName({ de: /Worum geht es\?/, en: /What is it about\?/ })).should('be.visible')

		})

		it('Should spawn another drop down field if the category "Bug" is selected (G_001.13)', () => {

			cy.findByRole('combobox', translateName({ de: /Worum geht es\?/, en: /What is it about\?/ })).click()
			cy.findByRole('option', translateName({ de: /Bug\/Fehler/, en: /Bug\/Error/ })).click()
			cy.findByRole('combobox', translateName({ de: /Wo trat das Problem auf\?/, en: /Where did the problem occur\?/ }))

		})

		it('Should spawn a checked checkbox "Send debug info" if the category "Bug" is selected (G_001.16)', () => {

			cy.findByRole('combobox', translateName({ de: /Worum geht es\?/, en: /What is it about\?/ })).click()
			cy.findByRole('option', translateName({ de: /Bug\/Fehler/, en: /Bug\/Error/ })).click()
			cy.findByRole('checkbox', translateName({ de: /Debug-Informationen senden/, en: /Send debug info/ }))
				.should('be.checked')

		})

		it('Should have a Message text input field (G_001.14)', () => {

			cy.findAllByRole('textbox', translateName({ de: /Nachricht/, en: /Message/ })).should('be.visible')

		})

		it('Should not be possible to submit the form without filling out the required fields', () => {

			clickButtonWithLabel({ de: 'Abschicken', en: 'Send' })
			cy.findAllByText(translateName({ de: /[Bitte fülle dieses Feld aus.]/, en: /[This field is required.]/ }).name).should('be.visible')
			cy.findByRole('form', translateName({
				de: 'Kontaktformular',
				en: 'Contact form'
			})).should('be.visible')

		})

		it('Should allow returning to the help page after submitting form', () => {

			cy.findByRole('textbox', translateName({ de: /E-Mail/, en: /Email/ })).type('test@example.com')
			cy.findByRole('combobox', translateName({ de: /Worum geht es\?/, en: /What is it about\?/ })).click()
			cy.findAllByRole('option').first().click()
			cy.findAllByRole('textbox', translateName({ de: /Nachricht/, en: /Message/ })).type('Example message')
			
			clickButtonWithLabel({ de: 'Abschicken', en: 'Send' })

			clickButtonWithLabel({ de: 'Okay', en: 'Okay' })

			cy.findByRole('form', translateName({
				de: 'Kontaktformular',
				en: 'Contact form'
			})).should('not.exist')


		})

		it('Should return to home page', () => {

			clickHomeBrandButton()

		})
	})
})
