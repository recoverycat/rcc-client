import { assertHomePage, clickButtonWithLabel, clickHomeBrandButton, clickMainMenuButton, findEmail, findTelephoneNumber } from '../../../../utils/e2e-utils'
import { assertHCPTitle, enterHCPRoot } from '../../../../utils/hcp-utils'

describe('A_014.2 HCP Impressum', () => {
	beforeEach(() => {
		enterHCPRoot()
	})

	describe('Navigate to impressum', () => {

		beforeEach(() => {
			clickMainMenuButton()

			clickButtonWithLabel({
				de: 'Impressum',
				en: 'Imprint',
			})
		})

		it('Should open impressum', () => {
			cy.get('h1').should('contain.text', 'Impressum')
			assertHCPTitle({ en: 'Imprint', de: 'Impressum' })
		})

		it('Should include a phone number', () => {
			findTelephoneNumber().should('be.visible')
		})

		// TODO: this test is failing with
		// This element `<a>` is not visible because it has CSS property: `position: fixed` and it's being covered by another element:
		// <strong>Geschäf...</strong>
		it.skip('Should include an email', () => {
			findEmail().should('be.visible')
		})

		it('Should return to home page', () => {
			clickHomeBrandButton()
			assertHomePage()
		})
	})
})
