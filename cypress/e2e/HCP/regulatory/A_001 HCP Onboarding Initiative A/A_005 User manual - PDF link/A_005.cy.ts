import { clickButtonWithLabel, clickLinkInSameTab, clickMainMenuButton, findButtonWithLabel } from '../../../../utils/e2e-utils'
import { enterHCPRoot } from '../../../../utils/hcp-utils'

describe('A_005 User manual (PDF link)', () => {
	beforeEach(() => {
		enterHCPRoot()
	})

	describe('Navigate to help page', () => {

		beforeEach(() => {
			clickMainMenuButton()

			clickButtonWithLabel({
				de: 'Hilfe & Benutzerhandbuch',
				en: 'Help & User manual',
			})
		})

		it('Should have PDF link that opens in a new tab', () => {
			findButtonWithLabel({
				de: 'Link zur PDF Version',
				en: 'Link to PDF version',
			}).should('have.attr', 'target', '_blank')
		})

		it('Should open PDF link', () => {
			clickLinkInSameTab({
				de: 'Link zur PDF Version',
				en: 'Link to PDF version',
			})

			cy.url().should('contain', '.pdf')
		})
	})
})
