import {
	assertAllDaysAreChecked,
	clickMonitoringSetupButton,
	clickSaturdayScheduleCheckBox,
	clickSundayScheduleCheckBox,
	enterHCPRoot,
	findCustomQuestionButton,
	findDepressionTemplateOption,
	findDefaultScheduleButton,
	findScheduleButtonWithLabel,
	findSymptomQuestionCategoryButton,
	findTemplatePullDownSelect,
	findQuestionTypePulldown,
} from '../../../../utils/hcp-utils'
import {
	findButtonWithLabel,
	findElementWithAriaLabel,
	findElementWithLabel, findElementWithName,
	findSubmitButton,
	getCurrentlyOpenModal,
} from '../../../../utils/e2e-utils'

describe('B_006 Scheduling Questions', () => {

	beforeEach(() => {
		enterHCPRoot()
		clickMonitoringSetupButton()
	})

	describe('template selected (B_004.1)', () => {

		beforeEach(() => {
			findTemplatePullDownSelect()
				.click()

			findDepressionTemplateOption()
				.click()

			findButtonWithLabel({
				en: 'Edit',
				de: 'Bearbeiten'
			}).click().wait(500)
		})

		it('B_006.2 should be possible top set a base schedule for the whole questionnaire', () => {

			findDefaultScheduleButton()
				.click()

			getCurrentlyOpenModal().within(() => {
				assertAllDaysAreChecked()

				clickSaturdayScheduleCheckBox()
				clickSundayScheduleCheckBox()

				findSubmitButton().click()
			})

			findScheduleButtonWithLabel({
				en: 'Mo, Tu, We, Th, Fr',
				de: 'Mo, Di, Mi, Do, Fr'
			})
		})

		it('B_006.6 should be possible to set a custom schedule for a custom question', () => {

			findSymptomQuestionCategoryButton()
				.click()

			findCustomQuestionButton()
				.click()

			// #region custom queston modal
			getCurrentlyOpenModal().within(() => {

				findQuestionTypePulldown()
					.click()

				findElementWithLabel({
					en: 'Yes/No question (1/0)',
					de: 'Ja/Nein Frage (1/0)'
				}).click()

				findElementWithName(
					{
						en: 'Edit question title',
						de: 'Fragentitel bearbeiten'
					}
				).scrollIntoView().click()

				findElementWithAriaLabel(
					{
						en: 'Question title',
						de: 'Fragentitel'
					}
				).type('This is a test Question')

				findElementWithName(
					{
						en : 'Finish editing question title',
						de: 'Fragentitel bestätigen'
					}
				).scrollIntoView().click()

				findElementWithAriaLabel({
					de:	'Individuellen Zeitplan setzen',
					en:	'Set individual schedule'
				}).click()
			})
			// #endregion

			// #region schedule modal
			getCurrentlyOpenModal().within(() => {
				findElementWithLabel({
					en: 'Individual Schedule',
					de: 'Individueller Zeitplan Auswahl'
				})

				// all days are selected by default
				assertAllDaysAreChecked()

				clickSaturdayScheduleCheckBox()
				clickSundayScheduleCheckBox()

				findSubmitButton().click()
			})
			// #endregion

			// #region custom question modal
			getCurrentlyOpenModal().within(() => {
				findElementWithLabel({
					en: 'Mo, Tu, We, Th, Fr',
					de: 'Mo, Di, Mi, Do, Fr'
				}).should('exist')

				findSubmitButton().click()
			})
			// #endregion

			findElementWithLabel({
				en: 'Mo, Tu, We, Th, Fr',
				de: 'Mo, Di, Mi, Do, Fr'
			}).should('exist')
		})

	})

	describe('No template selected (B_004.2)', () => {

		it('B_006.2 should be possible top set a base schedule for the whole questionnaire', () => {
			findDefaultScheduleButton()
				.click()

			// #region schedule modal
			getCurrentlyOpenModal().within(() => {
				assertAllDaysAreChecked()

				clickSaturdayScheduleCheckBox()
				clickSundayScheduleCheckBox()

				findSubmitButton().click()
			})
			// #endregion

			findScheduleButtonWithLabel({
				en: 'Mo, Tu, We, Th, Fr',
				de: 'Mo, Di, Mi, Do, Fr'
			})
		})

		it('B_006.6 should be possible to set a custom schedule for a custom question (No template)',  () => {
			findSymptomQuestionCategoryButton()
				.click()

			findCustomQuestionButton()
				.click()

			// #region custom question modal
			getCurrentlyOpenModal().within(() => {
				findQuestionTypePulldown()
					.click()

				findElementWithLabel({
					en: 'Yes/No question (1/0)',
					de: 'Ja/Nein Frage (1/0)'
				}).click()

				findElementWithName(
					{
						en: 'Edit question title',
						de: 'Fragentitel bearbeiten'
					}
				).scrollIntoView().click()

				findElementWithAriaLabel(
					{
						en: 'Question title',
						de: 'Fragentitel'
					}
				).type('This is a test Question')

				findElementWithName(
					{
						en : 'Finish editing question title',
						de: 'Fragentitel bestätigen'
					}
				).scrollIntoView().click()

				findElementWithLabel(
					{
						de:	'Individuellen Zeitplan setzen',
						en:	'Set individual schedule'
					},
					'button'
				).click()
			})
			// #endregion

			// #region schedule modal
			getCurrentlyOpenModal().within(() => {
				findElementWithLabel({
					en: 'Individual Schedule',
					de: 'Individueller Zeitplan Auswahl'
				})

				// all days are selected by default

				assertAllDaysAreChecked()

				clickSaturdayScheduleCheckBox()
				clickSundayScheduleCheckBox()

				findSubmitButton().click()
			})
			// #endregion

			// #region custom question modal
			getCurrentlyOpenModal().within(() => {
				findElementWithLabel({
					en: 'Mo, Tu, We, Th, Fr',
					de: 'Mo, Di, Mi, Do, Fr'
				}).should('exist')

				findSubmitButton().click()
			})
			// #endregion

			findElementWithLabel({
				en: 'Mo, Tu, We, Th, Fr',
				de: 'Mo, Di, Mi, Do, Fr'
			}).should('exist')
		})
	})
})
