import {
	clickMonitoringSetupButton,
	enterHCPRoot,
	findDepressionTemplateOption, findDosageInput, findMedicationDropDownSelect,
	findTemplatePullDownSelect
} from '../../../../utils/hcp-utils'
import {
	findElementWithLabel,
	translateName,
} from '../../../../utils/e2e-utils'

describe('B_015 Add and Edit Medication', () => {

	const testBehavior: () => void = () => {
		it('should display the medication modal with all elements', function () {
			cy.findByRole('textbox', translateName({
				de: 'Fragentitel',
				en: 'Question title',
			})).should('be.visible')

			findMedicationDropDownSelect()
				.should('exist')

			findDosageInput().should('be.visible')
		})

		it('should be possible to select a medication from the list (B_015.1)', () => {
			findMedicationDropDownSelect()
				.click()

			findElementWithLabel({
				en: 'Citalopram',
				de: 'Citalopram'
			})
				.scrollIntoView()
				.click()

			findElementWithLabel({
				en: 'Citalopram',
				de: 'Citalopram'
			})
				.scrollIntoView()
				.should('exist')
		})

		it('should be possible to set a dosage (B_015.2)', function () {
			findDosageInput().type('20mg')

			findDosageInput().should('have.value', '20mg')
		})

		it('should be possible to change the question wording (B_015.3)', function () {

			cy.findByRole('textbox', translateName({
				de: 'Fragentitel',
				en: 'Question title',
			})).should('be.visible').clear().type('TestQuestion')

			cy.findByRole('textbox', translateName({
				de: 'Fragentitel',
				en: 'Question title',
			})).should('have.value', 'TestQuestion')
		})

		it('should be possible to add the question to the monitoring', function () {
			// select medication
			findMedicationDropDownSelect()
				.click()

			findElementWithLabel({
				en: 'Citalopram',
				de: 'Citalopram'
			})
				.scrollIntoView()
				.click()


			findDosageInput().type('20mg')

			cy.findByRole('textbox', translateName({
				de: 'Fragentitel',
				en: 'Question title',
			})).last().clear().type('TestQuestion')

			// submit
			cy.findByRole('button', translateName({
				de: 'Bestätigen',
				en: 'Submit'
			})).click()

			findElementWithLabel({
				en: 'TestQuestion (Citalopram, 20mg)',
				de: 'TestQuestion (Citalopram, 20mg)'
			})
		})
	}

	beforeEach(() => {

		enterHCPRoot()

		clickMonitoringSetupButton()

	})

	describe('with Template selected (B_015.1)', () => {

		beforeEach(() => {
			findTemplatePullDownSelect()
				.click()

			// Wait for non-initial templates to load
			cy.wait(400)

			findDepressionTemplateOption()
				.scrollIntoView()
				.click()

			findElementWithLabel({
				en: 'Query Medication',
				de: 'Medikation abfragen'
			}).click()

		})

		testBehavior()
	})

	describe('without Template selected (B_015.4)', () => {

		beforeEach(() => {

			findElementWithLabel({
				en: 'Query Medication',
				de: 'Medikation abfragen'
			}).click()

		})

		testBehavior()
	})
})
