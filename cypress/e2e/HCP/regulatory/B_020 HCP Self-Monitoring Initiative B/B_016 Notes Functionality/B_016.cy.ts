import {
	clickMonitoringSetupButton,
	enterHCPRoot,
	findDepressionTemplateOption,
	findTemplatePullDownSelect
} from '../../../../utils/hcp-utils'
import { findElementWithLabel } from '../../../../utils/e2e-utils'

describe('B_016 Notes Functionality', () => {

	beforeEach(() => {

		enterHCPRoot()
		clickMonitoringSetupButton()

		findTemplatePullDownSelect()
			.click()

		findDepressionTemplateOption()
			.click()

	})

	it('should show the daily note', () => {

		findElementWithLabel({
			en: 'Share',
			de: 'Teilen',
		})

		findElementWithLabel({
			en: 'What else happened today?',
			de: 'Was ist heute sonst noch passiert?'
		}).scrollIntoView().should('be.visible')

	})

})
