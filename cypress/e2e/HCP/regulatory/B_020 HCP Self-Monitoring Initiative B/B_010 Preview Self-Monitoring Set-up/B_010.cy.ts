import {
	assertAllDaysAreChecked,
	clickMonitoringSetupButton, clickSaturdayScheduleCheckBox, clickSundayScheduleCheckBox,
	enterHCPRoot, findAndIncrementReminder,
	findDepressionTemplateOption, findDefaultScheduleButton, findNonEditableQuestionWithLabel, findQrCode,
	findTemplatePullDownSelect
} from '../../../../utils/hcp-utils'
import { findElementWithLabel } from '../../../../utils/e2e-utils'

describe('B_010 Preview Self-Monitoring Set-up', () => {

	beforeEach(() => {

		// set up basic monitoring

		enterHCPRoot()
		clickMonitoringSetupButton()

		findTemplatePullDownSelect()
			.scrollIntoView()
			.click()
			.wait(500)


		findDepressionTemplateOption()
			.click()

		findAndIncrementReminder()

		findDefaultScheduleButton()
			.click()

		assertAllDaysAreChecked()

		clickSaturdayScheduleCheckBox()
		clickSundayScheduleCheckBox()

		// TODO find better way to identify the submit button (issue created)
		cy.get('rcc-submit-button').click()

	})

	it('should go into preview mode directly with reminder and schedule editable', function () {

		findNonEditableQuestionWithLabel({
			en: 'How well were you able to manage your usual daily tasks today?',
			de: 'Konnten Sie Ihren Alltag heute so gut wie sonst bewältigen?'
		})

		findNonEditableQuestionWithLabel({
			en: 'Did you feel down today?',
			de: 'Haben Sie sich heute niedergeschlagen gefühlt?'
		})

		findNonEditableQuestionWithLabel({
			en: 'Was your energy low today?',
			de: 'Sind Sie antriebslos?'
		})

		findElementWithLabel({
			en: 'Mo, Tu, We, Th, Fr',
			de: 'Mo, Di, Mi, Do, Fr'
		}).should('not.be.disabled')

		findElementWithLabel({
			en: 'Reminder, 13:01h',
			de: 'Erinnerung, 13:01 Uhr'
		}).should('not.be.disabled')

		findElementWithLabel({
			en: 'Share',
			de: 'Teilen',
		}).click()

		findQrCode()

	})
})
