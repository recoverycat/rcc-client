import {
	clickMonitoringSetupButton,
	enterHCPRoot,
	findDepressionTemplateOption, findQrCode,
	findTemplatePullDownSelect
} from '../../../../utils/hcp-utils'
import { findElementWithLabel } from '../../../../utils/e2e-utils'

describe('B_009 HCP QR-Code Share Questionnaire', () => {

	beforeEach(() => {
		enterHCPRoot()
		clickMonitoringSetupButton().wait(500)
	})

	it('should be possible to open the qr-code page to transmit the questionnaire', function () {

		findTemplatePullDownSelect()
			.click().wait(500)

		findDepressionTemplateOption()
			.click().wait(500)

		findElementWithLabel({
			en: 'Share',
			de: 'Teilen',
		}).click()

		// Wait for the modal to show up
		cy.wait(500)

		findQrCode().should('exist')

	})

})
