import {
	clickCancelButton,
	findDeleteButtonOfQuestion,
	clickMonitoringSetupButton,
	clickTemplatePullDownSelect,
	enterHCPRoot, findDepressionTemplateOption, findQuestionnaireTitle, findTemplatePullDownSelect,
	findEmptyTemplateOption
} from '../../../../utils/hcp-utils'
import { findButtonWithLabel, findElementWithLabel } from '../../../../utils/e2e-utils'

describe('B_004.1 Template Selected', () => {

	beforeEach(() => {
		enterHCPRoot()
		clickMonitoringSetupButton()
	})

	describe('Back Button', () => {

		it('should return to the Homescreen selection screen', function () {
			clickCancelButton()

			findElementWithLabel({
				en: 'Welcome to Recovery Cat, you are using version',
				de: 'Willkommen bei Recovery Cat, Du nutzt Version'
			})
		})
	})

	describe('Template selection', () => {

		it('should show all templates in the pull down', function () {
			clickTemplatePullDownSelect()

			findDepressionTemplateOption()

			// wait for questions to be added
			cy.wait(700)

			findElementWithLabel({
				en: 'Bipolar Affective Disorder',
				de: 'Bipolare affektive Störung'
			})

			findElementWithLabel({
				en: 'Psychose',
				de: 'Psychose'
			})

			findElementWithLabel({
				en: 'Schizoaffektive Störungen',
				de: 'Schizoaffektive Störungen'
			})

		})

		it('should go to edit mode when the empty template is selected', function () {
			clickTemplatePullDownSelect()

			// Assuming findEmptyTemplateOption is a function to find the empty template option
			findEmptyTemplateOption().click()

			// wait for the mode to change
			cy.wait(700)

			// check to see if the mode has changed to edit
			findElementWithLabel({
				en: 'Preview',
				de: 'Vorschau'
			}).should('not.be.disabled')
		})

		// TODO add checks for the other tmaplates and all questions?
		it('should be possible to select a template (e.g. Depression) from a RC curated list', function () {
			clickTemplatePullDownSelect()

			findDepressionTemplateOption().click()

			// wait for questions to be added
			cy.wait(700)

			// check to see if a question from the template is displayed
			// TODO improve this check (e.g. check the category)
			findElementWithLabel({
				en: 'How well were you able to manage your usual daily tasks today?',
				de: 'Konnten Sie Ihren Alltag heute so gut wie sonst bewältigen?'
			})

			// wait for add animation to be done
			cy.wait(700)

			findButtonWithLabel({
				en: 'Share',
				de: 'Teilen'
			}).should('not.be.disabled')
		})

		it('should be possible to change the template after it has been selected', function () {

			clickTemplatePullDownSelect()

			findDepressionTemplateOption()
				.click()

			// wait for questions to be added
			cy.wait(700)

			findTemplatePullDownSelect()
				.should('exist')

		})

		it('should be possible to edit the questionnaire name after a template has been set', () => {
			clickTemplatePullDownSelect()

			findDepressionTemplateOption()
				.click()

			// wait for questions to be added
			cy.wait(700)

			findButtonWithLabel({
				en: 'Edit',
				de: 'Bearbeiten'
			}).click()

			findQuestionnaireTitle()
				.clear()
				.type('Test Questionnaire')

			findQuestionnaireTitle()
				.should('have.value', 'Test Questionnaire')

		})

	})

	describe('Question interaction', () => {

		it('should be possible to delete a question', () => {
			clickTemplatePullDownSelect()

			findDepressionTemplateOption()
				.click()

			// wait for questions to be added
			cy.wait(700)


			// check to see if a question from the template is displayed
			findElementWithLabel({
				en: 'How well were you able to manage your usual daily tasks today?',
				de: 'Konnten Sie Ihren Alltag heute so gut wie sonst bewältigen?'
			})

			findButtonWithLabel({
				en: 'Edit',
				de: 'Bearbeiten'
			}).click()

			findDeleteButtonOfQuestion({
				en: 'How well were you able to manage your usual daily tasks today?',
				de: 'Konnten Sie Ihren Alltag heute so gut wie sonst bewältigen?'
			}).as('deleteButton')
			cy.get('@deleteButton').scrollIntoView()
			cy.get('@deleteButton').click()

			findButtonWithLabel({
				en: 'yes',
				de: 'ja'
			}).click()

			// check to see if the question is no longer displayed
			findElementWithLabel({
				en: 'How well were you able to manage your usual daily tasks today?',
				de: 'Konnten Sie Ihren Alltag heute so gut wie sonst bewältigen?'
			}).should('not.exist')

		})

	})

})
