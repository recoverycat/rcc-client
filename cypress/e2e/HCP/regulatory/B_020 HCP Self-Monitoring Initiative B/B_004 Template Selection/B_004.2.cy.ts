import {
	clickCancelButton,
	clickMonitoringSetupButton,
	enterHCPRoot,
	findEarlyWarningSignsQuestionCategoryButton,
	findMedicationQuestionCategoryButton,
	findLabelInsideMonitoringModal,
	findQuestionnaireTitle,
	findResourceQuestionCategoryButton,
	findSymptomQuestionCategoryButton,
	findSideEffectsQuestionCategoryButton,
	findWithdrawalSymptomsQuestionCategoryButton,
	checkFilteringInsideMonitoringModal,
} from '../../../../utils/hcp-utils'
import {
	findElementWithLabel,
	findSubmitButton,
} from '../../../../utils/e2e-utils'

describe('B_004.2 No Template Selected', () => {

	beforeEach(() => {
		enterHCPRoot()
		clickMonitoringSetupButton()
	})

	describe('Back Button', () => {

		it('should return to the Homescreen selection screen', function () {
			clickCancelButton()

			findElementWithLabel({
				en: 'Welcome to Recovery Cat, you are using version',
				de: 'Willkommen bei Recovery Cat, Du nutzt Version'
			})
		})
	})

	describe('Questionnaire Title', () => {

		it('should be possible to set a questionnaire title after a question has been added', function () {
			// TODO this fails because the checkboxes and submit buttons are not dependably identifiable

			findSymptomQuestionCategoryButton()
				.click().wait(1000)

			cy.get('input[type="checkbox"]').first().click()

			findSubmitButton()
				.click()

			findQuestionnaireTitle()

		})

	})

	describe('Question Modals', () => {


		it('should open the symptom question modal', function () {

			findSymptomQuestionCategoryButton()
				.click()

			findLabelInsideMonitoringModal({
				en: 'Symptom question catalogue',
				de: 'Symptom Fragenkatalog'
			})

		})

		it('should open the medication question modal', function () {

			findMedicationQuestionCategoryButton()
				.click()

			findLabelInsideMonitoringModal({
				en: 'Medication',
				de: 'Medikation'
			})
		})

		it('should open the Resources question modal with prefiltering', function () {

			findResourceQuestionCategoryButton()
				.click()

			findLabelInsideMonitoringModal({
				en: 'Resource question catalogue',
				de: 'Ressource Fragenkatalog'
			})

			checkFilteringInsideMonitoringModal({
				en: 'Resource questions (36)',
				de: 'Ressourcen Fragen (36)'
			})

		})

		it('should open the Early Warning Signs question modal', function () {

			findEarlyWarningSignsQuestionCategoryButton()
				.click()

			findLabelInsideMonitoringModal({
				en: 'Early warning sign question catalogue',
				de: 'Frühwarnzeichen Fragenkatalog'
			})

		})

		it('should open the Side Effects question modal with prefiltering', function () {

			findSideEffectsQuestionCategoryButton()
				.click()

			findLabelInsideMonitoringModal({
				en: 'Side effects question catalogue',
				de: 'Nebenwirkung Fragenkatalog'
			})

			checkFilteringInsideMonitoringModal({
				en: 'Unwanted Side Effects (52)',
				de: 'Unerwünschte Nebenwirkungen (52)'
			})

		})

		it('should open the Withdrawal Symptoms question modal with prefiltering', function () {

			findWithdrawalSymptomsQuestionCategoryButton()
				.click()

			findLabelInsideMonitoringModal({
				en: 'Withdrawal symptom question catalogue',
				de: 'Absetzsymptom Fragenkatalog'
			})

			checkFilteringInsideMonitoringModal({
				en: 'Discontinuation emergent signs and symptoms (37)',
				de: 'Reduktions- und Absetzsymptome (37)'
			})

		})

	})

})
