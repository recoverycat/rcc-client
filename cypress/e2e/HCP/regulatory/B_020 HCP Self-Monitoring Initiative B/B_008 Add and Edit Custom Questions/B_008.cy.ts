import {
	findAnswerOptionValueForHCP,
	clickMonitoringSetupButton,
	clickSaturdayScheduleCheckBox,
	clickSundayScheduleCheckBox,
	enterHCPRoot,
	findAnswerOptionInputField,
	findCustomQuestionButton,
	findLabelInsideMonitoringModal,
	findSymptomQuestionCategoryButton,
	findToggleButton,
	findScrollBackButton,
	findCloseButton,
	findDepressionTemplateOption,
	findTemplatePullDownSelect
} from '../../../../utils/hcp-utils'
import {
	assertElementContainsText,
	dragAndDrop,
	findButtonWithLabel,
	findElementWithAriaLabel, findElementWithExactLabel,
	findElementWithLabel, findElementWithName,
	translateString,
} from '../../../../utils/e2e-utils'
import { TranslationTable } from '../../../../utils/e2e.commons'

describe('B_008 Add and Edit Custom Questions', () => {

	beforeEach(() => {
		enterHCPRoot()
		clickMonitoringSetupButton()
	})

	describe('with template selected (B_004.1)', function () {

		beforeEach(() => {

			findTemplatePullDownSelect()
				.scrollIntoView().click()

			findDepressionTemplateOption()
				.scrollIntoView()
				.scrollIntoView().click()

			findButtonWithLabel({
				en: 'Edit',
				de: 'Bearbeiten'
			}).click()

		})

		describe('B_008 altering question order and category', function () {

			const firstDepressionQuestionTranslationTable: TranslationTable = {
				en: 'How well were you able to manage your usual daily tasks today?',
				de: 'Konnten Sie Ihren Alltag heute so gut wie sonst bewältigen?'
			}

			const secondDepressionQuestionTranslationTable: TranslationTable = {
				en: 'Did you feel down today?',
				de: 'Haben Sie sich heute niedergeschlagen gefühlt?'
			}

			const thirdDepressionQuestionTranslationTable: TranslationTable = {
				en: 'Was your energy low today?',
				de: 'Sind Sie antriebslos?'
			}

			const symptomCategoryTranslationTable: TranslationTable = {
				en: 'Symptoms',
				de: 'Symptome'
			}

			const resourceCategoryTranslationTable: TranslationTable = {
				en: 'Resources',
				de: 'Ressourcen'
			}

			const resourceButtonTranslationTable: TranslationTable = {
				en: 'Resource Question',
				de: 'Ressource abfragen'
			}

			it('B_008 should reorder questions within the symptom category', () => {

				findElementWithLabel(firstDepressionQuestionTranslationTable).as('firstQuestion')

				findElementWithLabel(secondDepressionQuestionTranslationTable).as('secondQuestion')

				findElementWithLabel(thirdDepressionQuestionTranslationTable).as('thirdQuestion')

				assertElementContainsText('rcc-card.contains-question', firstDepressionQuestionTranslationTable, 0)
				assertElementContainsText('rcc-card.contains-question', secondDepressionQuestionTranslationTable, 1)
				assertElementContainsText('rcc-card.contains-question', thirdDepressionQuestionTranslationTable, 2)

				dragAndDrop('@secondQuestion', '@firstQuestion')
				dragAndDrop('@thirdQuestion', '@secondQuestion')

				assertElementContainsText('rcc-card.contains-question', thirdDepressionQuestionTranslationTable, 0)
				assertElementContainsText('rcc-card.contains-question', secondDepressionQuestionTranslationTable, 1)
				assertElementContainsText('rcc-card.contains-question', firstDepressionQuestionTranslationTable, 2)

			})

			it('B_008 should change the category of a question', () => {
				findElementWithLabel(firstDepressionQuestionTranslationTable).as('firstSymptomCategoryQuestion')
				findElementWithLabel(secondDepressionQuestionTranslationTable).as('secondSymptomCategoryQuestion')

				cy.get('rcc-question-lane').filter(`:contains("${translateString(symptomCategoryTranslationTable)}")`).as('symptomCategoryLane')

				cy.get('rcc-question-lane').filter(`:contains("${translateString(resourceCategoryTranslationTable)}")`).as('resourceCategoryLane')

				cy.get('rcc-card.contains-button').contains(translateString(resourceButtonTranslationTable)).as('resourceCategoryButton')

				cy.get('@symptomCategoryLane').within(() => {
					cy.get('rcc-card.contains-question').should('have.length', 3)
				})
				cy.get('@resourceCategoryLane').within(() => {
					cy.get('rcc-card.contains-question').should('have.length', 0)
				})

				dragAndDrop('@firstSymptomCategoryQuestion', '@resourceCategoryButton')
				dragAndDrop('@secondSymptomCategoryQuestion', '@resourceCategoryButton')

				cy.get('@symptomCategoryLane').within(() => {
					cy.get('rcc-card.contains-question').should('have.length', 1)
				})
				cy.get('@resourceCategoryLane').within(() => {
					cy.get('rcc-card.contains-question').should('have.length', 2)
				})

			})
		})

		it('B_008 should be possible to open the custom question modal', function () {

			findSymptomQuestionCategoryButton()
				.scrollIntoView().click().wait(500)

			findElementWithExactLabel({
				en: 'Symptom question catalogue',
				de: 'Symptom Fragenkatalog'
			}).should('be.visible')

			findCustomQuestionButton()
				.scrollIntoView().click()

			findElementWithExactLabel({
				en: 'Symptoms',
				de: 'Symptome'
			}).should('exist')

			findElementWithExactLabel({
				en: 'Edit Question',
				de: 'Frage bearbeiten'
			}).should('be.visible')


		})

		it('B_008.1 should be possible to select the type of question', function () {

			findSymptomQuestionCategoryButton()
				.scrollIntoView().click().wait(500)

			findCustomQuestionButton()
				.scrollIntoView().click().wait(500)

			// findQuestionTypePulldown()
			// .scrollIntoView().click().wait(500)

			// TODO needs to be changed back once the aria label above has been added again
			cy.get('rcc-pull-down-select')
				.last()
				.scrollIntoView().click()

			// Quantity question (3-0)
			findElementWithLabel({
				en: 'Quantity question (3-0)',
				de: 'Mengen Frage (3-0)'
			}).scrollIntoView().click().wait(500)

			// TODO better way to identify the different answer fields

			findAnswerOptionValueForHCP({
				en: 'strongly',
				de: 'stark'
			}, 0).should('exist')

			findAnswerOptionValueForHCP({
				en: 'moderately',
				de: 'mäßig'
			}, 1).should('exist')

			findAnswerOptionValueForHCP({
				en: 'somewhat',
				de: 'etwas'
			}, 2).should('exist')

			findAnswerOptionValueForHCP({
				en: 'no',
				de: 'nein'
			}, 3).should('exist')

			// findQuestionTypePulldown()
			// .scrollIntoView().click().wait(500)

			// TODO needs to be changed back once the aria label above has been added again
			cy.get('rcc-pull-down-select')
				.last()
				.scrollIntoView().click()

			// Yes/No question (1/0)
			findElementWithLabel({
				en: 'Yes/No question (1/0)',
				de: 'Ja/Nein Frage (1/0)'
			}).scrollIntoView().click().wait(500)

			findAnswerOptionValueForHCP({
				en: 'yes',
				de: 'ja'
			}, 0).should('exist')

			findAnswerOptionValueForHCP({
				en: 'no',
				de: 'nein'
			}, 1).should('exist')

			// findQuestionTypePulldown()
			// .scrollIntoView().click().wait(500)

			// TODO needs to be changed back once the aria label above has been added again
			cy.get('rcc-pull-down-select')
				.last()
				.scrollIntoView().click()

			// 4-Choice question (A-D)
			findElementWithLabel({
				en: '4-Choice question (A-D)',
				de: '4-Choice-Frage (A-D)'
			}).scrollIntoView().click().wait(500)

			findAnswerOptionValueForHCP({
				en: 'A',
				de: 'A'
			}, 0).should('exist')

			findAnswerOptionValueForHCP({
				en: 'B',
				de: 'B'
			}, 1).should('exist')

			findAnswerOptionValueForHCP({
				en: 'C',
				de: 'C'
			}, 2).should('exist')

			findAnswerOptionValueForHCP({
				en: 'D',
				de: 'D'
			}, 3).should('exist')

		})

		it('B_008.2 should be possible to set the answer option for a question', function () {

			findSymptomQuestionCategoryButton()
				.scrollIntoView().click().wait(500)

			findCustomQuestionButton()
				.scrollIntoView().click().wait(500)

			// findQuestionTypePulldown()
			// .scrollIntoView().click().wait(500)

			// TODO needs to be changed back once the aria label above has been added again
			cy.get('rcc-pull-down-select')
				.last()
				.scrollIntoView().click()

			findElementWithLabel({
				en: 'Yes/No question (1/0)',
				de: 'Ja/Nein Frage (1/0)'
			}).scrollIntoView().click().wait(500)

			// TODO better way to identify the different answer fields (no aria-label)
			findAnswerOptionInputField(0).within(() => {
				cy.get('button').eq(0).scrollIntoView().click()
				cy.get('input').first().clear().type('Custom answer option 1')
				cy.get('button').eq(1).scrollIntoView().click()
			})

			// TODO selects the second answer
			findAnswerOptionInputField(1).within(() => {
				cy.get('button').eq(2).scrollIntoView().click()
				cy.get('input').eq(1).clear().type('Custom answer option 2')
				cy.get('button').eq(3).scrollIntoView().click()
			})

		})

		it('B_008.3 should be possible to edit the question wording', function () {

			findSymptomQuestionCategoryButton()
				.scrollIntoView().click().wait(500)

			findCustomQuestionButton()
				.scrollIntoView().click().wait(500)

			findElementWithName(
				{
					en: 'Edit question title',
					de: 'Fragentitel bearbeiten'
				}
			).scrollIntoView().click()

			findElementWithAriaLabel(
				{
					en: 'Question title',
					de: 'Fragentitel'
				}
			).type('This is a test Question')

			findElementWithName(
				{
					en: 'Finish editing question title',
					de: 'Fragentitel bestätigen'
				}
			).scrollIntoView().click()

			findElementWithAriaLabel(
				{
					en: 'Question title',
					de: 'Fragentitel'
				}
			).should('have.value', 'This is a test Question')
		})

		it('B_008 should be possible to add the question to the list of questions', function () {
			findSymptomQuestionCategoryButton()
				.scrollIntoView().click().wait(500)

			findCustomQuestionButton()
				.scrollIntoView().click().wait(500)

			// findQuestionTypePulldown()
			// .scrollIntoView().click().wait(500)

			// TODO needs to be changed back once the aria label above has been added again
			cy.get('rcc-pull-down-select')
				.last()
				.scrollIntoView().click()

			findElementWithLabel({
				en: 'Yes/No question (1/0)',
				de: 'Ja/Nein Frage (1/0)'
			}).scrollIntoView().click().wait(500)

			findElementWithName(
				{
					en: 'Edit question title',
					de: 'Fragentitel bearbeiten'
				}
			).scrollIntoView().click()

			findElementWithAriaLabel(
				{
					en: 'Question title',
					de: 'Fragentitel'
				}
			).type('This is a test Question')

			findElementWithName(
				{
					en: 'Finish editing question title',
					de: 'Fragentitel bestätigen'
				}
			).scrollIntoView().click()

			// TODO better way to identify the different answer fields (no aria-label)
			findAnswerOptionInputField(0).within(() => {
				cy.get('button').eq(0).scrollIntoView().click()
				cy.get('input').first().clear().type('Custom answer option 1')
				cy.get('button').eq(1).scrollIntoView().click()
			})

			// TODO selects the second answer
			findAnswerOptionInputField(1).within(() => {
				cy.get('button').eq(2).scrollIntoView().click()
				cy.get('input').eq(1).clear().type('Custom answer option 2')
				cy.get('button').eq(3).scrollIntoView().click()
			})

			findToggleButton()
				.scrollIntoView().click()
				.wait(1500)

			findElementWithLabel({
				en: 'Individual Schedule',
				de: 'Individueller Zeitplan Auswahl'
			}).wait(500)

			clickSaturdayScheduleCheckBox()
			clickSundayScheduleCheckBox()

			cy.get('rcc-submit-button').eq(2).should('exist').scrollIntoView().click()
			// TODO find a better way of identifying the submit button

			cy.get('rcc-submit-button').eq(1).should('exist').scrollIntoView().click().wait(1000)

			findScrollBackButton().eq(0).click({ force: true }).wait(1500)

			findElementWithLabel({
				en: 'This is a test Question',
				de: 'This is a test Question'
			}).should('exist')

			findElementWithLabel({
				en: 'Custom answer option 1',
				de: 'Custom answer option 1'
			}).should('exist')

			findElementWithLabel({
				en: 'Custom answer option 2',
				de: 'Custom answer option 2'
			}).should('exist')

			findElementWithLabel({
				en: 'Mo, Tu, We, Th, Fr',
				de: 'Mo, Di, Mi, Do, Fr'
			})
		})

		it('B_008 should be possible to return to the list of questions', function () {

			findSymptomQuestionCategoryButton()
				.scrollIntoView().click().wait(500)

			findCustomQuestionButton()
				.scrollIntoView().click().wait(500)

			findCloseButton()
				.scrollIntoView().click().wait(500)

			findElementWithLabel({
				en: 'Custom questions',
				de: 'Frage bearbeiten',
			}).should('not.exist')

			findCloseButton()
				.scrollIntoView().click()
		})

	})

	describe('No template selected (B_004.2)', () => {

		it('B_008 should be possible to open the custom question modal without selecting a template', function () {

			findSymptomQuestionCategoryButton()
				.scrollIntoView().click().wait(500)

			findLabelInsideMonitoringModal({
				en: 'Symptom question catalogue',
				de: 'Symptom Fragenkatalog'
			})

			findCustomQuestionButton()
				.scrollIntoView().click()

			// TODO because modals can't be differentiated it finds two
			findLabelInsideMonitoringModal({
				en: 'Symptoms',
				de: 'Symptome'
			})

			findLabelInsideMonitoringModal({
				en: 'Edit Question',
				de: 'Frage bearbeiten'
			})


		})

		it('B_008.4 should be possible to select the type of question', function () {

			findSymptomQuestionCategoryButton()
				.scrollIntoView().click().wait(500)

			findCustomQuestionButton()
				.scrollIntoView().click().wait(500)

			// findQuestionTypePulldown()
			// .scrollIntoView().click().wait(500)

			// TODO needs to be changed back once the aria label above has been added again
			cy.get('rcc-pull-down-select')
				.last()
				.scrollIntoView().click()

			// Quantity question (3-0)
			findElementWithLabel({
				en: 'Quantity question (3-0)',
				de: 'Mengen Frage (3-0)'
			}).scrollIntoView().click().wait(500)

			findAnswerOptionValueForHCP({
				en: 'strongly',
				de: 'stark'
			}, 0).should('exist')

			findAnswerOptionValueForHCP({
				en: 'moderately',
				de: 'mäßig'
			}, 1).should('exist')

			findAnswerOptionValueForHCP({
				en: 'somewhat',
				de: 'etwas'
			}, 2).should('exist')

			findAnswerOptionValueForHCP({
				en: 'no',
				de: 'nein'
			}, 3).should('exist')

			cy.get('rcc-pull-down-select')
				.last()
				.scrollIntoView().click()

			// Yes/No question (1/0)
			findElementWithLabel({
				en: 'Yes/No question (1/0)',
				de: 'Ja/Nein Frage (1/0)'
			}).scrollIntoView().click().wait(500)

			findAnswerOptionValueForHCP({
				en: 'yes',
				de: 'ja'
			}, 0).should('exist')

			findAnswerOptionValueForHCP({
				en: 'no',
				de: 'nein'
			}, 1).should('exist')

			// findQuestionTypePulldown()
			// .scrollIntoView().click().wait(500)

			// TODO needs to be changed back once the aria label above has been added again
			cy.get('rcc-pull-down-select')
				.last()
				.scrollIntoView().click()

			// 4-Choice question (A-D)
			findElementWithLabel({
				en: '4-Choice question (A-D)',
				de: '4-Choice-Frage (A-D)'
			}).scrollIntoView().click().wait(500)

			findAnswerOptionValueForHCP({
				en: 'A',
				de: 'A'
			}, 0).should('exist')

			findAnswerOptionValueForHCP({
				en: 'B',
				de: 'B'
			}, 1).should('exist')

			findAnswerOptionValueForHCP({
				en: 'C',
				de: 'C'
			}, 2).should('exist')

			findAnswerOptionValueForHCP({
				en: 'D',
				de: 'D'
			}, 3).should('exist')

		})

		it('B_008.5 should be possible to set the answer option for a question', function () {

			findSymptomQuestionCategoryButton()
				.scrollIntoView().click().wait(500)

			findCustomQuestionButton()
				.scrollIntoView().click().wait(500)

			// findQuestionTypePulldown()
			// .scrollIntoView().click().wait(500)

			// TODO needs to be changed back once the aria label above has been added again
			cy.get('rcc-pull-down-select')
				.last()
				.scrollIntoView().click()

			findElementWithLabel({
				en: 'Yes/No question (1/0)',
				de: 'Ja/Nein Frage (1/0)'
			}).scrollIntoView().click().wait(500)

			// TODO better way to identify the different answer fields (no aria-label)
			findAnswerOptionInputField(0).within(() => {
				cy.get('button').eq(0).scrollIntoView().click()
				cy.get('input').first().clear().type('Custom answer option 1')
				cy.get('button').eq(1).scrollIntoView().click()
			})

			// TODO selects the second answer
			findAnswerOptionInputField(1).within(() => {
				cy.get('button').eq(2).scrollIntoView().click()
				cy.get('input').eq(1).clear().type('Custom answer option 2')
				cy.get('button').eq(3).scrollIntoView().click()
			})

		})

		it('B_008.6 should be possible to edit the question wording', function () {

			findSymptomQuestionCategoryButton()
				.scrollIntoView().click().wait(500)

			findCustomQuestionButton()
				.scrollIntoView().click().wait(500)

			findElementWithName(
				{
					en: 'Edit question title',
					de: 'Fragentitel bearbeiten'
				}
			).scrollIntoView().click()

			findElementWithAriaLabel(
				{
					en: 'Question title',
					de: 'Fragentitel'
				}
			).type('This is a test Question')

			findElementWithName(
				{
					en: 'Finish editing question title',
					de: 'Fragentitel bestätigen'
				}
			).scrollIntoView().click()


			findElementWithAriaLabel(
				{
					en: 'Question title',
					de: 'Fragentitel'
				}
			).should('have.value', 'This is a test Question')
		})

		it('B_008 should be possible to add the question to the list of questions without selecting a template', function () {
			findSymptomQuestionCategoryButton()
				.scrollIntoView().click().wait(500)

			findCustomQuestionButton()
				.scrollIntoView().click().wait(500)

			// findQuestionTypePulldown()
			// .scrollIntoView().click().wait(500)

			// TODO needs to be changed back once the aria label above has been added again
			cy.get('rcc-pull-down-select')
				.last()
				.scrollIntoView().click()

			findElementWithLabel({
				en: 'Yes/No question (1/0)',
				de: 'Ja/Nein Frage (1/0)'
			}).scrollIntoView().click().wait(500)

			findElementWithName(
				{
					en: 'Edit question title',
					de: 'Fragentitel bearbeiten'
				}
			).scrollIntoView().click()

			findElementWithAriaLabel(
				{
					en: 'Question title',
					de: 'Fragentitel'
				}
			).type('This is a test Question')

			// TODO better way to identify the different answer fields (no aria-label)
			findAnswerOptionInputField(0).within(() => {
				cy.get('button').eq(0).scrollIntoView().click()
				cy.get('input').first().clear().type('Custom answer option 1')
			})

			// TODO selects the second answer
			findAnswerOptionInputField(1).within(() => {
				cy.get('button').eq(2).scrollIntoView().click()
				cy.get('input').eq(1).clear().type('Custom answer option 2')
			})

			findToggleButton()
				.scrollIntoView().click()
				.wait(1500)

			findElementWithLabel({
				en: 'Individual Schedule',
				de: 'Individueller Zeitplan Auswahl'
			}).wait(500)

			clickSaturdayScheduleCheckBox()
			clickSundayScheduleCheckBox()

			cy.get('rcc-submit-button').eq(2).should('exist').scrollIntoView().click()
			// TODO find a better way of identifying the submit button

			cy.get('rcc-submit-button').eq(1).should('exist').scrollIntoView().click().wait(2000)

			findElementWithLabel({
				en: 'This is a test Question',
				de: 'This is a test Question'
			}).scrollIntoView().should('be.visible')

			findElementWithLabel({
				en: 'Custom answer option 1',
				de: 'Custom answer option 1'
			}).should('be.visible')

			findElementWithLabel({
				en: 'Custom answer option 2',
				de: 'Custom answer option 2'
			}).should('be.visible')

		})

		it('B_008 should be possible to return to the list of questions without selecting a template', function () {

			findSymptomQuestionCategoryButton()
				.scrollIntoView().click().wait(500)

			findCustomQuestionButton()
				.scrollIntoView().click().wait(500)

			findCloseButton()
				.scrollIntoView().click().wait(500)

			findElementWithLabel({
				en: 'Custom questions',
				de: 'Frage bearbeiten',
			}).should('not.exist')

			findCloseButton()
				.scrollIntoView().click()
		})

	})

})
