import {
	clickMonitoringSetupButton,
	enterHCPRoot,
	findDepressionTemplateOption, findEarlyWarningSignsQuestionCategoryButton,
	findLabelInsideMonitoringModal,
	findResourceQuestionCategoryButton, findSideEffectsQuestionCategoryButton,
	findSymptomQuestionCategoryButton,
	findTemplatePullDownSelect, findWithdrawalSymptomsQuestionCategoryButton, typeTranslatedText
} from '../../../../utils/hcp-utils'
import { findButtonWithLabel, findElementWithAriaLabel, findElementWithLabel } from '../../../../utils/e2e-utils'
import Chainable = Cypress.Chainable;

describe('B_017 Add and Edit RC Questions', () => {

	beforeEach(() => {

		enterHCPRoot()
		clickMonitoringSetupButton()

	})

	describe('With template selected (B_017.1)', () => {

		beforeEach(() => {

			findTemplatePullDownSelect()
				.click()

			findDepressionTemplateOption()
				.click()

			findButtonWithLabel({
				en: 'Edit',
				de: 'Bearbeiten'
			}).click().wait(500)

		})

		it('should be possible to open all question modals', function () {

			// symptoms
			findSymptomQuestionCategoryButton()
				.click()

			findLabelInsideMonitoringModal({
				en: 'Symptom question catalogue',
				de: 'Symptom Fragenkatalog'
			})

			// TODO differentiate close button of menu and modal
			findElementWithAriaLabel({
				en: 'Close',
				de: 'Schließen'
			}).last().click()

			// resources
			findResourceQuestionCategoryButton()
				.click()

			findLabelInsideMonitoringModal({
				en: 'Resource question catalogue',
				de: 'Ressource Fragenkatalog'
			})

			// TODO differentiate close button of menu and modal
			findElementWithAriaLabel({
				en: 'Close',
				de: 'Schließen'
			}).last().click()

			// early warnings
			findEarlyWarningSignsQuestionCategoryButton()
				.click()

			findLabelInsideMonitoringModal({
				en: 'Early warning sign question catalogue',
				de: 'Frühwarnzeichen Fragenkatalog'
			})

			// TODO differentiate close button of menu and modal
			findElementWithAriaLabel({
				en: 'Close',
				de: 'Schließen'
			}).last().click()

			// side effects
			findSideEffectsQuestionCategoryButton()
				.click()

			findLabelInsideMonitoringModal({
				en: 'Side effects question catalogue',
				de: 'Nebenwirkung Fragenkatalog'
			})

			// TODO differentiate close button of menu and modal
			findElementWithAriaLabel({
				en: 'Close',
				de: 'Schließen'
			}).last().click()

			// withdrawal
			findWithdrawalSymptomsQuestionCategoryButton()
				.click()

			findLabelInsideMonitoringModal({
				en: 'Withdrawal symptom question catalogue',
				de: 'Absetzsymptom Fragenkatalog'
			})

			// TODO differentiate close button of menu and modal
			findElementWithAriaLabel({
				en: 'Close',
				de: 'Schließen'
			}).last().click()

		})

		it('should be possible to filter based on the categories (B_017.1)', () => {

			findSymptomQuestionCategoryButton()
				.click()

			findElementWithAriaLabel({
				en: 'Filter by category',
				de: 'Durch Kategorie filtern'
			}).click()

			findElementWithLabel({
				en: 'Anxiety',
				de: 'Angststörung'
			}).click()


			// count checkboxes
			// TODO find better way of identifying the questions
			cy.get('input[type="checkbox"]').should('have.length.lte', 2)

		})

		it('should be possible to filter based on the filter text (B_017.2)', () => {

			findSymptomQuestionCategoryButton()
				.click()

			const freeTextSearch : Chainable<JQuery> = findElementWithAriaLabel({
				en: 'Free text search',
				de: 'Freitextsuche'
			})

			typeTranslatedText({
				en: 'feel down',
				de: 'niedergeschlagen'
			}, freeTextSearch)

			// count checkboxes
			// TODO find better way of identifying the questions
			cy.get('input[type="checkbox"]').should('have.length.lte', 2)

		})

		it('should be possible to select questions (B_017.3)', function () {

			findSymptomQuestionCategoryButton()
				.click()

			findElementWithAriaLabel({
				en: 'Filter by category',
				de: 'Durch Kategorie filtern'
			}).click()

			findElementWithLabel({
				en: 'Anxiety',
				de: 'Angststörung'
			}).click()

			cy.get('input[type="checkbox"]').first().click()

			// TODO find better way to identify the submit button (issue created)
			cy.get('rcc-submit-button').last().click()

			findElementWithLabel({
				en: 'Did you feel like your heart was beating irregularly or stumbling today?',
				de: 'Haben Sie heute das Gefühl, dass Ihr Herz unregelmäßig schlägt oder stolpert?'
			}).should('exist')

		})
	})

	describe('No template selected (B_017.4)', () => {

		it('should be possible to open all question modals', function () {

			// symptoms
			findSymptomQuestionCategoryButton()
				.click()

			findLabelInsideMonitoringModal({
				en: 'Symptom question catalogue',
				de: 'Symptom Fragenkatalog'
			})

			// TODO differentiate close button of menu and modal
			findElementWithAriaLabel({
				en: 'Close',
				de: 'Schließen'
			}).last().click()

			// resources
			findResourceQuestionCategoryButton()
				.click()

			findLabelInsideMonitoringModal({
				en: 'Resource question catalogue',
				de: 'Ressource Fragenkatalog'
			})

			// TODO differentiate close button of menu and modal
			findElementWithAriaLabel({
				en: 'Close',
				de: 'Schließen'
			}).last().click()

			// early warnings
			findEarlyWarningSignsQuestionCategoryButton()
				.click()

			findLabelInsideMonitoringModal({
				en: 'Early warning sign question catalogue',
				de: 'Frühwarnzeichen Fragenkatalog'
			})

			// TODO differentiate close button of menu and modal
			findElementWithAriaLabel({
				en: 'Close',
				de: 'Schließen'
			}).last().click()

			// side effects
			findSideEffectsQuestionCategoryButton()
				.click()

			findLabelInsideMonitoringModal({
				en: 'Side effects question catalogue',
				de: 'Nebenwirkung Fragenkatalog'
			})

			// TODO differentiate close button of menu and modal
			findElementWithAriaLabel({
				en: 'Close',
				de: 'Schließen'
			}).last().click()

			// withdrawal
			findWithdrawalSymptomsQuestionCategoryButton()
				.click()

			findLabelInsideMonitoringModal({
				en: 'Withdrawal symptom question catalogue',
				de: 'Absetzsymptom Fragenkatalog'
			})

			// TODO differentiate close button of menu and modal
			findElementWithAriaLabel({
				en: 'Close',
				de: 'Schließen'
			}).last().click()

		})

		it('should be possible to filter based on the categories (B_017.4)', () => {

			findSymptomQuestionCategoryButton()
				.click()

			findElementWithAriaLabel({
				en: 'Filter by category',
				de: 'Durch Kategorie filtern'
			}).click()

			findElementWithLabel({
				en: 'Anxiety',
				de: 'Angststörung'
			}).click()


			// count checkboxes
			// TODO find better way of identifying the questions
			cy.get('input[type="checkbox"]').should('have.length.lte', 2)

		})

		it('should be possible to filter based on the filter text (B_017.5)', () => {

			findSymptomQuestionCategoryButton()
				.click()

			const freeTextSearch : Chainable<JQuery> = findElementWithAriaLabel({
				en: 'Free text search',
				de: 'Freitextsuche'
			})

			typeTranslatedText({
				en: 'feel down',
				de: 'niedergeschlagen'
			}, freeTextSearch)

			// count checkboxes
			// TODO find better way of identifying the questions
			cy.get('input[type="checkbox"]').should('have.length.lte', 2)

		})

		it('should be possible to select questions (B_017.3)', function () {

			findSymptomQuestionCategoryButton()
				.click()

			findElementWithAriaLabel({
				en: 'Filter by category',
				de: 'Durch Kategorie filtern'
			}).click()

			findElementWithLabel({
				en: 'Anxiety',
				de: 'Angststörung'
			}).click()

			cy.get('input[type="checkbox"]').first().click()

			// TODO find better way to identify the submit button (issue created)
			cy.get('rcc-submit-button').last().click()

			findElementWithLabel({
				en: 'Did you feel like your heart was beating irregularly or stumbling today?',
				de: 'Haben Sie heute das Gefühl, dass Ihr Herz unregelmäßig schlägt oder stolpert?'
			}).should('exist')

		})

	})


})
