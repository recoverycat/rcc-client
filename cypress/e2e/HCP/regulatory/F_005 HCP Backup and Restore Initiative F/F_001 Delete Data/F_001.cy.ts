import { enterHCPRoot } from '../../../../utils/hcp-utils'
import { clickMainMenuButton, deleteData, findElementWithLabel, loadBackupFile, returnToHomePage } from '../../../../utils/e2e-utils'

describe('F_001 Delete Data', () => {

	beforeEach(() => {
		enterHCPRoot()

		deleteData()

		loadBackupFile('cypress/fixtures/test-patient-data.json')

		returnToHomePage()

		clickMainMenuButton()

	})

	it('should show the delete all data button in the menu (F_001.1)', () => {


		findElementWithLabel({
			en: 'Delete all data',
			de: 'Daten löschen'
		}).scrollIntoView().should('be.visible')

	})

	it('should be possible to delete all data', function () {

		findElementWithLabel({
			en: 'Delete all data',
			de: 'Daten löschen'
		}).scrollIntoView().click()

		findElementWithLabel({
			en: 'confirm',
			de: 'Bestätigen'
		}).click()

		// TODO check that the data is deleted
	})

})
