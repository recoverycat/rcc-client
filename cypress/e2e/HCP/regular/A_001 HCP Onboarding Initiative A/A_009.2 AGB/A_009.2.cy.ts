import { assertHCPTitle, enterHCPRoot } from '../../../../utils/hcp-utils'
import { assertHomePage, clickButtonWithLabel, clickHomeBrandButton, clickMainMenuButton } from '../../../../utils/e2e-utils'


describe('A_009.2 HCP AGB', () => {
	beforeEach(() => {
		enterHCPRoot()
	})

	describe('Navigate to AGB', () => {

		beforeEach(() => {
			clickMainMenuButton()

			clickButtonWithLabel({
				de: 'AGB',
				en: 'AGB',
			})
		})

		it('Should open AGB', () => {
			cy.get('h1').should('contain.text', 'Allgemeine Geschäftsbedingungen')
			assertHCPTitle({ en: 'AGB', de: 'AGB' })
		})

		it('Should return to home page', () => {
			clickHomeBrandButton()
			assertHomePage()
		})
	})
})
