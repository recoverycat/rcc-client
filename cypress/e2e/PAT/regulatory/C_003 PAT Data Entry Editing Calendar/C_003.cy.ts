import {
	deleteData,
	findElementWithLabel,
	loadBackupFile,
	translateName,
} from '../../../utils/e2e-utils'
import {
	assertAnswerSaved,
	dontAcceptPushNotifications,
	enterPATRoot,
	openDailyQuestions
} from '../../../utils/pat-utils'
import { C_004_2, C_004_3, C_004_4, C_004_5, testReturnToHomeScreen } from '../C_004 PAT Answer Questions/C_004-test-behavior-shared'

describe('C_003 PAT Data Entry Editing/Calendar', () => {

	beforeEach(() => {
		enterPATRoot()

		dontAcceptPushNotifications()

		deleteData()

		// After deleteData we need a bit of time
		dontAcceptPushNotifications(5000)

		loadBackupFile('cypress/fixtures/test-patient-data.json')

		openDailyQuestions()
	})

	describe('Calendar date selection', () => {
        it('should render the date picker as a button with an accessible name', () => {
            const selectedDate : Date = new Date()
            const todayString : string = selectedDate.toLocaleDateString('de-DE', { day:'2-digit', month:'2-digit', year:'numeric' })
            cy.findByTestId('rcc-date-picker').findByRole('button', translateName({
                de: `Heute, ${todayString}`,
                en: `Today, ${todayString}`,
            })).should('be.visible')
        })

		it('should show the selected date', () => {
			const selectedDate : Date = new Date()
			const todayString: string = selectedDate.toLocaleDateString('de-DE', { day: '2-digit', month:'2-digit', year:'numeric' })
			cy.findByTestId('rcc-date-picker').contains(todayString)

			// Select previous day
			cy.findByTestId('rcc-date-picker').click()
			cy.findAllByTestId('rcc-date-picker-day').not(':disabled').eq(-2).click()

			selectedDate.setDate(selectedDate.getDate()-1)
			const yesterdayString: string = selectedDate.toLocaleDateString('de-DE', { day: '2-digit', month:'2-digit', year:'numeric' })
			cy.findByTestId('rcc-date-picker').contains(yesterdayString)
		})

		it('should stay on the selected date when clicking outside the date picker', () => {
			cy.findByTestId('rcc-date-picker').click()
			cy.findByTestId('rcc-date-picker-current-month').should('be.visible')

			cy.get('body').click(0,0)
			cy.findByTestId('rcc-date-picker-current-month').should('not.exist')
			cy.findByTestId('rcc-date-picker').within(() => {
				findElementWithLabel({
					en: 'Today',
					de: 'Heute'
				})
			})
		})

		it('should be possible to answer questions for a previous date', () => {
			cy.findByTestId('rcc-date-picker').click()
			cy.findByTestId('rcc-date-picker-current-month').should('be.visible')
			cy.findAllByTestId('rcc-date-picker-day').not(':disabled').as('days')

			// Select yesterday's date and answer a question
			cy.get('@days').eq(-2).click()

			cy.get('rcc-card').eq(0).within(() => {
				findElementWithLabel({
					en: '[Mengen Frage]',
					de: 'Mengen Frage'
				}).then(() => {
					cy.findByRole('radio', translateName({
						en: '[etwas]',
						de: 'etwas'
					})).click()
				})
			})

			assertAnswerSaved()

			// Check that answers are reflected in the date picker
			cy.findByTestId('rcc-date-picker').click()
			cy.findByTestId('rcc-date-picker-current-month').should('be.visible')

			// 1 answered question for yesterday
			cy.get('@days').eq(-2).within(() => {
				cy.contains('1/6')
			})
			// 0 answered questions for today
			cy.get('@days').last().within(() => {
				cy.contains('0/6')
			})
		})

		it('should not be possible to answer questions for dates in the future', () => {
			const selectedDate : Date = new Date()
			selectedDate.setDate(selectedDate.getDate()+1)	// Set date to tomorrow
			const nextDay : string = selectedDate.toLocaleDateString('de-DE', { day:'2-digit' })

			cy.findByTestId('rcc-date-picker').as('date-picker').click()
			cy.get('@date-picker').contains(nextDay).should('be.disabled')
		})
	})

	describe('Repeat C_004 tests for a past date', () => {
		// Select yesterdays date
		beforeEach(() => {
			cy.findByTestId('rcc-date-picker').click()
			cy.findAllByTestId('rcc-date-picker-day').not(':disabled').eq(-2).click()
		})

		C_004_2()

		C_004_3()

		C_004_4()

		C_004_5()

		testReturnToHomeScreen()
	})
})
