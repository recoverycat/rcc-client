import { assertHomePage, findElementWithLabel, translateString } from '../../../../utils/e2e-utils'
import { assertPATTitle, dontAcceptPushNotifications } from '../../../../utils/pat-utils'

describe('B_020 General Questions', () => {

	beforeEach(() => {
		cy.intercept('GET', '/local/config.json', {
			statusCode: 200,
			body: {
				matomo: {
					'site-id': '2'
				},
				client_logo: {
					src: '/local/Alexianer_Logo.svg',
					altText: 'Alexianer Logo'
				},
				preConfiguredSymptomChecks: {
					starter_symptom_check: {
						meta: {
							label: 'Starter Symptom Check',
							defaultSchedule: [
								[],
								[
									'morning',
									'evening'
								]
							],
							creationDate: '2024-08-01'
						},
						questions: [
							{
								id: 'rcc-curated-starter-questions-0001-daily-0',
								category: 'symptoms'
							},
							{
								id: 'rcc-curated-starter-questions-0002-daily-0',
								category: 'symptoms'
							},
							{
								id: 'rcc-curated-starter-questions-0003-daily-1',
								category: 'resources'
							},
							{
								id: 'rcc-curated-default-00-daily-note',
								category: 'daily_notes'
							}
						]
					}
				}
			},
		})
        // Test link containing questions
        cy.visit(Cypress.config('baseUrl') + 'data?d=ewogICJwcmVDb25maWd1cmVkU3ltcHRvbUNoZWNrIjogInN0YXJ0ZXJfc3ltcHRvbV9jaGVjayIKfQ')

		dontAcceptPushNotifications()
	})

	it('should load the QR scan success modal (B_003.2)', () => {
        cy.get('rcc-data-arrived-modal').as('modal')

		cy.get('@modal').within(() => {
            findElementWithLabel({
				en: 'Scan QR Code',
				de: 'QR-Code scannen'
			}).should('be.visible')
        })

        cy.get('@modal').within(() => {
            findElementWithLabel({
                en: 'Open my questions',
                de: 'Meine Fragen öffnen'
            }).should('be.visible')
        })
	})

    it('should return to the home screen on cancel (B_003.2)', () => {
        cy.get('rcc-data-arrived-modal').as('modal')

		cy.get('@modal').within(() => {
            findElementWithLabel({
				en: 'Cancel',
				de: 'Abbrechen'
			}).click()
        })

        assertHomePage()
	})

    describe('Testing calendar file download (B_026)', () => {

        beforeEach(() => {
            cy.get('rcc-data-arrived-modal').within(() => {
                findElementWithLabel({
                    en: 'Open my questions',
                    de: 'Meine Fragen öffnen'
                }).click()
            })
        })

        it('should open the download prompt alert with 2 buttons', () => {
            cy.get('ion-alert').within(() => {
                findElementWithLabel({
                    en: 'Reminder',
                    de: 'Erinnerung'
                }).should('be.visible')

                findElementWithLabel({
                    en: 'No, thanks',
                    de: 'Nein, Danke.'
                }).should('be.visible')

                findElementWithLabel({
                    en: 'Download ICS Calendar file',
                    de: 'ICS Kalenderdaten herunterladen'
                }).should('be.visible')
            })
        })

        it('should close the alert on button click', () => {
            findElementWithLabel({
                en: 'No, thanks',
                de: 'Nein, Danke.'
            }).click()

            cy.get('ion-alert').should('not.exist')
        })

        it('should download the file and close the alert on button click', () => {
            findElementWithLabel({
                en: 'Download ICS Calendar file',
                de: 'ICS Kalenderdaten herunterladen'
            }).click()

            cy.readFile(`${Cypress.config('downloadsFolder')}/RecoveryCat.ics`)

            cy.get('ion-alert').should('not.exist')
        })

    })

    describe('Testing general questions from static QR code', () => {

        beforeEach(() => {
            cy.get('rcc-data-arrived-modal').within(() => {
                findElementWithLabel({
                    en: 'Open my questions',
                    de: 'Meine Fragen öffnen'
                }).click()
            })

            // Dismiss calendar file download alert
            cy.get('ion-alert').within(() => {
                findElementWithLabel({
                    en: 'No, thanks',
                    de: 'Nein, Danke.'
                }).click()
            })
        })

        it('should be able to open the query run', () => {
            assertPATTitle({ en: 'Today\'s Questions', de: 'Fragen beantworten' })
        })

        it('should display the correct questions in the query run', () => {
            cy.get('rcc-slide').as('slides')
            // Expect length 5: 3 questions + daily notes + all done slide
            cy.get('@slides').should('have.length', 5)

            cy.get('@slides').spread((s1, s2, s3, s4, s5) => {
                cy.wrap(s1).should('contain', translateString({
                    en: 'How is your mood today?',
                    de: 'Wie ist deine Stimmung heute?'
                }))

                cy.wrap(s2).should('contain', translateString({
                    en: 'How easy was it to do your daily routine today?',
                    de: 'Wie gut konntest du deinen Alltag heute bewältigen?'
                }))

                cy.wrap(s3).should('contain', translateString({
                    en: 'Did you do something for your well-being today?',
                    de: 'Hast du heute etwas Gutes für dich getan?'
                }))

                cy.wrap(s4).should('contain', translateString({
                    en: 'Notes',
                    de: 'Notizen'
                }))

                cy.wrap(s4).should('contain', translateString({
                    en: 'What else happened today?',
                    de: 'Was ist heute sonst noch passiert?'
                }))

                cy.wrap(s5).should('contain', translateString({
                    en: 'You answered 0 out of 3 today\'s questions.',
                    de: 'Du hast heute 0 von 3 Fragen beantwortet.'
                }))
            })
        })

        it('should be possible to answer the questions (C_004)', () => {
            cy.get('rcc-slide').as('slides')

            cy.get('@slides').eq(0).within(() => {
                cy.get('input').should('not.be.disabled')
            })

            cy.get('@slides').eq(1).within(() => {
                cy.get('input').should('not.be.disabled')
            })

            cy.get('@slides').eq(2).within(() => {
                cy.findAllByRole('radio').each(($radio) => {
                    cy.wrap($radio).should('not.be.disabled')
                })
            })
        })

        it('should be able to type in the daily notes question', () => {
            cy.get('rcc-slide').as('slides')

            cy.get('@slides').eq(3)
                .find('textarea')
                .clear()
                .type('My daily notes')

            cy.get('@slides').eq(3)
                .should('be.visible')
                .find('textarea')
                .should('have.value', 'My daily notes')
        })

    })

})
