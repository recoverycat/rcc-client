import {
	clickButtonWithLabel,
	clickMainMenuButton,
	findEmail,
	findTelephoneNumber,
	returnToHomePage,
	translateName
} from '../../../../utils/e2e-utils'
import { assertPATTitle, dontAcceptPushNotifications, enterPATRoot } from '../../../../utils/pat-utils'

describe('G_002 Help Page in the App', () => {

	beforeEach(() => {

		enterPATRoot()

		dontAcceptPushNotifications()

	})

	describe('Navigate to help page', () => {

		beforeEach(() => {

			clickMainMenuButton()

			clickButtonWithLabel({
				de: 'Hilfe & Benutzerhandbuch',
				en: 'Help & User manual',
			})

		})

		it('Should be possible to open the Help page (G_002.3)', () => {

			assertPATTitle({ en: 'Support & User Manual', de: 'Hilfe & Benutzerhandbuch' })
			cy.findByRole('heading', translateName({ de: 'Hilfe', en: 'Support' })).should('be.visible')

		})

		it('Should include an email address (G_002.4)', () => {

			findEmail().should('be.visible')

		})

		it('Should include a phone number (G_002.5)', () => {

			findTelephoneNumber().should('be.visible')

		})

		it('Should return to home page', () => {

			returnToHomePage()

		})
	})
})
