import {
	clickButtonWithLabel,
	clickHomeBrandButton,
	clickMainMenuButton,
	translateName,
} from '../../../../utils/e2e-utils'
import { assertPATTitle, dontAcceptPushNotifications, enterPATRoot } from '../../../../utils/pat-utils'

describe('G_001 Customer Support Entry & Form', () => {

	beforeEach(() => {

		enterPATRoot()

		dontAcceptPushNotifications()

	})

	describe('Test feedback form', () => {

		beforeEach(() => {

			clickMainMenuButton()

			clickButtonWithLabel({
				de: 'Hilfe & Benutzerhandbuch',
				en: 'Help & User manual',
			})

			clickButtonWithLabel({ de: 'Feedback senden', en: 'Send Feedback' })

		})

		it('Should be present and have the proper name (G_001.1)', () => {

			cy.findByRole('form', translateName({
				de: 'Kontaktformular',
				en: 'Contact form'
			})).should('be.visible')

		})

		it('Should have a Name text input field (G_001.2)', () => {

			cy.findByRole('textbox', translateName({ de: /Name/, en: /Name/ })).should('be.visible')

		})

		it('Should have an Email text input field (G_001.3)', () => {

			cy.findByRole('textbox', translateName({ de: /E-Mail/, en: /Email/ })).should('be.visible')

		})

		it('Should have a Subject text input field (G_001.4)', () => {

			cy.findByRole('textbox', translateName({ de: /Betreff/, en: /Subject/ })).should('be.visible')

		})

		it('Should have a Category drop down field (G_001.5)', () => {

			cy.findByRole('combobox', translateName({ de: /Worum geht es\?/, en: /What is it about\?/ })).should('be.visible')

		})

		it('Should spawn another drop down field if the category "Bug" is selected (G_001.6)', () => {

			cy.findByRole('combobox', translateName({ de: /Worum geht es\?/, en: /What is it about\?/ })).click()
			cy.findByRole('option', translateName({ de: /Bug\/Fehler/, en: /Bug\/Error/ })).click()
			cy.findByRole('combobox', translateName({ de: /Wo trat das Problem auf\?/, en: /Where did the problem occur\?/ }))

		})

		it('Should spawn a checked checkbox "Send debug info" if the category "Bug" is selected (G_001.15)', () => {

			cy.findByRole('combobox', translateName({ de: /Worum geht es\?/, en: /What is it about\?/ })).click()
			cy.findByRole('option', translateName({ de: /Bug\/Fehler/, en: /Bug\/Error/ })).click()
			cy.findByRole('checkbox', translateName({ de: /Debug-Informationen senden/, en: /Send debug info/ }))
				.should('be.checked')

		})

		it('Should have a Message text input field (G_001.7)', () => {

			cy.findAllByRole('textbox', translateName({ de: /Nachricht/, en: /Message/ })).should('be.visible')

		})

		it('Should stay visible if the user tries to send without required inputs and spawn an error message', () => {

			clickButtonWithLabel({ de: 'Abschicken', en: 'Send' })
			cy.findAllByText(translateName({ de: /[Bitte fülle dieses Feld aus.]/, en: /[This field is required.]/ }).name).should('be.visible')
			cy.findByRole('form', translateName({
				de: 'Kontaktformular',
				en: 'Contact form'
			})).should('be.visible')

		})

		it('Should return to the help page after submitting form', () => {

			cy.findByRole('textbox', translateName({ de: /E-Mail/, en: /Email/ })).type('test@example.com')
			cy.findByRole('combobox', translateName({ de: /Worum geht es\?/, en: /What is it about\?/ })).click()
			cy.findAllByRole('option').first().click()
			cy.findAllByRole('textbox', translateName({ de: /Nachricht/, en: /Message/ })).type('Example message')

			clickButtonWithLabel({ de: 'Abschicken', en: 'Send' })

			clickButtonWithLabel({ de: 'Okay', en: 'Okay' })

			cy.findByRole('form', translateName({
				de: 'Kontaktformular',
				en: 'Contact form'
			})).should('not.exist')

			assertPATTitle({ en: 'Support & User Manual', de: 'Hilfe & Benutzerhandbuch' })
			cy.findByRole('heading', translateName({ de: 'Hilfe', en: 'Support' })).should('be.visible')

		})

		it('Should return to home page', () => {

			clickHomeBrandButton()

		})
	})
})
