import { assertHomePage, deleteData, loadBackupFile } from '../../../utils/e2e-utils'
import { dontAcceptPushNotifications, enterPATRoot } from '../../../utils/pat-utils'

describe('C_001 PAT Home Screen', () => {

	beforeEach(() => {
		enterPATRoot()

		dontAcceptPushNotifications()

		deleteData()

		// After deleteData we need a bit of time
		dontAcceptPushNotifications(5000)

		loadBackupFile('cypress/fixtures/test-patient-data.json')

	})

	it('should load the home screen (C_001)', () => {
		assertHomePage()
	})


})
