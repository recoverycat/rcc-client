import {
	assertHomePage,
	deleteData,
	findElementWithLabel,
	loadBackupFile,
	translateName,
	scrollToElementWithWait,
} from '../../../utils/e2e-utils'
import {
	dontAcceptPushNotifications,
	enterPATRoot,
	findAnswerOptionValueForPAT,
	openDailyQuestions
} from '../../../utils/pat-utils'
import { C_004_2, C_004_3, C_004_4, C_004_5, testReturnToHomeScreen } from './C_004-test-behavior-shared'

describe('C_004 PAT Answer Questions', () => {

	beforeEach(() => {
		enterPATRoot()

		dontAcceptPushNotifications()

		deleteData()

		// After deleteData we need a bit of time
		dontAcceptPushNotifications(5000)

		loadBackupFile('cypress/fixtures/test-patient-data.json')

		openDailyQuestions()
	})

	describe('C_004.1 Find the `answer questions` button', () => {
		it('should find the answer questions button (C_004.1)', () => {
			// Return to home page first
			cy.get('rcc-logo').first().should('be.visible').click()
			assertHomePage()

			findElementWithLabel({
				en: 'Answer questions',
				de: 'Fragen beantworten'
			}).should('be.visible')
		})

		it('should have a page title (C_004.1)', () => {
			findElementWithLabel({
				en: 'Your daily Check-in',
				de: 'Dein täglicher Check-in'
			}).should('be.visible')
		})

		it('should display the correct widget for each answer type (C_004.1)', () => {
			cy.get('rcc-card').eq(0).within(() => {
				findElementWithLabel({
					en: '[Mengen Frage]',
					de: 'Mengen Frage'
				}).should('be.visible').then(() => {
					findAnswerOptionValueForPAT({
						en: '[stark]',
						de: 'stark'
					}, 0).should('exist')

					findAnswerOptionValueForPAT({
						en: '[mäßig]',
						de: 'mäßig'
					}, 1).should('exist')

					findAnswerOptionValueForPAT({
						en: '[etwas]',
						de: 'etwas'
					}, 2).should('exist')

					findAnswerOptionValueForPAT({
						en: '[nein]',
						de: 'nein'
					}, 3).should('exist')
				})
			})

			cy.get('rcc-card').eq(1).within((card) => {

				scrollToElementWithWait(card)

				findElementWithLabel({
					en: '[Ja/Nein Frage]',
					de: 'Ja/Nein Frage'
				}).should('be.visible').then(() => {
					findAnswerOptionValueForPAT({
						en: 'yes',
						de: 'ja'
					}, 0).should('exist')

					findAnswerOptionValueForPAT({
						en: 'no',
						de: 'nein'
					}, 1).should('exist')
				})
			})

			cy.get('rcc-card').eq(2).within((card) => {

				scrollToElementWithWait(card)

				findElementWithLabel({
					en: '[4-Choice-Frage]',
					de: '4-Choice-Frage'
				}).should('be.visible').then(() => {
					findAnswerOptionValueForPAT({
						en: '[A]',
						de: 'A'
					}, 0).should('exist')

					findAnswerOptionValueForPAT({
						en: '[B]',
						de: 'B'
					}, 1).should('exist')

					findAnswerOptionValueForPAT({
						en: '[C]',
						de: 'C'
					}, 2).should('exist')

					findAnswerOptionValueForPAT({
						en: '[D]',
						de: 'D'
					}, 3).should('exist')
				})
			})

			cy.get('rcc-slider').scrollTo(350, 0)

			cy.get('rcc-card').eq(3).within((card) => {

				scrollToElementWithWait(card)

				findElementWithLabel({
					en: '[Haben Sie Ihre Medikamente wie geplant eingenommen?] (Abilify, 1 Tabletten)',
					de: 'Haben Sie Ihre Medikamente wie geplant eingenommen? (Abilify, 1 Tabletten)'
				}).should('exist').then(() => {
					findAnswerOptionValueForPAT({
						en: 'yes',
						de: 'ja'
					}, 0).should('exist')

					findAnswerOptionValueForPAT({
						en: 'no',
						de: 'nein'
					}, 1).should('exist')
				})
			})

			cy.get('rcc-slider').scrollTo(350, 0)

			cy.get('rcc-card').eq(4).within((card) => {

				scrollToElementWithWait(card)

				findElementWithLabel({
					en: '[Slider-Frage]',
					de: 'Slider-Frage'
				}).should('exist').then(() => {
					cy.get('[role="radio"]').should('not.exist')
					cy.findByRole('slider', translateName({
						en: '[Slider-Frage]',
						de: 'Slider-Frage'
					})).should('exist')
				})
			})

			cy.get('rcc-slider').scrollTo(350, 0)

			cy.get('rcc-card').eq(5).within((card) => {

				scrollToElementWithWait(card)

				findElementWithLabel({
					en: '[Ziffern Frage]',
					de: 'Ziffern Frage'
				}).should('exist').then(() => {
					cy.get('[role="radio"]').should('not.exist')
					cy.get('[type="range"]').should('not.exist')
					cy.findByRole('spinbutton', translateName({
						en: '[Ziffern Frage]',
						de: 'Ziffern Frage'
					})).should('exist')
				})
			})

			cy.get('rcc-slider').scrollTo(350, 0)

			cy.get('rcc-card').eq(6).within((card) => {

				scrollToElementWithWait(card)

				findElementWithLabel({
					en: 'What else happened today?',
					de: 'Was ist heute sonst noch passiert?'
				}).should('exist').then(() => {
					cy.get('[role="radio"]').should('not.exist')
					cy.get('[type="range"]').should('not.exist')
					cy.get('[type="number"]').should('not.exist')
					cy.findByRole('textbox', translateName({
						en: 'What else happened today?',
						de: 'Was ist heute sonst noch passiert?'
					}))
				})
			})
		})
	})

	C_004_2()

	C_004_3()

	C_004_4()

	C_004_5()

	testReturnToHomeScreen()
})
