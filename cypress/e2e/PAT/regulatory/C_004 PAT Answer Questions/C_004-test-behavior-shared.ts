import { assertHomePage, assertPage, findElementWithLabel, scrollToElementWithWait, translateName, translateString } from '../../../utils/e2e-utils'
import {
	assertAllDoneSlide,
	assertAnswerChanged,
	assertAnswerDeleted,
	assertAnswerSaved,
	assertNotesDeleted,
	assertNotesSaved,
	clickNext,
	clickPrevious
} from '../../../utils/pat-utils'

export function C_004_2(): void {
	describe('C_004.2 Browse through the questions', () => {
		it('should be able to browse by scrolling (C_004.2)', () => {
			cy.get('rcc-slider').scrollTo(350, 0)
		})

		it('should be able to browse by using the navigation buttons (C_004.2)', () => {
			clickNext(0)
			clickNext(1)
			clickPrevious(2)
			clickNext(1)
		})

	})
}

export function C_004_3(): void {
	describe('C_004.3 Answer the different question types', () => {
		it('should be able to answer the different question types and show "Change answers" button when all are answered (C_004.3)', () => {
			cy.get('rcc-card').eq(0).within(() => {
				findElementWithLabel({
					en: '[Mengen Frage]',
					de: 'Mengen Frage'
				}).then(() => {
					cy.findByRole('radio', translateName({
						en: '[stark]',
						de: 'stark'
					})).click()
				})
			})

			assertAnswerSaved()

			cy.get('rcc-card').eq(1).within((card) => {

				scrollToElementWithWait(card)

				findElementWithLabel({
					en: '[Ja/Nein Frage]',
					de: 'Ja/Nein Frage'
				}).then(() => {
					cy.findByRole('radio', translateName({
						en: 'no',
						de: 'nein'
					})).click()
				})
			})

			assertAnswerSaved()

			cy.get('rcc-card').eq(2).within((card) => {

				scrollToElementWithWait(card)

				findElementWithLabel({
					en: '[4-Choice-Frage]',
					de: '4-Choice-Frage'
				}).then(() => {
					cy.findByRole('radio', translateName({
						en: '[C]',
						de: 'C'
					})).click()
				})
			})


			assertAnswerSaved()

			cy.get('rcc-slider').scrollTo(350, 0)

			cy.get('rcc-card').eq(3).within((card) => {

				scrollToElementWithWait(card)

				findElementWithLabel({
					en: '[Haben Sie Ihre Medikamente wie geplant eingenommen?] (Abilify, 1 Tabletten)',
					de: 'Haben Sie Ihre Medikamente wie geplant eingenommen? (Abilify, 1 Tabletten)'
				}).then(() => {
					cy.findByRole('radio', translateName({
						en: 'yes',
						de: 'ja'
					})).click()
				})
			})

			assertAnswerSaved()

			cy.get('rcc-slider').scrollTo(350, 0)

			cy.get('rcc-card').eq(4).within((card) => {

				scrollToElementWithWait(card)

				findElementWithLabel({
					en: '[Slider-Frage]',
					de: 'Slider-Frage'
				}).then(() => {
					cy.findByRole('slider', translateName({
						en: '[Slider-Frage]',
						de: 'Slider-Frage'
					})).as('slider')

					cy.get('@slider').invoke('val', 55).trigger('input')

					cy.wait(100)

					findElementWithLabel({
						en: '55',
						de: '55'
					}).should('exist')

					cy.get('@slider').should('have.value', '55')
				})
			})

			assertAnswerSaved()

			cy.get('rcc-slider').scrollTo(350, 0)

			cy.get('rcc-card').eq(5).within((card) => {

				scrollToElementWithWait(card)

				findElementWithLabel({
					en: '[Ziffern Frage]',
					de: 'Ziffern Frage'
				}).then(() => {
					cy.findByRole('spinbutton', translateName({
						en: '[Ziffern Frage]',
						de: 'Ziffern Frage'
					})).type('10')
				})
			})

			assertAnswerChanged()

			cy.get('rcc-slider').scrollTo(350, 0)

			cy.get('rcc-card').eq(6).within((card) => {

				scrollToElementWithWait(card)

				findElementWithLabel({
					en: 'What else happened today?',
					de: 'Was ist heute sonst noch passiert?'
				}).then(() => {
					cy.findByRole('textbox', translateName({
						en: 'What else happened today?',
						de: 'Was ist heute sonst noch passiert?'
					})).type('This is a test note')
				})
			})

			assertNotesSaved()

			// Find "Change answers" button (should go to first question)
			cy.get('rcc-card').eq(7).within(() => {
				cy.findByRole('button', translateName({
					en: 'Change answers',
					de: 'Antworten ändern'
				})).should('be.visible').click()
			})

			cy.get('rcc-card').eq(0).should('be.visible')
		})
	})
}

export function C_004_4(): void {
	describe('C_004.4 Reset a question', () => {
		it('should be possible to reset a all question types (C_004.4)', () => {
			cy.get('rcc-card').eq(0).within(() => {
				findElementWithLabel({
					en: '[Mengen Frage]',
					de: 'Mengen Frage'
				}).then(() => {
					findElementWithLabel({
						en: '[stark]',
						de: 'stark'
					}).click()
				})

				cy.wait(500)

				findElementWithLabel({
					en: '[Mengen Frage]',
					de: 'Mengen Frage'
				}).then(() => {
					findElementWithLabel({
						en: '[stark]',
						de: 'stark'
					}).click()
				})

			})

			assertAnswerDeleted()

			cy.get('rcc-card').eq(1).within(() => {
				findElementWithLabel({
					en: '[Ja/Nein Frage]',
					de: 'Ja/Nein Frage'
				}).then(() => {
					findElementWithLabel({
						en: '[ja]',
						de: 'ja'
					}).click()
				})

				cy.wait(500)

				findElementWithLabel({
					en: '[Ja/Nein Frage]',
					de: 'Ja/Nein Frage'
				}).then(() => {
					findElementWithLabel({
						en: '[ja]',
						de: 'ja'
					}).click()
				})

			})

			assertAnswerDeleted()


			cy.get('rcc-card').eq(2).within(() => {
				findElementWithLabel({
					en: '[4-Choice-Frage]',
					de: '4-Choice-Frage'
				}).then(() => {
					findElementWithLabel({
						en: '[B]',
						de: 'B'
					}).click()
				})
			})

			assertAnswerSaved()

			cy.get('rcc-card').eq(2).within(() => {
				findElementWithLabel({
					en: '[4-Choice-Frage]',
					de: '4-Choice-Frage'
				}).then(() => {
					findElementWithLabel({
						en: '[B]',
						de: 'B'
					}).click()
				})

			})

			assertAnswerDeleted()


			cy.get('rcc-slider').scrollTo(350, 0)

			cy.get('rcc-card').eq(3).within((card) => {

				scrollToElementWithWait(card)

				findElementWithLabel({
					en: '[Haben Sie Ihre Medikamente wie geplant eingenommen?] (Abilify, 1 Tabletten)',
					de: 'Haben Sie Ihre Medikamente wie geplant eingenommen? (Abilify, 1 Tabletten)'
				}).then(() => {
					cy.findByRole('radio', translateName({
						en: 'yes',
						de: 'ja'
					})).click()

					cy.wait(500)

					cy.findByRole('radio', translateName({
						en: 'yes',
						de: 'ja'
					})).click()
				})
			})

			assertAnswerDeleted()


			cy.get('rcc-slider').scrollTo(350, 0)

			cy.get('rcc-card').eq(4).within((card) => {
				scrollToElementWithWait(card)

				findElementWithLabel({
					en: '[Slider-Frage]',
					de: 'Slider-Frage'
				}).then(() => {
					cy.findByRole('slider', translateName({
						en: '[Slider-Frage]',
						de: 'Slider-Frage'
					})).as('slider')

					cy.get('@slider').invoke('val', 55).trigger('input')

					cy.wait(100)

					findElementWithLabel({
						en: '55',
						de: '55'
					}).should('exist')

					cy.wait(400)

					// We have to disable pointer events for this element so that the native
					// slider can work as expected. Instead, we check the event occurred within
					// the SVG via the mousedown event on the input itself
					cy.get('input').eq(0).click()
				})
			})

			assertAnswerDeleted()


			cy.get('rcc-slider').scrollTo(350, 0)

			cy.get('rcc-card').eq(5).within((card) => {

				scrollToElementWithWait(card)

				findElementWithLabel({
					en: '[Ziffern Frage]',
					de: 'Ziffern Frage'
				}).then(() => {
					cy.findByRole('spinbutton', translateName({
						en: '[Ziffern Frage]',
						de: 'Ziffern Frage'
					})).type('10')
				})
			})

			assertAnswerChanged()

			cy.get('rcc-card').eq(5).within((card) => {

				scrollToElementWithWait(card)

				findElementWithLabel({
					en: '[Ziffern Frage]',
					de: 'Ziffern Frage'
				}).then(() => {
					cy.findByRole('spinbutton', translateName({
						en: '[Ziffern Frage]',
						de: 'Ziffern Frage'
					})).wait(500).type('{backspace}'.repeat(2))
				})
			})

			cy.wait(300)

			assertAnswerDeleted()


			cy.get('rcc-slider').scrollTo(350, 0)

			cy.get('rcc-card').eq(6).within((card) => {

				scrollToElementWithWait(card)

				findElementWithLabel({
					en: 'What else happened today?',
					de: 'Was ist heute sonst noch passiert?'
				}).then(() => {
					cy.findByRole('textbox', translateName({
						en: 'What else happened today?',
						de: 'Was ist heute sonst noch passiert?'
					})).type('This is a test note').wait(500)
				})
			})

			assertNotesSaved()

			cy.get('rcc-card').eq(6).within((card) => {

				scrollToElementWithWait(card)

				findElementWithLabel({
					en: 'What else happened today?',
					de: 'Was ist heute sonst noch passiert?'
				}).then(() => {

					cy.findByRole('textbox', translateName({
						en: 'What else happened today?',
						de: 'Was ist heute sonst noch passiert?'
					})).type('{backspace}'.repeat(19))

				})
			})

			assertNotesDeleted()
			assertAllDoneSlide()


		})

	})
}

export function C_004_5(): void {
	describe('C_004.5 All done slide', () => {
		it('should visit my charts page when clicking "View Data"', () => {
			cy.wait(1000)

			cy.get('rcc-card').eq(7).within((card) => {

				scrollToElementWithWait(card)

				cy.findByText(translateString({
					en: 'View Data',
					de: 'Daten ansehen'
				})).should('be.visible').click()
			})

			assertPage('my-charts-page')
		})

		it('should return to the home screen when clicking "Home"', () => {
			cy.wait(1000)

			cy.get('rcc-slide').eq(7).within((slide) => {

				scrollToElementWithWait(slide)

				cy.findByText(translateString({
					en: 'Home',
					de: 'Startseite'
				})).should('be.visible').click()
			})

			assertHomePage()
		})

		it('should go to the first unanswered question when clicking "Answer Questions"', () => {
			cy.wait(1000)

			cy.get('rcc-card').eq(7).within((card) => {

				scrollToElementWithWait(card)

				cy.findByRole('button', translateName({
					en: 'Answer Questions',
					de: 'Fragen beantworten'
				})).should('be.visible').click()
			})

			// No questions answered => should go to first question
			cy.get('rcc-card').eq(0).should('be.visible')

			cy.get('rcc-card').eq(0).within(() => {
				findElementWithLabel({
					en: '[Mengen Frage]',
					de: 'Mengen Frage'
				}).then(() => {
					cy.findByRole('radio', translateName({
						en: '[stark]',
						de: 'stark'
					})).click()
				})
			})

			cy.get('rcc-card').eq(7).within((card) => {

				scrollToElementWithWait(card)

				cy.findByRole('button', translateName({
					en: 'Answer Questions',
					de: 'Fragen beantworten'
				})).should('be.visible').click()
			})

			// First question answered => should go to second (unanswered) question
			cy.get('rcc-card').eq(0).should('not.be.visible')
			cy.get('rcc-card').eq(1).should('be.visible').as('activeCard')

			// Selected card should have navigation buttons
			cy.get('@activeCard').within(() => {
				cy.findByRole('button', translateName({
					en: 'Next question',
					de: 'Nächste Frage'
				})).should('be.visible')
			})

		})
	})
}

export function testReturnToHomeScreen(): void {
	it('should return to the home screen when clicking the Recovery Cat logo', () => {
		cy.get('rcc-logo').first().scrollIntoView().should('be.visible').click()

		assertHomePage()
	})
}
