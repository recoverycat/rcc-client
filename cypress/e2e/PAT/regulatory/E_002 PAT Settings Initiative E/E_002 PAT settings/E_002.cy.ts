import {
	assertReceiveTransmissionType,
	assertSendTransmissionType,
	clickMainMenuButton, findElementWithAriaLabel, findElementWithExactLabel,
	findElementWithLabel,
	resetLanguage,
	resetTransmissionTypes,
} from '../../../../utils/e2e-utils'
import {
	dontAcceptPushNotifications,
	enterPATRoot,
	findReceiveDataTransmissionPullDown,
	findSendDataTransmissionPullDown
} from '../../../../utils/pat-utils'
import { assertHCPTitle, findBackButton, findLanguagePullDown } from '../../../../utils/hcp-utils'

describe.skip('E_002 PAT Settings', () => {

	beforeEach(() => {

		enterPATRoot()

		cy.wait(1000)

		resetLanguage('de')
		Cypress.env('language', 'de')

		resetTransmissionTypes()

		dontAcceptPushNotifications()

		clickMainMenuButton()
	})

	it('should be possible to open the settings menu (E_002.1)', () => {

		findElementWithLabel({
			en: 'Settings',
			de: 'Einstellungen'
		}).click()

		assertHCPTitle({
			en: 'Settings',
			de: 'Einstellungen'
		})

	})

	it('should be possible to open the language menu (E_002.2)', () => {

		findElementWithLabel({
			en: 'Settings',
			de: 'Einstellungen'
		}).click()

		findElementWithLabel({
			en: 'Language',
			de: 'Sprache'
		}).click()

		findElementWithLabel({
			en: 'Language settings',
			de: 'Spracheinstellungen'
		})

	})

	it('should be possible to change the language using the dropdown (E_002.3)', () => {

		findElementWithLabel({
			en: 'Settings',
			de: 'Einstellungen'
		}).click()

		findElementWithLabel({
			en: 'Language',
			de: 'Sprache'
		}).click()

		findLanguagePullDown()
			.click()

		findElementWithLabel({
			en: 'English',
			de: 'Englisch'
		}).click()

		Cypress.env('language', 'en')

		findElementWithLabel({
			en: 'Language setting',
			de: 'Spracheinstellungen',
		})

		findBackButton().last()
			.click()

		findBackButton().last()
			.click()

		findElementWithLabel({
			en: 'Welcome to Recovery Cat, you are using version',
			de: 'Willkommen bei Recovery Cat, Du nutzt Version'
		})
	})

	it('should be possible to open the data transmission menu (E_002.4)', () => {

		findElementWithLabel({
			en: 'Settings',
			de: 'Einstellungen'
		}).click()

		findElementWithLabel({
			en: 'Data transmission',
			de: 'Datenübertragung'
		}).click()

		findElementWithLabel({
			en: 'Send transmissions',
			de: 'Datenversand'
		}).scrollIntoView().should('be.visible')

		findElementWithLabel({
			en: 'Receive transmissions',
			de: 'Empfangsoptionen'
		}).scrollIntoView().should('be.visible')

	})

	it('should be possible to choose the data transmission options (E_001.5 and E_001.6)', () => {

		assertSendTransmissionType('COMBINED_TRANSMISSION_SERVICE').should('be.true')
		assertReceiveTransmissionType('COMBINED_TRANSMISSION_SERVICE').should('be.true')

		findElementWithLabel({
			en: 'Settings',
			de: 'Einstellungen'
		}).click()

		findElementWithLabel({
			en: 'Data transmission',
			de: 'Datenübertragung'
		}).click()

		findSendDataTransmissionPullDown().click().wait(5)

		findSendDataTransmissionPullDown().within(() => {

			findElementWithExactLabel({
				en: 'WebRTC',
				de: 'WebRTC'
			}).first().click()

		})

		findReceiveDataTransmissionPullDown().click().wait(5)

		findReceiveDataTransmissionPullDown().within(() => {

			findElementWithExactLabel({
				en: 'WebRTC',
				de: 'WebRTC'
			}).click()

		})

		assertSendTransmissionType('RTC_TRANSMISSION_SERVICE').should('be.true')
		assertReceiveTransmissionType('RTC_TRANSMISSION_SERVICE').should('be.true')

	})

	it('should be possible to open the info modals (E_001.7)', () => {

		findElementWithLabel({
			en: 'Settings',
			de: 'Einstellungen'
		}).click()

		findElementWithLabel({
			en: 'Data transmission',
			de: 'Datenübertragung'
		}).click()

		findElementWithAriaLabel({
			en: 'Information',
			de: 'Information'
		}).eq(0).click()

		findElementWithLabel({
			en: 'Configuration of data transfer',
			de: 'Konfiguration der Datenübertragung'
		})

		findElementWithLabel({
			en: 'Okay',
			de: 'Okay'
		}).click()

		findElementWithLabel({
			en: 'Configuration of data transfer',
			de: 'Konfiguration der Datenübertragung'
		}).should('not.exist')

		findElementWithAriaLabel({
			en: 'Information',
			de: 'Information'
		}).eq(1).click()

		findElementWithLabel({
			en: 'Configuration of data transfer',
			de: 'Konfiguration der Datenübertragung'
		})

		findElementWithLabel({
			en: 'Okay',
			de: 'Okay'
		}).click()

		findElementWithLabel({
			en: 'Configuration of data transfer',
			de: 'Konfiguration der Datenübertragung'
		}).should('not.exist')
	})
})
