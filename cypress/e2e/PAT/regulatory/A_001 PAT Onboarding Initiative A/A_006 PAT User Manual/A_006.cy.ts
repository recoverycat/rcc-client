import { assertPATTitle, enterPATRoot, dontAcceptPushNotifications } from '../../../../utils/pat-utils'
import {
	clickButtonWithLabel,
	clickLinkInSameTab,
	clickMainMenuButton,
	translateName
} from '../../../../utils/e2e-utils'
import { ByRoleOptions } from '@testing-library/cypress'




describe('A_006 PAT User Manual Initiative A', () => {

	beforeEach(() => {

		enterPATRoot()

		dontAcceptPushNotifications()

	})

	describe('Navigate to help page', () => {

		beforeEach(() => {

			clickMainMenuButton()

			clickButtonWithLabel({
				de: 'Hilfe & Benutzerhandbuch',
				en: 'Help & User manual',
			})

		})

		it('should be possible to open the Help page (G_002.1)', () => {

			assertPATTitle({ en: 'Support & User Manual', de: 'Hilfe & Benutzerhandbuch' })

		})

		it('should have PDF link that opens in a new tab (A_006.1)', () => {

			const translation : ByRoleOptions = translateName({ de: 'Link zur PDF Version', en: 'Link to PDF version' })
			cy.findByRole('link', translation)
				.scrollIntoView()
				.should('be.visible')
				.should('have.attr', 'target', '_blank')

		})

		it('should be possible to download the user manual (A_006.1)', () => {

			clickLinkInSameTab({
				de: 'Link zur PDF Version',
				en: 'Link to PDF version',
			})

			cy.url().should('contain', '.pdf')

		})

	})
})
