import { ByRoleOptions } from '@testing-library/cypress'
import { assertHomePage, clickButtonWithLabel, clickHomeBrandButton, clickMainMenuButton, clickScrollToTopButton, findEmail, findTelephoneNumber, translateName } from '../../../../utils/e2e-utils'
import { assertPATTitle, enterPATRoot, dontAcceptPushNotifications } from '../../../../utils/pat-utils'

describe('A_015 Privacy policy Initiative A', () => {
	beforeEach(() => {
		enterPATRoot()

		dontAcceptPushNotifications()

		clickMainMenuButton()

		clickButtonWithLabel({
			de: 'Datenschutzerklärung',
			en: 'Privacy Policy',
		})
	})

	it('should be possible to open the Privacy Policy page (A_015.1)', () => {
		assertPATTitle({ en: 'Privacy Policy', de: 'Datenschutzerklärung' })
		const translation : ByRoleOptions = translateName({ de: /Datenschutzerklärung/, en: /Datenschutzerklärung/ })
		cy.findByRole('heading', { level: 1, ...translation }).should('be.visible')
	})

	it('Should include Recovery Cat contact info', () => {
		findEmail('hello@recovery.cat').should('be.visible')
		findTelephoneNumber().should('be.visible')
	})

	it('Should include a link to the imprint', () => {
		cy.findAllByRole('link', translateName({
			de: /Impressum/, en: /imprint/
		}))
		.should('be.visible')
		.and('have.attr', 'href', '/imprint')
	})

	it('Should include the email of the data protection officer', () => {
		findEmail('datenschutz@recovery.cat').should('be.visible')
	})

	it('Should have a working scroll to top button', () => {
		cy.findAllByRole('heading').last().scrollIntoView()

		cy.findAllByRole('heading').first().should('not.be.visible')

		clickScrollToTopButton()

		cy.findAllByRole('heading').first().should('be.visible')
	})

	it('Should return to home page', () => {
		clickHomeBrandButton()
		assertHomePage()
	})
})
