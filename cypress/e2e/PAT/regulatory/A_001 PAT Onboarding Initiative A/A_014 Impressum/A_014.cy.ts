import { assertPATTitle, dontAcceptPushNotifications, enterPATRoot } from '../../../../utils/pat-utils'
import {
	clickButtonWithLabel,
	clickMainMenuButton,
	findEmail,
	findTelephoneNumber,
	returnToHomePage,
	translateName
} from '../../../../utils/e2e-utils'

describe('A_014 Impressum Initiative A', () => {

	beforeEach(() => {

		enterPATRoot()

		dontAcceptPushNotifications()

	})

	describe('Navigate to impressum', () => {

		beforeEach(() => {

			clickMainMenuButton()

			clickButtonWithLabel({
				de: 'Impressum',
				en: 'Imprint',
			})

		})

		it('should be possible to open the impressum page (A_014.1)', () => {

			assertPATTitle({ en: 'Imprint', de: 'Impressum' })
			cy.findByRole('heading',  translateName({ de: 'Impressum', en: 'Imprint' })).should('be.visible')

		})

		it('should include a phone number', () => {

			findTelephoneNumber().should('be.visible')

		})

		// TODO: this test is failing with
		// This element `<a>` is not visible because it has CSS property: `position: fixed` and it's being covered by another element:
		// <strong>Geschäf...</strong>
		it.skip('should include an email', () => {

			findEmail().should('be.visible')

		})

		it('should return to home page', () => {
			returnToHomePage()
		})
	})
})
