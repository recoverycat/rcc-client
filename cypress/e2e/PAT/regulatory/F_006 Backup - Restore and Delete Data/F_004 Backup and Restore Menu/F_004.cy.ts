import { assertPATTitle, dontAcceptPushNotifications, enterPATRoot } from '../../../../utils/pat-utils'
import { clickMainMenuButton, findElementWithLabel } from '../../../../utils/e2e-utils'
import { findBackButton, findQrCode } from '../../../../utils/hcp-utils'
import { assertNewFileWasDownloadedCorrectly } from '../../../../utils/fileUtils'

describe('F_004 Backup and Restore Initiative F', () => {

	let password: string = '' // added for possible restoreAction test

	beforeEach(() => {

		enterPATRoot()

		dontAcceptPushNotifications()

		clickMainMenuButton()

		findElementWithLabel({
			en: 'Backup & Restore',
			de: 'Backup & Wiederherstellung'
		}).click()

	})

	it('should be possible to open the Backup and Restore menu (F_004.1)', () => {

		assertPATTitle({ en: 'Backup & Restore', de: 'Backup & Wiederherstellung' })

	})

	it('should have all the required buttons (F_004.1)', () => {

		findElementWithLabel({
			en: 'Create Backup',
			de: 'Backup erstellen'
		}).scrollIntoView().should('be.visible')

		findElementWithLabel({
			en: 'Restore Backup',
			de: 'Backup wiederherstellen'
		}).scrollIntoView().should('be.visible')

		findElementWithLabel({
			en: 'Transmit Backup',
			de: 'Backup übertragen'
		}).scrollIntoView().should('be.visible')

		findBackButton()

	})

	it('should be possible to download the backup (F_004.2)', () => {

		const date: Date = new Date()
		const filetype: string = 'zip'

		findElementWithLabel({
			en: 'Create Backup',
			de: 'Backup erstellen'
		}).scrollIntoView().click()

		findElementWithLabel({
			en: 'confirm',
			de: 'Bestätigen'
		}).click().wait(1000)

		findElementWithLabel({
			en: 'Password',
			de: 'Passwort'
		}).next('p').then(label => {
			password = label.text()
			cy.log('password', password)
		}).then(() => {
			findElementWithLabel({
				en: 'confirm',
				de: 'Bestätigen'
			}).click().wait(1000)
		})

		assertNewFileWasDownloadedCorrectly(date, filetype)

	})

	it('should be possible to transmit the backup (F_004.4)', () => {

		findElementWithLabel({
			en: 'Transmit Backup',
			de: 'Backup übertragen'
		}).scrollIntoView().click()

		findQrCode().should('be.visible')

	})

	it('should be possible to cancel the backup transfer (F_004.5)', () => {

		findElementWithLabel({
			en: 'Transmit Backup',
			de: 'Backup übertragen'
		}).scrollIntoView().click()

		findQrCode().should('be.visible')

		findElementWithLabel({
			en: 'Cancel',
			de: 'Abbrechen'
		}).click()

		findQrCode().should('not.exist')

	})

	it('should show a retry button if the backup transfer fails (F_004.6)', () => {
		cy.clock()

		findElementWithLabel({
			en: 'Transmit Backup',
			de: 'Backup übertragen'
		}).scrollIntoView().click()

		findQrCode().should('be.visible')

		cy.tick(60000)

		findElementWithLabel({
			en: 'Transmission failed.',
			de: 'Übertragung fehlgeschlagen.'
		}).should('be.visible')

	})

})
