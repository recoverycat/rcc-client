import { dontAcceptPushNotifications, enterPATRoot } from '../../../../utils/pat-utils'
import { clickMainMenuButton, findElementWithLabel } from '../../../../utils/e2e-utils'

describe('F_002 Delete Data', () => {

	beforeEach(() => {

		enterPATRoot()

		dontAcceptPushNotifications()

		clickMainMenuButton()

		// TODO load a backup

	})

	it('should show the delete all data button in the menu (F_002.1)', () => {

		findElementWithLabel({
			en: 'Delete all data',
			de: 'Daten löschen'
		}).scrollIntoView().should('be.visible')

	})

	it('should be possible to delete all data (F_002.1)', () => {

		findElementWithLabel({
			en: 'Delete all data',
			de: 'Daten löschen'
		}).scrollIntoView().click()

		findElementWithLabel({
			en: 'confirm',
			de: 'Bestätigen'
		}).click()
	})
})
