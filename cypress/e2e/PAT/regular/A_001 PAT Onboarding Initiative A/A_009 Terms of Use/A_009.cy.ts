
import { ByRoleOptions } from '@testing-library/cypress'
import { assertPATTitle, dontAcceptPushNotifications, enterPATRoot } from '../../../../utils/pat-utils'
import { clickButtonWithLabel, clickMainMenuButton, returnToHomePage, translateName } from '../../../../utils/e2e-utils'

describe('A_009 Terms of Use Initiative A', () => {

	beforeEach(() => {

		enterPATRoot()

		dontAcceptPushNotifications()

	})

	describe('Navigate to Terms of Use', () => {

		beforeEach(() => {

			clickMainMenuButton()

			clickButtonWithLabel({
				de: 'AGB',
				en: 'Terms and Conditions',
			})

		})

		it('should be possible to open the Terms of Use page (A_009.1)', () => {

			assertPATTitle({ en: 'Terms and Conditions', de: 'AGB' })
			const translation : ByRoleOptions = translateName({ de: /Allgemeine Geschäftsbedingungen/, en: /Allgemeine Geschäftsbedingungen/ })
			cy.findByRole('heading', translation).should('be.visible')

		})

		it('should return to home page', () => {
			returnToHomePage()
		})
	})
})
