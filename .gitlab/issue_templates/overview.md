# Overview

Related software requirement: RK_SWR_01

Related Epic ClickUp ID: #1234567

Affected app variants: PAT, HCP, Study

## Summary
*Describe the gist of the feature in prose.*

## Related tickets

*Add the related issues that are part of this issue*

* [x] #12234+
