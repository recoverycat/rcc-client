import	{
			NgModule,
			Injectable,
		}											from '@angular/core'


import	{
			RccPublicRuntimeTextResourceService,
			RccPublicRuntimeTextResourceModule,
		}											from '@rcc/common'



export const mockData	:	Record<string, unknown>
						= 	{
								'my_example_emoji.txt'	: ':3',
								'my_example_text_de.md' : 'Deutscher __Beispieltext__',
								'my_example_text_en.md' : 'English __example text__',
							}

@Injectable()
export class MockRccPublicRuntimeResourceService extends RccPublicRuntimeTextResourceService {

	public async load(name: string) : Promise<string> {

		const response 	: 	unknown
						= 	mockData[name]

		const promise	: 	Promise<unknown>
						=	typeof response === 'string'
							?	Promise.resolve(response)
							:	Promise.reject( response instanceof Error ? response : new Error(String(response)))

		return await (promise as Promise<string>)
	}

}


@NgModule({
	imports:[
		RccPublicRuntimeTextResourceModule,
	],
	providers : [
		{
			provide:	RccPublicRuntimeTextResourceService,
			useClass:	MockRccPublicRuntimeResourceService
		},
	]
})
export class MockRccPublicRuntimeTextResourceModule {

	public constructor(
		public rccPublicRuntimeTextResourceService: RccPublicRuntimeTextResourceService
	){
		console.info('Mocking RccPublicRuntimeTextResourceService using this data', mockData)
	}
}
