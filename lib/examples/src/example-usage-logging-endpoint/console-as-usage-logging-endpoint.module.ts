import 	{
			NgModule,
			Injectable,
		}								from '@angular/core'

import	{
			Subject
		}								from 'rxjs'

import	{
			Factory
		}								from '@rcc/common'

import	{
			RccUsageLoggingService,
			RccUsageLoggingQueue,
			RccUsageLoggingJob,
			DynamicLoggingData,
			provideRccUsageLoggingJob,
		}								from '@rcc/features'

export const CONSOLE_LOGGING_QUEUE_ID : string = 'example-usage-logging-console-as-endpoint'

/**
 * This is just a quickly hacked testing service service
 * that is meant to support the development of parts of the usage
 * logging system.
 *
 * It serves as an endpoint for the usage logging system that
 * would normally post data to a backend, but in this case just,
 * posts (simulated) results to the console.
 */
@Injectable()
export class ConsoleAsUsageLoggingEndpointService {

	public fail : boolean = false

	public job	: RccUsageLoggingJob = undefined

	public constructor(
		private usageLoggingService: RccUsageLoggingService,
	){

		if(!window) return
		// @ts-expect-error: manipulating window object
		// eslint-disable-next-line
		if(window.consoleUsageLogging?.job)	this.job = 	window.consoleUsageLogging?.job as RccUsageLoggingJob

		// @ts-expect-error: manipulating window object
		window.consoleUsageLogging 	= 	this

		console.group('ConsoleAsUsageLoggingEndpointService')
		console.info('Use `consoleUsageLogging.fail=true` to fail posts on purpose.')
		console.info('Use `consoleUsageLogging.job.tick$.next({})` to trigger usage logging events.')
		console.groupEnd()

		const queue : 	RccUsageLoggingQueue
					= 	this.usageLoggingService
						.getQueue(CONSOLE_LOGGING_QUEUE_ID)

		queue
		.readyData$
		.subscribe( queuedLoggingData => {

			if(this.fail){
				console.info(`UsageLogging[failed] (${queuedLoggingData.jobId}):`, queuedLoggingData)
				queue.retryLater(queuedLoggingData)
				window.requestAnimationFrame( () => console.info('UsageLoggingQueue size:', queue.size))
			}
			else {
				console.info(`UsageLogging[successful] (${queuedLoggingData.jobId}):`, queuedLoggingData)
				console.info('UsageLoggingQueue size:', queue.size)
				queue.requestNext()
			}

		})

		queue
		.recycledData$
		.subscribe( queuedLoggingData => {
			console.info(`LoggingData[requeued] (${queuedLoggingData.jobId})`, queuedLoggingData)
			window.requestAnimationFrame( () => console.info('UsageLoggingQueue size:', queue.size))
		})

	}
}


const consoleUsageLoggingJobFactory	:	Factory<RccUsageLoggingJob>
									=	{
											deps:[],
											factory: () => {

												const job	: 	RccUsageLoggingJob
															=	{
																	id: 					'consoleUsageLogging',
																	description: 			'Logs events triggered from the console by `console.consoleJobTick$.next({})` ',
																	label: 					'Console Usage Logging Job',
																	tick$: 					new Subject<DynamicLoggingData>(),
																	requiresUserConsent: 	false,
																	fixedLoggingData: 		{
																								category: 'data-from-the-console',
																								key: 'example',
																							},
																	userId: 'none'
																}

												// @ts-expect-error: manipulating window object
												window.consoleUsageLogging = { job }

												return job
											}
										}

@NgModule({
	providers:[
		ConsoleAsUsageLoggingEndpointService,
		provideRccUsageLoggingJob(consoleUsageLoggingJobFactory),
	]
})
export class ConsoleAsUsageLoggingEndpointModule {

	public constructor(
		private consoleAsUsageLoggingEndpointService : ConsoleAsUsageLoggingEndpointService
	){
	}

}
