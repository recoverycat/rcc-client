import  {
			Injectable,
			NgModule
		}                              	 	from '@angular/core'

import  {
			Item,
			ItemStorage,
			NestedRecord,
			Question,
			QuestionConfig,
			QuestionStore,
		}                               	from '@rcc/core'

import  {
			provideTranslationMap,
			provideItemSelectionFilter,
			ItemSelectionFilter
		}                               	from '@rcc/common'

import  {
			QuestionnaireServiceModule,
		}                               	from '@rcc/features'



const en	: string | NestedRecord<string> =  {
						'NAME':     	'Example: Questions with categories',
						'CATEGORY_C':	'Category C',
						'CATEGORY_D':	'Category D'
					}

const de	: string | NestedRecord<string> =  {
						'NAME':     'Beispiel: Fragen in Kategorien',
						'CATEGORY_C':	'Kategorie C',
						'CATEGORY_D':	'Kategorie D'
					}




@Injectable()
export class ExampleCategorizedQuestionStore extends QuestionStore {

	public name : string = 'EXAMPLE_CATEGORIZED_QUESTION_STORE.NAME'

	public constructor(){
		super(ExampleQuestionStorage)
	}
}

const ExampleQuestionStorage : ItemStorage<Question> = { getAll: () => Promise.resolve(example_configs) }

const example_configs:QuestionConfig[] = [

	{
		id:         'c-weekly-A',
		type:       'integer',
		meaning:    'rate last week',
		translations: {
			'en': 'C How did the last seven days gone for you? [weekly]',
			'de': 'C Wie sind die vergangen 7 Tage für Dich gelaufen? [weekly]'
		},

		min:	0,
		max:	3,
		tags:       ['rcc-category-c']
	},

	{
		id:         'c-weekly-B',
		type:       'boolean',
		meaning:    'did something good happen to you in the past seven days',
		translations: {
			'en': 'C Did something good happen to you, during the past seven days? [weekly]',
			'de': 'C Ist Dir in den letzten 7 Tagen etwas Schönes passiert? [weekly]'
		},

		tags:	['rcc-category-c']
	},


	{
		id:         'd-daily-A',
		type:       'integer',
		meaning:    'favorite number < 10',       // This property will be used to display the question text,
															// if translations are missing,
															// but it's main purpose is to give the intent of the question.
															// (maybe we should drop this property)
		translations: {
			'en': 'D What\'s your favorite number below 10? [daily]',
			'de': 'D Was ist Deine Lieblingszahl unter 10? [daily]'
		},

		min:    0,
		max:    9,
		tags:   ['scale', 'rcc-category-d']
	},


	{
		id:       'd-daily-B',
		type:     'integer',
		meaning:   'What is your overall mood today?',
		translations: {
			en: 'D What is your overall mood today? [daily]',
			de: 'D Wie ist Ihre allgemeine Stimmung heute? [daily]'
		},

		tags: ['rcc-category-d'],

		options:    [
			{
				value: 0,
				translations:{
					en: 'depressed',
					de: 'bedrückt'
				}
			},
			{
				value: 1,
				translations:{
					en: 'okay',
					de: 'okay'
				}
			},
			{
				value: 2,
				translations:{
					en: 'good',
					de: 'gut'
				}
			},
			{
				value: 3,
				translations: {
					en: 'excited',
					de: 'enthusiastisch'
				}
			}
		]

	},



]


const itemSelectionFilters: ItemSelectionFilter[] = [
	{
		filter: (item:Item) => (item instanceof Question) && item.tags?.includes('rcc-category-c'),
		representation: {
			label: 'EXAMPLE_CATEGORIZED_QUESTION_STORE.CATEGORY_C',
			icon: 'question'
		}
	},
	{
		filter: (item:Item) => (item instanceof Question) && item.tags?.includes('rcc-category-d'),
		representation: {
			label: 'EXAMPLE_CATEGORIZED_QUESTION_STORE.CATEGORY_D',
			icon: 'question'
		}
	}
]





@NgModule({
	imports: [
		QuestionnaireServiceModule.forChild([ExampleCategorizedQuestionStore]),
	],
	providers: [
		ExampleCategorizedQuestionStore,
		provideTranslationMap('EXAMPLE_CATEGORIZED_QUESTION_STORE', { de ,en }),
		...itemSelectionFilters.map(provideItemSelectionFilter)
	]
})
export class ExampleCategorizedQuestionsSetupModule {}
