import  {   Injectable         		}   from '@angular/core'

import  {
				Entry,
				EntryConfig,
				EntryStore,
				ItemStorage,
				CalendarDateString,
				randomBase32String
		}								from '@rcc/core'


@Injectable()
export class ExampleChartEntryStoreService extends EntryStore {

		public readonly name 	: string
								= 'EXAMPLE_CHARTS.REPORT_STORE.NAME'

		public constructor(){
			super(staticStorage)
		}
}

const today					: 	string
							= 	CalendarDateString.today()

const aYearAgo				: 	string
							= 	CalendarDateString.daysBefore(today, 365)

const days					: 	string[]
							= 	CalendarDateString.range(aYearAgo, today)


const noteEntries			: 	EntryConfig[]
							= 	days
								.map( (day) : EntryConfig => ['rcc-curated-default-00-daily-note', randomBase32String(100).replace(/a|e|i|o|u/g,'$& '), day+'T18:00:00:00+01:00', null, day])
								.filter( () => Math.random() <= 0.2 )

const yesNoEntries_1		: 	EntryConfig[]
							= 	days
								.map( (day) : EntryConfig => ['example-yes-no-1', Math.random() > 0.5 ? true : false, day+'T18:00:00:00+01:00', null, day])
								.filter( () => Math.random() <= 0.9 )

const yesNoEntries_2		: 	EntryConfig[]
							= 	days
								.map( (day) : EntryConfig => ['example-yes-no-2', Math.random() > 0.5 ? true : false, day+'T18:00:00:00+01:00', null, day])
								.filter( () => Math.random() <= 0.9 )

const quantityEntries_1		: 	EntryConfig[]
							= 	days
								.map( (day) : EntryConfig => ['example-quantity-1', Math.floor(Math.random()*4), day+'T18:00:00:00+01:00', null, day])
								.filter( () => Math.random() <= 0.9 )

const quantityEntries_2		: 	EntryConfig[]
							= 	days
								.map( (day) : EntryConfig => ['example-quantity-2', Math.floor(Math.random()*4), day+'T18:00:00:00+01:00', null, day])
								.filter( () => Math.random() <= 0.9 )

const scaleEntries			:	EntryConfig[]
							=	days
								.map( (day) : EntryConfig => ['example-scale', Math.floor(Math.random()*100), day+'T18:00:00:00+01:00', null, day])
								.filter( () => Math.random() <= 0.9 )

const singleChoiceEntryOptions: string[] = ['none', 'drugs', 'meditation', 'sport', 'medicine']

const singleChoiceEntries_1	: 	EntryConfig[]
							= 	days
								.map( (day) : EntryConfig => ['example-single-choice-1', singleChoiceEntryOptions[Math.floor(Math.random()*5)], day+'T18:00:00:00+01:00', null, day])
								.filter( () => Math.random() <= 0.9 )

const entries				: 	EntryConfig[]
							= 	[
									...noteEntries,
									...yesNoEntries_1,
									...yesNoEntries_2,
									...quantityEntries_1,
									...quantityEntries_2,
									...scaleEntries,
									...singleChoiceEntries_1,
								]

const staticStorage 		: 	ItemStorage<Entry>
							= 	{ getAll: () => Promise.resolve(entries) }
