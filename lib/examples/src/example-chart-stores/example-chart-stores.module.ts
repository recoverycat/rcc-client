import 	{	NgModule 								} 	from '@angular/core'
import	{
			DevModule,
			provideTranslationMap,
		}												from '@rcc/common'

import	{
			EntryMetaStoreServiceModule,
			QuestionnaireServiceModule,
			CuratedQuestionStoreServiceModule
		}												from '@rcc/features'

import	{	ExampleChartEntryStoreService			}	from './example-chart-entry-store.service'
import	{	ExampleChartQuestionStoreService		}	from './example-chart-question-store.service'


import en from './i18n/en.json'
import de from './i18n/de.json'


/**
 * This module was specifically made to provide testing/dev/example data for the
 * figures.cc charts.
 */

@NgModule({
	imports: [
		CuratedQuestionStoreServiceModule,
		EntryMetaStoreServiceModule.forChild([ExampleChartEntryStoreService]),
		QuestionnaireServiceModule.forChild([ExampleChartQuestionStoreService]),
		DevModule.note('StaticReportStoreServiceModule'),
	],
	providers: [
		ExampleChartEntryStoreService,
		ExampleChartQuestionStoreService,
		provideTranslationMap('EXAMPLE_CHARTS', { en, de })
	]
})
export class ExampleChartStoresModule{}
