import	{	Injectable		}	from '@angular/core'

import	{
			Question,
			QuestionConfig,
			QuestionStore,
			ItemStorage
		}						from '@rcc/core'


@Injectable()
export class ExampleChartQuestionStoreService extends QuestionStore {

	public readonly name 	: string
							= 'EXAMPLE_CHARTS.QUESTION_STORE.NAME'

	public constructor(){
		super(staticStorage)
	}
}

const staticStorage : ItemStorage<Question, QuestionConfig>
					= { getAll: () => Promise.resolve(configs) }

const configs		:	QuestionConfig[]
					=	[
							{
								id:			'example-yes-no-1',
								type:		'boolean',
								meaning:	'Example YesNo: Is this real life?',

								translations: {
									'en': 'Example YesNo: Is this real life?',
									'de': 'Example YesNo: Ist das hier die Wirklichkeit?'
								},

								options: [
									{ value: true, translations: 	{ en: 'yes', de:'ja' } },
									{ value: false, translations: 	{ en: 'nah', de:'nö' } }
								]

							},

							{
								id:			'example-yes-no-2',
								type:		'boolean',
								meaning:	'Example YesNo: Are you sure?',

								translations: {
									'en': 'Example YesNo: Are you sure?',
									'de': 'Example YesNo: Ganz sicher?'
								},

								options: [
									{ value: true, translations: 	{ en: 'pretty', de:'ziemlich' } },
									{ value: false, translations: 	{ en: 'nay', 	de:'nee' } }
								]

							},


							{
								id:			'example-quantity-1',
								type:		'integer',
								meaning:	'Example Quantity: How many/much?',

								translations: {
									'en': 'Example Quantity: How many/much?',
									'de': 'Example Quantity: Wie viel(e)?'
								},

								options: [
									{ value: 0, meaning: 'none', 	translations: { en: 'none', 	de: 'keine'		} },
									{ value: 1, meaning: 'a few', 	translations: { en: 'a few', 	de: 'wenige'	} },
									{ value: 2, meaning: 'some', 	translations: { en: 'some', 	de: 'einige'	} },
									{ value: 3, meaning: 'lots', 	translations: { en: 'lots', 	de: 'viele'		} },
								]
							},

							{
								id:			'example-quantity-2',
								type:		'integer',
								meaning:	'Example Quantity: How many/much?',

								translations: {
									'en': 'Example Quantity: How intense?',
									'de': 'Example Quantity: Wie sehr?'
								},

								options: [
									{ value: 0, translations: { en: 'minor', 		de: 'unbedeutend'	} },
									{ value: 1, translations: { en: 'some', 		de: 'merklich'		} },
									{ value: 2, translations: { en: 'significant', 	de: 'deutlich'		} },
									{ value: 3, translations: { en: 'major', 		de: 'erheblich'		} },
								]
							},
							{
								id:			'example-scale',
								type:	 	'integer',
								meaning:	'Example Scale: What\'s your favorite number',

								translations: {
									en: 'Example Scale: What\'s your favorite number',
									de: 'Example Scale: Was ist deine Lieblingsnummer'
								},

								min: 0,
								max: 100,
								tags: ['rcc-scale'],
							},
							{
								id:			'example-single-choice-1',
								type:		'string',
								meaning:	'Example Single Choice: Which resources did you use today?',

								translations: {
									'en': 'Example Single Choice: Which resources did you use today?',
									'de': 'Example Single Choice: Welche Ressourcen hast du heute genutzt?'
								},

								options: [
									{ value: 'medicine', 	translations: { en: 'medicine', 	de: 'Medizin'		} },
									{ value: 'drugs', 		translations: { en: 'drugs', 		de: 'Drogen'		} },
									{ value: 'sport', 		translations: { en: 'sport', 		de: 'Sport'			} },
									{ value: 'meditation',	translations: { en: 'meditation', 	de: 'Meditation'	} },
									{ value: 'none',		translations: { en: 'none', 		de: 'nichts'		} },
								]

							},
						]
