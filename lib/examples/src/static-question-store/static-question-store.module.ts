import 	{	NgModule 				} 	from '@angular/core'
import	{
			DevModule,
			provideTranslationMap,
		}								from '@rcc/common'

import	{
			QuestionnaireServiceModule,
		}								from '@rcc/features'

import	{	StaticQuestionStoreService		}	from './static-question-store.service'

import en from './i18n/en.json'
import de from './i18n/de.json'

@NgModule({
	imports: [
		QuestionnaireServiceModule.forChild([StaticQuestionStoreService]),
		DevModule.note('StaticQuestionStoreServiceModule')
	],
	providers: [
		StaticQuestionStoreService,
		provideTranslationMap('STATIC_QUESTION_STORE', { en, de })
	]
})
export class StaticQuestionStoreServiceModule{}
