import { NgModule } from '@angular/core'
import { provideMainMenuEntry } from '@rcc/common'

@NgModule({
	providers: [
		provideMainMenuEntry({
			path: 'example-markdown',
			position: -1,
			icon: 'check_mark',
			label: 'Example markdown',
		})
	]
})
export class ExampleMarkdownTemplateMenuEntryModule {

}
