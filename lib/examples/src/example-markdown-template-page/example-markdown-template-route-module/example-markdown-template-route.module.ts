import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { ExampleMarkdownTemplatePageViewComponent } from '../example-markdown-template-page-view/example-markdown-template-page-view.component'

const routes: Routes = [
	{
		path:	'example-markdown',
		component: ExampleMarkdownTemplatePageViewComponent,
	}
]

@NgModule({
	imports: [
		RouterModule.forChild(routes)
	]
})
export class ExampleMarkdownTemplateRouteModule {

}
