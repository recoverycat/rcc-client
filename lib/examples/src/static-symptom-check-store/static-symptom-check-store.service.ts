import 	{ 	Injectable 			}	from '@angular/core'

import	{
			SymptomCheckConfig,
			SymptomCheckStore,
		}							from '@rcc/core'


@Injectable()
export class StaticSymptomCheckStoreService extends SymptomCheckStore {

	public readonly name = 'STATIC_SYMPTOM_CHECK_STORE.NAME'

	public constructor(){
		super(staticStorage)
	}
}

const staticStorage = { getAll: () => Promise.resolve(configs) }

const configs:SymptomCheckConfig[] = [
		{
			meta:		{
							label: 				'Example: Dr. Who',
							defaultSchedule:	[
													[],
													['morning', 'evening']
												],
							creationDate:		'2020-02-28T16:13:00.0Z'
						},
			questions:	['A', 'B', 'C']
		},

		{
			meta:		{
							label: 				'Example: Dr. Crumpler',
							defaultSchedule:	[
													[1,2,3,4,5],
													['afternoon', 'evening', 'night'],
												],
							creationDate:		'2020-05-28T03:33:00.0Z',
							paused: 			false
						},
			questions:	['B', 'C']
		},

		{
			meta:		{
							label: 				'Example: Dr. Strangelove',
							defaultSchedule:	[
													[0,2,3,4],
													['morning', 'afternoon',' evening']
												],
							creationDate:		'2020-05-28T03:33:00.0Z',
							paused: 			false
						},
			questions:	['A','D']
		},
	]
