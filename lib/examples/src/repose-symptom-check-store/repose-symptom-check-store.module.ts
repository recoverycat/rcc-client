import 	{	NgModule 								} 	from '@angular/core'
import	{	DevModule								}	from '@rcc/common'
import	{	SymptomCheckMetaStoreServiceModule		}	from '@rcc/features'
import	{	ReposeExampleSymptomCheckStore			}	from './repose-symptom-check-store.service'

@NgModule({
	imports: [
		SymptomCheckMetaStoreServiceModule.forChild([ReposeExampleSymptomCheckStore]),
		DevModule.note('ReposeExampleSymptomCheckStoreModule'),
	],
	providers: [
		ReposeExampleSymptomCheckStore,
	]
})
export class ReposeExampleSymptomCheckStoreModule{}
