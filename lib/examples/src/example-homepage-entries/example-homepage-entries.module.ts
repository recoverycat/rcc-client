import	{	Session					}	from '@rcc/core'

import	{
			NgModule
		}								from '@angular/core'

import  {
			provideHomePageEntry,
		}								from '@rcc/common'


const sessions : Session[] = [
	new Session({
		symptomCheck	: 	null,
		report			:	null,
		questions 		:	[],
	}),
	new Session({
		symptomCheck	: 	null,
		report			:	null,
		questions 		:	[],
	}),


]
@NgModule({
	providers: [
		...sessions.map(session => provideHomePageEntry({ item: session, position: () => new Date().getSeconds() % 10 > 5 ? 0 : -1 }))
	]
})
export class ExampleHomePageEntriesModule{}
