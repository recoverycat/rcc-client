import	{
			NgModule
		}										from '@angular/core'

import	{
			RccPublicRuntimeConfigModule,
			RccPublicRuntimeConfigService,
			requestPublicRuntimeConfigValue,
			requestPublicRuntimeTextResource
		}									from '@rcc/common'


@NgModule({
	imports:[
		RccPublicRuntimeConfigModule
	],
	providers : [
		requestPublicRuntimeConfigValue({
			path:				'test.myFirstValue',
			description: 		'My first test value meant for testing.',
			type:				'string',
		}),

		requestPublicRuntimeTextResource({
			name:				'startup_message_%lang.md',
			description:		'Message at startup',
		}),

	]
})
export class ExampleRuntimeConfigModule {

	public constructor(
		rccPublicRuntimeConfigService: RccPublicRuntimeConfigService
	){
		void	rccPublicRuntimeConfigService.get('test.myFirstValue')
				.then( value => console.info('Value for ExampleRuntimeConfigModule, "test.myFirstValue":', value) )
	}
}
