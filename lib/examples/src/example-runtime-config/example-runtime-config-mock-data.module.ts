import	{
			NgModule,
			Injectable,
		}									from '@angular/core'


import	{
			RccPublicRuntimeConfigModule,
			RccPublicRuntimeConfigService,
		}									from '@rcc/common'



const mockedConfig	:	unknown
					= 	{
							client_logo: {
								src: 'https://loremflickr.com/400/100',
								altText: 'Client Logo Name'
							},
							test: {
								myFirstValue: 'my first mocked value'
							},
							matomo: {
								'site-id': '2'
							},
							startup:{
								banner:{
									color:'secondary',
									markdown:{
										translations:{
											'de': '__fett__ normal<br>Zeilenumbruch',
											'en': '__bold__ normal<br>line break'
										}
									}
								},
								'contract-modal': {
									content: {
										translations: {
											de: `# Das ist eine Testversion

Um die App zu nutzen, müssen Sie den [Allgemeinen Geschäftsbedingungen](/terms-and-conditions) und den [Datenschutzhinweisen](/privacy-policy) zustimmen.

Abweichend zu den Allgemeinen Geschäftsbedingungen gilt in diesem [Angebot der Testversion und der entsprechenden Leistungsbeschreibung](http://www.example.com) eine eingeschränkte Nutzung für eine:n Nutzer:in: **Mit der Testversion können für maximal 10 Patient:innen Therapie Follow-ups erstellt werden, und die Nutzung ist begrenzt bis 30.04.2024.**

Im Rahmen der Testversion bitten wir um die Teilnahme an einer Nutzer:innenumfrage.

_Ich habe die Allgemeinen Geschäftsbedingungen, Datenschutzhinweisen, das Angebot der Testversion und die Leistungsbeschreibung gelesen und erkläre mich mit diesen Bedingungen einverstanden._`,

											en: '# This is a test version'
										}
									},
									userConfirmationKey: 'mock-contract-agreement',
									excludedRoutes: ['/terms-and-conditions', '/privacy-policy']
								}
							},
							'preConfiguredSymptomChecks': {
								'starter_symptom_check': {
										meta: {
											label: 'Starter Symptom Check',
											defaultSchedule: [
												[],
												['morning', 'evening'],
											],
											creationDate: '2024-08-01'

										},
										questions: [
											{
												id: 'rcc-curated-starter-questions-0001-daily-0',
												category: 'symptoms'
											},
											{
												id: 'rcc-curated-starter-questions-0002-daily-0',
												category: 'symptoms'
											},
											{
												id: 'rcc-curated-starter-questions-0003-daily-1',
												category: 'resources'
											},
											{
												id: 'rcc-curated-default-00-daily-note',
												category: 'daily_notes'
											}
										]
								}
							},
							'questionConfigsBackup': [
								{
									id:       'rcc-curated-default-01-daily',
									type:     'integer',
									meaning:    'Konnten Sie Ihren Alltag heute so gut wie sonst bewältigen?',
									translations: {
										en: 'How well were you able to manage your usual daily tasks today?',
										de: 'Konnten Sie Ihren Alltag heute so gut wie sonst bewältigen?'
									},
									options:    [
										{ value:0  , translations: { en: 'yes',                                                   de: 'ja, vollkommen' } },
										{ value:1  , translations: { en: 'somewhat worse',                                        de: 'ja, mit leichten Einschränkungen' } },
										{ value:2  , translations: { en: 'moderately worse',                                      de: 'mit deutlichen Einschränkungen' } },
										{ value:3  , translations: { en: 'a lot worse',                                           de: 'kaum' } },
									],
									tags:     ['rcc-category-psychosis', 'rcc-category-depression', 'rcc-category-bipolar-disorder', 'rcc-category-schizoaffective-disorders', 'icd-10-f20', 'icd-10-f25', 'icd-10-f31', 'icd-10-f33', 'schizoaffective disorders', 'psychosis', 'rcc-symptom-area-daily-function', 'rcc-category-daily', 'rcc-category-default']
								},
								{
									id:       'rcc-curated-default-01-daily-1',
									type:     'integer',
									meaning:    'Konnten Sie Ihren Alltag heute so gut wie sonst bewältigen?',
									translations: {
										en: 'How well were you able to manage your usual daily tasks today?',
										de: 'Konnten Sie Ihren Alltag heute so gut wie sonst bewältigen?'
									},
									options:    [
										{ value:0  , translations: { en: 'a lot worse',                                           de: 'kaum' } },
										{ value:1  , translations: { en: 'moderately worse',                                      de: 'mit deutlichen Einschränkungen' } },
										{ value:2  , translations: { en: 'somewhat worse',                                        de: 'ja, mit leichten Einschränkungen' } },
										{ value:3  , translations: { en: 'yes',                                                   de: 'ja, vollkommen' } },
									],
									tags:     ['rcc-category-psychosis', 'rcc-category-depression', 'rcc-category-bipolar-disorder', 'rcc-category-schizoaffective-disorders', 'icd-10-f20', 'icd-10-f25', 'icd-10-f31', 'icd-10-f33', 'schizoaffective disorders', 'psychosis', 'rcc-symptom-area-daily-function', 'rcc-category-daily', 'rcc-category-default']
								},
								{
									id:       'rcc-curated-default-01-daily-2',
									type:     'integer',
									meaning:    'Konnten Sie Ihren Alltag heute so gut wie sonst bewältigen?',
									translations: {
										en: 'How well were you able to manage your usual daily tasks today?',
										de: 'Konnten Sie Ihren Alltag heute so gut wie sonst bewältigen?'
									},
									options:    [
										{ value:0  , translations: { en: 'a lot worse',                                           de: 'sehr viel schlechter' } },
										{ value:1  , translations: { en: 'moderately worse',                                      de: 'deutlich schlechter' } },
										{ value:2  , translations: { en: 'somewhat worse',                                        de: 'etwas schlechter' } },
										{ value:3  , translations: { en: 'yes',                                                   de: 'ja' } },
									],
									tags:     ['rcc-category-psychosis', 'rcc-category-depression', 'rcc-category-bipolar-disorder', 'rcc-category-schizoaffective-disorders', 'icd-10-f20', 'icd-10-f25', 'icd-10-f31', 'icd-10-f33', 'schizoaffective disorders', 'psychosis', 'rcc-symptom-area-daily-function', 'rcc-category-daily', 'rcc-category-default']
								},
								{
									id:       'rcc-curated-default-01-daily-3',
									type:     'integer',
									meaning:    'Konnten Sie Ihren Alltag heute so gut wie sonst bewältigen?',
									translations: {
										en: 'How well were you able to manage your usual daily tasks today?',
										de: 'Konnten Sie Ihren Alltag heute so gut wie sonst bewältigen?'
									},
									options:    [
										{ value:0  , translations: { en: 'a lot worse',                                           de: 'sehr viel schlechter' } },
										{ value:1  , translations: { en: 'moderately worse',                                      de: 'deutlich schlechter' } },
										{ value:2  , translations: { en: 'somewhat worse',                                        de: 'etwas schlechter' } },
										{ value:3  , translations: { en: 'very',                                                  de: 'ja' } },
									],
									tags:     ['rcc-category-psychosis', 'rcc-category-depression', 'rcc-category-bipolar-disorder', 'rcc-category-schizoaffective-disorders', 'icd-10-f20', 'icd-10-f25', 'icd-10-f31', 'icd-10-f33', 'schizoaffective disorders', 'psychosis', 'rcc-symptom-area-daily-function', 'rcc-category-daily', 'rcc-category-default']
								},
								{
									id:       'rcc-curated-default-02-daily',
									type:     'integer',
									meaning:    'Haben Sie sich heute niedergeschlagen gefühlt?',
									translations: {
										en: 'Did you feel down today?',
										de: 'Haben Sie sich heute niedergeschlagen gefühlt?'
									},
									options:    [
										{ value:0  , translations: { en: 'not at all',                                                   de: 'nein' } },
										{ value:1  , translations: { en: 'somewhat',                                                     de: 'etwas' } },
										{ value:2  , translations: { en: 'moderately',                                                   de: 'mäßig' } },
										{ value:3  , translations: { en: 'very',                                                         de: 'stark' } },
									],
									tags:     ['rcc-category-psychosis', 'rcc-category-depression', 'rcc-category-bipolar-disorder', 'rcc-category-schizoaffective-disorders', 'icd-10-f20', 'icd-10-f25', 'icd-10-f31', 'icd-10-f33', 'schizoaffective disorders', 'psychosis', 'rcc-symptom-area-mood', 'rcc-category-daily', 'rcc-category-default']
								},
								{
									id:       'rcc-curated-default-03-daily',
									type:     'integer',
									meaning:    'Sind Sie antriebslos?',
									translations: {
										en: 'Was your energy low today?',
										de: 'Sind Sie antriebslos?'
									},
									options:    [

										{ value:0  , translations: { en: 'not at all',                             de: 'nein' } },
										{ value:1  , translations: { en: 'somewhat',                               de: 'etwas' } },
										{ value:2  , translations: { en: 'moderately',                             de: 'deutlich' } },
										{ value:3  , translations: { en: 'very',                                   de: 'stark' } },
									],
									tags:     ['rcc-category-psychosis', 'rcc-category-depression', 'rcc-category-bipolar-disorder', 'rcc-category-schizoaffective-disorders', 'icd-10-f20', 'icd-10-f25', 'icd-10-f31', 'icd-10-f33', 'schizoaffective disorders', 'psychosis', 'rcc-symptom-area-drive', 'rcc-category-daily', 'rcc-category-default']

								},
								{
									id:       'rcc-curated-default-04-daily',
									type:     'integer',
									meaning:    'Haben Sie das Gefühl, dass etwas Seltsames vor sich geht?',
									translations: {
										en: 'Did you feel like something strange was going on today?',
										de: 'Haben Sie das Gefühl, dass etwas Seltsames vor sich geht?'
									},
									options:    [

										{ value:0  , translations: { en: 'not at all',                     de: 'nein' } },
										{ value:1  , translations: { en: 'somewhat',                       de: 'manchmal' } },
										{ value:2  , translations: { en: 'moderately',                     de: 'häufig' } },
										{ value:3  , translations: { en: 'a lot',                          de: 'ständig' } },
									],
									tags:     ['rcc-category-psychosis', 'rcc-category-schizoaffective-disorders', 'icd-10-f20', 'icd-10-f25', 'schizoaffective disorders', 'psychosis', 'rcc-symptom-area-delusion', 'rcc-category-daily', 'rcc-category-default']
								},
								{
									id:       'rcc-curated-default-05-daily',
									type:     'integer',
									meaning:    'Sind Sie heute misstrauisch?',
									translations: {
										en: 'Did you feel suspicious today?',
										de: 'Sind Sie heute misstrauisch?'
									},
									options:    [

										{ value:0  , translations: { en: 'not at all',                            de: 'nein' } },
										{ value:1  , translations: { en: 'somewhat',                              de: 'gering' } },
										{ value:2  , translations: { en: 'moderately',                            de: 'mäßig' } },
										{ value:3  , translations: { en: 'very',                                  de: 'stark' } },
									],
									tags:     ['rcc-category-psychosis', 'rcc-category-schizoaffective-disorders', 'icd-10-f20', 'icd-10-f25', 'schizoaffective disorders', 'psychosis', 'rcc-symptom-area-paranoid-thoughts', 'rcc-category-daily', 'rcc-category-default']
								},
								{
									id:       'rcc-curated-default-05-daily-1',
									type:     'integer',
									meaning:    'Sind Sie heute misstrauisch?',
									translations: {
										en: 'Did you feel suspicious today?',
										de: 'Sind Sie heute misstrauisch?'
									},
									options:    [

										{ value:0  , translations: { en: 'not at all',                            de: 'nein' } },
										{ value:1  , translations: { en: 'somewhat',                              de: 'etwas' } },
										{ value:2  , translations: { en: 'moderately',                            de: 'mäßig' } },
										{ value:3  , translations: { en: 'very',                                  de: 'stark' } },
									],
									tags:     ['rcc-category-psychosis', 'rcc-category-schizoaffective-disorders', 'icd-10-f20', 'icd-10-f25', 'schizoaffective disorders', 'psychosis', 'rcc-symptom-area-paranoid-thoughts', 'rcc-category-daily', 'rcc-category-default']
								},
								{
									id:       'rcc-curated-default-06-daily',
									type:     'integer',
									meaning:    'Waren Sie voller Energie und motiviert, viele verschiedene Dinge zu tun?',
									translations: {
										en: 'Were you full of energy and motivated to do lots of different things today?',
										de: 'Waren Sie voller Energie und motiviert, viele verschiedene Dinge zu tun?'
									},
									options:    [

										{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
										{ value:1  , translations: { en: 'somewhat',                  de: 'gering' } },
										{ value:2  , translations: { en: 'moderately',                de: 'mäßig' } },
										{ value:3  , translations: { en: 'very',                      de: 'stark' } },
									],
									tags:     ['rcc-category-schizoaffective-disorders', 'rcc-category-bipolar-disorder', 'icd-10-f31', 'icd-10-f33', 'bipolar disorder', 'schizoaffective disorders', 'rcc-symptom-area-drive-activity', 'rcc-category-daily', 'rcc-category-default']
								},
								{
									id:       'rcc-curated-default-06-daily-1',
									type:     'integer',
									meaning:    'Waren Sie voller Energie und motiviert, viele verschiedene Dinge zu tun?',
									translations: {
										en: 'Were you full of energy and motivated to do lots of different things today?',
										de: 'Waren Sie heute voller Energie und motiviert, viele verschiedene Dinge zu tun?'
									},
									options:    [

										{ value:0  , translations: { en: 'not at all',                de: 'nein' } },
										{ value:1  , translations: { en: 'somewhat',                  de: 'gering' } },
										{ value:2  , translations: { en: 'moderately',                de: 'mäßig' } },
										{ value:3  , translations: { en: 'very',                      de: 'stark' } },
									],
									tags:     ['rcc-category-schizoaffective-disorders', 'rcc-category-bipolar-disorder', 'icd-10-f31', 'icd-10-f33', 'bipolar disorder', 'schizoaffective disorders', 'rcc-symptom-area-drive-activity', 'rcc-category-daily', 'rcc-category-default']
								},
								{
									id:       'rcc-curated-default-07-daily',
									type:     'integer',
									meaning:    'Haben Sie heute den Eindruck, die Gedanken rasen Ihnen durch den Kopf?',
									translations: {
										en: 'Did you have the impression that thoughts were racing through your head today?',
										de: 'Haben Sie heute den Eindruck, die Gedanken rasen Ihnen durch den Kopf?'
									},
									options:    [

										{ value:0  , translations: { en: 'not at all',            de: 'nein' } },
										{ value:1  , translations: { en: 'rarely',                de: 'manchmal' } },
										{ value:2  , translations: { en: 'often',                 de: 'häufig' } },
										{ value:3  , translations: { en: 'constantly',            de: 'ständig' } },
									],
									tags:     ['rcc-category-schizoaffective-disorders', 'rcc-category-bipolar-disorder', 'icd-10-f31', 'icd-10-f33', 'bipolar disorder', 'schizoaffective disorders', 'rcc-symptom-area-cognitive-abilities', 'rcc-category-daily', 'rcc-category-default']
								},
							]

						}

@Injectable()
export class MockRccPublicRuntimeConfigService extends RccPublicRuntimeConfigService{

	protected async loadConfig() : Promise<unknown> {

		return await Promise.resolve(mockedConfig)
	}

}


@NgModule({
	imports:[
		RccPublicRuntimeConfigModule,
	],
	providers : [
		{
			provide:	RccPublicRuntimeConfigService,
			useClass:	MockRccPublicRuntimeConfigService
		},
	]
})
export class MockRccPublicRuntimeConfigModule {

	public constructor(
		public rccPublicRuntimeConfigService: RccPublicRuntimeConfigService
	){
		console.info('Mocking RccPublicRuntimeConfigService using this config', mockedConfig)
	}
}
