/**
 * This is a quick example of how to add a new page to the recovery cat app.
 * Everything was put into one file for demonstration purposes.
 *
 * This example includes:
 * * a single page,
 * * a menu entry,
 * * translations
 *
 * In order for this module to do anything, it has to be imported into th AppModule (already done).
 */


import 	{
			Component,
			NgModule
		} 								from '@angular/core'

import	{	RouterModule			}	from '@angular/router'

import	{
			SharedModule,
			provideMainMenuEntry,
			provideTranslationMap
		}								from '@rcc/common'


const	en =	{
					'EXAMPLE_PAGE_MENU_ENTRY':	'ExamplePage',
					'EXAMPLE_TRANSLATION': 		'Translated text example (en)'
				}

const	de =	{
					'EXAMPLE_PAGE_MENU_ENTRY':	'Beispielseite',
					'EXAMPLE_TRANSLATION': 		'Übersetztes Textbeispiel (de)'
				}



@Component({
	template:  	`
					<ion-content>

						<h1> My example page </h1>

						<p> My example content </p>

						<p  [id] = "'EXAMPLES.EXAMPLE_TRANSLATION' | toID:'rcc-e2e'"> {{'EXAMPLES.EXAMPLE_TRANSLATION' | translate }} </p>

					</ion-content>
				`,
})
export class MyExamplePageComponent{}


@Component({
	template:	`
					<ion-item routerLink = "examples/example-page">
						<ion-label [id] = "'EXAMPLES.EXAMPLE_PAGE_MENU_ENTRY' | toID:'rcc-e2e'">{{ "EXAMPLES.EXAMPLE_PAGE_MENU_ENTRY" | translate }}</ion-label>
					</ion-item>
				`
})
export class MyExampleMenuEntryComponent {}


const routes 				=	[
									{
										path: 		'examples/example-page',
										component: 	MyExamplePageComponent
									}
								]


@NgModule({
	imports: [
		SharedModule,
		RouterModule.forChild(routes),

	],
	providers: [
		provideMainMenuEntry({ position: -1, component: MyExampleMenuEntryComponent }),
		provideTranslationMap('EXAMPLES', { en, de })
	],
	declarations:[
		MyExamplePageComponent,
		MyExampleMenuEntryComponent
	]
})
export class ExamplePageModule {}
