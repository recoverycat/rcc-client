import	{
			NgModule,
			Injectable,
		}									from '@angular/core'


import	{
			RccPublicRuntimeConfigModule,
			RccPublicRuntimeConfigService,
		}									from '@rcc/common'
import	{	SymptomCheckConfig			}	from '@rcc/core'


export const mockedSymptomCheckConfig	:	Record<string, Record<string, SymptomCheckConfig>>
					= 		{
								'preConfiguredSymptomChecks': {
									'name_of_a_preconfigured_symptom_check':
									{
										meta: {
											label: 'Example: Dr. Who',
												defaultSchedule: [
													[],
													['morning', 'evening']
												],
													creationDate: '2020-02-28T16:13:00.0Z'
										},
										questions: ['A', 'B', 'C']

									} // some dummy symptom check config
								}
							}

@Injectable()
export class MockSymptomCheckDataService extends RccPublicRuntimeConfigService{

	protected async loadConfig() : Promise<unknown> {

		return await Promise.resolve(mockedSymptomCheckConfig)
	}

}


@NgModule({
	imports:[
		RccPublicRuntimeConfigModule,
	],
	providers : [
		{
			provide:	RccPublicRuntimeConfigService,
			useClass:	MockSymptomCheckDataService
		},
	]
})
export class MockStaticRccPublicRuntimeConfigModule {

	public constructor(
		public rccPublicRuntimeConfigService: RccPublicRuntimeConfigService
	){
		console.info('Mocking MockStaticRccPublicRuntimeConfigModule using this config', mockedSymptomCheckConfig)
	}
}
