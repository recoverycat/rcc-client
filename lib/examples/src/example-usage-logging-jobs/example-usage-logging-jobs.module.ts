import	{
			NgModule,
		}										from '@angular/core'

import	{
			Subject, interval, map,
		}										from 'rxjs'

import {
			SettingsModule,
			TranslationsModule,
		}										from '@rcc/common'
import	{
			DynamicLoggingData,
			provideRccUsageLoggingJob
		}										from '@rcc/features/usage-logging/usage-logging.commons'

@NgModule({
	imports : [
		SettingsModule,
		TranslationsModule
	],
	providers: [
		provideRccUsageLoggingJob({
			id: 	'1',
			description: 'Example Logging Job none - 1',
			label: 'Example Logging Job - 1',
			tick$: interval(1000*5).pipe( map(() => ({
						extraValue: 'example-data',
						computableValue: 1
					}))),
			requiresUserConsent: false,
			fixedLoggingData: {
				category: 'example category',
				key: 'example key',
			},
			userId: 'none'
		}),
		provideRccUsageLoggingJob({
			id: '2',
			description: 'Example Logging Job daily - 2',
			label: 'Example Logging Job - 2',
			tick$: interval(1000*5.34).pipe( map(() => ({
						extraValue: 'example-data-daily',
						computableValue: 1
					}))),
			requiresUserConsent: false,
			fixedLoggingData: {
				category: 'example category',
				key: 'example key',
			},
			userId: 'daily'
		}),
		provideRccUsageLoggingJob({
			id: '3',
			description: 'Example Logging Job weekly - 3',
			label: 'Example Logging Job - 3',
			tick$: new Subject<DynamicLoggingData>(),
			requiresUserConsent: true,
			fixedLoggingData: {
				category: 'example category',
				key: 'example key',
			},
			userId: 'weekly'
		}),
	]
})

export class ExampleUsageLoggingJobsModule{}
