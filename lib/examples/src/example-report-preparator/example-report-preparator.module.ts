import 	{
			NgModule
		} 														from '@angular/core'

import	{
			DevModule,
			provideTranslationMap,
		}														from '@rcc/common'

import	{
			QuestionnaireServiceModule,
			ReportMetaStoreServiceModule,
			DataVisualizationModule,
		}														from '@rcc/features'

import	{	ExampleReportPreparatorQuestionStoreService 	}	from './example-report-preparator-question-store.service'
import	{	ExampleReportPreparatorReportStoreService		}	from './example-report-preparator-report-store.service'
import	{	ExampleReportPreparatorService					}	from './example-report-preparator.service'

import en from './i18n/en.json'
import de from './i18n/de.json'


@NgModule({
	imports: [
		QuestionnaireServiceModule.forChild( [ExampleReportPreparatorQuestionStoreService] ),
		ReportMetaStoreServiceModule.forChild( [ExampleReportPreparatorReportStoreService] ),
		DataVisualizationModule.forChild( [ExampleReportPreparatorService] ),
		DevModule.note('ExampleReportPreparatorModule')
	],
	providers: [
		ExampleReportPreparatorQuestionStoreService,
		ExampleReportPreparatorReportStoreService,
		ExampleReportPreparatorService,
		provideTranslationMap('EXAMPLE_REPORT_PREPARATOR', { en, de })
	]
})
export class ExampleReportPreparatorModule{}
