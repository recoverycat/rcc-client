import 	{	NgModule 						} 	from '@angular/core'
import	{
			DevModule,
			provideTranslationMap,
		}										from '@rcc/common'

import	{
			EntryMetaStoreServiceModule,
		}										from '@rcc/features'

import	{	StaticEntryStoreService				}	from './static-entry-store.service'


import en from './i18n/en.json'
import de from './i18n/de.json'

@NgModule({
	imports: [
		EntryMetaStoreServiceModule.forChild([StaticEntryStoreService]),
		DevModule.note('StaticEntryStoreServiceModule'),
	],
	providers: [
		StaticEntryStoreService,
		provideTranslationMap('STATIC_ENTRY_STORE', { en, de })
	]
})
export class StaticEntryStoreServiceModule{}
