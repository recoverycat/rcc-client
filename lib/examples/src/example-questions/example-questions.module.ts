/**
 * This is a quick example of how to add static questions to the recovery cat app.
 * Everything was put into one file for demonstration purposes.
 *
 * This example includes:
 * * static question store
 * * example questions
 *
 * In order for this module to do anything, it has to be imported into th AppModule (already done).
 *
 * The newly added example question(s) should show up in the QuestionnaireService
 */



import	{
			Injectable,
			NgModule
		}     							from '@angular/core'

import  {
			QuestionConfig,
			QuestionStore
		}								from '@rcc/core'

import  {
			provideTranslationMap,
		}								from '@rcc/common'

import	{
			QuestionnaireServiceModule
		}								from '@rcc/features'



const en =	{
				'NAME':		'Example Questions'
			}

const de = 	{
				'NAME':		'BeispielFragen'
			}




@Injectable()
export class ExampleQuestionStore extends QuestionStore {

	public name = 'EXAMPLE_QUESTION_STORE.NAME'

	public  constructor(){
	super(ExampleQuestionStorage)
	}
}


// A QuestionStore can get its questions from anywhere:
// localStorage, some REST-API, local file.
// But in this example case we just use static entries, hard coded right here:

const ExampleQuestionStorage = { getAll: () => Promise.resolve(example_configs) }

const example_configs:QuestionConfig[] = [

	{
		id:			'rcc-example-A',
		type:		'integer',
		meaning:	'just an example question text',			// This property will be used to display the question text,
																// if translations are missing,
																// but it's main purpose is to give the intent of the question.
																// (maybe we should drop this property)
		translations: {
			'en': 'What kind of example question is this?',
			'de': 'Was für eine Beispielfrage ist das denn?'
		},

		min:	0,
		max:	5,
		tags:	['rcc-example-question']

	},

]




@NgModule({
	imports: [
		QuestionnaireServiceModule.forChild([ExampleQuestionStore]),
	],
	providers: [
		ExampleQuestionStore,
		provideTranslationMap('EXAMPLE_QUESTION_STORE', { en, de })
	]
})
export class ExampleQuestionStoreModule{}
