import { CommonModule } from '@angular/common'
import { Component } from '@angular/core'
import { RccMarkdownComponent } from '@rcc/common/ui-components/markdown'

@Component({
	selector	: 'example-markdown-runtime-modal',
	templateUrl	: './example-markdown-runtime-modal.component.html',
	styleUrls	: ['example-markdown-runtime-modal.component.scss'],
	standalone	: true,
	imports		: [RccMarkdownComponent, CommonModule]
})
export class ExampleMarkdownRuntimeModalComponent {
	protected value: string = '# Try me\n\nEnter markdown text\n\n- like\n\n- this'

	protected onInput(event: Event): void {
		const target: HTMLInputElement = event.target as HTMLInputElement

		this.value = target.value
	}
}
