import	{	NgModule					}	from '@angular/core'
import	{
			provideTranslationMap,
			SharedModule
		}									from '@rcc/common'
import	{
			QueriesModule
		}									from '../queries'
import	{
			SymptomCheckQueryRunComponent
		}									from './symptom-check-query-run'
import	{
			SymptomCheckAllDoneSlideComponent
		}									from './symptom-check-all-done-slide'

import en from './i18n/en.json'
import de from './i18n/de.json'


@NgModule({
	imports:[
		QueriesModule,
		SharedModule,
	],
	declarations:[
		SymptomCheckQueryRunComponent,
		SymptomCheckAllDoneSlideComponent,
	],
	exports:[
		SymptomCheckQueryRunComponent,
		SymptomCheckAllDoneSlideComponent,
	],
	providers: [
		provideTranslationMap('SYMPTOM_CHECK_QUERIES', { en, de }),
	]
})
export class SymptomCheckQueryModule {}
