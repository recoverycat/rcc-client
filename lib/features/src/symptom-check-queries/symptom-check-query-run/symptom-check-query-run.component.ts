import 	{
			Component,
			ViewChild,
			OnDestroy,
			Input,
			OnChanges,
		}     								from '@angular/core'

import	{
			Router
		}									from '@angular/router'

import	{
			map,
			merge,
			Observable,
			share,
			Subject,
			takeUntil
		}									from 'rxjs'

import	{
			Action,
			PageHandler,
			RccSliderComponent,
		}									from '@rcc/common'

import	{
			assertSporadicScheduleConfig,
			CalendarDateString,
			Entry,
			InfoSlideConfig,
			isPeriodicScheduleConfig,
			isSporadicScheduleConfig,
			isWithinInterval,
			Question,
			Schedule,
			SymptomCheck
		}									from '@rcc/core'

import	{
			DayQueryService,
			QuestionnaireService,
			QueryControl,
			QueryRunComponent,
			SymptomCheckMetaStoreService,
			EntryMetaStoreService,
			JournalService,
			SubmitEntryFn,
		}									from '../..'


/**
 * This component is a more minimalist version of DayQueryRunPageComponent.
 * It shows a query run for a single {@link SymptomCheck} without calendar functionality.
 */
@Component({
    selector: 'rcc-symptom-check-query-run',
    templateUrl: './symptom-check-query-run.component.html',
    styleUrls: ['./symptom-check-query-run.component.css'],
    standalone: false
})
export class SymptomCheckQueryRunComponent implements OnChanges, OnDestroy {
	@Input()
	public	symptomCheck		: SymptomCheck

	@Input()
	public	doneAction			: Action

	public get slidesList(): (string | InfoSlideConfig)[] | undefined {
		if (this.symptomCheck?.presentation?.slides)
			return this.symptomCheck.presentation.slides
		return this.symptomCheck?.questionIds
	}

	@ViewChild(QueryRunComponent)
	public 	queryRunComponent	: QueryRunComponent

	/**
	 * Subscribed to by rcc-pagination to indicate the number of the currently selected slide.
	 */
	public	activeSlidePosition$ : Observable<number>

	public	pageHandlers		: PageHandler[]			= new Array<PageHandler>()
	public	queryControls		: QueryControl[]		= []

	public	queryControlUpdate$	: Subject<void>			= new Subject<void>()

	public	doneToday			: number				// number of questions answered today so far
	public	totalToday			: number				// number of total questions for today
	public	allDoneToday		: boolean	= false		// whether doneToday === totalToday

	public constructor(
		public	dayQueryService					: DayQueryService,
		public	questionnaireService			: QuestionnaireService,
		public	symptomCheckMetaStoreService	: SymptomCheckMetaStoreService,
		public	entryMetaStoreService			: EntryMetaStoreService,
		public	journalService					: JournalService,
		private	router							: Router
	) {
		void this.update()
	}

	public ngOnChanges(): void {
		void this.update()
	}

	public async update(): Promise<void> {
		this.updateActiveSlidePosition()
		await this.updateQueryControls()
		this.updatePageHandlers()
		this.updateStats()
	}

	/**
	 * Updates `activeSlidePosition$` whenever the current slide changes.
	 * This works independently of QueryControls, because not all slides
	 * have them.
	 */
	public updateActiveSlidePosition(): void {
		const slider : RccSliderComponent = this.queryRunComponent?.slider
		if (!slider)					return
		if (this.activeSlidePosition$)	return

		this.activeSlidePosition$ = slider.slideChange$
									.pipe(map(() => slider.currentSlidePosition))
	}

	/**
	 * Updates page handlers for pagination/progress from slider component.
	 * Needs to be called when query controls change.
	 */
	public updatePageHandlers() : void {
		this.pageHandlers = []
		if (!this.queryRunComponent)				return
		if (!this.queryRunComponent.slider)			return
		if (!this.queryRunComponent.slider.slides)	return

		const slideHandlers : PageHandler[] =	this.queryRunComponent.slider.slides
												.map((_, index) => ({
													label: 		(index+1).toString(),
													handler:	() => this.queryRunComponent.slider.slideTo(index)
												}))

		this.pageHandlers = slideHandlers
	}

	/**
	 * Gets {@link QueryControl}s for all due questions in the {@link SymptomCheck}.
	 * Simplified version of {@link DayQueryService.getQueryControls()} that can
	 * handle both periodic and sporadic schedule configs.
	 */
	public async getQueryControls(): Promise<QueryControl[]> {
		const questionIds	: string[] 		=	this.symptomCheck.getDueQuestionIds(new Date())

		await this.questionnaireService.ready
		const questions		: Question[]	=	this.questionnaireService.items
												.filter( q => questionIds.includes(q.id) )

		return await Promise.all(questions.map(q => {
			const schedule : Schedule = this.symptomCheck.getEffectiveSchedule(q.id)
			if (!schedule) throw new Error('SymptomCheckQueryRun.getQueryControls(): Missing schedule')

			if (isPeriodicScheduleConfig(schedule.config))
				return this.getPeriodicQueryControl(q, CalendarDateString.today())
			if (isSporadicScheduleConfig(schedule.config))
				return this.getSporadicQueryControl(q, schedule)
		} ))
	}

	/**
	 * Creates a {@link QueryControl} for the given periodic {@link Question} and target day.
	 */
	protected async getPeriodicQueryControl(question: Question, targetDay: string = CalendarDateString.today()) : Promise<QueryControl> {
		return await this.dayQueryService.getQueryControl(question, targetDay)
	}

	/**
	 * Creates a {@link QueryControl} for the given {@link Question} and sporadic {@link Schedule}.
	 * The query control is set up to write answers to the {@link JournalService}
	 * on submit. Also it will check if the question already has a stored entry
	 * for the given interval and if so, assigns it to the returned query control
	 * {@link QueryControl.entry}.
	 */
	protected async getSporadicQueryControl(question: Question, schedule: Schedule) : Promise<QueryControl> {
		assertSporadicScheduleConfig(schedule.config)

		await this.entryMetaStoreService.ready

		const today	: Date					= new Date()
		const allDueIntervals : Interval[]	= schedule.getDueIntervals()
		const currentInterval : Interval	= allDueIntervals.find(i => isWithinInterval(today, i))

		// If several entries exist for this question and interval, use the most recent one
		// targetDay is to be undefined for sporadic questions
		// This is to avoid overwriting entries for the same question that is part of a periodic symptom check
		const existingEntries : Entry[] 	=	this.entryMetaStoreService.items.filter(entry =>
													entry.questionId === question.id
													&& !entry.targetDay
													&& isWithinInterval(CalendarDateString.toDate(entry.date), currentInterval)
												)
		const existingDates : string[]		= existingEntries.map(entry => entry.date).sort().reverse()
		const initialEntry : Entry | null	= existingEntries.find(entry => entry.date === existingDates[0]) || null

		// Determines what happens when the QueryControl submits:
		const submitFn 		: 	SubmitEntryFn
							=	async (questionId:string, answer:string|number|boolean, note?:string): Promise<Entry> => await this.journalService.log(questionId, answer, note)

		// Determines what happens when the QueryControl wants to clean up an entry that is no longer needed
		// e.g. after multiple submissions or a reset.
		const cleanUpFn 	: 	(entry: Entry) => Promise<Entry>
							=	async (entry: Entry) : Promise<Entry> => {
									// Nothing to clean up:
									if (!entry) return null

									// There was an initial entry or some new entry
									// was created in the process
									// that is no longer needed, so remove it:
									return await this.journalService.removeEntry(entry)
								}


		return 	new QueryControl(
					question,
					submitFn,
					cleanUpFn,
					initialEntry
				)
	}

	public async updateQueryControls() : Promise<void>  {

		this.queryControlUpdate$.next()

		if (!this.symptomCheck) {
			this.queryControls = []
			return undefined
		}

		this.queryControls = await this.getQueryControls()

		const allChanges : Observable<void>[] =	this.queryControls.map( queryControl => queryControl.change$ )

		merge(...allChanges)
		.pipe(
			takeUntil(this.queryControlUpdate$),
			share()
		)
		.subscribe( () => this.updateStats() )

	}

	/**
	 * Calculates various stats used in the template, so they don't have to be recalculated on every change detection.
	 */
	public updateStats() : void {

		const total	: number	= this.queryControls.length
		const done	: number	= this.queryControls.filter( queryControl => !!queryControl.entry ).length

		this.doneToday				= done	? done	: 0
		this.totalToday				= total	? total : 0

		this.allDoneToday	= (done && total) && this.doneToday === this.totalToday

	}

	/**
	 * Restart the run
	 */
	public completeToday() : void {
		this.queryRunComponent.gotoFirstUnansweredQuery()
	}

	/**
	 * Go to first slide
	 */
	public gotoStart() : void {
		this.queryRunComponent.gotoStart()
	}

	/**
	 * Unsubscribe from subscriptions
	 */
	public ngOnDestroy() : void {
		this.queryControlUpdate$.next()
		this.queryControlUpdate$.complete()
	}

	/**
	 * Return to home page
	 */
	public onCancel(): void {
		void this.router.navigate(['/'])
	}


	/**
	 * Return to home page
	 */
	public onDone(): void {
		void this.router.navigate(['/'])
	}

}
