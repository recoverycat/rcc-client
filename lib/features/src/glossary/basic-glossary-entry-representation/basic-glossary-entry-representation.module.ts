import 	{
			NgModule,
		}									from '@angular/core'

import	{
			CommonModule
		}									from '@angular/common'

import	{
			provideItemRepresentation,
			TranslationsModule,
			RccToIDPipeModule,
			ItemRepresentation,
		}										from '@rcc/common'
import	{	BasicGlossaryEntryLabelComponent	}	from './basic-glossary-entry-label'
import	{	GlossaryEntry						}	from '@rcc/core/src/items/glossary-entry'

const basicGlossaryEntryRepresentation: ItemRepresentation = {
	itemClass:		GlossaryEntry,
	name:			'GLOSSARY_ENTRY',
	icon:			'report',
	labelComponent:	BasicGlossaryEntryLabelComponent,
	cssClass:		'glossary-entry-label'
}

@NgModule({
	imports:[
		CommonModule,
		TranslationsModule,
		RccToIDPipeModule
	],
	providers:[
		provideItemRepresentation(basicGlossaryEntryRepresentation)
	],
	declarations:[
		BasicGlossaryEntryLabelComponent
	]
})
export class BasicGlossaryEntryRepresentationModule {}
