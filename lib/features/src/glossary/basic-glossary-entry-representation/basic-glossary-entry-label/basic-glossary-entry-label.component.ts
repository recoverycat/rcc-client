import	{	Component 				}	from '@angular/core'
import	{	ExposeTemplateComponent	}	from '@rcc/common'
import	{	GlossaryEntry			}	from '@rcc/core/src/items/glossary-entry'


@Component({
    selector: 'rcc-basic-glossary-entry-label',
    templateUrl: './basic-glossary-entry-label.component.html',
    styleUrls: ['./basic-glossary-entry-label.component.scss'],
    standalone: false
})
export class BasicGlossaryEntryLabelComponent extends ExposeTemplateComponent{

	public activeLanguage: string

	public constructor (
		public glossaryEntry: GlossaryEntry) {
		super()
	}
}
