import	{
			NgModule,
			Provider,
			ModuleWithProviders,
		}											from '@angular/core'

import	{
			provideMainMenuEntry,
			Factory,
			Action,
			WithPosition,
			getEffectivePosition,
		}											from '@rcc/common'

import	{	GlossaryModule				}	from './glossary.module'

@NgModule({
	imports: [
				GlossaryModule,
			],

})

export class GlossaryMainMenuEntryModule {

	/**
	* This method can add entries to the main menu.
	*
	* Calling it without parameter, will add entries to the main menu
	* automatically at reasonably adequate positions.
	*
	* If you want to determine the positions yourself provide a `config` parameter :
	*
	* | .position  | value                  | effect                                                    |
	* |------------|------------------------|-----------------------------------------------------------|
	* | .position  | true                   | adds menu entry at a reasonably adequate position         |
	* |            | positive number        | adds menu entry at position counting from the top         |
	* |            | negative number        | adds menu entry at position counting from the bottom      |
	*
	* Example:     `SymptomCheckMetaStoreServiceMainMenuEntryModule.addEntry({ position: 1 })`,
	*/

	public static addEntry(config?: WithPosition): ModuleWithProviders<GlossaryMainMenuEntryModule> {

		const mainMenuEntry	: Factory<Action> =	{
			deps:		[],
			factory:	() => ({
				label: 			'GLOSSARY.MENU_ENTRY',
				icon: 			'glossary',
				path:			'/medical-glossary',
				position:		getEffectivePosition(config, .5),

			})
		}

		const providers : Provider[] = []

		providers.push(provideMainMenuEntry(mainMenuEntry))

		return {
			ngModule:	GlossaryMainMenuEntryModule,
			providers
		}
	}

}
