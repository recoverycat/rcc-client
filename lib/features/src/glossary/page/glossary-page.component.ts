import	{
			Component,
			Inject,
			OnDestroy,
			OnInit,
			Optional,
		}									from	'@angular/core'
import { FormControl } from '@angular/forms'

import	{
			takeUntil,
			Subject,
			distinctUntilChanged,
			startWith
		}									from	'rxjs'

import	{
			CuratedGlossaryEntryStoreService
		}									from	'../store'

import
		{
			GlossaryEntry,
		}									from	'@rcc/core/src/items/glossary-entry'
import
		{
			BaseRepresentation,
			ITEM_SELECTION_FILTERS,
			ItemGroup,
			ItemSelectionFilter,
			RccTitleService,
			RccTranslationService
		}									from	'@rcc/common'

@Component({
    selector: 'rcc-glossary',
    templateUrl: './glossary-page.component.html',
    styleUrls: ['./glossary-page.component.css'],
    standalone: false
})

export class GlossaryPageComponent implements OnDestroy, OnInit{

	private allGlossaryEntries			: Array<GlossaryEntry>	= []
	private languageFilteredEntries		: Array<GlossaryEntry>	= []
	private alphabeticallySortedEntries	: Array<GlossaryEntry>	= []
	protected itemGroups				: ItemGroup[]			= []

	private destroy$					: Subject<void>			= new Subject<void>()

	public constructor(
		public curatedGlossaryEntryStoreService		: CuratedGlossaryEntryStoreService,
		public rccTranslationService				: RccTranslationService,
		@Optional() @Inject(ITEM_SELECTION_FILTERS)
		private	itemSelectionFilters				: ItemSelectionFilter[],
		private rccTitleService						: RccTitleService
	)	{
		this.rccTranslationService.activeLanguageChange$
		.pipe(
			takeUntil(this.destroy$),
			distinctUntilChanged()
			)
		.subscribe(() => {
			void this.setup()
		})

	}

	public ngOnInit(): void {
		this.rccTitleService.setTitleSuffix('GLOSSARY.TITLE')

	}
	public ngOnDestroy(): void {
		this.destroy$.next()
		this.destroy$.complete()
	}

	private async setup(): Promise<void> {

		await this.curatedGlossaryEntryStoreService.ready

		this.filterControl.valueChanges
		.pipe(
			startWith(''),
			takeUntil(this.destroy$),
			distinctUntilChanged()
		)
		.subscribe(() => this.updateGroups())
	}

	/**
	 * The function returns an array of glossary entries that are filtered by the
	 * active language and sorted alphabetically by title.
	 * @returns an array of GlossaryEntry objects that have been filtered by the
	 * active language and sorted alphabetically by their title.
	 */
	private getLanguageAndAlphabeticallySortedEntries(): GlossaryEntry[] {

		const activeLanguage: string = this.rccTranslationService.activeLanguage

		this.allGlossaryEntries 			=	this.curatedGlossaryEntryStoreService.items
		this.languageFilteredEntries		=	this.allGlossaryEntries
												.filter(entries => entries.language === activeLanguage)
												.filter(entry => this.matchesFilter(entry))
		this.alphabeticallySortedEntries	=	this.languageFilteredEntries
												.sort((a, b) => a.title.localeCompare(b.title))

		return this.alphabeticallySortedEntries

	}

	/**
	 * The function takes an array of titles and returns an array of unique starting
	 * letters from those titles, sorted alphabetically in the active language.
	 * @param {string[]} titles - An array of strings representing titles.
	 * @returns an array of unique starting letters from the given array of titles.
	 * The letters are capitalized and sorted based on the active language.
	 */
	private getAlphabet(titles: string[]): string[] {

		const activeLanguage		: string
									= this.rccTranslationService.activeLanguage
		const allStartingLetters	: string[]
									= titles.map(title => title.charAt(0))

		const uniqueStartingLetters	: string[]
									= Array.from(new Set(allStartingLetters))

		return	uniqueStartingLetters
				.map(letter => letter && letter[0].toUpperCase())
				.sort((a, b) => new Intl.Collator(activeLanguage).compare(a, b))

	}
/**
 * The function `getItemGroup` takes a starting letter and an array of glossary
 * entries, filters the entries that start with the given letter, and returns an
 * item group object containing the filtered entries and a representation object.
 * @param {string} startingLetter - A string representing the starting letter of
 * the title of the glossary entries you want to filter.
 * @param {GlossaryEntry[]} glossaryEntries - An array of objects representing
 * glossary entries. Each object has the following properties:
 * @returns an object of type ItemGroup.
 */

	private getItemGroup(startingLetter: string, glossaryEntries: GlossaryEntry[]): ItemGroup {

		const representation	:	BaseRepresentation
								=	{ label : startingLetter, icon: 'letter' } // letter does not exist yet

		const items				:	GlossaryEntry[]
								=	glossaryEntries
									.filter(entry => entry.title.startsWith(startingLetter))

		const itemGroup			:	ItemGroup
								= 	{ items, representation }

		return itemGroup

	}

	/**
	 * The function "groupByAlphabet" takes an array of glossary entries, extracts the
	 * titles, groups them alphabetically, and returns an array of item groups.
	 * @param {GlossaryEntry[]} glossaryEntries - The `glossaryEntries` parameter is
	 * an array of objects of type `GlossaryEntry`. Each `GlossaryEntry` object has a
	 * `title` property, which is a string representing the title of the entry.
	 * @returns an array of ItemGroup objects.
	 */

	private groupByAlphabet(glossaryEntries: GlossaryEntry[]): ItemGroup[] {

		const titles		: string[]
							= glossaryEntries.map(entry => entry.title)
		const alphabet		: string[]
							= this.getAlphabet(titles)

		const itemGroups	: ItemGroup[]
							= alphabet.map(letter => this.getItemGroup(letter, glossaryEntries))

		return itemGroups

	}

// FILTER

	protected filterControl: FormControl<string> = new FormControl<string>('')

	protected updateGroups(): void {

		this.getLanguageAndAlphabeticallySortedEntries()

		this.itemGroups = this.groupByAlphabet(this.alphabeticallySortedEntries)

	}

	/**
	 * The function matchesFilter checks if a given GlossaryEntry matches a filter
	 * string by comparing the lowercased filter string with the lowercased title and
	 * content of the entry.
	 * @param {GlossaryEntry} entry - The `entry` parameter is an object of type
	 * `GlossaryEntry`. It represents a single entry in a glossary and contains
	 * properties such as `title` and `content`.
	 * @returns a boolean value. It returns true if the filter matches either the
	 * title or the content of the GlossaryEntry, and false otherwise.
	 */
	protected matchesFilter(entry: GlossaryEntry): boolean {
		const filter : string = this.filterControl.value
		if (!filter) return true

		const lowerCaseFilter		: string	= filter.toLowerCase()
		const lowerCaseTitle		: string	= entry.title.toLowerCase()
		const lowerCaseContent		: string[]	= entry.content.map(paragraph => paragraph.toLowerCase())

		const matchesTitle			: boolean	= lowerCaseTitle.includes(lowerCaseFilter)
		const matchesContent		: boolean	= lowerCaseContent.some(paragraph => paragraph.includes(lowerCaseFilter))

		if (matchesTitle) return true
		if (matchesContent) return true

		return false
	}

}
