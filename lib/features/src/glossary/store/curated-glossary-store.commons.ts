import	{	InjectionToken 	}		from '@angular/core'

import	{
			MetaAction
		}							from '@rcc/common'
import	{
			GlossaryEntry,
			GlossaryEntryConfig,
			GlossaryEntryStore
		}							from '@rcc/core/src/items/glossary-entry'

export const GLOSSARY_ENTRY_STORES 			: InjectionToken<GlossaryEntryStore>
											= new InjectionToken<GlossaryEntryStore>('Glossary Entry Stores')
export const GLOSSARY_ENTRY_META_ACTIONS	: InjectionToken<GlossaryEntryStore>
											= new InjectionToken<MetaAction<GlossaryEntryConfig, GlossaryEntry, GlossaryEntryStore>>('metaAction, any action related to an item type.')
