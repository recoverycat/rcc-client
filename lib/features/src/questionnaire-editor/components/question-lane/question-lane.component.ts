import	{
			AsyncPipe,
			NgForOf,
			NgIf
		}										from '@angular/common'
import	{
			ChangeDetectionStrategy,
			ChangeDetectorRef,
			Component,
			ElementRef,
			Input,
			NgZone,
			OnDestroy,
			OnInit,
			QueryList,
			Type,
			ViewChild,
			ViewChildren
		}										from '@angular/core'
import	{
			TranslationsModule,
			RccCardComponent,
			WidgetsModule,
			HandlerAction,
			RccModalController,
			RccTranslationService,
			RccSliderModule,
			RccSliderComponent,
			RccIconComponent,
			SharedModule,
			RccAlertController,
		}										from '@rcc/common'
import	{
			Question,
			QuestionConfig,
			Schedule,
			uuidv4
		}										from '@rcc/core'
import	{
			QueryControl
		}										from '@rcc/features/queries'
import	{
			RccThemeModule
		}										from '@rcc/themes/active'
import	{
			first,
			firstValueFrom,
			forkJoin,
			fromEvent,
			map,
			Observable,
			of,
			Subject,
			switchMap,
			take,
			tap,
			throttleTime
		}										from 'rxjs'
import	{
			AddSelectedQuestionsFormData,
			AddQuestionModalComponent,
			AddQuestionFormData
		}										from '../../modal/add-question/add-question-modal.component'
import	{
			FormData as EditQuestionFormData,
			EditQuestionModalComponent,
		}										from '../../modal/edit-question/edit-question-modal.component'
import	{
			EditMedicationModalComponent,
		}										from '../../modal/edit-medication/edit-medication-modal.component'
import	{
			CategorizedQuestionWithSchedule,
			isCategorizedQuestionWithSchedule,
			RccQuestionnaireEditorService
		}										from '../../questionnaire-editor.service'

export interface DragAndDropPayload {
	questionId: string
	fromCategory: string
	fromPosition: number
}

@Component({
    selector: 'rcc-question-lane',
    templateUrl: './question-lane.component.html',
    styleUrls: ['./question-lane.component.scss'],
    imports: [
        NgIf,
        NgForOf,
        AsyncPipe,
        TranslationsModule,
        RccCardComponent,
        WidgetsModule,
        RccSliderModule,
        RccThemeModule,
        RccIconComponent,
        SharedModule,
    ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class RccQuestionLaneComponent implements OnDestroy, OnInit {
	private _category: string | null = null
	@Input()
	public set category(value: string | null) {
		this._category = value

		if (value === 'undefined')
			return

		this.categorizedQuestions$ = this.questionnaireEditorService
			.getQuestionsByCategory$(value)
			.pipe(
				tap((questions: CategorizedQuestionWithSchedule[]) => {
					this.hasQuestions = questions.length > 0
					this.isEntireContainerVisible()
				})
			)
	}

	public get category(): string | null {
		return this._category
	}


	@Input() public mode			: 'edit' | 'preview' | 'init'	= 'edit'

	public _questionListContainer: ElementRef<HTMLDivElement>
	@ViewChild('questionListContainer')
	public set questionListContainer(value: ElementRef<HTMLDivElement>) {
		this._questionListContainer = value

		// To prevent constant change detection cycles being triggered, we manually attach
		// the dragover events here. We need to take care to ensure actual changes are
		// rendered when necessary.
		this.ngZone.runOutsideAngular(() => {
			this._questionListContainer?.nativeElement.addEventListener('dragover', (event: DragEvent) => {
				if (this.allowDragAndDrop )
					this.allowDrop(event)
				
			})
		})
	}

	public get questionListContainer(): ElementRef<HTMLDivElement> {
		return this._questionListContainer
	}

	@ViewChildren('slideRef', { read: ElementRef })
	public set slideElement(value: QueryList<ElementRef<HTMLElement>>) {
		value.forEach(slide => {
			const questionId: string = slide.nativeElement.getAttribute('data-question-id')

			// To prevent constant change detection cycles being triggered, we manually attach
			// the dragover events here. We need to take care to ensure actual changes are
			// rendered when necessary.
			this.ngZone.runOutsideAngular(() => {
				fromEvent(slide.nativeElement, 'dragover').pipe(
					throttleTime(160),
				).subscribe((event: DragEvent) => {
					this.onDragOverSlide(event, questionId)
				})
			})
		})
	}

	@ViewChildren('cardRef')
	public questionCards: QueryList<ElementRef<HTMLElement>>


	private _slider: RccSliderComponent

	protected entireSetOfQuestionsVisible: boolean = true
	protected trackQuestion(_: number, question: CategorizedQuestionWithSchedule): string {
		return question.question.id
	}

	@ViewChild(RccSliderComponent)
	private set slider(value: RccSliderComponent) {
		this._slider = value
		if (value != null)
			value.restoreState = () => {
				this.slider?.slideTo(0)
			}
	}

	private get slider(): RccSliderComponent {
		return this._slider
	}

	public categorizedQuestions$	: Observable<CategorizedQuestionWithSchedule[]>
									= of([] as CategorizedQuestionWithSchedule[])


	private hasQuestions			: boolean						= false

	protected queryControls			: Map<Question, QueryControl>	= new Map<Question, QueryControl>()

	// #region getters

	public get addQuestion(): HandlerAction {
		return {
			label:		`QUESTIONNAIRE_EDITOR.${this.category.toUpperCase()}.BUTTON`,
			category: 	'create',
			icon:		this.category,
			handler:    async () => {
							await this.addCatalogueOrCustomQuestion()
						}
		} as HandlerAction
	}

	public get hide(): boolean {
		return this.mode === 'preview' && !this.hasQuestions
	}

	// #endregion

	public constructor(
		public readonly questionnaireEditorService: RccQuestionnaireEditorService,
		private readonly rccModalController: RccModalController,
		private readonly translationService: RccTranslationService,
		private readonly rccAlertController: RccAlertController,
		private ngZone: NgZone,
		private readonly changeDetectorRef: ChangeDetectorRef
	) {
	}

	private destroy$: Subject<void> = new Subject()
	public ngOnDestroy(): void {
		this.destroy$.next()
		this.destroy$.complete()
	}

	public allowDragAndDrop: boolean

	public ngOnInit(): void {
		this.allowDragAndDrop = this.questionnaireEditorService.allowDragAndDrop(this.category)
	}

	// #region adding questions

	private async addCatalogueOrCustomQuestion() : Promise<void> {
		const result : AddSelectedQuestionsFormData = await firstValueFrom<AddSelectedQuestionsFormData>(
			this.getAddQuestionModalOptions$()
				.pipe(switchMap((options) => this.rccModalController.present<AddQuestionModalComponent, AddSelectedQuestionsFormData>(
						AddQuestionModalComponent,
						options,
					)
				))
			)

		if (result === null)
			return

		// adding a custom question is mutually exclusive
		// with adding/removing questions from the catalogue
		if(result.customQuestion) {
			this.questionnaireEditorService.addQuestion(
				result.customQuestion,
				this.category,
				result.schedule
			)
			return
		}

		const selectedIds		: Set<string> 	= new Set(result.selectedQuestions)
		const currentlySelected	: Set<string> 	= new Set(await firstValueFrom(this.getSelectedQuestionIds$(this.category)))

		const toRemove			: string[] 		= [...currentlySelected].filter((id) => !selectedIds.has(id))
		const toAdd				: string[] 		= [...selectedIds].filter((id) => !currentlySelected.has(id))

		this.questionnaireEditorService.addMultipleQuestions(toAdd, this.category)
		this.questionnaireEditorService.removeMultipleQuestions(toRemove, this.category)
	}

	private getAddQuestionModalOptions$(): Observable<object> {
		return forkJoin({
			disabledIds						: 	this.getDisabledQuestionIds$(this.category),
			selected						: 	this.getSelectedQuestionIds$(this.category),
			questions						: 	of(this.questionnaireEditorService.catalogue),
			category						: 	of(this.category),
			baseScheduleDays				: 	this.questionnaireEditorService.baseSchedule$.pipe(
													take(1),
													map((baseSchedule: Schedule) => baseSchedule.daysOfWeek)
												),
			catalogRestrictionsByCategory 	:	of(this.questionnaireEditorService.catalogRestrictionsByCategory),
		})
	}

	private getDisabledQuestionIds$(category: string|null): Observable<string[]> {
		// When a user selects questions from a given category (A), then opens
		// another category (B), the questions that were previously selected
		// in A should show up in B, marked as selected, but should be disabled
		// so that the user can't reselect them, or disable them. This functionality
		// may be revised at a later date
		return this.questionnaireEditorService.questions$
			.pipe(
				first(),
				map(
					(questions: CategorizedQuestionWithSchedule[]) =>
					questions
						.filter((item: CategorizedQuestionWithSchedule) => item.category !== category)
						.map((item: CategorizedQuestionWithSchedule) => item.question.id)
				)
			)
	}

	private getSelectedQuestionIds$(category: string | null): Observable<string[]> {
		return this.questionnaireEditorService.getQuestionsByCategory$(category)
			.pipe(
				first(),
				map((questions: CategorizedQuestionWithSchedule[]) =>
					new Set(questions.map((item: CategorizedQuestionWithSchedule) => item.question.id))
				),
				map((questionIds: Set<string>) => [...questionIds])
			)
	}

	// #endregion


	// #region editing questions
	protected getEditAction(categorizedQuestion : CategorizedQuestionWithSchedule): HandlerAction {
		return {
			icon: 'edit',
			handler: async () => this.editQuestion(categorizedQuestion),
			label: this.translationService.translate('EDIT') + ': ' + categorizedQuestion.question.config.translations[this.translationService.activeLanguage]
		}
	}

	private async editQuestion(categorizedQuestion : CategorizedQuestionWithSchedule): Promise<void> {
		const question 		: 	Question 	= categorizedQuestion.question
		const previousId 	: 	string 		= question.id

		const component: Type<unknown> = this.category === 'medication' ? EditMedicationModalComponent : EditQuestionModalComponent

		const result : EditQuestionFormData | string = await this.rccModalController.present(
			component,
			{
				question,
				category			: this.category,
				schedule			: categorizedQuestion.schedule,
				baseScheduleDays	: this.questionnaireEditorService.baseScheduleDays
			}
		)

		// EditQuestionModalComponent has a "Question Catalogue" button that dismisses
		// the modal and returns string 'question-list' when clicked. If that is the case
		// the question catalogue modal should open instead.
		if (result === 'question-list') {
			void this.addCatalogueOrCustomQuestion()
			return
		}

		if(result === null || typeof result === 'string') return

		this.questionnaireEditorService.updateQuestion(
			previousId,
			{
				question	: 	this.questionFormDataToQuestionConfig(uuidv4(), result),
				schedule	: 	result.schedule,
				category	: 	this.category
			}
		)
	}

	private questionFormDataToQuestionConfig(id: string, formData: EditQuestionFormData | AddQuestionFormData): QuestionConfig {
		const language	: string = this.translationService.activeLanguage
		return {
			id,
			meaning			:	formData.questionTitle,
			translations	:	'translations' in formData ? formData.translations : {
									[language]: formData.questionTitle
								},
			options			:	formData.options,
			type			:	formData.answerType,
			tags			:	formData.tags,
			min				:	formData.min,
			max				:	formData.max,
			unit			:	'unitText' in formData ? formData.unitText : undefined
		}
	}
	// #endregion

	protected questionHeading(question: Question): string {
		const questionName: string = this.translationService.translate(question)
		const unitText: string = question.unit

		if (!unitText)
			return questionName

		return `${questionName} (${unitText})`
	}


	// #region removing questions
	protected getRemoveAction(question: CategorizedQuestionWithSchedule):  HandlerAction {
		return {
			label: this.translationService.translate('REMOVE') + ': ' + question.question.config.translations[this.translationService.activeLanguage],
			icon: 'delete_circle',
			handler: () => { this.removeQuestion(question).catch(err => console.error(err)) }
		}
	}
	private async removeQuestion(question: CategorizedQuestionWithSchedule | Question | string): Promise<void> {

		await this.confirmDeleteQuestion()

		if(isCategorizedQuestionWithSchedule(question))
			this.questionnaireEditorService.removeQuestion(question)
		else
			this.questionnaireEditorService.removeQuestion(question, this.category)
	}

	private async confirmDeleteQuestion(): Promise<void> {

		const message	: string
						= 'QUESTIONNAIRE_EDITOR.DELETE_QUESTION_ALERT.HEADER'

		await this.rccAlertController.present({

			message,
			cssClass: 	'rcc-theme',
			buttons:	[
							{
								label:		'NO',
								rejectAs:	'dismiss'
							},
							{
								label:		'YES',
								resolveAs:	'confirm'
							}
						]
		})
	}

	/**
	 * This method is used to determine if all questions in the container are visible
	 * in the viewport. It uses a timeout to wait for the DOM to update when switching
	 * between the edit and preview mode as an additional card is added for adding a
	 * new question
	 */
	protected isEntireContainerVisible(): void {
		setTimeout(() => {

			if (!this.questionListContainer) return

			const rccCards: NodeListOf<HTMLElement>
				= this.questionListContainer.nativeElement.querySelectorAll('rcc-card')

			const containerRect: DOMRect
				= this.questionListContainer.nativeElement.getBoundingClientRect()

			const allCardsVisible: boolean = Array.from(rccCards).every(card => {
				const cardRect: DOMRect = card.getBoundingClientRect()

				return (
					cardRect.top >= containerRect.top &&
					cardRect.left >= containerRect.left &&
					cardRect.bottom <= containerRect.bottom &&
					cardRect.right <= containerRect.right
				)
			})
			this.entireSetOfQuestionsVisible = allCardsVisible

		}, 1)

	}

	// #endregion

	public getQueryControl(question: Question): QueryControl {
		const queryControl	:	QueryControl
							=	this.queryControls.get(question)
								||
								new QueryControl(
									question,
									() => undefined, // Do not do anything on submission
									() => undefined // Do not do anything on clean up
								)
		this.queryControls.set(question, queryControl)
		queryControl.disableForms()
		return queryControl
	}

	// #region drag and drop

	/**
	 *  This region is dedicated to the HCP  monitoring setup drag and drop
	 * 	functionality. For the future it may be wise to move this to its own
	 * 	separate service. There are various different events/states to look out for.
	 *
	 * 1. Start.
	 * 2. Hover. 2a. Enter 2b. Leave
	 * 3. Drop.
	 * 4. End.
	 *
	 *	2a and 2b are especially important for either hovering
	 *	on the higher level: over an encompassing question-container div element.
	 *	on the lower level: over an rcc-slide / rcc-card (these two are dealt together with
	 *	as you actually move the whole rcc-slide but you see only the rcc-card
	 *	being moved)
	 *
	 *  These states are used to track the drag and drop process.
	 * 	For more clarity on this best check the template where the different functions
	 * 	for each state and element are linked to the belonging native (drag) event.
	 *
	 * TODO: It is not yet possible to drag a card from one category to another +
	 *	also dropping it at a certain position in the other category. Changing this would
	 *	additionally require changing the visual cue.
	 */


	// #subregion 1. start

	/**
	 * Controls the drag start state with a brief visual feedback.
	 *
	 * @property startDragging
	 * @property _startDragging - Private flag to manage the start of a drag operation.
	 * @getter Returns the current drag start state.
	 * @setter Sets the drag start state and automatically resets it after 1ms if set to true.
	 *         This creates a brief visual effect for drag initiation.
	 */

	private _startDragging	: boolean = false

	public get startDragging(): boolean {
		return this._startDragging
	}

	public set startDragging(value: boolean) {
		this._startDragging = value

		if (value === true)
			setTimeout(() => {
				this._startDragging = false
			}, 1)
	}

	/**
	 * Handles the start of a drag operation for a question in the questionnaire editor.
	 *
	 * @property isDragSource - Indicates if this lane is the source of the current drag operation.
	 * @param $event - The DragEvent object triggered when the drag starts.
	 * @param question - The CategorizedQuestionWithSchedule object being dragged.
	 *
	 * This method performs the following actions:
	 * 1. Sets the startDragging flag to true (which is reset after a short timeout).
	 * 2. Marks this component (the specific question lane belonging to a category)
	 * 	as the drag source.
	 * 3. Retrieves the current list of questions for the category.
	 * 4. Finds the position of the dragged question in the list.
	 * 5. Creates a payload with the question's ID, category, and position.
	 * 6. Sets this payload as the drag data in a stringified JSON format.
	 *
	 * The payload is used to track the original position and category of the dragged question,
	 * which is crucial for handling the drop event correctly, especially when moving
	 * questions between different categories.
	 */

	public isDragSource: boolean = false


	public onDragStart($event: DragEvent, question: CategorizedQuestionWithSchedule): void {
		this.startDragging = true

		this.isDragSource = true
		// Get the array of questions for the current category
		this.questionnaireEditorService.getQuestionsByCategory$(this.category)
			.pipe(
				take(1), // Take only the first emission
				map((questions: CategorizedQuestionWithSchedule[]) => {
					const fromPosition: number = questions.findIndex(q => q.question.id === question.question.id)

					const payload: DragAndDropPayload = {
						questionId: question.question.id,
						fromCategory: this.category,
						fromPosition: fromPosition
					}

					$event.dataTransfer.setData('text/plain', JSON.stringify(payload))
				})
			)
			.subscribe()
	}

	// #subregion 2. hover

	// #subregion 2. hover, higher level over question-container

	// @property dragHover - Indicates if a dragged item is hovering over the question lane.
	public dragHover: boolean = false

	public onDragEnterLane(): void {
		this.dragHover = true
	}

	/**
	 * Handles the drag leave event for the question lane container.
	 *
	 * @param event - The DragEvent object containing information about the drag operation.
	 *
	 * This method determines if the dragged item has actually left the question lane
	 * or if it's just entered a child element within the lane. It does this by:
	 *
	 * 1. Identifying the element being entered (relatedTarget).
	 * 2. Checking if this element is a child of the question lane container.
	 * 3. If it's not a child, it sets dragHover to false, removing the hover effect.
	 * 4. If it is a child, it does nothing, maintaining the hover effect.
	 *
	 * This approach prevents the hover effect from flickering when dragging over
	 * child elements within the question lane.
	 */
	public onDragLeaveLane(event: DragEvent): void {
		// Because the dragleave event gets fired when the user hovers over children
		// of the question lane, we need to check whether the new element that's being
		// dragged over is a child of the question lane. If it is we retain the hover
		// effect, otherwise the user has left the question lane, and we can remove
		// the hover effect
		const newElementBeingEntered: HTMLElement = event.relatedTarget as HTMLElement
		const isChildOfQuestionLane: boolean = this.questionListContainer.nativeElement.contains(newElementBeingEntered)

		if (isChildOfQuestionLane) return

		this.currentDragOverId = undefined
		this.dragHover = false

	}

	// #subregion 2. hover, lower level over rcc-card / rcc-slide

	/**
	 * Handles the drop event for a question card.
	 */
	public currentDragOverId: string

	/**
	 * This method updates the drop zone indicators based on the current drag event
	 * via a throttled version of the dragOverSubject. The issue is that the dragOver
	 * event is continually triggered when the user hovers over an element which
	 * causes a lot of unnecessary calculations.
	 * @param event
	 * @param questionId
	 */

	public onDragOverSlide(event: DragEvent, questionId: string): void {
		const { isRightHalf, isLeftHalf } = this.updateDropZone(event)

		const hasChanges: boolean =
			isRightHalf !== this.isRightHalf ||
			isLeftHalf !== this.isLeftHalf ||
			questionId !== this.currentDragOverId

		if (!hasChanges)
			return
		
		this.ngZone.run(() => {
			this.isRightHalf = isRightHalf
			this.isLeftHalf = isLeftHalf
			this.currentDragOverId = questionId
			this.changeDetectorRef.markForCheck()
		})
	}

	/**
	 * This method updates the drop zone indicators throttled based on the
	 * current drag event
	 */
	public handleThrottledDragOver({ event, questionId }: { event: DragEvent, questionId: string }): void {
		if (this.currentDragOverId === questionId) this.updateDropZone(event)
	}

	/** Flags if the drag is over the right half of a question card.  */
	public isRightHalf: boolean = false
	/** Flags if the drag is over the left half of a question card. */
	public isLeftHalf: boolean = false

	// #subregion 3. drop

	/**
	 * Enables dropping of dragged elements by preventing the default behavior
	 * and setting the drop effect to 'move'.
	 *
	 * @param event - The DragEvent object for the current drag operation.
	 *
	 * This method is typically used as an event handler for the 'dragover' event
	 * on drop target elements. It allows the browser to accept the drop and
	 * indicates that the dragged item will be moved to the new location.
	 *
	 * Check the template to see how it is being used on different elements.
	 */
	public allowDrop(event: DragEvent): void {
		event.preventDefault()
		event.dataTransfer.dropEffect = 'move'
	}

	/**
	 *  During the drag and hover, these two functions isLeftDropZone and
	 * 	isRightDropZone specify where the card will be dropped to then adjust
	 *  the CSS styling for the drop indicator to be shown.
	 */

	public isLeftDropZone(questionId: string): boolean {
		return this.currentDragOverId === questionId && this.isLeftHalf && !this.isRightHalf
	}

	public isRightDropZone(questionId: string): boolean {
		return this.currentDragOverId === questionId && this.isRightHalf && !this.isLeftHalf
	}

	/**
	 * Updates the drop zone indicators based on the mouse position during a drag event.
	 *
	 * @param event - The DragEvent object containing information about the current drag operation.
	 * @param element - The CSS selector for the draggable element, defaults to 'rcc-card'.
	 *
	 * This method:
	 * 1. Finds the target card element within the current slide.
	 * 2. Calculates a threshold at 60% of the card's width. This allows for the user to
	 *	drag the mouse with the element directly over the middle of the card and
	 *  on dropping it having it switch position with that card.
	 * 3. Sets isRightHalf to true if the mouse is beyond this threshold, false otherwise.
	 * 4. Sets isLeftHalf to the opposite of isRightHalf.
	 *
	 * These boolean flags are used to determine where the dragged item will be dropped
	 * relative to the current card (before or after), and to update the visual indicators.
	 *
	 * TODO: For the future it would be good to refactor so that the determination
	 * of the drop position and the setting of the css styling to be decided on a
	 * single pair of variables instead of using
	 * 2 functions ( isLeftDropZone & isRightDropZone) and 2 variables (isLeftHalf & isRightHalf).
	 */

	private updateDropZone(event: DragEvent, element: string = 'rcc-card'): { isRightHalf: boolean, isLeftHalf: boolean } {
		const slideElement: HTMLElement = event.currentTarget as HTMLElement
		const cardElement: HTMLElement = slideElement.querySelector(element)

		let isRightHalf: boolean = false
		let isLeftHalf: boolean = false

		try {
			let rect: DOMRect = this.lastRect

			// Check if the cardElement is the same as the last one
			if (cardElement === this.lastCardElement && this.lastRect) rect = this.lastRect
			else {
				rect = cardElement.getBoundingClientRect()
				// Cache the new cardElement and rect
				this.lastCardElement = cardElement
				this.lastRect = rect
			}
			
			const threshold: number = rect.left + rect.width * 0.6
			isRightHalf = event.clientX > threshold
			isLeftHalf = !isRightHalf
		} catch (error) {
			console.error('Error updating drop zone:', error)
		}

		return {
			isRightHalf,
			isLeftHalf,
		}
	}

	public lastCardElement: HTMLElement | null = null
	public lastRect: DOMRect | null = null

	/**
	 *  Calculates the target drop position for a dragged element.
	 *  The position is incremented by 1 if the drop occurs in the
	 * 	right half of the target element.
	 */

	public getDropPosition(event: DragEvent): number {
		this.dragHover = false

		const targetElement: HTMLElement = event.target as HTMLElement
		if (!targetElement || !targetElement.parentElement) return 0

		let targetPosition: number = Array.from(targetElement.parentElement.children).indexOf(targetElement)

		if (this.isRightHalf) return targetPosition += 1

		return targetPosition

	}

	/**
	 * Handles the drop event when a question is dragged and dropped within the questionnaire editor.
	 *
	 * @property droppedId - Stores the ID of the question where the dragged item was dropped.

	 * @param event - The DragEvent object containing information about the drop operation.
	 *
	 * This method:
	 * 1. Calculates the target position for the dropped question.
	 * 2. Retrieves and parses the drag data containing the question's original position and category.
	 * 3. Reorders the question within the same category or moves it to a new category.
	 * 4. Updates the UI to reflect the new question order.
	 * 5. Resets drag-related states and CSS classes.
	 *
	 * The method uses requestAnimationFrame to ensure smooth UI updates after the drop operation.
	 * It also includes error handling for parsing the drag payload.
	 *
	 * TODO: For the future, it might be beneficial to generalize the function to not only
	 * work with rcc-cards as drag and droppable elements
	 */

	public droppedId: string

	// Variable to bypass default slider behavior to recognize the drag-and-drop operation

	public isDropping: number = undefined

	// Variable to pass the targetPosition to the slider setter

	protected targetPosition: number | null = null

	public onDrop(event: DragEvent): void {

		try {
			this.targetPosition = this.getDropPosition(event)

			const data: string = event.dataTransfer.getData('text/plain')

			const slideElement: HTMLElement = event.currentTarget as HTMLElement
			const cardElement: HTMLElement = slideElement.querySelector('rcc-card')

			if (!data) return

			const payload: DragAndDropPayload = JSON.parse(data) as DragAndDropPayload
			this.dragHover = false

			this.droppedId = payload.questionId

			requestAnimationFrame(() => {
					if (this.category === payload.fromCategory)
						this.questionnaireEditorService.reorderQuestion(
							this.category,
							payload.fromPosition,
							this.targetPosition
						)
					else this.questionnaireEditorService.moveQuestion(
						payload.questionId,
						this.category,
						this.targetPosition
					)

				this.currentDragOverId = undefined

				cardElement.classList.remove('left-drop-zone', 'right-drop-zone')
		})
		} catch (error) {
			console.error('Error parsing drag payload:', error)
		}
	}

	// #subregion 4. End
	public onDragEnd(): void {
		this.isDragSource = false
	}

	/**
	 * Cleans up the drag-related states when the drag operation is completed.
	 */
	public dragCleanUp(): void {

		this.currentDragOverId = null
		this.lastCardElement = null
		this.lastRect = null
	}

	// #endregion

}
