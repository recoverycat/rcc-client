import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing'
import { DragAndDropPayload, RccQuestionLaneComponent } from './question-lane.component'
import { CategorizedQuestionWithSchedule, MONITORING_SETUP_CONFIG, MonitoringSetupConfig, RccQuestionnaireEditorService } from '../../questionnaire-editor.service'
import { Question } from '@rcc/core'
import { RccAlertController, RccModalController, RccStorage, RccToastController } from '@rcc/common'
import { QuestionnaireService } from '../../../questions'
import { Observable, of } from 'rxjs'
import { ElementRef } from '@angular/core'
import { FallbackQueryWidgetsModule } from '../../../queries'

describe('RccQuestionLaneComponent', () => {
	let component: RccQuestionLaneComponent = undefined!
	let fixture: ComponentFixture<RccQuestionLaneComponent> = undefined!
	const rccModalController: RccModalController = undefined!
	const rccAlertController: RccAlertController = undefined!
	const rccToastController: RccToastController = undefined!

	/* eslint-disable */
	const mockRccStorage= {
		createItemStorage: jasmine.createSpy('createItemStorage').and.returnValue({
			getAll: jasmine.createSpy('getAll').and.returnValue(Promise.resolve([])),
			store: jasmine.createSpy('store').and.returnValue(Promise.resolve())
		}),
	}
	/* eslint-enable */

	const mockQuestion1: CategorizedQuestionWithSchedule = {
		question: new Question({
			id: '001',
			type: 'integer',
			meaning: 'Rate your pain level',
			translations: {
				en: 'Rate your pain level',
				de: 'Bewerten Sie Ihr Schmerzniveau'
			},
			options: [
				{ value: 0, translations: { en: 'No pain', de: 'Keine Schmerzen' } },
				{ value: 1, translations: { en: 'Mild', de: 'Leicht' } },
				{ value: 2, translations: { en: 'Moderate', de: 'Mäßig' } },
				{ value: 3, translations: { en: 'Severe', de: 'Stark' } },
			]
		}),
		category: 'Symptoms',
		schedule: null
	}


	const mockQuestion2: CategorizedQuestionWithSchedule = {
		question: new Question({
			id: '002',
			type: 'boolean',
			meaning: 'Did you take your medication today?',
			translations: {
				en: 'Did you take your medication today?',
				de: 'Haben Sie heute Ihre Medikamente genommen?'
			},
			options: [
				{ value: true, translations: { en: 'Yes', de: 'Ja' } },
				{ value: false, translations: { en: 'No', de: 'Nein' } }
			]
		}),
		category: 'Symptoms',
		schedule: null
	}

	const mockQuestion3: CategorizedQuestionWithSchedule = {
		question: new Question({
			id: '003',
			type: 'string',
			meaning: 'How would you describe your mood today?',
			translations: {
				en: 'How would you describe your mood today?',
				de: 'Wie würden Sie Ihre Stimmung heute beschreiben?'
			},
			options: [
				{ value: 'happy', translations: { en: 'Happy', de: 'Glücklich' } },
				{ value: 'neutral', translations: { en: 'Neutral', de: 'Neutral' } },
				{ value: 'sad', translations: { en: 'Sad', de: 'Traurig' } },
				{ value: 'anxious', translations: { en: 'Anxious', de: 'Ängstlich' } }
			]
		}),
		category: 'Symptoms',
		schedule: null
	}

	const mockQuestion4: CategorizedQuestionWithSchedule = {
		question: new Question({
			id: '004',
			type: 'boolean',
			meaning: 'Did you take your medication today?',
			translations: {
				en: 'Did you take your medication today?',
				de: 'Haben Sie heute Ihre Medikamente eingenommen?'
			}
		}),
		category: 'Symptoms',
		schedule: null
	}

	const mockQuestion5: CategorizedQuestionWithSchedule = {
		question: new Question({
			id: '005',
			type: 'boolean',
			meaning: 'Did you take your medication today?',
			translations: {
				en: 'Did you take your medication today?',
				de: 'Haben Sie heute Ihre Medikamente eingenommen?'
			}
		}),
		category: 'Symptoms',
		schedule: null
	}

	const mockQuestion6: CategorizedQuestionWithSchedule = {
		question: new Question({
			id: '004',
			type: 'boolean',
			meaning: 'Did you take your medication today?',
			translations: {
				en: 'Did you take your medication today?',
				de: 'Haben Sie heute Ihre Medikamente eingenommen?'
			}
		}),
		category: 'test-category',
		schedule: null
	}

	const mockQuestion7: CategorizedQuestionWithSchedule = {
		question: new Question({
			id: '005',
			type: 'boolean',
			meaning: 'Did you take your medication today?',
			translations: {
				en: 'Did you take your medication today?',
				de: 'Haben Sie heute Ihre Medikamente eingenommen?'
			}
		}),
		category: 'test-category',
		schedule: null
	}

	const mockQuestions: CategorizedQuestionWithSchedule[] = [
		mockQuestion1, mockQuestion2, mockQuestion3, mockQuestion4, mockQuestion5, mockQuestion6, mockQuestion7
	]

	const mockMonitoringSetupConfig: MonitoringSetupConfig = {
		questionCategories: ['category1', 'category2'],
		hiddenOnEditCategories: [],
		hiddenInCatalogue: [],
		defaultQuestionsByCategory: new Map(),
		addTemplateQuestionsTo: null,
		defaultScheduleConfig: {
			startDate: '2024-06-30',
			interval: [1],
		},
		catalogRestrictionsByCategory: {}
	}


	beforeEach(async () => {

		await TestBed.configureTestingModule({
			imports: [RccQuestionLaneComponent, FallbackQueryWidgetsModule],
			providers: [
				RccQuestionnaireEditorService,
				QuestionnaireService,
				{ provide: MONITORING_SETUP_CONFIG, useValue: mockMonitoringSetupConfig },
				{ provide: RccStorage, useValue: mockRccStorage },
				{ provide: RccModalController, useValue: rccModalController },
				{ provide: RccAlertController, useValue: rccAlertController },
				{ provide: RccToastController, useValue: rccToastController }
			],
			teardown: { destroyAfterEach: false }

		})
		.compileComponents()
	})

	beforeEach(() => {

		fixture = TestBed.createComponent(RccQuestionLaneComponent)
		component = fixture.componentInstance

		component.questionnaireEditorService.addMultipleQuestions(mockQuestions)

		component.category = 'Symptoms'

		fixture.detectChanges()
	})

	describe('onDragStart', () => {
		it('should set startDragging to true and set drag data when onDragStart is called', () => {
			const mockDataTransfer: {
				setData: jasmine.Spy<jasmine.Func>
				effectAllowed: string;
			} = {
				setData: jasmine.createSpy('setData'),
				effectAllowed: ''
			}
			const mockEvent: DragEvent = {
				dataTransfer: mockDataTransfer
			} as unknown as DragEvent

			component.category = 'Symptoms'
			component.onDragStart(mockEvent, mockQuestion1)

			expect(component.startDragging).toBe(true)
			expect(component['isDragSource']).toBe(true)
			expect(mockDataTransfer.setData).toHaveBeenCalledWith('text/plain', jasmine.any(String))

			const setDataArg: {
				questionId: string;
				fromCategory: string;
				fromPosition: number;
			} = JSON.parse(mockDataTransfer.setData.calls.mostRecent().args[1] as string) as {
				questionId: string;
				fromCategory: string;
				fromPosition: number;
			}

			expect(setDataArg).toEqual(jasmine.objectContaining({
				questionId: '001',
				fromCategory: 'Symptoms',
				fromPosition: 0
			}))

		})

		it('should set isDragSource to true when onDragStart is called', () => {
			const mockDragEvent: DragEvent = {
				dataTransfer: {
					setData: jasmine.createSpy('setData'),
					effectAllowed: ''
				},
				target: document.createElement('div')
			} as unknown as DragEvent

			spyOn(component['questionnaireEditorService'], 'getQuestionsByCategory$').and.returnValue(of([mockQuestion1]))

			component.onDragStart(mockDragEvent, mockQuestion1)

			expect(component['isDragSource']).toBe(true)
		})

		it('should call questionnaireEditorService.getQuestionsByCategory$ with the correct category', () => {
			const mockDragEvent: DragEvent = {
				dataTransfer: {
					setData: jasmine.createSpy('setData'),
					effectAllowed: ''
				},
				target: document.createElement('div')
			} as unknown as DragEvent

			const mockCategory: string = 'Medication'

			const getQuestionsByCategorySpy: jasmine.Spy<(category?: string | null) => Observable<CategorizedQuestionWithSchedule[]>> = spyOn(component.questionnaireEditorService, 'getQuestionsByCategory$').and.returnValue(of([mockQuestion1]))

			component.category = mockCategory
			component.onDragStart(mockDragEvent, mockQuestion1)

			expect(getQuestionsByCategorySpy).toHaveBeenCalledWith(mockCategory)
		})

		it('should correctly find the index of the dragged question in the questions array', () => {

			const draggedQuestion: CategorizedQuestionWithSchedule = mockQuestions[1]
			const setDataSpy: jasmine.Spy<jasmine.Func> = jasmine.createSpy('setData')
			const mockEvent: DragEvent = {
				dataTransfer: {
					setData: setDataSpy,
					effectAllowed: ''
				}
			} as unknown as DragEvent

			component.category = 'Symptoms'
			component.onDragStart(mockEvent, draggedQuestion)

			expect(component.startDragging).toBe(true)
			expect(component['isDragSource']).toBe(true)
			expect(setDataSpy).toHaveBeenCalledWith('text/plain', jasmine.any(String))

			const setDataArg: { questionId: string; fromCategory: string; fromPosition: number }
							= JSON.parse(setDataSpy.calls.mostRecent().args[1] as string) as {
				questionId: string;
				fromCategory: string;
				fromPosition: number;
			}

			expect(setDataArg.questionId).toBe('002')
			expect(setDataArg.questionId).not.toBe('001')
			expect(setDataArg.fromCategory).toBe('Symptoms')
			expect(setDataArg.fromCategory).not.toBe('Medication')
			expect(setDataArg.fromPosition).toBe(1)
			expect(setDataArg.fromPosition).not.toBe(0)

		})

		it('should create a payload with the correct questionId, fromCategory, and fromPosition', () => {
			const mockEvent: DragEvent = new DragEvent('dragstart')
			const setDataSpy: jasmine.Spy<jasmine.Func> = jasmine.createSpy('setData')
			Object.defineProperty(mockEvent, 'dataTransfer', {
				value: {
					setData: setDataSpy
				}
			})

			component.category = 'Test Category'

			spyOn(component['questionnaireEditorService'], 'getQuestionsByCategory$').and.returnValue(of(mockQuestions))

			component.onDragStart(mockEvent, mockQuestions[0])

			expect(component.startDragging).toBe(true)
			expect(component['isDragSource']).toBe(true)

			expect(setDataSpy).toHaveBeenCalledWith('text/plain', JSON.stringify({
				questionId: '001',
				fromCategory: 'Test Category',
				fromPosition: 0
			}))

			setDataSpy.calls.reset()
			component.startDragging = false
			component['isDragSource'] = false

			component.onDragStart(mockEvent, mockQuestions[4])

			expect(component.startDragging).toBe(true)
			expect(component['isDragSource']).toBe(true)

			expect(setDataSpy).toHaveBeenCalledWith('text/plain', JSON.stringify({
				questionId: '005',
				fromCategory: 'Test Category',
				fromPosition: 4
			}))

		})

		it('should handle the case when the dragged question is not found in the questions array', () => {
			interface DragPayload {
				questionId: string
				fromCategory: string
				fromPosition: number
			}

			function isDragPayload(obj: unknown): obj is DragPayload {
				return (
					typeof obj === 'object' &&
					obj !== null &&
					'questionId' in obj &&
					'fromCategory' in obj &&
					'fromPosition' in obj &&
					typeof (obj as DragPayload).questionId === 'string' &&
					typeof (obj as DragPayload).fromCategory === 'string' &&
					typeof (obj as DragPayload).fromPosition === 'number'
				)
			}

			const mockEvent: DragEvent = new DragEvent('dragstart')
			const setDataSpy: jasmine.Spy = jasmine.createSpy('setData')
			Object.defineProperty(mockEvent, 'dataTransfer', {
				value: {
					setData: setDataSpy
				}
			})

			const nonExistentQuestion: CategorizedQuestionWithSchedule = {
				question: { id: 'nonexistent' } as Question,
				category: 'Test',
				schedule: null
			}

			spyOn(component.questionnaireEditorService, 'getQuestionsByCategory$').and.returnValue(
				of([mockQuestion1])
			)

			component.category = 'Test'
			component.onDragStart(mockEvent, nonExistentQuestion)

			expect(component.startDragging).toBe(true)
			expect(component['isDragSource']).toBe(true)
			expect(setDataSpy).toHaveBeenCalledWith('text/plain', jasmine.any(String))

			const setDataArg: unknown = setDataSpy.calls.mostRecent().args[1]

			expect(typeof setDataArg).toBe('string')


			const parsedPayload: DragPayload = JSON.parse(setDataArg as string) as DragPayload
			if (!isDragPayload(parsedPayload)) throw new Error('Invalid payload structure')

			const payload: DragPayload = parsedPayload


			expect(payload.questionId).toBe('nonexistent')
			expect(payload.fromCategory).toBe(component.category)
			expect(payload.fromPosition).toBe(-1)


			// Add a second payload test
			const secondMockEvent: DragEvent = new DragEvent('dragstart')
			const secondSetDataSpy: jasmine.Spy = jasmine.createSpy('setData')
			Object.defineProperty(secondMockEvent, 'dataTransfer', {
				value: {
					setData: secondSetDataSpy
				}
			})

			const existingQuestion: CategorizedQuestionWithSchedule = {
				question: { id: '001' } as Question,
				category: 'Test',
				schedule: null
			}

			component.onDragStart(secondMockEvent, existingQuestion)

			const secondSetDataArg: unknown = secondSetDataSpy.calls.mostRecent().args[1]

			const parsedSecondPayload: DragPayload = JSON.parse(secondSetDataArg as string) as DragPayload
			if (!isDragPayload(parsedSecondPayload)) throw new Error('Invalid payload structure')

			const secondPayload: DragPayload = parsedSecondPayload


			expect(typeof secondSetDataArg).toBe('string')

			expect(secondPayload.questionId).toBe('001')
			expect(secondPayload.fromCategory).toBe(component.category)
			expect(secondPayload.fromPosition).toBe(0)
		})

	})

	describe('onDragEnd', () => {
		it('should reset isDragSource when called', () => {
			// Arrange

			const mockDataTransfer: {
				setData: jasmine.Spy<jasmine.Func>
				effectAllowed: string;
			} = {
				setData: jasmine.createSpy('setData'),
				effectAllowed: ''
			}

			const mockEvent: DragEvent = {
				dataTransfer: mockDataTransfer
			} as unknown as DragEvent

			component.onDragStart(mockEvent, mockQuestions[0])

			expect(component['isDragSource']).toBe(true)

			component.onDragEnd()

			expect(component['isDragSource']).toBe(false)
		})

	})

	describe('allowDrop', () => {
		it('should set dropEffect to `move`', () => {

			const mockEvent: DragEvent = {
				preventDefault: jasmine.createSpy('preventDefault'),
				dataTransfer: {
					getData: jasmine.createSpy('getData').and.returnValue(JSON.stringify({ fromCategory: 'OtherCategory' })),
					dropEffect: ''
				}
			} as unknown as DragEvent

			component.allowDrop(mockEvent)

			/* eslint-disable @typescript-eslint/unbound-method */
			const preventDefaultFn: () => void = mockEvent.preventDefault

			expect(preventDefaultFn).toHaveBeenCalledWith()
			expect(mockEvent.dataTransfer?.dropEffect).toBe('move')

		})
	})

	describe('onDrop', () => {
		it('should reorder question when dropping in the same category', fakeAsync(() => {
			const payload: DragAndDropPayload = {
				questionId: 'question1',
				fromCategory: 'category1',
				fromPosition: 0
			}

			const mockEvent: DragEvent= {
				preventDefault: jasmine.createSpy('preventDefault'),
				clientX: 100,
				dataTransfer: {
					getData: jasmine.createSpy('getData').and.returnValue(JSON.stringify(payload)),
					dropEffect: 'move'
				},
				currentTarget: {
					querySelector: () => ({
						classList: {
							remove: () => { }
						}
					})
				}
			} as unknown as DragEvent


			const targetPosition: number = 1
			const reorderQuestionSpy: jasmine.Spy = spyOn(component.questionnaireEditorService, 'reorderQuestion')
			const moveQuestionSpy: jasmine.Spy = spyOn(component.questionnaireEditorService, 'moveQuestion')

			component.category = 'category1'

			spyOn(component, 'getDropPosition').and.returnValue(1)

			component.onDrop(mockEvent)

			tick(1000)

			expect(reorderQuestionSpy).toHaveBeenCalledWith(component.category, payload.fromPosition, targetPosition)
			expect(moveQuestionSpy).not.toHaveBeenCalled()
		}))

		it('should move question when dropping it into another category', fakeAsync(() => {
			const payload: DragAndDropPayload = {
				questionId: 'question1',
				fromCategory: 'category1',
				fromPosition: 0
			}

			const mockEvent: DragEvent= {
				preventDefault: jasmine.createSpy('preventDefault'),
				clientX: 100,
				dataTransfer: {
					getData: jasmine.createSpy('getData').and.returnValue(JSON.stringify(payload)),
					dropEffect: 'move'
				},
				currentTarget: {
					querySelector: () => ({
						classList: {
							remove: () => { }
						}
					})
				}
			} as unknown as DragEvent


			const targetPosition: number = 3
			const reorderQuestionSpy: jasmine.Spy = spyOn(component.questionnaireEditorService, 'reorderQuestion')
			const moveQuestionSpy: jasmine.Spy = spyOn(component.questionnaireEditorService, 'moveQuestion')

			component.category = 'category2'

			spyOn(component, 'getDropPosition').and.returnValue(3)

			component.onDrop(mockEvent)

			tick(1000)

			expect(reorderQuestionSpy).not.toHaveBeenCalled()
			expect(moveQuestionSpy).toHaveBeenCalledWith(payload.questionId, component.category, targetPosition)
		}))
	})

	describe('isLeftDropZone', () => {
		it('should return true when currentDragOverId matches and isRightHalf is false', () => {
			component.currentDragOverId = 'question1'
			component.isRightHalf = false
			component.isLeftHalf = true

			expect(component.isLeftDropZone('question1')).toBe(true)
		})

		it('should return false when currentDragOverId matches but isRightHalf is true', () => {
			component.currentDragOverId = 'question1'
			component.isRightHalf = true

			expect(component.isLeftDropZone('question1')).toBe(false)
		})

		it('should not set left dropzone to true when currentDragOverId does not match', () => {
			component.currentDragOverId = 'question2'
			component.isRightHalf = false

			expect(component.isLeftDropZone('question1')).toBe(false)
		})
	})

	describe('isRightDropZone', () => {
		it('should return true when currentDragOverId matches and isRightHalf is true', () => {
			component.currentDragOverId = 'question1'
			component.isRightHalf = true

			expect(component.isRightDropZone('question1')).toBe(true)
		})

		it('should return false when currentDragOverId matches but isRightHalf is false', () => {
			component.currentDragOverId = 'question1'
			component.isRightHalf = false

			expect(component.isRightDropZone('question1')).toBe(false)
		})

		it('should not set right dropzone to true when currentDragOverId does not match', () => {
			component.currentDragOverId = 'question2'
			component.isRightHalf = true

			expect(component.isRightDropZone('question1')).toBe(false)
		})
	})

	describe('dragging over slide', () => {
		let targetElement: HTMLElement = undefined!

		beforeEach(() => {
			// Dragging over the second slide
			targetElement = (fixture.nativeElement as HTMLElement).querySelectorAll('rcc-slide').item(1) as HTMLElement
		})

		it('should update the drop indicator', () => {
			const cardWithinSlide: HTMLElement = targetElement.querySelector('rcc-card') as unknown as HTMLElement

			// Card is positioned 50px left of the screen, and is 200px wide
			spyOn(cardWithinSlide, 'getBoundingClientRect').and.returnValue({
				left: 50,
				width: 200
			} as DOMRect)

			// Drag occurs at 75px
			const event: DragEvent = new DragEvent('dragover', { clientX: 75 })

			targetElement.dispatchEvent(event)

			fixture.detectChanges()

			expect(component.isLeftHalf).toBeTrue()
			expect(component.isRightHalf).toBeFalse()
			expect(component.currentDragOverId).toBe('002')
		})

		it('should update the drop indicator when on the right half of the card', () => {
			const cardWithinSlide: HTMLElement = targetElement.querySelector('rcc-card') as unknown as HTMLElement

			// Card is positioned 50px left of the screen, and is 200px wide
			spyOn(cardWithinSlide, 'getBoundingClientRect').and.returnValue({
				left: 50,
				width: 200
			} as DOMRect)

			// Drag occurs at 171px
			const event: DragEvent = new DragEvent('dragover', { clientX: 171 })

			targetElement.dispatchEvent(event)

			fixture.detectChanges()

			expect(component.isLeftHalf).toBeFalse()
			expect(component.isRightHalf).toBeTrue()
			expect(component.currentDragOverId).toBe('002')
		})
	})

	describe('getDropPosition', () => {

		const mockParentElement: HTMLElement = document.createElement('div')
		const mockTargetElement: HTMLElement = document.createElement('div')

		mockTargetElement.setAttribute('data-question-id', 'test-question-id')
		mockParentElement.appendChild(mockTargetElement)

		for (let i: number = 0; i < 3; i++) mockParentElement.appendChild(document.createElement('div'))

		it('should return 1 if isRightHalf is true', () => {
			const mockEvent: DragEvent = {
				preventDefault: jasmine.createSpy('preventDefault'),
				clientX: 100,
				dataTransfer: {
					dropEffect: 'move'
				},
				target: mockTargetElement
			} as unknown as DragEvent


			component.isRightHalf = true
			const result: number = component.getDropPosition(mockEvent)

			expect(result).toBe(1)
		})

		it('should return 0 if isRightHalf is false', () => {

			const mockEvent: DragEvent = {
				preventDefault: jasmine.createSpy('preventDefault'),
				clientX: 100,
				dataTransfer: {
					dropEffect: 'move'
				},
				target: mockTargetElement
			} as unknown as DragEvent


			component.isRightHalf = false
			const result: number = component.getDropPosition(mockEvent)

			expect(result).toBe(0)
		})
	})

	describe('OnDragEnterLane', () => {
		it('should set dragHover to true', () => {
			// Arrange
			expect(component.dragHover).toBe(false)

			// Act
			component.onDragEnterLane()

			// Assert
			expect(component.dragHover).toBe(true)
		})
	})

	describe('onDragLeaveLane', () => {
		let mockQuestionListContainer: ElementRef<HTMLDivElement> = undefined!

		beforeEach(() => {
			mockQuestionListContainer = {
				nativeElement: document.createElement('div')
			}
			component['questionListContainer'] = mockQuestionListContainer
		})

		it('should not set dragHover to false when new element is a child of question lane', () => {

			mockQuestionListContainer = {
				nativeElement: document.createElement('div')
			}

			const childElement: HTMLDivElement = document.createElement('div')
			childElement.style.width = '100px'
			childElement.style.height = '50px'

			mockQuestionListContainer.nativeElement.appendChild(childElement)

			const mockEvent: DragEvent= {
				relatedTarget: childElement
			} as unknown as DragEvent

			component.onDragEnterLane()

			expect(component.dragHover).toBe(true)

			component.onDragLeaveLane(mockEvent)

			expect(component.dragHover).toBe(false)
		})

		it('should set dragHover to false when new element is not a child of question lane', () => {

			const childElement: HTMLDivElement = document.createElement('div')
			childElement.style.width = '100px'
			childElement.style.height = '50px'

			const mockEvent: DragEvent = {
				relatedTarget: childElement
			} as unknown as DragEvent

			component.onDragEnterLane()

			expect(component.dragHover).toBe(true)

			component.onDragLeaveLane(mockEvent)

			expect(component.dragHover).toBe(false)
		})
	})

	describe('dragCleanUp', () => {
		it('should reset drag-related properties', () => {
			component.currentDragOverId = 'someId'
			const mockCardElement: HTMLDivElement = document.createElement('div')
			component.lastCardElement = mockCardElement
			component.lastRect = component.lastCardElement.getBoundingClientRect()

			expect(component.currentDragOverId).toBe('someId')
			expect(component.lastCardElement).toBe(component.lastCardElement)
			expect(component.lastRect).toBe(component.lastRect)

			component.dragCleanUp()

			expect(component.currentDragOverId).toBeNull()
			expect(component.lastCardElement).toBeNull()
			expect(component.lastRect).toBeNull()
		})
	})
})
