import	{
			Inject,
			Injectable,
			InjectionToken,
			Optional
		}											from '@angular/core'
import	{
			SymptomCheck,
			Question,
			isErrorFree,
			has,
			assert,
			Schedule,
			QuestionConfig,
			toISO8601CalendarDate,
			SymptomCheckMetaConfig,
			ScheduleConfig,
			SymptomCheckConfig,
		}											from '@rcc/core'
import 	{
			BehaviorSubject,
			concatMap,
			firstValueFrom,
			forkJoin,
			from,
			map,
			mergeMap,
			Observable,
			of,
			switchMap,
			take,
			filter,
			toArray,
			merge,
			takeUntil,
			Subject,
			skip
		} 											from 'rxjs'
import 	{
			QuestionnaireService
		} 											from '../questions'

export const MONITORING_SETUP_CONFIG 	: InjectionToken<MonitoringSetupConfig>
										= new InjectionToken<MonitoringSetupConfig>('MonitoringSetupConfig')

export interface QuestionList {
	id			: string
	questions	: Question[]
}

export interface CategorizedQuestionWithSchedule {
	question	: Question;
	category	: string | null;
	schedule	: Schedule | null;
}

type QuestionUpdate  = Partial<QuestionConfig> & Required<Pick<QuestionConfig, 'id'>>;

interface CategorizedQuestionWithScheduleUpdate {
	question: QuestionUpdate;
	category?: string|null;
	schedule?: Schedule|null;
}

export interface MonitoringSetupConfig {
	questionCategories				: string[];
	hiddenOnEditCategories			: string[];
	hiddenInCatalogue				: string[];
	defaultQuestionsByCategory		: Map<string, string[]>;
	addTemplateQuestionsTo			: string|null;
	defaultScheduleConfig			: ScheduleConfig;
	catalogRestrictionsByCategory	: Record<string, { includeOnly: string[] }>;

}

/*
export const MONITORING_SETUP_CONFIG 	: InjectionToken<MonitoringSetupConfig>
										= new InjectionToken<MonitoringSetupConfig>('MonitoringSetupConfig')
*/

function assertIsCategorizedQuestionWithSchedule(x: unknown) : asserts x is CategorizedQuestionWithSchedule {
	assert(has(x, 'question'), 'isCategorizedQuestionWithSchedule(): missing .question on x', x)
	assert(Object.keys(x).length <= 3, 'isCategorizedQuestionWithSchedule(): x has extra properties.', x)
	assert(has(x, 'category'), 'isCategorizedQuestionWithSchedule(): missing .category on x', x)
	assert(has(x, 'schedule'), 'isCategorizedQuestionWithSchedule(): missing .schedule on x', x)
	assert(x.question instanceof Question, 'isCategorizedQuestionWithSchedule(): .question must be instance of Question on x', x)
	assert(typeof x.category === 'string' || x.category === null, 'isCategorizedQuestionWithSchedule(): .category must be of type string or null on x', x)
	assert(x.schedule instanceof Schedule || x.schedule === null, 'isCategorizedQuestionWithSchedule(): .schedule must be instance of Schedule or null on x', x)
}

export function isCategorizedQuestionWithSchedule(x: unknown) : x is CategorizedQuestionWithSchedule {
	return isErrorFree(() => assertIsCategorizedQuestionWithSchedule(x))
}

@Injectable()
export class RccQuestionnaireEditorService {

	private questionCatalogue	: Map<string, Question>

	public _questions$			: BehaviorSubject<CategorizedQuestionWithSchedule[]>
								= new BehaviorSubject<CategorizedQuestionWithSchedule[]>([])

	public questions$			: Observable<CategorizedQuestionWithSchedule[]>
								= this._questions$.asObservable()

	private _baseSchedule$		: BehaviorSubject<Schedule>
								= new BehaviorSubject<Schedule>(new Schedule([[0,1,2,3,4,5,6], ['12:00']]))

	public baseSchedule$		: Observable<Schedule>
								= this._baseSchedule$.asObservable()

	public ready				: Promise<void>

	private _dirty$				: BehaviorSubject<boolean>
								= new BehaviorSubject<boolean>(false)

	public dirty$				: Observable<boolean>
								= this._dirty$.asObservable()

	private destroy$			: Subject<void> = new Subject()

	public constructor(
		@Inject(MONITORING_SETUP_CONFIG) @Optional()
		private monitoringSetupConfig : MonitoringSetupConfig,
		private readonly questionnaireService: QuestionnaireService,
	){
		this.ready = this.reset()
	}

	public trackFormState(): void {
		this.unsubscribe()
		this.destroy$ = new Subject()

		// monitoring setup is flagged dirty whenever
		// _questions$ or _baseSchedule$ changes
		merge(
			this._questions$,
			this._baseSchedule$
		)
		.pipe(
			skip(2), // Both of the above BehaviorSubjects will emit on subscribe, we do not want to count these emission.
			takeUntil(this.destroy$),
			map(() => true),
		)
		.subscribe( dirty  => this._dirty$.next(dirty))
		// The line above used to look like this:
		// `.subscribe(this._dirty$)`
		// This is way more neat, but will trigger .complete
		// on `this._dirty$` when `this.destroy$` emits,
		// which cause `this._dirty$` to lose all observers,
		// what we do not want.
		// `this.destroy$` emits when this.unsubscribe()
		// is called, which happens right at the start of this method.

	}

	// #region getters & setters

	public get catalogue(): Question[] {
		return (this.questionCatalogue ? [...this.questionCatalogue.values()] : []) as Question[]
	}

	public get categories(): string[] {
		return this.monitoringSetupConfig?.questionCategories ?? []
	}

	public get hiddenOnEditCategories(): string[] {
		return this.monitoringSetupConfig?.hiddenOnEditCategories ?? []
	}

	public get categoryForTemplateQuestions(): string|null {
		return this.monitoringSetupConfig?.addTemplateQuestionsTo ?? null
	}
	public get catalogRestrictionsByCategory(): Record<string, { includeOnly: string[] }> {
		return this.monitoringSetupConfig?.catalogRestrictionsByCategory ?? null
	}

	public get baseScheduleDays(): number[] {
		return [...this._baseSchedule$.getValue().daysOfWeek]
	}

	public set baseScheduleDays(days: number[]) {
		this._baseSchedule$.next(new Schedule([[...days], []]))
	}

	public get dirty(): boolean {
		return this._dirty$.getValue()
	}

	public getQuestionsByCategory$(category: string | null = null): Observable<CategorizedQuestionWithSchedule[]> {
		return this._questions$.pipe(
			map((questions: CategorizedQuestionWithSchedule[]) => questions.filter((item: CategorizedQuestionWithSchedule) => item.category === category))
		)
	}

	// #endregion


	public hasTouchscreen(): boolean {
		let hasTouchScreen: boolean = false
		if ('maxTouchPoints' in navigator)
			hasTouchScreen = navigator.maxTouchPoints > 0
		else if ('msMaxTouchPoints' in navigator)
			hasTouchScreen = (navigator as Navigator & { msMaxTouchPoints?: number }).msMaxTouchPoints > 0
		else {
			const mQ: MediaQueryList = matchMedia?.('(pointer:coarse)')
			if (mQ?.media === '(pointer:coarse)') hasTouchScreen = !!mQ.matches
			else if ('orientation' in window) hasTouchScreen = true // deprecated, but as fallback
			else {
				// Only as a last resort, fall back to user agent sniffing
				const userAgent: string = (navigator as Navigator).userAgent
				hasTouchScreen =
					/\b(BlackBerry|webOS|iPhone|IEMobile)\b/i.test(userAgent) ||
					/\b(Android|Windows Phone|iPad|iPod)\b/i.test(userAgent)
			}
		}
		return hasTouchScreen
	}
	public allowDragAndDrop(category: string): boolean {

		if (category === 'medication') return false

		// the drag and drop function isn't working as intended on touch devices, hence it is disabled here

		if (this.hasTouchscreen()) return false

		return true
	}

	// #region add/remove/update questions

	public addQuestion(question: CategorizedQuestionWithSchedule): void
	public addQuestion(question: Question | string, category?: string|null, schedule?: Schedule|null): void
	public addQuestion(question: CategorizedQuestionWithSchedule | Question | string, category: string|null = null, schedule: Schedule|null = null): void {

		this._questions$
			.pipe(
				take(1),
				mergeMap((currentQuestions: CategorizedQuestionWithSchedule[]) => {
					if(isCategorizedQuestionWithSchedule(question))
						return of(currentQuestions.concat([question]))
					else if(question instanceof Question)
						return of(currentQuestions.concat([{ question, category, schedule }]))

					else if(typeof question === 'string')
						return of(question)
							.pipe(
								switchMap((questionId: string) =>
									this.questionnaireService.get(questionId)
								),
								map((newQuestion: Question) =>
									({ question: newQuestion, category, schedule } as CategorizedQuestionWithSchedule)
								),
								map((newQuestion: CategorizedQuestionWithSchedule) =>
									currentQuestions.concat([newQuestion])
								)
							)
				}),
				// this is here to safeguard against faulty input
				filter((updatedQuestions: CategorizedQuestionWithSchedule[]) => !!updatedQuestions)
			)
			.subscribe(
				(updatedQuestions: CategorizedQuestionWithSchedule[]) => this._questions$.next(updatedQuestions)
			)

		this._dirty$.next(true)

	}

	public addMultipleQuestions(questions: CategorizedQuestionWithSchedule[]): void
	public addMultipleQuestions(questions: Question[], category?: string|null, schedule?: Schedule|null): void
	public addMultipleQuestions(questions: string[], category?: string|null, schedule?: Schedule|null): void
	public addMultipleQuestions(questions: CategorizedQuestionWithSchedule[] | Question[] | string[], category: string|null = null, schedule: Schedule|null = null): void {

		this._questions$
			.pipe(
				take(1),
				switchMap((currentQuestions:CategorizedQuestionWithSchedule[]) => {
					if(isCategorizedQuestionWithSchedule(questions[0]))
						return of(currentQuestions
									.concat(questions as CategorizedQuestionWithSchedule[])
								)
					else if(questions[0] instanceof Question)
						return of(currentQuestions
									.concat(
										(questions as Question[])
											.map((question: Question) =>
												({ question, category, schedule })
										)
									)
								)
					else if(typeof questions[0] === 'string')
						return from((questions as string[]))
							.pipe(
								concatMap((question: string) =>
									this.questionnaireService.get(question)
								),
								map((question: Question) =>
									({ question, category, schedule } as CategorizedQuestionWithSchedule)
								),
								toArray(),
								map((newQuestions: CategorizedQuestionWithSchedule[]) =>
									currentQuestions.concat(newQuestions)
								)
							)
				}),
				// this is here to saveguard against faulty input
				filter((updatedQuestions: CategorizedQuestionWithSchedule[]) => !!updatedQuestions)
			)
			.subscribe(
				(updatedQuestions: CategorizedQuestionWithSchedule[]) =>
					this._questions$.next(updatedQuestions)
			)

		this._dirty$.next(true)
	}

	public reorderQuestion(category: string, fromPosition: number, toPosition: number): void {
		this.questions$.pipe(
			take(1),
			map(questions => {
				const categoryQuestions: CategorizedQuestionWithSchedule[] = questions.filter(q => q.category === category)
				const [reorderedQuestion] = categoryQuestions.splice(fromPosition, 1)

				// This is to account for the new array where the indices have changed since one question was removed. Depending on moving up or down the array, we adjust the 'toPosition' accordingly.
				const adjustedToPosition : number = fromPosition < toPosition ? toPosition - 1 : toPosition

				categoryQuestions.splice(adjustedToPosition, 0, reorderedQuestion)
				return questions.map(q => q.category === category ? categoryQuestions.shift() || q : q)
			})
		).subscribe((newQuestions: CategorizedQuestionWithSchedule[]) => {
			this._questions$.next(newQuestions)
			this.sortByCategory(newQuestions)

		})
	}

	public moveQuestion(questionId: string, toCategory: string, toPosition: number): void {
		this.questions$.pipe(
			take(1),
			map(questions => {
				const questionIndex : number = questions.findIndex(q => q.question.id === questionId)
				if (questionIndex === -1) return questions

				const [movedQuestion] = questions.splice(questionIndex, 1)
				movedQuestion.category = toCategory

				const insertIndex : number = questions.findIndex(q => q.category === toCategory) + toPosition
				questions.splice(insertIndex, 0, movedQuestion)

				return questions
			})
		).subscribe(newQuestions => {
			this._questions$.next(newQuestions)
			this.sortByCategory(newQuestions)

		})

	}

	public removeQuestion(question: CategorizedQuestionWithSchedule): void
	public removeQuestion(question: Question | string, category?: string|null): void
	public removeQuestion(question: CategorizedQuestionWithSchedule | Question | string, category: string|null = null): void {
		this._questions$
			.pipe(
				take(1),
				map((currentQuestions: CategorizedQuestionWithSchedule[]) => {
					if(isCategorizedQuestionWithSchedule(question))
						return currentQuestions
							.filter(
								(item: CategorizedQuestionWithSchedule) =>
									!(item.question.id === question.question.id && item.category === question.category)
							)
					else if(typeof question === 'string')
						return currentQuestions
							.filter(
								(item: CategorizedQuestionWithSchedule) =>
									!(item.question.id === question && item.category === category)
								)
					else if(question instanceof Question)
						return currentQuestions
							.filter(
								(item: CategorizedQuestionWithSchedule) =>
									!(item.question.id === question.id && item.category === category)
								)
				}),
				// this is here to saveguard against faulty input
				filter((updatedQuestions: CategorizedQuestionWithSchedule[]) => !!updatedQuestions)
			)
			.subscribe(
				(updatedQuestions: CategorizedQuestionWithSchedule[]) => {
					this._questions$.next(updatedQuestions)
					if(updatedQuestions.length === 1) this._dirty$.next(false)
				}
			)

	}

	public removeMultipleQuestions(questions: CategorizedQuestionWithSchedule[]): void
	public removeMultipleQuestions(questions: Question[], category?: string|null): void
	public removeMultipleQuestions(questions: string[], category?: string|null): void
	public removeMultipleQuestions(questions: CategorizedQuestionWithSchedule[]| Question[] | string[], category: string|null = null): void {
		if(!questions.length) return
		this._questions$
			.pipe(
				take(1),
				map((currentQuestions: CategorizedQuestionWithSchedule[]) => {
					if(isCategorizedQuestionWithSchedule(questions[0]))
						return currentQuestions
							.filter(
								(categorizedQuestion: CategorizedQuestionWithSchedule) =>
									!(questions as CategorizedQuestionWithSchedule[]).includes(categorizedQuestion)
							)
					else if(typeof questions[0] === 'string')
						return currentQuestions
							.filter(
								(categorizedQuestion: CategorizedQuestionWithSchedule) =>
									!(questions as string[])
										.find(
											(questionId: string) =>
												questionId === categorizedQuestion.question.id && category === categorizedQuestion.category
										)
							)
					else if(questions[0] instanceof Question)
						return currentQuestions
							.filter(
								(categorizedQuestion: CategorizedQuestionWithSchedule) =>
									!(questions as Question[])
										.find(
											(question: Question) =>
												question.id === categorizedQuestion.question.id && category === categorizedQuestion.category
										)
							)
				}),
				// this is here to safeguard against faulty input
				filter((updatedQuestions: CategorizedQuestionWithSchedule[]) => !!updatedQuestions)
			)
			.subscribe(
				(updatedQuestions: CategorizedQuestionWithSchedule[]) =>
					this._questions$.next(updatedQuestions)
			)
	}

	/**
	 * Updating a question will not actually change that question but replace it with a new question
	 * with the updated config and a new id. Since we don't want the order of the questions to
	 * change we have to find the old question and replace it by the new one, instead of removing
	 * it and adding the new one to the list.
	 * @param {string} currentId The id of the question to replace
	 * @param {CategorizedQuestionWithScheduleUpdate | QuestionUpdate} question The new question to
	 * insert in place of the previous one
	 * @param {string | null} category Optional. The category the question should be linked to.
	 * Defaults to null
	 * @param {Schedule | null} schedule Optional. Custom schedule for this question. Defaults to null.
	 */
	public updateQuestion(currentId: string, question: CategorizedQuestionWithScheduleUpdate): void
	public updateQuestion(currentId: string, question: QuestionUpdate, category?: string|null, schedule?: Schedule|null): void
	public updateQuestion(currentId: string, question: CategorizedQuestionWithScheduleUpdate | QuestionUpdate, category: string|null = null, schedule: Schedule|null = null): void {
		this._questions$.pipe(
			take(1),
			map((questions: CategorizedQuestionWithSchedule[]) => {
				let questionToUpdate : null | CategorizedQuestionWithSchedule = null

				if(has(question, 'id'))
					questionToUpdate	= 	questions
												.find(
													(item: CategorizedQuestionWithSchedule) =>
														item.question.id === currentId && item.category === category
												)
				else
					questionToUpdate	= 	questions
												.find(
													(item: CategorizedQuestionWithSchedule) =>
													item.question.id === currentId  && item.category === question.category
												)
				if(questionToUpdate) {
					questionToUpdate.question = new Question({
													...questionToUpdate.question.config,
													...(has(question, 'id') ? question : question.question)
												})
					questionToUpdate.schedule = (has(question, 'id') ? schedule : question.schedule) ?? null
				}

				return questions
			})
		)
		.subscribe(
			(questions: CategorizedQuestionWithSchedule[]) =>
				this._questions$.next(questions)
		)
	}

	// #endregion


	// #region submitting


	/**
	 * For sending the questionnaire to the PAT in the correct order (as displayed
	 * in the HCP UI) this method will do the sorting according to the category
	 * order which is optionally provided for more flexibility or simply provided by
	 * the monitoringSetupConfig.
	 */
	public sortByCategory(questions: CategorizedQuestionWithSchedule[], categoryOrder?: string[]): CategorizedQuestionWithSchedule[] {
		// If categoryOrder is not provided, use the one from monitoringSetupConfig
		const order : string[] = categoryOrder || this.monitoringSetupConfig.questionCategories

		// Create a map to group questions by category
		const categoryMap: Map<string, CategorizedQuestionWithSchedule[]> = new Map<string, CategorizedQuestionWithSchedule[]>()
		order.forEach(category => categoryMap.set(category, []))

		// Group questions by category
		questions.forEach(question => {
			if (categoryMap.has(question.category))
				categoryMap.get(question.category).push(question)

		})

		// Flatten the map back into an array, preserving category order
		return order.flatMap(category => categoryMap.get(category) || [])
	}

	public getConfig(meta: SymptomCheckMetaConfig): SymptomCheckConfig {
		const questions 	: CategorizedQuestionWithSchedule[]
							= this._questions$.getValue()

		const symptomCheck 	: SymptomCheck
							= new SymptomCheck({
								meta: {
									creationDate: toISO8601CalendarDate(new Date()),
									defaultSchedule: this._baseSchedule$.getValue().config,
									...meta
								},
								questions: []
							})

		questions.forEach(
			(categorizedQuestion: CategorizedQuestionWithSchedule) =>
				symptomCheck.addQuestionId(
					categorizedQuestion.question.id,
					categorizedQuestion.schedule?.config,
					categorizedQuestion.category
				)
			)

		return symptomCheck.config
	}

	public getQuestions(): QuestionConfig[] {
		const questions 	: CategorizedQuestionWithSchedule[]
							= this._questions$.getValue()

		const sortedQuestions	: CategorizedQuestionWithSchedule[]
								= this.sortByCategory(questions)

		const questionConfigs 	: QuestionConfig[]
								= sortedQuestions.map(
									(categorizedQuestion: CategorizedQuestionWithSchedule) => categorizedQuestion.question.config
								)
		return questionConfigs
	}

	// #endregion


	// #region setup

	private unsubscribe(): void {
		this.destroy$.next()
		this.destroy$.complete()
	}

	public async reset(setDirty: boolean = false) : Promise<void> {
		this.unsubscribe()
		this.initQuestionCatalogue()
		this._questions$.next([])

		if(this.monitoringSetupConfig !== null) {
			// add default questions
			const defaultQuestions 	: { [category: string] : Question[] }
									= await this.getDefaultQuestions()

			for (const [category, questions] of Object.entries(defaultQuestions))
				this.addMultipleQuestions(questions, category)

			// set default base schedule
			this._baseSchedule$.next(new Schedule(this.monitoringSetupConfig.defaultScheduleConfig))
		}
		this._dirty$.next(setDirty)
	}

	private initQuestionCatalogue(): void {
		void this.questionnaireService.ready
			.then(() => {
				const questions :	[string, Question][]
								=	this.questionnaireService.items
									.filter(
										(question) => !this.monitoringSetupConfig?.hiddenInCatalogue?.includes(question.id)
									)
									.map(
										(question) => [question.id, question]
									)

				this.questionCatalogue = new Map(questions)
			})
	}

	private async getDefaultQuestions(): Promise<{ [category: string]: Question[] }> {
		const defaultQuestionsSetup: { [category: string]: Promise<Question[]> }
			= {}

		this.monitoringSetupConfig.defaultQuestionsByCategory.forEach(
			(questions: string[], category: string) => {
				defaultQuestionsSetup[category] = this.questionnaireService.get(questions)
			}
		)

		// For unit testing the question lane component this safeguard is implemented as there are no defaultQuestions loaded
		if (Object.keys(defaultQuestionsSetup).length === 0)
			return {}

		return firstValueFrom(forkJoin(defaultQuestionsSetup))
	}

	// #endregion

}
