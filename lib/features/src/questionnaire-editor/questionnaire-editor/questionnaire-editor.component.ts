import	{
			Component,
			OnInit,
			OnDestroy,
			Input,
			Output,
			EventEmitter,
			ViewChild,
			ChangeDetectorRef,
			Inject,
			Optional
		}									from '@angular/core'

import	{
			DAILY_NOTES_ID,
			Question,
			QuestionConfig,
			QuestionRelation,
			QuestionRelationDto,
			QuestionStore,
			SymptomCheck,
			SymptomCheckConfig,
			indexArray,
			isDailyNotesQuestion,
		}									from '@rcc/core'
import	{
			HandlerAction,
			Action,
			RccModalController,
			RccTranslationService,
			RccToastController,
		}									from '@rcc/common'
import	{
			RccQuestionnaireEditorService
		}									from '../questionnaire-editor.service'
import	{
			BehaviorSubject,
			Observable,
			Subject,
			combineLatest,
			map,
			switchMap,
			distinctUntilChanged,
			takeUntil,
			filter,
			startWith,
			skip,
		}									from 'rxjs'
import	{
			CustomSymptomCheckStoreService,
			SymptomCheckMetaStoreService,
		}									from '../../symptom-checks'
import	{
			FormControl,
			FormGroup
		}									from '@angular/forms'
import	{
			EditScheduleModalComponent,
			FormData as ScheduleFormData
		}									from '../modal/edit-schedule/edit-schedule-modal.component'
import	{
			EditReminderModalComponent,
			FormData as ReminderFormData
		}									from '../modal/edit-reminder/edit-reminder-modal.component'
import	{
			RccEditTextComponent,
			RccFullPageComponent
		}									from '@rcc/themes/bengal'
import	{
			CustomQuestionStoreService,
			QUESTION_STORES,
			QuestionnaireService
		}									from	'../../questions'
import	{	Router						}	from	'@angular/router'

export interface ConfirmPayload {
	config: SymptomCheckConfig
	questions: Array<QuestionConfig>
}

export interface ExistingSymptomCheck {
	symptomCheck	: SymptomCheck
	questions		: Question[]
}

@Component({
    selector: 'rcc-questionnaire-editor-page',
    templateUrl: './questionnaire-editor.component.html',
    styleUrls: ['./questionnaire-editor.component.scss'],
    standalone: false
})
export class RccQuestionnaireEditorComponent implements OnInit, OnDestroy {

	@Input()
	public templateId				:	string

	@Input()
	public existingSymptomCheck		:	ExistingSymptomCheck

	@Input()
	public showSaveButton			:	boolean

	@Input()
	public updateNameFromTemplate	:	boolean		=	false

	@Input()
	public backButtonAction			:	Action

	@Input()
	public set initialName(value: string) {
		if (value == null) return
		this.nameFormGroup.setValue({
			name: value
		})
	}

	@Input()
	public set confirmLabel(label: string) { this.primaryAction.label = label }

	@Output()
	public confirm: EventEmitter<ConfirmPayload> = new EventEmitter<ConfirmPayload>()

	@ViewChild(RccFullPageComponent, { static: true }) private rccFullPageComponent: RccFullPageComponent
	@ViewChild(RccEditTextComponent, { static: false }) private rccEditTextComponent: RccEditTextComponent


	public expanded				:	boolean											= false

	private _mode$				:	BehaviorSubject<'edit'|'preview'|'init'>		= new BehaviorSubject('init')

	private reminderTime		:	BehaviorSubject<string> 						= new BehaviorSubject('12:00')
	private reminderEnabled		:	BehaviorSubject<boolean> 						= new BehaviorSubject(true)
	private templateControl		:	FormControl<string> 							= new FormControl<string>('')
	protected templateFormGroup	:	FormGroup<{ template: FormControl<string> }>
								=	new FormGroup({ template: this.templateControl })
	protected emptyTitle		:	string
								=	this.translationService.translate('CURATED.QUESTIONS.CATEGORIES.RCC-CATEGORY-EMPTY')
	protected nameFormGroup		:	FormGroup<{ name: FormControl<string> }>
								=	new FormGroup({ name: new FormControl(this.emptyTitle) })
	protected editName			:	boolean 										= false
	protected mode$				:	Observable<'edit'|'preview'|'init'>				= this._mode$.asObservable()

	protected templates$		:	Observable<{ value: string, label: string }[]>
								=	this.symptomCheckMetaStoreService.change$
									.pipe(
										startWith(null),
										map(() => this.symptomCheckMetaStoreService.items),
										map((templates) => templates.map((template) => ({
											value: template.id,
											label: template.meta.label,
										}))
										),
										)

	private unsubscribe$ 		: Subject<void>										= new Subject<void>()

	private onBeforeUnload : (e: BeforeUnloadEvent) => void = (e) =>{
		if(this.rccQuestionnaireEditorService.dirty) {
			e.preventDefault()
			e.returnValue = ''
		}
	}

	public constructor (
		private readonly rccQuestionnaireEditorService	: RccQuestionnaireEditorService,
		private readonly symptomCheckMetaStoreService	: SymptomCheckMetaStoreService,

		private readonly customSymptomCheckStoreService	: CustomSymptomCheckStoreService,
		private readonly customQuestionStoreService		: CustomQuestionStoreService,
		private readonly router							: Router,
		@Optional() @Inject(QUESTION_STORES)
		private readonly stores							: QuestionStore[],

		private readonly rccModalController				: RccModalController,
		private readonly rccToastController				: RccToastController,
		private readonly translationService				: RccTranslationService,
		private readonly questionnaireService			: QuestionnaireService,
		private readonly changeDetectorRef				: ChangeDetectorRef,
	) {}



	public async ngOnInit(): Promise<void> {
		await this.rccQuestionnaireEditorService.ready

		this.rccQuestionnaireEditorService.trackFormState()

		this.subscribeToModeChanges()
		this.subscribeForStatusChanges()
		this.subscribeToTemplateChanges()
		if (this.templateId)
			this.templateControl.setValue(this.templateId)


		if (this.existingSymptomCheck)
			await this.fillInSymptomCheck(this.existingSymptomCheck)

		// prompt user to confirm if they want to reload / leave
		// the page when service is in dirty state. The message
		// displayed depends on the browser.
		addEventListener(
			'beforeunload',
			this.onBeforeUnload
		)

	}

	public ngOnDestroy(): void {
		this.unsubscribe$.next()
		this.unsubscribe$.complete()
		void this.rccQuestionnaireEditorService.reset()

		removeEventListener(
			'beforeunload',
			this.onBeforeUnload
		)
	}

	protected visibleQuestionCategories : string[] = []

	protected updateVisibleQuestionCategories():void {

		const hiddenCategories			: 	string[]
										= 	this.rccQuestionnaireEditorService.hiddenOnEditCategories

		this.visibleQuestionCategories 	= 	this.editMode || this.initMode
											?	this.rccQuestionnaireEditorService.categories
												.filter( category => !hiddenCategories.includes(category))
											// preview mode:
											:	this.rccQuestionnaireEditorService.categories

		// The visible categories determine which questions
		// – and with it which query widget components are shown
		// those might raise a ExpressionChangedAfterItHasBeenCheckedError
		// So we tell angular to look for changes right away:
		this.changeDetectorRef.detectChanges()
	}

	protected get initMode(): boolean {
		return this._mode$.getValue() === 'init'
	}

	protected get editMode(): boolean {
		return this._mode$.getValue() === 'edit'
	}

	// #endregion

	// #region questionnaire name handling

	protected editQuestionnaireName(): void {
		this.editName = true
	}

	protected cancelEditQuestionnaireName(): void {
		this.editName = false
	}

	// #endregion


	// #region actions

	protected expandHeader(): void {
		this.expanded = !this.expanded
	}
	protected get backAction(): Action {
		if (this.backButtonAction) return this.backButtonAction

		return {
			path: '/',
			label: 'QUESTIONNAIRE_EDITOR.BUTTON_BACK.LABEL',
			icon: 'back',
			category: 'create',
		}
	}

	protected editAction: HandlerAction	=	{
		handler: () => this._mode$.next('edit'),
		label: 'QUESTIONNAIRE_EDITOR.BUTTON_EDIT.LABEL',
		icon: 'edit',
		category: 'create',
	}

	protected async saveOrConfirmSymptomCheck(action: string): Promise<void> {
		const config: SymptomCheckConfig = this.rccQuestionnaireEditorService.getConfig({
			label: this.nameFormGroup.get('name').value,
			reminder: this.reminderTime.value
		})

		const questions: QuestionConfig[] = this.rccQuestionnaireEditorService.getQuestions()

		if (this.existingSymptomCheck) config.id = this.existingSymptomCheck.symptomCheck.id

		const dailyNotesQuestionIndex: number = config.questions.findIndex(q => isDailyNotesQuestion(q))
		const questionsSorted: (string | QuestionRelationDto)[] = [
			...config.questions.filter((_, index) => index !== dailyNotesQuestionIndex),
			config.questions[dailyNotesQuestionIndex]
		]
		config.questions = questionsSorted

		// Determine whether to emit the confirm event or save the symptom check
		if (action === 'confirm')
			this.confirm.emit({
				config,
				questions,
			})

		if (action === 'save') {
			const hasQuestion: boolean =
				this.rccQuestionnaireEditorService.getQuestions().find(
					question => question.id !== DAILY_NOTES_ID
				) != null

			if (!hasQuestion) {
				this.scrollToTop()

				void this.rccToastController.present({
					message: 'QUESTIONNAIRE_EDITOR.TEMPLATE_REQUIRED',
					color: 'medium'
				})

				// The setTimeout is needed to wait for the scroll animation to complete
				setTimeout(() => this.rccEditTextComponent.editText(), 600)
				return
			}
			else await this.saveSymptomCheck({ config, questions })
		}

	}

	protected primaryAction	: 	HandlerAction
							=	{
									category: 	'share',
									handler: 	() => this.saveOrConfirmSymptomCheck('confirm'),
									icon: 		undefined,
									label: 		'' // will be updated with confirmationLabel setter (see above)
								}


	protected previewAction	:	HandlerAction
							=	{
									handler: 	() => this._mode$.next('preview'),
									label: 		'QUESTIONNAIRE_EDITOR.BUTTON_PREVIEW.LABEL',
									icon: 		'preview',
									disabled:  () => this.initMode,
									category: 	'share' // TODO: shouldn't be 'share', but this is according to the figma design
								}


	protected saveAction	:	HandlerAction
							=	{
									handler:	 ()  => this.saveOrConfirmSymptomCheck('save'),
									label:		'QUESTIONNAIRE_EDITOR.BUTTON_SAVE.LABEL',
									icon:		'',
									category:	'create',
									TEMP_OUTLINE_PROPERTY: true,
								}

	protected quickActions$: Observable<Action[]> = combineLatest(
		[this.translationService.activeLanguageChange$, this.rccQuestionnaireEditorService.baseSchedule$, this.reminderTime, this.reminderEnabled]
	).pipe(
		map(([, schedule, reminderTime, reminderEnabled]) => {

			const reminderTranslationKey: string = `QUESTIONNAIRE_EDITOR.REMINDER.${reminderEnabled ? 'VALUE' : 'NONE'}`

			const translation: string =	reminderTime
										?	this.translateReminderTime(reminderTime)
										:	undefined

			return [
				{
					icon: 'edit',
					label: schedule as unknown, // Will be replaced by the translation key
					handler: async () => {
						const result: ScheduleFormData = await this.rccModalController.present(
							EditScheduleModalComponent,
							{
								schedule: this.rccQuestionnaireEditorService.baseScheduleDays,
								title:	'QUESTIONNAIRE_EDITOR.SCHEDULE.LABEL.SYMPTOM_CHECK_DEFAULT'
							},
							{ mini: true },
						)

						if (result == null) return

						this.rccQuestionnaireEditorService.baseScheduleDays = result.schedule
					},
					data: schedule,
				} as HandlerAction,
				{
					icon: 'edit',
					label: `${this.translationService.translate(reminderTranslationKey, { time: translation })}`,
					handler: async () => {
						const result: ReminderFormData = await this.rccModalController.present(
							EditReminderModalComponent,
							{ defaultTime: reminderTime, enabled: reminderEnabled },
							{ mini: true },
						)

						if (result == null) return

						this.reminderTime.next(result.time)
						this.reminderEnabled.next(result.enabled)
					},
					data: `${this.translationService.translate(reminderTranslationKey, { time: translation })}`
				} as HandlerAction
			]
		})
	)

	protected async saveSymptomCheck(data: ConfirmPayload): Promise<void> {

		// Disable the button if the title hasn't been changed
		const isTitleChanged: boolean = this.nameFormGroup.get('name').dirty
		if (!isTitleChanged) {
			this.scrollToTop()

			// This should actually be .failure(), but design asks for a different color than 'warning'
			void this.rccToastController.present({
				message: 'QUESTIONNAIRE_EDITOR.TEMPLATE_NAME_REQUIRED',
				color: 'medium'
			})

			// The setTimeout is needed to wait for the scroll animation to complete
			setTimeout(() => this.rccEditTextComponent.editText(), 600)
			return
		}

		data.config.questions = data.config.questions.filter(
			question => !isDailyNotesQuestion(question)
		)
		await this.customSymptomCheckStoreService.addSymptomCheckConfig(data.config)
		await Promise.all([
			this.storeOnTheFlyQuestions(data.questions)
		])
		void this.rccToastController.success('QUESTIONNAIRE_EDITOR.TEMPLATE_SAVED')
	}

	private async storeOnTheFlyQuestions(questions: QuestionConfig[]): Promise<void> {
		await this.questionnaireService.ready
		const existingQuestionsIds : string[] = this.questionnaireService.items
			.map((question) => question.id)

			for(const question of questions) {
				if (existingQuestionsIds.includes(question.id)) continue
				await this.customQuestionStoreService.addQuestionConfig(question)
			}
	}

	private translateReminderTime(reminderTime: string): string {
		const [hours, minutes]: number[] = reminderTime.split(':').map((it: string) => parseInt(it, 10))
		const date: Date = new Date()
		date.setHours(hours)
		date.setMinutes(minutes)
		return this.translationService.translate(date, { hour: '2-digit', minute: '2-digit' })
	}

	// #endregion


	// #region subscriptions

	@Output()
	public metaLabelChange: EventEmitter<string> = new EventEmitter<string>()

	private  subscribeToTemplateChanges(): void {
		this.templateControl.valueChanges.pipe(
			switchMap((id: string) => this.symptomCheckMetaStoreService.get(id)),
			map((symptomCheck: SymptomCheck) => ({
				questionIds: symptomCheck?.questionIds ?? [],
				meta: symptomCheck?.meta,
				relations: symptomCheck.questionRelations
			})),
			takeUntil(this.unsubscribe$)
		).subscribe(  ({ questionIds, meta, relations }) => {
			this.metaLabelChange.emit(meta.label)

			// We assume that when a template has no questions, it can be reset to not being dirty anymore.
			if (questionIds.length === 0) void this.rccQuestionnaireEditorService.reset(false)
			else void this.rccQuestionnaireEditorService.reset(true)

			const fallbackCategory: string = this.rccQuestionnaireEditorService.categoryForTemplateQuestions
			const categoryFromConfigMap: Map<string, QuestionRelation> = indexArray(relations, item => item.questionId)

			for (const questionId of questionIds)
				void this.questionnaireService.get(questionId).then(question => {
						this.rccQuestionnaireEditorService.addQuestion(
							question,
							categoryFromConfigMap.get(questionId)?.category ?? fallbackCategory,
						)
					}
				)

			if (this.updateNameFromTemplate) this.nameFormGroup.setValue({ name: meta.label })

			if (meta.reminder != null)
				this.reminderTime.next(meta.reminder)

			if (meta.defaultSchedule.daysOfWeek.length > 0)
				this.rccQuestionnaireEditorService.baseScheduleDays = meta.defaultSchedule.daysOfWeek

			if (questionIds.length === 0) setTimeout(() => {
				this._mode$.next('edit')
			}, 500)
			else setTimeout(() => {
				this._mode$.next('preview')
			}, 500)

		})
	}

	private async fillInSymptomCheck(existingSymptomCheck: ExistingSymptomCheck): Promise<void> {
		const questionIndex: Map<string, Question> = indexArray(existingSymptomCheck.questions, question => question.id)

		await this.rccQuestionnaireEditorService.reset()
		for (const questionRelation of existingSymptomCheck.symptomCheck.questionRelations) {
			if (questionRelation.questionId === DAILY_NOTES_ID)
				continue

			const question: Question = questionIndex.get(questionRelation.questionId)

			this.rccQuestionnaireEditorService.addQuestion({
				category: questionRelation.category ?? 'symptoms',
				question,
				schedule: questionRelation.schedule ?? null
			})
		}

		if (this.updateNameFromTemplate)
			this.nameFormGroup.setValue({ name: existingSymptomCheck.symptomCheck.config.meta.label })

		this._mode$.next('edit')

	}

	/**
	 * Subscribes to changes in the QuestionnaireEditorService's state. This was changed
	 * due to the new userflow that allows the user to switch the template via the dropdown menu.
	 * Toggles between 'edit' and 'init' modes based on:
	 * 1. The 'dirty' flag in the service
	 * 2. The number of questions
	 *
	 * Mode changes to 'edit' when:
	 * - The service becomes dirty (true)
	 * - There's more than one question
	 *
	 * Mode changes to 'init' when:
	 * - The service is not dirty (false)
	 * - There's one or no questions
	 */
	private subscribeForStatusChanges(): void {

		combineLatest(
			[
				this.rccQuestionnaireEditorService.dirty$,
				this.rccQuestionnaireEditorService.questions$,
			]
		).pipe(
			skip(2),
			distinctUntilChanged(),
			map(([dirty, questions]) => {
				if (dirty || questions.length > 1) return 'edit'
				else return 'init'
			}),
			filter(mode => this._mode$.value !== mode),
			takeUntil(this.unsubscribe$)
		)
			.subscribe((mode: 'edit' | 'preview' | 'init') => this._mode$.next(mode))
	}

	private subscribeToModeChanges(): void {

		const modeChange$		:	Observable<string>
								=	this.mode$
									.pipe(
										distinctUntilChanged(),
										takeUntil(this.unsubscribe$),
									)

		const toPreviewSwitche$	:	Observable<string>
								=	this.mode$
									.pipe(
										filter((mode: string) => mode === 'preview')
									)

		toPreviewSwitche$
		.subscribe( () => this.scrollToTop() )

		modeChange$
		.subscribe( () => {
			this.updateVisibleQuestionCategories()
		})


	}

	private scrollToTop(): void {
		this.rccFullPageComponent?.elementRef.nativeElement.scrollTo({ top: 0, behavior: 'smooth' })
	}

	// #endregion
}
