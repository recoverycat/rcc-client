import { TestBed } from '@angular/core/testing'
import { RccQuestionnaireEditorService, CategorizedQuestionWithSchedule, MonitoringSetupConfig, MONITORING_SETUP_CONFIG } from './questionnaire-editor.service'
import { Question, Schedule } from '@rcc/core'
import { QuestionnaireService } from '../questions'

describe('RccQuestionnaireEditorService', () => {
	let rccQuestionnaireEditorService: RccQuestionnaireEditorService = undefined!

	const mockMonitoringSetupConfig: MonitoringSetupConfig = {
		questionCategories: ['General', 'Symptoms', 'Medication', 'Lifestyle'],
		hiddenOnEditCategories: ['System'],
		hiddenInCatalogue: ['Administrative'],
		defaultQuestionsByCategory: new Map([
			['General', ['question1', 'question2']],
			['Other-Stuff', ['question3', 'question4']],
		]),
		addTemplateQuestionsTo: 'General',
		defaultScheduleConfig: [[0, 1, 2, 3, 4, 5, 6], ['09:00', '18:00']],
		catalogRestrictionsByCategory: {
			'Symptoms': { includeOnly: ['pain', 'fatigue', 'nausea'] },
			'Medication': { includeOnly: ['dosage', 'frequency'] }
		}
	}

	const mockPainQuestion: Question = new Question({
		id: '001',
		type: 'integer',
		meaning: 'Rate your pain level',
		translations: {
			en: 'Rate your pain level',
			de: 'Bewerten Sie Ihr Schmerzniveau'
		},
		options: [
			{ value: 0, translations: { en: 'No pain', de: 'Keine Schmerzen' } },
			{ value: 1, translations: { en: 'Mild', de: 'Leicht' } },
			{ value: 2, translations: { en: 'Moderate', de: 'Mäßig' } },
			{ value: 3, translations: { en: 'Severe', de: 'Stark' } },
			{ value: 4, translations: { en: 'Very severe', de: 'Sehr stark' } }
		]
	})

	const mockFatigueQuestion: Question = new Question({
		id: 'symptom-fatigue-001',
		type: 'integer',
		meaning: 'How is your energy level today?',
		translations: {
			en: 'How is your energy level today?',
			de: 'Wie ist Ihr Energielevel heute?'
		},
		options: [
			{ value: 0, translations: { en: 'Very low', de: 'Sehr niedrig' } },
			{ value: 1, translations: { en: 'Low', de: 'Niedrig' } },
			{ value: 2, translations: { en: 'Moderate', de: 'Mäßig' } },
			{ value: 3, translations: { en: 'High', de: 'Hoch' } },
			{ value: 4, translations: { en: 'Very high', de: 'Sehr hoch' } }
		]
	})

	const mockNauseaQuestion: Question = new Question({
		id: 'symptom-nausea-001',
		type: 'boolean',
		meaning: 'Are you experiencing nausea?',
		translations: {
			en: 'Are you experiencing nausea?',
			de: 'Haben Sie Übelkeit?'
		},
		options: [
			{ value: true, translations: { en: 'Yes', de: 'Ja' } },
			{ value: false, translations: { en: 'No', de: 'Nein' } }
		]
	})

	const mockMeditationQuestion: Question = new Question({
		id: 'resource-meditation-001',
		type: 'boolean',
		meaning: 'Did you meditate today?',
		translations: {
			en: 'Did you meditate today?',
			de: 'Haben Sie heute meditiert?'
		},
		options: [
			{ value: true, translations: { en: 'Yes', de: 'Ja' } },
			{ value: false, translations: { en: 'No', de: 'Nein' } }
		]
	})

	const mockExerciseQuestion: Question = new Question({
		id: 'resource-exercise-001',
		type: 'integer',
		meaning: 'How many minutes did you exercise today?',
		translations: {
			en: 'How many minutes did you exercise today?',
			de: 'Wie viele Minuten haben Sie heute trainiert?'
		},
		options: [
			{ value: 0, translations: { en: 'None', de: 'Keine' } },
			{ value: 1, translations: { en: '1-15 minutes', de: '1-15 Minuten' } },
			{ value: 2, translations: { en: '16-30 minutes', de: '16-30 Minuten' } },
			{ value: 3, translations: { en: '31-60 minutes', de: '31-60 Minuten' } },
			{ value: 4, translations: { en: 'More than 60 minutes', de: 'Mehr als 60 Minuten' } }
		]
	})

	const mockSocialSupportQuestion: Question = new Question({
		id: 'resource-social-001',
		type: 'boolean',
		meaning: 'Did you connect with a supportive person today?',
		translations: {
			en: 'Did you connect with a supportive person today?',
			de: 'Hatten Sie heute Kontakt mit einer unterstützenden Person?'
		},
		options: [
			{ value: true, translations: { en: 'Yes', de: 'Ja' } },
			{ value: false, translations: { en: 'No', de: 'Nein' } }
		]
	})

	const mockSchedule: Schedule = new Schedule([[0, 1, 2, 3, 4, 5, 6], ['09:00', '18:00']])

	const mockPainCategorizedQuestion: CategorizedQuestionWithSchedule = {
		question: mockPainQuestion,
		category: 'Symptoms',
		schedule: mockSchedule
	}

	const mockFatigueCategorizedQuestion: CategorizedQuestionWithSchedule = {
		question: mockFatigueQuestion,
		category: 'Symptoms',
		schedule: mockSchedule
	}

	const mockNauseaCategorizedQuestion: CategorizedQuestionWithSchedule = {
		question: mockNauseaQuestion,
		category: 'Symptoms',
		schedule: mockSchedule
	}

	const mockMeditationCategorizedQuestion: CategorizedQuestionWithSchedule = {
		question: mockMeditationQuestion,
		category: 'Resources',
		schedule: mockSchedule
	}

	const mockExerciseCategorizedQuestion: CategorizedQuestionWithSchedule = {
		question: mockExerciseQuestion,
		category: 'Resources',
		schedule: mockSchedule
	}

	const mockSocialSupportCategorizedQuestion: CategorizedQuestionWithSchedule = {
		question: mockSocialSupportQuestion,
		category: 'Resources',
		schedule: mockSchedule
	}
	/*eslint-disable */
	function logQuestions(questions: CategorizedQuestionWithSchedule[]) : void{
		const simplifiedQuestions: {
			id: string
		category: string | null
		meaning: string
	} [] = questions.map(q => ({
			id: q.question.id,
			category: q.category,
			meaning: q.question.meaning
		}))
		console.info('Questions:', JSON.stringify(simplifiedQuestions, null, 2))
	}
	/* eslint-enable */

	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				RccQuestionnaireEditorService,
				QuestionnaireService,
				{ provide: MONITORING_SETUP_CONFIG, useValue: mockMonitoringSetupConfig }]
		})
		rccQuestionnaireEditorService = TestBed.inject(RccQuestionnaireEditorService)

	})

	describe('reorderQuestion', () => {

		it('should reorder a question within the same category', () => {
			const initialQuestions: CategorizedQuestionWithSchedule[] = [
				mockPainCategorizedQuestion,
				mockFatigueCategorizedQuestion,
				mockNauseaCategorizedQuestion,
				mockMeditationCategorizedQuestion,
				mockExerciseCategorizedQuestion,
				mockSocialSupportCategorizedQuestion
			]

			rccQuestionnaireEditorService._questions$.next(initialQuestions)

			rccQuestionnaireEditorService.reorderQuestion('Symptoms', 0, 2)

			rccQuestionnaireEditorService.questions$.subscribe((questions: CategorizedQuestionWithSchedule[]) => {
				const symptomQuestions: CategorizedQuestionWithSchedule[] = questions.filter(q => q.category === 'Symptoms')

				expect(symptomQuestions[0].question.id).toBe(mockFatigueQuestion.id)
				expect(symptomQuestions[1].question.id).toBe(mockPainQuestion.id)
				expect(symptomQuestions[2].question.id).toBe(mockNauseaQuestion.id)
			})
		}, 1000)

		it('should not affect questions in other categories', () => {
			const initialQuestions: CategorizedQuestionWithSchedule[] = [
				mockPainCategorizedQuestion,
				mockFatigueCategorizedQuestion,
				mockNauseaCategorizedQuestion,
				mockMeditationCategorizedQuestion,
				mockExerciseCategorizedQuestion,
				mockSocialSupportCategorizedQuestion
			]

			rccQuestionnaireEditorService._questions$.next(initialQuestions)

			rccQuestionnaireEditorService.reorderQuestion('Symptoms', 0, 1)

			rccQuestionnaireEditorService.questions$.subscribe((questions: CategorizedQuestionWithSchedule[]) => {
				const resourceQuestions: CategorizedQuestionWithSchedule[] = questions.filter(q => q.category === 'Resources')

				expect(resourceQuestions[0].question.id).toBe(mockMeditationQuestion.id)
				expect(resourceQuestions[1].question.id).toBe(mockExerciseQuestion.id)
				expect(resourceQuestions[2].question.id).toBe(mockSocialSupportQuestion.id)
			})
		})
	})

	describe('moveQuestion', () => {

		it('should move a question to a different category to the right position', () => {
			const initialQuestions: CategorizedQuestionWithSchedule[] = [
				mockPainCategorizedQuestion,
				mockFatigueCategorizedQuestion,
				mockNauseaCategorizedQuestion,
				mockMeditationCategorizedQuestion,
				mockExerciseCategorizedQuestion,
				mockSocialSupportCategorizedQuestion
			]

			rccQuestionnaireEditorService._questions$.next(initialQuestions)

			rccQuestionnaireEditorService.moveQuestion('001', 'Resources', 1)

			rccQuestionnaireEditorService.questions$.subscribe((questions: CategorizedQuestionWithSchedule[]) => {
				const resourceQuestions: CategorizedQuestionWithSchedule[] = questions.filter(q => q.category === 'Resources')

				expect(resourceQuestions[0].question.id).toBe(mockMeditationQuestion.id)
				expect(resourceQuestions[1].question.id).toBe(mockPainQuestion.id)
				expect(resourceQuestions[2].question.id).toBe(mockExerciseQuestion.id)
			})
		})

		it('should remove the moved question from the other category', () => {
			const initialQuestions: CategorizedQuestionWithSchedule[] = [
				mockPainCategorizedQuestion,
				mockFatigueCategorizedQuestion,
				mockNauseaCategorizedQuestion,
				mockMeditationCategorizedQuestion,
				mockExerciseCategorizedQuestion,
				mockSocialSupportCategorizedQuestion
			]

			rccQuestionnaireEditorService._questions$.next(initialQuestions)

			rccQuestionnaireEditorService.moveQuestion('001', 'Symptoms', 1)

			rccQuestionnaireEditorService.questions$.subscribe((questions: CategorizedQuestionWithSchedule[]) => {
				const symptomQuestions: CategorizedQuestionWithSchedule[] = questions.filter(q => q.category === 'Symptoms')

				const resourceQuestions: CategorizedQuestionWithSchedule[] = questions.filter(q => q.category === 'Resources')

				expect(symptomQuestions.length).toBe(3)
				expect(resourceQuestions.length).toBe(3)

				expect(resourceQuestions.some(q => q.question.id === mockPainQuestion.id)).toBe(false)


			})
		})
	})

	describe('sortByCategory', () => {
		it('should use the default category order from monitoringSetupConfig when categoryOrder is not provided', () => {
			const sortTestMockMonitoringSetupConfig: MonitoringSetupConfig = {
				questionCategories: ['General', 'Symptoms', 'Medication', 'Lifestyle'],
				hiddenOnEditCategories: ['System'],
				hiddenInCatalogue: ['Administrative'],
				defaultQuestionsByCategory: new Map([
					['General', ['question1', 'question2']],
					['Other-Stuff', ['question3', 'question4']],
				]),
				addTemplateQuestionsTo: 'General',
				defaultScheduleConfig: [[0, 1, 2, 3, 4, 5, 6], ['09:00', '18:00']],
				catalogRestrictionsByCategory: {
					'Symptoms': { includeOnly: ['pain', 'fatigue', 'nausea'] },
					'Medication': { includeOnly: ['dosage', 'frequency'] }
				}
			}
			rccQuestionnaireEditorService['monitoringSetupConfig'] = sortTestMockMonitoringSetupConfig

			const mockQuestions: CategorizedQuestionWithSchedule[] = [
			{ question: new Question({
				id: '001',
				type: 'integer',
				meaning: 'Rate your pain level',
				translations: {
					en: 'Rate your pain level',
					de: 'Bewerten Sie Ihr Schmerzniveau'
				},
				options: [
					{ value: 0, translations: { en: 'No pain', de: 'Keine Schmerzen' } },
					{ value: 1, translations: { en: 'Mild', de: 'Leicht' } },
					{ value: 2, translations: { en: 'Moderate', de: 'Mäßig' } },
					{ value: 3, translations: { en: 'Severe', de: 'Stark' } },
					{ value: 4, translations: { en: 'Very severe', de: 'Sehr stark' } }
				]
				}), category: 'Symptoms', schedule: null },
			{ question: new Question({
				id: '002',
				type: 'integer',
				meaning: 'Rate your pain level',
				translations: {
					en: 'Rate your pain level',
					de: 'Bewerten Sie Ihr Schmerzniveau'
				},
				options: [
					{ value: 0, translations: { en: 'No pain', de: 'Keine Schmerzen' } },
					{ value: 1, translations: { en: 'Mild', de: 'Leicht' } },
					{ value: 2, translations: { en: 'Moderate', de: 'Mäßig' } },
					{ value: 3, translations: { en: 'Severe', de: 'Stark' } },
					{ value: 4, translations: { en: 'Very severe', de: 'Sehr stark' } }
				]
			}), category: 'General', schedule: null },
			{ question: new Question({
				id: '003',
				type: 'integer',
				meaning: 'Rate your pain level',
				translations: {
					en: 'Rate your pain level',
					de: 'Bewerten Sie Ihr Schmerzniveau'
				},
				options: [
					{ value: 0, translations: { en: 'No pain', de: 'Keine Schmerzen' } },
					{ value: 1, translations: { en: 'Mild', de: 'Leicht' } },
					{ value: 2, translations: { en: 'Moderate', de: 'Mäßig' } },
					{ value: 3, translations: { en: 'Severe', de: 'Stark' } },
					{ value: 4, translations: { en: 'Very severe', de: 'Sehr stark' } }
				]
			}), category: 'Lifestyle', schedule: null },
			{ question: new Question({
				id: '004',
				type: 'integer',
				meaning: 'Rate your pain level',
				translations: {
					en: 'Rate your pain level',
					de: 'Bewerten Sie Ihr Schmerzniveau'
				},
				options: [
					{ value: 0, translations: { en: 'No pain', de: 'Keine Schmerzen' } },
					{ value: 1, translations: { en: 'Mild', de: 'Leicht' } },
					{ value: 2, translations: { en: 'Moderate', de: 'Mäßig' } },
					{ value: 3, translations: { en: 'Severe', de: 'Stark' } },
					{ value: 4, translations: { en: 'Very severe', de: 'Sehr stark' } }
				]
			}), category: 'Medication', schedule: null }
			]

			const result: CategorizedQuestionWithSchedule[] = rccQuestionnaireEditorService.sortByCategory(mockQuestions)

			expect(result.length).toBe(4)
			expect(result[0].question.id).toBe('002')
			expect(result[1].question.id).toBe('001')
			expect(result[2].question.id).toBe('004')
			expect(result[3].question.id).toBe('003')
		})

		it('should maintain the original order of questions within the same category', () => {
			const questions: CategorizedQuestionWithSchedule[] = [
				{ question: new Question({
					id: '01',
				type: 'integer',
				meaning: 'Rate your pain level',
				translations: {
					en: 'Rate your pain level',
					de: 'Bewerten Sie Ihr Schmerzniveau'
				},
				options: [
					{ value: 0, translations: { en: 'No pain', de: 'Keine Schmerzen' } },
					{ value: 1, translations: { en: 'Mild', de: 'Leicht' } },
					{ value: 2, translations: { en: 'Moderate', de: 'Mäßig' } },
					{ value: 3, translations: { en: 'Severe', de: 'Stark' } },
					{ value: 4, translations: { en: 'Very severe', de: 'Sehr stark' } }
				]
				}), category: 'A', schedule: null },
				{ question: new Question({
					id: '02',
				type: 'integer',
				meaning: 'Rate your pain level',
				translations: {
					en: 'Rate your pain level',
					de: 'Bewerten Sie Ihr Schmerzniveau'
				},
				options: [
					{ value: 0, translations: { en: 'No pain', de: 'Keine Schmerzen' } },
					{ value: 1, translations: { en: 'Mild', de: 'Leicht' } },
					{ value: 2, translations: { en: 'Moderate', de: 'Mäßig' } },
					{ value: 3, translations: { en: 'Severe', de: 'Stark' } },
					{ value: 4, translations: { en: 'Very severe', de: 'Sehr stark' } }
				]
				}), category: 'B', schedule: null },
				{ question: new Question({
					id: '03',
				type: 'integer',
				meaning: 'Rate your pain level',
				translations: {
					en: 'Rate your pain level',
					de: 'Bewerten Sie Ihr Schmerzniveau'
				},
				options: [
					{ value: 0, translations: { en: 'No pain', de: 'Keine Schmerzen' } },
					{ value: 1, translations: { en: 'Mild', de: 'Leicht' } },
					{ value: 2, translations: { en: 'Moderate', de: 'Mäßig' } },
					{ value: 3, translations: { en: 'Severe', de: 'Stark' } },
					{ value: 4, translations: { en: 'Very severe', de: 'Sehr stark' } }
				]
				}), category: 'A', schedule: null },
				{ question: new Question({
					id: '04',
				type: 'integer',
				meaning: 'Rate your pain level',
				translations: {
					en: 'Rate your pain level',
					de: 'Bewerten Sie Ihr Schmerzniveau'
				},
				options: [
					{ value: 0, translations: { en: 'No pain', de: 'Keine Schmerzen' } },
					{ value: 1, translations: { en: 'Mild', de: 'Leicht' } },
					{ value: 2, translations: { en: 'Moderate', de: 'Mäßig' } },
					{ value: 3, translations: { en: 'Severe', de: 'Stark' } },
					{ value: 4, translations: { en: 'Very severe', de: 'Sehr stark' } }
				]
				}), category: 'C', schedule: null },
				{ question: new Question({
					id: '05',
				type: 'integer',
				meaning: 'Rate your pain level',
				translations: {
					en: 'Rate your pain level',
					de: 'Bewerten Sie Ihr Schmerzniveau'
				},
				options: [
					{ value: 0, translations: { en: 'No pain', de: 'Keine Schmerzen' } },
					{ value: 1, translations: { en: 'Mild', de: 'Leicht' } },
					{ value: 2, translations: { en: 'Moderate', de: 'Mäßig' } },
					{ value: 3, translations: { en: 'Severe', de: 'Stark' } },
					{ value: 4, translations: { en: 'Very severe', de: 'Sehr stark' } }
				]
				}), category: 'B', schedule: null },
				{ question: new Question({
					id: '06',
				type: 'integer',
				meaning: 'Rate your pain level',
				translations: {
					en: 'Rate your pain level',
					de: 'Bewerten Sie Ihr Schmerzniveau'
				},
				options: [
					{ value: 0, translations: { en: 'No pain', de: 'Keine Schmerzen' } },
					{ value: 1, translations: { en: 'Mild', de: 'Leicht' } },
					{ value: 2, translations: { en: 'Moderate', de: 'Mäßig' } },
					{ value: 3, translations: { en: 'Severe', de: 'Stark' } },
					{ value: 4, translations: { en: 'Very severe', de: 'Sehr stark' } }
				]
				}), category: 'A', schedule: null },
			]


			const categoryOrder: string[] = ['A', 'B', 'C']

			const result: CategorizedQuestionWithSchedule[] = rccQuestionnaireEditorService.sortByCategory(questions, categoryOrder)

			expect(result.map(q => q.question.id)).toEqual(['01', '03', '06', '02', '05', '04'])
		})

		it('should handle an empty array of questions correctly', () => {
			const emptyQuestions: CategorizedQuestionWithSchedule[] = []
			const categoryOrder: string[] = ['Category1', 'Category2']

			const result: CategorizedQuestionWithSchedule[] = rccQuestionnaireEditorService['sortByCategory'](emptyQuestions, categoryOrder)

			expect(result).toEqual([])
			expect(result.length).toBe(0)
		})
	})
})
