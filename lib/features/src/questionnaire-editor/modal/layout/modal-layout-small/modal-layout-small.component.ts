import { Component, Input } from '@angular/core'
import { AbstractControl, ControlContainer, FormGroup, FormGroupDirective } from '@angular/forms'
import { RccModalController, SharedModule } from '@rcc/common'

@Component({
    templateUrl: 'modal-layout-small.component.html',
    styleUrls: ['./modal-layout-small.component.scss'],
    imports: [SharedModule],
    selector: 'rcc-monitoring-setup-modal-layout-small',
    viewProviders: [
        {
            provide: ControlContainer,
            useExisting: FormGroupDirective,
        }
    ]
})
export class ModalLayoutSmallComponent<T extends Record<string, AbstractControl>> {
	@Input()
	public formGroup: FormGroup<T>

	public constructor(
		private readonly rccModalController	: RccModalController,
	) {}

	protected onSubmit(): void {
		this.rccModalController.dismiss(this.formGroup.value)
	}

	protected cancelClick(): void {
		this.rccModalController.dismiss()
	}
}
