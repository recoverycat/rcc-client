import	{
			Component,
			Input,
			OnInit,
		} 									from '@angular/core'
import	{
			FormControl,
			FormGroup,
		}									from '@angular/forms'
import	{	RccModalController,
			RccTranslationService,
			SharedModule,
			WidgetsModule
		}									from '@rcc/common'
import	{	AnswerType, Question, QuestionOptionConfig, Schedule, TranslationList, uuidv4					}	from '@rcc/core'
import	{	FormGroupOf					}	from '@rcc/core/types'
import 	{	DrugListService				}	from '@rcc/features/medication/medication-question-edit-widget/drug-list.service'
import	{	ModalLayoutLargeComponent	}	from '../layout/modal-layout-large/modal-layout-large.component'
import	{	EditScheduleModalComponent	}	from '../edit-schedule'
import { QueryControl } from '@rcc/features/queries'

export interface FormData {
	questionTitle:	string,
	schedule:		Schedule,
	answerType:		AnswerType,
	options:		QuestionOptionConfig[]
	dosage:			string,
	medication:		string,
	translations:	TranslationList,
	tags:			string[],
}

type FormGroupType = FormGroupOf<FormData>

@Component({
    templateUrl: './edit-medication-modal.component.html',
    styleUrls: ['./edit-medication-modal.component.scss'],
    imports: [
        WidgetsModule,
        ModalLayoutLargeComponent,
        SharedModule
    ]
})
export class EditMedicationModalComponent implements OnInit {

	private language: string = this.translationService.activeLanguage

	public constructor (
		private drugListService				: DrugListService,
		private translationService			: RccTranslationService,
		private rccModalController			: RccModalController,
	) {}

	public ngOnInit(): void {
		this.formGroup.get('medication').valueChanges.subscribe(() => this.onMedicationOrDosageChange())
		this.formGroup.get('dosage').valueChanges.subscribe(() => this.onMedicationOrDosageChange())
		this.toggleScheduleControl.valueChanges.subscribe((enabled) => {
			if (!enabled)
				this.formGroup.get('schedule').setValue(null)
		})
		this.queryControl.disableForms()
	}

	@Input()
	public set schedule(value: Schedule) {
		if (value == null)
			return
		this.toggleScheduleControl.setValue(true)
		this.formGroup.get('schedule').setValue(value)
	}

	@Input()
	public category: string

	@Input()
	public set question(value: Question) {
		const formData: FormData = this.splitUpQuestion(value)
		this.formGroup.setValue(formData)

		this.formGroup.get('options').setValue(value.options)
	}

	@Input()
	public baseScheduleDays: number[]


	private splitUpQuestion(value: Question): FormData {
		const options: QuestionOptionConfig[]
			= this.formGroup.get('options').value as QuestionOptionConfig[]
		const schedule: Schedule
			= this.formGroup.get('schedule').value as Schedule
		const translations: TranslationList
			= this.formGroup.get('translations').value as TranslationList
		const answerType: AnswerType = 'boolean'

		const questionTitle: string = value.translations[this.language]
		const unitInformation: string[] = value.unit ? value.unit.split(', ') : undefined
		let dosage: string = ''
		let medication: string = ''

		if(unitInformation) {
			const firstGroup: string = unitInformation[0]
			const secondGroup: string = unitInformation[1]
			if(!secondGroup)
				if(this.drugNameList.includes(firstGroup))
					medication = firstGroup
				else
					dosage = firstGroup
			else {
				medication = firstGroup
				dosage = secondGroup
			}
		}
		const tags: string[] = this.formGroup.get('tags').value as string[]
		return {
			options,
			schedule,
			answerType,
			questionTitle,
			dosage,
			medication,
			translations,
			tags
		}
	}


	// #region formcontrol
	protected formGroup: FormGroup<FormGroupType> = new FormGroup<FormGroupType>({
		questionTitle:	new FormControl(this.translationService.translate('QUESTIONNAIRE_EDITOR.MEDICATION.NEW_QUESTION_DEFAULT')),
		translations:	new FormControl(this.translationService.translationList('QUESTIONNAIRE_EDITOR.MEDICATION.NEW_QUESTION_DEFAULT')),
		dosage:			new FormControl(''),
		medication:		new FormControl(''),
		schedule: 		new FormControl<Schedule>(null),
		answerType:		new FormControl<AnswerType>('boolean'),
		options:		new FormControl<QuestionOptionConfig[]>([
			{ value: true,	translations: this.translationService.translationList('YES') },
			{ value: false,	translations: this.translationService.translationList('NO') },
		]),
		tags:			new FormControl<string[]>(['rcc-mediaction'])
	})
	private get questionTitle(): string {
		return this.formGroup.get('questionTitle').value as string
	}
	private set questionTitle(value: string) {
		this.formGroup.get('questionTitle').setValue(value)
	}
	private get dosage(): string {
		return this.formGroup.get('dosage').value as string
	}
	private set dosage(value: string) {
		this.formGroup.get('dosage').setValue(value)
	}
	private get medication(): string {
		return this.formGroup.get('medication').value as string
	}
	private set medication(value: string) {
		this.formGroup.get('medication').setValue(value)
	}
	private get translations(): TranslationList {
		return this.formGroup.get('translations').value as TranslationList
	}
	private get options(): QuestionOptionConfig[] {
		return this.formGroup.get('options').value as QuestionOptionConfig[]
	}

	// #endregion

	private drugNameList: string[] = this.drugListService.getListOfDrugNames()
	public medications: {value: string, label: string }[] = this.drugNameList.map((drug: string) => (
		{ value: drug, label: drug }
	))

	// #region (medication, dosage) logic
	private getMedicationAndDosageString(medication: string, dosage: string): string {
		const medicationAndDosage: string = [medication, dosage].filter((value: string) => value !== '').join(', ')

		return medicationAndDosage ? '(' + medicationAndDosage + ')' : medicationAndDosage
	}

	private addMedicationAndDosage(currentValue: string, medication: string, dosage: string): string {
		return currentValue + ' ' + this.getMedicationAndDosageString(medication, dosage)
	}

	private onMedicationOrDosageChange(): void {
		if (!this.medication && !this.dosage) {
			this.transformSubmissionData = undefined
			return
		}

		this.transformSubmissionData = (formGroup) => {
			const data: Partial<FormData & { unitText: string }> = formGroup.value

			data.unitText = [this.medication, this.dosage].filter(v => !!v).join(', ')

			return data
		}
	}

	// #region schedule
	protected toggleScheduleControl: FormControl<boolean> = new FormControl(false)

	/**
	 * after the editSchedule toggle has changed we check the new value and if it is true open the modal
	 */
	protected toggleEditSchedule(): void {
		const currentValue: boolean = this.toggleScheduleControl.value
		if (currentValue === true)
			void this.editSchedule()
	}

	protected async editSchedule(): Promise<void> {
		const currentSchedule: Schedule | null = this.formGroup.get('schedule').value as Schedule | null
		const result: { schedule: number[] } | null = await this.rccModalController.present(
			EditScheduleModalComponent,
			{ schedule: currentSchedule?.daysOfWeek ?? this.baseScheduleDays ?? [] },
			{ mini: true },
		)

		if (result == null && currentSchedule == null)
			this.toggleScheduleControl.setValue(false)

		if (result == null)
			return

		const schedule: Schedule = new Schedule([result.schedule, []])
		this.formGroup.get('schedule').setValue(schedule)
		this.toggleScheduleControl.setValue(true)
	}
	// #endregion

	protected queryControl: QueryControl = new QueryControl(
		new Question({
			id: uuidv4(),
			translations: this.translations,
			options: this.options,
			type: 'boolean'
		}),
		() => undefined, // Do not do anything on submission
		() => undefined // Do not do anything on clean up
	)

	protected get questionTitleDisplayValue(): string {
		return this.addMedicationAndDosage(
			this.questionTitle,
			this.medication,
			this.dosage
		)
	}

	protected transformSubmissionData: (formData: FormGroup<FormGroupType>) => Partial<FormData & { unitText: string }>
}
