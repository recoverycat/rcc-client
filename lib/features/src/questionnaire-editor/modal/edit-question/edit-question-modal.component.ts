import	{
			Component,
			Inject,
			Input,
		} 										from '@angular/core'
import	{
			FormControl,
			FormGroup,
		}										from '@angular/forms'
import	{
			RccModalController,
			RccTranslationService,
			SharedModule,
			Action,
			WidgetsModule,
		}										from '@rcc/common'
import	{
			Validators,
		}									from '@angular/forms'
import	{
			AnswerType,
			Question,
			QuestionOptionConfig,
			Schedule,
		}									from '@rcc/core'
import	{	FormGroupOf					}	from '@rcc/core/types'
import	{	MonitoringSetupControl		}	from '@rcc/features/monitoring-setup-question-widgets'
import	{	MONITORING_SETUP_QUESTION	}	from '@rcc/features/monitoring-setup-question-widgets/monitoring-setup-question.common'
import	{	EditScheduleModalComponent	}	from '../edit-schedule/edit-schedule-modal.component'
import	{	ModalLayoutLargeComponent	}	from '../layout/modal-layout-large/modal-layout-large.component'
import	{	OptionEditorComponent		}	from './option-editor/option-editor.component'
import	{	QueryControl				}	from '@rcc/features/queries'

export interface FormData {
	questionTitle	: string,
	options			: QuestionOptionConfig[],
	answerType		: AnswerType,
	schedule		: Schedule,
	min				: number,
	max				: number,
	tags 			: string[],
	unitText		: string,
}

type FormGroupType = FormGroupOf<FormData>

@Component({
    templateUrl: './edit-question-modal.component.html',
    styleUrls: ['./edit-question-modal.component.scss'],
    imports: [
        ModalLayoutLargeComponent,
        OptionEditorComponent,
        SharedModule,
        WidgetsModule,
    ]
})
export class EditQuestionModalComponent {

	public  switchToQuestionListAction		: Action = {
		handler: () => this.cancelAndOpenQuestionList(),
		label:	'QUESTIONNAIRE_EDITOR.QUESTION_LIST',
		icon:	'edit'
	}

	public constructor (
		private translationService			: RccTranslationService,
		@Inject(MONITORING_SETUP_QUESTION)
		private questionTypes				: MonitoringSetupControl[],
		private rccModalController			: RccModalController,
	) {
		this.questionTypeOptions = questionTypes.map((type, index) => ({
			value: index,
			label: type.label,
		}))
		this.setQuestionType()
		this.questionTypeControl.valueChanges.subscribe((value) => this.setQuestionType(value))

		this.toggleScheduleControl.valueChanges.subscribe((enabled) => {
			if (!enabled)
				this.formGroup.get('schedule').setValue(null)
		})
	}

	protected showValueIndicator: boolean = false

	protected valueIndicators: number[] = []

	private setQuestionType(value: number = this.questionTypeControl.value): void {
		const questionType: MonitoringSetupControl = this.questionTypes[value]
		this.showWidgetIfNeeded(questionType)
		this.showUnitInput = questionType.unitInput

		this.showValueIndicator = this.showValueIndicatorForAnswerOptions(questionType)

		if (!this.showUnitInput)
			this.transformSubmissionData = (formGroup) => {
				const result: Partial<FormData> = formGroup.value
				result.unitText = undefined
				return result
			}
		else
			this.transformSubmissionData = undefined


		if (this.question != null && questionType.questionMatch(this.question)) {
			this.formGroup.get('options').setValue(this.question.options)
			this.formGroup.get('answerType').setValue(this.question.type)
			return
		}
		this.formGroup.get('options').setValue(questionType.options)
		this.formGroup.get('answerType').setValue(questionType.answerType)
		this.formGroup.get('min').setValue(questionType.min)
		this.formGroup.get('max').setValue(questionType.max)
		this.formGroup.get('tags').setValue(questionType.tags || [])

	}

	protected showValueIndicatorForAnswerOptions(questionType: MonitoringSetupControl): boolean {
		return this.isQuantityQuestion(questionType) || this.isYesNoQuestion(questionType)
	}


	protected isYesNoQuestion(questionType: MonitoringSetupControl): boolean {
		return questionType.answerType === 'boolean'
	}

	protected isQuantityQuestion(questionType: MonitoringSetupControl): boolean {
		return questionType.answerType === 'integer' && questionType.options.length === 4
	}

	private getDummyScaleQuestionControl(questionType: MonitoringSetupControl): QueryControl {
		return new QueryControl(
			new Question(	{
				id		:	'A',
				type	:	questionType.answerType,
				translations: { 'en':	'dummy string' },
				min		:	questionType.min,
				max		:	questionType.max,
				tags	:	questionType.tags
			}),
			() => undefined,
			() => undefined
		)
	}

	private showWidgetIfNeeded(questionType: MonitoringSetupControl): void {
		if(questionType.showWidget) {
			this.widgetControl = this.getDummyScaleQuestionControl(questionType)
			this.widgetControl.disableForms()
			this.showWidget = true

		} else
			this.showWidget = false

	}

	protected questionTypeControl: FormControl<number> = new FormControl(2)
	protected questionTypeOptions: Array<{ value: number, label: string }>

	private language: string = this.translationService.activeLanguage

	private _question: Question
	public get question(): Question {
		return this._question
	}

	@Input()
	public set question(value: Question) {
		this._question = value
		this.formGroup.get('questionTitle').setValue(value.translations[this.language])
		this.formGroup.get('options').setValue(value.options)
		this.formGroup.get('answerType').setValue(value.type)
		this.formGroup.get('min').setValue(value.min)
		this.formGroup.get('max').setValue(value.max)
		this.formGroup.get('tags').setValue(value.tags)
		this.formGroup.get('unitText').setValue(value.unit ?? '')
		const matchingQuestionType: number = this.questionTypes.findIndex((type) => type.questionMatch(value))
		if (matchingQuestionType !== -1)
			this.questionTypeControl.setValue(matchingQuestionType)
	}

	@Input()
	public set schedule(value: Schedule) {
		if (value == null)
			return
		this.toggleScheduleControl.setValue(true)
		this.formGroup.get('schedule').setValue(value)
	}

	@Input()
	public category: string

	@Input()
	public baseScheduleDays: number[]


	private optionIsValid(questionOption: QuestionOptionConfig): boolean {
		const hasLabelInActiveLanguage: boolean = questionOption.translations[this.translationService.activeLanguage] > ''
		return hasLabelInActiveLanguage
	}

	protected formGroup: FormGroup<FormGroupType> = new FormGroup<FormGroupType>({
		questionTitle: new FormControl('',[Validators.required]),
		options: new FormControl<QuestionOptionConfig[]>([], (control: FormControl<QuestionOptionConfig[]>) => {
			const value: QuestionOptionConfig[] = control.value
			const allValuesSet: boolean = value.find((item) => !this.optionIsValid(item)) == null
			return allValuesSet ? null : { invalidOptions: true }
		}),
		answerType: new FormControl<AnswerType>('boolean'),
		schedule: new FormControl<Schedule>(null),
		min: new FormControl<number>(null),
		max: new FormControl<number>(null),
		tags: new FormControl<string[]>([]),
		unitText: new FormControl<string>(''),
	})

	protected async editSchedule(): Promise<void> {
		const currentSchedule: Schedule | null = this.formGroup.get('schedule').value as Schedule | null
		const result: { schedule: number[] } | null = await this.rccModalController.present(
			EditScheduleModalComponent,
			{
				schedule: currentSchedule?.daysOfWeek ?? this.baseScheduleDays ?? [],
				title: 	'QUESTIONNAIRE_EDITOR.SCHEDULE.LABEL.INDIVIDUAL'
			},
			{ mini: true },
		)

		if (result == null && currentSchedule == null)
			this.toggleScheduleControl.setValue(false)

		if (result == null)
			return

		const schedule: Schedule = new Schedule([result.schedule, []])
		this.formGroup.get('schedule').setValue(schedule)
		this.toggleScheduleControl.setValue(true)
	}

	protected toggleScheduleControl: FormControl<boolean> = new FormControl(false)

	/**
	 * after the editSchedule toggle has changed we check the new value and if it is true open the modal
	 */
	protected toggleEditSchedule(): void {
		const currentValue: boolean = this.toggleScheduleControl.value
		if (currentValue === true)
			void this.editSchedule()
	}
/**
 * The `dismiss` function outputs a string as a parameter. This string is the name of
 * the component that you want to open.
 */
	public cancelAndOpenQuestionList() : void {
		this.rccModalController.dismiss('question-list')
	}

	protected showWidget: boolean = false
	protected showUnitInput: boolean = false
	protected widgetControl: QueryControl

	protected get questionTitleDisplayValue(): string {
		if (!this.showUnitInput)
			return

		const questionTitle: string = this.formGroup.get('questionTitle').value as string
		const unitText: string = this.formGroup.get('unitText').value as string

		if (questionTitle && unitText)
			return `${questionTitle} (${unitText})`
		return questionTitle
	}

	protected transformSubmissionData: (FormGroup: FormGroup<FormGroupType>) => Partial<FormData>
}
