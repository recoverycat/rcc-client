import 	{
			Component,
			Input,
			OnInit,
			Type
		} 										from '@angular/core'
import 	{
			FormControl,
			FormGroup
		} 										from '@angular/forms'
import 	{
			Action,
			RccModalController,
			RccTranslationService,
			SharedModule
		} 										from '@rcc/common'
import 	{
			AnswerType,
			Question,
			QuestionOptionConfig,
			Schedule,
			TranslationList,
			uuidv4
		} 										from '@rcc/core'
import 	{
			FormGroupOf
		} 										from '@rcc/core/types'
import 	{
			IonicModalsModule
		} 										from '@rcc/ionic'
import 	{
			RccThemeModule
		} 										from '@rcc/themes/active'
import 	{
			FormData as EditQuestionFormData,
			EditQuestionModalComponent,
		} 										from '../edit-question/edit-question-modal.component'
import 	{
			ModalLayoutLargeComponent
		} 										from '../layout/modal-layout-large/modal-layout-large.component'
import 	{
			QuestionListControlComponent
		} 										from './question-list-control/question-list-control.component'
import	{
			EditMedicationModalComponent,
		}										from '../edit-medication/edit-medication-modal.component'

export interface AddSelectedQuestionsFormData {
	selectedQuestions 	: string[]
	customQuestion?		: Question
	schedule			: Schedule
}

export interface AddQuestionFormData {
	questionTitle:	string,
	answerType:		AnswerType,
	options:		QuestionOptionConfig[]
	schedule:		Schedule,
	translations?:	TranslationList,
	min?			: number,
	max?			: number,
	tags? 			: string[],
}

type FormGroupType = FormGroupOf<Omit<AddSelectedQuestionsFormData, 'schedule'>>

@Component({
    templateUrl: './add-question-modal.component.html',
    styleUrls: ['./add-question-modal.component.scss'],
    imports: [
        SharedModule,
        QuestionListControlComponent,
        IonicModalsModule,
        RccThemeModule,
        ModalLayoutLargeComponent
    ]
})
export class AddQuestionModalComponent implements OnInit {
	@Input()
	public questions: Question[]

	@Input()
	public set selected(value: string[]) {
		this.formGroup.setValue({
			selectedQuestions: value
		})
	}

	@Input()
	public disabledIds: string[]

	@Input()
	public category: string



	protected get isMedicationQuestion(): boolean {
		return this.category === 'medication'
	}

	@Input()
	public catalogRestrictionsByCategory: Record<string, { includeOnly: string[] }>


	@Input()
	public baseScheduleDays: number[] = []

	public constructor(
		private readonly rccModalController	: RccModalController,
		private readonly translationService	: RccTranslationService
	) {}

	public ngOnInit(): void {
		if(this.isMedicationQuestion)
			this.addCustomQuestion().catch(() => {
				this.rccModalController.dismiss(null)
			})
	}

	protected formGroup : FormGroup<FormGroupType> = new FormGroup<FormGroupType>({
		selectedQuestions: new FormControl([])
	})

	protected customQuestionAction : Action = {
		handler		: async () => this.addCustomQuestion(),
		icon		: undefined,
		label		: 'QUESTIONNAIRE_EDITOR.CUSTOM_QUESTION'
	}

	private async addCustomQuestion(): Promise<void> {
			const result 	: EditQuestionFormData | undefined
							= await this.rccModalController.present(
								this.isMedicationQuestion ? EditMedicationModalComponent : EditQuestionModalComponent as Type<unknown>,
								{
									category: this.category,
									baseScheduleDays: this.baseScheduleDays
								}
							)

			if (result === null) return

			const question : Question = new Question({
				id				: 	`rcc-custom-${uuidv4()}`,
				meaning			: 	result.questionTitle,
				translations	: 	{
										[this.translationService.activeLanguage]: result.questionTitle
									},
				type			: 	result.answerType,
				options			: 	result.options,
				min				:	result.min,
				max				:	result.max,
				tags			: 	result.tags,
				unit			:	result.unitText,
			})

			const formData : AddSelectedQuestionsFormData = {
				selectedQuestions	: 	[],
				customQuestion		: 	question,
				schedule			: 	result.schedule
			}

			this.rccModalController.dismiss(formData)
	}
}
