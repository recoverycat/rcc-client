import { TestBed } from '@angular/core/testing'
import { QuestionListControlComponent } from './question-list-control.component'
import { ItemSelectionFilter, RccTranslationService } from '@rcc/common'
import { Question } from '@rcc/core'


describe('QuestionListControlComponent', () => {
	let component: QuestionListControlComponent = undefined!
	let questions: Question[] = undefined!

	beforeEach(() => {

		TestBed.configureTestingModule({
			providers: [
				QuestionListControlComponent,
				RccTranslationService
			]
		})

		component = TestBed.inject(QuestionListControlComponent)

		questions = [
			{ id: '1', tags: ['tag1', 'tag2'] },
			{ id: '2', tags: ['tag2', 'tag3'] },
			{ id: '3', tags: ['tag1', 'tag3'] }
		] as Question[]
	})

	describe('filterCategoriesWithCount$', () => {
		it('should return filtered categories with correct counts', () => {

			component.questions = questions

			component['itemSelectionFilters'] = [
				{ representation: { label: 'FILTER.TAG1' } },
				{ representation: { label: 'FILTER.TAG2' } },
				{ representation: { label: 'FILTER.TAG3' } }
			] as ItemSelectionFilter[]

			component.filterCategoriesWithCount$.subscribe((result) => {
				expect(result).toEqual([
					{ value: 'tag1', label: 'FILTER.TAG1 (2)' },
					{ value: 'tag2', label: 'FILTER.TAG2 (2)' },
					{ value: 'tag3', label: 'FILTER.TAG3 (2)' }
				])
			})
		})

		it('should apply catalog restrictions when set', () => {

			component.questions = questions

			component['itemSelectionFilters'] = [
				{ representation: { label: 'FILTER.TAG1' } },
				{ representation: { label: 'FILTER.TAG2' } },
				{ representation: { label: 'FILTER.TAG3' } }
			] as ItemSelectionFilter[]

			component.category = 'testCategory'
			component.catalogRestrictionsByCategory = {
				testCategory: { includeOnly: ['tag1', 'tag2'] }
			}

			component.filterCategoriesWithCount$.subscribe((result) => {
				expect(result).toEqual([
					{ value: 'tag1', label: 'FILTER.TAG1 (2)' },
					{ value: 'tag2', label: 'FILTER.TAG2 (2)' }
				])
			})
		})

		it('should update category control when only one option is available', () => {

			component.questions = questions

			component['itemSelectionFilters'] = [
				{ representation: { label: 'FILTER.TAG1' } }
			] as ItemSelectionFilter[]

			const spy : jasmine.Spy = spyOn(component['categoryControl'], 'setValue')

			component.filterCategoriesWithCount$.subscribe(() => {
				expect(spy).toHaveBeenCalledWith('tag1', { emitEvent: true })
			})
		})
	})

	describe('filteredQuestions$', () => {
		it('should filter questions based on search and category', () => {
			component.questions = questions

			component['searchFilter$'].next('')
			component['categoryFilter$'].next('tag1')

			component.filteredQuestions$.subscribe(filteredQuestions => {

				expect(filteredQuestions.length).toBe(2)
				expect(filteredQuestions[0].id).toBe('1')
				expect(filteredQuestions[1].id).toBe('3')
			})

		})

		it('should apply catalog restrictions when category and catalogRestrictionsByCategory are set', () => {
			component.questions = questions
			component.category = 'testCategory'
			component.catalogRestrictionsByCategory = {
				testCategory: { includeOnly: ['tag2'] }
			}

			component.filteredQuestions$.subscribe(filteredQuestions => {

				expect(filteredQuestions.length).toBe(2)
				expect(filteredQuestions[0].id).toBe('1')
				expect(filteredQuestions[1].id).toBe('2')
			})

		})

		it('should return all questions when no category filter is applied', () => {
			component.questions = questions

			component.filteredQuestions$.subscribe(filteredQuestions => {
				expect(filteredQuestions.length).toBe(3)
			})

		})
	})
})
