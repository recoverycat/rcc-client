import	{
			AfterViewInit,
			Component,
			forwardRef,
			Inject,
			Input,
			OnDestroy,
			Optional
		}							from	'@angular/core'
import	{
			ControlValueAccessor,
			FormControl,
			FormGroup,
			NG_VALUE_ACCESSOR

		}							from	'@angular/forms'
import	{
			ItemSelectionFilter,
			ITEM_SELECTION_FILTERS,
			RccTranslationService,
			SharedModule
		}							from	'@rcc/common'
import	{
			Question,
			uniqueId
		}							from	'@rcc/core'
import	{
			BehaviorSubject,
			combineLatest,
			map,
			Observable,
			Subject,
			takeUntil,
			tap
		}							from	'rxjs'

@Component({
    selector: 'rcc-question-list-control',
    templateUrl: './question-list-control.component.html',
    styleUrls: ['./question-list-control.component.scss'],
    imports: [SharedModule],
    providers: [
        { provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => QuestionListControlComponent),
            multi: true
        }
    ]
})
export class QuestionListControlComponent implements ControlValueAccessor, OnDestroy, AfterViewInit {
	@Input()
	public questions: Question[] = []

	@Input()
	public category: string

	@Input()
	public catalogRestrictionsByCategory: Record<string, { includeOnly: string[] }> = {}

	@Input()
	public set disabledIds(value: string[]) {
		this.disabledQuestions = new Set(value)
	}

	public constructor(
		private readonly translationService: RccTranslationService,
		@Optional() @Inject(ITEM_SELECTION_FILTERS)
		private	itemSelectionFilters		: ItemSelectionFilter[],
	) {
		translationService.activeLanguageChange$.pipe(
			takeUntil(this.destroy$)
		).subscribe(() => {
			this.optionsTextCache.clear()
			this.questionTextCache.clear()
		})
	}

	public ngAfterViewInit(): void {
		this.categoryControl.valueChanges.subscribe((value) => {
			this.categoryFilter$.next(value)
		})
	}

	public ngOnDestroy(): void {
		this.destroy$.next()
		this.destroy$.complete()
	}

	private destroy$: Subject<void> = new Subject()

	public writeValue(value: string[]): void {
		this.selectedQuestions = new Set(value)
	}

	public selectedQuestions: Set<string> = new Set()
	public disabledQuestions: Set<string> = new Set()

	public registerOnChange(fn: (value: string[]) => void): void {
		this.onChange = fn
	}

	public registerOnTouched(fn: () => void): void {
		this.onTouched = fn
	}

	public onChange: (value: string[]) => void
	public onTouched: () => void

	protected clickQuestion(id: string): void {
		if (this.disabledQuestions.has(id))
			return

		if (this.selectedQuestions.has(id))
			this.selectedQuestions.delete(id)
		else
			this.selectedQuestions.add(id)

		this.onChange([...this.selectedQuestions])
		this.onTouched()
	}

	// This translation function is very slow, causing a noticeable lag when
	// checking or unchecking a question, as it needs to be recalculated for
	// each question, so we write the translation results to a cache to
	// quickly look them up on subsequent change detections
	private optionsTextCache: Map<string, string> = new Map()

	protected optionsText(question: Question): string {
		const current: string = this.optionsTextCache.get(question.id)

		if (current)
			return current

		const calculated: string = question.options.map((o) => this.translationService.translate(o)).join(', ')

		this.optionsTextCache.set(question.id, calculated)
		return calculated
	}

	private questionTextCache: Map<string, string> = new Map()

	protected questionText(question: Question): string {
		const current: string = this.questionTextCache.get(question.id)

		if (current)
			return current

		const calculated: string = this.translationService.translate(question)
		this.questionTextCache.set(question.id, calculated)
		return calculated
	}

	private uniqueIdPart: string = uniqueId('question-list')

	protected id(id: string): string {
		return `${id}-${this.uniqueIdPart}`
	}

	private searchFilter$: BehaviorSubject<string> = new BehaviorSubject('')
	private categoryFilter$: BehaviorSubject<string> = new BehaviorSubject('')

	protected updateSearchFilter(event: Event): void {
		const target: HTMLInputElement = event.target as HTMLInputElement

		this.searchFilter$.next(target.value)
	}

	private filteredBySearch$: Observable<Question[]> = this.searchFilter$.pipe(
		map((searchTerm) => searchTerm === '' ? this.questions : this.questions.filter(question => {

			const translatedText: string = this.questionText(question).toLowerCase()
			const searchPattern: string = searchTerm.toLowerCase()

			const index: number = translatedText.indexOf(searchPattern)
			return index !== -1
		}))
	)

	public filterCategoriesWithCount$: Observable<Array<{ value: string, label: string }>> = combineLatest([this.filteredBySearch$, this.categoryFilter$]).pipe(
		map(([questions, currentFilter]) => {
			const tagCounts: Map<string, number> = this.determineCurrentTagCounts(questions)

			return this.itemSelectionFilters
				.map((filter) => this.getFilterWithCount(filter, tagCounts))
				.filter((category) => {
					//  If the category key is included, it returns true, keeping this category in the filtered list. If there are no catalog restrictions or the category doesn't meet the restrictions, it returns true by default, including all categories.

					if (this.catalogRestrictionsByCategory && this.catalogRestrictionsByCategory[this.category])
						return this.catalogRestrictionsByCategory[this.category].includeOnly.includes(category.key)
					else return true
				})
				.filter((value) => value.count > 0 || value.key === currentFilter)
				.map((value) => ({
					value: value.key,
					label: `${this.translationService.translate(value.filter)} (${value.count})`
				}))
				.sort((first, second) => first.label.localeCompare(second.label))
		}),

		// Automatically selects the category if only one option is available.
		tap((value) => {
			if(value.length === 1)
				this.categoryControl.setValue(value[0].value, { emitEvent: true })
			}
		)
	)

	private determineCurrentTagCounts(questions: Question[]): Map<string, number> {
		return questions.reduce((prev, next) => {
			(next.tags ?? []).forEach((tag) => {
				this.incrementCount(prev, tag.toLowerCase())
			})
			return prev
		}, new Map<string, number>())
	}

	private incrementCount(countMap: Map<string, number>, key: string): void {
		countMap.set(key, (countMap.get(key) ?? 0) + 1)
	}

	private getFilterWithCount(filter: ItemSelectionFilter, tagCounts: Map<string, number>): { filter: string, key: string, count: number } {
		const key: string = filter.representation.label.split('.').pop().toLocaleLowerCase()
		return {
			filter: filter.representation.label,
			key,
			count: tagCounts.get(key) ?? 0
		}
	}

	protected categoryControl: FormControl<string> = new FormControl('')
	protected formGroup: FormGroup<{ category: FormControl<string> }> = new FormGroup({ category: this.categoryControl })

	protected updateCategoryFilter(event: Event): void {
		const target: HTMLSelectElement = event.target as HTMLSelectElement

		this.categoryFilter$.next(target.value)
	}

/**
 * The `filteredQuestions$` property in the `QuestionListControlComponent`
 * class is an Observable that combines the filtered questions based on
 * search term and category filter. It uses the `combineLatest` operator
 * to listen to changes in both the `filteredBySearch$` Observable
 * (filtered based on search term) and the`categoryFilter$`
 * BehaviorSubject (filtered based on category selection).
 */
	public filteredQuestions$: Observable<Question[]> = combineLatest([this.filteredBySearch$, this.categoryFilter$]).pipe(
		map(([questions, category]) => {
			let filteredQuestions : Question[]= category === ''
				? questions
				: questions.filter((question) => (question.tags ?? []).includes(category))

			// for type ass
			if (
				this.category &&
				this.catalogRestrictionsByCategory &&
				this.catalogRestrictionsByCategory[this.category]
			) {

				const includeOnly: string[] = this.catalogRestrictionsByCategory[this.category].includeOnly
				filteredQuestions = filteredQuestions.filter(question =>
					question?.tags?.some(tag => includeOnly.includes(tag))
				)

			}

			return filteredQuestions
		})
	)
}
