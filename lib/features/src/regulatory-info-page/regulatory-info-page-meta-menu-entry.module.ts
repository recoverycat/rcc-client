import	{	ModuleWithProviders,
			NgModule,
			Provider					}	from '@angular/core'
import	{	RegulatoryInfoPageModule,
			aboutPagePath
										}	from './regulatory-info-page.module'
import	{	Action,
			getEffectivePosition,
			provideMainMenuMetaEntry,
			WithPosition				}	from '@rcc/common/index'

@NgModule({
	imports: [
		RegulatoryInfoPageModule
	]
})
export class RegulatoryInfoPageMetaMenuEntryModule {

	/**
	 * This method can add entries to the meta main menu.
	 */

	public static addEntry(config?: WithPosition): ModuleWithProviders<RegulatoryInfoPageMetaMenuEntryModule> {

		const metaMenuEntry	: Action =	{
			position:		getEffectivePosition(config, .5),
			icon: 			undefined,
			label: 			'ABOUT.MENU_ENTRY',
			path:			aboutPagePath,
		}

		const providers : Provider[] = [provideMainMenuMetaEntry(metaMenuEntry)]

		return {
			ngModule:	RegulatoryInfoPageMetaMenuEntryModule,
			providers
		}
	}


}
