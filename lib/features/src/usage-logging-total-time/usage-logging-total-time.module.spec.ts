import	{ 	TestBed						}	from '@angular/core/testing'
import	{
			DynamicLoggingData,
		}									from '../usage-logging/usage-logging.commons'
import	{
			RccUsageLoggingTotalTimeModule
		}									from './usage-logging-total-time.module'
import	{
			Subject,
			firstValueFrom,
		}									from 'rxjs'
import	{
			RccUsageLoggingTimeTrackingService,
			ReportValue,
		}									from '../usage-logging-time-tracking/usage-logging-time-tracking.service'
import	{	UsageLoggingTestService		}	from '../usage-logging/usage-logging-module-test-helpers'

describe('RccUsageLoggingTotalTimeModule', () => {
	const submitReport: Subject<ReportValue[]> = new Subject<ReportValue[]>()
	let timeTrackingService: Partial<RccUsageLoggingTimeTrackingService> = {}

	beforeEach(() => {
		timeTrackingService = {
			report$: submitReport.asObservable()
		}

		TestBed.configureTestingModule({
			imports: [
				RccUsageLoggingTotalTimeModule,
			],
			providers: [
				UsageLoggingTestService,
				{ provide: RccUsageLoggingTimeTrackingService, useValue: timeTrackingService },
			],
		})
	})

	it('Should calculate the total of all logged days', async () => {
		const service: UsageLoggingTestService = TestBed.inject(UsageLoggingTestService)

		const promise: Promise<DynamicLoggingData[]> = firstValueFrom(service.jobs)

		submitReport.next([
			{ date: new Date('2020-01-01'), minutes: 11 },
			{ date: new Date('2020-01-01'), minutes: 25 },
			{ date: new Date('2020-01-01'), minutes: 42 },
		])

		const result: DynamicLoggingData[] = await promise

		expect(result[0].computableValue).toBe(11+25+42)
	})
})
