import	{
			NgModule,
		}								from '@angular/core'
import	{
			RccUsageLoggingJob,
			provideRccUsageLoggingJob,
		}								from '../usage-logging/usage-logging.commons'
import	{	Factory, provideTranslationMap 				}	from '@rcc/common'
import	{
			RccUsageLoggingTimeTrackingService
		}								from '../usage-logging-time-tracking/usage-logging-time-tracking.service'
import	{	map						}	from 'rxjs'

import en from './i18n/en.json'
import de from './i18n/de.json'


const loggingJob: Factory<RccUsageLoggingJob> = {
	deps	: [RccUsageLoggingTimeTrackingService],
	factory: (rccUsageLoggingTimeTrackingService: RccUsageLoggingTimeTrackingService) => ({
			id					: 'weekly-total-usage',
			tick$				: rccUsageLoggingTimeTrackingService.report$.pipe(
				map((values) => {
					const totalSum		: number = values.reduce((sum, next) => sum + next.minutes, 0)
					return { computableValue: totalSum }
				}),
			),
			description			: 'USAGE_LOGGING.TOTAL_TIME.DESCRIPTION',
			label				: 'USAGE_LOGGING.TOTAL_TIME.LABEL',
			fixedLoggingData	: {
				category		: 'engagement',
				key				: '7-day-usage-time-total',
			},
			requiresUserConsent	: false,
			userId				: 'none',
		})
}

@NgModule({
	providers: [
		provideRccUsageLoggingJob(loggingJob),
		provideTranslationMap('USAGE_LOGGING.TOTAL_TIME', { en, de })
	]
})
export class RccUsageLoggingTotalTimeModule {}
