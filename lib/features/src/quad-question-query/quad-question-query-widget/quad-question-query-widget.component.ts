import { Component } from '@angular/core'
import { NumberOptionConfig } from '@rcc/core'
import { QueryControl, QueryWidgetComponent } from '../../queries'

@Component({
    templateUrl: './quad-question-query-widget.component.html',
    styleUrls: ['./quad-question-query-widget.component.css'],
    standalone: false
})
export class QuadQuestionQueryWidgetComponent extends QueryWidgetComponent {
	public static widgetMatch(queryControl: QueryControl): number {
		const isQuadQuestion: boolean =
			queryControl.question.options?.length === 4 &&
			queryControl.question.options.reduce(
				(prev, next, i) => prev && next.value === i, true
			)

		if (isQuadQuestion)
			return 2

		return -1
	}

	protected tabIndex(answer: NumberOptionConfig, isFirst: boolean): 0 | -1 {
		const selectedValue: number | '' = this.queryControl.answerControl.value as '' | number
		if (selectedValue === '') return isFirst ? 0 : -1

		return answer.value === selectedValue ? 0 : -1
	}

	protected isChecked(answer: NumberOptionConfig): boolean {
		return this.queryControl.answerControl.value === answer.value
	}

	protected get answers(): NumberOptionConfig[] {
		return this.queryControl.question.options as NumberOptionConfig[]
	}

	protected selectAnswer(value: number): void {
		this.queryControl.answerControl.setValue(value)
	}

	private goTo(event: Event, nextIndex: number): void {
		const currentRadio: HTMLElement		= event.target as HTMLElement
		const container: HTMLElement		= currentRadio.closest('[role="radiogroup"]')

		const radios: NodeListOf<HTMLElement>	= container.querySelectorAll('[role="radio"]')
		const nextRadio: HTMLElement			= radios.item(nextIndex)

		currentRadio.tabIndex	= -1
		nextRadio.tabIndex		= 0
		nextRadio.focus()
		this.selectAnswer(parseInt(nextRadio.getAttribute('value'), 10))
	}

	protected goToNext(event: Event, currentIndex: number): void {
		const shouldRollover: boolean	= currentIndex === this.answers.length - 1
		const nextIndex: number			= shouldRollover ? 0 : currentIndex + 1

		this.goTo(event, nextIndex)
	}

	protected goToPrevious(event: Event, currentIndex: number): void {
		const shouldRollover: boolean	= currentIndex === 0
		const nextIndex: number			= shouldRollover ? this.answers.length - 1 : currentIndex - 1

		this.goTo(event, nextIndex)
	}
}
