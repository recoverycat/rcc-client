import { Inject, Injectable } from '@angular/core'
import { DynamicLoggingData, RCC_USAGE_LOGGING_JOB_TOKEN, RccUsageLoggingJob } from './usage-logging.commons'
import { Observable, combineLatest } from 'rxjs'

@Injectable()
export class UsageLoggingTestService {
	public constructor(
		@Inject(RCC_USAGE_LOGGING_JOB_TOKEN)
		private rccUsageLoggingJobs: RccUsageLoggingJob[],
	) {}

	public jobs: Observable<DynamicLoggingData[]> = combineLatest(
		this.rccUsageLoggingJobs.map(job => job.tick$)
	)
}
