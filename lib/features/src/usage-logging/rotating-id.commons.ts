import	{
			assert,
			assertType,
			isErrorFree,
			assertProperty
		}									from '@rcc/core'

// eslint-disable-next-line @typescript-eslint/typedef
export const rotationScopes = ['none', 'daily', 'weekly', 'monthly', 'quarterly'] as const
export type RotationScope 	=	typeof rotationScopes[number]

export function assertRotationScope(x: unknown) : asserts x is RotationScope {

	assertType(x, 'string')
	assert(rotationScopes.includes(x as RotationScope))
}

export function isRotationScope(x: unknown) : x is RotationScope {
	return isErrorFree( () => assertRotationScope(x) )
}



export interface RotationConfig {
	scope:			RotationScope,
	seed:			string,
	representation:	string
}


export function assertRotationConfig(x: unknown) : asserts x is RotationConfig {
	assertProperty(x, ['scope', 'seed', 'representation'])
	assertType(x.scope, 'string')
	assertType(x.seed, 'string')
	assertType(x.representation, 'string')
}

export function isRotationConfig(x: unknown) : x is RotationConfig {
	return isErrorFree( () => assertRotationConfig(x) )
}


