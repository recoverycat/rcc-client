/* eslint-disable @typescript-eslint/unbound-method */
import { TestBed } from '@angular/core/testing'
import { RccUsageLoggingSettingsService } from './usage-logging-settings.service'
import { RccSettingsService, RccStorage, provideSettingsEntry } from '@rcc/common'
import { RCC_USAGE_LOGGING_JOB_TOKEN, RccUsageLoggingJob, USAGE_LOGGING_ENABLED_SETTING_ID, jobToSettingsId } from './usage-logging.commons'
import { Subject } from 'rxjs'
import { RccUsageLoggingService } from './usage-logging.service'
import { IndexedDbService } from '../storage/indexed-db'

describe('RccUsageLoggingSettingsService', () => {
	const loggingJob: RccUsageLoggingJob = {
		id: 'fake-job-ID',
		description: '',
		tick$: new Subject(),
		fixedLoggingData: {
			category: '',
			key: '',
		},
		label: '',
		requiresUserConsent: false,
		userId: 'none',
	}
	const jobSettingId: string = jobToSettingsId(loggingJob)
	let usageLoggingService: jasmine.SpyObj<RccUsageLoggingService> | undefined = undefined

	beforeEach(() => {
		usageLoggingService = jasmine.createSpyObj<RccUsageLoggingService>(['enableLogging', 'disableLogging', 'enableJob', 'disableJob'])

		TestBed.configureTestingModule({
			providers: [
				provideSettingsEntry({
					id: USAGE_LOGGING_ENABLED_SETTING_ID,
					defaultValue: false,
					type: 'boolean',
					description: '',
					label: '',
					icon: '',
				}),
				provideSettingsEntry({
					id: jobSettingId,
					defaultValue: false,
					type: 'boolean',
					description: '',
					label: '',
					icon: '',
				}),
				RccUsageLoggingSettingsService,
				RccSettingsService,
				IndexedDbService,
				{ provide: RccStorage, useExisting: IndexedDbService, },
				{ provide: RccUsageLoggingService, useValue: usageLoggingService },
				{ provide: RCC_USAGE_LOGGING_JOB_TOKEN, useValue: loggingJob, multi: true },
			]
		})
		TestBed.inject(RccUsageLoggingSettingsService)
	})

	describe('When the user does not enable logging', () => {
		it('should not start logging', async () => {
			const settingsService: RccSettingsService = TestBed.inject(RccSettingsService)

			await settingsService.set(USAGE_LOGGING_ENABLED_SETTING_ID, false)

			expect(usageLoggingService?.disableLogging).toHaveBeenCalledWith()
		})
	})

	describe('When the user enables logging', () => {
		it('should start logging', async () => {
			const settingsService: RccSettingsService = TestBed.inject(RccSettingsService)

			await settingsService.set(USAGE_LOGGING_ENABLED_SETTING_ID, true)

			expect(usageLoggingService?.enableLogging).toHaveBeenCalledWith()
		})
	})

	describe('When the user disables a job', () => {
		it('should disable job logging', async () => {
			const settingsService: RccSettingsService = TestBed.inject(RccSettingsService)

			await settingsService.set(jobSettingId, false)

			expect(usageLoggingService?.disableJob).toHaveBeenCalledWith(loggingJob)
		})
	})

	describe('When the user enables a job', () => {
		it('should enable job logging', async () => {
			const settingsService: RccSettingsService = TestBed.inject(RccSettingsService)

			await settingsService.set(jobSettingId, true)

			expect(usageLoggingService?.enableJob).toHaveBeenCalledWith(loggingJob)
		})
	})
})
