import	{
			Inject,
			Injectable,
			OnDestroy
		} 								from '@angular/core'
import	{
			filter,
			map,
			mergeMap,
			Observable,
			Subject,
			takeUntil,
			tap,
			Subscription
		}								from 'rxjs'
import	{
			assert
		}								from '@rcc/core'
import	{
			RccBuildInfoService,
			RccTranslationService,
			RccPersistentQueueService,
			PersistentQueueSubject
		}								from '@rcc/common'
import	{
			DynamicLoggingData,
			LoggingData,
			FixedLoggingData,
			EnvironmentLoggingData,
			RCC_USAGE_LOGGING_JOB_TOKEN,
			RccUsageLoggingJob,
			JobAnd,
		}								from './usage-logging.commons'

import	{
			RccUsageLoggingQueue,
			QueuedLoggingData
		}								from './usage-logging-queue.class'

import	{
			RccRotatingIdService
		}								from './rotating-id.service'

/**
 * This service tracks usage logging events from various jobs,
 * registered earlier. The data from these events is amended with
 * additional data from the job definition (fixed logging data), from
 * the setup (e.g. version; environment logging data) and a rotating
 * id (depending on the scope defined in the logging job).
 *
 * Data of all the events is channeled into one Observable: {@link .event$}
 * and will be emitted if both logging in general and the
 * originating job are enabled. Data emission will be blocked if
 * the browser's do-not-track settings are enabled.
 *
 * This service will not post data to any server, instead another service
 * has to subscribe to the above Observable and do the posting.
 */
@Injectable()
export class RccUsageLoggingService implements OnDestroy{

	public	readonly destroy$				:	Subject<void>
											=	new Subject<void>()

	private	readonly jobSubscriptions		:	Map<string, Subscription>
											= 	new Map<string, Subscription>()

	private	readonly enabledJobs			:	Set<string>
											=	new Set<string>()

	private readonly initialLoggingEvent$	:	Subject<JobAnd<DynamicLoggingData>>
											=	new Subject<JobAnd<DynamicLoggingData>>()

	private	 allowUsageLogging				:	boolean
											=	false

	/**
	 * Emits all the logging data from all active jobs.
	 * Will skip events while logging is not enabled.
	 * Subscription to a logging job's events (.tick$) will
	 * start when the job becomes active for the first time
	 * (logging is enabled in general AND the logging jobs is enabled).
	 *
	 * Consider using UsageLoggingQueueService instead, when trying to post data.
	 */
	public event$							:	Observable<JobAnd<LoggingData>>
											=	undefined
	public constructor(
		private readonly buildInfoService			: RccBuildInfoService,
		private readonly rccRotatingIdService		: RccRotatingIdService,
		private readonly rccTranslationService		: RccTranslationService,
		private readonly rccPersistentQueueService	: RccPersistentQueueService,
		@Inject(RCC_USAGE_LOGGING_JOB_TOKEN)
		private readonly jobs						: RccUsageLoggingJob[]
	) {

		this.assertNoDuplicateIds()

		/*
		In an earlier iteration we used sharing and multicasting
		for the .event$ Observable (e.g. ReplaySubject or shareReplay).
		That however came with timing issues, as the logging permission,
		can be different at the time of initial emission and later replay.
		Another drawback of the share/replay approach is that some logging
		jobs can trigger on subscription (i.e. when shared observable/subject
		connects) before the permission for logging are set and thus get
		blocked and never triggered again.
		*/

		this.event$				=	this.initialLoggingEvent$
									.pipe(
										filter(		({ job, 		})	=> this.passIfLoggingAllowed(job) ),
										map(		({ job, data	})	=> this.addFixedLoggingData(job, data) ),
										map( 		({ job, data	})	=> this.addEnvironmentToLoggingData(job, data) ),
										mergeMap( 	({ job, data	})	=> this.addRotatingId(job, data) ),
										tap(		({ job, data	})	=> this.logToConsole({ job, data }) )
									)

	}

	private assertNoDuplicateIds() : void {

		const ids 			: string[]
							= this.jobs.map(job=>job.id)

		const duplicates	: string[]
							= ids.filter((id, index) => index !== ids.indexOf(id))

		assert(duplicates.length === 0, 'RccUsageLoggingService: duplicate job ids: ', duplicates)
	}

	private passIfLoggingAllowed(job: RccUsageLoggingJob) : boolean {

		if(!job) return false

		if(navigator.doNotTrack === '1'){
			this.logToConsole({ job, message: 'blocked due to "Do Not Track" browser setting.' })
			return false
		}

		if(!this.usageLoggingAllowed) {
			this.logToConsole({ job, message: 'blocked due to app setting.' })
			return false
		}

		if(!this.isUsageLoggingJobEnabled(job)) {
			this.logToConsole({ job, message: 'blocked due to this job being disabled in particular' })
			return false
		}

		return true

	}

	private addFixedLoggingData<D extends Partial<LoggingData>>(job: RccUsageLoggingJob, data: D) : JobAnd<D & FixedLoggingData> {

		const extendedData	:	D & FixedLoggingData
							=  	{
									...data,
									...job.fixedLoggingData
								}

		return { job, data: extendedData }
	}

	private async addRotatingId<D extends Partial<LoggingData>>(job: RccUsageLoggingJob, data: D) : Promise<JobAnd<D & { id: string }>> {

		const id 			:	string
							=	await this.rccRotatingIdService.compileId(job.userId)

		const extendedData	:	D & { id: string }
							=	{ ...data, id }

		return 	{ job, data: extendedData }
	}

	private addEnvironmentToLoggingData<D extends Partial<LoggingData>>(job: RccUsageLoggingJob, data: D) : JobAnd<D & EnvironmentLoggingData> {

		const version 					: 	string
										=	this.buildInfoService.buildInfos.tag

		const flavor 					:	string
										=	this.buildInfoService.buildInfos.flavor

		const environmentLoggingData 	: 	EnvironmentLoggingData
										= 	{
												version,
												flavor
											}

		const extendedData				:	D & EnvironmentLoggingData
										=	{ ...data, ...environmentLoggingData }

		return { job, data: extendedData }
	}

	private logToConsole({ job, data, message }: {job: RccUsageLoggingJob, data?: LoggingData, message?: string}) : void {

		const jobLabel			: string
								= this.rccTranslationService.translate(job.label)


		const jobDescription	: string
								= this.rccTranslationService.translate(job.description)

		console.groupCollapsed(`Usage logging – ${message||''} ${jobLabel} (${job.id})`)
					console.info(jobDescription)
		if(data)	console.info('Data to be logged:', data)
		console.groupEnd()

	}

	/**
	 * Manages the subscriptions to registered jobs.
	 * Will subscribe to a job's events (.tick$) iff logging
	 * is allowed in general and at the same time the specific
	 * job is enabled.
	 *
	 * Will only unsubscribe when `this.destroy$`
	 */
	private updateJobSubscriptions() : void {

		for(const job of this.jobs){
			const loggingAllowed	: 	boolean
									=	this.usageLoggingAllowed
										&&
										this.isUsageLoggingJobEnabled(job)

			const existingSub		: 	Subscription
									= 	this.jobSubscriptions.get(job.id)

			const firstTimeAllowed	:	boolean
									=	loggingAllowed && !existingSub


			if(!firstTimeAllowed) continue

			const newSub			: 	Subscription
									=	job.tick$
										.pipe( takeUntil(this.destroy$) )
										.subscribe( data => {
											this.initialLoggingEvent$.next({ job, data })
										})

			this.jobSubscriptions.set(job.id, newSub)

			this.logToConsole({ job, message: 'Connected.' })

		}
	}

	public getQueue(queueId: string) : RccUsageLoggingQueue {

		const queuedLoggingData$	:	PersistentQueueSubject<QueuedLoggingData>
									=	this.rccPersistentQueueService
										.getOrCreate(queueId) as PersistentQueueSubject<QueuedLoggingData>

		const usageLoggingQueue		:	RccUsageLoggingQueue
									=	new RccUsageLoggingQueue( this, queuedLoggingData$ )

		this.destroy$.subscribe( () => usageLoggingQueue.disconnect() )

		return usageLoggingQueue
	}

	public getJob(jobId: string) : RccUsageLoggingJob {
		return this.jobs.find(j => j.id === jobId)
	}

	/**
	 * Checks if a respective RccUsageLoggingJob was properly registered.
	 */
	public isUsageLoggingJobRegistered(jobOrId: RccUsageLoggingJob | string) : boolean {

		const jobId :	string
					= 	typeof jobOrId === 'string'
						?	jobOrId
						:	jobOrId.id

		return !!this.getJob(jobId)

	}

	/**
	 * Checks if app and browser settings allow logging generally.
	 * And if the specific job is enabled.
	 */
	public isUsageLoggingJobAllowed(jobOrId: RccUsageLoggingJob | string) : boolean {

		const job	:	RccUsageLoggingJob
					=	typeof jobOrId === 'string'
						?	this.getJob(jobOrId)
						:	jobOrId

		return this.passIfLoggingAllowed(job)
	}

	// I turned this into a getter, because if it is a method and
	// accidentally called without parenthesis (e.g.`.usageLoggingAllowed`)
	// it would return the method an thus be thruthy, regardless of the actual state!
	public get usageLoggingAllowed(): boolean {
		return this.allowUsageLogging
	}

	public isUsageLoggingJobEnabled(job: RccUsageLoggingJob): boolean {
		return this.enabledJobs.has(job.id)
	}

	public enableJob(job: RccUsageLoggingJob): void {
		this.enabledJobs.add(job.id)
		this.updateJobSubscriptions()
	}

	public disableJob(job: RccUsageLoggingJob): void {
		this.enabledJobs.delete(job.id)
		this.updateJobSubscriptions()
	}

	public enableLogging(): void {
		this.allowUsageLogging = true
		this.updateJobSubscriptions()
	}

	public disableLogging(): void {
		this.allowUsageLogging = false
		this.updateJobSubscriptions()
	}

	public ngOnDestroy() : void {
		this.destroy$.next()
		this.destroy$.complete()
	}

}
