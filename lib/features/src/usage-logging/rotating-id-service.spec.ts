import	{
			RccKeyValueStorageService,
			KeyValuePair
		}									from '@rcc/common/key-value-storage'

import	{
			RccRotatingIdService,
		}									from './rotating-id.service'

import	{
			rotationScopes,
			RotationScope,
			RotationConfig
		}									from './rotating-id.commons'

import	{
			addHours,
			addDays,
			addWeeks,
			addMonths,
			addQuarters,
			startOfDay,
			startOfWeek,
			startOfMonth,
			startOfQuarter,
		}									from 'date-fns'

describe('RccRotatingIdService', () => {

	let storage					:	Map<string, unknown>
								=	new Map<string, unknown>()

	let mockKeyValueStorage		:	jasmine.SpyObj<RccKeyValueStorageService>
								=	undefined


	let rccRotatingIdService	:	RccRotatingIdService
								=	undefined

	let document				:	Document
								=	undefined

	beforeEach( () => {

		mockKeyValueStorage		=	jasmine.createSpyObj<RccKeyValueStorageService>('RccKeyValueStorageService', ['set', 'getValue', 'delete'] )

		mockKeyValueStorage.getValue
		.and.callFake( <T = unknown>(key : string) 		=> Promise.resolve(storage.get(key)) as T )

		mockKeyValueStorage
		.set.and.callFake( <T = unknown>(key : string, value: T) => { storage.set(key,value); return Promise.resolve<void>(undefined) })

		mockKeyValueStorage
		.delete.and.callFake( (key : string) => { storage.delete(key); return Promise.resolve<void>(undefined) })

		document				=	new EventTarget() as Document

		rccRotatingIdService	=	new RccRotatingIdService(mockKeyValueStorage, document)
		storage 				= 	new Map<string, KeyValuePair<unknown>>()
	})

	describe('.compileId', () => {


		function testByScope(scope: RotationScope) : void {

			function startOfPeriod(date : Date, s: RotationScope) : Date {

				switch(s){

					case 'daily': 		return startOfDay(date)

					case 'weekly':		return startOfWeek(date)

					case 'monthly':		return startOfMonth(date)

					case 'quarterly': 	return startOfQuarter(date)

					default:			return date
				}
			}

			// When at the beginning of a scope's period,
			// skipping ahead three minor steps should still stay
			// within said period.
			function minorSkip(date:Date, s: RotationScope) : Date | undefined {

				switch(s){

					case 'daily': 		return addHours(date,6)

					case 'weekly':		return addDays(date,2)

					case 'monthly':		return addDays(date, 9)

					case 'quarterly': 	return addDays(date,25)

					default:			return undefined
				}
			}

			// When at the beginning of a scope's period,
			// skipping ahead one major step should still stay
			// within said period.
			function majorSkip(date:Date, s: RotationScope) : Date {

				switch(s){

					case 'daily': 		return addDays(date,1)

					case 'weekly':		return addWeeks(date,1)

					case 'monthly':		return addMonths(date, 1)

					case 'quarterly': 	return addQuarters(date,1)

					default:			return date // no skip, meaning the period id effectively of length 0
				}
			}


			describe(`rotation scope: "${scope}"`, () => {

				beforeEach(() => {
					jasmine.clock().install()
				})

				afterEach(() => {
					jasmine.clock().uninstall()
				})

				describe('(concerning output)', () => {


					it('should return a 16 character long hex string', async () => {

						const id 	: string
									= await rccRotatingIdService.compileId(scope)

						expect(id).toMatch(/^[0-9a-f]{16}$/)

					})


					if(scope !== 'none')
					it('should return the same value for the same date.', async() => {

						const now 	: Date
									= new Date()


						jasmine.clock().mockDate(now)

						const id1	: string
									= await rccRotatingIdService.compileId(scope)

						jasmine.clock().mockDate(now)

						const id2	: string
									= await rccRotatingIdService.compileId(scope)

						expect(id1).toBe(id2)

						expect(id1).toMatch(/^[0-9a-f]{16}$/)

					})

					it('should return different values after scope specific period has passed.', async() => {

						let date 		: Date
										= new Date()

						const ids		: Set<string>
										= new Set<string>()

						const count		: number
										= 100

						for (let i : number = 0; i <count; i++){

								date 	= majorSkip(date,scope)

								jasmine.clock().mockDate(date)

								const nextId	: string
												= await rccRotatingIdService.compileId(scope)

								ids.add(nextId)

						}

						expect(ids).toHaveSize(count)

						Array.from(ids).forEach( id =>

							expect(id).toMatch(/^[0-9a-f]{16}$/)

						)

					})

					if(scope !== 'none')
					it('should return the same value for different dates within the same scope specific period."', async() => {

						let date 		: Date
										= startOfPeriod(new Date(), scope)

						const ids		: Set<string>
										= new Set<string>()

						const count		: number
										= 3

						for (let i : number = 0; i < count; i++){

								date 	= minorSkip(date,scope)

								jasmine.clock().mockDate(date)

								const nextId	: string
												= await rccRotatingIdService.compileId(scope)

								ids.add(nextId)

						}

						expect(ids).toHaveSize(1)

						const onlyId 	: string
										= Array.from(ids)[0]

						expect(onlyId).toMatch(/^[0-9a-f]{16}$/)

					})
				})

				describe('(concerning storage)', () => {

					if(scope !== 'none')
					it('should store different seeds after scope specific period has passed.', async() => {

						let date 			: Date
											= new Date()

						const seeds			: Set<string>
											= new Set<string>()

						const count			: number
											= 100


						for (let i : number = 0; i <count; i++){

								date 	= majorSkip(date,scope)

								jasmine.clock().mockDate(date)

								await rccRotatingIdService.compileId(scope)

								expect(storage).toHaveSize(1)

								const rotationConfig	: RotationConfig
														= Array.from(storage.values())[0] as RotationConfig

								expect(rotationConfig).toBeDefined()
								expect(rotationConfig.seed).toBeDefined()

								const seed				: string
														= rotationConfig.seed

								seeds.add(seed)

						}

						expect(seeds).toHaveSize(100)
					})

					if(scope !== 'none')
					it('should remove seeds after scope specific period has passed.', async () => {

						await rccRotatingIdService.compileId(scope)

						expect(storage).toHaveSize(1)

						jasmine.clock().mockDate(majorSkip(new Date, scope))

						document.dispatchEvent(new Event('visibilitychange'))

						// we need some time to wait for the async chin to complete,
						// hopefully this is enough:
						await new Promise(resolve => window.requestAnimationFrame(resolve))

						expect(storage).toHaveSize(0)

					})

					if(scope === 'none')
					it('should store no seeds for scope "none"', async ()=> {

						await rccRotatingIdService.compileId(scope)

						expect(storage).toHaveSize(0)
					})

				})

			})
		}

		rotationScopes.forEach(rotationScope => testByScope(rotationScope))

	})

})
