import	{
			NgModule,
		}										from '@angular/core'

import {
			Factory,
			SettingsEntry,
			SettingsModule,
			TranslationsModule,
			provideSettingsEntry,
			provideTranslationMap
		}										from '@rcc/common'
import	{	RccUsageLoggingService			}	from './usage-logging.service'
import
		{
			RCC_USAGE_LOGGING_JOB_TOKEN,
			RccUsageLoggingJob,
			USAGE_LOGGING_ENABLED_SETTING_ID,
			jobToSettingsEntry,
			jobToSettingsId,

		}										from './usage-logging.commons'
import	{	RccUsageLoggingSettingsService	}	from './usage-logging-settings.service'

const allowUsageLoggingEntry : Factory<SettingsEntry> = {

	deps: 				[RccUsageLoggingService],
	factory: 			() => ({
		id:				USAGE_LOGGING_ENABLED_SETTING_ID,
		label: 			'USAGE_LOGGING.ALLOW_USAGE_LOGGING.LABEL',
		description:	'USAGE_LOGGING.ALLOW_USAGE_LOGGING.DESCRIPTION',
		icon:			'settings',
		type:			'boolean' as const,
		defaultValue:	true,
	})
}


@NgModule({
	imports : [
		SettingsModule,
		TranslationsModule
	],
	providers: [
		RccUsageLoggingSettingsService,
		provideTranslationMap('SETTINGS_ENTRY_GROUPS.USAGE_LOGGING', {
			en: {
				LABEL: 'Usage Analysis',
				DESCRIPTION: 'Decide which usage data is passed on to Recovery Cat for analysis.'
			},

			de: {
				LABEL: 'Nutzungsanalyse',
				DESCRIPTION: 'Entscheide, welche Nutzungsdaten an Recovery Cat zur Analyse weitergegeben werden.'
			}
		}),

		provideSettingsEntry({
			deps: [RCC_USAGE_LOGGING_JOB_TOKEN],
			factory: (rccUsageLoggingJobs: RccUsageLoggingJob[]) => ({
				id: 			'usage-logging',
				type: 			'group',
				label: 			'SETTINGS_ENTRY_GROUPS.USAGE_LOGGING.LABEL',
				description: 	'SETTINGS_ENTRY_GROUPS.USAGE_LOGGING.DESCRIPTION',
				subSettingIds: 	[
									USAGE_LOGGING_ENABLED_SETTING_ID,
									...rccUsageLoggingJobs.map((job: RccUsageLoggingJob) => jobToSettingsId(job)),
								],
				icon: 			'settings',
				position: 		-5,
			})
		}),

		provideSettingsEntry(allowUsageLoggingEntry),

		...(new Array(100).fill(1).map((_, i) =>
			provideSettingsEntry({
				deps: [RCC_USAGE_LOGGING_JOB_TOKEN],
				factory: (rccUsageLoggingJobs: RccUsageLoggingJob[]) => jobToSettingsEntry(rccUsageLoggingJobs[i]) || null
			}),
		)),
	]
})


export class RccUsageLoggingSettingsModule {

	public constructor(
		private rccUsageLoggingSettingsService	: RccUsageLoggingSettingsService,
	){}

}
