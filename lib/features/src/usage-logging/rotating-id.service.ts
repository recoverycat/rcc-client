import	{
			Injectable,
			OnDestroy,
			Inject
		}									from '@angular/core'

import	{
			DOCUMENT
		}									from '@angular/common'

import	{
			fromEvent,
			Subject,
			takeUntil,
			startWith,
		}									from 'rxjs'

import	{
			assertType,
			randomBase32String,
			format
		}									from '@rcc/core'

import	{
			RccKeyValueStorageService
		}									from '@rcc/common'

import	{
			assertRotationConfig,
			isRotationConfig,
			RotationConfig,
			RotationScope,
			rotationScopes
		}									from './rotating-id.commons'



/**
 * This service provides consistent ids for various time scopes
 * ('e.g. daily, weekly...). Each id is unique to the current client
 * and stays the same for a scope specific period of time. (e.g. weekly ids
 * stay the same for one week). After that period we generate a new id for the
 * respective scope, which again will persist for the same period of time.
 *
 * To keep things persistent over time we create a seed for each scope and
 * store it over time.
 * The seed is then used to create the ids by hashing and salting the hash
 * with a representation of the current scope (e.g. the year and the number
 * of the calendar week). This way we ensure that the id we output will
 * always change when the scope's time period restarts, even if an error
 * caused the seed not to change.
 *
 * To prevent the seeds from becoming hidden persistent ids, we also create new
 * seeds, whenever a new time period starts for a respective scope. That however is
 * not as straight forward: while outputting an id requires the app to run, the
 * passage of time does not. So we have to resort to cleaning up seeds, before ids
 * are generated and periodically while the app is running.
 *
 * All in all, the output is guaranteed to change over time, while the seeds will
 * be cleaned up, only when the app is actively used.
 *
 * Ids for scope "none" will always be random.
 */
@Injectable({
	providedIn: 'root'
})
export class RccRotatingIdService implements OnDestroy{

	private readonly storageKeyPrefix	: 	string
										=	'rotation-config'

	private readonly destroy$			:	Subject<void>
										=	new Subject<void>()

	public constructor(
		private readonly rccKeyValueStorageService 	: RccKeyValueStorageService,
		@Inject(DOCUMENT)
		private readonly document					: Document
	){

		fromEvent(this.document, 'visibilitychange')
		.pipe(
			startWith(0),
			takeUntil(this.destroy$)
		)
		.subscribe( () => void this.cleanUp() )
	}

	/**
	 * Removes everything that is no longer necessary for the
	 * creation of current ids. (e.g. seeds for past periods).
	 */
	public async cleanUp(): Promise<void>{
		await this.removeInvalidRotationConfigs()
	}

	/**
	 * Creates a new unique id, that will persist over the scope's
	 * specific period of time. If scope is "none" the id will
	 * always be a new random one.
	 *
	 * @returns	string	16 characters long hex string.
	 */
	public async compileId(scope:RotationScope): Promise<string> {

		await this.cleanUp()

		const rotationConfig	: RotationConfig
								= await this.getRotationConfig(scope)

		const seed				: string
								= rotationConfig.seed

		const representation	: string
								= rotationConfig.representation

		const hashHex			: string
								= await this.hashAndSalt(seed, representation)

		const compiledId		: string
								= hashHex.slice(0,16)

		return compiledId

	}

	/**
	 * Gets all the data needed to create an id for
	 * the current rotation of the scope. Creates new data
	 * if no data could be retrieved from the storage.
	 */
	private async getRotationConfig(scope: RotationScope ) : Promise<RotationConfig> {

		try{
			return await this.loadRotationConfig(scope)
		} catch(e) {
			// If something goes wrong we just
			// continue creating a new config below.
		}

		const newRotationConfig	: RotationConfig
								= this.createRotationConfig(scope)

		if(scope !== 'none') void this.storeRotationConfig(newRotationConfig)

		return newRotationConfig

	}

	/**
	 * Loads all the data from storage needed to create an id for
	 * the current rotation of the scope.
	 *
	 * @throws if no data is present or data is out of date.
	 */
	private async loadRotationConfig(scope: RotationScope) : Promise<RotationConfig> {

		const 	key				: string
								= this.getStorageKey(scope)

		let 	rotationConfig	: unknown
								= undefined

		try {
			rotationConfig		= await this.rccKeyValueStorageService.getValue(key)
		} catch(e) {
			// It's okay if we have no value.
		}

		assertRotationConfig(rotationConfig)

		return rotationConfig
	}

	/**
	 * Stores all the data needed to create an id for
	 * the current rotation of the scope.
	 */
	private async storeRotationConfig(rotationConfig: RotationConfig): Promise<void>{

		const key	: string
					= this.getStorageKey(rotationConfig.scope)
		try {
			await this.rccKeyValueStorageService.set(key, rotationConfig)
		} catch(e) {
			console.warn('RccRotatingIdService.loadRotationConfig() unable to write to storage.', e)
		}
	}

	/**
	 * Deletes all data associated with a scope.
	 */
	private async deleteRotationConfig(rotationConfig: RotationConfig): Promise<void> {

		const key	: string
					= this.getStorageKey(rotationConfig.scope)
		try {
			await this.rccKeyValueStorageService.delete(key)
		} catch(e) {
			console.warn('RccRotatingIdService.loadRotationConfig() unable to delete from storage.', e)
		}
	}

	private getStorageKey(scope:RotationScope) : string {
		return `${this.storageKeyPrefix}-${scope}`
	}

	/**
	 * Checks if something is a {@link RotationConfig} and if
	 * that config is valid and not out of date.
	 */
	private isValidRotationConfig(x: unknown) : x is RotationConfig {

		if(!isRotationConfig(x)) return false

		const currentRepresentation	: string
									= this.getScopeRepresentation(x.scope)

		const representationIsValid	: boolean
									= x.representation === currentRepresentation

		return representationIsValid

	}

	/**
	 * Creates all the data needed to create an id for
	 * the current rotation of the scope.
	 */
	private createRotationConfig(scope: RotationScope) : RotationConfig {

		const seed				: string
								= randomBase32String(16)

		const representation	: string
								= this.getScopeRepresentation(scope)


		return { scope, seed, representation }
	}

	/**
	 * Gets a representation of the current time period for
	 * the provided scope. A Representation is a string that changes
	 * whenever we enter the next period of time. (e.g. year plus
	 * week number for the weekly scope)
	 */
	private getScopeRepresentation(scope: RotationScope) : string {

		const now	: Date
					= new Date()

		if(scope === 'daily')		return format(now, 'yyyy-DDD') 	// Year plus day of year; 2023-056
		if(scope === 'weekly')		return format(now, 'yyyy-ww')	// Year plus week of year; 2023-35
		if(scope === 'monthly')		return format(now, 'yyyy-MM')	// Year plus month of year; 2023-03
		if(scope === 'quarterly')	return format(now, 'yyyy-Q')	// Year plus quarter of year; 2023-4

		return randomBase32String(8)

	}

	/**
	 * Appends a seed string with a salt string and returns
	 * SHA-256 hash value as hexadecimal string.
	 */
	private async hashAndSalt(seed: string, salt: string) : Promise<string> {

		assertType(seed, 'string')
		assertType(salt, 'string')

		const value				:	string
								=	seed + salt

		const valueUint8 		:	Uint8Array
								=	new TextEncoder().encode(value)

		const hashBuffer 		:	ArrayBuffer
								=	await crypto.subtle.digest('SHA-256', valueUint8)

		const hashArray 		:	number[]
								=	Array.from(new Uint8Array(hashBuffer))

		const hashHex 			:	string
								=	hashArray
									.map( (b) => b.toString(16).padStart(2, '0'))
									.join('')
		return hashHex
	}

	/**
	 * Removes all out of date data from the storage.
	 */
	public async removeInvalidRotationConfigs() : Promise<void> {

		for( const scope of rotationScopes){

			let rotationConfig	: RotationConfig
								= undefined

			try {
				rotationConfig = await this.loadRotationConfig(scope)
			} catch (e) {
				// If we cannot load RotationConfigs
				// then there is nothing to delete :)

				continue
			}

			if(this.isValidRotationConfig(rotationConfig)) continue

			try {
				await this.deleteRotationConfig(rotationConfig)
			} catch(e){
				console.warn('RccRotatingIdService.removeInvalidIds() unable to delete invalid RotationConfig:', rotationConfig)
			}
		}

	}

	public ngOnDestroy(): void {
		this.destroy$.next()
		this.destroy$.complete()
	}

}

