import	{ NgModule, inject, provideAppInitializer } 										from '@angular/core'

import	{
			provideTranslationMap
		} 										from '@rcc/common'
import	{
			RccUsageLoggingService
		}										from './usage-logging.service'

import	{
			RccUsageLoggingSettingsModule
		}										from './usage-logging-settings.module'

import en from './i18n/en.json'
import de from './i18n/de.json'

@NgModule({
	imports:[
		RccUsageLoggingSettingsModule
	],
	providers: [
		RccUsageLoggingService,
		provideTranslationMap('USAGE_LOGGING', { en, de }),
		provideAppInitializer(() => {
			inject(RccUsageLoggingService)
		}),
	],
	declarations: [],
})
export class RccUsageLoggingModule {

}
