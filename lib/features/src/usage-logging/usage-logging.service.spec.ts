import {
			RccBuildInfoService,
			RccTranslationService,
			RccPersistentQueueService,
			PersistentQueueSubject
		} 								from '@rcc/common'

import	{
			firstValueFrom,
			BehaviorSubject,
		}								from 'rxjs'

import	{
			RccUsageLoggingService
		}								from './usage-logging.service'

import	{
			RccRotatingIdService
		}								from './rotating-id.service'

import	{
			DynamicLoggingData,
			RccUsageLoggingJob
		} 								from './usage-logging.commons'

import	{
			RccUsageLoggingQueue,
			QueuedLoggingData
		}								from './usage-logging-queue.class'

import	{
			Subject
		} 								from 'rxjs'



describe('RccUsageLoggingService', () => {

	let		service					:	RccUsageLoggingService
									=	undefined

	let 	doNotTrackSpy 			:	jasmine.Spy
									=	undefined

	const 	jobOneTick$ 			:	Subject<DynamicLoggingData>
									=	new Subject<DynamicLoggingData>()

	const 	firstJob				:	RccUsageLoggingJob
									=	{
											id: '1',
											description: 'test logging Job - 1',
											label: 'test logging Job - 1',
											tick$: jobOneTick$,
											requiresUserConsent: false,
											fixedLoggingData: {
												category: 'test category',
												key: 'test key',
											},
											userId: 'none'
										}

	const	secondJob				:	RccUsageLoggingJob
									=	{
											id: '2',
											description: 'test logging Job - 2',
											label: 'test logging Job - 2',
											tick$: new Subject<DynamicLoggingData>(),
											requiresUserConsent: true,
											fixedLoggingData: {
												category: 'test category',
												key: 'test key',
											},
											userId: 'weekly'

										}

	const rccBuildInfoService		:	RccBuildInfoService
									=	{
											buildInfos: {
												version: '0.0',
												tag: 'test tag',
												flavor: 'unit test'
											}
										} as unknown as RccBuildInfoService

	const rccTranslationService		:	RccTranslationService
									=	{
											translate: () => 'unit test mock translation'
										} as unknown as RccTranslationService

	const rccRotatingIdService		:	RccRotatingIdService
									=	{
											compileId: () => 'unit-test-mock-id'
										} as unknown as RccRotatingIdService

	const rccPersistentQueueService	:	RccPersistentQueueService
									=	{
											getOrCreate: () : PersistentQueueSubject<QueuedLoggingData> =>
												new PersistentQueueSubject<QueuedLoggingData>(
													() => Promise.resolve(),
													() => Promise.resolve([] as QueuedLoggingData[])
												)

										} as unknown as RccPersistentQueueService


	beforeEach(() => {

		const jobs 		:	RccUsageLoggingJob[]
						=	[firstJob, secondJob]

		doNotTrackSpy 	=	spyOnProperty(navigator, 'doNotTrack', 'get')

		service 		=	new RccUsageLoggingService(
								rccBuildInfoService,
								rccRotatingIdService,
								rccTranslationService,
								rccPersistentQueueService,
								jobs
							)
	})

	it('should throw when initialized with duplicate job ids', () => {
		expect( () => 	new RccUsageLoggingService(
							rccBuildInfoService,
							rccRotatingIdService,
							rccTranslationService,
							rccPersistentQueueService,
							[firstJob,firstJob]
						)
		).toThrow()
	})

	it('should initialize with usage logging disabled by default', () => {
		expect(service.usageLoggingAllowed).toBe(false)
	})

	it('should initialize with all logging jobs disabled regardless whether they need consent or not', () => {
		expect(firstJob.requiresUserConsent).toBe(false)
		expect(secondJob.requiresUserConsent).toBe(true)

		expect(service.isUsageLoggingJobEnabled(firstJob)).toBe(false)
		expect(service.isUsageLoggingJobEnabled(secondJob)).toBe(false)
	})

	describe('.enableLogging()', () => {

		it('should switch allowUsageLogging to true', () => {

			expect(service.usageLoggingAllowed).toBe(false)

			service.enableLogging()

			expect(service.usageLoggingAllowed).toEqual(true)

		})
	})

	describe('.disableLogging()', () => {

		it('should switch allowUsageLogging to false', () => {

			expect(service.usageLoggingAllowed).toBe(false)

			service.enableLogging()

			expect(service.usageLoggingAllowed).toEqual(true)

			service.disableLogging()

			expect(service.usageLoggingAllowed).toEqual(false)

		})
	})

	describe('.enableJob()', () => {

		it('should enable the provided job', () => {

			expect(service.isUsageLoggingJobEnabled(firstJob)).toBe(false)

			service.enableJob(firstJob)

			expect(service.isUsageLoggingJobEnabled(firstJob)).toEqual(true)

		})
	})

	describe('.disableJob()', () => {

		it('should disable the provided job', () => {

			expect(service.isUsageLoggingJobEnabled(firstJob)).toEqual(false)

			service.enableJob(firstJob)

			expect(service.isUsageLoggingJobEnabled(firstJob)).toEqual(true)

			service.disableJob(firstJob)

			expect(service.isUsageLoggingJobEnabled(firstJob)).toEqual(false)

		})

	})

	describe('.getJob()', () => {

		it('should return the job, it was properly registered.', () => {
			const job 	: RccUsageLoggingJob
						= service.getJob(firstJob.id)

			expect(job).toBe(firstJob)
		})

		it('should return undefined, if no job with the respective id exists.', () => {
			const job 	: RccUsageLoggingJob
						= service.getJob('some-nonsense-id')

			expect(job).toBeUndefined()
		})
	})

	describe('.isUsageLoggingJobRegistered()', () => {
	
		it('should return true, if the respective job was registered', () => {
			expect(service.isUsageLoggingJobRegistered(firstJob)).toBeTrue()
			expect(service.isUsageLoggingJobRegistered(firstJob.id)).toBeTrue()
		})

		it('should return false, if the respective job was not registered', () => {

			const unregisteredJob 	:	RccUsageLoggingJob
									=	{
											id: 'some-unregistered-job',
											description: 'some-unregistered-job',
											label: 'some-unregistered-job',
											tick$: new Subject<DynamicLoggingData>(),
											requiresUserConsent: true,
											fixedLoggingData: {
												category: 'some-unregistered-job',
												key: 'some-unregistered-job',
											},
											userId: 'weekly'
										}

			expect(service.isUsageLoggingJobRegistered(unregisteredJob)).toBeFalse()
			expect(service.isUsageLoggingJobRegistered('some-thing-something')).toBeFalse()
		})

	})

	describe('.isUsageLoggingJobAllowed', () => {
		
		it('should return true, when logging is enabled generally and do-not-track is not set and the respective job is enabled.', () => {
			doNotTrackSpy.and.returnValue(null)
			service.enableLogging()
			service.enableJob(firstJob)

			expect(service.isUsageLoggingJobAllowed(firstJob)).toBeTrue()

		})

		it('should return false if do-not-track is enabled', () => {

			doNotTrackSpy.and.returnValue('1')
			service.enableLogging()
			service.enableJob(firstJob)

			expect(service.isUsageLoggingJobAllowed(firstJob)).toBeFalse()

		})

		it('should return false if logging is disabled generally', () => {

			doNotTrackSpy.and.returnValue(null)
			service.disableLogging()
			service.enableJob(firstJob)

			expect(service.isUsageLoggingJobAllowed(firstJob)).toBeFalse()

		})

		it('should return false if the respective job is disabled', () => {

			doNotTrackSpy.and.returnValue(null)
			service.enableLogging()
			service.disableJob(firstJob)

			expect(service.isUsageLoggingJobAllowed(firstJob)).toBeFalse()

		})


	})
	

	describe('event$', () => {

		it('emits only if logging is enabled', async () => {
			let count: number = 0

			const 	step$	: 	Subject<void>
							=	new Subject<void>()

			service.event$.subscribe(() => {
				count++
				step$.next()
			})

			jobOneTick$.next({})

			expect(count).toBe(0)


			service.enableJob(firstJob)
			jobOneTick$.next({})

			expect(count).toBe(0)


			service.enableLogging()

			jobOneTick$.next({})

			await firstValueFrom(step$)

			expect(count).toBe(1)

			service.disableLogging()
			jobOneTick$.next({})

			expect(count).toBe(1)

			service.enableLogging()

			jobOneTick$.next({})

			await firstValueFrom(step$)

			expect(count).toBe(2)
		})

		it('emits only for enabled jobs', async () => {

			let 	count	:	number
							=	0

			const 	step$	: 	Subject<void>
							=	new Subject<void>()

			service.event$.subscribe(() => {
				count++
				step$.next()
			})

			jobOneTick$.next({})

			expect(count).toBe(0)

			service.enableJob(firstJob)

			jobOneTick$.next({})

			expect(count).toBe(0)

			service.enableLogging()

			jobOneTick$.next({})

			await firstValueFrom(step$)

			expect(count).toBe(1)


			service.disableJob(firstJob)

			jobOneTick$.next({})

			expect(count).toBe(1)


			service.enableJob(firstJob)

			jobOneTick$.next({})

			await firstValueFrom(step$)

			expect(count).toBe(2)


			service.disableLogging()

			jobOneTick$.next({})

			expect(count).toBe(2)

			service.enableLogging()

			jobOneTick$.next({})

			await firstValueFrom(step$)

			expect(count).toBe(3)

		})


		it('respects do-not-track setting', async () => {


			const step$			: 	Subject<void>
								=	new Subject<void>()


			service.event$.subscribe(() => {
				count++
				step$.next()
			})

			doNotTrackSpy.and.returnValue('1')

			let count: number = 0

			service.enableLogging()
			service.enableJob(firstJob)



			jobOneTick$.next({})

			expect(count).toBe(0)

			doNotTrackSpy.and.returnValue('0')

			jobOneTick$.next({})

			await firstValueFrom(step$)

			expect(count).toBe(1)


			doNotTrackSpy.and.returnValue('1')

			jobOneTick$.next({})

			expect(count).toBe(1)

			doNotTrackSpy.and.returnValue(null)

			jobOneTick$.next({})

			await firstValueFrom(step$)

			expect(count).toBe(2)

		})

		it('should pass through job ticks that emit _on subscription_, only when the respective job is activated for the first time this run (job enabled AND general logging allowed).', async() => {

			let count1 		:	number
							=	0

			let count2 		:	number
							=	0

			const step$		:	Subject<void>
							=	new Subject<void>()

			const eagerJob1	:	RccUsageLoggingJob
							=	{
									id: 'eager-1',
									description: 'Test logging Job emitting on subscription',
									label: 'Eager Logging Job -1',
									tick$: new BehaviorSubject<DynamicLoggingData>({ extraValue: 'eager-extra-value-1' }),
									requiresUserConsent: false,
									fixedLoggingData: {
										category: 'test category',
										key: 'test key',
									},
									userId: 'daily'
								}

			const eagerJob2	:	RccUsageLoggingJob
							=	{
									id: 'eager-2',
									description: 'Test logging Job emitting on subscription',
									label: 'Eager Logging Job - 2',
									tick$: new BehaviorSubject<DynamicLoggingData>({ extraValue: 'eager-extra-value-2' }),
									requiresUserConsent: false,
									fixedLoggingData: {
										category: 'test category',
										key: 'test key',
									},
									userId: 'daily'
								}

			service = new RccUsageLoggingService(
				rccBuildInfoService,
				rccRotatingIdService,
				rccTranslationService,
				rccPersistentQueueService,
				[firstJob, eagerJob1, eagerJob2]
			)

			// early subscription;
			service.event$.subscribe( jobAndData => {
				if(jobAndData.data.extraValue === 'eager-extra-value-1')	count1++
				if(jobAndData.data.extraValue === 'eager-extra-value-2')	count2++
				step$.next()
			})

			service.enableLogging()
			service.enableJob(eagerJob1)
			service.enableJob(firstJob)
			jobOneTick$.next({})

			await firstValueFrom(step$)

			expect(count1).toBe(1)
			expect(count2).toBe(0)

			service.disableLogging()
			service.enableLogging()
			jobOneTick$.next({})

			await firstValueFrom(step$)

			expect(count1).toBe(1)
			expect(count2).toBe(0)

			service.disableJob(eagerJob1)
			service.enableJob(eagerJob1)
			service.enableJob(eagerJob2)
			jobOneTick$.next({})

			await firstValueFrom(step$)

			expect(count1).toBe(1)
			expect(count2).toBe(1)
			
		})
	})


	describe('.getQueue()', () => {

		it('should return an RccUsageLoggingQueue, that queues logging data from registered jobs', async () => {

			const rccUsageLoggingQueue	: RccUsageLoggingQueue
										= service.getQueue('test-queue')

			const dataPromise			: Promise<QueuedLoggingData>
										= firstValueFrom(rccUsageLoggingQueue.readyData$)


			service.enableLogging()
			service.enableJob(firstJob)

			expect(rccUsageLoggingQueue).toBeInstanceOf(RccUsageLoggingQueue)

			jobOneTick$.next({ extraValue: 'some-extra-value' })

			await dataPromise

		})

	})
})
