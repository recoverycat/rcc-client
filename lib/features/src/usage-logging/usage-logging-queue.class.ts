import	{
			Observable,
			Subject,
			takeUntil,
			merge,
			fromEvent,
			filter,
			interval
		}									from 'rxjs'

import	{
			PersistentQueueSubject,
		}									from '@rcc/common'

import	{
			RccUsageLoggingService
		} 									from '../usage-logging'

import	{
			LoggingData,
			JobAnd,
		}									from '../usage-logging/usage-logging.commons'


export const ONE_MINUTE									: number
														= 1000*60

export const ONE_DAY									: number
														= 1000*60*60*24

export const USAGE_LOGGING_QUEUE_BLOCK_RETRY_DURATION	: number
														= ONE_MINUTE

export const USAGE_LOGGING_QUEUE_DATA_EXPIRE_AFTER_DAYS	: number
														= 8 // 8 days

export const USAGE_LOGGING_QUEUE_DATA_MAX_FAILURES		: number
														= 5

export const USAGE_LOGGING_QUEUE_TRIGGER_INTERVAL		: number
														= ONE_MINUTE*30

export const USAGE_LOGGING_QUEUE_MAX_LENTGH				: number
														= 100

export interface QueuedLoggingData{
	jobId:			string,
	/** Timestamp */
	originalDate:	number,
	/** Timestamp */
	lastAttempt?: 	number,
	failures:		number,
	loggingData:	LoggingData
}

export type PostLoggingDataCallback =  (loggingData: LoggingData, originalDate?: number) => Promise<void>


/**
 * RccUsageLoggingQueue represents a queue of usage
 * logging data.
 *
 * Services that post this kind of data to some remote
 * endpoint are supposed to use this queue to store logging
 * data after failed posting attempts. The queue will advance regularly
 * to retry stored data. It will check for permissions, online state,
 * age etc before handing data back for a retry. Consuming services
 * do not to have to manage the queue, they just have to put data on it,
 * and subscribe to .readyData$ for retries.
 *
 * The main use case is to store logging events while the user is offline
 * to post them later, when they're back online.
 *
 */
export class RccUsageLoggingQueue {

	private disconnect$			: 	Subject<void>
								= 	new Subject<void>()

	private	_shiftRequest$		: 	Subject<void>
								= 	new Subject<void>()

	private _recycledData$		: 	Subject<QueuedLoggingData>
								= 	new Subject<QueuedLoggingData>()

	/**
	 * Emits data that came off the queue and was readded to the end of
	 * the queue, because it wasn't their time yet to be
	 * retried.
	 */
	public	recycledData$		: 	Observable<QueuedLoggingData>
								= 	this._recycledData$.asObservable()

	private _droppedData$		: 	Subject<QueuedLoggingData>
								= 	new Subject<QueuedLoggingData>()

	/**
	 * Emits data that came off the queue, but was deemed no longer
	 * required (e.g. too old, lacking permission, failed to often etc.)
	 */
	public	droppedData$		: 	Observable<QueuedLoggingData>
								= 	this._droppedData$.asObservable()

	private _readyData$			: 	Subject<QueuedLoggingData>
								= 	new Subject<QueuedLoggingData>()

	/**
	 * Emits data that came off the queue and is ready to be posted.
	 */
	public	readyData$			: 	Observable<QueuedLoggingData>
								= 	this._readyData$.asObservable()



	public constructor(
		private readonly rccUsageLoggingService	: RccUsageLoggingService,
		private readonly queuedLoggingData$		: PersistentQueueSubject<QueuedLoggingData>,
	) {

		this.processIncomingUsageLoggingData()
		this.shiftQueueOnTriggers()
		this.processShiftedData()
	}

	/**
	 * Subscribes to the original usage logging data.
	 * Processes it right away as if it had come off the queue.
	 */
	private processIncomingUsageLoggingData() : void {

		this.rccUsageLoggingService.event$
			.pipe( takeUntil(this.disconnect$) )
			.subscribe( jobAndData	=> void this.processIncomingData(jobAndData) )

	}

	/**
	 * Subscribes to the queue (anything that comes off it).
	 */
	private processShiftedData() : void {

		this.queuedLoggingData$
			.pipe( takeUntil(this.disconnect$) )
			.subscribe( queuedLoggingData	=> void this.processQueuedLoggingData(queuedLoggingData) )
	}

	/**
	 * Subscribes to various triggers and shifts the queue.
	 */
	private shiftQueueOnTriggers() : void {

		const appOnline$			:	Observable<unknown>
									=	fromEvent(window, 'online')

		merge(
			/**
			 * When the app comes online, we should try to post:
			 */
			appOnline$,

			/**
			 * We should try periodically, in hopes that obstacles,
			 * we have no control over have been removed.
			 */
			interval(USAGE_LOGGING_QUEUE_TRIGGER_INTERVAL),

			/**
			 * When data was dropped, then there must have
			 * been a reason to try and since some piece of
			 * data was removed from the queue we have good
			 * reason to try the next one without risking an
			 * infinite loop:
			 */
			this._droppedData$,

			/**
			 * Manually triggered:
			 */
			this._shiftRequest$
		)
		.pipe(
			takeUntil(this.disconnect$),
			filter( () => navigator.onLine )
		)
		.subscribe( () => this.queuedLoggingData$.shift() )
	}

	/**
	 * Processes and piece of data that came of the queue and checks and
	 * check whether to drop it, recycle it (re-add it at the end of the queue)
	 * or hand it over as ready.
	 *
	 * Will trigger the respective Observables.
	 */
	private processQueuedLoggingData(queuedLoggingData: QueuedLoggingData) : void {

		const now 				: number
								= Date.now()

		const { jobId, originalDate, failures, lastAttempt = 0 } = queuedLoggingData


		// ## Check whether to drop the data:

		// In some cases queued logging data ought to be dropped,
		// never to be tried again:


		// The job has been removed from the app:
		const jobNotRegistered	: boolean
								= !this.rccUsageLoggingService.isUsageLoggingJobRegistered(jobId)

		const jobNotAllowed		: boolean
								= !this.rccUsageLoggingService.isUsageLoggingJobAllowed(jobId)

		// Failsafe for data getting stuck on the queue:
		const tooOld			: boolean
								= now -(originalDate||0) > USAGE_LOGGING_QUEUE_DATA_EXPIRE_AFTER_DAYS * ONE_DAY

		const tooManyFailures	: boolean
								= failures > USAGE_LOGGING_QUEUE_DATA_MAX_FAILURES


		const reasonToDrop		: boolean
								= jobNotRegistered || tooOld	|| tooManyFailures || jobNotAllowed

		if(reasonToDrop) return this._droppedData$.next(queuedLoggingData)

		// ## Check whether to recycle the data:

		// In some cases queued logging data should
		// not be tried to be posted and instead just be
		// readded to the end of the queue without any changes:


		const triedTooRecently	: boolean
								= now - lastAttempt < USAGE_LOGGING_QUEUE_BLOCK_RETRY_DURATION
												
		const reasonToRecycle	: boolean
								= triedTooRecently

		if(reasonToRecycle) return this.recycleQueuedLoggingData(queuedLoggingData)
				

		this._readyData$.next({ ...queuedLoggingData, lastAttempt:now })

	}

	/**
	 * Extend and amend incoming user data to QueuedLoggingData and processes it,
	 * as if it had come off the queue. So anything that comes in new
	 * will be processed immediately instead of being added to the end of the queue.
	 */
	private processIncomingData(jobAndData: JobAnd<LoggingData>) : void {

		const lastAttempt 	: number = undefined
		const failures		: number = 0
		const originalDate	: number = Date.now()

		const loggingData	: LoggingData
							= jobAndData?.data

		const jobId			: string
							= jobAndData?.job?.id

		this.processQueuedLoggingData({ originalDate, lastAttempt, failures, loggingData , jobId })

	}

	private recycleQueuedLoggingData(queuedloggingData: QueuedLoggingData) : void {
		this.queuedLoggingData$.push(queuedloggingData)
		this._recycledData$.next(queuedloggingData)
	}

	// Public part:


	public get size() : number {
		return this.queuedLoggingData$.size
	}

	/**
	 * Triggers the queue to process the next piece of data in line.
	 * This is meant to be used after e.g. previous data was posted
	 * successfully. This is only a request though, because the queue
	 * could be empty or the first piece of data on the queue was tried
	 * too recently and needs to be recycled. In such a case .readyData$
	 * will not emit another piece of data.
	 */
	public requestNext(): void {
		this._shiftRequest$.next()
	}

	/**
	 * Puts a piece of logging data onto the queue. Intended for data that
	 * failed to post earlier. (and came off the queue).
	 *
	 * If the queue is too large, ignores the request.
	 */
	public retryLater(queuedLoggingData: QueuedLoggingData, increaseFailureCount : boolean = true) : void {

		if(this.queuedLoggingData$.size >= USAGE_LOGGING_QUEUE_MAX_LENTGH) return

		let failures			:	number
								= 	queuedLoggingData.failures || 0

		if(increaseFailureCount) failures++

		this.queuedLoggingData$.push({ ...queuedLoggingData, failures })
	}

	/**
	 * Cleans up all the subscriptions.
	 */
	public disconnect(): void {
		this.disconnect$.next()
		this.disconnect$.complete()

		this._recycledData$.complete()
		this._droppedData$.complete()
		this._readyData$.complete()
	}
}
	
