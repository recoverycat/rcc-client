import	{
			Subject,
			Observable,
			firstValueFrom,
			BehaviorSubject,
			count,
			merge,
			takeUntil,
			toArray,
			take,
			skip
		}												from 'rxjs'

import	{
			PersistentQueueSubject,
		}												from '@rcc/common'

import	{
			RccUsageLoggingService
		}												from './usage-logging.service'

import	{
			JobAnd,
			LoggingData,
			RccUsageLoggingJob
		}												from './usage-logging.commons'

import	{
			QueuedLoggingData,
			RccUsageLoggingQueue,
			USAGE_LOGGING_QUEUE_DATA_MAX_FAILURES,
			USAGE_LOGGING_QUEUE_DATA_EXPIRE_AFTER_DAYS,
			USAGE_LOGGING_QUEUE_BLOCK_RETRY_DURATION,
			ONE_DAY,
		}												from './usage-logging-queue.class'

describe('RccUsageLoggingQueue', () => {


	let ulQueue						:	RccUsageLoggingQueue
									=	undefined

	const mockJob					:	RccUsageLoggingJob
									=	{
											id					: 'mock-job',
											tick$				: new Subject(),
											description			: 'mock-job-description',
											label				: 'mock-job-label',
											fixedLoggingData	: { category:'mock-category', key:'mock-key' },
											requiresUserConsent	: false,
											userId				: 'none',
										}

	let mockEvents					:	Subject<JobAnd<LoggingData>>
									=	undefined

	let onlineSpy					:	jasmine.Spy
									=	undefined

	let isUsageLoggingJobRegistered	:	jasmine.Spy<(id:string) => boolean>
									=	undefined

	let isUsageLoggingJobAllowed	:	jasmine.Spy<(id:string) => boolean>
									=	undefined

	let storage						:	BehaviorSubject<QueuedLoggingData[]>
									=	undefined

	let queuedLoggingData$			:	PersistentQueueSubject<QueuedLoggingData>
									=	undefined

	let rccUsageLoggingService		:	RccUsageLoggingService
									=	undefined

	beforeEach( () => {

		storage								=	new BehaviorSubject<QueuedLoggingData[]>([])
		onlineSpy							=	spyOnProperty(navigator, 'onLine', 'get')
		isUsageLoggingJobRegistered			=	jasmine.createSpy('isUsageLoggingJobRegistered')
		isUsageLoggingJobAllowed			=	jasmine.createSpy('isUsageLoggingJobAllowed')
		mockEvents							=	new Subject<JobAnd<LoggingData>>()

		rccUsageLoggingService				=	{
													event$: mockEvents,
													isUsageLoggingJobRegistered,
													isUsageLoggingJobAllowed
												} as unknown as RccUsageLoggingService
			

		queuedLoggingData$					=	new PersistentQueueSubject<QueuedLoggingData>(
													queue => {
														storage.next(queue)
														return Promise.resolve()
													},
													() => firstValueFrom(storage)
												)


		ulQueue 							= 	new RccUsageLoggingQueue(
													rccUsageLoggingService,
													queuedLoggingData$
												)
	})

	afterEach( ()=> {
		ulQueue.disconnect()
	})


	describe('New incoming logging data', () => {

		it('should be extended to a QueuedLoggingData and processed as if it came right off the queue', async () => {

			const loggingData			: 	LoggingData
										=	{
												category: 	'mock-category-ready-N',
												key:		'mock-key',
												version:	'mock-version',
												flavor:		'mock-flavor'
											}

			isUsageLoggingJobRegistered.and.returnValue(true)
			isUsageLoggingJobAllowed.and.returnValue(true)
			onlineSpy.and.returnValue(true)

			const readyDataPromise		: 	Promise<QueuedLoggingData>
										= 	firstValueFrom(ulQueue.readyData$)

			mockEvents.next({
				job: 	mockJob,
				data: 	loggingData
			})

			const readyData 			: QueuedLoggingData
										= await readyDataPromise

			expect(readyData.loggingData.category).toBe('mock-category-ready-N')

		})

	})

	describe('.droppedData$', () => {

		it('should emit after logging data was shifted, but respective job is not registered; logging data should no longer be stored in the queue.', async () => {

			isUsageLoggingJobRegistered.and.returnValue(false)
			isUsageLoggingJobAllowed.and.returnValue(true)
			onlineSpy.and.returnValue(true)

			const droppedDataPromise	: Promise<QueuedLoggingData>
										= firstValueFrom(ulQueue.droppedData$)

			queuedLoggingData$.push({
				jobId: 			'mock-job',
				originalDate: 	Date.now(),
				failures:		0,
				loggingData: 	{
									category: 	'mock-category-dropped-A',
									key:		'mock-key',
									version:	'mock-version',
									flavor:		'mock-flavor'
								}
			})

			queuedLoggingData$.shift()

			const droppedData 			: QueuedLoggingData
										= await droppedDataPromise

			expect(droppedData.loggingData.category).toBe('mock-category-dropped-A')

			expect(storage.value.length).toBe(0)
		})

		it('should emit after logging data was shifted, but respective job is not allowed; logging data should no longer be stored in the queue.', async () => {

			isUsageLoggingJobRegistered.and.returnValue(true)
			isUsageLoggingJobAllowed.and.returnValue(false)
			onlineSpy.and.returnValue(true)

			const droppedDataPromise	: Promise<QueuedLoggingData>
										= firstValueFrom(ulQueue.droppedData$)

			queuedLoggingData$.push({
				jobId: 			'mock-job',
				originalDate: 	Date.now(),
				failures:		0,
				loggingData:	{
									category: 	'mock-category-dropped-B',
									key:		'mock-key',
									version:	'mock-version',
									flavor:		'mock-flavor'
								}
			})

			queuedLoggingData$.shift()

			const droppedData 			: QueuedLoggingData
										= await droppedDataPromise

			expect(droppedData.loggingData.category).toBe('mock-category-dropped-B')

			expect(storage.value.length).toBe(0)
		})

		it('should emit after logging data was shifted, but was too old; logging data should no longer be stored in the queue.', async () => {

			isUsageLoggingJobRegistered.and.returnValue(true)
			isUsageLoggingJobAllowed.and.returnValue(true)
			onlineSpy.and.returnValue(true)

			const droppedDataPromise	: Promise<QueuedLoggingData>
										= firstValueFrom(ulQueue.droppedData$)

			queuedLoggingData$.push({
				jobId: 			'mock-job',
				originalDate: 	Date.now() - USAGE_LOGGING_QUEUE_DATA_EXPIRE_AFTER_DAYS * ONE_DAY -1,
				failures:		0,
				loggingData:	{
									category: 	'mock-category-dropped-C',
									key:		'mock-key',
									version:	'mock-version',
									flavor:		'mock-flavor'
								}
			})

			queuedLoggingData$.shift()

			const droppedData 			: QueuedLoggingData
										= await droppedDataPromise

			expect(droppedData.loggingData.category).toBe('mock-category-dropped-C')

			expect(storage.value.length).toBe(0)

		})


		it('should emit after logging data was shifted, but had failed too often before; logging data should no longer be stored in the queue.', async () => {

			isUsageLoggingJobRegistered.and.returnValue(true)
			isUsageLoggingJobAllowed.and.returnValue(true)
			onlineSpy.and.returnValue(true)

			const droppedDataPromise	: Promise<QueuedLoggingData>
										= firstValueFrom(ulQueue.droppedData$)

			queuedLoggingData$.push({
				jobId: 			'mock-job',
				originalDate: 	Date.now(),
				failures:		USAGE_LOGGING_QUEUE_DATA_MAX_FAILURES+1,
				loggingData:	{
									category: 	'mock-category-dropped-D',
									key:		'mock-key',
									version:	'mock-version',
									flavor:		'mock-flavor'
								}
			})

			queuedLoggingData$.shift()

			const droppedData 			: QueuedLoggingData
										= await droppedDataPromise

			expect(droppedData.loggingData.category).toBe('mock-category-dropped-D')

			expect(storage.value.length).toBe(0)

		})

		it('should shift the queue', async () => {

			isUsageLoggingJobRegistered.and.returnValue(true)
			isUsageLoggingJobAllowed.and.returnValue(true)
			onlineSpy.and.returnValue(true)

			const nextTwoDropped		: 	Observable<QueuedLoggingData[]>
										=	ulQueue.droppedData$
											.pipe( take(2), toArray() )

			const nextTwoDroppedPromise	: Promise<QueuedLoggingData[]>
										= firstValueFrom(nextTwoDropped)

			queuedLoggingData$.push({
				jobId: 			'mock-job',
				originalDate: 	Date.now(),
				failures:		USAGE_LOGGING_QUEUE_DATA_MAX_FAILURES+1,
				loggingData:	{
									category: 	'mock-category-dropped-D',
									key:		'mock-key',
									version:	'mock-version',
									flavor:		'mock-flavor'
								}
			})

			queuedLoggingData$.push({
				jobId: 			'mock-job',
				originalDate: 	Date.now(),
				failures:		USAGE_LOGGING_QUEUE_DATA_MAX_FAILURES+1,
				loggingData:	{
									category: 	'mock-category-dropped-E',
									key:		'mock-key',
									version:	'mock-version',
									flavor:		'mock-flavor'
								}
			})

			// only one manual shift:
			queuedLoggingData$.shift()

			await nextTwoDroppedPromise

			expect(storage.value.length).toBe(0)

		})




	})

	describe('.recycledData$', () => {

		it('should emit after logging data was shifted, but was tried already too recently; logging data should be moved to the end of the queue', async () => {

			isUsageLoggingJobRegistered.and.returnValue(true)
			isUsageLoggingJobAllowed.and.returnValue(true)
			onlineSpy.and.returnValue(true)

			jasmine.clock().install()

			const now 					: number
										= Date.now()

			const recycledDataPromise	: Promise<QueuedLoggingData>
										= firstValueFrom(ulQueue.recycledData$)

			queuedLoggingData$.push({
				jobId: 			'mock-job',
				originalDate: 	Date.now(),
				failures:		0,
				lastAttempt:	Date.now()-USAGE_LOGGING_QUEUE_BLOCK_RETRY_DURATION+1000,
				loggingData:	{
									category: 	'mock-category-recycled-A',
									key:		'mock-key',
									version:	'mock-version',
									flavor:		'mock-flavor'
								}
			})

			queuedLoggingData$.push({
				jobId: 			'mock-job',
				originalDate: 	Date.now(),
				lastAttempt:	undefined,
				failures:		0,
				loggingData:	{
									category: 	'mock-category-recycled-B',
									key:		'mock-key',
									version:	'mock-version',
									flavor:		'mock-flavor'
								}
			})

			queuedLoggingData$.shift()

			const recycledData 			: QueuedLoggingData
										= await recycledDataPromise

			expect(recycledData.loggingData.category).toBe('mock-category-recycled-A')

			expect(storage.value.length).toBe(2)

			expect(storage.value[1].loggingData.category).toBe('mock-category-recycled-A')

			jasmine.clock().mockDate(new Date(now+2000))

			const nowReadyDataPromise	: Promise<QueuedLoggingData>
										= firstValueFrom(ulQueue.readyData$.pipe(skip(1)))

			queuedLoggingData$.shift() // skip one piece of logging data
			queuedLoggingData$.shift() // this should give us the previously recycled one

			const nowReadyData			: QueuedLoggingData
										= await nowReadyDataPromise

			expect(nowReadyData.loggingData.category).toBe('mock-category-recycled-A')

			expect(storage.value.length).toBe(0)

			jasmine.clock().uninstall()
		})
	})

	describe('.readyData$', () => {

		it('should emit after logging data was shifted, and everything is okay (no other observable emitted); logging data should no longer be stored in the queue.', async () => {

			isUsageLoggingJobRegistered.and.returnValue(true)
			isUsageLoggingJobAllowed.and.returnValue(true)
			onlineSpy.and.returnValue(true)

			let undesiredCount			: 	number
										=	undefined
			merge(
				ulQueue.recycledData$,
				ulQueue.droppedData$,
			)
			.pipe(
				takeUntil(ulQueue.readyData$),
				count()
			)
			.subscribe( num => undesiredCount = num)

			const readyDataPromise		: 	Promise<QueuedLoggingData>
										= 	firstValueFrom(ulQueue.readyData$)



			queuedLoggingData$.push({
				jobId: 			'mock-job',
				originalDate: 	Date.now() - USAGE_LOGGING_QUEUE_DATA_EXPIRE_AFTER_DAYS * ONE_DAY / 2,
				failures:		0,
				loggingData:	{
									category: 	'mock-category-ready-C',
									key:		'mock-key',
									version:	'mock-version',
									flavor:		'mock-flavor'
								}
			})

			queuedLoggingData$.shift()

			const readyData 			: QueuedLoggingData
										= await readyDataPromise

			expect(undesiredCount).toBe(0)

			expect(readyData.loggingData.category).toBe('mock-category-ready-C')

			expect(storage.value.length).toBe(0)
		})

	})

	describe('.retryLater()', () => {

		it('should add QueuedLoggingData to the end of the queue with updated failure count.', async () => {

			// Put something on the queue, so that it's not empty
			queuedLoggingData$.push({
				jobId: 			'mock-job',
				originalDate: 	Date.now(),
				failures:		4,
				loggingData:	{
									category: 	'mock-category-X',
									key:		'mock-key',
									version:	'mock-version',
									flavor:		'mock-flavor'
								}
			})

			ulQueue.retryLater({
				jobId: 			'mock-job',
				originalDate: 	Date.now(),
				failures:		0,
				loggingData:	{
									category: 	'mock-category-A',
									key:		'mock-key',
									version:	'mock-version',
									flavor:		'mock-flavor'
								}
			}) // <- failure count is increased by default


			ulQueue.retryLater({
				jobId: 			'mock-job',
				originalDate: 	Date.now(),
				failures:		0,
				loggingData:	{
									category: 	'mock-category-B',
									key:		'mock-key',
									version:	'mock-version',
									flavor:		'mock-flavor'
								}
			}, true ) // <- explicitly increase failure count

			ulQueue.retryLater({
				jobId: 			'mock-job',
				originalDate: 	Date.now(),
				failures:		0,
				loggingData:	{
									category: 	'mock-category-C',
									key:		'mock-key',
									version:	'mock-version',
									flavor:		'mock-flavor'
								}
			}, false) // <- do not increase failure count


			const nextFourValues	:	Observable<QueuedLoggingData[]>
									=	queuedLoggingData$
										.pipe(
											take(4),
											toArray()
										)

			const dataPromise		: 	Promise<QueuedLoggingData[]>
									= 	firstValueFrom(nextFourValues)

			queuedLoggingData$.shift()
			queuedLoggingData$.shift()
			queuedLoggingData$.shift()
			queuedLoggingData$.shift()

			const data				:	QueuedLoggingData[]
									=	await dataPromise

			expect(data[0].loggingData.category).toBe('mock-category-X')

			expect(data[0].failures).toBe(4)

			expect(data[1].loggingData.category).toBe('mock-category-A')

			expect(data[1].failures).toBe(1)

			expect(data[2].loggingData.category).toBe('mock-category-B')

			expect(data[2].failures).toBe(1)

			expect(data[3].loggingData.category).toBe('mock-category-C')

			expect(data[3].failures).toBe(0)


		})

		it('should not comply with requests when queue is too large', async () => {

			for(let i: number =0; i<99; i++)
				queuedLoggingData$.push({
					jobId: 			'mock-job',
					originalDate: 	Date.now(),
					failures:		4,
					loggingData:	{
										category: 	'mock-category-X',
										key:		'mock-key',
										version:	'mock-version',
										flavor:		'mock-flavor'
									}
				})

			await queuedLoggingData$.ready

			expect(queuedLoggingData$.size).toBe(99)

			queuedLoggingData$.push({
				jobId: 			'mock-job',
				originalDate: 	Date.now(),
				failures:		4,
				loggingData:	{
									category: 	'mock-category-X',
									key:		'mock-key',
									version:	'mock-version',
									flavor:		'mock-flavor'
								}
			})

			await queuedLoggingData$.ready

			expect(queuedLoggingData$.size).toBe(100)


			ulQueue.retryLater({
				jobId: 			'mock-job',
				originalDate: 	Date.now(),
				failures:		0,
				loggingData:	{
									category: 	'mock-category-C',
									key:		'mock-key',
									version:	'mock-version',
									flavor:		'mock-flavor'
								}
			})

			await queuedLoggingData$.ready

			expect(queuedLoggingData$.size).toBe(100)

		})

	})

	describe('requestNext()', () => {

		it('should shift the next element off the queue, if there is a ready element at start of the queue', async () => {

			isUsageLoggingJobRegistered.and.returnValue(true)
			isUsageLoggingJobAllowed.and.returnValue(true)
			onlineSpy.and.returnValue(true)

			queuedLoggingData$.push({
				jobId: 			'mock-job',
				originalDate: 	Date.now(),
				failures:		0,
				loggingData:	{
									category: 	'mock-category-request-shift-ready',
									key:		'mock-key',
									version:	'mock-version',
									flavor:		'mock-flavor'
								}
			})


			const readyDataPromise		: 	Promise<QueuedLoggingData>
										= 	firstValueFrom(ulQueue.readyData$)

			ulQueue.requestNext()

			const readyData 			: QueuedLoggingData
										= await readyDataPromise

			expect(readyData.loggingData.category).toBe('mock-category-request-shift-ready')

		})

	})

	describe('.disconnect()', () => {

		it('should trigger completion of all observables and unsubscribe from injected observables', () => {

			let completeCount		: number
									= 0

			expect(mockEvents.observed).toBe(true)
			expect(queuedLoggingData$.observed).toBe(true)

			ulQueue.readyData$
			.subscribe( { complete: () => completeCount++ })

			ulQueue.recycledData$
			.subscribe( { complete: () => completeCount++ })

			ulQueue.droppedData$
			.subscribe( { complete: () => completeCount++ })

			ulQueue.disconnect()

			expect(completeCount).toBe(3)

			expect(mockEvents.observed).toBe(false)
			expect(queuedLoggingData$.observed).toBe(false)

		})


	})

})
