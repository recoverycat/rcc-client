import	{	Inject, Injectable, OnDestroy			}	from '@angular/core'

import	{
			RCC_USAGE_LOGGING_JOB_TOKEN,
			RccUsageLoggingJob,
			USAGE_LOGGING_ENABLED_SETTING_ID,
			jobToSettingsId
		}												from './usage-logging.commons'

import	{	RccUsageLoggingService					}	from './usage-logging.service'

import	{	RccSettingsService						}	from '@rcc/common'

import	{ 	Subject 								}	from 'rxjs'

@Injectable()
export class RccUsageLoggingSettingsService implements OnDestroy {

	private destroy$ : Subject<void> = new Subject<void>()

	public constructor(
		@Inject(RCC_USAGE_LOGGING_JOB_TOKEN)
		private rccUsageLoggingJobs: RccUsageLoggingJob[],
		private rccUsageLoggingService: RccUsageLoggingService,
		private rccSettingsService: RccSettingsService
	){
		void this.setup()
	}

	private setup(): void {

		this.rccSettingsService.valueChange$(USAGE_LOGGING_ENABLED_SETTING_ID)
			.subscribe((enabled: boolean) =>
				enabled
				?	this.rccUsageLoggingService.enableLogging()
				:	this.rccUsageLoggingService.disableLogging()
			)


		for (const job of this.rccUsageLoggingJobs)
			this.rccSettingsService.valueChange$(jobToSettingsId(job))
				.subscribe((enabled: unknown) =>
					enabled
						? this.rccUsageLoggingService.enableJob(job)
						: this.rccUsageLoggingService.disableJob(job)
				)

	}

	public ngOnDestroy(): void {
		this.destroy$.next()
		this.destroy$.complete()
	}
}
