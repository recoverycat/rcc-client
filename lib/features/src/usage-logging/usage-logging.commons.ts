import	{
			InjectionToken,
			Provider
		}									from '@angular/core'

import	{
			getProvider,
			ProductOrFactory,
			SettingsEntry
		}									from '@rcc/common'

import	{
			Observable
		} 									from 'rxjs'

import	{
			RotationScope
		}									from './rotating-id.commons'


export const RCC_USAGE_LOGGING_JOB_TOKEN:InjectionToken<RccUsageLoggingJob> = new InjectionToken<RccUsageLoggingJob>('RCC_USAGE_LOGGING_JOB')

export const USAGE_LOGGING_ENABLED_SETTING_ID: string = 'usage-logging-enabled'

declare module '@rcc/common/settings/settings.service' {
	interface SettingValueTypeMap {
		'usage-logging-enabled': boolean
	}
}

export function provideRccUsageLoggingJob(job: ProductOrFactory<RccUsageLoggingJob>): Provider {
	return getProvider(RCC_USAGE_LOGGING_JOB_TOKEN, job, true)
}

/** These values have to be defined on build time.
From loggingData and userId (and description) we
should be able to infer if user consent is required **/
export interface RccUsageLoggingJob {
	id					: string
	tick$				: Observable<DynamicLoggingData>
	description			: string
	label				: string
	fixedLoggingData	: FixedLoggingData
	requiresUserConsent	: boolean
	userId				: RotationScope
}

/** Meant to be defined when an RccUsageLoggingJob is defined,
 * these values get transmitted and are not supposed to change at runtime **/

export interface FixedLoggingData {
	/** category that describes the scope of the logging job, eg. the module that it is located in*/
	category			: string,
	/** describes the action that happened */
	key					: string,
}


/** Meant to be determined when an event occurs
that is to be logged. These values will be transmitted,
but are determined during runtime  **/
export interface DynamicLoggingData {
	/** can be used to log extra information
	 * that can be used to group log entries */
	extraValue?		: string,

	/** numeric value that can be used to calculate
	median values or sums for a specific action */
	computableValue?	: number,
}

/**
 * Data that is always added and is independent of a particular
 * usage logging Job, but informs about what app is running.
 */
export interface EnvironmentLoggingData {
	version	: string,
	flavor	: string
}

export type RawLoggingData 	= { id?: string } & FixedLoggingData & DynamicLoggingData
export type LoggingData		= RawLoggingData & EnvironmentLoggingData

export interface JobAnd<Data> {
	job: 	RccUsageLoggingJob,
	data:	Data
}

export function jobToSettingsId(job: RccUsageLoggingJob): string {
	return 'usage-logging-job-' + job.id
}

export function jobToSettingsEntry(job: RccUsageLoggingJob): SettingsEntry {
	if (!job) return null

	const label: string = job.label
	const description: string = job.description
	const icon: string = undefined
	const type: 'boolean' = 'boolean' as const
	const defaultValue: boolean = job.requiresUserConsent ? false : true
	const id: string = jobToSettingsId(job)

	return {
		id,
		label,
		description,
		icon,
		type,
		defaultValue,
	}
}

