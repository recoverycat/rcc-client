import	{	Question				} from '@rcc/core'

export interface CombinedData {
	questions		: Question[]
	data			: unknown[][]
	labels			: string[]
	getComplexLabel	: (index: number) => string[],
	getMediumLabel	: (index: number) => string[],
	dateStrings		: string[]
	min				: (number|null)[]
	max				: (number|null)[]
	stepSize		: (number|null)[]
	weekAvg?		: unknown[][]
	weekDayAvg?		: unknown[][]
}

