import	{	TestBed					}	from	'@angular/core/testing'
import	{	BackupService			}	from	'./backup.service'
import	{
			RccStorage,
			RccToastController,
									}	from	'@rcc/common'
import	{	ItemStorage				}	from	'@rcc/core'

describe('BackupService', () => {
	let service: BackupService = undefined!
	let rccStorage: RccStorage = undefined!
	const rccToastController: RccToastController = undefined!

	const storage_1_data 	:	Record<string, unknown>[]
							=	[
									{
										id:		'item_1_1',
										name:	'name_1_1'
									},
									{
										id:		'item_1_2',
										name:	'name_1_2'
									},
									{
										id:		'item_1_3',
										name:	'name_1_3'
									},

								]
	const storage_2_data 	:	Record<string, unknown>[]
							=	[
									{
										id:		'item_2_1',
										label: 	'label_2_1'
									},
									{
										id:		'item_2_2',
										label: 	'label_2_2'
									}

								]
	const storage_3_data 	:	Record<string, unknown>[] =	[]


	const mockData	:	Record<string, unknown[]>
					=	{
							storageName_1: storage_1_data,
							storageName_2: storage_2_data,
							storageName_3: storage_3_data
						}

	rccStorage = {
		getStorageNames: () => Object.keys(mockData),
		createItemStorage: (name: string) => ({
			getAll: () => Promise.resolve(mockData[name])
		}) as unknown as ItemStorage
	} as unknown as RccStorage

	beforeEach(() => {

		TestBed.configureTestingModule({
			providers: [
				BackupService,
				{ provide: RccStorage, useValue: rccStorage },
				{ provide: RccToastController, useValue: rccToastController },



			]
		})

		service = TestBed.inject(BackupService)

	})

	describe('exportData', () => {
		it('should export data from storage', async () => {
			const data: Record<string, unknown[]> = await service.exportData()

			expect(data).toEqual(mockData)
		})
	})

	describe('createZip', () => {
		it('should be reversible by readZip()', async () => {
			const data : Record<string, unknown> = {
				test: 'ABC'
			}

			const zipBlob : Blob = await service.createZip(JSON.stringify(data), 'test.json')

			expect(zipBlob).toBeInstanceOf(Blob)

			const unzippedData : string = await service.readZip(zipBlob, '')
			const parsedUnzippedData : unknown = JSON.parse(unzippedData)

			expect(parsedUnzippedData).toEqual(data)


		})
	})

	describe('exportAsZip', () => {
		it('should create a zip file from exported data', async () => {

			const zipBlob: Blob = await service.exportAsZip('backup.zip')
			const zipData : string = await service.readZip(zipBlob, '')
			const parsedUnzippedData : unknown = JSON.parse(zipData)

			expect(zipBlob).toBeInstanceOf(Blob)
			expect(parsedUnzippedData).toEqual(mockData)

		})

		it('should create a zip file that is only readable with the correct password', async () => {

			const password				: string	= 'CORRECT TEST PASSWORD'
			service.password						= password

			const zipBlob				: Blob		= await service.exportAsZip('test-backup.zip')
			const zipData				: string	= await service.readZip(zipBlob, password)
			const parsedUnzippedData 	: unknown	= JSON.parse(zipData)

			expect(zipBlob).toBeInstanceOf(Blob)

			expect(parsedUnzippedData).toEqual(mockData)

			await expectAsync(service.readZip(zipBlob, 'WRONG PASSWORD')).toBeRejected()
			await expectAsync(service.readZip(zipBlob, 'ADSLKASLDK')).toBeRejected()
			await expectAsync(service.readZip(zipBlob, ' ')).toBeRejected()
			await expectAsync(service.readZip(zipBlob, '')).toBeRejected()

		})
	})

})
