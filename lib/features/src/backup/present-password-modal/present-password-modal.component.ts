import	{
			Component,
			OnInit
		}									from '@angular/core'
import	{
			SharedModule
		}									from '@rcc/common'
import	{	generateRandomSixDigitCode	}	from '@rcc/core'
import	{	BackupService	}				from '../backup.service'

@Component({
    templateUrl: './present-password-modal.component.html',
    styleUrls: ['./present-password-modal.component.scss'],
    selector: 'rcc-present-password-modal',
    imports: [
        SharedModule,
    ]
})

export class RccBackupPresentPasswordComponent implements OnInit {

	protected password: string = undefined
	protected label: string = 'BACKUP.PASSWORD.LABEL'

	public constructor(
		private backupService: BackupService
	) {}

	public ngOnInit(): void {
		this.generateAndSetPassword()
	}

	private generateAndSetPassword(): void {
		this.password = generateRandomSixDigitCode().toString()
		this.backupService.setPassword(this.password)
	}

}
