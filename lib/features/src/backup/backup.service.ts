import	{	Injectable				} 	from '@angular/core'
import	{
			RccStorage,
			RccToastController,
		}								from '@rcc/common'

import	{	assert, zip				}	from '@rcc/core'
import {
			BlobReader,
			Entry,
			TextWriter,
			ZipReader,
									} from '@zip.js/zip.js'
@Injectable()
export class BackupService {

	public password 			: string

	public get showPassword()	: string {
		return this.password
	}

	public constructor(
		protected rccStorage: RccStorage,
		protected rccToastController: RccToastController
	){}

	// #region methods to handle exports
public async createZip(content: string, filename: string): Promise<Blob> {
		return await zip(content, filename, this.password)
	}

	public async exportData(): Promise<Record<string, unknown[]>> {

		const storageNames: string[]	=	await this.rccStorage.getStorageNames()

		const entries: [string, unknown[]][]	=	await Promise.all(
			storageNames.map(
				name => this.rccStorage.createItemStorage(name).getAll()
						.then( data => [name, data] as [string, unknown[]] )
			)
		)

		const result: Record<string, unknown[]>	=	Object.fromEntries(entries)

		return result

	}

	public async exportAsZip(filename: string): Promise<Blob> {
		const data: Record<string, unknown[]> = await this.exportData()
		const jsonString: string = JSON.stringify(data)

		// Create a zip file with the JSON string
		const zipBlob: Blob = await this.createZip(jsonString, filename)
		if (!zipBlob) throw new Error('Creating zip file failed.')

		return zipBlob
	}

	// #endregion exports

	// #region methods for importing data

		// Method to import data, handling JSON data.
	public async importData(data: Record<string, unknown[]>): Promise<void> {
		await Promise.all(
			Object.keys(data)
				.map(name => this.rccStorage.createItemStorage(name).store(data[name]))
		)
	}

	/**
	 * Extracts a zip archive into a string. Reads the content of the first file
	 * in a zip archive.
	 * @param zipBlob The blob containing the zip archive.
	 * @param password Password for the zip archive. Empty string means no encryption.
	 * @returns Array of strings representing the content of each file in the zip archive.
	 */
	public async readZip(zipBlob: Blob, password: string): Promise<string> {
		const zipBlobReader: BlobReader = new BlobReader(zipBlob)
		const zipReader: ZipReader<Blob> = new ZipReader(zipBlobReader, { password })
		try {

			const entries: Entry[] = await zipReader.getEntries()

			const contents: string[] = await Promise.all(entries.map(async (entry) => {
				const writer: TextWriter = new TextWriter()
				await entry.getData(writer)
				return await writer.getData()
			}))
			return contents[0]
		} catch(e:unknown) {
			console.info(e)
			throw e
		} finally {
			await zipReader.close()
		}
	}

	public async getDataFromArrayBuffer(zipData: ArrayBuffer, password: string): Promise<Record<string, unknown[]>> {
		try {
			const unzippedData: string = await this.readZip(new Blob([zipData]), password)
			const parsedData: unknown = JSON.parse(unzippedData)

			assert(typeof parsedData === 'object')
			assert(Object.keys(parsedData).every(key => Array.isArray((parsedData as Record<string, unknown>)[key])))
			return parsedData as Record<string, unknown[]>

		} catch (e: unknown) {
			await this.rccToastController.failure('BACKUP.PASSWORD.WRONG_PASSWORD')
			throw new Error('BackupService.getDataFromZip: Unable to read backup data from zip content.', { cause: e as Error })
		}
	}

	public getDataFromText(textData: string): Record<string, unknown[]> {
		try {
			const result: Record<string, unknown[]> = JSON.parse(textData) as Record<string, unknown[]>
			assert(typeof result === 'object')
			return result
		} catch (e: unknown) {
			throw new Error('BackupService.getDataFromText: Unable to parse text data into backup data.', { cause: e as Error })
		}
	}

	public setPassword(password: string): void {
		this.password = password
	}
	// #endregion imports

}
