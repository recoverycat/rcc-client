import 	{
			Component,
			ElementRef,
			ViewChild
		}											from '@angular/core'

import {
			HandlerAction,
			DownloadAction,
			RccAlertController,
			RccTranslationService,
			ShareDataService,
			RccModalController,
		}											from '@rcc/common'
import	{	CalendarDateString, UserCanceledError, assert			}	from '@rcc/core'
import	{
			BackupRestoreHelper,
		}											from '../backup-restore.helper'
import	{	BackupService						}	from '../backup.service'
import	{	RccBackupPresentPasswordComponent	}	from '../present-password-modal/present-password-modal.component'
import	{	RccBackupInputPasswordComponent		}	from '../input-password-modal/input-password-modal.component'

const BACKUP_FILE_PREFIX	: string = 'rcc-backup-'

@Component({
    templateUrl: './backup-page.component.html',
    styleUrls: ['./backup-page.component.scss'],
    standalone: false
})

export class BackupPageComponent{
	protected filenameBase: string = undefined

	@ViewChild('fileInput')
	public elementRef: ElementRef<HTMLInputElement>

	public downloadSuccess: boolean = false

	public get downloadAction(): DownloadAction {
		return {
		label:				this.getDownloadLabel(),
		description:				'BACKUP.PAGE.DOWNLOAD.DESCRIPTION',
		icon:				'backup_download',
		mimeType:			'application/zip',
		data:				() => this.downloadBackup(),
		filename:			() => this.filenameBase + '.zip',
		category:			this.downloadSuccess ? 'create' : 'misc'
		}
	}

	public restoreAction: HandlerAction = {
		label:       'BACKUP.PAGE.RESTORE.LABEL',
		description: 'BACKUP.PAGE.RESTORE.DESCRIPTION',
		icon:		 'backup_restore',
		handler:	 () => this.restore()
	}

	public sendAction: HandlerAction = {
		label:		 'BACKUP.PAGE.SEND.LABEL',
		description: 'BACKUP.PAGE.SEND.DESCRIPTION',
		icon:        'backup_transfer',
		handler:	 () => this.sendData()
	}

	// 1. Backup erzeugen
	// 2. Download triggern (Action oben)

	public constructor(
		private backupService			: BackupService,
		private rccAlertController		: RccAlertController,
		private rccTranslationService	: RccTranslationService,
		private shareDataService		: ShareDataService,
		private backupRestoreHelper		: BackupRestoreHelper,
		private rccModalController		: RccModalController,

	){
		backupRestoreHelper.showToastAfterReload()
	}

	public async downloadBackup(): Promise<Blob> {
		this.filenameBase = BACKUP_FILE_PREFIX + CalendarDateString.today() + '-' + String(new Date().getTime())

		return this.rccAlertController.confirm('BACKUP.PAGE.DOWNLOAD.CONFIRM').then(
			async () => {
				await this.presentPasswordModal()
				const zipData: Blob = await this.backupService.exportAsZip(this.filenameBase + '.json')

				this.downloadSuccess = true

				setTimeout(() => {
					this.downloadSuccess = false
				}, 3000)

				return zipData
			}
		)
	}

	public restore(): void {
		this.elementRef.nativeElement.click()
	}

	public onChange(): void {
		const file		: File		= this.elementRef.nativeElement.files[0]
		const date		: Date		= this.getDateFromFileName(file.name)

		const confirmationMessage : string = this.getConfirmationMessage(date)

		void this.rccAlertController.confirm(confirmationMessage)
			.then(() => this.restoreFromFile(file))
				.catch((error) => {
					if (error instanceof UserCanceledError) this.elementRef.nativeElement.value = ''
					else console.error(error)
				})
	}

	private getDateFromFileName(filename: string): Date {
		try {
			const dateAndTime	: string	= filename.split(BACKUP_FILE_PREFIX)[1]
			const split		: string[]	= dateAndTime.split('-')

			const year	: string = split[0]
			const month	: string = split[1]
			const day 	: string = split[2]

			return CalendarDateString.toDate(`${year}-${month}-${day}`)
		} catch (e: unknown) {
			return undefined
		}
	}

	private getConfirmationMessage(date: Date): string {
		if (date) {
			const dateString: string	= this.rccTranslationService.translate(
				date,
				{ year: 'numeric', month:'2-digit', day: '2-digit' }
			)
			return this.rccTranslationService.translate('BACKUP.PAGE.RESTORE.CONFIRM', { date: dateString })
		} else
			return this.rccTranslationService.translate('BACKUP.PAGE.RESTORE.CONFIRM_UNKNOWN_DATE')
	}

	private restoreFromFile(file: File): void {
		const reader: FileReader = new FileReader()
		const handleFileLoadEvent: () => Promise<void>
			= async (): Promise<void> => {
				const isText: boolean = typeof reader.result === 'string'
				const isArrayBuffer: boolean = reader.result instanceof ArrayBuffer

				assert(isText || isArrayBuffer, 'Unable to read text or array buffer from file.')

				let backupData: Record<string, unknown[]> = undefined
					if (isText) backupData = this.backupService.getDataFromText(reader.result as string)

					if (isArrayBuffer) {
						const password: string = await this.rccModalController.present(RccBackupInputPasswordComponent)
						if (password == null)
							return
						this.backupService.password = password
						try {
							backupData = await this.backupService.getDataFromArrayBuffer(reader.result as ArrayBuffer, this.backupService.password)
						} catch (error) {
							this.restoreFromFile(file)
						}
					}

				if (backupData) this.backupRestoreHelper.importAndToast(backupData)
			}
		reader.onload = () => void handleFileLoadEvent()

		// Determine if the file is JSON or zip based on the extension or type
		if (file.type === 'application/zip' || file.name.endsWith('.zip'))
			return reader.readAsArrayBuffer(file)
		return reader.readAsText(file)
	}

	public async sendData(): Promise<void> {
		const data: Record<string, unknown[]>|Blob = await this.backupService.exportData()
		await this.shareDataService.share(['rcc-backup', data], 'send', 'SHARE_DATA.SHOW_QR_CODE.BACKUP')
	}

	public async presentPasswordModal(): Promise<void> {
		await this.rccModalController.present(RccBackupPresentPasswordComponent)
	}

	private getDownloadLabel(): string {
		return this.downloadSuccess ? 'BACKUP.PAGE.DOWNLOAD.SUCCESS' : 'BACKUP.PAGE.DOWNLOAD.LABEL'
	}

}
