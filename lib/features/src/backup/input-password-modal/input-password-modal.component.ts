import	{
			Component,
			OnInit
		}							from '@angular/core'
import	{
			FormBuilder,
			FormControl,
			FormGroup,
			Validators
		}							from '@angular/forms'
import	{
			RccModalController,
			SharedModule
		}							from '@rcc/common'

@Component({
    templateUrl: './input-password-modal.component.html',
    styleUrls: ['./input-password-modal.component.scss'],
    selector: 'rcc-input-password-modal',
    imports: [
        SharedModule,
    ]
})
/** `RccBackupInputPasswordComponent` is an Angular component that represents
 * an input password modal. It contains a form for users to
 * input a password with validation rules (required and minimum length
 * of 6 characters). The component has a method to handle form
 * submission and dismiss the modal either with the entered password
 * or with a `UserCanceledError` if the form is not valid.
*/
export class RccBackupInputPasswordComponent implements OnInit {

	protected code: string = undefined

	public passwordForm: FormGroup<{ password: FormControl<string> }>

	public constructor(
		private rccModalController: RccModalController,
		private formBuilder: FormBuilder
	) {

	}

	public ngOnInit(): void {
		this.passwordForm = this.formBuilder.group({
			password: ['', [Validators.required, Validators.minLength(6)]]
		})
	}

	public onSubmit() : void {
		this.rccModalController.dismiss(this.passwordForm.value.password)
	}
}
