import	{
			IncomingDataService,
			RccAlertController,
			RccToastController
									}	from '@rcc/common'
import  {   Injectable              }   from '@angular/core'
import  {   filter                  }   from 'rxjs'
import  {   BackupService           }   from './backup.service'

export const BACKUP_RESTORED_KEY: string = 'backup_restored'

@Injectable()
export class  BackupRestoreHelper {

	public constructor(
		private backupService       : BackupService,
		private rccToastController  : RccToastController,
		incomingDataService         : IncomingDataService,
		rccAlertController	        : RccAlertController,
	) {
		incomingDataService
			.pipe(
				filter((data : unknown) => Array.isArray(data) && data[0] === 'rcc-backup')
			)
			.subscribe((data : unknown) => {
				if(!Array.isArray(data))
					return

				const backup: Record<string, unknown[]> = data[1] as Record<string, unknown[]>
				void rccAlertController.confirm('BACKUP.INCOMING.CONFIRM')
					.then( () => this.importAndToast(backup))
					.then( () => window.location.reload() )
			})
	}

	public showToastAfterReload(): void {
		// we need to reload so the settings after a successful restore get applied
		// but we want to show a success toast
		// this is a hack to show it after reload
		if(localStorage.getItem(BACKUP_RESTORED_KEY)) {
			// setTimeout is necessary if the language changed to get the correct translation
			setTimeout(() => void this.rccToastController.success('BACKUP.PAGE.RESTORE.SUCCESS'))
			localStorage.removeItem(BACKUP_RESTORED_KEY)
		}
	}

	public importAndToast(data: Record<string, unknown[]>): void {
		try {
			this.backupService.importData(data).then(() => {
			// we need to reload so the settings get applied
				// but we want to show a success toast
				// this is a hack to show it after reload
				localStorage.setItem(BACKUP_RESTORED_KEY, 'true')
				window.location.reload()
		}	).catch(() => {
				void this.rccToastController.failure('BACKUP.RESTORE.FAILURE')
			})
		} catch(e: unknown) {
			void this.rccToastController.failure('BACKUP.RESTORE.FAILURE')
		}
}
}
