import	{
			NgModule,
		}								from '@angular/core'
import	{
			RccUsageLoggingJob,
			provideRccUsageLoggingJob,
		}								from '../usage-logging/usage-logging.commons'
import	{	Factory, provideTranslationMap 				}	from '@rcc/common'
import	{
			RccUsageLoggingTimeTrackingService
		}								from '../usage-logging-time-tracking/usage-logging-time-tracking.service'
import	{	filter, map						}	from 'rxjs'

import en from './i18n/en.json'
import de from './i18n/de.json'

const loggingJob: Factory<RccUsageLoggingJob> = {
	deps	: [RccUsageLoggingTimeTrackingService],
	factory	: (rccUsageLoggingService: RccUsageLoggingTimeTrackingService) => ({
			id					: 'weekly-average-usage-per-day',
			tick$				: rccUsageLoggingService.report$.pipe(
				filter(value => value.length > 0),
				map((value) => {
					const sum		: number = value.reduce((prev, next) => prev + next.minutes, 0)
					const average	: number = sum / value.length
					return { computableValue: average }
				}),
			),
			description			: 'USAGE_LOGGING_AVERAGE.DESCRIPTION',
			label				: 'USAGE_LOGGING_AVERAGE.LABEL',
			fixedLoggingData	: {
				category		: 'engagement',
				key				: '7-day-usage-time-daily-average',
			},
			requiresUserConsent	: false,
			userId				: 'none',
		})
}

@NgModule({
	providers: [
		provideTranslationMap('USAGE_LOGGING_AVERAGE', { en, de }),
		provideRccUsageLoggingJob(loggingJob)
	]
})
export class RccUsageLoggingAveragePerDayModule {}

