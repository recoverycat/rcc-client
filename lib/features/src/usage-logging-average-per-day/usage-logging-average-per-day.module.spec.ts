import	{ 	TestBed						}	from '@angular/core/testing'
import	{
			DynamicLoggingData,
		}									from '../usage-logging/usage-logging.commons'
import	{
			RccUsageLoggingAveragePerDayModule
		}									from './usage-logging-average-per-day.module'
import	{
			Subject,
			firstValueFrom,
		}									from 'rxjs'
import	{
			RccUsageLoggingTimeTrackingService,
			ReportValue,
		}									from '../usage-logging-time-tracking/usage-logging-time-tracking.service'
import	{	UsageLoggingTestService		}	from '../usage-logging/usage-logging-module-test-helpers'

describe('RccUsageLoggingAveragePerDayModule', () => {

	const submitReport: Subject<ReportValue[]> = new Subject<ReportValue[]>()
	let timeTrackingService: Partial<RccUsageLoggingTimeTrackingService> = {}

	beforeEach(() => {
		timeTrackingService = {
			report$: submitReport.asObservable()
		}

		TestBed.configureTestingModule({
			imports: [
				RccUsageLoggingAveragePerDayModule,
			],
			providers: [
				UsageLoggingTestService,
				{ provide: RccUsageLoggingTimeTrackingService, useValue: timeTrackingService },
			],
		})
	})

	it('Should calculate the average of all logged days', async () => {
		const service: UsageLoggingTestService = TestBed.inject(UsageLoggingTestService)

		const promise: Promise<DynamicLoggingData[]> = firstValueFrom(service.jobs)

		submitReport.next([
			{ date: new Date('2020-01-01'), minutes: 11 },
			{ date: new Date('2020-01-01'), minutes: 25 },
			{ date: new Date('2020-01-01'), minutes: 42 },
		])

		const result: DynamicLoggingData[] = await promise

		expect(result[0].computableValue).toBe(26)
	})
})
