import 	{
			Pipe,
			PipeTransform,
		}     								from '@angular/core'


@Pipe({
	name: 'addEmptyLineAfterSentence',
	standalone: true
})
export class EmptyLineAfterSentencePipe implements PipeTransform {
  public transform(value: string): string {
    return value.replace(/\.\s/g, '.<br><br>')
  }
}
