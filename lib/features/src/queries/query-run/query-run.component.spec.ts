import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing'
import { ModalProviderModule, RccSliderModule } from '@rcc/common'
import { RouterModule } from '@angular/router'
import {
	DayQueryModule,
	DayQueryService,
	EventType,
	IndexedDbModule,
	QueriesModule,
	QueryControl,
	QueryRunComponent,
	StarterQuestionStoreServiceModule,
	ToastMessageMaterial
} from '@rcc/features'
import { IonicAlertController, IonicModalsModule } from '@rcc/ionic'
import { SymptomCheck, SymptomCheckConfig } from '@rcc/core'
import { RccThemeModule } from '@rcc/themes/default'

class QueryRunComponentWrapper extends QueryRunComponent {
	// Overwrite getHeadingText since it's protected in the original component
	public getHeadingText(queryControl: QueryControl): string {
		return super.getHeadingText(queryControl)
	}

	public getShowSuccessToastMaterial(queryControl: QueryControl, eventType: EventType): ToastMessageMaterial {
		return super.getShowSuccessToastMaterial(queryControl, eventType)
	}
}

describe('QueryRunComponent', () => {

	let component: QueryRunComponentWrapper = undefined!
	let fixture: ComponentFixture<QueryRunComponentWrapper> = undefined!

	const symptomCheckConfig: SymptomCheckConfig = {
		meta: {
			label: 'Starter Symptom Check',
			defaultSchedule: [
				[],
				['morning', 'evening'],
			],
			creationDate: '2024-08-01'

		},
		questions: [
			{
				id: 'rcc-curated-starter-questions-0001-daily-0',
				category: 'symptoms'
			},
			{
				id: 'rcc-curated-starter-questions-0002-daily-0',
				category: 'symptoms'
			},
			{
				id: 'rcc-curated-starter-questions-0003-daily-1',
				category: 'resources'
			},
			{
				id: 'rcc-curated-default-00-daily-note',
				category: 'daily_notes'
			}
		]
	}

	beforeEach(async () => {

		// Due to an error/warning saying that "Some of your tests did a full page reload!", the following fix was found
		// https://stackoverflow.com/questions/29352578/some-of-your-tests-did-a-full-page-reload-error-when-running-jasmine-tests
		window.onbeforeunload = jasmine.createSpy()

		await TestBed.configureTestingModule({
			imports: [
				RccThemeModule,
				RccSliderModule,
				StarterQuestionStoreServiceModule,
				IndexedDbModule,
				ModalProviderModule.forRoot({
					alertController: IonicAlertController,
				}),
				IonicModalsModule,
				QueriesModule,
				DayQueryModule,
				RouterModule.forRoot([])
			],
			declarations: [QueryRunComponentWrapper],
			teardown: { destroyAfterEach: false }

		}).compileComponents()

		fixture = TestBed.createComponent(QueryRunComponentWrapper)
		component = fixture.componentInstance
		component.queryControls = []
		fixture.detectChanges()

	})

	it('should create the component', () => {
		expect(component).toBeTruthy()
	})

	it('should process slidesOrQueries correctly for basic scenario with getHeadingText', fakeAsync(async () => {

		const dayQueryService: DayQueryService = TestBed.inject(DayQueryService)

		expect(component.queryControls).toEqual([])
		expect(component.slidesOrQueries).toEqual([])

		const queryControlRecords: Record<string, QueryControl[]> = await dayQueryService.getQueryControls([new SymptomCheck(symptomCheckConfig)])

		expect(queryControlRecords).toBeDefined()

		const dateKey: string = Object.keys(queryControlRecords)[0]
		const queryControlsValue: QueryControl[] = queryControlRecords[dateKey]

		component.queryControls = queryControlsValue

		expect(component.queryControls).toHaveSize(4)
		expect(component.slidesOrQueries).toHaveSize(4)

		const headingText: string = component.getHeadingText(queryControlsValue[0])

		expect(headingText).toEqual('1/3')

		const headingText1: string = component.getHeadingText(queryControlsValue[1])

		expect(headingText1).toEqual('2/3')

		const headingText2: string = component.getHeadingText(queryControlsValue[2])

		expect(headingText2).toEqual('3/3')

		// Daily notes
		const headingText3: string = component.getHeadingText(queryControlsValue[3])

		expect(headingText3).toEqual('')

	}))

	it('should process showSuccessToast as expected', fakeAsync(async () => {

		const dayQueryService: DayQueryService = TestBed.inject(DayQueryService)
		const queryControlRecords: Record<string, QueryControl[]> = await dayQueryService.getQueryControls([new SymptomCheck(symptomCheckConfig)])

		const dateKey: string = Object.keys(queryControlRecords)[0]
		const queryControlsValue: QueryControl[] = queryControlRecords[dateKey]

		component.queryControls = queryControlsValue
		queryControlsValue[0].answerControl.setValue(1)

		const showSuccessToastBaseInfo: ToastMessageMaterial = component.getShowSuccessToastMaterial(queryControlsValue[0], EventType.SAVE_EVENT)

		expect(showSuccessToastBaseInfo.showMessageKey).toEqual('QUERIES.SAVE.SUCCESS')

		const showSuccessToastBaseInfo3: ToastMessageMaterial = component.getShowSuccessToastMaterial(queryControlsValue[3], EventType.SAVE_EVENT)

		expect(showSuccessToastBaseInfo3.showMessageKey).toEqual('QUERIES.SAVE.NOTES.SUCCESS')

	}))

})
