import	{
			Component,
			Input,
		}							from '@angular/core'

import	{	QueryControl		}	from '../query-control.class'
import	{	CategoricalColor	}	from '@rcc/themes/index'
import	{
			DAILY_NOTES_ID,
			Question
		}							from '@rcc/core'
import 	{
			HandlerAction,
			RccAlertController,
			RccTranslationService
		} 							from '@rcc/common'


/**
 * This Component renders the UI for answering of a single question.
 * This includes the presentation of the question's wording and input widgets.
 *
 * Note: Why have an extra component that only adds the question's wording
 * on top of the input widgets? Why not use the rcc-widget by itself and have
 * those widgets reder the question's wording? – Answer: The presentation
 * of the question's wording should have a consistent look, regardless
 * type of question or possible answer. The input widget though will vary
 * with question/answer type. So we should keep these to concern separated.
 */
@Component({
    selector: 'rcc-query-view',
    templateUrl: './query-view.component.html',
    styleUrls: ['./query-view.component.css'],
    standalone: false
})
export class QueryViewComponent {

	public constructor(
		private readonly 	rccTranslationService	: RccTranslationService,
		protected 			rccAlertController		: RccAlertController
	) {}

	/**
	 * The {@link QueryControl} holds all the necessary information to run
	 * the question answer process.
	 */
	@Input()
	public queryControl: QueryControl

	@Input()
	public autosave: boolean = false

	public get hasAnswer() : boolean {
		return !!this.queryControl?.entry
	}

	protected category(queryControl: QueryControl): CategoricalColor {
		if (queryControl.question.id === DAILY_NOTES_ID)
			return 'share'

		return 'analyze'
	}

	protected getHeading(queryControl: QueryControl): string {
		const question: Question = queryControl.question
		const title: string = this.rccTranslationService.translate(question)
		const unitText: string = question.unit ? ` (${question.unit})`: ''
		return `${title}${unitText}`
	}

	public get entryExists(): boolean {
		return !!this.queryControl?.entry
	}

	public get changesExist(): boolean {

		if(!this.validUserInputExists) 	return false
		if(!this.entryExists) 			return true

		return this.queryControl.answer !== this.queryControl.entry.answer
	}

	public get validUserInputExists(): boolean {
		return this.queryControl?.complete
	}

	protected get showResetButton(): boolean {
		if (this.autosave)
			return false
		return this.canSubmitOrRevert
	}

	private get canSubmitOrRevert(): boolean {
		if (this.autosave)
			return this.canDelete

		return !this.queryControl?.submissionsDisabled && this.changesExist
	}

	protected get canDelete(): boolean {
		return !this.queryControl?.submissionsDisabled && this.entryExists
	}

	protected reset: HandlerAction = {
		handler 	: () => {
			this.revertEntry()
		},
		icon		: 'refresh',
		label		: 'QUERIES.QUERY_RUN.RESET',
		disabled	: () => !this.canSubmitOrRevert
	}

	public revertEntry(): void {
		this.queryControl.revert()
	}

	protected get deleteAnswer(): HandlerAction { return {
		handler		: () => this.removeEntry(),
		icon		: this.autosave ? 'refresh' : 'delete',
		label		: 'QUERIES.QUERY_RUN.DELETE',
		disabled	: () => !this.canDelete
	}}

	public async removeEntry(): Promise<void> {

		await	this.queryControl.reset()

	}
}
