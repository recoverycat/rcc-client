import { Component, ElementRef, EventEmitter, Inject, Input, Optional, Output, ViewChild, forwardRef } from '@angular/core'
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms'
import { CARD_VIEW, GREYED_OUT } from '@rcc/common'

export type FallbackValueType = string | number

@Component({
    selector: 'fallback-query-value-control-accessor',
    templateUrl: './fallback-query-value-control-accessor.html',
    styleUrls: ['./fallback-query-value-control-accessor.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => FallbackQueryValueControlAccessorComponent),
            multi: true,
        }
    ],
    standalone: false
})
export class FallbackQueryValueControlAccessorComponent implements ControlValueAccessor {
	public constructor(
		@Optional() @Inject(GREYED_OUT) 	injectedGreyedOut	: boolean,
		@Optional() @Inject(CARD_VIEW) 	injectedCardView	: boolean
	) {
		if(injectedGreyedOut)	this.greyedOut	= injectedGreyedOut
		if (injectedCardView)	this.cardView	= injectedCardView
	}

	/**
	 * we are allowing the type input to be deleted by 'backspace-removing' the
	 * value. To make this work I have implemented a workaround.
	 * We emit an event when a deletion
	 * is occurring. This is noticed by the parent component that holds the
	 * queryControl.
	 * In that parent component we then reset the whole queryControl. Check
	 * FallbackQueryWidgetComponent for the next step.
	*/

	@Output() public deleted: EventEmitter<void> = new EventEmitter<void>()

	private _input: ElementRef<HTMLInputElement>
	@ViewChild('input')
	protected set input(value: ElementRef<HTMLInputElement>) {
		this._input = value
		this.setInputValue(this.value)
	}
	@Input() public colorCategory: string

	@Input()
	public type: string

	@Input()
	public parseValue: (inputValue: string) => FallbackValueType = (value) => value

	@Input()
	public placeholder: string

	public cardView: boolean = false

	@Input()
	public min: number

	@Input()
	public max: number

	@Input()
	public step: string = 'any'

	@Input()
	public ariaLabel: string

	public greyedOut: boolean = false

	protected value: FallbackValueType
	public writeValue(value: FallbackValueType): void {
		this.value = value
		this.setInputValue(this.value)
	}

	private onChange: (value: FallbackValueType) => void = () => undefined
	public registerOnChange(fn: (value: FallbackValueType) => void): void {
		this.onChange = fn
	}

	private setInputValue(value: FallbackValueType): void {
		if (this._input)
			this._input.nativeElement.value = value?.toString() ?? ''

		this.value = value
	}

	protected handleInput(event: Event): void {

		const target: HTMLInputElement = event.target as HTMLInputElement
		const value: string = target.value
		const parsedValue: FallbackValueType = this.parseValue(value)

		if(value === '') this.deleted.emit()
		this.onChange(parsedValue)
		this.value = parsedValue
	}

	protected handleChange(event: Event): void {
		this.handleInput(event)
		this.setInputValue(this.value)
	}

	protected onTouch: () => void = () => undefined
	public registerOnTouched(fn: () => void): void {
		this.onTouch = fn
	}

	protected disabled: boolean = false
	public setDisabledState?(isDisabled: boolean): void {
		this.disabled = isDisabled
	}
}
