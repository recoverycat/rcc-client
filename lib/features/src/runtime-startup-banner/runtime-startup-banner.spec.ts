import	{
			RccPublicRuntimeConfigService,
			RccBannerService,
			RccTranslationService,
			WithLabelTranslator
		}										from '@rcc/common'

import	{
			RccRuntimeStartupBannerService
		}										from './runtime-startup-banner.service'




describe('RccRuntimeStartupBannerService', () => {

	let config  						: unknown
										= undefined

	let	rccRuntimeStartupBannerService	: RccRuntimeStartupBannerService
										= undefined

	let	rccTranslationService			: RccTranslationService
										= undefined

	let mockPublicRuntimeConfigService	: Record<string, jasmine.Spy>
										= undefined

	let	mockBannerService				: Record<string, jasmine.Spy>
										= undefined

	beforeEach( () => {

		mockPublicRuntimeConfigService	= 	jasmine.createSpyObj(['get']) as Record<string, jasmine.Spy>


		mockBannerService				= 	jasmine.createSpyObj(['next']) as Record<string, jasmine.Spy>

		mockPublicRuntimeConfigService.get.and.callFake( () => config instanceof Error ? Promise.reject(config) : Promise.resolve(config) )

		config 							= 	undefined
		rccTranslationService			=	new RccTranslationService([new WithLabelTranslator()], document) // Needs at least one Translator for setup
		rccRuntimeStartupBannerService 	= 	new RccRuntimeStartupBannerService(
												mockPublicRuntimeConfigService as unknown as RccPublicRuntimeConfigService,
												mockBannerService as unknown as RccBannerService,
												rccTranslationService
											)
	})

	describe('.setup()', () => {



		it('should resolve, but add no banner, if runtime config cannot be loaded', async () => {

			config = new Error('meh -.-')

			await expectAsync(rccRuntimeStartupBannerService.setup()).toBeResolved()

			expect(mockBannerService.next).not.toHaveBeenCalled()

		})

		it('should resolve, but add no banner, if runtime config is empty', async () => {

			config = undefined

			await expectAsync(rccRuntimeStartupBannerService.setup()).toBeResolved()

			expect(mockBannerService.next).not.toHaveBeenCalled()

		})



		it('should reject, if some startup banner config exists but is malformed', async () => {

			config			=	{}

			await expectAsync( rccRuntimeStartupBannerService.setup() ).toBeRejected()

			config			=	{ somethig: 'something' }

			await expectAsync( rccRuntimeStartupBannerService.setup() ).toBeRejected()

			config			=	true

			await expectAsync( rccRuntimeStartupBannerService.setup() ).toBeRejected()

		})

		it('should reject, if some available language has no banner configured', async () => {

			config			=	{
									color: 'primary',
									markdown: {
										translations: {
											'xx' : 'XX language __banner__'
										}
									}
								}


			// language independent, so everything except xx is missing:
			await expectAsync( rccRuntimeStartupBannerService.setup() ).toBeRejected()

			expect( () => rccTranslationService.restrictLanguagePool(['xx', 'yy']) ).not.toThrowError()

			// missing yy
			await expectAsync( rccRuntimeStartupBannerService.setup() ).toBeRejected()

			expect( () => rccTranslationService.restrictLanguagePool(['xx']) ).not.toThrowError()

			// should work (just to make sure the above config is not causing errors for other reasons)
			await expectAsync( rccRuntimeStartupBannerService.setup() ).toBeResolved()


		})

		it('should resolve, and add banner, if runtime proper config is supplied', async () => {

			config =	{
							color: 'primary',
							markdown: {
								translations: {
									'xx' : 'XX language __banner__',
									'yy' : 'YY language __banner__'
								}
							}
						}

			expect( () => rccTranslationService.restrictLanguagePool(['xx', 'yy']) ).not.toThrowError()

			await expectAsync(rccRuntimeStartupBannerService.setup()).toBeResolved()

			expect(mockBannerService.next).toHaveBeenCalledWith(config)

		})

	})


})
