import	{
			Injectable
		}										from '@angular/core'

import	{
			RccPublicRuntimeConfigService,
			assertBannerConfig,
			RccBannerService,
			RccTranslationService
		}										from '@rcc/common'

import	{
			assert
		}										from '@rcc/core'

import	{
			runtimeConfigPath,
		}										from './runtime-startup-banner.commons'


/**
 * Checks if a startup banner is configured
 * in the runtime config. If so registers banner
 * data to be displayed instantly.
 *
 * If no suitable configuration data can be found,
 * do nothing; this banner is optional.
 */
@Injectable()
export class RccRuntimeStartupBannerService {

	public constructor(
		private rccPublicRuntimeConfigService	: RccPublicRuntimeConfigService,
		private rccBannerService				: RccBannerService,
		private rccTranslationService			: RccTranslationService
	){
		void this.setup()
	}

	public async setup() : Promise<void> {

		const config 	: unknown
						= await	this.rccPublicRuntimeConfigService.get(runtimeConfigPath)
								.catch( () => undefined) // If the config is not available just skip

		if(!config) return // no banner configured

		assertBannerConfig(config)

		const translations				: Record<string,string>
										= config.markdown.translations

		const configuredLanguages		: string[]
										= Object.keys(translations)

		const availableLanguages		: string[]
										= this.rccTranslationService.availableLanguages

		const hasLanguageRestrictions	: boolean
										= availableLanguages !== null

		// If there are no language restrictions (i.e.. the app is language independent),
		// we cannot check if banners for all available languages have been configured;
		// resp. we know they have not.
		assert(hasLanguageRestrictions, `RccRuntimeStartupBannerService.setup() The startup banner configuration introduces language restrictions (${configuredLanguages.join(', ')}, despite the app itself not having any. Restrict the app to these languages before using this startup banner config.`)

		// Look for a language that is available to the app, but has no banner:
		const availableButNotConfigured	: string
										= availableLanguages.find( lang => !configuredLanguages.includes(lang) )

		assert(!availableButNotConfigured, `RccRuntimeStartupBannerService.setup() missing language entry for ${availableButNotConfigured}.`)

		this.rccBannerService.next(config)
	}

}
