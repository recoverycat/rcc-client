export type Choice = 'with-camera' | 'no-camera'

declare module '@rcc/common/settings/settings.service' {
	interface SettingValueTypeMap {
		'data-transmission-preference-qr-code': Choice
	}
}

export const dataTransmissionPreferenceSettingsEntryId: string = 'data-transmission-preference-qr-code'
