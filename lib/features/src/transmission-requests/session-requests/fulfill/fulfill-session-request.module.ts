import 	{	NgModule							}	from '@angular/core'

import	{
			provideTranslationMap
		}											from '@rcc/common'

import	{	RccTransmissionRequestModule		}	from '../../transmission-request.module'

import	{	FulFillSessionRequestService		}	from './fulfill-session-request.service'


import en from './i18n/en.json'
import de from './i18n/de.json'


/**
 * When this module is imported the app will respond to transmission requests
 * for session data.
 *
 * _Note:_ This opens the app up for potential fishing attacks.
 * Please only include if there is no other way!
 */
@NgModule({
	imports:[
		RccTransmissionRequestModule
	],
	providers:[
		FulFillSessionRequestService,
		provideTranslationMap('SESSION_REQUESTS.FULFILL', { en,de })
	]
})
export class FulFillSessionRequestModule {


	public constructor(
		// Makes sure this service is instantiated
		private fulFillSessionRequestService: FulFillSessionRequestService
	){}

}
