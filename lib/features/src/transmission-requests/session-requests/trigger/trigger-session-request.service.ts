import	{	Injectable						}	from '@angular/core'

import	{
			IncomingDataService
		}										from '@rcc/common'

import	{
			RccTransmissionRequestService,
		}										from '../../transmission-request.service'

/**
 * This service can trigger a transmission request for session data.
 */
@Injectable()
export class TriggerSessionRequestService {

	public constructor(
		private incomingDataService				: IncomingDataService,
		private rccTransmissionRequestService	: RccTransmissionRequestService
	){}

	/**
	 * Triggers a transmission request for session data. Waits for the other
	 * party's device to send new transmission data via the suggested channel
	 * and then announces the new transmission data to the app, for the regular
	 * transmission process to take over.
	 */
	public async requestSession() : Promise<void> {


		const transmissionMeta 	: unknown
								= await this.rccTransmissionRequestService.triggerRequest('session', 'SESSION_REQUESTS.TRIGGER.REQUEST_CONTENT')

		this.incomingDataService.next(transmissionMeta)

	}

}
