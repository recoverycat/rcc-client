import	{
			NgModule,
			ModuleWithProviders,
			Provider,
			InjectionToken
		}										from '@angular/core'

import	{

			provideHomePageEntry,
			Factory,
			Action,
			WithPosition,
			getEffectivePosition,
		}										from '@rcc/common'

import	{
			provideTranslationMap
		}										from '@rcc/common'


import	{
			RccTransmissionRequestModule,
		}										from '../../transmission-request.module'

import	{
			TriggerSessionRequestService
		}										from './trigger-session-request.service'


import en from './i18n/en.json'
import de from './i18n/de.json'



const REQUEST_SESSION_HOME_PAGE_ENTRY_CONFIG	: InjectionToken<WithPosition>
												= new InjectionToken<WithPosition>('Configuration for the request session home page entry.')



const homePageEntry	:Factory<Action> = {

	deps:		[
					TriggerSessionRequestService,
					REQUEST_SESSION_HOME_PAGE_ENTRY_CONFIG
				],

	factory:	(
					triggerSessionRequestService	: TriggerSessionRequestService,
					config							: WithPosition
				) => ({

					position:		getEffectivePosition(config, 3),
					label:			'SESSION_REQUESTS.TRIGGER.HOME_PAGE_ENTRY.LABEL',
					icon:			'share_data',
					description:	'SESSION_REQUESTS.TRIGGER.HOME_PAGE_ENTRY.DESCRIPTION',
					category:		'receive',
					handler:		() => triggerSessionRequestService.requestSession()

				})

}





const configProvider 	: 	Provider
						= 	{
								provide: 	REQUEST_SESSION_HOME_PAGE_ENTRY_CONFIG,
								useValue:	{ position: 0.1 }
							}


/**
 * This module enables the app to trigger transmission requests for session
 * data. It add a button to the home page that will do exactly that.
 */
@NgModule({
	imports:[
		RccTransmissionRequestModule
	],
	providers: [
		configProvider,
		TriggerSessionRequestService,
		provideHomePageEntry(homePageEntry),
		provideTranslationMap('SESSION_REQUESTS.TRIGGER',{ en,de })
	],
})

export class RccTriggerSessionRequestHomepageEntryModule {

	/**
	* This method adds a button to the homepage to request the transmission of a
	* Session.
	*
	* Calling it without parameter or importing the module with out calling
	* this method, will add entries to the home page automatically at reasonably
	* adequate positions.
	*
	* If you want to determine the positions yourself provide a `config`
	* parameter :
	*
	* | .position  | value                  | effect                                                    |
	* |------------|------------------------|-----------------------------------------------------------|
	* | .position  | positive number        | adds home page entry at position counting from the top    |
	* |            | negative number        | adds home page entry at position counting from the bottom |
	* |            | undefined        		| adds home page entry at a position somewhere in between   |
	* |------------|------------------------|-----------------------------------------------------------|
	*
	* Example: 	`RequestSessionHomePageEntryModule.addEntry({ position: 1 })`,
	*/

	public static addEntry(config?: WithPosition): ModuleWithProviders<RccTriggerSessionRequestHomepageEntryModule> {

		return {
			ngModule:	RccTriggerSessionRequestHomepageEntryModule,
			providers:	[{ provide: REQUEST_SESSION_HOME_PAGE_ENTRY_CONFIG, useValue: config }]
		}
	}
}
