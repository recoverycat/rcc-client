import	{
			BehaviorSubject,
			lastValueFrom
		}											from 'rxjs'

import	{
			RccTransmissionService,
			RccOverlayService,
			RccAlertController
		}											from '@rcc/common'

import	{
			assert,
			UserCanceledError
		}											from '@rcc/core'

import	{
			assertRequestData,
			RequestData
		}											from './transmission-request.commons'

import	{
			RequestPresentationModalComponent
		}											from './modals/request-presentation/request-presentation-modal.component'


import	{
			CodePresentationModalComponent
		}											from './modals/code-presentation/code-presentation-modal.component'





/**
 * This class represents an App fulfilling the transmission request from another one.
 * It is meant to make the whole data requesting process easier to read and is
 * only used in {@link RccTransmissionRequestService}.
 */
export class Requester {

	private meta			: unknown
	private requestedData	: string
	private	code 			: string
	private newMeta			: unknown
	private state$			: BehaviorSubject<string> 	= new BehaviorSubject<string>('initialized')

	/**
	 * This promise will reject with a {@link UserCanceledError},
	 * when the process was canceled prematurely.
	 */
	public	done			: Promise<string> = lastValueFrom(this.state$)

	private get state() 	: string  { return this.state$.value }


	public constructor(
		private requestData				: RequestData,
		private message					: string,
		private rccTransmissionService	: RccTransmissionService,
		private rccOverlayService		: RccOverlayService,
		private ccAlertController		: RccAlertController
	){
		assertRequestData(requestData)

		this.requestedData 	= 	requestData[1]
		this.meta 			= 	requestData[2]

	}

	/**
	 * Cancel the whole requesting process.
	 */
	public cancel() :void {
		this.state$.error(new UserCanceledError('Fulfiller canceled'))
	}

	public async presentRequest(message: string) : Promise<void> {

		assert(this.state === 'initialized', 		'Requester.presentRequest() can only be called in "initialized" state. Create new instance if need be.', this.state)

		const  presentation	:	Promise<unknown>
							=	this.rccOverlayService.relay(
									RequestPresentationModalComponent,
									{
										requestData: 	this.requestData,
										done:			this.done,
										message
									},
									{ fitContent: true }
								)
								.catch( e => this.state$.error(e) )

		this.state$.next('request_presented')

		await presentation

	}

	public async receiveCode() : Promise<void> {

		assert(this.state === 'request_presented', 	'Requester.receiveCode() can only be called in "request_presented" state. Create new instance if need be.', this.state)
		assert(!this.code, 							'Requester.receiveCode() code already received. Cannot overwrite. Call .presentPreliminaryRequestData() beforehand.')

		const data	: unknown
					= await 	this.rccTransmissionService.listen(this.meta)
								.catch(e => this.state$.error(e) )

		if(typeof data === 'string'){

			this.code = data
			this.state$.next('code_received')

		} else

			this.state$.error(new Error('code transmission failed; got wrong type of data.'))

	}

	public async presentCode() : Promise<void> {

		assert(this.state === 'code_received',		'Requester.presentCode() can only be called in "code_received" state. Call .receiveCode() beforehand.', this.state)

		const presentation 	:	Promise<unknown>
							= 	this.rccOverlayService.relay(
									CodePresentationModalComponent,
									{
										message: 	this.message,
										code:		this.code,
										done:		this.done
									}
								)
								.catch( e => this.state$.error(e) )

		this.state$.next('code_presented')

		await presentation

	}

	public async receiveNewMeta() : Promise<void> {

		assert(this.state === 'code_presented',		'Requester.receiveNewMeta() can only be called in "code_presented" state. Call .presentCode() beforehand.', this.state)

		this.newMeta = await 	this.rccTransmissionService.listen(this.meta)
								.catch( e => this.state$.error(e) )

		this.state$.next('new_meta_received')

	}


	public getNewMeta() : unknown {

		assert(this.state === 'new_meta_received',	'Requester.receiveNewMeta() can only be called in "new_meta_received" state. Call .receiveNewMeta() beforehand.', this.state)
		assert(this.newMeta,						'Requester.getNewMeta() missing newMeta.')

		this.state$.next('done')
		this.state$.complete()

		return this.newMeta
	}

}


