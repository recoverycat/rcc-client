import	{
			Injectable,
		}												from '@angular/core'


import	{
			Observable,
			filter,
		}												from 'rxjs'

import	{
			AbstractTransmissionService,
			IncomingDataService,
			RccAlertController,
			RccOverlayService,
			RccTransmissionService,
		}												from '@rcc/common'


import	{
			assert,
			UserCanceledError
		}												from '@rcc/core'

import	{
			RequestData,
			isRequestData
		}												from './transmission-request.commons'


import	{
			Requester
		}												from './requester.class'


import	{
			Fulfiller
		}												from './fulfiller.class'


/**
 * This service manages data requests. Unlike the transmission services,
 * this service enables the user to request another device/user to send data.
 *
 * It does not however just trigger data transmissions but manages a whole
 * process of of sender and recipient negotiating the transfer.
 */
@Injectable()
export class RccTransmissionRequestService {

	/**
	 * Will emit whenever a transmission request is detected.
	 */
	public	request$	:	Observable<RequestData>

	public constructor(
		protected readonly incomingDataService 		: IncomingDataService,
		protected readonly rccTransmissionService	: RccTransmissionService,
		protected readonly rccAlertController		: RccAlertController,
		protected readonly rccOverlayService		: RccOverlayService
	){

		this.request$ = 	this.incomingDataService
							.pipe(
								filter(isRequestData),
							)


	}


	/**
	 * Generates transmission meta data and wraps it together with an identifier
	 * for the requested data type into a bundle of {@link RequestData}.
	 *
	 * Thus bundle consists of all the information another party needs to send
	 * you the requested data.
	 *
	 */
	public async getNewRequestData(requestedData: string) : Promise<RequestData> {

		const meta		: unknown
						= await this.getNewRequestTransmissionMeta()

		return ['req', requestedData, meta]
	}

	/**
	 * Generates meta data for a transmission this device can receive data with.
	 * Prefers using a service that is also allowed for sending.
	 */
	public async getNewRequestTransmissionMeta() : Promise<unknown> {

		const receivingServices		: 	AbstractTransmissionService[]
									= 	this.rccTransmissionService.allowedReceivingServices

		const sendingService		: 	AbstractTransmissionService
									= 	this.rccTransmissionService.allowedSendingService

		// Since the requesting App usually has more than one service
		// allowed to receive data. We have to pick one: we go for the allowed
		// sending service and fallback to the first one we find.
		// If everything is on default settings, then it's very
		// likely, that this service is also the allowed sending service on the
		// device we request data from.
		const requestService		: 	AbstractTransmissionService
									= 	receivingServices.includes(sendingService)
										?	sendingService
										:	receivingServices[0]

		assert(requestService, 'TransmissionRequestService.getRequestTransmissionMeta: no allowed receiving transmission service found.')

		const meta 					:	unknown
									= 	await requestService.createMeta()

		return meta
	}

	/**
	 * Shows a QR code modal to the user containing the request data for a session.
	 * The modal will close, when the user cancels or when a transmission is
	 * received on the the embedded communication channel.
	 *
	 * @returns Meta data for the transmission of the actual requested data.
	 */
	public async triggerRequest(requestedData: string, message : string): Promise<unknown> {

		const requestData 		: 	RequestData
								= 	await 	this.getNewRequestData(requestedData)

		const payloadMeta		: 	unknown
								= 	await 	this.negotiateMeta({ requestData, role: 'request', message })
											.catch( e => {
												if(e instanceof UserCanceledError) throw e
												console.error(e)
												return this.retryTriggerRequest(requestedData, message)
											})

		return payloadMeta
	}


	public async retryTriggerRequest(requestedData: string, message: string) : Promise<unknown> {

		await this.rccAlertController.confirm('TRANSMISSION_REQUESTS.NEGOTIATING_RETRY')

		return await this.triggerRequest(requestedData, message)

	}


	/**
	 * Tries to verify the requesting source by sending a random string through
	 * the proposed channel (given by the transmission meta) and checking if the
	 * same random string can be delivered back by some other means.
	 *
	 * This method works for both sides of the process, skipping the part of the
	 * requester or fulfiller respectively. This way the whole process is in one
	 * place and easier to comprehend.
	 *
	 * @returns new transmission meta to send/receive the actual payload with
	 */
	public async negotiateMeta(config: {role: 'request'|'fulfill', requestData: RequestData, message : string }) : Promise<unknown> {

		const 	request 		: boolean 			= config.role === 'request'
		const 	fulfill 		: boolean			= config.role === 'fulfill'
		const	message			: string			= config.message
		const 	requestData		: RequestData		= config.requestData

		assert(requestData, 			'RccTransmissionRequestService.negotiateMeta() missing requestData.')
		assert(request || fulfill, 		'RccTransmissionRequestService.negotiateMeta() no role was specified. Pick either "request" or "fulfill".')

		let		requester		: Requester			= undefined
		let		fulfiller		: Fulfiller			= undefined



		if(fulfill) fulfiller 	= new Fulfiller(requestData, message, this.rccTransmissionService, this.rccOverlayService, this.rccAlertController)
		if(request) requester 	= new Requester(requestData, message, this.rccTransmissionService, this.rccOverlayService, this.rccAlertController)

		assert(!fulfiller || !requester, 'RccTransmissionRequestService.negotiateMeta() must not run with both roles!')
		assert( fulfiller ||  requester, 'RccTransmissionRequestService.negotiateMeta() must run with at least one role!')


		if(request)
			await 	Promise.race([
						requester.presentRequest(message),
						requester.receiveCode(),
						requester.done
					])


		if(fulfiller){
			void	fulfiller.showInitModal()
			void  	fulfiller.generateCode()

			await 	Promise.race([
						fulfiller.sendCode(),
						fulfiller.done
					])
		}

		if(requester)
			await 	Promise.race([
						requester.presentCode(),
						requester.receiveNewMeta(),
						requester.done
					])


		if(fulfiller){
			await 	Promise.race([
						fulfiller.verifyManualCodeInput(message),
						fulfiller.done
					])
			await 	Promise.race([
						fulfiller.sendNewMeta(),
						fulfiller.done
					])
		}

		if(requester)
			return 	requester.getNewMeta()

		if(fulfiller)
			return 	fulfiller.getNewMeta()


		throw new Error('RccTransmissionRequestService.negotiateMeta() failed to return new meta. Should never get to this point.')

	}

}
