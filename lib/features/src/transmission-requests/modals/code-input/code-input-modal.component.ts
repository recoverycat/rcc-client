import	{
			Component,
			OnDestroy,
			Inject
		}								from '@angular/core'

import	{
			FormControl,
			FormGroup
		}								from '@angular/forms'

import	{
			SharedModule,
			RccModalController,
			RELAY_PROPERTIES,
			OverlaysModule,
		}								from '@rcc/common'

import	{
			assert,
			assertProperty,
			UserCanceledError
		}								from '@rcc/core'
import	{	FormGroupOf				}	from '@rcc/core/types'

type FormGroupType = FormGroupOf<{ code: string }>

/**
 * Modal shown asking to enter the transmission code.
 */
@Component({
    selector: 'rcc-code-input-modal',
    templateUrl: './code-input-modal.component.html',
    styleUrls: ['./code-input-modal.component.scss'],
    imports: [
        SharedModule,
        OverlaysModule
    ]
})
export class CodeInputModalComponent implements OnDestroy{

	public message		: string
						= undefined

	public destroyed	: boolean
						= false


	public constructor(
		@Inject(RELAY_PROPERTIES)
		relayProperties		: unknown,
		private rccModalController	: RccModalController
	){
		assertProperty(relayProperties, ['message', 'done'])
		assert(typeof relayProperties.message === 'string',	'CodeInputModalComponent.constructor: relayProperties.message must be a string', relayProperties)
		assert(relayProperties.done instanceof Promise, 	'CodeInputModalComponent.constructor: relayProperties.done must be a instance of Promise', relayProperties)

		this.message = relayProperties.message

		void relayProperties.done
			.finally( ()=> this.rccModalController.dismiss() )
	}

	public cancel() : void {
		this.rccModalController.dismiss(new UserCanceledError())
	}

	public submit(): void {
		this.rccModalController.dismiss(this.formGroup.value.code)
	}

	public ngOnDestroy() : void {
		this.destroyed = true
	}

	protected formGroup: FormGroup<FormGroupType> = new FormGroup<FormGroupType>({
		code: new FormControl('')
	})
}
