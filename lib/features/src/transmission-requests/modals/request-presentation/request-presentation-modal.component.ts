import	{
			Component,
			Inject,
			OnDestroy
		}								from '@angular/core'

import	{
			SharedModule,
			RccModalController,
			RELAY_PROPERTIES,
			ShareDataModule
		}								from '@rcc/common'

import	{
			assert,
			assertProperty,
			UserCanceledError,
		}								from '@rcc/core'

import	{
			assertRequestData,
			RequestData
		}								from '../../transmission-request.commons'

/**
 * Modal shown showing the transmission code.
 */
@Component({
    selector: 'rcc-request-presentation-modal',
    templateUrl: './request-presentation-modal.component.html',
    imports: [
        SharedModule,
        ShareDataModule
    ]
})
export class RequestPresentationModalComponent implements OnDestroy {

	public requestData	: RequestData
						= undefined

	public destroyed	: boolean
						= false

	public message		: string
						= undefined

	public constructor(
		@Inject(RELAY_PROPERTIES)
		relayProperties		: unknown,
		private rccModalController	: RccModalController
	){
		assertProperty(relayProperties, ['requestData', 'done'])
		assertRequestData(relayProperties.requestData)
		assert(relayProperties.done instanceof Promise, 	'RequestPresentationModalComponent.constructor: relayProperties.done must be an instance of Promise', relayProperties)
		assert(typeof relayProperties.message === 'string', 'RequestPresentationModalComponent.constructor: relayProperties.message must be a string', relayProperties)

		this.requestData 	= relayProperties.requestData
		this.message		= relayProperties.message

		void relayProperties.done
			.finally( () => this.destroyed || this.rccModalController.dismiss() )
	}

	public cancel() : void {
		this.rccModalController.dismiss(new UserCanceledError())
	}

	public ngOnDestroy() :void {
		this.destroyed = true
	}
}
