import 	{
			assert,
			isErrorFree
		} 								from '@rcc/core'


// Needs to be rather short, so that everything fits into a qr-code
export type RequestData = [
	'req', 		// identifier
	string, 	// type of data requested
	unknown		// transmission meta data
]


export function assertRequestData(x: unknown): asserts x is RequestData {

	assert( Array.isArray(x),						'assertRequestData: not an Array.')
	assert(x[0] === 'req', 							'assertRequestData: [0] must be "req"')
	assert(typeof x[1] === 'string', 				'assertRequestData: [1] must be a string')
	assert(typeof x[3] !== undefined, 				'assertRequestData: [2] must not be undefined')

}

export function isRequestData(x: unknown) : x is RequestData {
	return isErrorFree( () => assertRequestData(x) )
}
