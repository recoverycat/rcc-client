import	{
			BehaviorSubject,
			lastValueFrom
		}											from 'rxjs'

import	{
			RccTransmissionService,
			RccOverlayService,
			RccTransmission,
			RccAlertController
		}											from '@rcc/common'

import	{
			assert,
			randomBiasedBase10String
		}											from '@rcc/core'

import	{
			assertRequestData,
			RequestData
		}											from './transmission-request.commons'

import	{
			BusyModalComponent
		}											from './modals/busy/busy-modal.component'


import	{
			CodeInputModalComponent
		}											from './modals/code-input/code-input-modal.component'
import	{	RccCodeVerifiedModalComponent		}	from '@rcc/themes/active'

/**
 * This class represents an App requesting a data transmission from another one.
 * It is meant to make the whole data requesting process easier to read and is
 * only used in {@link RccTransmissionRequestService}.
 */
export class Fulfiller {

	private meta			: unknown
	private requestedData	: string
	private code 			: string
	private newMeta			: unknown
	private state$			: BehaviorSubject<string> = new BehaviorSubject<string>('initialized')

	/**
	 * This promise will reject with a {@link UserCanceledError},
	 * when the process was canceled prematurely.
	 */
	public	done			: Promise<string> = lastValueFrom(this.state$)


	private get state() : string  { return this.state$.value }


	public constructor(
		private requestData				: RequestData,
		private message					: string,
		private rccTransmissionService	: RccTransmissionService,
		private rccOverlayService		: RccOverlayService,
		private rccAlertController		: RccAlertController
	){
		assertRequestData(requestData)

		this.requestedData 	= requestData[1]
		this.meta 			= requestData[2]
	}

	public showInitModal() : void {
		void this.rccOverlayService.relay(BusyModalComponent, { message: 'TRANSMISSION_REQUEST.REQUEST_RECEIVED' })
	}


	public generateCode() : void {

		assert(this.state === 'initialized', 	'Fulfiller.generateCode() can only be called in "initialized" state. Create new instance if need be.', this.state)
		assert(!this.code, 						'Fulfiller.generateCode() Code already exists; cannot be overwritten. Create new instance of Requester class if need be.')

		this.code = randomBiasedBase10String(6)

		this.state$.next('code_generated')
	}



	public async sendCode() : Promise<void> {

		assert(this.state === 'code_generated',	'Fulfiller.sendCode() can only be called in "code_generated" state. Call .generateCode() beforehand.', this.state)
		assert(this.code, 						'Fulfiller.sendCode() missing code. Call .generateCode() beforehand.')

		const	transmission 	: 	RccTransmission
								= 	await 	this.rccTransmissionService.setupWithExistingMeta(this.code, this.meta)
											.catch(e => { this.state$.error(e); throw e })

		// Send the random code through the transmission channel we want to
		// test:
		await 	transmission.start()
				.catch( e => this.state$.error(e) )

		this.state$.next('code_sent')

	}




	public async verifyManualCodeInput(message: string) : Promise<void> {

		assert(this.state === 'code_sent',		'Fulfiller.verifyManualCodeInput() can only be called in "code_sent" state. Call .sendCode() beforehand.', this.state)
		assert(this.code, 						'Fulfiller.verifyManualCodeInput() missing code. Call .generateCode() beforehand.')

		// Wait for the user to enter the transmission code received by some
		// other means; e.g. the other party verbally informed us.
		const	returnedCode	: 	unknown
								= 	await 	this.rccOverlayService.relay(
												CodeInputModalComponent,
												{
													done:		this.done,
													message
												},
											)
											.catch( e => { this.state$.error(e); throw e } )

		const codeChecksOut		: 	boolean
								= 	typeof returnedCode === 'string'
									&&
									this.code === returnedCode

		if(codeChecksOut)
			this.state$.next('code_verified')
		else{
			void this.rccAlertController.present({ message: 'TRANSMISSION_REQUESTS.TAN.INPUT.MISMATCH' })
			this.state$.error(new Error('transmitted code does not match entered code.') )
		}

	}




	public async sendNewMeta() : Promise<void> {

		assert(this.state === 'code_verified',	'Fulfiller.sendNewMeta() can only be called in "code_verified" state. Call .verifyManualCodeInput() beforehand.', this.state)
		assert(!this.newMeta,					'Fulfiller.getNewMeta() newMeta already exists. Cannot overwrite.')

		await this.rccOverlayService.relay(RccCodeVerifiedModalComponent)

		const newMeta 		: unknown
							= await this.rccTransmissionService.createMeta()
									.catch(e => { this.state$.error(e); throw e })


		const transmission 	: RccTransmission
							= await this.rccTransmissionService.setupWithExistingMeta(newMeta, this.meta)
									.catch(e => { this.state$.error(e); throw e })
		assert(transmission)

		this.newMeta 		= newMeta

		await 	transmission.start()
				.catch(e => this.state$.error(e) )

		this.state$.next('new_meta_sent')
	}



	public getNewMeta() : unknown {

		assert(this.state === 'new_meta_sent',	'Fulfiller.getNewMeta() can only be called in "new_meta_sent" state. Call .sendNewMeta() beforehand.', this.state)
		assert(this.newMeta,					'Fulfiller.getNewMeta() missing newMeta.')

		this.state$.next('done')

		return this.newMeta

	}




}
