import	{
			NgModule,
			ModuleWithProviders,
			Provider,
		}								from '@angular/core'
import	{

			provideHomePageEntry,
			Factory,
			Action,
			WithPosition,
			getEffectivePosition
		}								from '@rcc/common'
import	{	EntryShareServiceModule	}	from './entry-share.module'
import	{	EntryShareService		}	from './entry-share.service'


@NgModule({
	imports:[
		EntryShareServiceModule,
	],
	providers: [
		EntryShareService,
	],
})

export class EntryShareServiceHomePageEntryModule {

	/**
	* This method can add entries to the home page.
	*
	* Calling it without parameter, will add entries to the home
	* page automatically at reasonably adequate positions.
	*
	* If you want to determine the positions yourself provide a `config` parameter :
	*
	* | .position  | value                  | effect                                                    |
	* |------------|------------------------|-----------------------------------------------------------|
	* | .position  | true                   | adds home page entry at a reasonably adequate position    |
	* |            | positive number        | adds home page entry at position counting from the top    |
	* |            | negative number        | adds home page entry at position counting from the bottom |
	* |            | undefined        		| adds home page entry at a position somewhere in between   |
	* |------------|------------------------|-----------------------------------------------------------|
	*
	* Example: 	`QrCodeHomePageEntryModule.addEntry({ position: 1 })`,
	*/

	public static addEntry(config?: WithPosition): ModuleWithProviders<EntryShareServiceHomePageEntryModule> {

		const homePageEntry	:Factory<Action> = {

			deps:		[EntryShareService],
			factory:	(entryShareService: EntryShareService) => ({
							position:		getEffectivePosition(config, 3),
							label:			'ENTRY_SHARE.HOME_ENTRY.LABEL',
							icon:			'share_data',
							description:	'ENTRY_SHARE.HOME_ENTRY.DESCRIPTION',
							category:		'share',
							handler:		() => entryShareService.shareSymptomCheckAsSession()
						})

		}

		const providers : Provider[] = [provideHomePageEntry(homePageEntry)]

		return {
			ngModule:	EntryShareServiceHomePageEntryModule,
			providers
		}
	}
}
