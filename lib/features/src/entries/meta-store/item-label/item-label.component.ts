import	{	Component 			}	from '@angular/core'
import	{	Entry				}	from '@rcc/core'

@Component({
    templateUrl: './item-label.component.html',
    standalone: false
})
export class EntryLabelComponent {
	public constructor(public entry: Entry){}
}
