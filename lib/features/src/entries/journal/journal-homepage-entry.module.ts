import	{
			NgModule,
			Provider,
			ModuleWithProviders,
		}									from '@angular/core'

import	{
			Report
		}									from '@rcc/core'

import	{
			provideHomePageEntry,
			Factory,
			Action,
			RccModalController,
			WithPosition,
			getEffectivePosition
		}									from '@rcc/common'

import	{
			JournalService
		}									from './journal.service'

import	{
			JournalServiceModule,
		}									from './journal.module'

import	{
			ReportViewComponent,
		}									from '../../reports/basic-view'

@NgModule({
	imports: [
		JournalServiceModule,
	],

})

export class JournalServiceHomePageEntryModule {

	/**
	* This method can add an entry to the home page.
	*
	* Calling it without parameter, will add an entry to home
	* page automatically at reasonably adequate positions.
	*
	* If you want to determine the positions yourself provide a `config` parameter :
	*
	* | .position  | value                  | effect                                                   |
	* |------------|------------------------|-----------------------------------------------------------|
	* | .position  | undefined, false, null | adds no home page entry                                   |
	* |            | true                   | adds home page entry at a reasonably adequate position    |
	* |            | positive number        | adds home page entry at position counting from the top    |
	* |            | negative number        | adds home page entry at position counting from the bottom |
	* Example: 	`JournalServiceHomePageEntryModule.addEntry({ position: 1 })`,

	*/

	public static addEntry(config?: WithPosition): ModuleWithProviders<JournalServiceHomePageEntryModule> {

		const homePageEntry : Factory<Action>	= 	{

			deps:		[JournalService, RccModalController],
			factory:	(journalService: JournalService, rccModalController: RccModalController) => ({
								position: 		getEffectivePosition(config, .5),
								label: 			'JOURNAL.HOME.VIEW_DATA.LABEL',
								description:	'JOURNAL.HOME.VIEW_DATA.DESCRIPTION',
								icon: 			'data',
								category:		'analyze',
								handler:	async () => {
									const report : Report = Report.from(journalService.items)

									// TODO: This should be a method of a ReportService
									// We should not be able to use ReportViewComponent without
									// importing ReportModule first, because then among other issues
									// the translation strings are missing.
									await rccModalController.present(ReportViewComponent, { report  })
								},
						}),
		}

		const providers : Provider[] = [provideHomePageEntry(homePageEntry)]

		return {
			ngModule:	JournalServiceHomePageEntryModule,
			providers
		}
	}
}
