import	{	NgModule	}	from '@angular/core'
import	{	RccDummySupportService				}	from './dummy-support.service'
import	{
			RccUserSupportModule,
			RccUserSupportService
		}										from '../'


@NgModule({
	providers:		[
						RccDummySupportService,
						{
							provide:		RccUserSupportService,
							useExisting:	RccDummySupportService
						}
					],
	imports:		[
						RccUserSupportModule
					]


})
/* FOR TESTING PURPOSES: The `RccDummySupportModule` is an Angular module that provides the
`RccDummySupportService` and configures it to be used as a
replacement for the `RccUserSupportService` for testing purposes. It also imports the `RccUserSupportModule`. */
export class RccDummySupportModule {}
