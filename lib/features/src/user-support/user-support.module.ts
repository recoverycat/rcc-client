import	{	RccUserSupportService		}	from './user-support.service'
import	{	RccUserSupportFormComponent	}	from './user-support-form.component'
import	{	RccUserSupportPageComponent	}	from './user-support-page.component'
import	{	NgModule					}	from '@angular/core'
import	{ 	RouterModule, Routes	 	}	from '@angular/router'
import	{
			SharedModule,
			provideTranslationMap,
			provideIcon,
			Action,
			IconAlias
		}									from '@rcc/common'

import en from './i18n/en.json'
import de from './i18n/de.json'

const routes	:	Routes
				=	[
						{ path: 'support-form', component: RccUserSupportPageComponent }
					]

export const gotoSupportFormAction : Action = {
	path:			'/support-form',
	label:			'USER_SUPPORT.ACTIONS.FORM.LABEL',
	description:	'USER_SUPPORT.ACTIONS.FORM.DESCRIPTION',
	icon:			'send_support_request', // was share_data
	category:		'share'
}


export const rccSendSupportRequestIcon : IconAlias = {
	name : 'send_support_request',
	from : 'share_data'
}

@NgModule({
	providers:		[
						RccUserSupportService,
						provideTranslationMap('USER_SUPPORT', { en, de }),
						provideIcon(rccSendSupportRequestIcon)
					],
	imports:		[
						RouterModule.forRoot(routes),
						SharedModule
					],
	declarations:	[
						RccUserSupportPageComponent,
						RccUserSupportFormComponent
					]

})
export class RccUserSupportModule {}
