import	{	Component					}	from '@angular/core'

@Component({
    selector: 'rcc-support-page',
    templateUrl: './user-support-page.component.html',
    styleUrls: ['./user-support-page.component.css'],
    standalone: false
})

export class RccUserSupportPageComponent {}
