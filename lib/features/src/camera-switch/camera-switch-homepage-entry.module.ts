import	{
			NgModule,
			ModuleWithProviders,
			Provider,
		}									from '@angular/core'
import	{
			provideHomePageEntry,
			Factory,
			Action,
			WithPosition,
			getEffectivePosition
		}									from '@rcc/common'
import	{	CameraSwitchServiceModule	}	from './camera-switch.module'
import	{	CameraSwitchService			}	from './camera-switch.service'


@NgModule({
	imports:[
		CameraSwitchServiceModule,
	],
	providers: [
		CameraSwitchService,
	],
})

export class CameraSwitchHomePageEntryModule {

	/**
	* This method can add entries to the home page.
	*
	* Calling it without parameter, will add entries to the home
	* page automatically at reasonably adequate positions.
	*
	* If you want to determine the positions yourself provide a `config` parameter :
	*
	* | .position  | value                  | effect                                                    |
	* |------------|------------------------|-----------------------------------------------------------|
	* | .position  | true                   | adds home page entry at a reasonably adequate position    |
	* |            | positive number        | adds home page entry at position counting from the top    |
	* |            | negative number        | adds home page entry at position counting from the bottom |
	* |            | undefined        		| adds home page entry at a position somewhere in between   |
	* |------------|------------------------|-----------------------------------------------------------|
	*
	* Example: 	`QrCodeHomePageEntryModule.addEntry({ position: 1 })`,
	*/

	public static addEntry(config?: WithPosition): ModuleWithProviders<CameraSwitchHomePageEntryModule> {

		const homePageEntry	:Factory<Action> = {

			deps:		[CameraSwitchService],
			factory:	(cameraSwitchService: CameraSwitchService) => ({
							position:		getEffectivePosition(config, 3),
							label:			'ENTRY_SHARE.HOME_ENTRY.LABEL',
							icon:			'share_data',
							description:	'ENTRY_SHARE.HOME_ENTRY.DESCRIPTION',
							category:		'share',
							handler:		() => cameraSwitchService.send()
						})

		}

		const providers : Provider[] = [provideHomePageEntry(homePageEntry)]

		return {
			ngModule:	CameraSwitchHomePageEntryModule,
			providers
		}
	}
}
