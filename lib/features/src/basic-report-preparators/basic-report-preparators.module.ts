import 	{
			NgModule,
			Injectable
		}								from '@angular/core'

import	{
			Report,
		}								from '@rcc/core'

import	{
			QuestionnaireService,
			QuestionnaireServiceModule
		}								from '../questions'

import	{
			DataVisualizationModule,
			Dataset,
			ReportPreparator
		}								from '../data-visualization'





@Injectable()
export class QuestionReportPreparator extends ReportPreparator {

	public constructor(
		public questionnaireService: QuestionnaireService
	){
		super()
	}



	public async prepare(report: Report): Promise<Dataset[]> {

		const questions = await this.questionnaireService.get(report.questionIds)

		return 	questions.map( question => ({
					question,
					datapoints:	report.entries
								.filter( 	entry =>  	entry.questionId === question.id)
								.filter(	entry =>	entry.targetDay )
								.filter(	entry =>	entry.answer !== null)
								.filter(	entry =>	entry.answer !== undefined)
								.map( 		entry => 	({
															value: 	entry.answer,
															note: 	entry.note,
															date:	entry.targetDay
														})
								)
				}))


	}
}



@NgModule({
	imports: [
		QuestionnaireServiceModule,
		DataVisualizationModule.forChild( [QuestionReportPreparator] )
	],
	providers: [
		QuestionReportPreparator,
	]

})
export class BasicReportPreparatorModule {}
