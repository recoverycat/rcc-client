import { Component } from '@angular/core'
import { PathAction, ShareDataService } from '@rcc/common'
import { Session } from '@rcc/core'
import { ConfirmPayload, ExistingSymptomCheck } from '@rcc/features/questionnaire-editor/questionnaire-editor/questionnaire-editor.component'
import { OpenSessionStoreService } from '../../open-sessions'
import { Observable } from 'rxjs'
import { Router } from '@angular/router'

@Component({
    selector: 'rcc-edit-active-symptom-check',
    templateUrl: './edit-active-symptom-check.component.html',
    styleUrls: ['edit-active-symptom-check.component.scss'],
    standalone: false
})
export class RccEditActiveSymptomCheckComponent {
	public constructor(
		private readonly shareDataService			: ShareDataService,
		private readonly openSessionStoreService	: OpenSessionStoreService,
		private readonly router						: Router,
	) {
		if (!this.openSessionStoreService.activeSession)
			void this.router.navigate(['..'])
	}

	protected backAction: PathAction = {
		path: '..',
		icon: '',
		label: 'CANCEL'
	}

	public session$: Observable<Session> = this.openSessionStoreService.activeSession$

	public get activeSymptomCheck(): ExistingSymptomCheck {
		const session: Session = this.openSessionStoreService.activeSession

		if (session == null) return

		return {
			symptomCheck: session.symptomCheck,
			questions: session.questions,
		}
	}

	protected async shareEditedSymptomCheck(event: ConfirmPayload): Promise<void> {
		await this.shareDataService.share([event.config, event.questions], 'send')
	}
}
