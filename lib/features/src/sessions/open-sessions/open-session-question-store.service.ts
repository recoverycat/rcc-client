import	{
			Injectable,
			Injector
		}								from '@angular/core'
import	{
			Question,
			QuestionStore
		}								from '@rcc/core'
import	{
			OpenSessionStoreService
		}								from './open-session-store.service'

@Injectable()
export class OpenSessionQuestionStoreService extends QuestionStore {

	public name = 'SESSIONS.OPEN_SESSIONS.QUESTION_STORE.NAME'

	private openSessionStoreService : OpenSessionStoreService


	public constructor(
		private injector : Injector
	){
		super()
	}

	public get map(): Map<string, Question> {

		// If we inject OpenSessionStoreService the usual way, we get a dependency loop.
		if(!this.openSessionStoreService) this.openSessionStoreService = this.injector.get(OpenSessionStoreService)

		const questions = this.openSessionStoreService.activeSession?.questions || []

		return new Map(questions.map( question => [question.id, question] ) )
	}

	public set map(m: Map<string, Question>) {}


}
