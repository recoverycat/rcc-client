import	{
			Injectable,
		}									from '@angular/core'

import	{
			HomePageEntry,
			RccHomePageEntryService,
		}									from '@rcc/common'
import	{	OpenSessionStoreService		}	from './open-session-store.service'
import { Session } from '@rcc/core'

@Injectable()
export class OpenSessionHomePageEntryService{
	public constructor(
		public readonly openSessionStoreService: OpenSessionStoreService,
		private readonly homepageEntryService: RccHomePageEntryService,
	){
		void this.openSessionStoreService.ready.then(() => {
			this.init()
		})
	}

	private homePageEntries: Map<Session, HomePageEntry> = new Map()

	private init(): void {
		this.openSessionStoreService.addition$.subscribe((addition: Session) => {
			this.addSession(addition)
		})

		this.openSessionStoreService.removal$.subscribe((removed: Session) => {
			this.removeSession(removed)
		})

		this.openSessionStoreService.items.forEach((session: Session) => {
			this.addSession(session)
		})
	}

	private addSession(session: Session): void {
		const homePageEntry: HomePageEntry = { item: session, position: -1, itemActionRoles: ['edit', 'destructive'] }
		this.homePageEntries.set(session, homePageEntry)
		this.homepageEntryService.addHomePageEntry(homePageEntry)
	}

	private removeSession(session: Session): void {
		const homePageEntry: HomePageEntry = this.homePageEntries.get(session)
		this.homePageEntries.delete(session)
		this.homepageEntryService.removeHomePageEntry(homePageEntry)
	}
}
