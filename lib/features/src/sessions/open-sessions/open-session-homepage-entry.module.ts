import	{
			NgModule,
		}											from '@angular/core'
import { OpenSessionModule } from './open-session.module'
import { OpenSessionHomePageEntryService } from './open-session-homepage-entry.service'


@NgModule({
	imports:[
		OpenSessionModule,
	],
	providers: [
		OpenSessionHomePageEntryService,
	],
})


export class OpenSessionHomePageEntryModule {
	public constructor(
		private readonly openSessionHomePageEntryService: OpenSessionHomePageEntryService
	) {}
}
