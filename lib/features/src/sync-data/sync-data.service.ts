import { Injectable } from '@angular/core'
import { IncomingDataService, RccPublicRuntimeConfigService } from '@rcc/common'
import { SymptomCheckMetaStoreService } from '@rcc/features/symptom-checks'
import { AnswerType, assertQuestionConfig, DAILY_NOTES_ID, Question, QuestionConfig, SymptomCheck } from '@rcc/core'
import { take } from 'rxjs'
import { QuestionnaireService } from '@rcc/features'

/**
 * The purpose of this service is to help to fix missing questionConfigs in the 'import-question-store'
 * based on existing SymptomChecks and the referenced question ids.
 */
@Injectable()
export class SyncDataService {
	private QUESTION_CONFIGS_BACKUP_KEY: string = 'questionConfigsBackup'
	private UNKNOWN_TYPE: AnswerType = 'unknown'

	public constructor(
		private readonly incomingDataService: IncomingDataService,
		private readonly publicRuntimeConfigService: RccPublicRuntimeConfigService,
		private readonly questionnaireService: QuestionnaireService,
		private readonly symptomCheckMetaStoreService: SymptomCheckMetaStoreService
	) {
		void this.sync()
	}

	/**
	 * Synchronizes question configurations with existing symptom checks.
	 *
	 * The method performs the following steps:
	 * 1. Gets the backup of QuestionConfigs from the runtime configuration.
	 * 2. Gets the existing SymptomChecks.
	 * 3. Extracts questions referenced by the SymptomChecks.
	 * 4. Filters QuestionConfigs based on whether their related questions exist, their type,
	 *    and ensures they are not the daily note type.
	 * 5. Sends the filtered QuestionConfigs to the IncomingDataStore.
	 *
	 * If no action is required at any step (e.g., missing configs or symptom checks),
	 * the method exits early with a log message.
	 *
	 * @returns {Promise<void>} A promise that resolves when the synchronization process completes.
	 * @private
	 */
	private async sync(): Promise<void> {

		// 1. Get the QuestionConfigs store in the RuntimeConfig

		await this.publicRuntimeConfigService.ready
		const rawQuestionConfigsBackup: unknown = await this.publicRuntimeConfigService.get(this.QUESTION_CONFIGS_BACKUP_KEY)
		const questionConfigsBackup: QuestionConfig[] | null = this.unknownToQuestionConfigs(rawQuestionConfigsBackup)

		if (!questionConfigsBackup || questionConfigsBackup.length === 0) {
			console.info('SyncData - Nothing to do -> No questionConfigsBackup found')
			return
		}

		// 2. Get existing SymptomChecks

		await this.symptomCheckMetaStoreService.ready
		const symptomChecks: SymptomCheck[] = this.symptomCheckMetaStoreService.items

		if (symptomChecks.length === 0) {
			console.info('SyncData - Nothing to do -> No symptomChecks')
			return
		}

		// 3. Get Questions from existing SymptomChecks

		await this.questionnaireService.ready
		const questionIdsFromSymptomChecks: string[] = symptomChecks.flatMap(symptomCheck => symptomCheck.questionIds)

		// It looks like QuestionnaireService.get will only return null if you pass null or undefined as the id_or_ids parameter.
		const questionsFromSymptomChecks: Question[] | null = await this.questionnaireService.get(questionIdsFromSymptomChecks)

		if (!questionsFromSymptomChecks || questionsFromSymptomChecks.length === 0) {
			console.info('SyncData - Nothing to do -> No questions in symptomChecks')
			return
		}

		// 4. Filter QuestionsConfigs to be stored based on whether their related Question (obtained from 3) exists or not and based on their type.

		const unknownQuestionsFromSymptomChecks: Question[] = questionsFromSymptomChecks
			.filter(question => question !== null)
			.filter(
				(question) =>
					question.id !== DAILY_NOTES_ID &&
					question.type === this.UNKNOWN_TYPE
			)

		const questionConfigsToBeImported: QuestionConfig[] = questionConfigsBackup.filter(
			(questionConfig) =>
				unknownQuestionsFromSymptomChecks
					.filter(question => question !== null)
					.find((question) => question.id === questionConfig.id)
		)

		if (questionConfigsToBeImported.length !== unknownQuestionsFromSymptomChecks.length)
			console.warn('SyncData - Unknown questions not found in backup store ->', unknownQuestionsFromSymptomChecks.map(question => question.id))

		if (questionConfigsToBeImported.length === 0) {
			console.info('SyncData - Nothing to do -> questionConfigs found in ImportStore')
			return
		}

		// 5. Store QuestionConfigs sending them to the IncomingDataStore

		this.questionnaireService.change$
			.pipe(
				take(questionConfigsToBeImported.length)
			)
			.subscribe((question) => {
				const expected: boolean = unknownQuestionsFromSymptomChecks.find((q) => q.id === question.id) !== undefined
				console.info(`SyncData - Question stored(expected:${expected}) ->`, question)
			})

		this.incomingDataService.next(questionConfigsToBeImported)
		console.info(`SyncData - Questions - stored: ${questionConfigsToBeImported.length}, unknown: ${unknownQuestionsFromSymptomChecks.length}, offset: ${unknownQuestionsFromSymptomChecks.length - questionConfigsToBeImported.length}`)

	}

	/**
	 * Validates and converts raw question configs to a strongly typed QuestionConfig array.
	 *
	 * @param rawQuestionConfigsBackup - The raw data to be validated and converted.
	 * @returns A valid QuestionConfig array, or null if the data is invalid.
	 * @throws {AssertionError} If validation of a question config fails.
	 * @private
	 */
	private unknownToQuestionConfigs(rawQuestionConfigsBackup: unknown): QuestionConfig[] | null {
		if (!Array.isArray(rawQuestionConfigsBackup)) throw Error('SyncData - Invalid data type -> Expected array for questionConfigsBackup')

		try {
			rawQuestionConfigsBackup.forEach(assertQuestionConfig)
		} catch (error) {
			throw Error('SyncData - Invalid QuestionConfig data')

		}

		return rawQuestionConfigsBackup as QuestionConfig[]
	}


}
