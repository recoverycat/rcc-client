import { NgModule } from '@angular/core'

import { requestPublicRuntimeConfigValue } from '@rcc/common'

import { SyncDataService } from './sync-data.service'


@NgModule({
	imports: [],
	providers: [
		SyncDataService,

		/* The `requestPublicRuntimeConfigValue` function is being used to request a
		specific value from the public runtime configuration. In this case, it is
		requesting a value located at the path 'questionConfigsBackup'. */
		requestPublicRuntimeConfigValue({
			path: 'questionConfigsBackup',
			description: 'A dictionary of questionConfigs that are used for synchronizing question versions',
			type: 'QuestionConfig',
			required: false
		})
	]
})
export class SyncDataModule {

	public constructor(
		private syncDataService: SyncDataService,
	) {
	}
}
