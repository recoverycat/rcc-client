import { fakeAsync, TestBed, tick } from '@angular/core/testing'
import { SyncDataService } from './sync-data.service'
import { IncomingDataService, IncomingDataServiceModule, RccPublicRuntimeConfigService } from '@rcc/common'
import { SymptomCheckMetaStoreService } from '@rcc/features/symptom-checks'
import { QuestionnaireService } from '@rcc/features'
import { of, Subject } from 'rxjs'
import { DAILY_NOTES_ID, Question, QuestionConfig, unknownConfig } from '@rcc/core'

import questionConfigsBackup from './fixtures/questionConfigsBackup.json'
import symptomCheckConfigs from './fixtures/symptomChecks.json'
import importQuestionStore from './fixtures/importQuestionStore.json'

describe('SyncDataService', () => {

	beforeEach(() => {
		// Due to an error/warning saying that "Some of your tests did a full page reload!", the following fix was found
		// https://stackoverflow.com/questions/29352578/some-of-your-tests-did-a-full-page-reload-error-when-running-jasmine-tests
		window.onbeforeunload = jasmine.createSpy()
	})

	describe('With empty SymptomChecks', () => {

		let service: SyncDataService = undefined!
		let mockPublicRuntimeConfigService: jasmine.SpyObj<RccPublicRuntimeConfigService> = undefined!
		let mockQuestionnaireService: jasmine.SpyObj<QuestionnaireService> = undefined!
		let mockSymptomCheckMetaStoreService: jasmine.SpyObj<SymptomCheckMetaStoreService> = undefined!

		beforeEach(() => {

			mockPublicRuntimeConfigService = jasmine.createSpyObj('RccPublicRuntimeConfigService', ['get'], { ready: Promise.resolve() }) as jasmine.SpyObj<RccPublicRuntimeConfigService>
			mockQuestionnaireService = jasmine.createSpyObj('QuestionnaireService', ['get'], {
				ready: Promise.resolve(),
				change$: of()
			}) as jasmine.SpyObj<QuestionnaireService>
			mockSymptomCheckMetaStoreService = jasmine.createSpyObj('SymptomCheckMetaStoreService', [], {
				ready: Promise.resolve(),
				items: []
			}) as jasmine.SpyObj<SymptomCheckMetaStoreService>

			TestBed.configureTestingModule({
				imports: [
					IncomingDataServiceModule
				],
				providers: [
					SyncDataService,
					{ provide: RccPublicRuntimeConfigService, useValue: mockPublicRuntimeConfigService },
					{ provide: QuestionnaireService, useValue: mockQuestionnaireService },
					{ provide: SymptomCheckMetaStoreService, useValue: mockSymptomCheckMetaStoreService }
				]
			})
			service = TestBed.inject(SyncDataService)

		})

		it('should be created for (With empty SymptomChecks', () => {
			mockPublicRuntimeConfigService.get.and.resolveTo([])

			expect(service).toBeTruthy()
		})

		it('should not emit data if questionConfigsBackup is empty', fakeAsync(async () => {
			const incomingDataService: IncomingDataService = TestBed.inject(IncomingDataService)
			mockPublicRuntimeConfigService.get.and.resolveTo([])

			let valueEmitted: boolean = false
			incomingDataService.subscribe(() => {
				valueEmitted = true
			})

			await service['sync']()
			tick(1000)

			expect(valueEmitted).toBeFalse()
		}))

		it('should not emit data if there are no SymptomChecks', fakeAsync(async () => {
			const incomingDataService: IncomingDataService = TestBed.inject(IncomingDataService)
			mockPublicRuntimeConfigService.get.and.resolveTo(questionConfigsBackup)

			let valueEmitted: boolean = false
			incomingDataService.subscribe(() => {
				valueEmitted = true
			})

			await service['sync']()
			tick(1000)

			expect(valueEmitted).toBeFalse()
		}))

	})

	describe('With SymptomChecks', () => {

		let service: SyncDataService = undefined!
		let mockPublicRuntimeConfigService: jasmine.SpyObj<RccPublicRuntimeConfigService> = undefined!
		let mockQuestionnaireService: jasmine.SpyObj<QuestionnaireService> = undefined!
		let mockSymptomCheckMetaStoreService: jasmine.SpyObj<SymptomCheckMetaStoreService> = undefined!

		const submitQuestion: Subject<Question> = new Subject<Question>()

		beforeEach(() => {

			mockPublicRuntimeConfigService = jasmine.createSpyObj('RccPublicRuntimeConfigService', ['get'], { ready: Promise.resolve() }) as jasmine.SpyObj<RccPublicRuntimeConfigService>
			mockQuestionnaireService = jasmine.createSpyObj('QuestionnaireService', ['get'], {
				ready: Promise.resolve(),
				change$: submitQuestion.asObservable()
			}) as jasmine.SpyObj<QuestionnaireService>
			mockSymptomCheckMetaStoreService = jasmine.createSpyObj('SymptomCheckMetaStoreService', [], {
				ready: Promise.resolve(),
				items: symptomCheckConfigs
			}) as jasmine.SpyObj<SymptomCheckMetaStoreService>

			TestBed.configureTestingModule({
				imports: [
					IncomingDataServiceModule
				],
				providers: [
					SyncDataService,
					{ provide: RccPublicRuntimeConfigService, useValue: mockPublicRuntimeConfigService },
					{ provide: QuestionnaireService, useValue: mockQuestionnaireService },
					{ provide: SymptomCheckMetaStoreService, useValue: mockSymptomCheckMetaStoreService }
				]
			})
			service = TestBed.inject(SyncDataService)

		})

		it('should be created for (With SymptomChecks)', () => {
			mockPublicRuntimeConfigService.get.and.resolveTo([])

			expect(service).toBeTruthy()
		})

		it('should not emit data if QuestionConfigs are found in ImportStore', fakeAsync(async () => {
			const incomingDataService: IncomingDataService = TestBed.inject(IncomingDataService)
			mockPublicRuntimeConfigService.get.and.resolveTo(questionConfigsBackup)

			const questionConfigs: QuestionConfig[] = importQuestionStore as QuestionConfig[]
			const questions: Question[] = questionConfigs.map(qConfigs => new Question(qConfigs))
			mockQuestionnaireService.get.and.resolveTo(questions)

			let valueEmitted: boolean = false
			incomingDataService.subscribe(() => {
				valueEmitted = true
			})

			await service['sync']()
			tick(1000)

			expect(valueEmitted).toBeFalse()
		}))

		it('should not emit data if symptomChecks is empty', fakeAsync(async () => {
			const incomingDataService: IncomingDataService = TestBed.inject(IncomingDataService)
			mockPublicRuntimeConfigService.get.and.resolveTo(questionConfigsBackup)

			mockQuestionnaireService.get.and.resolveTo([])

			let valueEmitted: boolean = false
			incomingDataService.subscribe(() => {
				valueEmitted = true
			})

			await service['sync']()
			tick(1000)

			expect(valueEmitted).toBeFalse()
		}))

		it('should not emit data if symptomChecks is null', fakeAsync(async () => {
			const incomingDataService: IncomingDataService = TestBed.inject(IncomingDataService)
			mockPublicRuntimeConfigService.get.and.resolveTo(questionConfigsBackup)

			mockQuestionnaireService.get.and.resolveTo(null)

			let valueEmitted: boolean = false
			incomingDataService.subscribe(() => {
				valueEmitted = true
			})

			await service['sync']()
			tick(1000)

			expect(valueEmitted).toBeFalse()
		}))

		it('should filter and import valid QuestionConfigs accordingly with "null" in questions', fakeAsync(async () => {
			const incomingDataService: IncomingDataService = TestBed.inject(IncomingDataService)
			mockPublicRuntimeConfigService.get.and.resolveTo(questionConfigsBackup)

			const questions: Question[] = symptomCheckConfigs
				.flatMap(symptomCheckConfig => symptomCheckConfig.questions)
				.map(question => question.id)
				.map(questionId => unknownConfig(questionId))
				.map(questionConfig => new Question(questionConfig))
				.fill(null, 0, 1)

			mockQuestionnaireService.get.and.resolveTo(questions)

			let emission: QuestionConfig[] = []
			incomingDataService.subscribe(value => {
				emission = value as QuestionConfig[]
			})

			await service['sync']()
			tick(1000)

			emission.map(q => new Question(q)).map(question => submitQuestion.next(question))

			tick(1000)

			const length: number = questions
				.filter(question => question !== null)
				.filter(question => question.id !== DAILY_NOTES_ID).length

			expect(emission.length).toBe(length)
			expect(emission.length).toBe(2)

		}))

		it('should filter and import valid QuestionConfigs', fakeAsync(async () => {
			const incomingDataService: IncomingDataService = TestBed.inject(IncomingDataService)
			mockPublicRuntimeConfigService.get.and.resolveTo(questionConfigsBackup)

			const questions: Question[] = symptomCheckConfigs
				.flatMap(symptomCheckConfig => symptomCheckConfig.questions)
				.map(question => question.id)
				.map(questionId => unknownConfig(questionId))
				.map(questionConfig => new Question(questionConfig))

			mockQuestionnaireService.get.and.resolveTo(questions)

			let emission: QuestionConfig[] = []
			incomingDataService.subscribe(value => {
				emission = value as QuestionConfig[]
			})

			await service['sync']()
			tick(1000)

			emission.map(q => new Question(q)).map(question => submitQuestion.next(question))

			tick(1000)

			const length: number = questions.filter(question => question.id !== DAILY_NOTES_ID).length

			expect(emission.length).toBe(length)
			expect(emission.length).toBe(3)

		}))

		it('should filter and import valid QuestionConfigs with offset', fakeAsync(async () => {
			const incomingDataService: IncomingDataService = TestBed.inject(IncomingDataService)
			mockPublicRuntimeConfigService.get.and.resolveTo(questionConfigsBackup.filter(question => question.id !== 'rcc-curated-default-01-daily-2'))

			const questions: Question[] = symptomCheckConfigs
				.flatMap(symptomCheckConfig => symptomCheckConfig.questions)
				.map(question => question.id)
				.map(questionId => unknownConfig(questionId))
				.map(questionConfig => new Question(questionConfig))

			mockQuestionnaireService.get.and.resolveTo(questions)

			let emission: QuestionConfig[] = []
			incomingDataService.subscribe(value => {
				emission = value as QuestionConfig[]
			})

			await service['sync']()
			tick(1000)

			emission.map(q => new Question(q)).map(question => submitQuestion.next(question))

			tick(1000)

			const length: number = questions.filter((question) =>
				question.id !== DAILY_NOTES_ID && question.id !== 'rcc-curated-default-01-daily-2'
			).length

			expect(emission.length).toBe(length)
			expect(emission.length).toBe(2)

		}))

	})

	describe('unknownToQuestionConfigs', () => {

		let service: SyncDataService = undefined!
		let mockPublicRuntimeConfigService: jasmine.SpyObj<RccPublicRuntimeConfigService> = undefined!
		let mockQuestionnaireService: jasmine.SpyObj<QuestionnaireService> = undefined!
		let mockSymptomCheckMetaStoreService: jasmine.SpyObj<SymptomCheckMetaStoreService> = undefined!

		beforeEach(fakeAsync(() => {

			mockPublicRuntimeConfigService = jasmine.createSpyObj('RccPublicRuntimeConfigService', ['get'], { ready: Promise.resolve() }) as jasmine.SpyObj<RccPublicRuntimeConfigService>
			mockPublicRuntimeConfigService.get.and.resolveTo([])
			mockQuestionnaireService = jasmine.createSpyObj('QuestionnaireService', ['get'], {
				ready: Promise.resolve(),
				change$: of()
			}) as jasmine.SpyObj<QuestionnaireService>
			mockSymptomCheckMetaStoreService = jasmine.createSpyObj('SymptomCheckMetaStoreService', [], {
				ready: Promise.resolve(),
				items: []
			}) as jasmine.SpyObj<SymptomCheckMetaStoreService>

			TestBed.configureTestingModule({
				imports: [
					IncomingDataServiceModule
				],
				providers: [
					SyncDataService,
					{ provide: RccPublicRuntimeConfigService, useValue: mockPublicRuntimeConfigService },
					{ provide: QuestionnaireService, useValue: mockQuestionnaireService },
					{ provide: SymptomCheckMetaStoreService, useValue: mockSymptomCheckMetaStoreService }
				]
			})
			service = TestBed.inject(SyncDataService)
			tick(1000)

		}))

		it('should throw an error if the input is not an array', async () => {
			mockPublicRuntimeConfigService.get.and.resolveTo({ foo: 'bar' })
			await expectAsync(service['sync']()).toBeRejected()
		})

		it('should throw an error if invalid QuestionConfig data is provided', async () => {
			mockPublicRuntimeConfigService.get.and.resolveTo([{ id: '1', type: 'invalidType' }])

			await expectAsync(service['sync']()).toBeRejected()

		})

	})


})
