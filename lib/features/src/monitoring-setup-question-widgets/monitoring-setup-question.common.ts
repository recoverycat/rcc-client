import { InjectionToken, Provider } from '@angular/core'
import { MonitoringSetupControl } from './monitoring-setup-control.class'

export const MONITORING_SETUP_QUESTION: InjectionToken<MonitoringSetupControl> = new InjectionToken('Monitoring setup question')

export function provideControl(control: MonitoringSetupControl): Provider {
	return {
		provide		: MONITORING_SETUP_QUESTION,
		useValue	: control,
		multi		: true
	}
}
