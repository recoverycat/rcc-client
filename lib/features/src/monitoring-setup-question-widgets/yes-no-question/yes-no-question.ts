import { MonitoringSetupControl } from '../monitoring-setup-control.class'

export const yesNoQuestion: MonitoringSetupControl = {
	label: 'MONITORING_SETUP_QUESTIONS.YES_NO',
	options: [
		{ value: true, translations: { de: 'ja', en: 'yes' } },
		{ value: false, translations: { de: 'nein', en: 'no' } },
	],
	answerType: 'boolean',
	questionMatch: (question) => question.type === 'boolean'
}
