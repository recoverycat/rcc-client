import { NgModule } from '@angular/core'
import { provideTranslationMap, WidgetsModule } from '@rcc/common'
import { quantityQuestion } from './quantity-question/quantity-question'
import { fourChoiceQuestion } from './four-choice-question/four-choice-question'
import { yesNoQuestion } from './yes-no-question/yes-no-question'
import { sliderQuestion } from './slider-question/slider-question'
import { numericalQuestion } from './numerical-question/numerical-question'
import { MonitoringSetupControl } from './monitoring-setup-control.class'
import { provideControl } from './monitoring-setup-question.common'

import de from './i18n/de.json'
import en from './i18n/en.json'

const questions: MonitoringSetupControl[] = [
	fourChoiceQuestion,
	quantityQuestion,
	yesNoQuestion,
	sliderQuestion,
	numericalQuestion,
]

@NgModule({
	imports: [
		WidgetsModule,
	],
	providers: [
		provideTranslationMap('MONITORING_SETUP_QUESTIONS', { de, en }),
		...questions.map(provideControl)
	]
})
export class RccMonitoringSetupQuestionWidgetsModule {}
