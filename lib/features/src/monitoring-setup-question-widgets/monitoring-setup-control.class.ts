import { Type } from '@angular/core'
import { AnswerType, Question, QuestionOptionConfig } from '@rcc/core'

export interface MonitoringSetupControl {
	label: string
	options?: QuestionOptionConfig[]
	questionMatch: (question: Question) => boolean
	answerType: AnswerType
	component?: Type<unknown>
	min?: number
	max?: number
	tags?: string[]
	showWidget?: boolean
	unitInput?: boolean
}
