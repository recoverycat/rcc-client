import	{ NgModule 						} 	from '@angular/core'
import	{
			provideSettingsEntry,
			provideTranslationMap
		} 									from '@rcc/common'


@NgModule({
	providers:[
		provideTranslationMap('SETTINGS_ENTRY_GROUPS.LANGUAGE', {
			en: {
				LABEL: 			'Language',
				DESCRIPTION:	'Translate interface and questions'
			},

			de: {
				LABEL: 			'Sprache',
				DESCRIPTION:	'Übersetzungen für Bedienelemente und Fragen'
			}
		}),

		provideSettingsEntry({
			id: 			'language_group',
			type:			'group',
			label:			'SETTINGS_ENTRY_GROUPS.LANGUAGE.LABEL',
			description:	'SETTINGS_ENTRY_GROUPS.LANGUAGE.DESCRIPTION',
			subSettingIds:	['activeLanguage'],
			icon:			'settings',
			position:		1

		})
	]
})
export class RccLanguageSettingsEntryGroupModule {}
