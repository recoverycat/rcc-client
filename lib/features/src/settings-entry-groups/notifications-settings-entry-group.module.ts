import { NgModule } from '@angular/core'
import { NOTIFICATIONS_ENABLED_SETTING_ID } from './settings-entry.common.module'
import { provideTranslationMap } from '@rcc/common/translations/ng/translations.commons'
import { provideSettingsEntry } from '@rcc/common/settings/settings.commons'

declare module '@rcc/common/settings/settings.service' {
	interface SettingValueTypeMap {
		'notifications-enabled': boolean
	}
}

@NgModule({
	providers: [
		provideTranslationMap('SETTINGS_ENTRY_GROUPS.NOTIFICATIONS', {
			en: {
				GROUP: {
					LABEL: 'Notifications',
				},
				ENTRY: {
					LABEL: 'Enable Notifications',
					DESCRIPTION: '{{APP_NAME}} can notify you when your attention is required.'
				}
			},

			de: {
				GROUP: {
					LABEL: 'Benachrichtigungen',
				},
				ENTRY: {
					LABEL: 'Benachrichtigungen aktivieren',
					DESCRIPTION: '{{APP_NAME}} kann Dich benachrichtigen, wenn Deine Aufmerksamkeit benötigt wird.'
				}
			}
		}),

		provideSettingsEntry({
			id: 			'notification_group',
			type:			'group',
			label:			'SETTINGS_ENTRY_GROUPS.NOTIFICATIONS.GROUP.LABEL',
			description:	'',
			icon:			'notification',
			subSettingIds:	[NOTIFICATIONS_ENABLED_SETTING_ID]
		}),

		provideSettingsEntry({
			id:				NOTIFICATIONS_ENABLED_SETTING_ID,
			label:			'SETTINGS_ENTRY_GROUPS.NOTIFICATIONS.ENTRY.LABEL',
			description:	'SETTINGS_ENTRY_GROUPS.NOTIFICATIONS.ENTRY.DESCRIPTION',
			defaultValue:	true,
			type:			'boolean' as const,
			icon:			'notification'
		})
	]
})
export class RccNotificationSettingsEntryGroupModule {}
