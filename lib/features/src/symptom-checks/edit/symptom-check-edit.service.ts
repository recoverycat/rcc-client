import	{	Injectable					}	from '@angular/core'
import	{
			SymptomCheck,
			Question
		}									from '@rcc/core'
import	{
			RccModalController,
			ItemEditService,
			ItemSelectService,
		}									from '@rcc/common'
import	{
			SymptomCheckMetaStoreService
		}									from '../meta-store'
import	{	SymptomCheckEditComponent	}	from './symptom-check-edit.component'


@Injectable()
export class SymptomCheckEditService extends ItemEditService<SymptomCheck, Question[]> {


	public constructor(
		protected rccModalController			: RccModalController,
		protected itemSelectService				: ItemSelectService,
		protected symptomCheckMetaStoreService	: SymptomCheckMetaStoreService
	){
		super(
			SymptomCheck,
			SymptomCheckEditComponent,
			rccModalController,
		)
	}
}
