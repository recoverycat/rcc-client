import 	{ 	Injectable } 		from '@angular/core'
import	{
			SymptomCheck,
			SymptomCheckConfig,
			SymptomCheckStore
		}						from '@rcc/core'
import	{
			RccStorage,
		}						from '@rcc/common'


@Injectable()
export class CustomSymptomCheckStoreService extends SymptomCheckStore {

	public readonly name: string = 'CUSTOM_SYMPTOM_CHECK_STORE.NAME'

	public constructor(
		rccStorage			: RccStorage,
	){
		super(
			rccStorage.createItemStorage('rcc-custom-symptom-checks'),
		)

		this.update$.subscribe( () => void this.storeAll() )

	}


	public async addSymptomCheckConfig(config: SymptomCheckConfig): Promise<SymptomCheck> {

		const symptom_check: SymptomCheck = this.addConfig(config)

		return 	this.storeAll()
				.then( () => symptom_check)

	}


	public async deleteSymptomCheck(symptom_check: SymptomCheck): Promise<SymptomCheck> {

		if(!this.removeItem(symptom_check)) throw new Error('CustomSymptomCheckStoreService.delete: Unable to delete symptom check with id: ' + symptom_check.id)

		return 	this.storeAll()
				.then( () => symptom_check)
	}
}
