import 	{
			Component,
			OnInit
		}							from '@angular/core'

import	{
			mergeMap
		}							from 'rxjs/operators'

import	{
			ActivatedRoute,
			ParamMap
		}							from '@angular/router'

import	{
			SymptomCheckMetaStoreService
		}							from '../../meta-store'

import	{
			SymptomCheck
		}							from '@rcc/core'



@Component({
    selector: 'rcc-symptom-check-view',
    templateUrl: './view-page.component.html',
    styleUrls: ['./view-page.component.scss'],
    standalone: false
})
export class SymptomCheckViewPageComponent  implements OnInit {


	public symptom_check: SymptomCheck

	protected noLabel: string = 'SYMPTOM_CHECKS_VIEW.DETAILS.NO_LABEL'


	public constructor(
		private activatedRoute				:	ActivatedRoute,
		private symptomCheckMetaStoreService: 	SymptomCheckMetaStoreService
	){}


	public ngOnInit(): void {

		this.activatedRoute.paramMap
		.pipe(
			mergeMap( 	(params	: ParamMap) 	=> this.symptomCheckMetaStoreService.get(params.get('id')) )
		)
		.subscribe(		(sc		: SymptomCheck)	=> this.symptom_check = sc )



		// will unsubscribe automatically as part of route
	}

}
