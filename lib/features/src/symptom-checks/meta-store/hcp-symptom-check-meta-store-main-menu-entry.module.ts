import	{
			NgModule,
			Provider,
			ModuleWithProviders,
		}												from '@angular/core'
import	{
			provideMainMenuEntry,
			Factory,
			Action,
			WithPosition,
			getEffectivePosition,
		}												from '@rcc/common'
import	{
			SymptomCheckMetaStoreServiceHCPHomePath
		}												from './symptom-check-meta-store.commons'
import	{
			SymptomCheckMetaStoreServiceModule,
		}												from './symptom-check-meta-store.module'
import	{
			RouterModule,
			Routes
		}												from '@angular/router'
import	{	HCPSymptomCheckMetaStorePageComponent	}	from './overview-page-hcp/overview-page.component'


const routes: Routes		=	[
									{ path: SymptomCheckMetaStoreServiceHCPHomePath,	component: HCPSymptomCheckMetaStorePageComponent },
								]

@NgModule({
	imports: [
		SymptomCheckMetaStoreServiceModule,
		RouterModule.forChild(routes),
	],

})

export class HCPSymptomCheckMetaStoreServiceMainMenuEntryModule {

	/**
	* This method can add entries to the main menu.
	*
	* Calling it without parameter, will add entries to the main menu
	* automatically at reasonably adequate positions.
	*
	* If you want to determine the positions yourself provide a `config` parameter :
	*
	* | .position  | value                  | effect                                                    |
	* |------------|------------------------|-----------------------------------------------------------|
	* | .position  | true                   | adds menu entry at a reasonably adequate position         |
	* |            | positive number        | adds menu entry at position counting from the top         |
	* |            | negative number        | adds menu entry at position counting from the bottom      |
	*
	* Example:     `HCPSymptomCheckMetaStoreServiceMainMenuEntryModule.addEntry({ position: 1 })`,
	*/

	public static addEntry(config?: WithPosition): ModuleWithProviders<HCPSymptomCheckMetaStoreServiceMainMenuEntryModule> {

		const mainMenuEntry	: Factory<Action> =	{
			deps:		[],
			factory:	() => ({
				label: 			'SYMPTOM_CHECK_META_STORE.MENU_ENTRY.HCP',
				icon: 			'sheet',
				path:			SymptomCheckMetaStoreServiceHCPHomePath,
				position:		getEffectivePosition(config, .5),

			})
		}

		const providers : Provider[] = []

		providers.push(provideMainMenuEntry(mainMenuEntry))

		return {
			ngModule:	HCPSymptomCheckMetaStoreServiceMainMenuEntryModule,
			providers
		}
	}

}
