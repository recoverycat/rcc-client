import 	{
			Component, Input,
		} 								from '@angular/core'

import	{	FormControl 			}	from '@angular/forms'

import	{
			SymptomCheckMetaStoreService
		}								from '../symptom-check-meta-store.service'
import { RccTitleService } from '@rcc/common'


@Component({
    selector: 'rcc-meta-store-page',
    template: '',
    standalone: false
})
export class SymptomCheckMetaStorePageBaseComponent {

	@Input()
	public heading			: string

	public filterControl 	: FormControl	= new FormControl()

	public constructor(
		protected symptomCheckMetaStoreService: SymptomCheckMetaStoreService,
		protected rccTitleService: RccTitleService
	) { }

}
