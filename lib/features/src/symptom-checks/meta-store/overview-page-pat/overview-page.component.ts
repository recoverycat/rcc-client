import 	{
			Component,
			OnInit,
		} 												from '@angular/core'
import	{	SymptomCheckMetaStorePageBaseComponent	}	from '../overview-page-base/overview-page.component'


@Component({
    selector: 'rcc-meta-store-page',
    templateUrl: '../overview-page-base/overview-page.component.html',
    styleUrls: ['../overview-page-base/overview-page.component.scss'],
    standalone: false
})
export class PatSymptomCheckMetaStorePageComponent extends SymptomCheckMetaStorePageBaseComponent implements OnInit {

	public heading: string = 'SYMPTOM_CHECK_META_STORE.MAIN_HEADER.PAT'

	public ngOnInit(): void {
		this.rccTitleService.setTitleSuffix('SYMPTOM_CHECK_META_STORE.MAIN_HEADER.PAT')
	}

}
