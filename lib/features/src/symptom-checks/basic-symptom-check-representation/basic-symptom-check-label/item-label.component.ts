import	{	Component 				}	from '@angular/core'
import	{	SymptomCheck			}	from '@rcc/core'
import	{	ExposeTemplateComponent	}	from '@rcc/common'

@Component({
    selector: 'rcc-item-label',
    templateUrl: './item-label.component.html',
    styleUrls: ['./item-label.component.scss'],
    standalone: false
})
export class SymptomCheckLabelComponent extends ExposeTemplateComponent {
	public constructor(public symptomCheck: SymptomCheck){ super() }

	private _dateFormat: Intl.DateTimeFormatOptions = {
		day: '2-digit',
		month: '2-digit',
		year: 'numeric',
	}
	protected dateFormat: Record<string, string> = this._dateFormat as Record<string, string>

	protected get questionCount(): number {
		return this.symptomCheck.questionIds.length
	}
}
