import	{
			NgModule
		}								from '@angular/core'
import	{
			DynamicLoggingData,
			RccUsageLoggingJob,
		}								from '../usage-logging/usage-logging.commons'
import 	{
			BehaviorSubject,
		}								from 'rxjs'
import 	{
			Factory,
			provideTranslationMap,
		}								from '@rcc/common'
import	{
			provideRccUsageLoggingJob
		}								from '../usage-logging/usage-logging.commons'

import en from './i18n/en.json'
import de from './i18n/de.json'

const tickSubject: BehaviorSubject<DynamicLoggingData> = new BehaviorSubject<DynamicLoggingData>({})

const loggingJob : Factory<RccUsageLoggingJob> = {
	deps	: [],
	factory	: () => ({
		id					: 'user-type',
		tick$				: tickSubject.asObservable(),
		description			: 'USAGE_LOGGING_USERTYPE.DESCRIPTION',
		label				: 'USAGE_LOGGING_USERTYPE.LABEL',
		fixedLoggingData: {
			key				: 'siteLoad',
			category		: 'usageLoggingUsertype'
		},
		requiresUserConsent	: false,
		userId				: 'none'
	}),
}

@NgModule({
	providers: [
		provideTranslationMap('USAGE_LOGGING_USERTYPE', { en, de }),
		provideRccUsageLoggingJob(loggingJob),
	]

})
export class RccUsageLoggingUsertypeModule {

}
