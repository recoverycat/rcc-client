import	{
			NgModule
		}											from '@angular/core'

import	{
			MedicationQuestionEditWidgetModule
		}											from './medication-question-edit-widget'


@NgModule({
	imports: [
		MedicationQuestionEditWidgetModule
	]
})
export class MedicationModule{}

