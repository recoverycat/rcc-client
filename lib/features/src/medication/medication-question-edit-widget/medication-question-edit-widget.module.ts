import	{
			NgModule
		}											from '@angular/core'
import	{
			provideWidget,
			provideTranslationMap,
			SharedModule,

		}											from '@rcc/common'

import	{
			MedicationQuestionEditWidgetComponent,
		}											from './medication-question-edit-widget.component'
import 	{ 	DrugListService 					} 	from './drug-list.service'

import en from './i18n/en.json'
import de from './i18n/de.json'

@NgModule({
	imports: [
		SharedModule,
	],
	providers: [
		provideWidget(MedicationQuestionEditWidgetComponent),
		provideTranslationMap('MEDICATION.QUESTION_EDIT_WIDGET',{ en,de }),
		DrugListService
	],
	declarations:[
		MedicationQuestionEditWidgetComponent,
	]
})
export class MedicationQuestionEditWidgetModule{}
