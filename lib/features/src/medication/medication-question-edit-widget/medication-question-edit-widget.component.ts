import 	{
			Component,
			OnDestroy
		}										from '@angular/core'

import	{
			FormControl,
			FormArray,
			ValidationErrors,
			AbstractControl
		}										from '@angular/forms'

import	{
			merge,
			takeUntil,
			startWith,
			debounceTime,
			Subject
		}										from 'rxjs'

import	{
			RccTranslationService,
			WidgetComponent,
			RccModalController
		}										from '@rcc/common'


import	{
			AnswerType,
			QuestionConfig,
			QuestionOptionConfig,
			TranslationList,
			uuidv4
		}										from '@rcc/core'

import	{
			QuestionEditControl,
		}										from '../../questions/question-edit-widgets'
import 	{ 	DrugListService 				} 	from './drug-list.service'

@Component({
    templateUrl: './medication-question-edit-widget.component.html',
    standalone: false
})
export class MedicationQuestionEditWidgetComponent extends WidgetComponent<QuestionEditControl> implements OnDestroy {

	public static label: string	= 'MEDICATION.QUESTION_EDIT_WIDGET.LABEL'

	public static controlType: typeof QuestionEditControl	= QuestionEditControl

	public static widgetMatch(questionEditControl: QuestionEditControl): number {

		const config: QuestionConfig	= questionEditControl.questionConfig

		// New question, can still be handled:
		if(!config)				return 0

		const hasMedicationTag: boolean	= config.tags?.includes('rcc-medication')

		// Can still handle the question:
		if(!hasMedicationTag)	return 0

		const hasFourOptions: boolean	= config.options?.length === 4

		// Can still handle the question:
		if(!hasFourOptions)		return 0

		const typeMatch: boolean 		= config.type === 'integer'

		// Can still handle the question:
		if(!typeMatch)			return 0

		const values: unknown[]		= config.options.map( option => option.value )
		const valuesMatch: boolean		= values.sort().every( (value, index) => value === index)

		// Can still handle the question:
		if(!valuesMatch)		return 0

		// Made to handle questions like this:
		return 2
	}


	// INSTANCE"

	public wordingControl			: FormControl<string>	=	new FormControl('', this.validateWording())
	public useGenericWordingControl	: FormControl<boolean>	=	new FormControl(true)
	public drugControl				: FormControl<string>	= 	new FormControl('', this.validateDrug())
	public dosageControl			: FormControl<string>	= 	new FormControl('')


	public allForms	: FormArray		= 	new FormArray([
											this.wordingControl,
											this.useGenericWordingControl,
											this.drugControl,
											this.dosageControl
										])

	public drugList		: Array<string>	=	[]
	protected destroy$	: Subject<void>	=	new Subject()

	public constructor(
		protected questionEditControl	: QuestionEditControl,
		protected rccTranslationService	: RccTranslationService,
		protected rccModalController	: RccModalController,
		protected drugListService		: DrugListService
	){

		super(questionEditControl)

		this.drugList = drugListService.getListOfDrugNames()

		questionEditControl.editForms = this.allForms
		this.updateInputsFromConfig()

		this.useGenericWordingControl.valueChanges
		.pipe(
			takeUntil(this.destroy$),
			startWith(this.useGenericWordingControl.value)
		)
		.subscribe( useGenericWording => {

			if (useGenericWording)	this.wordingControl.disable()
			else					this.wordingControl.enable()

			if (useGenericWording)	this.drugControl.enable()
			else					this.drugControl.disable()

			if (useGenericWording)	this.dosageControl.enable()
			else					this.dosageControl.disable()

			if (useGenericWording)	this.drugControl.markAsTouched()

		})

		merge(
			this.drugControl.valueChanges,
			this.dosageControl.valueChanges
		).pipe(
			takeUntil(this.destroy$),
			debounceTime(300)
		)
		.subscribe( () => this.updateWording() )


		// Update question:
		this.allForms.valueChanges
		.pipe(
			takeUntil(this.destroy$),
			startWith(null),
			debounceTime(300)
		)
		.subscribe( ( ) => this.updateConfigFromInputs() )

	}


	public updateWording() : void {
		const drug			: string	= this.drugControl.value
		const dosage		: string	= this.dosageControl.value
		const medication	: string	= drug + (dosage ? `, ${dosage}`: '')

		const wording 		: string	= this.rccTranslationService.translate('MEDICATION.QUESTION_EDIT_WIDGET.GENERIC_WORDING', { medication })

		this.wordingControl.setValue(wording)
	}


	public validateDrug(): (ac: AbstractControl<unknown>) => ValidationErrors| null {

		return (ac: AbstractControl<unknown>) : ValidationErrors| null => {

			const useGenericWording: boolean = this.useGenericWordingControl.value

			if(!useGenericWording) 	return null

			const value	: unknown	= ac.value

			if(!value) return { 'MEDICATION.QUESTION_EDIT_WIDGET.ERRORS.MISSING_DRUG': true }
		}

	}
	public validateWording(): (ac: AbstractControl<unknown>) => ValidationErrors| null {

		return (ac: AbstractControl<unknown>) : ValidationErrors| null => {

			const useGenericWording: boolean = this.useGenericWordingControl?.value

			if(useGenericWording) 	return null

			const value: unknown	= ac.value

			if(!value) return { 'MEDICATION.QUESTION_EDIT_WIDGET.ERRORS.MISSING_WORDING': true }
		}

	}

	public updateInputsFromConfig() : void {

		const editConfig		: QuestionConfig	= 	this.questionEditControl.questionConfig

		const activeLanguage	: string			= 	this.rccTranslationService.activeLanguage

		const wording			: string			= 	editConfig.translations[activeLanguage]
															|| editConfig.meaning

		const wordingPattern	: string			=	this.rccTranslationService.translate('MEDICATION.QUESTION_EDIT_WIDGET.GENERIC_WORDING', { medication: '###' })
															.replace(/[^a-z#]/gi, '.')
															.replace(/###/g, '(.+)')

		const medicationRegex	: RegExp			=	new RegExp(wordingPattern)

		const medicationMatch	: RegExpExecArray	=	medicationRegex.exec(wording)

		const medicationGuess	: string	=	medicationMatch && medicationMatch[1]
		const drugGuess			: string	=	medicationGuess && medicationGuess.split(', ')[0]
		const dosageGuess		: string	=	medicationGuess && medicationGuess.split(', ')[1]

		const useGenericWording	: boolean	=	!wording || !!medicationGuess

		this.useGenericWordingControl.setValue(useGenericWording)
		this.drugControl.setValue(drugGuess || '')
		this.dosageControl.setValue(dosageGuess || '')

		this.wordingControl.setValue(wording)

	}

	public getConfigFromInputs() : QuestionConfig {

		const editConfig: QuestionConfig	=	this.questionEditControl.questionConfig

		const id	: string			=	editConfig?.id || uuidv4()
		const type	: AnswerType		=	'integer'

		const activeLanguage: string	= 	this.rccTranslationService.activeLanguage

		const meaning		: string	=	this.wordingControl.value

		const translations	: TranslationList		=	{ [activeLanguage] : meaning }

		const options	: QuestionOptionConfig[]	=	[2,0,1,3].map( value => {
										const valueMeaning 	: string =
											this.rccTranslationService.translate(`MEDICATION.QUESTION_EDIT_WIDGET.OPTIONS_${value}`)
										const valueTranslations : TranslationList =	{ [activeLanguage] : valueMeaning }

										return { value, meaning: valueMeaning, translations: valueTranslations }
									})

		const tags		: string[]			=	Array.from( new Set([...editConfig.tags, 'rcc-medication']) )

		const config 	: QuestionConfig	= 	{ id, type, meaning, translations, tags, options }

		return config

	}

	public updateConfigFromInputs() : void {

		const config: QuestionConfig = this.getConfigFromInputs()

		this.questionEditControl.questionConfig = config

	}

	public ngOnDestroy() : void {

		this.destroy$.next()
		this.destroy$.complete()

	}

}
