import	{
			Component,
			inject
		} 											from '@angular/core'
import	{
			Question,
			QuestionOptionConfig,
			hasOptions,
		}											from '@rcc/core'
import	{
			DataViewWidgetComponent,
			DataViewControl,
			Dataset,
			ChartStrategy,
		} 											from '../../data-visualization'
import	{	quantitiesChart						}	from '@rcc/features/data-visualization/chart-types/quantities-chart'
import	{	ChartRendererComponentService		}	from '../chart-renderer.component.service'
import	{	ChartWidgetStatic					}	from '../complex-data-view-widgets.commons'


/**
 * This DatiaView widget visualizes all questions that take single choice answers.
 * As second data set it can handle any question without options, that takes
 * string answers. This is intended to accommodate the daily notes question, but
 * the widget can handle any such question. A second data set is not required.
 */
@Component({
    templateUrl: '../complex-data-view-widgets.commons.html',
    styleUrls: ['../complex-data-view-widgets.commons.css'],
    providers: [ChartRendererComponentService],
    standalone: false
})
export class LabeledIntegersDataViewWidgetComponent extends DataViewWidgetComponent implements ChartWidgetStatic<typeof LabeledIntegersDataViewWidgetComponent>  {
	public static chartStrategy: ChartStrategy = quantitiesChart

	// STATIC:

	public static widgetMatch(dataViewControl: DataViewControl): number {

		const primaryDataset 	: Dataset
								= dataViewControl.datasets[0]

		if(!primaryDataset)
			return -1

		const primaryQuestion	: Question
								= primaryDataset.question

		if(primaryQuestion.type !== 'integer')
			return -1
		if(!hasOptions(primaryQuestion.config))
			return -1

		const min: number = primaryQuestion.min
		const max: number = primaryQuestion.max

		if(min && max) {
			if (min > max)
				return -1

			// create an array with all possible integer values
			const values: number[] = Array.from(Array(1+max-min).keys()).map((value: number) => value + min)

			values.forEach((value: number) => {
				if (primaryQuestion.options.find(
					(questionOptionConfig: QuestionOptionConfig) => questionOptionConfig.value === value
				) === undefined)
					return -1
			})
		}

		const secondaryDataset 	: Dataset
								= dataViewControl.datasets[1]

		const secondaryQuestion	: Question
								= secondaryDataset?.question

		if(secondaryDataset)
			if(secondaryQuestion?.type === 'string')
				if(!hasOptions(secondaryQuestion.config))
					return 2
				else
					return 1
			else
				return -1

		return 1
	}

	private chartRendererComponentService: ChartRendererComponentService = inject(ChartRendererComponentService)

	public constructor(
		public 	dataViewControl			: DataViewControl,
	){
		super(dataViewControl)
		this.chartRendererComponentService.renderChart(
			dataViewControl,
			LabeledIntegersDataViewWidgetComponent.chartStrategy,
			(popup) => this.togglePopup(popup),
		)
	}
}

