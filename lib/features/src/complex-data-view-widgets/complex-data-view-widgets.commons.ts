import { ChartStrategy } from '../data-visualization'

export interface PopupStatus {
	xPositionPercentage: number;
	text: string
	date: Date
}

export interface ChartWidget {
	chartStrategy: ChartStrategy
}

export type ChartWidgetStatic<TClass extends ChartWidget & (new(...args: unknown[]) => unknown)>
  = InstanceType<TClass>;
