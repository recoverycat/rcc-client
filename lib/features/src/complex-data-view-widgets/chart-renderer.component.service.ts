import { ElementRef, Injectable, OnDestroy, inject } from '@angular/core'
import { ChartStrategy, DataViewControl } from '../data-visualization'
import BaseChart from '@rcc/figures.cc/rcc-dataviz/src/components/charts/base-chart'
import { ConsumableDataBase } from '@rcc/figures.cc/index'
import { Subject, skip, takeUntil } from 'rxjs'
import { RccTranslationService } from '@rcc/common'
import { PopupStatus } from './complex-data-view-widgets.commons'

type DataTarget = SVGElement & { __data__: { date: Date, note: string } }

@Injectable()
export class ChartRendererComponentService implements OnDestroy {
	private rccTranslationService: RccTranslationService = inject(RccTranslationService)

	private destroy$: Subject<void> = new Subject()

	private elementRef			: ElementRef<HTMLElement>	= inject<ElementRef<HTMLElement>>(ElementRef)

	public renderChart(dataViewControl: DataViewControl, chartStrategy: ChartStrategy, handlePopup: (popup: PopupStatus) => void): void {
		const chart: BaseChart<ConsumableDataBase> = chartStrategy.createChart(
			dataViewControl,
			{
				language: this.rccTranslationService.activeLanguage,
				onTick: (e) => {
					handlePopup(this.handleTick(e))
				},
				style: {
					main: 'var(--main)',
					secondary: 'var(--supp-highlight-secondary)',
				},
			}
		)

		this.rccTranslationService.activeLanguageChange$
			.pipe(
				skip(1),
				takeUntil(this.destroy$)
			)
			.subscribe( (lang : string) => chart.updateLanguage(lang) )


		const chartSvg: SVGSVGElement	=	chart.svg.node()

		chartSvg.style.width 			= 	'100%'

		this.elementRef.nativeElement.append(chartSvg)

		this.destroy$.subscribe(
			() => chartSvg.remove()
		)

		chart.render()
	}

	private handleTick(event: Event): PopupStatus {
		const target: DataTarget = event.target as DataTarget

		const noteText: string = target.__data__.note
		const circle: SVGElement = target.closest('g')
		const circleArray: SVGElement = target.closest('g.x-axis')
		const svgParent: SVGElement = target.closest('svg')

		const circleOffset: number = this.getXTranslateValue(circle.getAttribute('transform'))
		const arrayOffset: number = this.getXTranslateValue(circleArray.getAttribute('transform'))
		const totalWidth: number = parseFloat(svgParent.getAttribute('width'))

		const xPositionPercentage: number = (circleOffset + arrayOffset) * 100 / totalWidth

		return {
			xPositionPercentage,
			text: noteText,
			date: target.__data__.date
		}
	}

	private getXTranslateValue(transformAttributeString: string): number {
		if (!transformAttributeString) return 0

		const pattern: RegExp = /translate\(([0-9.]+), .+\)/
		const patternMatch: RegExpMatchArray = transformAttributeString.match(pattern)

		if (patternMatch == null) return 0

		return parseFloat(patternMatch[1])
	}

	public ngOnDestroy(): void {
		this.destroy$.next()
		this.destroy$.complete()
	}
}
