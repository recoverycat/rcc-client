import { Component, inject } from '@angular/core'
import { ChartStrategy, DataViewControl, DataViewWidgetComponent, Dataset } from '../../data-visualization'
import { Question, hasOptions } from '@rcc/core'
import { sliderChart } from '@rcc/features/data-visualization/chart-types/slider-chart'
import { ChartRendererComponentService } from '../chart-renderer.component.service'
import { ChartWidgetStatic } from '../complex-data-view-widgets.commons'

@Component({
    templateUrl: '../complex-data-view-widgets.commons.html',
    styleUrls: ['../complex-data-view-widgets.commons.css'],
    providers: [ChartRendererComponentService],
    standalone: false
})
export class RawNumbersDataViewWidgetComponent extends DataViewWidgetComponent implements ChartWidgetStatic<typeof RawNumbersDataViewWidgetComponent> {
	public static chartStrategy: ChartStrategy = sliderChart

	public static widgetMatch(dataViewControl: DataViewControl): number {
		const primaryDataset 	: Dataset
								= dataViewControl.datasets[0]

		if(!primaryDataset) return -1

		const primaryQuestion	: Question
								= primaryDataset.question

		const correctQuestionType: boolean =
			primaryQuestion.type === 'integer' || primaryQuestion.type === 'decimal'
		if (!correctQuestionType) return -1

		if (hasOptions(primaryQuestion.config)) return -1

		const hasSecondaryDataset: boolean = dataViewControl.datasets[1] != null
		const secondaryDatasetType: string = dataViewControl.datasets[1]?.question.type

		if (!hasSecondaryDataset) return 1
		if (secondaryDatasetType === 'string') return 2

		return -1
	}

	private chartRendererComponentService: ChartRendererComponentService = inject(ChartRendererComponentService)

	public constructor(
		public dataViewControl			: DataViewControl,
	) {
		super(dataViewControl)
		this.chartRendererComponentService.renderChart(
			dataViewControl,
			RawNumbersDataViewWidgetComponent.chartStrategy,
			(popup) => this.togglePopup(popup),
		)
	}
}
