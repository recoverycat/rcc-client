import { Injectable, inject } from '@angular/core'
import { WidgetComponentType, WidgetsService } from '@rcc/common'
import { DataViewControl } from '../data-visualization'
import { ChartWidget } from '../complex-data-view-widgets/complex-data-view-widgets.commons'
import { ConsumableDataBase } from '@rcc/figures.cc/index'
import BaseChart from '@rcc/figures.cc/rcc-dataviz/src/components/charts/base-chart'

export type RendererFunction = (language: string, width: number, height: number) => SVGSVGElement

@Injectable()
export class PDFWidgetComponentAdapterService {
	private widgetsService: WidgetsService = inject(WidgetsService)

	public getRendererFromDataViewControl(dataViewControl: DataViewControl): RendererFunction {
		const matches: WidgetComponentType<DataViewControl>[] = this.widgetsService.getWidgetMatches(dataViewControl)
		const matchWithChart: ChartWidget = matches.find(Match => (Match as unknown as ChartWidget).chartStrategy != null) as unknown as ChartWidget

		if (matchWithChart == null)
			return

		return (language, width, height) => {
			const chart: BaseChart<ConsumableDataBase> =  matchWithChart.chartStrategy.createChart(
				dataViewControl,
				{
					language,
					width,
					height,
					style: {
						main: '#22A58F',
						secondary: '',
					},
					filterTicks: true,
					aspectCompressed: 10
				},
			)
			chart.render()
			return chart.svg.node()
		}
	}
}
