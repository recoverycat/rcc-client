import	{
			Injectable
		}							from '@angular/core'
import	{
			RccTranslationService
		}							from '@rcc/common'
import	{
			CalendarDateString,
			DAILY_NOTES_ID,
			Question,
			Report,
			sub
		}							from '@rcc/core'
import	{
			DataViewControl,
			Dataset,
			DatasetsService,
		}							from '../data-visualization'
import	{
			PDFDocument,
			PDFFont,
			PDFImage,
			PDFPage,
			RGB,
			StandardFonts,
			rgb,
		}							from 'pdf-lib'
import	*	as windows1252 			from 'windows-1252'
import	{
			PDFWidgetComponentAdapterService,
			RendererFunction
		}							from './pdf-widget-component-adapter.service'
import	{
			notesSvgBackground,
			svgPathHcpCat,
			svgPathHcpCross,
			svgPathHcpEarLeft,
			svgPathHcpEarRight,
			svgPathHcpRecovery,
		}							from './svg-full-parts'

export type PDFData = { questions: Question[] }

export interface PageOptions {
	pageMargin: number
	imageWidthPercentage: number
	notesPage?: boolean
}

type LineOptions = {
	lineMargin?: number
	fontSize: number,
	font: PDFFont,
	noteDatapoint?: boolean
	questionDatapoint?: boolean
	horizontalMargin?: number
}

type NotesOptions = {
	pdf: PDFDocument,
	horizontalPageMargin: number,
	headingBottomMargin: number,
	notesBottomMargin: number,
	noteDataset: Dataset,
	dateFont: PDFFont,
	answerFont: PDFFont,
	fontSizeHeading: number,
	fontSizeParagraph: number,
	questionSectionMargin: number
}

type GeneralDatasetsOptions = {
	pdf: PDFDocument,
	datasets: Dataset[],
	legendFont: PDFFont,
	questionFont: PDFFont,
	regularFont: PDFFont
}

type GeneralDatasetChartOptions = {
	writer: LineWriter,
	pdf: PDFDocument,
	dataset: Dataset,
	question: Question,
	horizontalPageMargin: number,
	legendFont: PDFFont,
	questionFont: PDFFont,
	headingBottomMargin: number
}

export interface LineWriter {
	writeLine: (text: string, options: LineOptions) => void
	newLine: (newLineHeight: number) => void
	addImage: (image: PDFImage) => void
	addChart: (image: PDFImage, text: string) => void
	paintOverPageRemnants: () => void
}
/**
 * Figma PDF A4 dimensions: 	1190x1684
 * PDF-Lib A4 dimensions: 96 dpi, 595x842, thus factor .5
 *  */

@Injectable()
export class RccPdfService {

	protected activeLanguage			: string 	= this.translationService.activeLanguage

	protected pageMargin				: number 	= 30
	protected fontSizeH1				: number 	= 8
	protected headingMargin				: number 	= 82
	protected fontSizeNormal			: number 	= 8
	protected fontSizeSmall				: number 	= 6.4
	protected questionMargin			: number 	= 16
	protected answerMargin				: number 	= 20
	protected questionSectionMargin		: number 	= 20
	protected notesQuestionSectionMargin: number	= 40
	protected notesBottomMargin			: number	= 48
	protected dateQuestionMargin		: number	= 4
	protected notesHorizontalMargin		: number	= this.pageMargin + 12

	protected svgWidth					: number 	= 500
	protected svgHeight					: number 	= 150
	protected pngWidth					: number 	= 2500

	protected numberOfDays				: number 	= 60

	public rCViolet						: RGB 		= rgb(0.3137, 0.2902, 0.9412)
	protected rCBackgroundViolet		: RGB 		= rgb(0.658, 0.728, 0.98)

	public rCOrange						: RGB 		= rgb(0.9059, 0.5059, 0.2627)

	public rCGreen						: RGB 		= rgb(0.1333, 0.6471, 0.5608)
	protected rCBackgroundGreen			: RGB 		= rgb(0.92, 0.96, 0.95)

	protected ibmPlexMonoReplacementFont: PDFFont	= undefined
	protected ibmPlexSansReplacementFont: PDFFont	= undefined

	public isANotesPage					: boolean	= undefined

	public constructor(
		private readonly datasetsService: DatasetsService,
		private readonly translationService: RccTranslationService,
		private readonly pdfWidgetComponentAdapterService: PDFWidgetComponentAdapterService
	) {}

	public async createPDFOfReport(report: Report): Promise<string> {
		const pdf				: PDFDocument = await PDFDocument.create()

		const regularSansFont	: PDFFont = pdf.embedStandardFont(StandardFonts.Helvetica)
		const boldSansFont		: PDFFont = pdf.embedStandardFont(StandardFonts.HelveticaBold)
		const regularMonoFont	: PDFFont = pdf.embedStandardFont(StandardFonts.Courier)
		const generalFont		: PDFFont = regularSansFont

		const datasets			: Dataset[] = await this.datasetsService.getDatasets(report)

		const notesDatasets		: Dataset[] = datasets.filter(dataset => dataset.question.id === DAILY_NOTES_ID)
		const generalDatasets	: Dataset[] = datasets.filter(dataset => dataset.question.id !== DAILY_NOTES_ID)

		await this.createGeneralDatasetsSection({
			pdf:					pdf,
			datasets:				generalDatasets,
			legendFont:				regularMonoFont,
			questionFont:			boldSansFont,
			regularFont:			generalFont
		})

		for (const dataset of notesDatasets)
			this.createNotesSection({
				pdf:					pdf,
				horizontalPageMargin:	this.notesHorizontalMargin,
				headingBottomMargin:	this.dateQuestionMargin,
				notesBottomMargin:		this.notesBottomMargin,
				noteDataset:			dataset,
				dateFont:				boldSansFont,
				answerFont:				regularSansFont,
				fontSizeHeading:		this.fontSizeNormal,
				fontSizeParagraph:		this.fontSizeSmall,
				questionSectionMargin:	this.questionSectionMargin
			})


		return await pdf.save() as unknown as string
	}

	public createNotesSection(options: NotesOptions): void {

		const isAStringQuestion		: boolean =	options.noteDataset.question.type === 'string'
		const hasNoOptions			: boolean =	!options.noteDataset.question.options
												|| options.noteDataset.question.options.length === 0
		const isFreeTextQuestion	: boolean =	isAStringQuestion && hasNoOptions

		// Toggle for header
		if (isFreeTextQuestion) this.isANotesPage = true

		const noNotesData				: boolean = options.noteDataset.datapoints.length === 0

		const notAFreeTextQuestionText	: string
			= this.translationService.translate('PDF_REPORT.XXXX')
		const noNotesDataText			: string
			= this.translationService.translate('PDF_REPORT.YYYY')
		const dailyNoteWriter			: LineWriter
			= this.createWriter(options.pdf, options.answerFont, {
				pageMargin:				options.horizontalPageMargin,
				imageWidthPercentage:	80,
				notesPage:				true
			})

		dailyNoteWriter.newLine(this.headingMargin)

		if (!isFreeTextQuestion)
			dailyNoteWriter.writeLine(notAFreeTextQuestionText, {
				lineMargin:	options.headingBottomMargin,
				fontSize:	options.fontSizeParagraph,
				font:		options.answerFont,
			})

		if (noNotesData)
			dailyNoteWriter.writeLine(noNotesDataText, {
				lineMargin:	options.headingBottomMargin,
				fontSize:	options.fontSizeParagraph,
				font:		options.answerFont
			})

		options.noteDataset.datapoints.forEach(datapoint => {

			const answerText: string = String(datapoint.value)
			const dateText	: string = this.translationService.translate(
				datapoint.date,
				{
					weekday:	'long',
					day:		'2-digit',
					month:		'2-digit',
					year:		'numeric'
				}
			)
			const answerLineText: string = answerText

			dailyNoteWriter.writeLine(dateText, {
				lineMargin: options.headingBottomMargin,
				fontSize: options.fontSizeHeading,
				font: options.dateFont,
				noteDatapoint: true,
				horizontalMargin: options.horizontalPageMargin
			})
			dailyNoteWriter.writeLine(answerLineText, {
				lineMargin: options.notesBottomMargin,
				fontSize: options.fontSizeParagraph,
				font: options.answerFont,
				horizontalMargin: options.horizontalPageMargin
			})
		})
		dailyNoteWriter.paintOverPageRemnants()
		dailyNoteWriter.newLine(options.questionSectionMargin)

	}

	public async createGeneralDatasetsSection(options: GeneralDatasetsOptions): Promise<void> {

		const writer			: LineWriter
			= this.createWriter(options.pdf, options.regularFont, {
			pageMargin: this.pageMargin,
			imageWidthPercentage: 95
		})

		writer.newLine(this.headingMargin)

		for (const dataset of options.datasets)
			await this.createGeneralDatasetChart(
				{
					writer:					writer,
					pdf:					options.pdf,
					dataset:				dataset,
					question:				dataset.question,
					horizontalPageMargin:	this.pageMargin,
					legendFont:				options.legendFont,
					questionFont:			options.questionFont,
					headingBottomMargin:	this.dateQuestionMargin
				}
			)

		writer.newLine(this.dateQuestionMargin)
		writer.paintOverPageRemnants()

	}

	private async createGeneralDatasetChart(options: GeneralDatasetChartOptions): Promise<void> {

		const question		: Question	= options.dataset.question
		const questionText	: string	= this.translationService.translate(question)

		const dataViewControl: DataViewControl = new DataViewControl({
			datasets:	[options.dataset],
			endDate:	CalendarDateString.today(),
			startDate:	CalendarDateString.daysBefore(CalendarDateString.today(), 60),
			scope:		'month',
		})

		const renderer			: RendererFunction
			= this.pdfWidgetComponentAdapterService
			.getRendererFromDataViewControl(dataViewControl)
		const questionHasChart	: boolean = renderer != null

		if (questionHasChart) {

			const svg		: SVGSVGElement = renderer(
				this.translationService.activeLanguage,
				this.svgWidth,
				this.svgHeight
			)
			const pngImage	: string 		= await this.convertSvgToPng(svg, this.pngWidth)
			const image		: PDFImage		= await options.pdf.embedPng(pngImage)

			options.writer.addChart(image, questionText)
		}
	}

	public createLogo(page: PDFPage): void {

		page.moveTo(this.pageMargin, page.getHeight() - this.pageMargin)

		page.drawSvgPath(svgPathHcpCross, { color: this.rCOrange })
		page.drawSvgPath(svgPathHcpEarLeft, { color: this.rCViolet })
		page.drawSvgPath(svgPathHcpEarRight, { color: this.rCViolet })
		page.drawSvgPath(svgPathHcpRecovery, { color: this.rCViolet })
		page.drawSvgPath(svgPathHcpCat, { color: this.rCOrange })

	}
	private createNoteBoxes(page: PDFPage, currentYPosition: number, height: number, width: number): void {
		page.moveTo(this.pageMargin, currentYPosition + 16)

		page.drawSvgPath(notesSvgBackground(height, width), { color: rgb(1,1,1), borderColor: this.rCBackgroundViolet, borderWidth: 2, borderOpacity: .1 })

	}

	private drawChartBox(page: PDFPage, currentYPosition: number, height: number, width: number): void {
		page.moveTo(this.pageMargin, currentYPosition + height)

		page.drawSvgPath(notesSvgBackground(height, width), { color: rgb(1,1,1) })

	}
	private createNotesBackground(page: PDFPage, currentYPosition: number): void {

		if (this.isANotesPage) {

			page.moveTo(this.pageMargin, currentYPosition)

			page.drawRectangle({
				x: this.pageMargin / 2,
				y: this.pageMargin,
				width: page.getWidth() - this.pageMargin,
				height: page.getHeight() - this.pageMargin * 4,
				color: this.rCBackgroundViolet,
			})

		}

	}

	private createContentBackground(page: PDFPage, currentYPosition: number): void {

		if (!this.isANotesPage) {

			page.moveTo(this.pageMargin, currentYPosition)

			page.drawRectangle({
				x: this.pageMargin / 2,
				y: this.pageMargin,
				width: page.getWidth() - this.pageMargin,
				height: page.getHeight() - this.pageMargin * 4,
				color: this.rCBackgroundGreen,
			})

		}
	}

	public createColoredHeaderElement(page: PDFPage, elementColor: RGB, pageHeight: number, pageWidth: number): void {

		pageHeight	= page.getHeight()
		pageWidth	= page.getWidth()

		const buttonWidth		: number = 168
		const buttonHeight		: number = 13
		const circleRadius		: number = buttonHeight / 2
		const rectangleWidth	: number = buttonWidth - circleRadius * 2 // Whole button width - two half circles on each end

		// Left circle

		page.drawCircle({
			x:		pageWidth - this.pageMargin - circleRadius - rectangleWidth,
			y:		pageHeight - 51,
			size:	circleRadius,
			color:	elementColor,
		})

		// Right circle
		page.drawCircle({
			x:		pageWidth - this.pageMargin - circleRadius,
			y:		pageHeight - 51,
			size:	circleRadius,
			color:	elementColor,
		})

		// Rectangle
		page.drawRectangle({
			x:		pageWidth - this.pageMargin - circleRadius - rectangleWidth,
			y:		pageHeight - 57.5,
			width:	buttonWidth - circleRadius * 2,
			height:	buttonHeight,
			color:	elementColor,
		})
	}

	public createHeaderTexts(pdfDoc: PDFDocument, page: PDFPage, pageHeight: number, pageWidth: number, monoFont: PDFFont, sansFont: PDFFont): void {

		const dateToday		: string = new Date()
										.toLocaleDateString(this.activeLanguage)
		const dateStart		: string = sub(new Date(), { days: this.numberOfDays })
										.toLocaleDateString(this.activeLanguage)

		const textAsOf		: string = this.translationService.translate('PDF_MODULE.AS_OF') + ' ' + dateToday
		const textSince		: string = this.translationService.translate('PDF_MODULE.SINCE') + ' ' + dateStart
		const textDateStart	: string = dateStart
		const textSubtitle	: string = this.translationService.translate('PDF_MODULE.REPORT_SUBTITLE')
		const textInterval	: string = this.translationService.translate('PDF_MODULE.REPORT_INTERVAL')
		const textNotes		: string = this.translationService.translate('PDF_MODULE.TYPE.NOTES')
		const textQuestions	: string = this.translationService.translate('PDF_MODULE.TYPE.QUESTIONS')

		const textAsOfTextWidth					: number
												= monoFont
													.widthOfTextAtSize(
														textAsOf,
														this.fontSizeNormal
													)
		const textSinceTextWidth				: number
												= monoFont
													.widthOfTextAtSize(
														textSince,
														this.fontSizeNormal
													)
		const textDateStartTextWidth			: number
												= monoFont
													.widthOfTextAtSize(
														textDateStart,
														this.fontSizeNormal
													)
		const textIntervalTextQuestionsWidth	: number
												= sansFont
													.widthOfTextAtSize(
														textInterval
														+ ' '
														+ textQuestions,
														this.fontSizeNormal
													)
		const textIntervalTextNotesWidth		: number
												= sansFont
													.widthOfTextAtSize(
														textInterval
														+ ' '
														+textNotes,
														this.fontSizeNormal
													)

		// Left-aligned texts

		page.drawText(textSubtitle, {
			font:	monoFont,
			color:	rgb(0, 0, 0),
			x:		this.pageMargin,
			y:		pageHeight - 54,
			size:	this.fontSizeSmall
		})

		// Right-aligned texts

		page.drawText(`${textAsOf}`, {
			font:	monoFont,
			color:	rgb(0, 0, 0),
			x:		pageWidth - this.pageMargin - textAsOfTextWidth,
			y:		pageHeight - this.pageMargin - this.fontSizeNormal,
			size:	this.fontSizeNormal
		})
		page.drawText(`${textSince}`, {
			font:	monoFont,
			color:	rgb(0, 0, 0),
			x:		pageWidth - this.pageMargin - textSinceTextWidth,
			y:		pageHeight - this.pageMargin - this.fontSizeNormal * 5,
			size:	this.fontSizeNormal
		})
		page.drawText(dateStart, {
			font:	monoFont,
			color:	this.rCGreen,
			x:		pageWidth - this.pageMargin - textDateStartTextWidth,
			y:		pageHeight - this.pageMargin - this.fontSizeNormal * 5,
			size:	this.fontSizeNormal,
		})


		if (this.isANotesPage)
			page.drawText(textInterval + ' ' + textNotes, {
				font:	sansFont,
				color:	rgb(1, 1, 1),
				x:		pageWidth - this.pageMargin - 168 / 2 - textIntervalTextNotesWidth / 2,
				y:		pageHeight - 54,
				size:	this.fontSizeNormal
			})

		if (!this.isANotesPage)
			page.drawText(textInterval + ' ' + textQuestions, {
				font:	sansFont,
				color:	rgb(1, 1, 1),
				x:		pageWidth - this.pageMargin - 168 / 2 - textIntervalTextQuestionsWidth / 2,
				y:		pageHeight - 54,
				size:	this.fontSizeNormal
			})

	}

	public createHeader(pdfDoc: PDFDocument, page: PDFPage, monoFont: PDFFont, sansFont: PDFFont): void {

		const pageHeight: number = page.getHeight()
		const pageWidth	: number = page.getWidth()

		// this.createReferenceBox(page, pageHeight, pageWidth)

		this.createLogo(page)

		if (this.isANotesPage)
			this.createColoredHeaderElement(page, this.rCViolet, pageHeight - 60, pageWidth)
		if (!this.isANotesPage)
			this.createColoredHeaderElement(page, this.rCGreen, pageHeight, pageWidth)

		this.createHeaderTexts(pdfDoc, page, pageHeight, pageWidth, monoFont, sansFont)

	}


	/**
	* @param svgImage The original SVG image
	* @param pngWidth The rendered width of the png. Aspect ratio of the SVG will be maintained
	*
	* Converts an SVG image to a PNG raster via the browser's HTML canvas
	*/
	private async convertSvgToPng(svgImage: SVGSVGElement, pngWidth: number): Promise<string> {
		const serializer: XMLSerializer = new XMLSerializer()
		const svgString: string = serializer.serializeToString(svgImage)

		// Note: Don't use base 64 encoding here, as it causes an error
		// when handling umlauts
		const utf8: string = `data:image/svg+xml;charset=utf-8,${encodeURIComponent(svgString)}`
		const img: HTMLImageElement = document.createElement('img')
		img.src = utf8

		// Need to wait for the browser to have loaded the image into an element
		// before we can copy it onto the canvas
		await new Promise((resolve, reject) => {
			img.onload = resolve
			img.onerror = reject
		})

		const canvas: HTMLCanvasElement = document.createElement('canvas')
		const svgWidth: number = svgImage.width.baseVal.value
		const svgHeight: number = svgImage.height.baseVal.value

		// We want to scale up the PNG image, so we get a decent resolution
		// when it's rendered in the PDF.
		// Note this isn't the width we'll be rendering on the page, but of
		// the embedded PNG itself.
		const pngHeight: number = (svgHeight / svgWidth) * pngWidth
		canvas.width = pngWidth
		canvas.height = pngHeight
		canvas.getContext('2d').drawImage(img, 0, 0, pngWidth, pngHeight)
		const dataUrl: string = canvas.toDataURL('image/png', 1)
		return dataUrl
	}

	/**
	 * @returns A function to write a new line to the PDF, this handles the tracking
	 * of the current line position
	 */
	// private async createWriter(pdf: PDFDocument, options: PageOptions): Promise<LineWriter> {
	private createWriter(pdf: PDFDocument, generalFont: PDFFont, options: PageOptions): LineWriter {
		let currentPage: PDFPage = pdf.addPage()

		this.ibmPlexMonoReplacementFont = pdf.embedStandardFont(StandardFonts.Courier)
		this.ibmPlexSansReplacementFont = pdf.embedStandardFont(StandardFonts.Helvetica)

		// Adding first page header
		this.createHeader(pdf, currentPage, this.ibmPlexMonoReplacementFont, this.ibmPlexSansReplacementFont)
		const { width, height } = currentPage.getSize()
		const allowedWidth: number = width - options.pageMargin * 2
		const imagePadding: number = 20

		// The y coordinates of the page go from 0 (bottom of the page), to [height]
		// (top of the page) - so to write top down, we set the y coordinates
		// to the height of the page, - minus the margin
		const initialYPosition: () => number = () => height - options.pageMargin
		let currentYPosition: number = initialYPosition()

		// Add notes background color
		this.createContentBackground(currentPage, currentYPosition)
		this.createNotesBackground(currentPage, currentYPosition)

		const createNewPage: () => void = () => {
			currentPage = pdf.addPage()

			currentYPosition = initialYPosition()
			currentYPosition -= this.headingMargin

			// Adding headers on all following pages
			this.createHeader(pdf, currentPage, this.ibmPlexMonoReplacementFont, this.ibmPlexSansReplacementFont)


			// Add notes background color on all following pages
			this.createNotesBackground(currentPage, currentYPosition)

			this.createContentBackground(currentPage, currentYPosition)

		}
		const writeLine: (lineText: string, fontSize: number, individualFont?: PDFFont, horizontalMargin?: number) => void = (lineText, fontSize, individualFont, horizontalMargin) => {
			// To determine the y position that we actually write at, we
			// subtract the font size of the text we are writing divided
			// by 2, as the y position refers to the vertical center of
			// the text
			const y: () => number = () => currentYPosition - fontSize / 2

			const needToAddNewPage: boolean = currentYPosition - fontSize < options.pageMargin
			if (needToAddNewPage)

				createNewPage()

			/**
			 * If an individual font is passed to LineWriter.writeLine, then this one will be used instead
			 */
			currentPage.drawText(lineText, { x: horizontalMargin ? this.notesHorizontalMargin : options.pageMargin, y: y(), size: fontSize, font: individualFont ? individualFont : generalFont })
			currentYPosition -= fontSize * 1.5

		}

		return {
			writeLine: (text, lineOptions) => {
				const printText: string = this.replaceInvalidCharactersWithQuestionMarks(text, lineOptions.font)
				const lineWidth: number = lineOptions.font.widthOfTextAtSize(printText, lineOptions.fontSize)
				const textTooWideForPage: boolean = lineWidth > allowedWidth
				const needToAddNewPage: boolean = currentYPosition - 60 < options.pageMargin

				if (lineOptions.noteDatapoint === true){

					if (needToAddNewPage) createNewPage()

					this.createNoteBoxes(currentPage, currentYPosition, 52, currentPage.getWidth() - this.pageMargin * 2.47)

				}

				if (textTooWideForPage) {
					const lines: string[] = this.calculateLineBreaks(printText, lineOptions.font, lineOptions.fontSize, allowedWidth)

					lines.forEach((line) => writeLine(line, lineOptions.fontSize, lineOptions.font))
					lines.slice(1).forEach(() => currentYPosition += 9)
				} else
					writeLine(printText, lineOptions.fontSize, lineOptions.font)

				currentYPosition -= (lineOptions.lineMargin || 0)

			},
			newLine: (newLineHeight) => {
				currentYPosition -= newLineHeight
			},
			paintOverPageRemnants: () => {
				currentPage.drawRectangle({
					x: this.pageMargin / 2,
					y: 0,
					width: currentPage.getWidth() - this.pageMargin,
					height: currentYPosition + 6,
					color: rgb(1, 1, 1),
				})
			},
			addImage: (image) => {
				const { height: imageHeight, width: imageWidth } = image
				const renderedWidth: number = (width - (options.pageMargin * 2)) * (options.imageWidthPercentage / 100)
				const renderedHeight: number = (imageHeight / imageWidth) * renderedWidth
				let yCoordinateOfBottomEdgeOfImage: number = currentYPosition - renderedHeight - imagePadding

				const needToAddNewPage: boolean = yCoordinateOfBottomEdgeOfImage <= options.pageMargin

				if (needToAddNewPage)
					createNewPage()

				yCoordinateOfBottomEdgeOfImage = currentYPosition - renderedHeight - imagePadding
				const xCoordinate: number = options.pageMargin + renderedWidth * ((100 - options.imageWidthPercentage) / 100) / 2

				currentPage.drawImage(image, {
					y: currentYPosition - renderedHeight - imagePadding,
					x: xCoordinate,
					width: renderedWidth,
					height: renderedHeight,
				})
				currentYPosition -= (renderedHeight + imagePadding * 2)
			},
			addChart: (image, text) => {
				const { height: imageHeight, width: imageWidth } = image
				const renderedWidth: number = (width - (options.pageMargin * 2)) * (options.imageWidthPercentage / 100)
				const renderedHeight: number = (imageHeight / imageWidth) * renderedWidth
				let yCoordinateOfBottomEdgeOfImage: number = currentYPosition - renderedHeight - imagePadding

				const needToAddNewPage: boolean = yCoordinateOfBottomEdgeOfImage - this.questionMargin <= options.pageMargin

				if (needToAddNewPage)
					createNewPage()

				currentYPosition -= this.questionMargin / 2

				yCoordinateOfBottomEdgeOfImage = currentYPosition - renderedHeight - imagePadding
				const xCoordinate: number = options.pageMargin + renderedWidth * ((100 - options.imageWidthPercentage) / 100) / 2

				const textOffset: number = this.fontSizeH1 * 3

				this.drawChartBox(currentPage, yCoordinateOfBottomEdgeOfImage, renderedHeight + imagePadding / 2 + textOffset, renderedWidth + imagePadding / 2)

				writeLine(text, this.fontSizeH1, generalFont,imagePadding / 2)

				currentPage.drawImage(image, {
					y: currentYPosition - renderedHeight - imagePadding,
					x: xCoordinate,
					width: renderedWidth,
					height: renderedHeight,
				})
				currentYPosition -= (renderedHeight + imagePadding * 2 + this.questionMargin / 2)
			}
		}
	}

	private replaceInvalidCharactersWithQuestionMarks(text: string, font: PDFFont): string {
		// allow only windows1252 chars as the current font only supports those
		// remove windows-1252 dependency once font changes
		const win1252: Uint16Array = windows1252.encode(text, { mode: 'replacement' })
		// replace windows1252 chars not supported by the font with ?
		const filteredText: Uint16Array = win1252.map((char) => font.getCharacterSet().includes(char) ? char : 63)
		return windows1252.decode(filteredText)
	}

	/**
	* Uses bisection search to determine the maximum length of words that can fit on a line
	*
	* @returns a list of lines, each of which is short enough to fit on the allowed width of the page
	*/
	private calculateLineBreaks(text: string, font: PDFFont, fontSize: number, allowedWidth: number): string[] {
		const words: string[] = text.split(' ')
		const wordLength: number = words.length

		/** Used to track the start of the current line - starting with the first word */
		let start: number = 0
		/**
		 * Used to track the current lowest current guess of the bisection search, will
		 * be updated when the current guess is too short
		 */
		let lowest: number = start
		/**
		 * Used to track the current highest guess of the bisection search, will be updated
		 * when the current guess is too long
		 */
		let end: number = wordLength

		/** Used to track the middle point between the lower and upper markers of the bisection search */
		let middle: number = 0
		const determineMiddle: () => number = () => Math.floor((end - lowest) / 2) + lowest
		middle = determineMiddle()

		let finished: boolean = false
		const lines: string[] = []

		while (!finished) {
			const line: string = words.slice(start, middle).join(' ')
			const width: number = font.widthOfTextAtSize(line, fontSize)

			if (width > allowedWidth) {
				end = middle
				middle = determineMiddle()
			}

			if (width <= allowedWidth) {
				lowest = middle
				middle = determineMiddle()
			}

			if (middle === lowest) {
				lines.push(words.slice(start, lowest).join(' '))

				start = lowest
				lowest = start
				end = wordLength
				middle = determineMiddle()
				const remainingLine: string = words.slice(start, end).join(' ')

				if (font.widthOfTextAtSize(words.slice(start, end).join(' '), fontSize) < allowedWidth) {
					lines.push(remainingLine)
					finished = true
				}
			}

		}
		return lines
	}
}
