import { PDFPage, rgb } from 'pdf-lib'

/**
 * For development purposes only. Put this in the createWriter() function and within the createNewPage()
 * @param page
 * @param pageHeight
 * @param pageWidth
 */
export function createReferenceBox(page: PDFPage, pageHeight: number, pageWidth: number, pageMargin: number): void {

	const referenceBoxWidth		: number = (pageWidth - pageMargin - pageMargin)
	const referenceBoxHeight	: number = (pageHeight - pageMargin - pageMargin)
	const referenceBoxX			: number = pageMargin
	const referenceBoxY			: number = pageMargin

	page.drawRectangle({
		x:				referenceBoxX,
		y:				referenceBoxY,
		width:			referenceBoxWidth,
		height:			referenceBoxHeight,
		borderColor:	rgb(1, 0, 0),
	})

}
