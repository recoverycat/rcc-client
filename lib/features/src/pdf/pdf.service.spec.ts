import { RccPdfService } from './pdf.service'
import { PDFDocument, PDFFont, PDFPage, RGB, StandardFonts, rgb } from 'pdf-lib'
import { Question, sub } from '@rcc/core'
import { Datapoint, Dataset, DatasetsService } from '../data-visualization'
import { RccTranslationService } from '@rcc/common'
import { PDFWidgetComponentAdapterService } from './pdf-widget-component-adapter.service'
import {
	svgPathHcpCat,
	svgPathHcpCross,
	svgPathHcpEarLeft,
	svgPathHcpEarRight,
	svgPathHcpRecovery
} from '@rcc/features/pdf/svg-full-parts'

describe('RccPdfService', () => {

	let service				: RccPdfService = undefined

	const rCViolet			: RGB 			= rgb(0.3137, 0.2902, 0.9412)
	const rCOrange			: RGB 			= rgb(0.9059, 0.5059, 0.2627)
	const rCGreen			: RGB 			= rgb(0.1333, 0.6471, 0.5608)

	const pageMargin		: number		= 30

	beforeEach(() => {

		const dataSetsService	: jasmine.SpyObj<DatasetsService>
								= jasmine.createSpyObj<DatasetsService>('DataSetsService', ['getDatasets'])

		const translationService	: jasmine.SpyObj<RccTranslationService>
									= jasmine.createSpyObj<RccTranslationService>('RccTranslationService', ['translate'] )
		const pdfWidgetComponentAdapterService	: jasmine.SpyObj<PDFWidgetComponentAdapterService>
												= jasmine.createSpyObj<PDFWidgetComponentAdapterService>('PDFWidgetComponentAdapterService', ['getRendererFromDataViewControl'])

		translationService.translate.and.returnValue('blank')

		service = new RccPdfService(
			dataSetsService,
			translationService,
			pdfWidgetComponentAdapterService
		)

	})

	describe('.createNotesSection()', () => {

		const validAndShortDataset : Dataset	= {
			question: new Question({ id: 'question', type: 'string', translations: { de: 'bla', en: 'bla' }, }),
			datapoints: [{ value: 'bla', date : '2024-01-01', note: 'blalalaasldalsd' }]
		}

		const validAndLongDataset : Dataset	= {
			question: new Question({ id: 'question', type: 'string', translations: { de: 'bla', en: 'bla' }, }),
			datapoints: new Array<Datapoint>(100).fill({ value: 'bla', date : '2024-01-01', note: 'blalalaasldalsdblalalaasldalsdblalal' })
		}

		const invalidDataset : Dataset	= {
			question: new Question({ id: 'question', type: 'boolean', translations: { de: 'bla', en: 'bla' }, }),
			datapoints: [{ value: 'bla', date : '2024-01-01', note: 'blalalaasldalsd' }]
		}

		const emptyDataset : Dataset	= {
			question: new Question({ id: 'question', type: 'string', translations: { de: 'bla', en: 'bla' }, }),
			datapoints: []
		}

		it('should add an extra page to an existing PDF when called with a dataset', async() => {

			const pdfDoc : PDFDocument = await PDFDocument.create()

			const regularSansFont	: PDFFont	= pdfDoc.embedStandardFont(StandardFonts.Helvetica)

			const numberOfPagesOriginal : number = pdfDoc.getPages().length

			expect(numberOfPagesOriginal).toEqual(0)


			service.createNotesSection({ pdf: pdfDoc, horizontalPageMargin: 20, headingBottomMargin: 30, notesBottomMargin: 20, noteDataset: validAndShortDataset, dateFont: regularSansFont, answerFont: regularSansFont, fontSizeHeading: 16, fontSizeParagraph: 10, questionSectionMargin: 20 })

			const numberOfPagesFirstRun : number = pdfDoc.getPages().length

			expect(numberOfPagesFirstRun).toEqual(1)


			service.createNotesSection({ pdf: pdfDoc, horizontalPageMargin: 20, headingBottomMargin: 30, notesBottomMargin: 20, noteDataset: validAndShortDataset, dateFont: regularSansFont, answerFont: regularSansFont, fontSizeHeading: 16, fontSizeParagraph: 10, questionSectionMargin: 20 })

			const numberOfPagesSecondRun : number = pdfDoc.getPages().length

			expect(numberOfPagesSecondRun).toEqual(2)

		})

		it('should add multiple pages for a larger dataset', async() => {

			const pdfDoc : PDFDocument = await PDFDocument.create()

			const regularSansFont	: PDFFont	= pdfDoc.embedStandardFont(StandardFonts.Helvetica)

			const numberOfPagesOriginal : number = pdfDoc.getPages().length

			expect(numberOfPagesOriginal).toEqual(0)

			service.createNotesSection({ pdf: pdfDoc, horizontalPageMargin: 20, headingBottomMargin: 30, notesBottomMargin: 20, noteDataset: validAndLongDataset, dateFont: regularSansFont, answerFont: regularSansFont, fontSizeHeading: 16, fontSizeParagraph: 10, questionSectionMargin: 20 })

			const numberOfPagesFirstRun : number = pdfDoc.getPages().length

			expect(numberOfPagesFirstRun).toBeGreaterThan(5)


		})

		it('should add a single page when called with an invalid dataset', async () => {
			const pdfDoc : PDFDocument = await PDFDocument.create()

			const regularSansFont	: PDFFont	= pdfDoc.embedStandardFont(StandardFonts.Helvetica)

			const numberOfPagesOriginal : number = pdfDoc.getPages().length

			expect(numberOfPagesOriginal).toEqual(0)


			service.createNotesSection({ pdf: pdfDoc, horizontalPageMargin: 20, headingBottomMargin: 30, notesBottomMargin: 20, noteDataset: validAndLongDataset, dateFont: regularSansFont, answerFont: regularSansFont, fontSizeHeading: 16, fontSizeParagraph: 10, questionSectionMargin: 20 })

			const numberOfPagesFirstRun : number = pdfDoc.getPages().length

			expect(numberOfPagesFirstRun).toBeGreaterThan(1)


			service.createNotesSection({ pdf: pdfDoc, horizontalPageMargin: 20, headingBottomMargin: 30, notesBottomMargin: 20, noteDataset: invalidDataset, dateFont: regularSansFont, answerFont: regularSansFont, fontSizeHeading: 16, fontSizeParagraph: 10, questionSectionMargin: 20 })

			const numberOfPagesSecondRun : number = pdfDoc.getPages().length

			expect(numberOfPagesSecondRun).toEqual(numberOfPagesFirstRun + 1)

		})

		it('should add a single page when the provided dataset is empty i.e. no answers given', async () => {
			const pdfDoc : PDFDocument = await PDFDocument.create()
			const regularSansFont	: PDFFont	= pdfDoc.embedStandardFont(StandardFonts.Helvetica)

			const numberOfPagesOriginal : number = pdfDoc.getPages().length

			expect(numberOfPagesOriginal).toEqual(0)


			service.createNotesSection({ pdf: pdfDoc, horizontalPageMargin: 20, headingBottomMargin: 30, notesBottomMargin: 20, noteDataset: validAndLongDataset, dateFont: regularSansFont, answerFont: regularSansFont, fontSizeHeading: 16, fontSizeParagraph: 10, questionSectionMargin: 20 })

			const numberOfPagesFirstRun : number = pdfDoc.getPages().length

			expect(numberOfPagesFirstRun).toBeGreaterThan(1)


			service.createNotesSection({ pdf: pdfDoc, horizontalPageMargin: 20, headingBottomMargin: 30, notesBottomMargin: 20, noteDataset: emptyDataset, dateFont: regularSansFont, answerFont: regularSansFont, fontSizeHeading: 16, fontSizeParagraph: 10, questionSectionMargin: 20 })

			const numberOfPagesSecondRun : number = pdfDoc.getPages().length

			expect(numberOfPagesSecondRun).toEqual(numberOfPagesFirstRun + 1)

		})
	})

	describe('.createGeneralDatasetsSection()', () => {

		const validDataset: Dataset = {
			question: new Question({ id: 'question', type: 'string', translations: { de: 'bla valide', en: 'bla valid' }, }),
			datapoints: [{ value: 'bla', date: '2024-01-01' }]
		}

		const invalidDataset: Dataset = {
			question: new Question({ id: 'question', type: 'boolean', translations: { de: 'bla invalide', en: 'bla invalid' }, }),
			datapoints: [{ value: 'bla', date: '2024-01-01' }]
		}

		const emptyDataset: Dataset = {
			question: new Question({ id: 'question', type: 'string', translations: { de: 'bla leer', en: 'bla empty' }, }),
			datapoints: []
		}

		it('should add an extra page to an existing PDF when createGeneralDatasetSection is called with a dataset', async () => {

			const pdfDoc: PDFDocument = await PDFDocument.create()

			const regularSansFont: PDFFont = pdfDoc.embedStandardFont(StandardFonts.Helvetica)

			const numberOfPagesOriginal: number = pdfDoc.getPages().length

			expect(numberOfPagesOriginal).toEqual(0)

			await service.createGeneralDatasetsSection(
				{
					datasets: [validDataset],
					legendFont: regularSansFont,
					pdf: pdfDoc,
					questionFont: regularSansFont,
					regularFont: regularSansFont,
				}
			)

			const numberOfPagesFirstRun: number = pdfDoc.getPages().length

			expect(numberOfPagesFirstRun).toEqual(1)

			await service.createGeneralDatasetsSection(
				{
					datasets: [validDataset],
					legendFont: regularSansFont,
					pdf: pdfDoc,
					questionFont: regularSansFont,
					regularFont: regularSansFont,
				}
			)

			const numberOfPagesSecondRun: number = pdfDoc.getPages().length

			expect(numberOfPagesSecondRun).toEqual(2)
		})

		it('should add a single page when createGeneralDatasetSection called with an invalid dataset', async () => {

			const pdfDoc: PDFDocument = await PDFDocument.create()

			const regularSansFont: PDFFont = pdfDoc.embedStandardFont(StandardFonts.Helvetica)

			const numberOfPagesOriginal: number = pdfDoc.getPages().length

			expect(numberOfPagesOriginal).toEqual(0)

			await service.createGeneralDatasetsSection(
				{
					datasets: [validDataset],
					legendFont: regularSansFont,
					pdf: pdfDoc,
					questionFont: regularSansFont,
					regularFont: regularSansFont,
				}
			)

			const numberOfPagesFirstRun: number = pdfDoc.getPages().length

			expect(numberOfPagesFirstRun).toEqual(1)

			await service.createGeneralDatasetsSection(
				{
					datasets: [invalidDataset],
					legendFont: regularSansFont,
					pdf: pdfDoc,
					questionFont: regularSansFont,
					regularFont: regularSansFont,
				}
			)

			const numberOfPagesSecondRun: number = pdfDoc.getPages().length

			expect(numberOfPagesSecondRun).toEqual(numberOfPagesFirstRun + 1)
		})

		it('should add a single page when createGeneralDatasetSection is called and the provided dataset is empty i.e. no answers given', async () => {

			const pdfDoc: PDFDocument = await PDFDocument.create()

			const regularSansFont: PDFFont = pdfDoc.embedStandardFont(StandardFonts.Helvetica)

			const numberOfPagesOriginal: number = pdfDoc.getPages().length

			expect(numberOfPagesOriginal).toEqual(0)

			await service.createGeneralDatasetsSection(
				{
					datasets: [validDataset],
					legendFont: regularSansFont,
					pdf: pdfDoc,
					questionFont: regularSansFont,
					regularFont: regularSansFont,
				}
			)

			const numberOfPagesFirstRun: number = pdfDoc.getPages().length

			expect(numberOfPagesFirstRun).toEqual(1)

			await service.createGeneralDatasetsSection(
				{
					datasets: [emptyDataset],
					legendFont: regularSansFont,
					pdf: pdfDoc,
					questionFont: regularSansFont,
					regularFont: regularSansFont,
				}
			)

			const numberOfPagesSecondRun: number = pdfDoc.getPages().length

			expect(numberOfPagesSecondRun).toEqual(numberOfPagesFirstRun + 1)

		})

	})

	describe('createHeader()', () => {
		it('should create header with correct elements for notes page', async () => {
			const pdfDoc: PDFDocument = await PDFDocument.create()
			const page: PDFPage = pdfDoc.addPage()
			const monoFont: PDFFont = pdfDoc.embedStandardFont(StandardFonts.Helvetica)
			const sansFont: PDFFont = pdfDoc.embedStandardFont(StandardFonts.Helvetica)

			// Stub methods called within createHeader method
			const createLogoSpy					: jasmine.Spy = spyOn(service, 'createLogo')
			const createColoredHeaderElementSpy	: jasmine.Spy = spyOn(service, 'createColoredHeaderElement')
			const createHeaderTextsSpy			: jasmine.Spy = spyOn(service, 'createHeaderTexts')

			// Set isANotesPage to true to simulate a notes page scenario
			service.isANotesPage = true

			// Call createHeader method
			service.createHeader(pdfDoc, page, monoFont, sansFont)

			// Expectations
			expect(createLogoSpy).toHaveBeenCalledWith(page)
			expect(createColoredHeaderElementSpy).toHaveBeenCalledWith(page, rCViolet, page.getHeight() - 60, page.getWidth())
			expect(createHeaderTextsSpy).toHaveBeenCalledWith(pdfDoc, page, page.getHeight(), page.getWidth(), monoFont, sansFont)
		})

		it('should create header with correct elements for non-notes page', async () => {
			const pdfDoc: PDFDocument = await PDFDocument.create()
			const page: PDFPage = pdfDoc.addPage()
			const monoFont: PDFFont = pdfDoc.embedStandardFont(StandardFonts.Helvetica)
			const sansFont: PDFFont = pdfDoc.embedStandardFont(StandardFonts.Helvetica)

			// Stub methods called within createHeader method
			const createLogoSpy: jasmine.Spy = spyOn(service, 'createLogo')
			const createColoredHeaderElementSpy: jasmine.Spy = spyOn(service, 'createColoredHeaderElement')
			const createHeaderTextsSpy: jasmine.Spy = spyOn(service, 'createHeaderTexts')


			// Set isANotesPage to false to simulate a non-notes page scenario
			service.isANotesPage = false

			// Call createHeader method
			service.createHeader(pdfDoc, page, monoFont, sansFont)

			// Expectations
			expect(createLogoSpy).toHaveBeenCalledWith(page)
			expect(createColoredHeaderElementSpy).toHaveBeenCalledWith(page, rCGreen, page.getHeight(), page.getWidth())
			expect(createHeaderTextsSpy).toHaveBeenCalledWith(pdfDoc, page, page.getHeight(), page.getWidth(), monoFont, sansFont)
		})
	})

	describe('createHeaderTexts()',  () => {

		it('should create the header text correctly', async () => {

			const dateToday		: string = new Date()
				.toLocaleDateString('en')

			const dateStart		: string = sub(new Date(), { days: 60 })
				.toLocaleDateString('en')

			const pdfDoc: PDFDocument = await PDFDocument.create()
			const page: PDFPage = pdfDoc.addPage()
			const monoFont: PDFFont = pdfDoc.embedStandardFont(StandardFonts.Helvetica)
			const sansFont: PDFFont = pdfDoc.embedStandardFont(StandardFonts.Helvetica)

			const pageHeight	: number = page.getHeight()

			const drawTextSpy: jasmine.Spy = spyOn(page, 'drawText')

			service.createHeaderTexts(pdfDoc, page, page.getHeight(), page.getWidth(), monoFont, sansFont)

			expect(drawTextSpy.calls.argsFor(0)).toEqual(['blank', {
				font: monoFont,
				color: rgb(0, 0, 0),
				x: pageMargin,
				y: pageHeight - 54,
				size: 6.4,
			}])

			expect(drawTextSpy.calls.argsFor(1)).toEqual(['blank ' + dateToday, {
				font:	monoFont,
				color:	rgb(0, 0, 0),
				x:		page.getWidth() - pageMargin - monoFont.widthOfTextAtSize('blank ' + dateToday, 8),
				y:		pageHeight - pageMargin - 8,
				size:	8
			}])

			expect(drawTextSpy.calls.argsFor(2)).toEqual(['blank ' + dateStart, {
				font:	monoFont,
				color:	rgb(0, 0, 0),
				x:		page.getWidth() - pageMargin - monoFont.widthOfTextAtSize('blank ' + dateStart, 8),
				y:		pageHeight - pageMargin - 8 * 5,
				size:	8
			}])

			expect(drawTextSpy.calls.argsFor(3)).toEqual([dateStart, {
				font:	monoFont,
				color:	rCGreen,
				x:		page.getWidth() - pageMargin - monoFont.widthOfTextAtSize(dateStart, 8),
				y:		pageHeight - pageMargin - 8 * 5,
				size:	8
			}])

			expect(drawTextSpy.calls.argsFor(4)).toEqual(['blank blank', {
				font:	sansFont,
				color:	rgb(1, 1, 1),
				x:		page.getWidth() - pageMargin - 168 / 2 - sansFont.widthOfTextAtSize('blank blank', 8) / 2,
				y:		pageHeight - 54,
				size:	8
			}])

		})

		it('should create the header Text correctly if the page is a notes page', async () => {

			service.isANotesPage = true

			const dateToday		: string = new Date()
				.toLocaleDateString('en')

			const dateStart		: string = sub(new Date(), { days: 60 })
				.toLocaleDateString('en')

			const pdfDoc: PDFDocument = await PDFDocument.create()
			const page: PDFPage = pdfDoc.addPage()
			const monoFont: PDFFont = pdfDoc.embedStandardFont(StandardFonts.Helvetica)
			const sansFont: PDFFont = pdfDoc.embedStandardFont(StandardFonts.Helvetica)

			const pageHeight	: number = page.getHeight()

			const drawTextSpy: jasmine.Spy = spyOn(page, 'drawText')

			service.createHeaderTexts(pdfDoc, page, page.getHeight(), page.getWidth(), monoFont, sansFont)

			expect(drawTextSpy.calls.argsFor(0)).toEqual(['blank', {
				font: monoFont,
				color: rgb(0, 0, 0),
				x: pageMargin,
				y: pageHeight - 54,
				size: 6.4,
			}])

			expect(drawTextSpy.calls.argsFor(1)).toEqual(['blank ' + dateToday, {
				font:	monoFont,
				color:	rgb(0, 0, 0),
				x:		page.getWidth() - pageMargin - monoFont.widthOfTextAtSize('blank ' + dateToday, 8),
				y:		pageHeight - pageMargin - 8,
				size:	8
			}])

			expect(drawTextSpy.calls.argsFor(2)).toEqual(['blank ' + dateStart, {
				font:	monoFont,
				color:	rgb(0, 0, 0),
				x:		page.getWidth() - pageMargin - monoFont.widthOfTextAtSize('blank ' + dateStart, 8),
				y:		pageHeight - pageMargin - 8 * 5,
				size:	8
			}])

			expect(drawTextSpy.calls.argsFor(3)).toEqual([dateStart, {
				font:	monoFont,
				color:	rCGreen,
				x:		page.getWidth() - pageMargin - monoFont.widthOfTextAtSize(dateStart, 8),
				y:		pageHeight - pageMargin - 8 * 5,
				size:	8
			}])

			expect(drawTextSpy.calls.argsFor(4)).toEqual(['blank blank', {
				font:	sansFont,
				color:	rgb(1, 1, 1),
				x:		page.getWidth() - pageMargin - 168 / 2 - sansFont.widthOfTextAtSize('blank blank', 8) / 2,
				y:		pageHeight - 54,
				size:	8
			}])

		})

	})

	describe('createLogo', () => {
		it('should draw the logo on the page', async () => {
			const pdfDoc: PDFDocument = await PDFDocument.create()
			const page: PDFPage = pdfDoc.addPage()

			// Spy on page.drawSvgPath to check if it's called with the correct parameters
			const drawSvgPth :jasmine.Spy = spyOn(page, 'drawSvgPath')

			service.createLogo(page)

			expect(drawSvgPth).toHaveBeenCalledWith(svgPathHcpCross		, { color: rCOrange })
			expect(drawSvgPth).toHaveBeenCalledWith(svgPathHcpEarLeft	, { color: rCViolet })
			expect(drawSvgPth).toHaveBeenCalledWith(svgPathHcpEarRight	, { color: rCViolet })
			expect(drawSvgPth).toHaveBeenCalledWith(svgPathHcpRecovery	, { color: rCViolet })
			expect(drawSvgPth).toHaveBeenCalledWith(svgPathHcpCat		, { color: rCOrange })
		})
	})

	describe('.createColoredHeaderElement()', () => {

		it('should draw two circles and rectangle', async () => {

			const pdfDoc: PDFDocument = await PDFDocument.create()
			const page: PDFPage = pdfDoc.addPage()

			const drawCircleSpy: jasmine.Spy = spyOn(page, 'drawCircle')
			const drawRectangleSpy: jasmine.Spy = spyOn(page, 'drawRectangle')

			const buttonWidth		: number = 168
			const buttonHeight		: number = 13
			const circleRadius		: number = buttonHeight / 2
			const rectangleWidth	: number = buttonWidth - circleRadius * 2

			service.createColoredHeaderElement(page, rCGreen, page.getHeight(), page.getWidth())

			expect(drawCircleSpy.calls.argsFor(0)).toEqual([
				{
					x:		403.78,
					y:		page.getHeight() - 51,
					size:	circleRadius,
					color:	rCGreen
				}])

			expect(drawCircleSpy.calls.argsFor(1)).toEqual([
				{
					x:		page.getWidth() - pageMargin - circleRadius,
					y:		page.getHeight() - 51,
					size:	circleRadius,
					color:	rCGreen
				}
			])

			expect(drawRectangleSpy.calls.argsFor(0)).toEqual([{
				x:		page.getWidth() - pageMargin - circleRadius - rectangleWidth,
				y:		page.getHeight() - 57.5,
				width:	buttonWidth - circleRadius * 2,
				height:	buttonHeight,
				color:	rCGreen,
			}])
		})

	})


	/**
	 * Test written for parts not done by author (Hannu).
	 */
	// describe('.createPDFOfReport()', () => {

	// 	it('should throw if called with an empty object', async () => {

	// 		const mockReport: Report = {} as Report // Replace with the actual Report type

	// 		const pdfResultPromise : Promise<string> = service.createPDFOfReport(mockReport)
	// 		await expectAsync(pdfResultPromise).toBeRejected()

	// 	})

	// 	it('should return something not undefined when called with a report', async () => {

	// 		const emptyReport: Report = new Report(['test report', '1998-30-12T12:00:00.000Z', []])

	// 		const pdfResult: string = await service.createPDFOfReport(emptyReport)

	// 		expect(pdfResult).toBeDefined()

	// 	})

	// 	it('should create a one page PDF if the report is empty', async () => {
	// 		// Mock necessary dependencies and input data
	// 		const emptyReport: Report = new Report(['test report', '1998-30-12', []])

	// 		const pdfResult: string = await service.createPDFOfReport(emptyReport)
	// 		const pdf : PDFDocument = await PDFDocument.load(pdfResult)

	// 		const numberOfPages : number = pdf.getPages().length

	// 		expect(numberOfPages).toEqual(1)
	// 	})

	// 	it('should add a separate page for recent notes', async () => {
	// 		// Mock necessary dependencies and input data
	// 		const reportWithNotes: Report = new Report(['test report', '1998-30-12T12:00:00.000Z', [['rcc-curated-default-00-daily-note', 'HAayAYA', CalendarDateString.today()+'T12:00:00.000+12:00']]])

	// 		const pdfResult: string = await service.createPDFOfReport(reportWithNotes)
	// 		const pdf : PDFDocument = await PDFDocument.load(pdfResult)

	// 		const numberOfPages : number = pdf.getPages().length

	// 		expect(numberOfPages).toEqual(2)
	// 	})
	// })

})
