import	{
			Injectable,
			NgModule
		}								from '@angular/core'
import	{
			ExportConfig,
			ExportMetadata,
			ExportResult,
			ExportService,
			TranslationsModule,
			provideExportService,
			provideTranslationMap
		}								from '@rcc/common'
import	{
			Report,
			assert
		} 								from '@rcc/core'
import	{
			RccPdfService
		}								from './pdf.service'
import	{
			PDFWidgetComponentAdapterService,
		}								from './pdf-widget-component-adapter.service'

import en from './i18n/en.json'
import de from './i18n/de.json'
import { DataVisualizationModule } from '../data-visualization'

@Injectable()
export class ReportPdfExportService extends ExportService<Report,string> {
	public label	 	: string	= 'PDF_MODULE.EXPORT.LABEL'
	public description	: string	= 'PDF_MODULE.EXPORT.DESCRIPTION'

	public constructor(
		public rccPdfService: RccPdfService
	) {
		super()
	}

	public canExport(x: unknown): x is Report {
		return x instanceof Report
	}


	public async export(x: ExportConfig<Report>): Promise<ExportResult<string>> {
		assert(this.canExport(x.data), 'ReportJsonExportService.export: x.data must be of type Report')

		const pdf : string = await this.rccPdfService.createPDFOfReport(x.data)

		return {
			toString: () => pdf,
			raw: () => pdf
		}
	}

	public getMetadata(x: ExportConfig<Report>): ExportMetadata {
		assert(this.canExport(x.data), 'ReportPdfExportService.export: x.data must be of type Report')
		return {
			suggestedFilename	: `RCC-Report-${x.data.date.toISOString().split('T')[0]}.pdf`,
			mimeType			: 'application/pdf'
		}
	}
}


@NgModule({
	providers: [
		RccPdfService,
		provideTranslationMap('PDF_MODULE', { en,de }),
		...provideExportService(ReportPdfExportService),
		PDFWidgetComponentAdapterService,
	],
	imports: [
		DataVisualizationModule,
		TranslationsModule
	]
})
export class PDFModule {}
