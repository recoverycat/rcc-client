import	{
			Component,
			ElementRef,
			Input,
			OnChanges,
			ViewChild,
			SimpleChanges
		}									from '@angular/core'
import	{
			Action,
			RccTranslationService
		}									from '@rcc/common'
import	{
			CalendarDateString,
			DAILY_NOTES_ID,
			Report,
		}									from '@rcc/core'

import	{
			Dataset,
			DatasetsService,
			DataViewControl,
			DataViewScope,
			Datapoint
		}									from '../../data-visualization'
import	{	QuestionnaireService		}	from '../../questions'



interface DatasetInfo {
	dataset			: Dataset,
	pinAction		: Action,
	dataViewControl	: DataViewControl,
}


@Component({
    selector: 'rcc-report-stacked-view',
    styleUrls: ['./report-stacked-view.component.scss'],
    templateUrl: './report-stacked-view.component.html',
    standalone: false
})
export class ReportStackedViewComponent implements OnChanges {

	@Input() public report			: Report
	@Input() public dataViewScope	: DataViewScope
	@Input() public startDate		: string
	@Input() public endDate			: string

	protected allDatasets			: Dataset[]			= []
	protected regularDatasets		: Dataset[]			= []
	private notes					: Dataset
	private emptyNotes				: Dataset

	protected get notesToShow()		: Dataset {
		return this.notes ? {
			question: this.notes.question,
			datapoints: this.notes.datapoints
				.filter((dataPoint: Datapoint) =>
					CalendarDateString.isWithinInterval(dataPoint.date, this.startDate, this.endDate)
				)
				.sort((a, b) => CalendarDateString.diffInDays(b.date, a.date) > 0 ? 1 : -1)
		} : undefined
	}

	private _datasetInfos			: DatasetInfo[]		= []
	protected get datasetInfos()	: DatasetInfo[] {
		return this._datasetInfos.filter((datasetInfo: DatasetInfo) => datasetInfo.dataset.question.id !== this.pinnedQuestionId)
	}


	public constructor(
		private readonly datasetsService		: DatasetsService,
		private readonly translationService		: RccTranslationService,
		private readonly questionnaireService	: QuestionnaireService,
	) {
		void this.getEmptyNotes()
	}

	private async getEmptyNotes(): Promise<Dataset> {
		this.emptyNotes = this.emptyNotes ?? { question: await this.questionnaireService.get(DAILY_NOTES_ID), datapoints: [] }
		return this.emptyNotes
	}

	protected get message(): string {
		return this.pinnedDataSetInfo ? 'REPORT_STACKED_VIEW.LIST.PINNED_HEADING' : 'REPORT_STACKED_VIEW.LIST.HEADING'
	}

	protected get showNotes(): boolean {
		const isYear: boolean = this.dataViewScope ? this.dataViewScope === 'year' : false
		return !isYear && this.notes?.datapoints?.length > 0
	}

	protected collapseNotes		: boolean
								= true
	protected get noteHeader(): string  {

		const noteSuffix: string = this.dataViewScope ? this.dataViewScope.toUpperCase() : 'GENERIC'

		return this.translationService.translate(`REPORT_STACKED_VIEW.NOTES.HEADER.${noteSuffix}`)
	}
	protected toggleNotes(): void {
		this.collapseNotes = !this.collapseNotes
	}

	protected isDatasetCurrentlyPinned(datasetInfo: DatasetInfo): boolean {
		if (!this.pinnedDataSetInfo || !this.pinnedDataSetInfo.dataset || !this.pinnedDataSetInfo.dataset.question) return false

		return datasetInfo.dataset.question.id === this.pinnedDataSetInfo.dataset.question.id
	}

	public async ngOnChanges(changes: SimpleChanges) : Promise<void> {

		if('report' in changes){

			this.allDatasets 			=	this.report
											?	await this.datasetsService.getDatasets(this.report)
											:	[]

			this.notes					=	this.allDatasets
											.find(	(dataset: Dataset) => dataset.question.id === DAILY_NOTES_ID )
											?? await this.getEmptyNotes()

			this.regularDatasets		=	this.allDatasets
											.filter((dataset: Dataset) => dataset.question.id !== DAILY_NOTES_ID )

		}

		this._datasetInfos = this.regularDatasets.map( (dataset: Dataset) => {

			const pinAction			: 	Action
									= 	this.getPinAction(dataset)

			const dataViewControl	: 	DataViewControl
									=	new DataViewControl({
											datasets: 	[dataset, this.notes],
											scope:		this.dataViewScope,
											startDate:	this.startDate,
											endDate:	this.endDate
										})
			return { dataset, pinAction, dataViewControl }
		})


	}






	// #region pinning
	@ViewChild('pinScrollAnchor')
	private pinScrollAnchor? 		: ElementRef<HTMLElement>

	protected pinnedQuestionId?		: string
									= undefined

	protected get pinnedDataSetInfo() : DatasetInfo | undefined {
		return this._datasetInfos.find( (dI: DatasetInfo) => dI.dataset.question.id === this.pinnedQuestionId )
	}


	private getPinAction(dataset: Dataset): Action {
		return {
			icon: 'pin',
			label: `${this.translationService.translate('REPORT_STACKED_VIEW.PIN.LABEL')} - ${this.translationService.translate(dataset.question)}`,
			handler: () => {
				if(this.pinnedQuestionId === dataset.question.id)
					this.pinnedQuestionId = undefined
				else {
					this.pinnedQuestionId = dataset.question.id
					setTimeout(() => { this.pinScrollAnchor?.nativeElement.scrollIntoView() } )
				}
			}
		}
	}
	// #endregion
}
