import	{
			Component,
		}									from '@angular/core'
import { RccReportStackedViewPageBaseComponent } from '../base/report-stacked-view-page-base.component'

@Component({
    selector: 'rcc-report-stacked-view-reduced-page',
    templateUrl: './report-stacked-view-reduced-page.component.html',
    styleUrls: [
        './report-stacked-view-reduced-page.component.scss',
        '../base/report-stacked-view-page-base.component.scss',
    ],
    standalone: false
})
export class RccReportStackedViewReducedPageComponent extends RccReportStackedViewPageBaseComponent {}
