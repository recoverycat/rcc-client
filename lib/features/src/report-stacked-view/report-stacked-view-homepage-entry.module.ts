import	{
			NgModule,
			ModuleWithProviders,
			Provider,
		}									from '@angular/core'
import	{
			Action,
			getEffectivePosition,
			WithPosition,
			provideHomePageEntry
		}									from '@rcc/common'
import	{	REPORT_STACKED_VIEW_PATH, ReportStackedViewModule		}	from './report-stacked-view.module'

@NgModule({
	imports:[
		ReportStackedViewModule,
	],
})
export class ReportStackedViewHomePageEntryModule {

/**
* This method can add entries to the home page.
*
* Calling it without parameter, will add entries to the home
* page automatically at reasonably adequate positions.
*
* If you want to determine the positions yourself provide a `config` parameter :
*
* | .position  | value                  | effect                                                    |
* |------------|------------------------|-----------------------------------------------------------|
* | .position  | true                   | adds home page entry at a reasonably adequate position    |
* |            | positive number        | adds home page entry at position counting from the top    |
* |            | negative number        | adds home page entry at position counting from the bottom |
* |            | undefined        		| adds home page entry at a position somewhere in between   |
* |------------|------------------------|-----------------------------------------------------------|
*
* Example: 	`ReportStackedViewHomePageEntryModule.addEntry({ position: 1 })`,
*/

	public static addEntry(config?: WithPosition): ModuleWithProviders<ReportStackedViewHomePageEntryModule> {

		const homePageEntry: Action = {
			label:			'REPORT_STACKED_VIEW.HOMEPAGE_ENTRY.LABEL',
			description:	'REPORT_STACKED_VIEW.HOMEPAGE_ENTRY.DESCRIPTION',
			icon:			'data',
			position:		getEffectivePosition(config, 1),
			path:			REPORT_STACKED_VIEW_PATH,
			category:		'analyze',
		}

		const providers : Provider[] = []

		providers.push(provideHomePageEntry(homePageEntry))

		return {
			ngModule:	ReportStackedViewHomePageEntryModule,
			providers
		}
	}
}
