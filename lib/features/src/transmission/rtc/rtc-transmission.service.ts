import 	{
			Injectable,
			Inject,
		} 									from '@angular/core'
import	{
			firstValueFrom,
		}									from 'rxjs'

import	{
			randomString,
			EncryptionHandler,
			GcmHandler,
			RccRtc,
			assert
		}									from '@rcc/core'

import	{
			RccTransmission,
			AbstractTransmissionService,
		}									from '@rcc/common'


import	{
			assertRtcTransmissionConfig,
			assertRtcTransmissionMeta,
			isRtcTransmissionMeta,
			RTC_SIGNAL_SERVER,
			RTC_STUN_SERVERS,
			RtcTransmissionConfig,
			RtcTransmissionMeta,
		}									from './rtc-transmission.commons'




// TODO needs rework or removal, see combined

export class RtcTransmission implements RccTransmission{


	/* STATIC */

	public static async createMeta() : Promise<unknown> {
		const key 			: string 	= (await GcmHandler.generate())[0]
		const channel		: string	= randomString(20)

		return ['rcc-rtc', channel, key]
	}



	/* INSTANCE */

	protected data				: unknown
	protected url				: string
	protected channel			: string

	protected signalServer		: string
	protected stunServers		: string[]

	protected key				: string
	protected encryptionHandler : EncryptionHandler

	protected rccRtc			: RccRtc

	public ready				: Promise<void>

	public constructor(config: RtcTransmissionConfig, meta? : RtcTransmissionMeta){

		assertRtcTransmissionConfig(config)

		this.data			=	config.data
		this.signalServer	=	config.signalServer
		this.stunServers	=	config.stunServers

		this.ready	 		= 	meta
								?	Promise.resolve(this.setupWithExistingMeta(meta))
								:	this.setup()

	}



	public async setup() : Promise<void> {

		const meta	: unknown
					= await RtcTransmission.createMeta()

		this.setupWithExistingMeta(meta)
	}

	public setupWithExistingMeta(meta: unknown) : void {

		assertRtcTransmissionMeta(meta)

		this.channel 	= meta[1]
		this.key 		= meta[2]

		const encryptionHandler : GcmHandler 	= 	new GcmHandler(this.key)
		const url				: string		=	this.signalServer
		const stunServers		: string[]		=	this.stunServers
		const channel			: string		=	this.channel

		this.rccRtc = new RccRtc({
			url,
			channel,
			encryptionHandler,
			stunServers
		})

	}


	public get meta() : string[] {
		return ['rcc-rtc', this.channel, this.key]
	}

	public async start() : Promise<void> {
		await 	this.rccRtc.open({
					timeoutPeer:		60*1000,
					timeoutConnection:	 3*1000
				})
		await 	this.rccRtc.send(this.data)
		await 	this.rccRtc.close()

	}

	public async cancel(): Promise<void> {
		await this.rccRtc.close()
	}

}




@Injectable()
export class RtcTransmissionService extends AbstractTransmissionService {

	public override id			:		string = 'RTC_TRANSMISSION_SERVICE'
	public override label		:		string = 'RTC_TRANSMISSION_SERVICE.LABEL'
	public override description	:		string = 'RTC_TRANSMISSION_SERVICE.DESCRIPTION'

	public constructor(
		@Inject(RTC_STUN_SERVERS)
		protected stunServers	: string[],
		@Inject(RTC_SIGNAL_SERVER)
		protected signalServer	: string
	){
		super()
		this.assertServiceConfiguration()
	}



	public validateMeta(meta:unknown): boolean {
		return isRtcTransmissionMeta(meta)
	}

	public async createMeta() : Promise<unknown> {
		return await RtcTransmission.createMeta()
	}

	public assertServiceConfiguration() : void {
		assert(this.stunServers, 	'RtcTransmissionService.assertConfiguration(): missing stun servers. Please use RtcTransmissionServiceModule.forRoot() to set these up.')
		assert(this.signalServer,	'RtcTransmissionService.assertConfiguration(): missing signal server. Please use RtcTransmissionServiceModule.forRoot() to set this up.')
	}

	public async setup(data:unknown) : Promise<RtcTransmission> {

		const stunServers	: string[]			= this.stunServers
		const signalServer	: string 			= this.signalServer

		const transmission	: RtcTransmission	= new RtcTransmission({
													data,
													signalServer,
													stunServers
												})

		await transmission.ready

		return transmission
	}

	/**
	 * This method does the same as {@link RtcTransmissionService.setup()}
	 * and additionally initializes the returned {@link RtcTransmission} with
	 * the provided meta data.
	 *
	 * _Note:_ Use with caution! Unlike
	 * {@link RtcTransmissionService.setup()}
	 * the origin of the meta data is unknown to this method. So data could end
	 * up anywhere, if the meta data was not verified beforehand.

	 */
	public async setupWithExistingMeta(data: unknown, meta: unknown) : Promise<RtcTransmission> {

		assertRtcTransmissionMeta(meta)

		const stunServers	: string[]			= 	this.stunServers
		const signalServer	: string 			= 	this.signalServer

		const transmission	: RtcTransmission	= 	new RtcTransmission(
														{
															data,
															signalServer,
															stunServers
														},
														meta
													)

		await transmission.ready

		return transmission
	}



	public async listen(meta: RtcTransmissionMeta) : Promise<unknown> {

		assertRtcTransmissionMeta(meta)

		const [channel, key]	: string[]			=	meta.slice(1)
		const url				: string			=	this.signalServer
		const stunServers		: string[]			=	this.stunServers
		const encryptionHandler	: GcmHandler		=	new GcmHandler(key)

		const rccRtc			: RccRtc			= 	new RccRtc({
														url,
														channel,
														encryptionHandler,
														stunServers
													})

		const data 				: Promise<unknown>	= 	firstValueFrom(rccRtc.data$)

		await 	rccRtc.open({
					timeoutPeer:		60*1000,
					timeoutConnection:	 3*1000
				})

		const result			: unknown			= 	await data

		await rccRtc.done()
		void  rccRtc.close()

		return result
	}

}
