import	{	InjectionToken		}	from '@angular/core'

import	{
			isErrorFree,
			assert,
			assertProperty
		}							from '@rcc/core'


export type RtcTransmissionMeta = ['rcc-wrt', string, string] // ["rcc-wrt", channel, key]


export function assertRtcTransmissionMeta(x:unknown): asserts x is RtcTransmissionMeta {

	assert(x instanceof Array,			'assertRtcTransmissionMeta: must be an array.')
	assert(x[0] === 'rcc-rtc',			'assertRtcTransmissionMeta: [0] must be \'rcc-rtc.')
	assert(typeof x[1] === 'string',	'assertRtcTransmissionMeta: [1] must be string.')
	assert(typeof x[2] === 'string',	'assertRtcTransmissionMeta: [2] must be string.')
}

export function isRtcTransmissionMeta(x:unknown): x is RtcTransmissionMeta {

	return isErrorFree( () => assertRtcTransmissionMeta(x))

}


export interface RtcTransmissionConfig {
	data			: unknown,
	signalServer	: string
	stunServers		: string[]
}



export function assertRtcTransmissionConfig(x:unknown) : asserts x is RtcTransmissionConfig {


	assertProperty(x, 'data',					'assertRtcTransmissionConfig: missing data.')
	assertProperty(x, 'signalServer',			'assertRtcTransmissionConfig: missing url.')
	assert(typeof x.signalServer === 'string',	'assertRtcTransmissionConfig: url must be a string.')
	assert(x.signalServer.match(/^wss:/),		'assertRtcTransmissionConfig: url must start with \'wss:\'.')

}


export function isRtcTransmissionConfig(x:unknown) : x is RtcTransmissionConfig {
	return isErrorFree( () => assertRtcTransmissionConfig(x) )
}


export const RTC_SIGNAL_SERVER 	: InjectionToken<string> = new InjectionToken<string>('url of the websocket server for WebRTC ice negotiation')
export const RTC_STUN_SERVERS 	: InjectionToken<string> = new InjectionToken<string>('url of the stun servers for WebRTC ice negotiation')
