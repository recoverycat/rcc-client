import	{
			NgModule,
			ModuleWithProviders
		}										from '@angular/core'
import	{
			DevModule,
			TransmissionModule,
			IncomingDataServiceModule,
			provideTranslationMap,
		}										from '@rcc/common'

import	{	RtcTransmissionService		}		from './rtc-transmission.service'

import	{
			RTC_SIGNAL_SERVER,
			RTC_STUN_SERVERS
		}										from './rtc-transmission.commons'


import en from './i18n/en.json'
import de from './i18n/de.json'


@NgModule({
	providers: [
		RtcTransmissionService,
		provideTranslationMap('RTC_TRANSMISSION_SERVICE', { en, de })
	],
	imports: [
		DevModule.note('WebrtcTransmissionModule'),
		TransmissionModule.forChild(RtcTransmissionService),
		IncomingDataServiceModule
	],
})
export class RtcTransmissionModule {
	public static forRoot(url:string, stunServers: string[]): ModuleWithProviders<RtcTransmissionModule> {

		return 	{
					ngModule: 	RtcTransmissionModule,
					providers:	[
									{
										provide: 	RTC_SIGNAL_SERVER,
										useValue: 	url
									},
									{
										provide: 	RTC_STUN_SERVERS,
										useValue: 	stunServers
									},

								]
				}
	}
}
