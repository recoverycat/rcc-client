import 	{
			Injectable,
			Inject,
		} 									from '@angular/core'

import	{
			firstValueFrom,
		}									from 'rxjs'

import	{
			cycleString,
			EncryptionHandler,
			GcmHandler,
			randomString,
			RccRtc,
			RccWebSocket,
			throwError,
			timeoutPromise,
			UserCancel,
			UserCanceledError
		}									from '@rcc/core'
import	{
			RccTransmission,
			AbstractTransmissionService
		}									from '@rcc/common'


import	{
			assertCmbTransmissionConfig,
			assertCmbTransmissionMeta,
			CMB_SIGNAL_SERVER,
			CMB_STUN_SERVERS,
			CmbTransmissionConfig,
			CmbTransmissionMeta,
			isCmbTransmissionMeta,
		}									from './combined-transmission.commons'


export class CombinedTransmission implements RccTransmission{

	/* STATIC */

	public static async createMeta() : Promise<unknown> {
		const key			:	string		= (await GcmHandler.generate())[0]
		const channel 		: 	string 		= randomString(20)

		return ['rcc-cmb', channel, key]
	}


	/* INSTANCE */

	protected data				: unknown
	protected url				: string
	protected channel			: string



	protected signalServer		: string
	protected stunServers		: string[]

	protected key				: string
	protected encryptionHandler : EncryptionHandler

	protected rccRtc			: RccRtc
	protected rccWebSocket		: RccWebSocket


	public ready				: Promise<void>

	public userCancel			: UserCancel
								= new UserCancel('CombinedTransmission: user canceled.')

	public constructor(config: CmbTransmissionConfig, meta?: CmbTransmissionMeta){

		assertCmbTransmissionConfig(config)

		this.data			=	config.data

		this.data			=	config.data
		this.signalServer	=	config.signalServer
		this.stunServers	=	config.stunServers

		this.ready	 		= 	meta
								?	Promise.resolve(this.setupWithExistingMeta(meta))
								:	this.setup()

	}



	public async setup() : Promise<void>{

		const meta	: unknown
					= await CombinedTransmission.createMeta()

		this.setupWithExistingMeta(meta)
	}


	public setupWithExistingMeta(meta: unknown) : void {

		assertCmbTransmissionMeta(meta)

		this.channel		= meta[1]
		this.key 			= meta[2]

		const encryptionHandler	: GcmHandler	= 	new GcmHandler(this.key)

		const url				: string		=	this.signalServer
		const stunServers		: string[]		=	this.stunServers
		const channel			: string		=	this.channel

		// Nothing special happening here; I just want the fallback channel name
		// to be different but derivable from the channel name, in a way that you cannot easily
		// distinguish regular channel names from fallback channel names.
		const fallbackChannel	: string	=	cycleString(channel)

		this.rccRtc			= new RccRtc({
								url,
								channel,
								encryptionHandler,
								stunServers
							})

		this.rccWebSocket =	new RccWebSocket({
								url,
								encryptionHandler,
								channel: 	fallbackChannel,
							})
	}



	public get meta() : CmbTransmissionMeta {
		return ['rcc-cmb', this.channel, this.key]
	}





	public async startRtcTransmission() : Promise<void> {

		await 	this.rccRtc.open({
					timeoutPeer: 		60*1000,
					timeoutConnection:	5 *1000
				})
		await this.rccRtc.send(this.data)
		await this.rccRtc.done()

		void this.rccRtc.close()
	}


	public async startWebsocketTransmission() : Promise<void> {

		await this.rccWebSocket.open()
		await this.rccWebSocket.send(this.data)
		await this.rccWebSocket.done()

		void this.rccWebSocket.close()
	}


	public async start(): Promise<void>{

		try{ await this.startRtcTransmission() }

		catch(e) {

			if( e instanceof UserCanceledError ) throwError(e)

			void this.rccRtc.close()
			if(!this.rccRtc.signalReliable)
				throwError('CombinedTransmission.start() failed due to WebSocket failure', e)

			await 	Promise.race([
						this.startWebsocketTransmission(),
						timeoutPromise(10000)
					]) // TODO shorter timeout?

		}
	}

	public async cancel() : Promise<void>{
		// TODO catch errors
		this.userCancel.cancel()
		await this.rccRtc.cancel()
		void this.rccWebSocket.close()
	}

}




@Injectable()
export class CombinedTransmissionService extends AbstractTransmissionService {

	public override id			: string = 'COMBINED_TRANSMISSION_SERVICE'
	public override label		: string = 'COMBINED_TRANSMISSION_SERVICE.LABEL'
	public override description	: string = 'COMBINED_TRANSMISSION_SERVICE.DESCRIPTION'

	public constructor(
		@Inject(CMB_STUN_SERVERS)
		protected stunServers	: string[],
		@Inject(CMB_SIGNAL_SERVER)
		protected signalServer	: string
	){
		super()
	}


	public validateMeta(meta:unknown): meta is CmbTransmissionMeta  {
		return isCmbTransmissionMeta(meta)
	}

	public async createMeta() : Promise<unknown> {
		return await CombinedTransmission.createMeta()
	}

	public async setup(data:unknown) : Promise<CombinedTransmission> {

		const stunServers	: string[]	= this.stunServers
		const signalServer 	: string	= this.signalServer

		const transmission	: CombinedTransmission
							= new CombinedTransmission({ data, signalServer, stunServers })

		await transmission.ready

		return transmission
	}

	/**
	 * This method does the same as {@link CombinedTransmissionService.setup()}
	 * and additionally initializes the returned {@link CombinedTransmission}
	 * with the provided meta data.
	 *
	 * _Note:_ Use with caution! Unlike
	 * {@link CombinedTransmissionService.setup()}
	 * the origin of the meta data is unknown to this method. So data could end
	 * up anywhere, if the meta data was not verified beforehand.

	 */
	public async setupWithExistingMeta(data: unknown, meta : CmbTransmissionMeta) : Promise<CombinedTransmission> {

		assertCmbTransmissionMeta(meta)

		const stunServers	: string[]	= this.stunServers
		const signalServer 	: string	= this.signalServer

		const transmission	: CombinedTransmission
							= new CombinedTransmission({ data, signalServer, stunServers }, meta)

		await transmission.ready

		return transmission
	}



	public async listen(meta: CmbTransmissionMeta) : Promise<unknown> {

		assertCmbTransmissionMeta(meta)

		const [channel, key]	: string[]		=	meta.slice(1)
		const fallbackChannel	: string		=	cycleString(channel) // see above
		const url				: string		=	this.signalServer
		const stunServers		: string[]		=	this.stunServers
		const encryptionHandler	: GcmHandler	=	new GcmHandler(key)


		const rccRtc			: RccRtc		= 	new RccRtc({
														url,
														channel,
														encryptionHandler,
														stunServers
													})
		const rccWebSocket 		: RccWebSocket	= 	new RccWebSocket({
														url,
														channel: fallbackChannel,
														encryptionHandler
													})

		try {
			return await this.listenOnRtc(rccRtc)
		} catch(e) {
			if(!rccRtc.signalReliable)
				throwError('RtcTransmissionService.listen(): failed due to signaling failure.')

			return await this.listenOnWebSocket(rccWebSocket)
		}
	}

	private async listenOnRtc(rccRtc: RccRtc) : Promise<unknown> {
		const data: Promise<unknown>	= 	firstValueFrom(rccRtc.data$)

		await 	rccRtc.open({
					timeoutPeer: 		60*1000,
					timeoutConnection:	5 *1000
				})

		const result: unknown	= 	await data

		await rccRtc.done()
			.then( () => rccRtc.close() )

		return result
	}

	private async listenOnWebSocket(rccWebSocket: RccWebSocket): Promise<unknown> {
		const data: Promise<unknown>		= 	firstValueFrom(rccWebSocket.data$)

		await rccWebSocket.open()

		const result: unknown	= 	await data

		await rccWebSocket.done()
			.then( () => rccWebSocket.close() )

		return result
	}
}
