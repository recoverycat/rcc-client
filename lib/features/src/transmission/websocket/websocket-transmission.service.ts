import 	{
			Injectable,
			Inject,
		} 									from '@angular/core'
import	{
			Subscription,
			firstValueFrom,
		}									from 'rxjs'

import	{
			EncryptionHandler,
			GcmHandler,
			RccWebSocket,
			randomString,
			assert
		}									from '@rcc/core'

import	{
			RccTransmission,
			AbstractTransmissionService,

		}									from '@rcc/common'

import	{
			WsTransmissionConfig,
			WsTransmissionMeta,
			isWsTransmissionMeta,
			assertWsTransmissionMeta,
			assertWsTransmissionConfig,
			WEBSOCKET_DEFAULT_URL
		}									from './websocket-transmission.commons'


// TODO needs rework or removal, see combined



export class WebSocketTransmission implements RccTransmission{

	/* STATIC */

	public static async createMeta() : Promise<unknown> {
		const key 			: string 	= (await GcmHandler.generate())[0]
		const channel		: string	= randomString(20)

		return ['rcc-wst', channel, key]
	}



	/* INSTANCE */



	public data					: unknown
	public channel				: string
	public url					: string
	public ready				: Promise<unknown>
	// public cancelled			: boolean = false

	private key					: string
	private rccWebSocket		: RccWebSocket
	private subscriptions		: Subscription[]
	private encryptionHandler	: EncryptionHandler

	public constructor(config: WsTransmissionConfig, meta?: WsTransmissionMeta) {

		assertWsTransmissionConfig(config)

		this.data		=	config.data
		this.url		=	config.url
		this.ready	 	= 	meta
							?	Promise.resolve(this.setupWithExistingMeta(meta))
							:	this.setup()

	}





	public async setup(): Promise<void>{

		const meta	: unknown
					= await WebSocketTransmission.createMeta()

		this.setupWithExistingMeta(meta)

	}

	public setupWithExistingMeta(meta: unknown) : void {

		assertWsTransmissionMeta(meta)

		this.channel 	= meta[1]
		this.key 		= meta[2]

		const encryptionHandler : GcmHandler 	= 	new GcmHandler(this.key)
		const url				: string		=	this.url
		const channel			: string		=	this.channel

		this.rccWebSocket 		=  	new RccWebSocket({
										url,
										channel,
										encryptionHandler,
									})
	}


	public get meta(): WsTransmissionMeta {

		return 	['rcc-wst', this.channel, this.key ]
	}


	public async start(): Promise<void> {


		await this.rccWebSocket.open()
		// at least one other party present

		await this.rccWebSocket.send(this.data)
		// got receipt

		await this.rccWebSocket.close()
		// done disconnecting
	}


	public async cancel(): Promise<void> {
	
		await this.rccWebSocket.close()
	}

}




@Injectable()
export class WebSocketTransmissionService extends AbstractTransmissionService {

	public override id			: string = 'WEBSOCKET_TRANSMISSION_SERVICE'
	public override label		: string = 'WEBSOCKET_TRANSMISSION_SERVICE.LABEL'
	public override description	: string = 'WEBSOCKET_TRANSMISSION_SERVICE.DESCRIPTION'

	public constructor(
		@Inject(WEBSOCKET_DEFAULT_URL)
		public defaultUrl	: string,
	){
		super()
		this.assertServiceConfiguration()
	}


	public assertServiceConfiguration(): void {
		assert(this.defaultUrl, 'WebsocketTransmissionService.setup(): missing defaultUrl. Please use WebsocketTransmissionModule.forRoot(url) to set defaultUrl.')

	}

	public validateMeta(data:unknown): boolean{
		return isWsTransmissionMeta(data)
	}

	public async createMeta() : Promise<unknown> {
		return await WebSocketTransmission.createMeta()
	}

	public async setup(data:unknown) : Promise<WebSocketTransmission> {

		const url			: string				= this.defaultUrl
		const transmission	: WebSocketTransmission	= new WebSocketTransmission({ data, url })

		await transmission.ready

		return transmission
	}


	/**
	 * This method does the same as {@link WebSocketTransmissionService.setup()}
	 * and additionally initializes the returned {@link WebSocketTransmission}
	 * with the provided meta data.
	 *
	 * _Note:_ Use with caution! Unlike
	 * {@link WebSocketTransmissionService.setup()}
	 * the origin of the meta data is unknown to this method. So data could end
	 * up anywhere, if the meta data was not verified beforehand.
	 */
	public async setupWithExistingMeta(data: unknown, meta: WsTransmissionMeta) : Promise<WebSocketTransmission> {

		assertWsTransmissionMeta(meta)

		const url			: string				= this.defaultUrl
		const transmission	: WebSocketTransmission	= new WebSocketTransmission({ data, url }, meta)

		await transmission.ready

		return transmission
	}



	public async listen(meta: WsTransmissionMeta) : Promise<unknown> {

		assertWsTransmissionMeta(meta)

		const [channel, key] 	: string[]			=	meta.slice(1)
		const encryptionHandler	: GcmHandler		=	new GcmHandler(key)
		const url				: string			=	this.defaultUrl
		const rccWebSocket		: RccWebSocket		= 	new RccWebSocket({ url, channel, encryptionHandler })
		const data				: Promise<unknown>	= 	firstValueFrom(rccWebSocket.data$)

		await rccWebSocket.open()

		const result			: unknown			= 	await data

		void rccWebSocket.close()

		return result
	}

}
