import 	{
			Component,
			OnInit,
			ViewChild,
		}								from '@angular/core'

import	{
			Location
		}								from '@angular/common'

import	{
			ActivatedRoute
		}								from '@angular/router'

import	{
			combineLatest,
			map,
			Observable
		}								from 'rxjs'

import	{
			Question,
			assert,
			uuidv4,
			UserCanceledError,
			QuestionConfig
		}								from '@rcc/core'

import	{
			RccToastController,
			ItemEditResult
		}								from '@rcc/common'

import	{	QuestionnaireService			}	from '../../questionnaire'
import	{	QuestionEditComponent	}	from '../../edit'
import	{
			CustomQuestionStoreService
		}								from '../custom-question-store.service'

@Component({
    templateUrl: './question-edit-page.component.html',
    standalone: false
})
export class CustomQuestionEditPageComponent implements OnInit {


	@ViewChild(QuestionEditComponent)
	public questionEditComponent	: QuestionEditComponent

	public questionEdit				: Question

	public mode						: string

	public constructor(
		public activatedRoute				: ActivatedRoute,
		public customQuestionStoreService	: CustomQuestionStoreService,
		public questionnaireService			: QuestionnaireService,
		public rccToastController			: RccToastController,
		public location						: Location
	){}


	public ngOnInit(): void {

		const observables: { id: Observable<string>, mode: Observable<string> } = {
								id:		this.activatedRoute.paramMap
										.pipe( map ( params		=> params.get('id') ) ),

								mode: 	this.activatedRoute.data
										.pipe( map ( ({ mode })	=> typeof mode === 'string' ? mode : undefined ) )
							}

		combineLatest(observables)
		.subscribe( ({ id, mode }) => void this.setup(id, mode) )

	}


	public async setup(id: string | undefined, mode: string): Promise<void> {

		assert(!this.questionEditComponent, 	'QuestionEditPageComponent.setup(): questionEditComponent was rendered before .setup(). Maybe the path has channged and the component got rerendered?')
		assert(['new', 'from'].includes(mode), 	`QuestionEditPageComponent.setup(): unknown mode; must be one of 'new', or 'from', got ${mode}.`, mode)

		this.mode 				= 	mode


		if( this.mode === 'new') return

		this.questionEdit 		= 	await this.questionnaireService.get(id)

		assert(this.questionEdit instanceof Question, `QuestionEditPageComponent.setup(): no matching question found ${id}`)

	}


	public get applyLabel() : string {
		if(this.mode === 'new') 	return 'SAVE'
		if(this.mode === 'from')	return 'SAVE'

		return 'APPLY'
	}

	public async onApply(result: ItemEditResult<Question>): Promise<void> {

		const question: Question		= result.item
		const config: QuestionConfig 	= question.config

		if(config.id) config.id = uuidv4() // making sure we add a copy of the question and won't overwrite the original one.

		try{		await 	this.customQuestionStoreService.addQuestionConfig(config) }
		catch(e){	void	this.rccToastController.failure('CUSTOM_QUESTION_STORE.EDIT.SAVE_FAILURE'); throw e }

		this.location.back()

		void this.rccToastController.success('CUSTOM_QUESTION_STORE.EDIT.SAVE_SUCCESS')

	}


	public onCancel(): void {

		this.location.back()

		throw new UserCanceledError()

	}

}
