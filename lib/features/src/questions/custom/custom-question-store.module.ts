import 	{
			NgModule,
		}											from '@angular/core'

import	{	RouterModule						}	from '@angular/router'

import	{	ReactiveFormsModule					}	from '@angular/forms'

import	{	Question							}	from '@rcc/core'
import 	{
			SharedModule,
			provideItemAction,
			ItemAction,
			provideTranslationMap,
		}											from '@rcc/common'

import	{	CustomQuestionEditPageComponent		}	from './edit-page/question-edit-page.component'
import	{	CustomQuestionStoreService					}	from './custom-question-store.service'
import	{	QuestionEditModule					}	from '../edit'
import	{	QuestionnaireServiceModule					}	from '../questionnaire'

import	de from './i18n/de.json'
import	en from './i18n/en.json'

const routes 		=	[
							{
								path: 'questionnaire/custom',
								children: [
									{
										path: 'new',
										component: CustomQuestionEditPageComponent,
										data: { mode:'new' }

									},
									{
										path: 'from/:id',
										component: CustomQuestionEditPageComponent,
										data: { mode:'from' }
									}
								]
							}
						]


const itemAction : ItemAction<Question> = 	{
							storeClass: CustomQuestionStoreService,
							itemClass:	Question,
							role:		'destructive' as const,
							getAction:	(question: Question, store: CustomQuestionStoreService) => ({
								label: 			'CUSTOM_QUESTION_STORE.ACTIONS.DELETE.LABEL',
								successMessage:	'CUSTOM_QUESTION_STORE.ACTIONS.DELETE.SUCCESS',
								failureMessage:	'CUSTOM_QUESTION_STORE.ACTIONS.DELETE.FAILURE',
								confirmMessage:	'CUSTOM_QUESTION_STORE.ACTIONS.DELETE.CONFIRM',
								handler: 	 	() => store.delete(question),
								icon:			'delete',
							})
						}


const metaActions	=	[
							{
								label:			'CUSTOM_QUESTION_STORE.ACTIONS.CREATE.LABEL',
								icon:			'new',
								path:			'questionnaire/custom/new',
								role:			'productive' as const
							}
						]


@NgModule({
	declarations: [
		CustomQuestionEditPageComponent
	],
	imports: [
		SharedModule,
		RouterModule.forChild(routes),
		QuestionEditModule,
		QuestionnaireServiceModule.forChild([CustomQuestionStoreService], metaActions),
		ReactiveFormsModule
	],
	exports: [
	],
	providers:[
		CustomQuestionStoreService,
		provideItemAction(itemAction),
		provideTranslationMap('CUSTOM_QUESTION_STORE', { de, en })
	]
})
export class CustomQuestionStoreServiceModule { }
