import 	{ 	Injectable } 		from '@angular/core'

import	{
			Question,
			QuestionStore,
			QuestionConfig
		}						from '@rcc/core'

import	{
			RccStorage
		}						from '@rcc/common'



@Injectable()
export class CustomQuestionStoreService extends QuestionStore {

	public readonly name = 'CUSTOM_QUESTION_STORE.NAME'

	public constructor(
		rccStorage:	RccStorage
	){
		super(
			rccStorage.createItemStorage('rcc-custom-questions'),
		)
	}

	public async addQuestionConfig(config: QuestionConfig): Promise<Question> {
		const question = this.addConfig(config)
		return 	this.storeAll()
				.then( () => question)
	}

	public async delete(question: Question): Promise<Question> {

		if(!this.removeItem(question)) throw new Error('CustomQuestionStoreService.delete: Unable to delete question with id: ' + question.id)


		return 	this.storeAll()
				.then( () => question )
	}
}
