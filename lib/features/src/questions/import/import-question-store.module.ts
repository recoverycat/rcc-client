import	{
			NgModule
		} 									from '@angular/core'

import	{	Question					}	from '@rcc/core'
import	{
			IncomingDataServiceModule,
			ItemAction,
			provideItemAction,
			provideTranslationMap
		}									from '@rcc/common'
import	{	ImportQuestionStoreService			}	from './import-question-store.service'
import	{	QuestionnaireServiceModule			}	from '../questionnaire'

import en from './i18n/en.json'
import de from './i18n/de.json'

const itemAction : ItemAction<Question> = 	{
								role:			'destructive' as const,
								itemClass:		Question,
								storeClass:		ImportQuestionStoreService,
								getAction:		(question: Question, store: ImportQuestionStoreService) => ({

									label: 			'IMPORT_QUESTION_STORE.ACTIONS.DELETE.LABEL',
									icon:			'delete',
									successMessage:	'IMPORT_QUESTION_STORE.ACTIONS.DELETE.SUCCESS',
									failureMessage:	'IMPORT_QUESTION_STORE.ACTIONS.DELETE.FAILURE',
									confirmMessage:	'IMPORT_QUESTION_STORE.ACTIONS.DELETE.CONFIRM',
									handler: 		() => store.deleteQuestion(question),


								})
							}



@NgModule({
	providers: [
		ImportQuestionStoreService,
		provideItemAction(itemAction),
		provideTranslationMap('IMPORT_QUESTION_STORE', { en,de })
	],
	imports: [
		QuestionnaireServiceModule.forChild([ImportQuestionStoreService]),
		IncomingDataServiceModule
	]
})
export class ImportQuestionStoreServiceModule {}
