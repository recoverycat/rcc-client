import 	{
			Pipe,
			PipeTransform,
		}								from '@angular/core'

import	{
			Question,
			QuestionOptionConfig
		}								from '@rcc/core'

import	{
			QuestionnaireService
		}								from './questionnaire.service'


@Pipe({
    name: 'id2Question',
    standalone: false
})
export class Id2QuestionPipe implements PipeTransform {

	public constructor(private questionnaireService: QuestionnaireService){}

	public transform(id: string): Promise<Question> {
		return this.questionnaireService.get(id)
	}
}

@Pipe({
    name: 'labeledOptions',
    standalone: false
})
export class LabeledOptionsPipe implements PipeTransform {

	public transform(question: Question): QuestionOptionConfig[] {
		if(!question.options) return []
		const options_with_labels : QuestionOptionConfig[] = question.options.filter( option => option.meaning || option.translations)
		if(options_with_labels.length === 0) return question.options

		return 	options_with_labels
	}
}


@Pipe({
    name: 'asAnswerTo',
    standalone: false
})
export class AsAnswerToPipe implements PipeTransform {


	public constructor(private questionnaireService: QuestionnaireService){}


	public async transform(answer: unknown, question: Question)	: Promise<unknown>
	public async transform(answer: unknown, id: string)			: Promise<unknown>
	public async transform(answer: unknown, q: Question|string)	: Promise<unknown> {

		const question : Question 	= 	q instanceof Question
									?	q
									:	await this.questionnaireService.get(q)


		if(question.options) return question.options.find( option => option.value === answer)

		return answer
	}
}
