import	{	InjectionToken 	}		from '@angular/core'
import 	{
			Question,
			QuestionConfig,
			QuestionStore,
		}							from '@rcc/core'

import	{
			MetaAction
		}							from '@rcc/common'



export const QUESTION_STORES 		: InjectionToken<QuestionStore>		= new InjectionToken<QuestionStore>('Question Stores')
export const QUESTION_META_ACTIONS	: InjectionToken<QuestionStore>		= new InjectionToken<MetaAction<QuestionConfig, Question, QuestionStore>>('metaAction, any action related to an item type.')
