import	{	Component				}	from '@angular/core'
import	{	Question				}	from '@rcc/core'
import	{	ExposeTemplateComponent	}	from '@rcc/common'

@Component({
    selector: 'rcc-question-label',
    templateUrl: './question-label.component.html',
    styleUrls: ['./question-label.component.scss'],
    standalone: false
})
export class QuestionLabelComponent  extends ExposeTemplateComponent{
	public constructor( public question: Question){ super() }
}
