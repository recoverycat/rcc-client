import 	{
			Component,
			OnDestroy,
			Optional
		}								from '@angular/core'

import	{
			FormControl,
			FormArray,
			FormGroup,
			AbstractControl
		}								from '@angular/forms'

import	{
			Subject,
			takeUntil,
			debounceTime,
			startWith
		}								from 'rxjs'

import	{
			QuestionConfig,
			AnswerType,
			uuidv4,
			QuestionOptionConfig
		}								from '@rcc/core'

import	{
			WidgetComponent,
			RccTranslationService
		}	                            from '@rcc/common'

import	{
			QuestionnaireService
		}								from '../../questionnaire'

import	{	QuestionEditControl		}	from '../question-edit-control.class'

import	{
			validateWording,
			validateAnswerType,
			validateOption,
			validateOptions,
			validateMinMax,
			ValidationOptions,
		}								from '../question-edit-validators'


export type OptionControl = FormGroup<{value: FormControl<string|number|boolean>, label: FormControl<string>}>

@Component({
    templateUrl: './generic-question-edit-widget.component.html',
    standalone: false
})
export class GenericQuestionEditWidgetComponent extends WidgetComponent<QuestionEditControl> implements OnDestroy {


	// STATIC

	public static label: string = 'QUESTIONS.EDIT.FALLBACK_WIDGET'

	public static controlType: typeof QuestionEditControl = QuestionEditControl

	/**
	 * Determines at which position in question type select menu the new question type shows up
	 * (the higher the value the earlier is the position in the select menu). If its return value
	 * is negative it will not show up in the menu at all. If the user selects the new question
	 * type, the question will be handed over to the widget, regardless of the return value of
	 * .widgetMatch(). If .widgetMatch() scores the highest of all widgets, the associated widget,
	 * will be at the first position in the select menu AND the question will be immediately handed
	 * to that widget, without further user interaction.
	 *
	 * @returns A value indicating how well the widget suits the question
	 */
	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	public static widgetMatch(questionEditControl: QuestionEditControl): number {

		// Can properly handle every question
		return 1
	}



	// INSTANCE


	public wordingControl: FormControl<string>			= new FormControl<string>('', validateWording)
	public answerTypes: Array<AnswerType>				= Array<AnswerType>('string', 'integer', 'decimal', 'boolean')
	public answerTypeControl: FormControl<AnswerType>	= new FormControl<AnswerType>('string', validateAnswerType(this.answerTypes))
	public unitControl: FormControl<string>				=	new FormControl<string>('')
	public tagControl: FormControl<string>				=	new FormControl<string>('')

	public limitationControl: FormControl<string>		=	new FormControl<string>('none')
	public limitationOptions: string[]					=	['none']

	public enableMinControl: FormControl<boolean>		=	new FormControl<boolean>(false)
	public minControl: FormControl<number>				=	new FormControl<number>(0)
	public enableMaxControl: FormControl<boolean>		=	new FormControl<boolean>(false)
	public maxControl: FormControl<number>				=	new FormControl<number>(3)
	// eslint-disable-next-line @typescript-eslint/typedef
	public minMaxControls			=	new FormGroup({
										enableMin:	this.enableMinControl,
										min: 		this.minControl,
										enableMax:	this.enableMaxControl,
										max: 		this.maxControl
									}, validateMinMax)

	public optionControls: FormArray<OptionControl>
								=	new FormArray<OptionControl>(
										[],
										validateOptions(this.answerTypeControl)
									)
	public optionValueType: string						=	'text'

	public knownTags: Set<string>			=	new Set<string>()
	public tags: Set<string>				=	new Set<string>()

	public allForms: FormArray				= 	new FormArray<AbstractControl>([
										this.wordingControl,
										this.answerTypeControl,
										this.optionControls,
										this.unitControl,
										this.tagControl,
										this.minMaxControls,
										this.limitationControl
									])

	protected destroy$: Subject<void>			=	new Subject<void>()

	public constructor(
		protected questionEditControl	: QuestionEditControl,
		protected rccTranslationService	: RccTranslationService,
		@Optional()
		protected questionnaireService	: QuestionnaireService = null
	){

		super(questionEditControl)

		void this.setup()
	}

	private async setup() : Promise<void> {

		this.updateInputsFromConfig()

		this.questionEditControl.editForms = this.allForms

		// Collect known tags:
		if(this.questionnaireService){
			await this.questionnaireService.ready

			this.questionnaireService.items
			.map( question => question.tags)
			.concat( this.questionEditControl.originalQuestionConfig?.tags)
			.flat()
			.forEach( tag => this.knownTags.add(tag) )
		}

		// answer type:

		this.answerTypeControl.valueChanges
		.pipe(
			takeUntil(this.destroy$),
			startWith(this.answerTypeControl.value)
		)
		.subscribe(answerType =>{

			const limitation: string 		=	this.limitationControl.value
			const isNumberType: boolean		= 	['decimal', 'integer'].includes(answerType)

			// unit
			if (isNumberType)	this.unitControl.enable()
			else				this.unitControl.disable()

			// limitations:
			this.limitationOptions 	= ['none']

			if(isNumberType) 				this.limitationOptions.push('minmax', 'options')
			if(answerType === 'string')		this.limitationOptions.push('options')
			if(answerType === 'boolean')	this.limitationOptions.push('options')

			if(!this.limitationOptions.includes(limitation)) this.limitationControl.setValue('none')

			// options
			this.optionValueType 	= 'text'

			if(isNumberType) 				this.optionValueType = 'number'
			if(answerType === 'boolean')	this.optionValueType = 'boolean'

			// trying to transform option values when answer type changes
			this.optionControls.controls.forEach( (optionControl, index) => {

				const value: string | number | boolean = optionControl.value.value

				let parsedValue	: string|number|boolean|undefined = undefined

				if(answerType === 'string')		parsedValue = 	String(value)
				if(answerType === 'decimal')	parsedValue = 	Number.isFinite(value) 		? value	: parseFloat(String(value))
				if(answerType === 'integer')	parsedValue = 	Number.isInteger(value) 	? value : parseInt(String(value), 10)
				if(answerType === 'boolean')	parsedValue = 	typeof value === 'boolean' 	? value : [true, false][index]


				optionControl.controls.value.setValue(parsedValue)
			})

			// type unknown:
			if(answerType === 'unknown'){
				this.wordingControl.disable()
				this.tagControl.disable()
			}else {
				this.wordingControl.enable()
				this.tagControl.enable()
			}
		})



		// limitations:

		this.limitationControl.valueChanges
		.pipe(
			takeUntil(this.destroy$),
			startWith(this.limitationControl.value)
		)
		.subscribe( limitation => {

			if(limitation === 'minmax'){
				this.enableMinControl.enable()
				this.enableMaxControl.enable()
			} else
				this.minMaxControls.disable()

			if (limitation === 'options')	this.optionControls.enable({ onlySelf: true })
			else							this.optionControls.disable()

			while(limitation === 'options' && this.optionControls.controls.length < 2) this.addOption()

		})


		// minimum
		this.enableMinControl.valueChanges
		.pipe(
			takeUntil(this.destroy$),
			startWith(this.enableMinControl.value)
		)
		.subscribe(
			enabled => 	enabled
						?	this.minControl.enable({ onlySelf: true })
						:	this.minControl.disable({ onlySelf: true })
		)



		// maximum
		this.enableMaxControl.valueChanges
		.pipe(
			takeUntil(this.destroy$),
			startWith(this.enableMaxControl.value)
		)
		.subscribe(
			enabled => 	enabled
						?	this.maxControl.enable({ onlySelf: true })
						:	this.maxControl.disable({ onlySelf: true })
		)





		// update question:

		this.allForms.valueChanges
		.pipe(
			takeUntil(this.destroy$),
			startWith(null),
			debounceTime(300)
		)
		.subscribe( ( ) => this.updateConfigFromInputs() )



	}

	protected addOption(data?: {value?:string|number|boolean, label?: string}, options?: ValidationOptions) : AbstractControl {

		if(!data && this.answerTypeControl.value === 'integer'){

			const values: (string | number | boolean)[]	= this.optionControls.value.map( option => option.value)
			// eslint-disable-next-line @typescript-eslint/typedef
			const allNumbers							= values.every( (v:unknown) : v is number => typeof v === 'number')
			const maxValue: number						= (allNumbers && values.length) ? Math.max(...values) : -1
			const nextValueGuess: number			 	= maxValue +1

			data = { value: nextValueGuess }

		}

		const optionControl: OptionControl 	= 	new FormGroup({
										value : new FormControl<string|number|boolean>(data?.value),
										label : new FormControl<string>(data?.label),
									}, validateOption(this.answerTypeControl, options) )

		this.optionControls.push(optionControl)

		return optionControl

	}

	protected removeOption(optionControl: OptionControl) : void {
		const pos: number = this.optionControls.controls.indexOf(optionControl)

		this.optionControls.removeAt(pos)
		this.optionControls.markAsTouched()
	}

	protected addTagFromControl(control: FormControl<string>) : void {

		const newTags: string[] = control.value
						.replace(/[^0-9a-zA-T-_,]/g,'').split(',')
						.filter( tag => !!tag)

		newTags.forEach( tag => this.tags.add(tag) )

		control.setValue('')

	}


	private updateInputsFromConfig() : void {

		const activeLanguage: string			= 	this.rccTranslationService.activeLanguage
		const editConfig: QuestionConfig		= 	this.questionEditControl.questionConfig
		const originalConfig: QuestionConfig	=	this.questionEditControl.originalQuestionConfig

		if(!editConfig && !originalConfig)	return		// If neither editConfig or originalConfig exist, we deal with a newly created question.

		const wording: string					= 	editConfig.translations[activeLanguage]
									||
									editConfig.meaning

		const min: number		=	editConfig.min
		const max: number		=	editConfig.max
		const unit: string		=	editConfig.unit

		// Tags must be taken from the original config, NOT the
		// currently in process config. That's because tags can
		// include values other QuestionEditWidget do not know about.
		// E.g. The scale widget adds 'rcc-scale' as a tag, then the
		// use switches to another QuestionEditWidget, that does not
		// know about the tag and thus cannot remove it; so the tag
		// stays forever just because the user skipped through different
		// QuestionEditWidgets.
		//
		// Alternatives:
		// (1) 	Whenever a new widget takes over, reload all original data
		//		Drawback: Edits get lost, when switching between widgets.
		//		Wording though should not reset.
		// (2)	Implement some cleanUp procedure.

		const tags: string[]					=	originalConfig?.tags

		const options: QuestionOptionConfig[]	=	editConfig.options

		const limitation: 'options' | 'minmax' | 'none'
				=	(options && options.length > 0)
									?	'options'
									:	(min !== undefined || max !== undefined)
									?	'minmax'
									:	'none'


		const answerTypeGuess: AnswerType =	this.answerTypes.includes(editConfig.type)
									?	editConfig.type
									:	this.answerTypes[0]


		this.answerTypeControl.setValue(answerTypeGuess)

		this.wordingControl.setValue(wording)
		this.enableMinControl.setValue(typeof min === 'number')
		this.minControl.setValue(min)
		this.enableMaxControl.setValue(typeof max === 'number')
		this.maxControl.setValue(max)
		this.unitControl.setValue(unit)

		this.limitationControl.setValue(limitation)

		this.tags				=	new Set(tags)

		this.optionControls.clear()

		options?.forEach( option => {

			const value: string | number | boolean = option.value
			const label: string = option.translations && option.translations[activeLanguage]
							||
							option.meaning

			this.addOption({ value, label })
		})

	}

	protected getConfigFromInputs() : QuestionConfig {

		const questionConfig: QuestionConfig		=	this.questionEditControl.questionConfig

		const activeLanguage: string				= 	this.rccTranslationService.activeLanguage

		const meaning: string						=	this.wordingControl.value
		const translations: Record<string, string>	=	{ [activeLanguage]:  meaning }
		const id: string							=	questionConfig?.id || uuidv4()		// TODO: Find a better way to assign IDs to new questions.
		const type: AnswerType						=	this.answerTypeControl.value

		const unit: string							=	this.unitControl.enabled
														?	this.unitControl.value
														:	undefined

		const tags: string[]						=	this.tagControl.enabled
														?	Array.from(this.tags)
														:	[...questionConfig?.tags || [] ]

		const min: number	 						=		this.minMaxControls.enabled
														&& 	this.enableMinControl.value
														?	this.minControl.value
														:	undefined

		const max: number	 						=		this.minMaxControls.enabled
														&& 	this.enableMaxControl.value
														?	this.maxControl.value
														:	undefined

		const options: Array<{ meaning: string; translations: Record<string, string>; value: string | number | boolean }>
													=	this.optionControls.enabled
														?	this.optionControls.value.map( ({ value, label }) => ({
																meaning: 		label || undefined,
																translations: 	label ? { [activeLanguage]:  label } : undefined,
																value
															}))
														:	undefined

		const config : QuestionConfig	= 	{ id, type, meaning, translations, tags, options, unit, min, max }

		return config

	}

	private updateConfigFromInputs() : void {

		const config: QuestionConfig = this.getConfigFromInputs()

		this.questionEditControl.questionConfig = config

	}

	public ngOnDestroy() : void {

		this.destroy$.next()
		this.destroy$.complete()

	}

	protected setAnswerTypeAsSelectedOrDefault(): void {
		const previouslySelectedAnswerType: AnswerType = this.answerTypeControl.value
		const previouslySelectedAnswerTypeAvailable: boolean = this.answerTypes.includes(previouslySelectedAnswerType)

		if (previouslySelectedAnswerTypeAvailable)
			return


		this.answerTypeControl.setValue(this.answerTypes[0])
	}

}
