import	{	NgModule							} 	from '@angular/core'
import	{
			provideWidget,
			SharedModule
		}											from '@rcc/common'
import	{	GenericQuestionEditWidgetComponent	}	from './generic-question-edit-widget.component'


@NgModule({
	imports:[
		SharedModule
	],
	providers:[
		provideWidget(GenericQuestionEditWidgetComponent),
	],
	declarations:[
		GenericQuestionEditWidgetComponent
	]
})
export class FallbackQuestionEditWidgetsModule {}
