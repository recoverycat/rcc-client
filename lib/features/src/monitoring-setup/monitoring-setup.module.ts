import	{
			ModuleWithProviders,
			NgModule
		}										from '@angular/core'

import 	{
			RouterModule,
			Routes,
		}										from '@angular/router'

import	{
			provideTranslationMap,
			SharedModule,
		}										from '@rcc/common'

import	{
			RccMonitoringSetupPageComponent
		}										from './monitoring-setup-page.component'
import	{
			MONITORING_SETUP_CONFIG,
			MonitoringSetupConfig,
		}										from '../questionnaire-editor/questionnaire-editor.service'
import 	{
			canDeactivateMonitoringSetupFn
		} 										from './can-deactivate-monitoring-setup-fn'
import	{	QuestionnaireEditorModule		}	from '../questionnaire-editor/questionnaire-editor.module'

import en from './i18n/en.json'
import de from './i18n/de.json'

export const MONITORING_SETUP_PATH: string = 'monitoring-setup'

const routes: Routes	=	[
	{
		path: 		MONITORING_SETUP_PATH,
		title:		'MONITORING_SETUP.PAGE_TITLE',
		component:	RccMonitoringSetupPageComponent,
		canDeactivate: [canDeactivateMonitoringSetupFn]
	},
]

@NgModule({
	providers: [
		provideTranslationMap('MONITORING_SETUP', { en, de }),
	],
	declarations: [
		RccMonitoringSetupPageComponent,
	],

	imports: [
		RouterModule.forChild(routes),
		SharedModule,
		QuestionnaireEditorModule,
	]

})
export class RccMonitoringSetupModule {

	public static forRoot(config: Partial<MonitoringSetupConfig>): ModuleWithProviders<RccMonitoringSetupModule> {
		return {
			ngModule: RccMonitoringSetupModule,
			providers: [{
				provide: MONITORING_SETUP_CONFIG,
				useValue: {
					questionCategories: [...(config.questionCategories ?? [])],
					hiddenOnEditCategories: [...(config.hiddenOnEditCategories ?? [])],
					hiddenInCatalogue: [...(config.hiddenInCatalogue ?? [])],
					defaultQuestionsByCategory: config.defaultQuestionsByCategory ? new Map([...config.defaultQuestionsByCategory]) : new Map([]),
					addTemplateQuestionsTo: config.addTemplateQuestionsTo ?? config.questionCategories?.length ? config.questionCategories[0] : null,
					defaultScheduleConfig: config.defaultScheduleConfig ?? [[0,1,2,3,4,5,6], []],
					catalogRestrictionsByCategory: config.catalogRestrictionsByCategory ?? {},
				} as MonitoringSetupConfig,
				multi: false
			}]
		}
	}
}
