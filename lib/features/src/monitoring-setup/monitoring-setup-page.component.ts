import	{
			Component,
			OnInit,
		}									from '@angular/core'
import	{
			RccTitleService,
			RccTranslationService,
			ShareDataService,
		}									from '@rcc/common'
import	{	ConfirmPayload				}	from '../questionnaire-editor/questionnaire-editor/questionnaire-editor.component'

@Component({
    selector: 'rcc-monitoring-setup-page',
    templateUrl: './monitoring-setup-page.component.html',
    styleUrls: ['./monitoring-setup-page.component.scss'],
    standalone: false
})
export class RccMonitoringSetupPageComponent implements OnInit {

	private questionnaireLabel		: string
	private questionnaireSuffix		: string 				= 'MONITORING_SETUP.COPY'
	protected editName				: boolean 				= false
	protected initialName			: string

	public constructor (
		private readonly rccTitleService				: RccTitleService,
		private readonly shareDataService				: ShareDataService,
		private readonly rccTranslationService			: RccTranslationService
	) {}

	public ngOnInit(): void {
		this.rccTitleService.setTitleSuffix('MONITORING_SETUP.PAGE_TITLE')
	}

	public  handleMetaLabelChange(label: string): void {
		this.questionnaireLabel = label
		this.updateQuestionnaireName()
	}

	private updateQuestionnaireName(): void {
		this.initialName = `${this.questionnaireLabel || ''} ${this.rccTranslationService.translate(this.questionnaireSuffix)}`
	}

	protected async shareFollowUp(event: ConfirmPayload): Promise<void> {
		await this.shareDataService.share([event.config, event.questions], 'send')
	}
}
