import { TestBed } from '@angular/core/testing'
import { RccPreConfiguredSymptomCheckService } from './pre-configured-symptom-checks.service'
import { IncomingDataService, IncomingDataServiceModule, RccPublicRuntimeConfigService, RccToastController } from '@rcc/common'
import { lastValueFrom, Observable } from 'rxjs'
import { QuestionnaireService } from '../questions'
import { Question, SymptomCheckConfig } from '@rcc/core'

describe('RccPreConfiguredSymptomCheckService', () => {
	let service: RccPreConfiguredSymptomCheckService = undefined!
	let incomingDataService: IncomingDataService = undefined!
	let mockRccToastController : RccToastController = undefined!

	const mockRuntimeConfigService: Partial<RccPublicRuntimeConfigService> = {
		get: (): Promise<object> => Promise.resolve({})
	}

	beforeEach(() => {

		mockRccToastController = jasmine.createSpyObj('RccToastController', ['present', 'success', 'failure', 'info']) as RccToastController

		const mockQuestionnaireService: QuestionnaireService = {
			items: [
				{ id: 'question1' } as Question,
				{ id: 'question2' } as Question,
			],
		} as QuestionnaireService

		TestBed.configureTestingModule({
			imports: [
				IncomingDataServiceModule
			],
			providers: [
				RccPreConfiguredSymptomCheckService,
				{ provide: RccToastController, useValue: mockRccToastController },
				{ provide: RccPublicRuntimeConfigService, useValue: mockRuntimeConfigService },
				{ provide: QuestionnaireService, useValue: mockQuestionnaireService },
			]
		})

		service = TestBed.inject(RccPreConfiguredSymptomCheckService)
		incomingDataService = TestBed.inject(IncomingDataService)

	})

	it('should call handlePreConfiguredSymptomCheck() when valid data is present', () => {
		const spy: jasmine.Spy = spyOn(service, 'handlePreConfiguredSymptomCheck')
		incomingDataService.next({ source: 'link', data: { preConfiguredSymptomCheck: 'validSymptomCheck' } })

		expect(spy).toHaveBeenCalledWith('validSymptomCheck')
	})

	it('should not call handlePreConfiguredSymptomCheck() when data is not present', () => {
		const spy : jasmine.Spy = spyOn(service, 'handlePreConfiguredSymptomCheck')
		incomingDataService.next({ source: 'link' })

		expect(spy).not.toHaveBeenCalled()
	})

	it('should not call handlePreConfiguredSymptomCheck() when data is null', () => {
		const spy: jasmine.Spy = spyOn(service, 'handlePreConfiguredSymptomCheck')
		incomingDataService.next({ source: 'link', data: null })

		expect(spy).not.toHaveBeenCalled()
	})

	it('should not call handlePreConfiguredSymptomCheck() when data is undefined', () => {
		const spy: jasmine.Spy = spyOn(service, 'handlePreConfiguredSymptomCheck')
		incomingDataService.next({ source: 'link', data: undefined })

		expect(spy).not.toHaveBeenCalled()
	})

	it('should not call handlePreConfiguredSymptomCheck() when data is an empty object', () => {
		const spy: jasmine.Spy = spyOn(service, 'handlePreConfiguredSymptomCheck')
		incomingDataService.next({ source: 'link', data: {} })

		expect(spy).not.toHaveBeenCalled()
	})

	it('should not call handlePreConfiguredSymptomCheck() when data does not contain preConfiguredSymptomCheck', () => {
		const spy: jasmine.Spy = spyOn(service, 'handlePreConfiguredSymptomCheck')
		incomingDataService.next({ source: 'link', data: { someOtherKey: 'value' } })

		expect(spy).not.toHaveBeenCalled()
	})


	it('should filter data by source', () => {
		expect(service.assertSource({ source: 'link' })).toBeTrue()
		expect(service.assertSource({ source: 'other' })).toBeFalse()
		expect(service.assertSource({})).toBeFalse()
	})


	it('should filter data by data field', () => {
		expect(service.assertDataField({ data: {} })).toBeTrue()
		expect(service.assertDataField({})).toBeFalse()
	})

	it('should extract data field', () => {
		expect(service.extractDataField({ data: 'test' })).toBe('test')
		expect(service.extractDataField({})).toBeFalse()
	})

	it('should extract symptom check', () => {
		expect(service.extractSymptomCheck({ preConfiguredSymptomCheck: 'test' })).toBe('test')
		expect(service.extractSymptomCheck({})).toBeFalse()
	})

	it('should assert key', () => {
		expect(service.assertKey({ preConfiguredSymptomCheck: 'test' })).toBeTrue()
		expect(service.assertKey({})).toBeFalse()
	})

	it('should assert value', () => {
		expect(service.assertValue('test')).toBe('test')
		expect(service.assertValue(123)).toBe('')
	})

	it('should throw an error if a question is not found in the question store', async () => {
		const config: SymptomCheckConfig = {
			meta: {},
			questions: ['question1', 'non-existing-question'],
		}
  
		await expectAsync(service.assertQuestionsInStore(config)).toBeRejectedWith(
			new Error('RccPreConfiguredSymptomCheckService.handlePreConfiguredSymptomCheck(): can\'t find question in store: non-existing-question'),
		)

		// eslint-disable-next-line @typescript-eslint/unbound-method
		expect(mockRccToastController.failure).toHaveBeenCalledWith(
			'PRE_CONFIGURED_SYMPTOM_CHECK.ERROR_MESSAGES.MISSING_QUESTIONS',
		)
	})

	it('should not throw an error if all questions are found in the question store', async () => {
		const config: SymptomCheckConfig = {
			meta: {},
			questions: ['question1', 'question2'],
		}
  
		await expectAsync(service.assertQuestionsInStore(config)).toBeResolved()
  
		// eslint-disable-next-line @typescript-eslint/unbound-method
		expect(mockRccToastController.failure).not.toHaveBeenCalled()
	})

	it('should handle error', async () => {
		const result: Observable<null>  = service.handleError('error')
		const resolvedValue : null | undefined = await lastValueFrom(result)

		expect(resolvedValue).toBeNull()
	})

})
