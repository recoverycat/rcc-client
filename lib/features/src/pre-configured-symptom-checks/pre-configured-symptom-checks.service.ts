import	{	Injectable					}		from	'@angular/core'
import	{
			RccPublicRuntimeConfigService,
			IncomingDataService,
			RccToastController
		}										from	'@rcc/common'
import	{
			assertSymptomCheckConfig,
			has,
			QuestionRelationDto,
			SymptomCheckConfig,
		} 										from '@rcc/core'
import	{
			filter,
			map,
			Observable,
			of,
		} 										from 'rxjs'
import	{	QuestionnaireService		}		from '../questions'


/**
 * `RccPreConfiguredSymptomCheckService` is a service that
 * listens for incoming data, filters and maps the data, and then
 * handles pre-configured symptom checks based on the data received.
 * It retrieves pre-configured symptom checks from the runtime
 * configuration service, validates the configuration, and then sends
 * the symptom check configuration to the incomingDataService for
 * further processing. If the specified symptom check is not found in
 * the pre-configured list, it throws an error.
 */
@Injectable()
export class RccPreConfiguredSymptomCheckService {

	public constructor(
		private rccPublicRuntimeConfigService: RccPublicRuntimeConfigService,
		private incomingDataService: IncomingDataService,
		private rccToastController: RccToastController,
		private questionnaireService: QuestionnaireService,
	) {
		this.setup()
	}

	public setup(): void {
		this.incomingDataService
			.pipe(
				filter(data => this.assertSource(data)),
				filter(data => this.assertDataField(data)),
				map(data => this.extractDataField(data)),
				filter(data => this.assertKey(data)),
				map(data => this.extractSymptomCheck(data)),
				map(data => this.assertValue(data)),
			)
			.subscribe({
				next: name => void this.handlePreConfiguredSymptomCheck(name),
				error: err => this.handleError(err)
			})
	}

	/**
	 * Handles the pre-configured symptom check based on the provided name.
	 * It retrieves the pre-configured symptom checks from the runtime configuration service,
	 * validates the configuration, and sends the symptom check configuration to the incomingDataService for further processing.
	 * If the specified symptom check is not found in the pre-configured list, it throws an error.
	 *
	 * @param name - The name of the pre-configured symptom check to handle.
	 * @returns A promise that resolves when the symptom check configuration is successfully processed.
	 * @throws An error if the symptom check configuration cannot be retrieved from the runtime configuration.
	 */
	public async handlePreConfiguredSymptomCheck(name: string): Promise<void> {
		const preConfiguredSymptomChecks: unknown = await this.rccPublicRuntimeConfigService.get('preConfiguredSymptomChecks')

		if (has(preConfiguredSymptomChecks, name)) {
			const symptomCheckConfig: unknown = preConfiguredSymptomChecks[name]
			assertSymptomCheckConfig(symptomCheckConfig)
			await this.assertQuestionsInStore(symptomCheckConfig)
			this.incomingDataService.next(symptomCheckConfig)
		} else {
			void this.rccToastController.failure('PRE_CONFIGURED_SYMPTOM_CHECK.ERROR_MESSAGES.UNABLE_TO_RETRIEVE_SYMPTOM_CHECK_CONFIG')
			throw Error('RccPreConfiguredSymptomCheckService.handlePreConfiguredSymptomCheck(): unable to retrieve SymptomCheckConfig from RuntimeConfig')
		}
	}

	public async assertQuestionsInStore(config: SymptomCheckConfig): Promise<void> {
		await this.questionnaireService.ready
		const storedQuestionIds : string[] = this.questionnaireService.items.map(q => q.id)

		config.questions.forEach((q : string | QuestionRelationDto) => {
			const id : string = typeof q === 'string' ? q : q?.id
			if (!storedQuestionIds.includes(id)) {
				void this.rccToastController.failure('PRE_CONFIGURED_SYMPTOM_CHECK.ERROR_MESSAGES.MISSING_QUESTIONS')
				throw Error('RccPreConfiguredSymptomCheckService.handlePreConfiguredSymptomCheck(): can\'t find question in store: ' + id)
			}
		})
	}

	/**
	 * This assertion function is a bit different from the other assert functions as to logging and error reports.
	 * The main issue is that the incomingDataService emits different types of data, which at some point is not what we are expecting, or
	 * with which we can work with. That is the reason why we are not logging or showing the toast.
	 * For debugging, purposes, you might want to add a log statement.
	 * @param data Represents {"data":{"preConfiguredSymptomCheck":"name_of_a_preconfigured_symptom_check"},"source":"link"}
	 */
	public assertSource(data: unknown): boolean {
		if (!has(data, 'source')) return false
		if (data.source !== 'link') return false

		return true
	}

	public assertDataField(data: unknown): boolean {
		if (!has(data, 'data') || !data.data) {
			console.error('Invalid data field:', data)
			void this.rccToastController.failure('PRE_CONFIGURED_SYMPTOM_CHECK.ERROR_MESSAGES.INVALID_DATA_FIELD')
			return false
		}
		return true
	}

	/**
	 * Extracts the respective 'data' and 'preConfiguredSymptomCheck' fields from the provided object if it exists.
	 *
	 * @param data - The object from which to extract the 'data' or 'preConfiguredSymptomCheck' field. This object is expected to have a 'data' or 'preConfiguredSymptomCheck' property.
	 * @returns The value of the 'data' or 'preConfiguredSymptomCheck' field if it exists, otherwise `undefined`.
	 */
	public extractDataField(data: unknown): unknown {
		return has(data, 'data') && data.data
	}

	public extractSymptomCheck(data: unknown): unknown {
		return has(data, 'preConfiguredSymptomCheck') && data.preConfiguredSymptomCheck
	}

	public assertKey(data: unknown): boolean {
		if (!has(data, 'preConfiguredSymptomCheck')) {
			console.error('Missing preConfiguredSymptomCheck field:', data)
			void this.rccToastController.failure('PRE_CONFIGURED_SYMPTOM_CHECK.ERROR_MESSAGES.MISSING_PRECONFIGURED_SYMPTOM_CHECK_FIELD')
			return false
		}
		return true
	}

	public assertValue(name: unknown): string {
		if (typeof name !== 'string') {
			console.error('Invalid preConfiguredSymptomCheck value:', name)
			void this.rccToastController.failure('PRE_CONFIGURED_SYMPTOM_CHECK.ERROR_MESSAGES.INVALID_PRECONFIGURED_SYMPTOM_CHECK_VALUE')
			return ''
		}
		return name
	}

	public handleError(err: unknown): Observable<null> {
		console.error('Error processing incoming data:', err)
		void this.rccToastController.failure('PRE_CONFIGURED_SYMPTOM_CHECK.ERROR_MESSAGES.UNABLE_TO_RETRIEVE_SYMPTOM_CHECK_CONFIG')
		return of(null) // Return a null observable to continue the stream
	}

}
