import	{	NgModule								}	from	'@angular/core'
import	{
			IncomingDataServiceModule,
			provideTranslationMap,
			RccPublicRuntimeConfigModule,
			requestPublicRuntimeConfigValue
		}												from	'@rcc/common'
import	{	SymptomCheckConfig						}	from	'@rcc/core'
import	{	RccPreConfiguredSymptomCheckService		}	from	'./pre-configured-symptom-checks.service'

import en from './i18n/en.json'
import de from './i18n/de.json'

export interface SymptomCheckDictionary {
	[index: string]: SymptomCheckConfig
}
@NgModule({
	imports: [
		IncomingDataServiceModule,
		RccPublicRuntimeConfigModule,
	],
	providers: [
		provideTranslationMap('PRE_CONFIGURED_SYMPTOM_CHECK', { en, de }),
		RccPreConfiguredSymptomCheckService,

		/* The `requestPublicRuntimeConfigValue` function is being used to request a
		specific value from the public runtime configuration. In this case, it is
		requesting a value located at the path 'preConfiguredSymptomChecks'. */
		requestPublicRuntimeConfigValue({
			path: 'preConfiguredSymptomChecks',
			description: 'A dictionary of symptom checks, that can be used by the app right from the start.',
			type: 'SymptomCheckDictionary',
			required: false
		})
	]
})
export class PreConfiguredSymptomChecksModule {
	public constructor(
		private readonly service: RccPreConfiguredSymptomCheckService,
	){}
}
