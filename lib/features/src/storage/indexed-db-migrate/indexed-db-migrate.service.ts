import 	{ 	Injectable 			} 	from '@angular/core'

import	{
			Item,
			ItemStorage,
			ConfigOf
		}							from '@rcc/core'

import	{
			RccStorage
		}							from '@rcc/common'

import	{
			IndexedDbService
		}							from '../indexed-db'

import	{
			LocalStorageService
		}							from '../local-storage'


@Injectable()
export class IndexedDbMigrateService extends RccStorage {

	protected localStorageService:	LocalStorageService
	protected indexedDbService:		IndexedDbService

	public constructor(
	) {
		super()

		this.localStorageService 	= new LocalStorageService()
		this.indexedDbService		= new IndexedDbService()

	}

	public createItemStorage<I extends Item<any>, C extends ConfigOf<I> = ConfigOf<I> >(id:string): ItemStorage<I> {

		const localStorageItemStorage 	= this.localStorageService.createItemStorage<I>(id)
		const indexedDbItemStorage		= this.indexedDbService.createItemStorage<I>(id)


		const store		=	(items: (C|I)[]): Promise<void> => indexedDbItemStorage.store(items)
		const getAll 	= 	async(): Promise<C[]> => {
								const ls_item_configs 	= await localStorageItemStorage.getAll()
								const idb_item_configs	= await indexedDbItemStorage.getAll()

								// ls_item_configs and idb_item_configs should not be populated at the same time.
								// if they do however we should not just disregard one of them,
								// so we accept possible duplicates:
								const item_configs		= [...ls_item_configs, ...idb_item_configs]


								await indexedDbItemStorage.store(item_configs)
								await this.localStorageService.clearItemStorage(id)

								return item_configs
							}


		return	{ getAll, store }

	}

	public async clearItemStorage(id: string) : Promise<void> {
		return await this.indexedDbService.clearItemStorage(id)
	}

	public async getStorageNames() : Promise<string[]> {
		return await this.indexedDbService.getStorageNames()
	}

}
