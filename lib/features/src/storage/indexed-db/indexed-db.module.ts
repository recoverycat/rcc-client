import 	{	NgModule 				} 	from '@angular/core'
import	{
			DevModule,
			StorageProviderModule,
		}								from '@rcc/common'

import	{
			IndexedDbService
		}								from './indexed-db.service'


@NgModule({
	providers:[
		IndexedDbService
	],
	imports: [
		StorageProviderModule.forRoot(IndexedDbService),
		DevModule.note('IndexedDbModule'),
	],
})
export class IndexedDbModule{

}
