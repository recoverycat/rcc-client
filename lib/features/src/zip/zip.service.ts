import { R4, Report, zip } from '@rcc/core'

export class ZipService {
	public async createZipOfReport(report: Report | R4.IQuestionnaireResponse, filename: string, password?: string): Promise<Blob> {
		if (!password) password = ''

		const reportAsString : string = JSON.stringify(report, null, 2)

		return await zip(reportAsString, filename, password)
	}
}
