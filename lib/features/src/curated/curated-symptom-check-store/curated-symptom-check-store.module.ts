import	{	NgModule 						}		from '@angular/core'
import	{
			provideTranslationMap,
		}											from '@rcc/common'

import	{
			SymptomCheckMetaStoreServiceModule,
		}											from '../../symptom-checks/meta-store'
import	{	CuratedDefaultQuestionStoreService	}	from '../curated-questions/curated-default-questions'
import	{	CuratedSymptomCheckStoreService		}	from './curated-symptom-check-store.service'


import en from './i18n/en.json'
import de from './i18n/de.json'
import { StarterQuestionStoreService } from '../curated-symptom-checks/starter'

@NgModule({
	imports: [
		SymptomCheckMetaStoreServiceModule.forChild([CuratedSymptomCheckStoreService]),
	],
	providers: [
		CuratedDefaultQuestionStoreService,
		StarterQuestionStoreService,
		CuratedSymptomCheckStoreService,
		provideTranslationMap('CURATED', { en, de })
	]
})
export class CuratedSymptomCheckStoreServiceModule{}
