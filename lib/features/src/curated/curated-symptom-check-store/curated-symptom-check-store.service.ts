import 	{ 	Injectable 							}	from '@angular/core'

import	{
			RccTranslationService,
		}											from '@rcc/common'

import	{
			SymptomCheckStore,
			SymptomCheckConfig,
			QuestionStore,
		}											from '@rcc/core'

import	{	QuestionnaireService				}	from 	'../../questions/questionnaire'
import { CuratedDefaultQuestionStoreService } from '../curated-questions/curated-default-questions'


// TODO: Switch Translation Keys QUESTIONS.CURATED to CURATED.QUESTIONS  + move curated questions to features/src/curated

const initialCategories: string[] = 	[
	'rcc-category-empty',			// there is no question with this tag, thus the resulting symptom check will have no questions
	'rcc-category-depression',
	'rcc-category-bipolar-disorder',
	'rcc-category-psychosis',
	'rcc-category-schizoaffective-disorders'
]

@Injectable()
export class CuratedSymptomCheckStoreService extends SymptomCheckStore {

	public readonly name: string = 'CURATED.SYMPTOM_CHECKS.STORE_NAME'

	public constructor(
		private rccTranslationService		: RccTranslationService,
		private questionnaireService		: QuestionnaireService,
		private curatedDefaultQuestionStoreService: CuratedDefaultQuestionStoreService
	){
		super()
		initialCategories.forEach(category => void this.createSymptomCheckByCategory(category, curatedDefaultQuestionStoreService))
	}

	private setCategory(category: string): string {
		const categoryTranslationKey	: string
										= `CURATED.QUESTIONS.CATEGORIES.${category.toUpperCase()}`
		let categoryTranslation			: string
										= this.rccTranslationService.translate(categoryTranslationKey)

		if (category !== 'rcc-category-empty') categoryTranslation = `RC ${categoryTranslation}`

		return categoryTranslation
	}

	public async createSymptomCheckByCategory(category: string, store: QuestionStore | QuestionnaireService = this.questionnaireService): Promise<SymptomCheckConfig> {

		await this.ready

		let symptomCheckConfig: SymptomCheckConfig = undefined

			await store.ready

		const categoryTranslation	:	string
									=	this.setCategory(category)

		const label					:	string
									=	this.rccTranslationService.translate('CURATED.SYMPTOM_CHECKS.TEMPLATE.LABEL', {
										category: categoryTranslation })

		const questions				:	string[]
									=	store.items
										.filter(question => {

											const matches : boolean = question.tags?.includes(category)
											return matches

										})
										.map(q => q.id)


		symptomCheckConfig 			= 	{
											meta: { label },
											questions
										}

		this.addConfig(symptomCheckConfig)

		return symptomCheckConfig
	}
}
