import	{	NgModule 							}	from '@angular/core'
import	{
			SymptomCheckMetaStoreServiceModule,
		}											from '../../symptom-checks/meta-store'
import	{	StarterSymptomCheckStoreService		}	from './starter-symptom-check-store.service'
import	{
			StarterQuestionStoreService,
			StarterQuestionStoreServiceModule,
		}											from '../curated-symptom-checks/starter'
/**
 * This module provides services for the StarterSymptomCheckStoreService for the
 * PAT version of the app. It is therefore only loaded here with the
 * StarterQuestionStoreService and it circumvents the
 * loading of the CuratedDefaultQuestionStoreService, which would result in the seven
 * additional default question in the PAT query runner right from the start of the app.
 *
 * UPDATE 12.09.24: The above problem doesn't seem to occur anymore,
 * loading CuratedDefaultQuestionStoreServiceModule in the features module
 * does not automatically add any questions to the query runner.
 */

@NgModule({
	imports: [
		SymptomCheckMetaStoreServiceModule.forChild([StarterSymptomCheckStoreService]),
		StarterQuestionStoreServiceModule
	],
	providers: [
		StarterQuestionStoreService,
		StarterSymptomCheckStoreService,
	]
})
export class StarterSymptomCheckStoreServiceModule{}
