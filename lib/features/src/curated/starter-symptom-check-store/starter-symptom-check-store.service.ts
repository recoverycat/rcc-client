import 	{ 	Injectable 			}	from '@angular/core'
import	{	SymptomCheckStore	}	from '@rcc/core'

@Injectable()
export class StarterSymptomCheckStoreService extends SymptomCheckStore {

	public readonly name: string = 'CURATED.SYMPTOM_CHECKS.STORE_NAME'

	public constructor(){
		super()
	}

}
