import	{	NgModule	}								from	'@angular/core'

import	{
			provideItemSelectionFilter
		}												from	'@rcc/common'

import	{	QuestionnaireServiceModule			}		from 	'../../../questions/questionnaire'

import	{	CuratedSymptomCheckStoreService		}		from	'../../curated-symptom-check-store'

import	{

			diaryCardsQuestionCategory,
			diaryCardsQuestionCategoryFilter,
			CuratedDiaryCardsQuestionStoreService
		}												from	'./curated-diary-cards.commons'

@NgModule({
	imports:[
		QuestionnaireServiceModule.forChild([CuratedDiaryCardsQuestionStoreService])
	],
	providers: [
		CuratedDiaryCardsQuestionStoreService,
		provideItemSelectionFilter(diaryCardsQuestionCategoryFilter)
	]
})
export class CuratedDiaryCardsSymptomCheckStoreServiceModule {
	public constructor(
		private curatedDiaryCardsQuestionStoreService	: CuratedDiaryCardsQuestionStoreService,
		private curatedSymptomCheckStoreService			: CuratedSymptomCheckStoreService,
	) {
		void this.setup()
	}

	private async setup() : Promise<void> {
		await this.curatedDiaryCardsQuestionStoreService.ready
		await this.curatedSymptomCheckStoreService.createSymptomCheckByCategory(diaryCardsQuestionCategory)
	}
}
