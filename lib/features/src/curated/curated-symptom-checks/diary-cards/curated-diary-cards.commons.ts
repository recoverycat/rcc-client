import	{
			Injectable
		}								from '@angular/core'

import	{
			ItemSelectionFilter
		}								from '@rcc/common'

import	{
			QuestionConfig,
			Question,
			QuestionStore,
			Item
		}								from '@rcc/core'


export const diaryCardsQuestionCategory : string = 'rcc-category-diary-cards'

export const diaryCardsQuestionCategoryFilter: ItemSelectionFilter = {
	filter: (item: Item) => (item instanceof Question) && item.tags?.includes(diaryCardsQuestionCategory),
	representation: {
		label: `CURATED.QUESTIONS.CATEGORIES.${diaryCardsQuestionCategory.toUpperCase()}`,
		icon: 'question'
	}
}

@Injectable()
export class CuratedDiaryCardsQuestionStoreService extends QuestionStore {

	public readonly name 	: string = 'CURATED.QUESTIONS.CURATED_DIARY_CARDS_NAME'

	public constructor(){
		super({ getAll: () => Promise.resolve(diaryCardsConfig) })
	}
}


export const diaryCardsConfig	:	QuestionConfig[]	= [

	{
		id: 'rcc-curated-diary-cards-0001-daily-1',
		type: 'integer',
		meaning: 'Suizidale Ideen',
		translations: {
			en: 'Suicidal thoughts',
			de: 'Suizidale Ideen'
		},
		options: [
			{ value: 0, translations: { en: 'not present',		de: 'nicht vorhanden' } },
			{ value: 1, translations: { en: 'somewhat',			de: 'etwas' } },
			{ value: 2, translations: { en: 'moderately',		de: 'mäßig' } },
			{ value: 3, translations: { en: 'strong',			de: 'stark' } },
		],
		tags: ['rcc-category-diary-cards', 'rcc-category-daily']
	},
	{
		id: 'rcc-curated-diary-cards-0002-daily-0',
		type: 'integer',
		meaning: 'Selbstschädigungsdrang',
		translations: {
			en: 'Self-harm urge',
			de: 'Selbstschädigungsdrang'
		},
		options: [
			{ value: 0, translations: { en: 'not present',		de: 'nicht vorhanden' } },
			{ value: 1, translations: { en: 'somewhat',			de: 'etwas' } },
			{ value: 2, translations: { en: 'moderately',		de: 'mäßig' } },
			{ value: 3, translations: { en: 'strong',			de: 'stark' } },
		],
		tags: ['rcc-category-diary-cards', 'rcc-category-daily']
	},
	{
		id: 'rcc-curated-diary-cards-0003-daily-0',
		type: 'boolean',
		meaning: 'Selbstschädigende Handlung',
		translations: {
			en: 'Self-harming action',
			de: 'Selbstschädigende Handlung'
		},
		tags: ['rcc-category-diary-cards', 'rcc-category-daily']
	},
	{
		id: 'rcc-curated-diary-cards-0004-daily-0',
		type: 'integer',
		meaning: 'Not/Elend',
		translations: {
			en: 'Distress/misery',
			de: 'Not/Elend'
		},
		options: [
			{ value: 0, translations: { en: 'not present',		de: 'nicht vorhanden' } },
			{ value: 1, translations: { en: 'somewhat',			de: 'etwas' } },
			{ value: 2, translations: { en: 'moderately',		de: 'mäßig' } },
			{ value: 3, translations: { en: 'strong',			de: 'stark' } },
		],
		tags: ['rcc-category-diary-cards', 'rcc-category-daily']
	},
	{
		id: 'rcc-curated-diary-cards-0005-daily-1',
		type: 'integer',
		meaning: '[Spezielles Problemverhalten] Drang',
		translations: {
			en: '[Specific problem behavior] urge',
			de: '[Spezielles Problemverhalten] Drang'
		},
		options: [
			{ value: 0, translations: { en: 'not present',		de: 'nicht vorhanden' } },
			{ value: 1, translations: { en: 'somewhat',			de: 'etwas' } },
			{ value: 2, translations: { en: 'moderately',		de: 'mäßig' } },
			{ value: 3, translations: { en: 'strong',			de: 'stark' } },
		],
		tags: ['rcc-category-diary-cards', 'rcc-category-daily']
	},
	{
		id: 'rcc-curated-diary-cards-0006-daily-2',
		type: 'boolean',
		meaning: 'Spezielles Problemverhalten Handlung',
		translations: {
			en: '[Specific problem behavior] action',
			de: '[Spezielles Problemverhalten] Handlung'
		},
		tags: ['rcc-category-diary-cards', 'rcc-category-daily']
	},
	{
		id: 'rcc-curated-diary-cards-0007-daily-2',
		type: 'integer',
		meaning: '[Spezielles Problemverhalten 2] Drang',
		translations: {
			en: '[Specific problem behavior 2] urge',
			de: '[Spezielles Problemverhalten 2] Drang'
		},
		options: [
			{ value: 0, translations: { en: 'not present',		de: 'nicht vorhanden' } },
			{ value: 1, translations: { en: 'somewhat',			de: 'etwas' } },
			{ value: 2, translations: { en: 'moderately',		de: 'mäßig' } },
			{ value: 3, translations: { en: 'strong',			de: 'stark' } },
		],
		tags: ['rcc-category-diary-cards', 'rcc-category-daily']
	},
	{
		id: 'rcc-curated-diary-cards-0008-daily-0',
		type: 'boolean',
		meaning: 'Spezielles Problemverhalten 2 Handlung',
		translations: {
			en: '[Specific problem behavior 2] action',
			de: '[Spezielles Problemverhalten 2] Handlung'
		},
		tags: ['rcc-category-diary-cards', 'rcc-category-daily']
	},
	{
		id: 'rcc-curated-diary-cards-0009-daily-0',
		type: 'boolean',
		meaning: 'Prämenstruelles Syndrom',
		translations: {
			en: 'Premenstrual syndrome',
			de: 'Prämenstruelles Syndrom'
		},
		tags: ['rcc-category-diary-cards', 'rcc-category-daily']
	},
	{
		id: 'rcc-curated-diary-cards-0010-daily-0',
		type: 'boolean',
		meaning: 'Menstruation',
		translations: {
			en: 'Menstruation',
			de: 'Menstruation'
		},
		tags: ['rcc-category-diary-cards', 'rcc-category-daily']
	},
	{
		id: 'rcc-curated-diary-cards-0011-daily-0',
		type: 'integer',
		meaning: 'Schlaf',
		translations: {
			en: 'Sleep',
			de: 'Schlaf'
		},
		options: [
			{ value: 0, translations: { en: 'not at all',			de: 'gar nicht' } },
			{ value: 1, translations: { en: 'poor',					de: 'schlecht' } },
			{ value: 2, translations: { en: 'moderately',			de: 'mäßig' } },
			{ value: 3, translations: { en: 'good',					de: 'gut' } },
		],
		tags: ['rcc-category-diary-cards', 'rcc-category-daily']
	},
	{
		id: 'rcc-curated-diary-cards-0012-daily-0',
		type: 'integer',
		meaning: 'Entscheidung für neuen Weg',
		translations: {
			en: 'Decision for new path',
			de: 'Entscheidung für neuen Weg'
		},
		options: [
			{ value: 0, translations: { en: 'no',				de: 'nein' } },
			{ value: 1, translations: { en: 'somewhat',			de: 'etwas' } },
			{ value: 2, translations: { en: 'moderately',		de: 'mäßig' } },
			{ value: 3, translations: { en: 'strong',			de: 'stark' } },
		],
		tags: ['rcc-category-diary-cards', 'rcc-category-daily']
	},
	{
		id: 'rcc-curated-diary-cards-0013-daily-1',
		type: 'integer',
		meaning: 'Skills',
		translations: {
			en: 'Skills',
			de: 'Skills'
		},
		options: [
			{ value: 0, translations: { en: 'did not think about it',	de: 'nicht daran gedacht' } },
			{ value: 1, translations: { en: 'thought about it',			de: 'daran gedacht' } },
			{ value: 2, translations: { en: 'tried to apply',			de: 'versucht anzuwenden' } },
			{ value: 3, translations: { en: 'automatically applied',	de: 'automatisch angewendet' } },
		],
		tags: ['rcc-category-diary-cards', 'rcc-category-daily']
	},
	{
		id: 'rcc-curated-diary-cards-0014-daily-0',
		type: 'integer',
		meaning: 'Körperliche Aktivität',
		translations: {
			en: 'Physical activity',
			de: 'Körperliche Aktivität'
		},
		options: [
			{ value: 0, translations: { en: 'no',				de: 'nein' } },
			{ value: 1, translations: { en: 'somewhat',			de: 'etwas' } },
			{ value: 2, translations: { en: 'moderately',		de: 'mäßig' } },
			{ value: 3, translations: { en: 'strong',			de: 'stark' } },
		],
		tags: ['rcc-category-diary-cards', 'rcc-category-daily']
	},
	{
		id: 'rcc-curated-diary-cards-0015-daily-0',
		type: 'integer',
		meaning: 'Freude',
		translations: {
			en: 'Pleasure',
			de: 'Freude'
		},
		options: [
			{ value: 0, translations: { en: 'not present',		de: 'nicht vorhanden' } },
			{ value: 1, translations: { en: 'somewhat',			de: 'etwas' } },
			{ value: 2, translations: { en: 'moderately',		de: 'mäßig' } },
			{ value: 3, translations: { en: 'strong',			de: 'stark' } },
		],
		tags: ['rcc-category-diary-cards', 'rcc-category-daily']
	},
	{
		id: 'rcc-curated-diary-cards-0016-daily-0',
		type: 'boolean',
		meaning: 'Positives Ereignis (wenn ja, bitte in Notiz eintragen)',
		translations: {
			en: 'Positive event (if yes, please enter in note)',
			de: 'Positives Ereignis (wenn ja, bitte in Notiz eintragen)'
		},
		tags: ['rcc-category-diary-cards', 'rcc-category-daily']
	},
]
