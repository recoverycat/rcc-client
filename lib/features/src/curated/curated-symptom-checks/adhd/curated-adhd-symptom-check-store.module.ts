import	{	NgModule	}							from	'@angular/core'

import	{
			provideItemSelectionFilter
		}											from	'@rcc/common'

import	{	QuestionnaireServiceModule			}	from 	'../../../questions/questionnaire'

import	{	CuratedSymptomCheckStoreService		}	from	'../../curated-symptom-check-store'

import	{

			adhdQuestionCategory,
			adhdQuestionCategoryFilter,
			CuratedAdhdQuestionStoreService
		}											from	'./curated-adhd.commons'

@NgModule({
	imports:[
		QuestionnaireServiceModule.forChild([CuratedAdhdQuestionStoreService])
	],
	providers: [
		CuratedAdhdQuestionStoreService,
		provideItemSelectionFilter(adhdQuestionCategoryFilter)
	]
})
export class CuratedAdhdSymptomCheckStoreServiceModule {
	public constructor(
		private curatedAdhdQuestionStoreService	: CuratedAdhdQuestionStoreService,
		private curatedSymptomCheckStoreService	: CuratedSymptomCheckStoreService,
	) {
		void this.setup()
	}

	private async setup() : Promise<void> {
		await this.curatedAdhdQuestionStoreService.ready
		await this.curatedSymptomCheckStoreService.createSymptomCheckByCategory(adhdQuestionCategory)
	}
}
