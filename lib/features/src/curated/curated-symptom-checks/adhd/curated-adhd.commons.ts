import	{
			Injectable
		}								from '@angular/core'

import	{	ItemSelectionFilter		}	from '@rcc/common'

import	{
			Item,
			Question,
			QuestionConfig,
			QuestionStore
		} 								from '@rcc/core'



export const  adhdQuestionCategory	: string			= 'rcc-curated-indication-adhd'

export const adhdQuestionCategoryFilter: ItemSelectionFilter = {
	filter: (item: Item) => (item instanceof Question) && item.tags?.includes(adhdQuestionCategory),
	representation: {
		label: `CURATED.QUESTIONS.CATEGORIES.${adhdQuestionCategory.toUpperCase()}`,
		icon: 'question'
	}
}

@Injectable()
export class CuratedAdhdQuestionStoreService extends QuestionStore {

	public readonly name 	: string = 'CURATED.QUESTIONS.CURATED_INDICATION_ADHD_NAME'

	public constructor(){
		super({ getAll: () => Promise.resolve(adhdQuestionConfigs) })
	}
}


export const adhdQuestionConfigs	: QuestionConfig[]	= [
	{
		id: 'rcc-curated-indication-adhd-0001-weekly-1',
		type: 'integer',
		meaning: 'Waren Sie in den letzten sieben Tagen unaufmerksam gegenüber Details oder haben Sie Flüchtigkeitsfehler bei der Arbeit gemacht?',
		translations: {
			en: 'In the last seven days, have you been inattentive to details or have you made some accidental errors in your work due to your inattentiveness?',
			de: 'Waren Sie in den letzten sieben Tagen unaufmerksam gegenüber Details oder haben Sie Flüchtigkeitsfehler bei der Arbeit gemacht?'
		},
		options: [
			{ value: 0, translations: { en: 'no',				de: 'trifft nicht zu' } },
			{ value: 1, translations: { en: 'occasionally',		de: 'gelegentlich' } },
			{ value: 2, translations: { en: 'often',			de: 'oft' } },
			{ value: 3, translations: { en: 'almost always',	de: 'nahezu immer' } },
		],
		tags: ['rcc-curated-indication-adhd', 'rcc-category-weekly']
	},
	{
		id: 'rcc-curated-indication-adhd-0002-weekly-2',
		type: 'integer',
		meaning: 'Ist es Ihnen in den letzten sieben Tagen bei der Arbeit oder anderen Aktivitäten (z. B. Lesen, Fernsehen, Spiele spielen) schwergefallen, konzentriert durchzuhalten?',
		translations: {
			en: 'In the last seven days, have you found it difficult to stay focused at work or other activities (e.g. reading, watching TV, playing games)?',
			de: 'Ist es Ihnen in den letzten sieben Tagen bei der Arbeit oder anderen Aktivitäten (z. B. Lesen, Fernsehen, Spiele spielen) schwergefallen, konzentriert durchzuhalten?'
		},
		options: [
			{ value: 0, translations: { en: 'no',				de: 'trifft nicht zu' } },
			{ value: 1, translations: { en: 'occasionally',		de: 'gelegentlich' } },
			{ value: 2, translations: { en: 'often',			de: 'oft' } },
			{ value: 3, translations: { en: 'almost always',	de: 'nahezu immer' } },
		],
		tags: ['rcc-curated-indication-adhd', 'rcc-category-weekly']
	},
	{
		id: 'rcc-curated-indication-adhd-0003-weekly-1',
		type: 'integer',
		meaning: 'Hatten Sie in den letzten sieben Tagen das Gefühl, Sie hören nicht richtig zu, wenn jemand etwas zu Ihnen sagt?',
		translations: {
			en: 'In the last seven days, have you had the feeling that you are not listening properly when someone is telling you something?',
			de: 'Hatten Sie in den letzten sieben Tagen das Gefühl, Sie hören nicht richtig zu, wenn jemand etwas zu Ihnen sagt?'
		},
		options: [
			{ value: 0, translations: { en: 'not true',				de: 'trifft nicht zu' } },
			{ value: 1, translations: { en: 'occasionally',			de: 'gelegentlich' } },
			{ value: 2, translations: { en: 'often',				de: 'oft' } },
			{ value: 3, translations: { en: 'almost always',		de: 'nahezu immer' } },
		],
		tags: ['rcc-curated-indication-adhd', 'rcc-category-weekly']
	},
	{
		id: 'rcc-curated-indication-adhd-0004-weekly-2',
		type: 'integer',
		meaning: 'Ist es Ihnen in den letzten sieben Tagen schwergefallen, Aufgaben am Arbeitsplatz, wie sie Ihnen erklärt wurden, zu erfüllen?',
		translations: {
			en: 'In the last seven days, have you found it difficult to perform tasks at work as they were explained to you?',
			de: 'Ist es Ihnen in den letzten sieben Tagen schwergefallen, Aufgaben am Arbeitsplatz, wie sie Ihnen erklärt wurden, zu erfüllen?'
		},
		options: [
			{ value: 0, translations: { en: 'not true',				de: 'trifft nicht zu' } },
			{ value: 1, translations: { en: 'occasionally',			de: 'gelegentlich' } },
			{ value: 2, translations: { en: 'often',				de: 'oft' } },
			{ value: 3, translations: { en: 'almost always',		de: 'nahezu immer' } },
		],
		tags: ['rcc-curated-indication-adhd', 'rcc-category-weekly']
	},
	{
		id: 'rcc-curated-indication-adhd-0005-weekly-2',
		type: 'integer',
		meaning: 'Ist es Ihnen in den letzten sieben Tagen schwergefallen, Projekte, Vorhaben oder Aktivitäten zu organisieren?',
		translations: {
			en: 'Have you found it difficult to organize projects, plans or activities in the last seven days?',
			de: 'Ist es Ihnen in den letzten sieben Tagen schwergefallen, Projekte, Vorhaben oder Aktivitäten zu organisieren?'
		},
		options: [
			{ value: 0, translations: { en: 'no',				de: 'trifft nicht zu' } },
			{ value: 1, translations: { en: 'occasionally',		de: 'gelegentlich' } },
			{ value: 2, translations: { en: 'often',			de: 'oft' } },
			{ value: 3, translations: { en: 'almost always',	de: 'nahezu immer' } },
		],
		tags: ['rcc-curated-indication-adhd', 'rcc-category-weekly']
	},
	{
		id: 'rcc-curated-indication-adhd-0006-weekly-1',
		type: 'integer',
		meaning: 'Sind Sie Aufgaben, die geistige Anstrengung erforderlich machen, in den letzten sieben Tagen am liebsten aus dem Weg gegangen, da Sie solche Arbeiten nicht mögen oder sich innerlich dagegen gesträubt haben?',
		translations: {
			en: 'In the last seven days, have you preferred to avoid tasks that require mental effort because you do not like such work or have felt inner resistance to tackle them?',
			de: 'Sind Sie Aufgaben, die geistige Anstrengung erforderlich machen, in den letzten sieben Tagen am liebsten aus dem Weg gegangen, da Sie solche Arbeiten nicht mögen oder sich innerlich dagegen gesträubt haben?'
		},
		options: [
			{ value: 0, translations: { en: 'no',				de: 'trifft nicht zu' } },
			{ value: 1, translations: { en: 'occasionally',		de: 'gelegentlich' } },
			{ value: 2, translations: { en: 'often',			de: 'oft' } },
			{ value: 3, translations: { en: 'almost always',	de: 'nahezu immer' } },
		],
		tags: ['rcc-curated-indication-adhd', 'rcc-category-weekly']
	},
	{
		id: 'rcc-curated-indication-adhd-0007-weekly-2',
		type: 'integer',
		meaning: 'Haben Sie in den letzten sieben Tagen wichtige Gegenstände (z. B. Schlüssel, Portemonnaie, Mobiltelefon) verlegt?',
		translations: {
			en: 'Have you misplaced important items (e.g. keys, wallet, mobile phone) in the last seven days?',
			de: 'Haben Sie in den letzten sieben Tagen wichtige Gegenstände (z. B. Schlüssel, Portemonnaie, Mobiltelefon) verlegt?'
		},
		options: [
			{ value: 0, translations: { en: 'no',				de: 'trifft nicht zu' } },
			{ value: 1, translations: { en: 'occasionally',		de: 'gelegentlich' } },
			{ value: 2, translations: { en: 'often',			de: 'oft' } },
			{ value: 3, translations: { en: 'almost always',	de: 'nahezu immer' } },
		],
		tags: ['rcc-curated-indication-adhd', 'rcc-category-weekly']
	},
	{
		id: 'rcc-curated-indication-adhd-0008-weekly-1',
		type: 'integer',
		meaning: 'Haben Sie sich in den letzten sieben Tagen bei Tätigkeiten leicht ablenken lassen?',
		translations: {
			en: 'Have you been easily distracted during activities in the last seven days?',
			de: 'Haben Sie sich in den letzten sieben Tagen bei Tätigkeiten leicht ablenken lassen?'
		},
		options: [
			{ value: 0, translations: { en: 'no',				de: 'trifft nicht zu' } },
			{ value: 1, translations: { en: 'occasionally',		de: 'gelegentlich' } },
			{ value: 2, translations: { en: 'often',			de: 'oft' } },
			{ value: 3, translations: { en: 'almost always',	de: 'nahezu immer' } },
		],
		tags: ['rcc-curated-indication-adhd', 'rcc-category-weekly']
	},
	{
		id: 'rcc-curated-indication-adhd-0009-weekly-1',
		type: 'integer',
		meaning: 'Haben Sie in den letzten sieben Tagen Verabredungen, Termine oder telefonische Rückrufe vergessen?',
		translations: {
			en: 'Have you forgotten meetings, appointments or phone calls in the last seven days?',
			de: 'Haben Sie in den letzten sieben Tagen Verabredungen, Termine oder telefonische Rückrufe vergessen?'
		},
		options: [
			{ value: 0, translations: { en: 'no',				de: 'trifft nicht zu' } },
			{ value: 1, translations: { en: 'occasionally',		de: 'gelegentlich' } },
			{ value: 2, translations: { en: 'often',			de: 'oft' } },
			{ value: 3, translations: { en: 'almost always',	de: 'nahezu immer' } },
		],
		tags: ['rcc-curated-indication-adhd', 'rcc-category-weekly']
	},
	{
		id: 'rcc-curated-indication-adhd-0010-weekly-1',
		type: 'integer',
		meaning: 'Waren Sie in den letzten sieben Tagen zappelig?',
		translations: {
			en: 'Have you been fidgety in the last seven days?',
			de: 'Waren Sie in den letzten sieben Tagen zappelig?'
		},
		options: [
			{ value: 0, translations: { en: 'no',				de: 'trifft nicht zu' } },
			{ value: 1, translations: { en: 'occasionally',		de: 'gelegentlich' } },
			{ value: 2, translations: { en: 'often',			de: 'oft' } },
			{ value: 3, translations: { en: 'almost always',	de: 'nahezu immer' } },
		],
		tags: ['rcc-curated-indication-adhd', 'rcc-category-weekly']
	},
	{
		id: 'rcc-curated-indication-adhd-0011-weekly-2',
		type: 'integer',
		meaning: 'Ist es Ihnen in den letzten sieben Tagen schwergefallen, längere Zeit sitzen zu bleiben (z. B. im Kino oder Theater)?',
		translations: {
			en: 'In the last seven days, have you found it difficult to remain seated for long periods of time (e.g. at the movies or theater)?',
			de: 'Ist es Ihnen in den letzten sieben Tagen schwergefallen, längere Zeit sitzen zu bleiben (z. B. im Kino oder Theater)?'
		},
		options: [
			{ value: 0, translations: { en: 'no',				de: 'trifft nicht zu' } },
			{ value: 1, translations: { en: 'occasionally',		de: 'gelegentlich' } },
			{ value: 2, translations: { en: 'often',			de: 'oft' } },
			{ value: 3, translations: { en: 'almost always',	de: 'nahezu immer' } },
		],
		tags: ['rcc-curated-indication-adhd', 'rcc-category-weekly']
	},
	{
		id: 'rcc-curated-indication-adhd-0012-weekly-1',
		type: 'integer',
		meaning: 'Haben Sie sich in den letzten sieben Tagen unruhig gefühlt?',
		translations: {
			en: 'Have you felt restless in the last seven days?',
			de: 'Haben Sie sich in den letzten sieben Tagen unruhig gefühlt?'
		},
		options: [
			{ value: 0, translations: { en: 'no',				de: 'trifft nicht zu' } },
			{ value: 1, translations: { en: 'occasionally',		de: 'gelegentlich' } },
			{ value: 2, translations: { en: 'often',			de: 'oft' } },
			{ value: 3, translations: { en: 'almost always',	de: 'nahezu immer' } },
		],
		tags: ['rcc-curated-indication-adhd', 'rcc-category-weekly']
	},
	{
		id: 'rcc-curated-indication-adhd-0013-weekly-1',
		type: 'integer',
		meaning: 'Konnten Sie sich in den letzten sieben Tagen schlecht leise beschäftigen? Wenn Sie etwas gemacht haben, ging es laut zu?',
		translations: {
			en: 'Have you had trouble keeping quiet in the last seven days? When you did something, did it have to be noisy?',
			de: 'Konnten Sie sich in den letzten sieben Tagen schlecht leise beschäftigen? Wenn Sie etwas gemacht haben, ging es laut zu?'
		},
		options: [
			{ value: 0, translations: { en: 'no',				de: 'trifft nicht zu' } },
			{ value: 1, translations: { en: 'occasionally',		de: 'gelegentlich' } },
			{ value: 2, translations: { en: 'often',			de: 'oft' } },
			{ value: 3, translations: { en: 'almost always',	de: 'nahezu immer' } },
		],
		tags: ['rcc-curated-indication-adhd', 'rcc-category-weekly']
	},
	{
		id: 'rcc-curated-indication-adhd-0014-weekly-1',
		type: 'integer',
		meaning: 'Waren Sie in den letzten sieben Tagen ständig rastlos und auf Achse und fühlten sich wie von einem Motor angetrieben?',
		translations: {
			en: 'Have you been constantly on the move and restless in the last seven days and felt like you were driven by an engine?',
			de: 'Waren Sie in den letzten sieben Tagen ständig rastlos und auf Achse und fühlten sich wie von einem Motor angetrieben?'
		},
		options: [
			{ value: 0, translations: { en: 'no',				de: 'trifft nicht zu' } },
			{ value: 1, translations: { en: 'occasionally',		de: 'gelegentlich' } },
			{ value: 2, translations: { en: 'often',			de: 'oft' } },
			{ value: 3, translations: { en: 'almost always',	de: 'nahezu immer' } },
		],
		tags: ['rcc-curated-indication-adhd', 'rcc-category-weekly']
	},
	{
		id: 'rcc-curated-indication-adhd-0015-weekly-2',
		type: 'integer',
		meaning: 'Ist es Ihnen in den vergangenen sieben Tagen schwergefallen, abzuwarten, bis andere ausgesprochen haben? Sind Sie anderen ins Wort gefallen?',
		translations: {
			en: 'Have you found it difficult in the past seven days to wait until someone has finished their sentence? Have you interrupted others?',
			de: 'Ist es Ihnen in den vergangenen sieben Tagen schwergefallen, abzuwarten, bis andere ausgesprochen haben? Sind Sie anderen ins Wort gefallen?'
		},
		options: [
			{ value: 0, translations: { en: 'no',				de: 'trifft nicht zu' } },
			{ value: 1, translations: { en: 'occasionally',		de: 'gelegentlich' } },
			{ value: 2, translations: { en: 'often',			de: 'oft' } },
			{ value: 3, translations: { en: 'almost always',	de: 'nahezu immer' } },
		],
		tags: ['rcc-curated-indication-adhd', 'rcc-category-weekly']
	},
	{
		id: 'rcc-curated-indication-adhd-0016-weekly-1',
		type: 'integer',
		meaning: 'Waren Sie in den letzten sieben Tagen ungeduldig und konnten nicht warten, bis Sie an der Reihe waren (z. B. beim Einkaufen)?',
		translations: {
			en: 'Have you been impatient in the last seven days when waiting in lines and could not wait for your turn (e.g. when shopping)?',
			de: 'Waren Sie in den letzten sieben Tagen ungeduldig und konnten nicht warten, bis Sie an der Reihe waren (z. B. beim Einkaufen)?'
		},
		options: [
			{ value: 0, translations: { en: 'no',				de: 'trifft nicht zu' } },
			{ value: 1, translations: { en: 'occasionally',		de: 'gelegentlich' } },
			{ value: 2, translations: { en: 'often',			de: 'oft' } },
			{ value: 3, translations: { en: 'almost always',	de: 'nahezu immer' } },
		],
		tags: ['rcc-curated-indication-adhd', 'rcc-category-weekly']
	},
	{
		id: 'rcc-curated-indication-adhd-0017-weekly-1',
		type: 'integer',
		meaning: 'Haben Sie andere in den letzten sieben Tagen unterbrochen und gestört, wenn sie etwas getan haben?',
		translations: {
			en: 'Have you interrupted and disturbed others when they were doing something in the last seven days?',
			de: 'Haben Sie andere in den letzten sieben Tagen unterbrochen und gestört, wenn sie etwas getan haben?'
		},
		options: [
			{ value: 0, translations: { en: 'no',				de: 'trifft nicht zu' } },
			{ value: 1, translations: { en: 'occasionally',		de: 'gelegentlich' } },
			{ value: 2, translations: { en: 'often',			de: 'oft' } },
			{ value: 3, translations: { en: 'almost always',	de: 'nahezu immer' } },
		],
		tags: ['rcc-curated-indication-adhd', 'rcc-category-weekly']
	},
	{
		id: 'rcc-curated-indication-adhd-0018-weekly-1',
		type: 'integer',
		meaning: 'Haben Sie in den letzten sieben Tagen viel geredet, auch wenn Ihnen keiner zuhören wollte?',
		translations: {
			en: 'Have you talked a lot in the last seven days, even if nobody wanted to listen to you?',
			de: 'Haben Sie in den letzten sieben Tagen viel geredet, auch wenn Ihnen keiner zuhören wollte?'
		},
		options: [
			{ value: 0, translations: { en: 'no',				de: 'trifft nicht zu' } },
			{ value: 1, translations: { en: 'occasionally',		de: 'gelegentlich' } },
			{ value: 2, translations: { en: 'often',			de: 'oft' } },
			{ value: 3, translations: { en: 'almost always',	de: 'nahezu immer' } },
		],
		tags: ['rcc-curated-indication-adhd', 'rcc-category-weekly']
	},
]
