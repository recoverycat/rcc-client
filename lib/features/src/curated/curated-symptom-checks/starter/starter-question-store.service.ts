import	{
			Injectable
		}								from '@angular/core'
import	{

			QuestionConfig,
			QuestionStore
		} 								from '@rcc/core'

/**
 * This StarterQuestionStoreService holds the starter questions which the patient
 * user can get before his/her first contact with the doctor. This store is only
 * imported on the PAT side. It is utilized by the corresponding StarterSymptomCheckStoreService.
 */
@Injectable()
export class StarterQuestionStoreService extends QuestionStore {

	public readonly name: string = 'CURATED.QUESTIONS.CURATED_STARTER_NAME'

	public constructor(){
		super({ getAll: () => Promise.resolve(starterQuestionConfigs) })
	}
}

export const starterQuestionConfigs: QuestionConfig[] = [

	{
		id: 'rcc-curated-starter-questions-0001-daily-0',
		type: 'integer',
		meaning: 'Wie ist deine Stimmung heute? (0: bedrückt - 100: euphorisch)',
		min: 0,
		max: 100,
		translations: {
			en: 'How is your mood today? (0: down - 100: euphoric)',
			de: 'Wie ist deine Stimmung heute? (0: bedrückt - 100: euphorisch)'
		},
		options: [],
		tags: ['rcc-symptom-area-mood', 'rcc-category-daily', 'rcc-category-starter', 'rcc-scale']
	},
	{
		id: 'rcc-curated-starter-questions-0002-daily-0',
		type: 'integer',
		meaning: 'Wie gut konntest du deinen Alltag heute bewältigen?',
		min: 0,
		max: 100,
		translations: {
			en: 'How easy was it to do your daily routine today?',
			de: 'Wie gut konntest du deinen Alltag heute bewältigen?'
		},
		options: [],
		tags: ['rcc-symptom-area-drive', 'rcc-category-daily', 'rcc-category-starter', 'rcc-scale']
	},
	// Old version of question 0003:
	{
		id: 'rcc-curated-starter-questions-0003-daily-0',
		type: 'integer',
		meaning: 'Hast du heute etwas Gutes für dich getan? (Schreibe gern etwas dazu in der Frage “Tägliche Notizen”)',
		translations: {
			en: 'Did you do something for your well-being today? (Feel free to write about it in the “Daily notes” question)',
			de: 'Hast du heute etwas Gutes für dich getan? (Schreibe gern etwas dazu in der Frage “Tägliche Notizen”)'
		},
		options: [
			{ value: 0, translations: { en: 'no', de: 'nein' } },
			{ value: 1, translations: { en: 'some', de: 'etwas' } },
			{ value: 2, translations: { en: 'yes', de: 'ja' } },
			{ value: 3, translations: { en: 'a lot', de: 'sehr viel' } },
		],
		tags: ['rcc-category-resources', 'rcc-category-daily', 'rcc-category-starter'],
	},
	// New version of question 0003:
	{
		id: 'rcc-curated-starter-questions-0003-daily-1',
		type: 'integer',
		meaning: 'Hast du heute etwas Gutes für dich getan? (Schreibe gern etwas dazu in der Frage “Notizen”)',
		translations: {
			en: 'Did you do something for your well-being today? (Feel free to write about it in the “Notes” question)',
			de: 'Hast du heute etwas Gutes für dich getan? (Schreibe gern etwas dazu in der Frage “Notizen”)'
		},
		options: [
			{ value: 0, translations: { en: 'no', de: 'nein' } },
			{ value: 1, translations: { en: 'some', de: 'etwas' } },
			{ value: 2, translations: { en: 'yes', de: 'ja' } },
			{ value: 3, translations: { en: 'a lot', de: 'sehr viel' } },
		],
		tags: ['rcc-category-resources', 'rcc-category-daily', 'rcc-category-starter'],
	},
	{
		id: 'rcc-curated-default-00-daily-note',
		type:     'string',
		meaning:  'Was ist heute sonst noch passiert?',
		translations: {
			en: 'What else happened today?',
			de: 'Was ist heute sonst noch passiert?'
		},
		tags:     ['rcc-long-text', 'rcc-category-daily', 'rcc-category-default']
	},

]
