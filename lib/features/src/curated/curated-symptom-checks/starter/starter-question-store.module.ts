import	{	NgModule						}	from	'@angular/core'
import	{	QuestionnaireServiceModule		}	from 	'../../../questions/questionnaire'

import	{
			StarterQuestionStoreService
		}											from './starter-question-store.service'

@NgModule({
	imports:[
		QuestionnaireServiceModule.forChild([StarterQuestionStoreService])
	],
	providers: [
		StarterQuestionStoreService,
	]
})
export class StarterQuestionStoreServiceModule {
	public constructor(
		private starterQuestionStoreService	: StarterQuestionStoreService,
	) {
		void this.setup()
	}

	private async setup() : Promise<void> {
		await this.starterQuestionStoreService.ready
	}
}
