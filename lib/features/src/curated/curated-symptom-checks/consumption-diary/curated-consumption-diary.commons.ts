
import	{	Injectable			}	from '@angular/core'
import	{	ItemSelectionFilter	}	from '@rcc/common'
import	{
			QuestionConfig,
			Question,
			QuestionStore,
			Item
		}							from '@rcc/core'

export const consumptionDiaryQuestionCategory : string = 'rcc-category-consumption-diary'

export const consumptionDiaryQuestionCategoryFilter: ItemSelectionFilter = {
			filter: (item: Item) => (item instanceof Question) && item.tags?.includes(consumptionDiaryQuestionCategory),
			representation: {
				label: `CURATED.QUESTIONS.CATEGORIES.${consumptionDiaryQuestionCategory.toUpperCase()}`,
				icon: 'question'
			}
		}

@Injectable()
export class CuratedConsumptionDiaryQuestionStoreService extends QuestionStore {

	public readonly name 	: string = 'CURATED.QUESTIONS.CURATED_CONSUMPTION_DIARY_NAME'

	public constructor(){
		super({ getAll: () => Promise.resolve(consumptionDiaryConfigs) })
	}
}

export const consumptionDiaryConfigs:QuestionConfig[] = [
	{
		id: 'rcc-curated-consumption-diary-0001-daily-1',
		type: 'decimal',
		meaning: 'Wie viele alkoholische Getränke haben Sie heute getrunken? (0,5 L Flaschen/Gläser Getränk)',
		translations: {
			en: 'How many alcoholic drinks did you have today?',
			de: 'Wie viele alkoholische Getränke haben Sie heute getrunken? '
		},
		options: [],
		unit: '0,5 L',
		tags: ['rcc-category-consumption-diary', 'rcc-category-daily']
	},
	{
		id: 'rcc-curated-consumption-diary-0002-daily-1',
		type: 'decimal',
		meaning: 'Wie viel haben Sie von [alkoholisches Getränk 2] heute getrunken?',
		translations: {
			en: 'How many drinks did you have today [of alcohol type 2]?',
			de: 'Wie viel haben Sie von [alkoholisches Getränk 2] heute getrunken?'
		},
		options: [],
		unit: '0,5 L',
		tags: ['rcc-category-consumption-diary', 'rcc-category-daily']
	},
]
