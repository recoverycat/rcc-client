import	{	NgModule	}								from	'@angular/core'

import	{
			provideItemSelectionFilter
		}												from	'@rcc/common'

import	{	QuestionnaireServiceModule			}		from 	'../../../questions/questionnaire'

import	{	CuratedSymptomCheckStoreService		}		from	'../../curated-symptom-check-store'

import	{

			consumptionDiaryQuestionCategory,
			consumptionDiaryQuestionCategoryFilter,
			CuratedConsumptionDiaryQuestionStoreService
		}												from	'./curated-consumption-diary.commons'

@NgModule({
	imports:[
		QuestionnaireServiceModule.forChild([CuratedConsumptionDiaryQuestionStoreService])
	],
	providers: [
		CuratedConsumptionDiaryQuestionStoreService,
		provideItemSelectionFilter(consumptionDiaryQuestionCategoryFilter)
	]
})
export class CuratedConsumptionDiarySymptomCheckStoreServiceModule {
	public constructor(
		private curatedConsumptionDiaryQuestionStoreService	: CuratedConsumptionDiaryQuestionStoreService,
		private curatedSymptomCheckStoreService	: CuratedSymptomCheckStoreService,
	) {
		void this.setup()
	}

	private async setup() : Promise<void> {
		await this.curatedConsumptionDiaryQuestionStoreService.ready
		await this.curatedSymptomCheckStoreService.createSymptomCheckByCategory(consumptionDiaryQuestionCategory)
	}
}
