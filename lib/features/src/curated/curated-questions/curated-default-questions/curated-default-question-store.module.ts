import 	{	NgModule								}	from '@angular/core'
import	{	QuestionnaireServiceModule				}	from '../../../questions/questionnaire'
import	{	CuratedDefaultQuestionStoreService		}	from './curated-default-question-store.service'
@NgModule({
	imports:[
		QuestionnaireServiceModule.forChild([CuratedDefaultQuestionStoreService])
	],
	providers:[
		CuratedDefaultQuestionStoreService,
	]
})
export class CuratedDefaultQuestionStoreServiceModule {

}
