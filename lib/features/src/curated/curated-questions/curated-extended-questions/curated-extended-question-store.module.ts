import 	{	NgModule						}	from '@angular/core'

import	{	QuestionnaireServiceModule		}	from '../../../questions/questionnaire'
import	{
			CuratedExtendedQuestionStoreService
		}										from './curated-extended-question-store.service'
@NgModule({
	imports:[
		QuestionnaireServiceModule.forChild([CuratedExtendedQuestionStoreService])
	],
	providers:[
		CuratedExtendedQuestionStoreService
	]
})
export class CuratedExtendedQuestionStoreServiceModule {

}
