import { ComponentFixture, TestBed } from '@angular/core/testing'
import { Router, RouterModule, Params } from '@angular/router'
import { firstValueFrom } from 'rxjs'
import { StaticQrCodeComponent } from './static-qr-page.component'
import { StaticQrCodeModule, staticQRCodeRoute } from './static-qr.module'
import { IncomingDataService, IncomingDataServiceModule, RccToastController } from '@rcc/common'
import { objectToBase64URIComponent } from '@rcc/core'

describe('StaticQrCodeComponent', () => {
	let fixture: ComponentFixture<StaticQrCodeComponent> = undefined!
	let router: Router = undefined!
	let incomingDataService: IncomingDataService = undefined!

	async function runWithProvidedData(data: unknown): Promise<void> {

		const queryObjectPromise: Promise<unknown> = firstValueFrom(incomingDataService)

		await openStaticQrCodePathWithQueryParams({ d: objectToBase64URIComponent(data) })

		const wrappedQueryObject: unknown = await queryObjectPromise

		expect(wrappedQueryObject).toEqual({ data, source: 'link' })
		expect(router.url).toBe('/')

	}

	async function openStaticQrCodePathWithQueryParams(queryParams: Params): Promise<void> {
		await router.navigate([staticQRCodeRoute.path], { queryParams })
	}

	beforeEach(() => {

		const mockRccToastController: RccToastController = jasmine.createSpyObj('RccToastController', ['present', 'success', 'failure', 'info']) as RccToastController

		void TestBed.configureTestingModule({
			imports:	[
					StaticQrCodeModule,
					RouterModule.forRoot([]),
					IncomingDataServiceModule
				],
			providers: [{ provide: RccToastController, useValue: mockRccToastController },]
		}).compileComponents()

		fixture					=	TestBed.createComponent(StaticQrCodeComponent)
		router					=	TestBed.inject(Router)
		incomingDataService		=	TestBed.inject(IncomingDataService)
	})

	it('should announce wrapped query data on IncomingDataService and navigate back to homepage ', async () => {
		fixture.detectChanges()

		await	runWithProvidedData({
					firstKey: 'firstValue',
					secondKey: 'secondValue'
				})
		await	runWithProvidedData({
					firstKey: 123,
					secondKey: 'secondValue'
				})
		await	runWithProvidedData({
					firstKey: true,
					secondKey: 'secondValue'
				})
		await	runWithProvidedData({
					firstKey: [1,2,3,'a'],
					secondKey: {
						a:	'a'
					}
				})
		await	runWithProvidedData('ABC')
		await	runWithProvidedData(123)
		await	runWithProvidedData([])
		await	runWithProvidedData({})
	})

	it('should not announce anything on IncomingDataService for invalid queryParams', async () => {
		let count : number = 0
		incomingDataService.subscribe(() => count++)

		await openStaticQrCodePathWithQueryParams({ d: '123' })
		await openStaticQrCodePathWithQueryParams({ d: 'ABC' })
		await openStaticQrCodePathWithQueryParams({ d: '' })
		await openStaticQrCodePathWithQueryParams({})
		await openStaticQrCodePathWithQueryParams({ x: '123' })
		await openStaticQrCodePathWithQueryParams({ d: '[]' })
		await openStaticQrCodePathWithQueryParams({ x: '[]' })
		await openStaticQrCodePathWithQueryParams({ x: objectToBase64URIComponent({ a: 123 }) })

		expect(count).toBe(0)
	})

})
