import	{
			Component,
			OnDestroy,
			OnInit							}	from	'@angular/core'
import	{
			ActivatedRoute,
			Router							}	from	'@angular/router'
import	{	IncomingDataService, RccToastController				}	from	'@rcc/common'
import	{
			base64URIComponentToObject,
			has,
		}										from	'@rcc/core'
import	{
			Subject,
			takeUntil						}	from	'rxjs'

@Component({
	template: '',
	standalone: true,

})

/**
 * The StaticQrCodeComponent processes query parameters from the URL, decodes
 * them if they are present and in the correct format, and sends the decoded
 * data to a service for further handling.
 */
export class StaticQrCodeComponent implements OnInit, OnDestroy {

	public destroy$ : Subject<void> = new Subject<void>()

	public constructor(
		private router: Router,
		private activatedRoute: ActivatedRoute,
		private incomingDataService: IncomingDataService,
		private rccToastController: RccToastController

	){}

	/**
	 * Subscribes to the query parameters from the URL and handles them.
	 *
	 * @returns A subscription to the query parameters.
	 */
	public ngOnInit(): void {
		this.activatedRoute.queryParams
		.pipe(takeUntil(this.destroy$))
		.subscribe(params => void this.handleQueryParams(params))
	}

	/**
	 * Handles the query parameters from the URL.
	 *
	 * @param params - The query parameters object from the URL.
	 */
	public async handleQueryParams(params: unknown): Promise<void> {
		// If 'd' parameter is not present or it's not a string, navigate back to the root route.
		if(!has(params, 'd') || typeof params.d !== 'string') return void this.router.navigate(['/'])

		await this.assertParamLength(params.d, 150)

		try {
			// Decodes the base64 string from the 'd' parameter and assigns it to the 'data' variable.
			const data: unknown = base64URIComponentToObject(params.d)

			// Calls the handleData method with the decoded data.
			this.handleData(data)
		} catch (error) {
			console.error('Invalid base64 or JSON format:', error)
			void this.rccToastController.failure('STATIC_QR_CODE.ERROR_MESSAGES.INVALID_BASE64_OR_JSON_FORMAT')
			void this.router.navigate(['/'])
		}
	}

	public async assertParamLength(param: string, length: number): Promise<void> {
		try {
			if (param.length < length) return
			throw new Error('Parameter "d" is too long')
		} catch (error) {
			console.error('Parameter "d" is too long:', param.length)
			await this.rccToastController.failure('STATIC_QR_CODE.ERROR_MESSAGES.PARAM_TOO_LONG')
			void this.router.navigate(['/'])
			throw error
		}
	}

	/**
	 * Handles the decoded data and sends it to the IncomingDataService.
	 *
	 * @param data - The decoded data to be handled.
	 */
	public handleData(data: unknown): void {
		this.incomingDataService.next(this.wrapData(data))

		// Logging the data to see if it works properly as no other part of the
		// app is using it yet.
		console.info('StaticQrCodeComponent - Receiving data: ', data)
		void this.router.navigate(['/'])
	}

	/**
	 * Wraps the provided data with a source attribute set to 'link'.
	 *
	 * @param data - The data to be wrapped.
	 * @returns The wrapped data object with a 'source' attribute set to 'link'.
	 */
	public wrapData(data: unknown): unknown {
		return { data, source: 'link' }
	}

	public ngOnDestroy(): void {
		this.destroy$.next()
		this.destroy$.complete()
	}

}
