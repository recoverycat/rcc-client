import { Injectable, OnDestroy } from '@angular/core'
import { Router } from '@angular/router'
import { IncomingDataService } from '@rcc/common'
import { Subject, filter, takeUntil } from 'rxjs'

@Injectable()
/**
 * The StaticQrCodeService processes incoming data, validates URLs, and navigates
 * based on these URLs. This service is injectable and manages subscriptions to
 * clean up resources when it is destroyed.
 */
export class StaticQrCodeService implements OnDestroy {
	private destroy$: Subject<void> = new Subject<void>()

	/**
	 * The constructor function takes in an IncomingDataService and Router, subscribes
	 * to incoming data, extracts URL parameters, and navigates to a new route based
	 * on the URL.
	 * @param {IncomingDataService} incomingDataService - The `incomingDataService`
	 * parameter is an instance of the `IncomingDataService`. It is being
	 * used to receive incoming data and process it in the constructor function.
	 * @param {Router} router - The `router` parameter in the constructor is an
	 * instance of the Angular Router service. It is used for navigating between
	 * different views in an Angular application based on the provided URL.
	 */
	public constructor(
		private incomingDataService: IncomingDataService,
		private router: Router
	) {
		this.incomingDataService
		.pipe(
			takeUntil(this.destroy$),
			filter(this.isValidUrl),
			filter(this.isRccUrl)
		)

		.subscribe(urlString => {
			const url : URL = new URL(urlString)
			const queryParams : unknown = Object.fromEntries(url.searchParams)

			void this.router.navigate([url.pathname], { queryParams })
		})
	}

	/**
	 * The function `isValidUrl` in TypeScript checks if a given value is a valid URL
	 * string.
	 * @param {void}  - This a TypeScript type guard function
	 * named `isValidUrl`. It takes an argument `potentialUrl` of type `unknown` and
	 * checks if it is a valid URL string. The function returns a boolean indicating
	 * whether the `potentialUrl` is a valid URL string or not.
	 * @param {unknown} potentialUrl - The `potentialUrl` parameter is a value that is
	 * being checked to determine if it is a valid URL. The `isValidUrl` function
	 * checks if the `potentialUrl` is a string and then attempts to create a new URL
	 * object with it. If the URL object can be successfully created, the
	 * @returns The `isValidUrl` function is checking if the `potentialUrl` is a valid
	 * URL by first verifying if it is a string and then attempting to create a new
	 * URL object with the provided string. If the URL creation is successful, it
	 * returns `true`, indicating that the `potentialUrl` is a valid URL. If an error
	 * is caught during the URL creation attempt, it returns `false`,
	 */
	private isValidUrl(this: void, potentialUrl: unknown): potentialUrl is string {
		if(typeof potentialUrl !== 'string') return false
		try{
			new URL(potentialUrl)
			return true
		} catch (e) {
			return false
		}
	}

	/**
	 * This TypeScript function checks if a given URL matches the hostname and
	 * protocol of the current window's URL.
	 * @param {void}  - The function `isRccUrl` is designed to check if a given URL
	 * matches the hostname and protocol of the current window location. It takes a
	 * `potentialMatch` string as input and attempts to create a new URL object from
	 * it. If successful, it compares the hostname and protocol of the new
	 * @param {string} potentialMatch - The `potentialMatch` parameter in the
	 * `isRccUrl` function is a string that represents a URL that you want to check
	 * for a match with the current webpage's URL. The function attempts to create a
	 * new URL object from this string and then compares the hostname and protocol of
	 * the potential URL
	 * @returns The function `isRccUrl` is returning a boolean value. It returns
	 * `true` if the hostname and protocol of the `potentialMatch` URL match the
	 * hostname and protocol of the current window's URL, and `false` if there is an
	 * error or if the match fails.
	 */
	private isRccUrl(this: void, potentialMatch: string): boolean {
		try{
			const newUrl					: URL		= new URL(potentialMatch)

			const potentialMatchHostname 	: string	= newUrl.hostname
			const potentialMatchProtocol	: string	= newUrl.protocol
			const rccHostname 				: string	= window.location.hostname
			const rccProtocol				: string	= window.location.protocol

			const hostnameMatch				: boolean 	= rccHostname === potentialMatchHostname
			const protocolMatch				: boolean 	= rccProtocol === potentialMatchProtocol
			return hostnameMatch && protocolMatch
		} catch (e) {
			return false
		}
	}
	public ngOnDestroy(): void {
		this.destroy$.next()
		this.destroy$.complete()
	}
}
