import	{	NgModule					}	from '@angular/core'
import	{
			IncomingDataServiceModule,
			provideTranslationMap,
		}									from '@rcc/common'
import	{	StaticQrCodeComponent		}	from './static-qr-page.component'
import	{	Route, RouterModule			}	from '@angular/router'
import	{	StaticQrCodeService			}	from './static-qr-code.service'

export const staticQRCodeRoute: Route = { path: 'data', component: StaticQrCodeComponent }

import en from './i18n/en.json'
import de from './i18n/de.json'

@NgModule({
	imports: [
		IncomingDataServiceModule,
		RouterModule.forChild([staticQRCodeRoute])
	],
	providers: [
		StaticQrCodeService,
		provideTranslationMap('STATIC_QR_CODE', { en, de }),
	]
})
export class StaticQrCodeModule {
	public constructor(private staticQrCodeService: StaticQrCodeService){}
}
