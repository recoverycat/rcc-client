import	{
			Inject,
			Injectable,
			NgZone,
			OnDestroy,
		}									from '@angular/core'

import	{
			HttpClient,
			HttpParams,
			HttpResponse
		}									from '@angular/common/http'

import	{
			firstValueFrom,
			Subject,
		}									from 'rxjs'

import	{
			RccPublicRuntimeConfigService,
		}									from '@rcc/common'

import	{
			randomHexString,
			assert
		}									from '@rcc/core'

import	{
			QueuedLoggingData,
			LoggingData,
			RccUsageLoggingQueue,
			RccUsageLoggingService
		} 									from '../usage-logging'

import 	{
			matomoPHPUrlAppendix,
		}									from './matomo.commons'


export const MATOMO_LOGGING_QUEUE_ID 		: string
											= 'matomo-logging-queue'


/**
 * Service that listens to UsageLoggingData from
 * {@link RccUsageLoggingService} and tries to post it to
 * the Matomo backend.
 *
 * Failed attempts are queued and retried.
 */
@Injectable()
export class MatomoService implements OnDestroy{

	private destroy$			: Subject<void>
								= new Subject<void>()

	private usageLoggingQueue	: RccUsageLoggingQueue
								= undefined

	public constructor(
		@Inject('MATOMO_LOGGING_ENDPOINT')
		private readonly usageLoggingEndpoint			: string,
		private readonly http							: HttpClient,
		private readonly rccPublicRuntimeConfigService	: RccPublicRuntimeConfigService,
		private readonly rccUsageLoggingService			: RccUsageLoggingService,
		private readonly ngZone							: NgZone
	) {

		// Because of Angular not becoming stable in due time, we need to follow the
		// recommendations from here: https://angular.dev/errors/NG0506 as
		// when normally run, it blocks other processes like the UpdateModule.
		// Issue: https://git.recoverycat.de/recoverycat/rcc-client/-/issues/1358
		ngZone.runOutsideAngular(() => {

			this.usageLoggingQueue = rccUsageLoggingService.getQueue(MATOMO_LOGGING_QUEUE_ID)

			this.usageLoggingQueue
				.readyData$
				.subscribe( queuedLoggingData => {
					void this.processQueue(queuedLoggingData)
				})

		})

		let endpointUrl 	: URL
							= undefined

		try {
			endpointUrl = new URL(usageLoggingEndpoint)
		} catch (e) {
			throw new Error(`MatomoService: invalid endpoint url; got: '${usageLoggingEndpoint}'`, { cause:e })
		}


		assert(endpointUrl.protocol === 'https:', `MatomoService: endpoint url protocol must be "https:", got: '${usageLoggingEndpoint}'`)
	}

	/**
	 * Processes items that come off the queue. It tries to post data and puts
	 * it back onto the queue when posting fails.
	 */
	private async processQueue(queuedLoggingData: QueuedLoggingData) : Promise<void> {


		try {

			await this.postData(queuedLoggingData.loggingData)

			// Keep going while posting works:
			this.usageLoggingQueue.requestNext()
		}
		catch(e){

			const countAsFailure 	: boolean
									= this.errorCountsAsFailure(e)

			this.usageLoggingQueue.retryLater(queuedLoggingData, countAsFailure)
		}
	}

	/**
	 * Tries to deliver logging data to the Matomo backend.
	 */
	private async postData(loggingData: LoggingData, originalDate?: number): Promise<void> {

		const categoryValue	: 	string
							= 	loggingData.category + ' - ' + loggingData.flavor + ' - ' + loggingData.version

		const timestamp		: 	Date
							= 	originalDate
								?	new Date(originalDate)
								:	new Date()

		const siteId		:	string
							=	await this.rccPublicRuntimeConfigService.get('matomo.site-id') as string

		const [h,m,s]		:	string[]
							=	timestamp
								.toLocaleTimeString('de', { hour: '2-digit', minute:'2-digit', second: '2-digit', hourCycle: 'h24' })
								.split(':')

		const params		:	HttpParams
							=	new HttpParams()
								.append('rand', randomHexString(16) )
								.append('idsite', siteId)
								.append('rec', '1')
								.append('action_name', loggingData.category)
								.append('e_c', categoryValue)
								.append('e_a', loggingData.key)
								.append('e_n', loggingData.extraValue ? loggingData.extraValue : '')
								.append('e_v', loggingData.computableValue ? loggingData.computableValue.toString() : '')
								.append('_id', loggingData.id)
								.append('uid', loggingData.id)
								.append('h', h)
								.append('m', m)
								.append('s', s)
								.append('send_image', 0) // prevents Matomo from responding with a tracking pixel.


		const url			:	string
							=	this.usageLoggingEndpoint + matomoPHPUrlAppendix


		// Using response type: 'text', because we expect an empty response,
		// but if Matomo sends one anyway we do not want the response parsing
		// to fail. This happened before, while not using 'send_image=0', as Matomo returned a GIF.
		await firstValueFrom(this.http.post(url, null, { params, responseType: 'text' }))

	}

	/**
	 * Determines if a failed posting attempt should be counted as a failure.
	 * Failures will only be retried a few times, while all other unsuccessful
	 * attempts can be retried without limitation.
	 * (see {@link RccUsageLoggingQueue})
	 */
	private errorCountsAsFailure(e: unknown) : boolean {

			const unableToReachServer	:	boolean
										=	e instanceof HttpResponse
											&&
											e.status === 0

			const serverError			:	boolean
										=	e instanceof HttpResponse
											&&
											e.status >= 500
											&&
											e.status < 600

			/*
			If we cannot reach the server, chances are good that we
			can do so later, so retrying later makes a lot of sense:

			When the backend is down, it should be
			up and running again soon. If the client's internet connection
			is not working, we can hope that it eventually will again.

			If we get a server error, chances are good that the server
			will get fixed soon, so again retrying later makes a lot of sense.

			If on the other hand we _can_ reach the server and there is
			a non-server error, or if something goes wrong other than the
			actual request (e.g. some runtime error), chances are good, that
			the data is broken somehow or the client has a malfunction, meaning
			that this particular piece of data is not going to go through any time
			soon. We still try again later, but count this as a failure,
			dropping data if posting it failed too often (see {@link RccUsageLoggingQueue})
			*/

			const countAsFailure	: boolean
									= !unableToReachServer && !serverError

		return countAsFailure
	}

	public ngOnDestroy(): void {
		this.destroy$.next()
		this.destroy$.complete()
		this.usageLoggingQueue.disconnect()
	}
}

