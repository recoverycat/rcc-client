import { ModuleWithProviders, NgModule, inject, provideAppInitializer }							from '@angular/core'

import {
			MatomoService
		}							from './matomo.service'
import	{
			MatomoConfig
		}							from './matomo.commons'
import	{
			environment
		} 							from '../../../../app/generic/src/environments/environment'
import	{
			requestPublicRuntimeConfigValue
		}							from '@rcc/common'

@NgModule({
	providers: [
		MatomoService,
		requestPublicRuntimeConfigValue({
			description: 'Matomo site ID',
			path: 'matomo.site-id',
			type: 'string',
			required: true
		}),
		provideAppInitializer(() => {
			inject(MatomoService)
		})
	],
	declarations: [],
})

export class MatomoModule {

	public static forRoot(config: MatomoConfig) : ModuleWithProviders<MatomoModule> {

		if (environment.matomo.disabled)
			return

		if (config.url === '' || config.url === undefined)
			throw new Error('Logging endpoint is not defined')

		return 	{
			ngModule: 	MatomoModule,
			providers:	[
				{
					provide: 	'MATOMO_LOGGING_ENDPOINT',
					useValue: 	config.url
				}
			]
		}

	}


}
