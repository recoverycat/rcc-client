import	{
			HttpClient,
			HttpParams,
			HttpResponse
		}												from '@angular/common/http'

import	{
			Subject,
			Observable,
			firstValueFrom,
			BehaviorSubject,
			toArray,
			take,
			throwError,
			of
		}												from 'rxjs'

import	{
			PersistentQueueSubject,
			RccPublicRuntimeConfigService
		}												from '@rcc/common'

import	{
			RccUsageLoggingService,
			JobAnd,
			LoggingData,
			RccUsageLoggingJob,
			QueuedLoggingData,
			RccUsageLoggingQueue,
		}												from '../usage-logging'

import	{
			MatomoService
		}												from './matomo.service'
import {
			NgZone
		} 												from '@angular/core'
import {
			MockNgZone
		}												from '../../../test-utils'

describe('RccUsageLoggingQueue', () => {


	let mockUsageLoggingQueue		:	RccUsageLoggingQueue
									=	undefined

	const mockJob					:	RccUsageLoggingJob
									=	{
											id					: 'mock-job-ID',
											tick$				: new Subject(),
											description			: 'mock-job-description',
											label				: 'mock-job-label',
											fixedLoggingData	: { category:'mock-category', key:'mock-key' },
											requiresUserConsent	: false,
											userId				: 'none',
										}

	let mockEvents					:	Subject<JobAnd<LoggingData>>
									=	undefined

	let onlineSpy					:	jasmine.Spy
									=	undefined

	let storage						:	BehaviorSubject<QueuedLoggingData[]>
									=	undefined

	let mockQueuedLoggingData$		:	PersistentQueueSubject<QueuedLoggingData>
									=	undefined

	let mockUsageLoggingService		:	RccUsageLoggingService
									=	undefined

	let mockRuntimeConfig			:	RccPublicRuntimeConfigService
									=	undefined

	let mockHttpClient				:	HttpClient
									=	undefined

	let postSpy						:	jasmine.Spy
									=	undefined

	let matomoService				:	MatomoService
									=	undefined

	const mockNgZone				: MockNgZone
									= new MockNgZone()

	beforeEach( () => {

		postSpy								=	jasmine.createSpy('post')

		storage								=	new BehaviorSubject<QueuedLoggingData[]>([])
		onlineSpy							=	spyOnProperty(navigator, 'onLine', 'get')

		mockEvents							=	new Subject<JobAnd<LoggingData>>()

		mockUsageLoggingService				=	{
													event$: mockEvents,
													isUsageLoggingJobRegistered: () => true,
													isUsageLoggingJobAllowed: () => true,
													getQueue: () => mockUsageLoggingQueue
												} as unknown as RccUsageLoggingService


		mockQueuedLoggingData$				=	new PersistentQueueSubject<QueuedLoggingData>(
													queue => {
														storage.next(queue)
														return Promise.resolve()
													},
													() => firstValueFrom(storage)
												)


		mockUsageLoggingQueue 					= 	new RccUsageLoggingQueue(
														mockUsageLoggingService,
														mockQueuedLoggingData$,
													)

		mockHttpClient						=	{
													post: postSpy
												} as unknown as HttpClient

		mockRuntimeConfig					=	{
													get: () => Promise.resolve('mock-site-id')
												} as unknown as RccPublicRuntimeConfigService

		matomoService						=	new MatomoService(
													'https://matomo-endpoint-url',
													mockHttpClient,
													mockRuntimeConfig,
													mockUsageLoggingService,
													mockNgZone as NgZone
												)

		onlineSpy.and.returnValue(true)
	})

	afterEach( ()=> {
		matomoService.ngOnDestroy()
	})

	describe('constructor', () => {

		it('should throw an error if the endpoint url was not provided', () => {
			expect(
				() => 	new MatomoService(
							undefined,
							mockHttpClient,
							mockRuntimeConfig,
							mockUsageLoggingService,
							mockNgZone as NgZone
						)
			).toThrow()
		})

		it('should throw an error if the endpoint url was invalid', () => {
			expect(
				() => 	new MatomoService(
							'bli-bla-blub',
							mockHttpClient,
							mockRuntimeConfig,
							mockUsageLoggingService,
							mockNgZone as NgZone
						)
			).toThrow()
		})

		it('should throw an error if the endpoint url is valid, but does not start with "https:"', () => {
			const url	: string
						= 'http://blub.de'

			expect( () => new URL(url)).not.toThrow()

			expect(
				() => 	new MatomoService(
							url,
							mockHttpClient,
							mockRuntimeConfig,
							mockUsageLoggingService,
							mockNgZone as NgZone
						)
			).toThrow()
		})

	})

	describe('Interaction with usage logging queue', () => {

		it('should try to post data coming off the queue to correct endpoint with correct siteId.', async () => {

			let   postResolve	: 	(value?:unknown) => void
								=	undefined

			const postPromise	:	Promise<void>
								= 	new Promise(resolve => postResolve = resolve)

			postSpy.and.callFake( () => of(postResolve()) )

			mockEvents.next({
				job: mockJob,
				data: {
					category: 	'mock-category',
					key:		'example-key',
					version:	'00.unit.test',
					flavor:		'unit-test'
				}
			})

			await postPromise

			expect(postSpy).toHaveBeenCalledTimes(1)

			/* eslint-disable */
			const call					= postSpy.calls.first()

			const firstArgument			= call.args[0]

			expect(firstArgument).toBe('https://matomo-endpoint-url/matomo.php')

			const optionsArgument		= call.args[2]

			const httpParamters			= optionsArgument.params

			const queryParameters		= httpParamters.toString()

			expect(httpParamters).toBeInstanceOf(HttpParams)

			expect(httpParamters.get('idsite')).toBe('mock-site-id')

			expect(queryParameters).toContain('mock-category')

			expect(queryParameters).toContain('example-key')

			expect(queryParameters).toContain('00.unit.test')

			expect(queryParameters).toContain('unit-test')
			/* eslint-enable */

		})

		it('should not retry data that was successfully posted, instead should request the next element of the queue.', async () => {


			const nextTwoValues			:	Observable<QueuedLoggingData[]>
										= 	mockQueuedLoggingData$
											.pipe( take(2), toArray() )

			const nextTwoValuesPromise	:	Promise<QueuedLoggingData[]>
										= 	firstValueFrom(nextTwoValues)

			const requestSpy			:	jasmine.Spy
										=	spyOn(mockUsageLoggingQueue, 'requestNext')

			const retrySpy				:	jasmine.Spy
										=	spyOn(mockUsageLoggingQueue, 'retryLater')

			requestSpy.and.callThrough()

			postSpy.and.callFake( () => of(undefined))

			mockQueuedLoggingData$.push({
				jobId: 			'mock-job',
				originalDate: 	Date.now(),
				failures:		0,
				loggingData:	{
									category: 	'mock-category',
									key:		'mock-key-A',
									version:	'mock-version',
									flavor:		'mock-flavor'
								}
			})

			mockQueuedLoggingData$.push({
				jobId: 			'mock-job',
				originalDate: 	Date.now(),
				failures:		0,
				loggingData:	{
									category: 	'mock-category',
									key:		'mock-key-B',
									version:	'mock-version',
									flavor:		'mock-flavor'
								}
			})


			mockQueuedLoggingData$.shift()

			await nextTwoValuesPromise

			expect(postSpy).toHaveBeenCalledTimes(2)

			expect(requestSpy).toHaveBeenCalledWith()

			expect(retrySpy).not.toHaveBeenCalled()
		})

		it('should retry data when Matomo is not reachable and not increase failure count.', async () => {

			const fakeErrorResponse :	HttpResponse<unknown>
									= 	new HttpResponse<unknown>({
											status: 0
										})

			const retrySpy			:	jasmine.Spy
									=	spyOn(mockUsageLoggingQueue, 'retryLater')

			let   retryResolve	: 	(value?:unknown) => void
								=	undefined
			const retryPromise	:	Promise<void>
								= 	new Promise(resolve => retryResolve = resolve)

			postSpy.and.returnValue(throwError( () => fakeErrorResponse))
			retrySpy.and.callFake( () => retryResolve() )

			const data			:	LoggingData
								=	{
										category: 	'mock-retry-A',
										key:		'example',
										version:	'00.unit.test',
										flavor:		'unit-test'
									}

			mockEvents.next({
				job: mockJob,
				data
			})

			await retryPromise

			expect(retrySpy).toHaveBeenCalledTimes(1)

			/* eslint-disable */

			const call				= retrySpy.calls.first()

			const firstArgument		= call.args[0]

			expect(firstArgument).toEqual( jasmine.objectContaining({ loggingData: data }) )

			const secondArgmunet	= call.args[1]

			expect(secondArgmunet).toBe(false)

			/* eslint-enable */

		})

		it('should retry data when Matomo responds with a server error.', async () => {

			const fakeErrorResponse :	HttpResponse<unknown>
									= 	new HttpResponse<unknown>({
											status: 503
										})

			const retrySpy			:	jasmine.Spy
									=	spyOn(mockUsageLoggingQueue, 'retryLater')

			let   retryResolve		: 	(value?:unknown) => void
									=	undefined

			const retryPromise		:	Promise<void>
									= 	new Promise(resolve => retryResolve = resolve)

			postSpy.and.returnValue(throwError( () => fakeErrorResponse))
			retrySpy.and.callFake( () => retryResolve() )

			const data				: 	LoggingData
									=	{
											category: 	'mock-retry-B',
											key:		'example',
											version:	'00.unit.test',
											flavor:		'unit-test'
										}

			mockEvents.next({
				job: mockJob,
				data
			})

			await retryPromise

			expect(retrySpy).toHaveBeenCalledTimes(1)

			/* eslint-disable */

			const call				= retrySpy.calls.first()

			const firstArgument		= call.args[0]

			expect(firstArgument).toEqual(jasmine.objectContaining({ loggingData: data }) )

			const secondArgument	= call.args[1]

			expect(secondArgument).toBe(false)

			/* eslint-enable */

		})


		it('should retry data when Matomo responds with a non-server error and increase failure count.', async () => {

			const fakeErrorResponse :	HttpResponse<unknown>
									= 	new HttpResponse<unknown>({
											status: 404
										})

			const retrySpy			:	jasmine.Spy
									=	spyOn(mockUsageLoggingQueue, 'retryLater')

			let   retryResolve	: 	(value?:unknown) => void
								=	undefined
			const retryPromise	:	Promise<void>
								= 	new Promise(resolve => retryResolve = resolve)

			postSpy.and.returnValue(throwError( () => fakeErrorResponse))
			retrySpy.and.callFake( () => retryResolve() )

			const data			:	LoggingData
								=	{
										category: 	'mock-retry-B',
										key:		'example',
										version:	'00.unit.test',
										flavor:		'unit-test'
									}

			mockEvents.next({
				job: mockJob,
				data
			})

			await retryPromise

			/* eslint-disable */

			const call				= retrySpy.calls.first()

			const firstArgument		= call.args[0]

			expect(firstArgument).toEqual(jasmine.objectContaining({ loggingData: data }) )

			const secondArgument	= call.args[1]

			expect(secondArgument).toBe(true)

			/* eslint-enable */

		})

	})
})
