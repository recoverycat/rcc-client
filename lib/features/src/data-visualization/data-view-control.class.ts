import	{	WidgetControl		}		from '@rcc/common'
import	{	Dataset				}		from './data-sets'


export type DataViewScope = 'week' | 'month' | 'year'

export interface DataviewControlConfig {

	datasets	: Dataset[]
	startDate?	: string			// YYYY-MM-DD
	endDate?	: string			// YYYY-MM-DD
	scope?		: DataViewScope
}


/**
 * For the time being, the DataViewControl comes with only an array of {@link Dataset}s.
 * This will change soon and probably add parameters for start and end dates or other view options.
 *
 * The first element of the Datasets array is considered the primary Dataset. These second the secondary and so on.
 * Widgets don't have to visualize all of them. But if a Widget can only visualize one {@link Dataset} it should
 * not {@Link Widget#match} a DataViewControl with more than one Dataset.
 */
export class DataViewControl extends WidgetControl {

	public datasets		: Dataset[]
	public startDate? 	: string 		// YYYY-MM-DD
	public endDate?		: string		// YYYY-MM-DD
	public scope?		: DataViewScope

	public constructor(
		public	config 	: Dataset[] | DataviewControlConfig // Dataset[] <- legacy
	){
		super()

		if(Array.isArray(config) ){
				this.datasets = config
				return this
		}

		this.datasets 	= config.datasets
		this.startDate 	= config.startDate
		this.endDate	= config.endDate
		this.scope		= config.scope
	}

	/**
	 * @deprecated legacy
	 */
	public get data(): Dataset[] {
		console.warn('DataViewControl.data is deprecated; use .datasets')
		return this.datasets
	}

}
