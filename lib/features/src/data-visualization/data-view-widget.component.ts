import	{
			Component,
		}								from '@angular/core'
import	{
			WidgetComponent,
		}								from '@rcc/common'
import	{	DataViewControl			}	from './data-view-control.class'
import	{	PopupStatus				}	from '../complex-data-view-widgets/complex-data-view-widgets.commons'

/**
 * This class is the base class for all DataViewWidgets.
 * You can still use `WidgetComponent<DataViewControl>` instead,
 * but with {@Link DataViewWidgetComponent} you don't have to touch
 * the generic widget infrastructure at all.
 *
 * Every DataViewWidgetComponent is supposed to extend this class.
 * A data view widget represented by DataViewWidgetComponent is meant to manage
 * that part of the UI that visualized answer data for given {@link Question}s.
 *
 * A data view widget is _not_ concerned with how the answer is stored, how to
 * load it or how to change it.
 * Instead it brings up a chart or something similar (e.g. a table) that fits
 * the parameters provided by a {@link DataViewControl}.
 *
 * Each data view widget focuses on one type of visualization mechanism;
 * the static widgetMatch method checks if that mechanism is suitable for a
 * given set of visualization parameters.
 *
 * Every extension of DataViewWidgetComponent will receive an instance of
 * {@link DataViewControl} as parameter to its constructor. This DataViewControl
 * instance has all the information about which data should be visualized for
 * which time frame and comes with additional properties to interact with the
 * rest of the UI.
 *
 * When implementing your own constructor, make sure to pass the DataViewControl
 * instance on to super().
 *
 * Here's how to use this class:
 *
 * ```ts
 *
 * import	{	Component			}		from '@angular/core'
 *
 * import	{
 * 				DataViewControl,
 * 				DataViewWidgetComponent
 * 			}								from '@rcc/features'
 *
 *
 * @Component({
 *	templateUrl: 	'./my-data-view-widget.component.html',
 *	styleUrls: 		['./my-data-view.component.scss'],
 * })
 * export class MyDataViewWidgetComponent extends DataViewWidgetComponent {
 *
 *	static widgetMatch(dataViewControl: DataViewControl): number{
 *
 *		return	dataViewControl.datasets[0].question?.tags?.includes('my-special-tag')
 *				?	 2
 *				:	-1
 *	}
 *
 * 	// You can skip the constructor, if you don't do anything with it.
 * 	public constructor(
 * 		public dataViewControl: DataViewControl
 * 	){
 * 		super(dataViewControl)
 *
 * 		//...
 * 	}
 *
 * }
 *
 * ```
 *
 */
 @Component({
    template: 'Extension of DataViewWidgetComponent is missing a template.',
    standalone: false
})
export class DataViewWidgetComponent extends WidgetComponent<DataViewControl> {

	public static controlType: typeof DataViewControl = DataViewControl

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	public static widgetMatch(_: DataViewControl): number {

		console.warn('static widgetMatch() not implemented for '+this.name )

		return 	-1
	}

	public constructor(
		public dataViewControl: DataViewControl,
	){
		super(dataViewControl)
	}

	protected popupPositionLeft: string = '0'

	protected showPopup: boolean = false

	protected closePopup(): void {
		this.showPopup = false
	}

	protected popupText: string = ''

	protected popupDate: Date

	protected togglePopup(popup: PopupStatus): void {
		this.popupPositionLeft = `${popup.xPositionPercentage}%`
		this.popupText = popup.text
		this.popupDate = popup.date

		// We want to save the existing value first, so that
		// we can toggle the appearance of the note, otherwise
		// the clickoutside event gets called, and the popup will
		// re-open
		const currentValue: boolean = this.showPopup

		// If we open the popup immediately, the click event
		// registers as a clickOutside, as it wasn't within the
		// popup at the time of clicking, therefore it closes
		// immediately. To prevent this we open the popup on
		// a slight delay.
		setTimeout(() => {
			this.showPopup = !currentValue
		}, 1)
	}

	private _dateFormat: Intl.DateTimeFormatOptions = {
		weekday: 'long',
		day: '2-digit',
		month: '2-digit',
		year: 'numeric'
	}
	protected dateFormat: Record<string, string> = this._dateFormat as Record<string, string>
}
