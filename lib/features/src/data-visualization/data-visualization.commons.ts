import { TimeLocaleDefinition } from 'd3-time-format'
import { DataViewControl, DataViewScope } from './data-view-control.class'
import { BaseChartConfig, ChartType, ConsumableDataBase, DatapointBasics, OptionBasics, OptionInteger, RawDataString, generateConsumableDataByTimePeriod } from '@rcc/figures.cc/index'
import { Datapoint, Dataset } from '.'
import { CalendarDateString, assert, differenceInCalendarDays } from '@rcc/core'

/**
 * Converts rcc data to data consumable by figures.cc charts
 */
export function getConsumableChartData<T extends ConsumableDataBase>(dataViewControl: DataViewControl, chartType: ChartType, options?: OptionInteger[]): T[] {

	if(!dataViewControl) return []


	if (dataViewControl.datasets[1] != null)
		assert(
			dataViewControl.datasets[1]?.question.type === 'string',
			'getConsumableChartData: secondary dataset must refer to a question with string answers.',
		)

	const mainDataPoints 	: 	Datapoint[]
							= 	dataViewControl.datasets[0].datapoints
								.map( datapoint => ({
									...datapoint,
									// The charts expect note to always be a string (not null)
									note: datapoint.note ?? ''
								}))

	// The charts expect the notes dataset to be present, even if we don't want to render notes
	const notesDataset		: 	Dataset
							= 	dataViewControl.datasets[1] ?? {
								datapoints: [],
								question: null
							}

	const startDate			: 	Date
							= 	CalendarDateString.toDate(dataViewControl.startDate)

	const endDate			: 	Date
							= 	CalendarDateString.toDate(dataViewControl.endDate)

	const consumableData 	:	T[]
							=	generateConsumableDataByTimePeriod({
									dataPoints:		mainDataPoints 	as DatapointBasics[],
									notes:			notesDataset 	as unknown as RawDataString,
									startDate:		startDate,
									daysAhead:		differenceInCalendarDays(endDate,startDate),
									timePeriod: 	dataViewControl.scope,
									chartType:		chartType,
									options,
								}) as unknown as BaseChartConfig<T>['data']

	return consumableData
}

/**
 * Options are meant to be used as y-Axis labels for certain figures.cc charts.
 * They are just a copy of rcc question options.
 */
export function getChartOptions<T extends OptionBasics>(dataViewControl: DataViewControl) : T[] {
	if(!dataViewControl) return undefined
	return dataViewControl.datasets[0]?.question.options as T[]
}

type Dimensions = BaseChartConfig<ConsumableDataBase>['dimensions']

/**
 * Calculates the dimension for a figures.cc chart. Use this function to ensure
 * that all the charts align properly.
 */
export function getChartDimensions(width: number = 400, height?: number): Dimensions {
	return {
		height,
		width,
		margin: {
			top: 0,
			right: 0,
			bottom: Math.max(width * 0.05, 32),
			left: Math.max(width * 0.1, 54),
		},
	}
}

export const localeDE: TimeLocaleDefinition = {
	dateTime:	'%A, der %e. %B %Y, %X',
	date:	'%d.%m.%Y',
	time:	'%H:%M:%S',
	periods:	[
				'AM', 'PM'
			],
	days: [
		'Sonntag',
		'Montag',
		'Dienstag',
		'Mittwoch',
		'Donnerstag',
		'Freitag',
		'Samstag',
	],
	shortDays: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
	months: [
		'Januar',
		'Februar',
		'März',
		'April',
		'Mai',
		'Juni',
		'Juli',
		'August',
		'September',
		'Oktober',
		'November',
		'Dezember',
	],
	shortMonths: ['J', 'F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D'],
}

export const localeEN : TimeLocaleDefinition = {

	dateTime:	'%A, %e. %B %Y, %X',
	date:	'%d.%m.%Y',
	time:	'%H:%M:%S',
	periods:	[
				'AM', 'PM'
			],
	days: [
		'Sunday',
		'Monday',
		'Tuesday',
		'Wednesday',
		'Thursday',
		'Friday',
		'Saturday',
	],
	shortDays: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
	months: [
		'January',
		'February',
		'March',
		'April',
		'May',
		'June',
		'Juli',
		'August',
		'September',
		'October',
		'November',
		'December',
	],
	shortMonths: ['J', 'F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D'],
}

export const locales: Record<string, TimeLocaleDefinition> = {
	de: localeDE,
	en: localeEN,
}

interface CommonChartOptions {
	startDate: Date
	endDate: Date
	timePeriod: DataViewScope
	dimensions: Dimensions
	locales: Record<string, TimeLocaleDefinition>
}

export function commonChartOptions(dataViewControl: DataViewControl, width?: number, height?: number): CommonChartOptions {
	return {
		startDate: CalendarDateString.toDate(dataViewControl.startDate),
		endDate: CalendarDateString.toDate(dataViewControl.endDate),
		timePeriod: dataViewControl.scope,
		dimensions: getChartDimensions(width, height),
		locales,
	}
}
