import BaseChart from '@rcc/figures.cc/rcc-dataviz/src/components/charts/base-chart'
import { DataViewControl } from '../data-view-control.class'
import { ConsumableDataBase } from '@rcc/figures.cc/index'

export interface ChartSettings {
	language: string
	height?: number
	width?: number
	onTick?: (event: Event) => void
	style: {
		main: string,
		secondary: string,
	}
	filterTicks?: boolean
	aspectCompressed?: number
}

export interface ChartStrategy {
	createChart: (dataViewControl: DataViewControl, settings: ChartSettings) => BaseChart<ConsumableDataBase>,
}
