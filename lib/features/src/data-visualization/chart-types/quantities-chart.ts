import { COLORS_DEFAULT, QuantitiesChart } from '@rcc/figures.cc/index'
import { DataViewControl } from '../data-view-control.class'
import { commonChartOptions, getChartOptions, getConsumableChartData } from '../data-visualization.commons'
import { ChartSettings, ChartStrategy } from './chart-types'

function createChart(dataViewControl: DataViewControl, settings: ChartSettings): QuantitiesChart {
	return new QuantitiesChart({
		data: getConsumableChartData(dataViewControl, 'quantities'),
		options: getChartOptions(dataViewControl),
		...commonChartOptions(dataViewControl, settings.width, settings.height),
		language: settings.language,
		onTick: settings.onTick ?? (() => {}),
		styles: {
			color: {
				primary:	settings.style.main,
				inactive:	COLORS_DEFAULT.greyBack,
			},
			note: {
				backgroundColor: settings.style.secondary,
			}
		},
		filterTicks: settings.filterTicks,
		aspectCompressed: settings.aspectCompressed,
	})
}

export const quantitiesChart: ChartStrategy = {
	createChart,
}
