import { COLORS_DEFAULT, SliderChart } from '@rcc/figures.cc/index'
import { DataViewControl } from '../data-view-control.class'
import { commonChartOptions, getConsumableChartData } from '../data-visualization.commons'
import { Dataset } from '../data-sets'
import { ChartSettings, ChartStrategy } from './chart-types'

function createChart(dataViewControl: DataViewControl, settings: ChartSettings): SliderChart {
	const { min, max } = determineMinMaxValues(dataViewControl)

	return new SliderChart({
		data: getConsumableChartData(dataViewControl, 'slider'),
		...commonChartOptions(dataViewControl, settings.width, settings.height),
		language: settings.language,
		onTick: settings.onTick ?? (() => {}),
		min,
		max,
		styles: {
			color: {
				primary: 	settings.style.main,
				inactive:	COLORS_DEFAULT.greyBack,
			},
			note: {
				backgroundColor: settings.style.secondary
			}
		},
		filterTicks: settings.filterTicks,
		aspectCompressed: settings.aspectCompressed,
	})
}

function determineMinMaxValues(dataViewControl: DataViewControl): { min: number, max: number } {
	const primaryDataSet: Dataset = dataViewControl.datasets[0]
	const minFromQuestion: number | undefined = primaryDataSet.question.min
	const maxFromQuestion: number | undefined = primaryDataSet.question.max

	if (minFromQuestion != null && maxFromQuestion != null)
		return { min: minFromQuestion, max: maxFromQuestion }

	const values: number[] = primaryDataSet.datapoints.map(datapoint => datapoint.value as number)

	const min: number = minFromQuestion ?? Math.min(...values)
	const max: number = maxFromQuestion ?? Math.max(...values)

	return { min, max }
}

export const sliderChart: ChartStrategy = {
	createChart,
}
