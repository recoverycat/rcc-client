import { ChartSettings, ChartStrategy } from './chart-types'
import { COLORS_DEFAULT, YesNoChart } from '@rcc/figures.cc/index'
import { DataViewControl } from '../data-view-control.class'
import { commonChartOptions, getChartOptions, getConsumableChartData } from '../data-visualization.commons'

function createChart(dataViewControl: DataViewControl, settings: ChartSettings): YesNoChart {
	return new YesNoChart({
		data: getConsumableChartData(dataViewControl, 'yesNo'),
		options: getChartOptions(dataViewControl),
		...commonChartOptions(dataViewControl, settings.width, settings.height),
		language: settings.language,
		onTick: settings.onTick ?? (() => {}),
		styles: {
			color: {
				primary: settings.style.main,
				inactive: COLORS_DEFAULT.greyBack,
			},
			note: {
				backgroundColor: settings.style.secondary,
			},
		},
		filterTicks: settings.filterTicks,
		aspectCompressed: settings.aspectCompressed,
	})
}

export const booleanChart: ChartStrategy = {
	createChart,
}
