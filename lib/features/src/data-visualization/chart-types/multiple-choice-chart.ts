import { COLORS_DEFAULT, MultipleChoice, OptionInteger } from '@rcc/figures.cc/index'
import { DataViewControl } from '../data-view-control.class'
import { ChartSettings, ChartStrategy } from './chart-types'
import { commonChartOptions, getChartOptions, getConsumableChartData } from '../data-visualization.commons'
import { Question, QuestionOptionConfig, indexArray } from '@rcc/core'
import { Datapoint, Dataset } from '../data-sets'

function createChart(dataViewControl: DataViewControl, settings: ChartSettings): MultipleChoice {
	const convertedDataViewControl: DataViewControl = convertDataViewControlToIntegerValues(dataViewControl)
	const options: OptionInteger[] = getChartOptions(convertedDataViewControl)

	return new MultipleChoice({
		data: getConsumableChartData(convertedDataViewControl, 'multipleChoice', options),
		options,
		...commonChartOptions(convertedDataViewControl, settings.width, settings.height),
		language: settings.language,
		onTick: settings.onTick ?? (() => {}),
		styles: {
			color: {
				primary:	settings.style.main,
				inactive:	COLORS_DEFAULT.greyBack,
				scale:		new Map(options.map((value) => [value.value, settings.style.main]))
			},
			note: {
				backgroundColor: settings.style.secondary,
			}
		},
		filterTicks: settings.filterTicks,
		aspectCompressed: settings.aspectCompressed,
	})
}

function convertDataViewControlToIntegerValues(dataViewControl: DataViewControl): DataViewControl {
	const dataset: Dataset = dataViewControl.datasets[0]
	const valueToIndexMap: Map<string | number | boolean, { option: QuestionOptionConfig, index: number }>
		= indexArray(dataset.question.options.map((option, index) => ({ option, index })), item => item.option.value)


	const dataPointsConvertedToIntegers: Datapoint[] = dataset.datapoints.map(
			(datapoint: Datapoint) => {
				const index: number = valueToIndexMap.get(datapoint.value as string | number | boolean).index
				return { ...datapoint, value: index }
			}
	)

	const convertedQuestion: Question = new Question({
		...dataset.question.config,
		options: dataset.question.options.map(option => ({
			...option,
			value: valueToIndexMap.get(option.value).index
		})),
		type: 'integer'
	})

	const convertedDataset: Dataset = {
		...dataset,
		datapoints: dataPointsConvertedToIntegers,
		question: convertedQuestion,
	}

	return ({
		...dataViewControl,
		datasets: [
			convertedDataset,
			...dataViewControl.datasets.slice(1)
		]
	}) as DataViewControl
}

export const multipleChoiceChart: ChartStrategy = {
	createChart,
}
