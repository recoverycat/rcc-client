import 	{
			Schedule
		}								from '@rcc/core'

import	{
			Translator,
			TranslationResult,
			normalizeTranslationKey
		}								from '@rcc/common'


export class ScheduleTranslator extends Translator {

	public compatibleLanguages: string[] = ['en', 'de'] // only works with german and english


	public constructor(){
		super()
	}

	public match(x: unknown): number {
		return 	x instanceof Schedule
				?	2
				:	-1
	}

	public translate(schedule: Schedule): TranslationResult {

		if(! (schedule instanceof Schedule) ) return null

		const intermediate: string = this.toTranslationKeys(schedule)
		return { intermediate }
	}

	/**
	 * Checks if the translations of two inputs (optionally including parameter objects) will likely
	 * result in different translations.
	 */
	public assessDifference(

		input_a		: unknown,
		input_b		: unknown,

	): boolean {

		if(input_a !== input_b) return true

		if(
			// None of the inputs is a Schedule
			!(input_a instanceof Schedule)
			&&
			!(input_b instanceof Schedule)

		) return false

		if(
			// Only one of the inputs is a Schedule
			(input_a instanceof Schedule)
			!==
			(input_b instanceof Schedule)

		) return true

		// Both inputs are Schedules:

		const { daysOfWeek: dow_a, timesOfDay: tod_a } = input_a as Schedule
		const { daysOfWeek: dow_b, timesOfDay: tod_b } = input_b as Schedule

		function shallowArrayDiff(a: unknown[], b: unknown[]) : boolean {

			if(a.length !== b.length) return true

			for(let i: number = 0; i < a.length; i++)
				if(a[i] !== b[i]) return true


			return false
		}

		if(shallowArrayDiff(dow_a, dow_b)) return true
		if(shallowArrayDiff(tod_a, tod_b)) return true

		return false

	}

	private toTranslationKeys(schedule: Schedule): string {

		const { daysOfWeek, timesOfDay, everyDay } = schedule

		const day_names		: string[]	=	daysOfWeek
								.map( (day:number) 		=> normalizeTranslationKey(`SCHEDULES.DAYS_SHORT.${day}`))
								.map( (translationKey 	=> `{{${translationKey}}}`))


		const list_of_days	: string	=	day_names.length > 1
								?	[day_names.slice(0, -1).join(', '), ...day_names.slice(-1)].join(', ')
								:	day_names[0]

		const time_clauses	: string[]	=	timesOfDay
								.map( (time:string) 	=> normalizeTranslationKey(`{{SCHEDULES.TIMES_OF_DAY.AT.${time}`))
								.map( (translationKey 	=> `{{${translationKey}}}`))


		const list_of_times	: string	=	time_clauses.length > 1
								?	[time_clauses.slice(0, -1).join(', '), ...time_clauses.slice(-1)].join(' und ')
								:	time_clauses[0]

		const translation	: string 	=		(everyDay ? '{{SCHEDULES.DAILY}}' : list_of_days)
								+	(list_of_times ? ' '+list_of_times : '')

		return translation
	}

}
