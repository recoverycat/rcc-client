import 	{
			Pipe,
			PipeTransform,
		}								from '@angular/core'

import	{
	ParsedIsoString,
			Schedule
		}								from '@rcc/core'

@Pipe({
    name: 'asLocalDate',
    standalone: false
})
export class AsLocalDatePipe implements PipeTransform {

	public transform(str: string): Date {

		console.error('asLocalDate is deprecated, don`t use! was used for:' + str)

		const parsedIsoString: ParsedIsoString | null = Schedule.parse(str)

		return parsedIsoString && parsedIsoString.asLocalDate || new Date(NaN)

	}

}
