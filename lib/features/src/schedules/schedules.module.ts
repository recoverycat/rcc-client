import	{
			NgModule
		}									from '@angular/core'

import	{
			provideTranslationMap,
			provideTranslator,
			SharedModule,
		}									from '@rcc/common'

import	{
			AsLocalDatePipe,
		}									from './schedules.pipes'

import	{
			ScheduleTranslator,
		}									from './schedule.translator'

import	{
			ScheduleEditComponent,
			ScheduleEditService
		}									from './edit'



import	en	from './i18n/en.json'
import	de	from './i18n/de.json'



@NgModule({
	imports:[
		SharedModule
	],
	providers:[
		provideTranslator(ScheduleTranslator),
		provideTranslationMap('SCHEDULES', { en, de } ),
		ScheduleEditService
	],
	declarations:[
		AsLocalDatePipe,
		ScheduleEditComponent,
	],
	exports:[
		AsLocalDatePipe,
		ScheduleEditComponent,
	]
})
export class ScheduleModule {}
