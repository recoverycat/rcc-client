import	{
			Component,
			AfterViewInit,
			OnDestroy,
			ViewChild,
			ElementRef,
			Inject,
		} 								from '@angular/core'
import	{	FormControl				}	from '@angular/forms'
import	{
			fromEvent,
			Subject,
			takeUntil,
			throttleTime,
		}								from 'rxjs'
import	{
			RccModalController,
			RccToastController
		}								from '@rcc/common'
import		QrScanner					from 'qr-scanner'
import	{	DOCUMENT				}	from '@angular/common'

/**
 * Vertical truncation means the video is being cut off at the sides, and we are
 * showing the middle vertical part of the video. This happens when the document
 * is narrower than the aspect ratio of the video stream.
 *
 * Horizontal truncation means the video is being cut off at the bottom, and we
 * are showing the top half of the video. This happens when the document is wider
 * than the aspect ratio of the video stream.
 */
type VideoTruncation = 'vertical' | 'horizontal'

/**
 * This component opens a modal showing the video stream on the user's device
 * and scans a QR code
 */
@Component({
    selector: 'rcc-qr-code-scanner',
    templateUrl: './scan-modal.component.html',
    styleUrls: ['./scan-modal.component.scss'],
    standalone: false
})
export class QrCodeScanModalComponent implements AfterViewInit, OnDestroy {
	@ViewChild('video')
	public video			: ElementRef<HTMLVideoElement>

	@ViewChild('videoOverlay')
	public videoOverlay		: ElementRef<HTMLDivElement>

	public hasPermission	: boolean
	public availableDevices	: MediaDeviceInfo[]		= []
	public currentDevice	: FormControl<{ deviceId: string }>	= new FormControl<MediaDeviceInfo>(null)
	public codeReader		: QrScanner				= undefined
	public initializing		: boolean				= true
	public cameraNotFound	: boolean				= false
	public ready			: boolean				= false
	private stream			: MediaStream
	private destroy$		: Subject<void>			= new Subject<void>()

	public constructor(
		private rccModalController 	: RccModalController,
		private rccToastController 	: RccToastController,
		@Inject(DOCUMENT)
		private readonly document	: Document,
	) {}

	private async setupStreamsAndDevices(): Promise<void> {
		try {
			this.stream	=	await navigator.mediaDevices.getUserMedia({ video: { facingMode: 'environment' } })
		} catch(e) {
			this.stream	= 	await navigator.mediaDevices.getUserMedia({ video: true })
		}

		// Get devices: Labels will only be present until tracks are closed -.-
		const devices : MediaDeviceInfo[]	= 	await navigator.mediaDevices.enumerateDevices()

		this.availableDevices 	= 	devices.filter(device => device.kind === 'videoinput')

		const defaultCameraId: string	= 	this.stream
											.getVideoTracks()[0]
											.getSettings().deviceId

		if(!defaultCameraId) throw new Error('Camera not found')

		const default_device : MediaDeviceInfo	=	this.availableDevices
													.find(device => device.deviceId === defaultCameraId)
		this.currentDevice.setValue(default_device)

		await this.scan()
	}

	private scanCallback(result: string, error?: Error): Promise<void> {
		if (error) throw error
		if (result) {
			this.codeReader.stop()
			return this.success(result)
		}
	}

	public async scan(): Promise<void> {
		this.codeReader	= new QrScanner(
			this.video.nativeElement,
			result => { void this.scanCallback(result.data) },
			{
				highlightScanRegion: true,
				highlightCodeOutline: true,
				overlay: this.videoOverlay.nativeElement,
				/**
				 * By default, the QR code library assumes that we are rendering the video
				 * stream in the same aspect ratio that it's being fed into at. However
				 * due to the way we're rendering the video on the screen, we're either
				 * truncating the video vertically (cutting off the left and right sides)
				 * of the video), or horizontally (cutting off the bottom of the video).
				 * We therefore need to take this into account when setting the area of the
				 * video that is used for scanning.
				 *
				 * The dimensions of the return value however are in relation to the dimensions
				 * of the video file. Note that the videoWidth and videoHeight properties are
				 * _not_ changed by the width of the video element itself, but are intrinsic
				 * properties of the video that is being rendered, in this case the width and
				 * height of camera feed (which may differ by device).
				 *
				 * Note that, at extreme dimensions, (extremely thin or long pages), and depending
				 * on the resolution of the recording device, the QR code reading may struggle to
				 * work, as we've reduced the scanning area too much. This is however enough
				 * of an edge case as to not warrant accounting for. On mobile devices this won't be
				 * relevant, as the page dimensions are constant, and the recording device will
				 * be designed specifically for these dimensions. This problem can only arise
				 * on desktop with a page resized to above normal usage.
				 */
				calculateScanRegion: (video: HTMLVideoElement) => {
					// Dimensions of the video stream. Set by the device input.
					const videoWidth			: number = video.videoWidth
					const videoHeight			: number = video.videoHeight
					const videoAspectRatio		: number = videoWidth / videoHeight

					// Dimensions of the video element. Set by the client.
					const displayWidth			: number = this.document.body.clientWidth
					const displayHeight			: number = this.document.body.clientHeight
					const displayAspectRatio	: number = displayWidth / displayHeight

					const truncation			: VideoTruncation = displayAspectRatio > videoAspectRatio ? 'horizontal' : 'vertical'
					const videoScaleRatio		: number = truncation === 'horizontal' ? displayWidth / videoWidth : displayHeight / videoHeight

					const smallestDimension		: number = Math.min(displayWidth, displayHeight)
					const scanRegionSize		: number = (smallestDimension * .75) / videoScaleRatio

					const visibleVideoHeight	: number = truncation === 'vertical' ? videoHeight : (displayHeight / videoScaleRatio)

					return {
						x: Math.round((video.videoWidth - scanRegionSize) / 2),
						y: Math.round((visibleVideoHeight - scanRegionSize) / 2),
						width: scanRegionSize,
						height: scanRegionSize,
					}
				},
			}
		)

		await this.codeReader.start()
		this.subscribeToResizeEvent()
	}

	public async success(result: string): Promise<void> {
		let content: string | object = undefined

		try {
			content = JSON.parse(result) as object
		} catch(e) {
			content = result
		}

		await this.rccToastController.success('QRCODE.SCAN_SUCCESS')
		this.rccModalController.dismiss(content)
	}

	public cancel(): void {
		this.closeVideoTracks()
		void this.rccToastController.info('QRCODE.SCAN_CANCELLED')
		this.rccModalController.dismiss(null)
	}

	public async ngAfterViewInit(): Promise<void> {
		this.initializing = true

		await 	this.setupStreamsAndDevices()
				.catch((e) => {
					console.warn(e)
					this.cameraNotFound = true
				})
				.then(() => {
					this.ready = true
				})
				.finally(
					()	=> this.initializing = false
				)

		this.currentDevice.valueChanges
			.pipe(takeUntil(this.destroy$))
			.subscribe((device) => {
				this.closeVideoTracks()
				void this.codeReader.setCamera(device.deviceId)
			})

		this.initializing = false
	}

	private closeVideoTracks(): void {
		this.stream?.getVideoTracks()
			.forEach((track) => track.stop())
	}

	public ngOnDestroy(): void {
		this.codeReader?.destroy()
		this.closeVideoTracks()
		this.destroy$.next()
		this.destroy$.complete()
	}

	protected get hasMultipleDevices(): boolean {
		return this.availableDevices.length > 1
	}

	private getCurrentDeviceId(): string {
		const stream			: MediaStream = this.codeReader.$video.srcObject as MediaStream
		const track				: MediaStreamTrack = stream.getVideoTracks()[0]
		const currentDeviceId	: string = track.getSettings().deviceId

		return currentDeviceId
	}

	protected changeDevice(): void {
		const currentDeviceId		: string = this.getCurrentDeviceId()
		const currentDeviceIndex	: number = this.availableDevices.findIndex((device) => device.deviceId === currentDeviceId)

		const nextDeviceIndex		: number = currentDeviceIndex >= this.availableDevices.length - 1 ? 0 : currentDeviceIndex + 1
		const nextDeviceId			: string = this.availableDevices[nextDeviceIndex].deviceId

		void this.codeReader.setCamera(nextDeviceId)
	}

	private subscribeToResizeEvent(): void {
		fromEvent(window, 'resize').pipe(
			throttleTime(100),
			takeUntil(this.destroy$),
		).subscribe(() => {
			this.reloadScanner()
		})
	}

	private reloadScanner(): void {
		this.codeReader.stop()
		void this.codeReader.start()
	}
}
