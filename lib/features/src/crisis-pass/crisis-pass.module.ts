import { NgModule } from '@angular/core'
import { RccCrisisPassOverviewComponent } from './crisis-pass-overview/crisis-pass-overview.component'
import { SharedModule, provideTranslationMap } from '@rcc/common'

import en from './i18n/en.json'
import de from './i18n/de.json'

@NgModule({
	imports: [
		SharedModule,
	],
	declarations: [
		RccCrisisPassOverviewComponent,
	],
	exports: [
		RccCrisisPassOverviewComponent,
	],
	providers: [
		provideTranslationMap('CRISIS_PASS', { en, de })
	]
})
export class RccCrisisPassModule {

}
