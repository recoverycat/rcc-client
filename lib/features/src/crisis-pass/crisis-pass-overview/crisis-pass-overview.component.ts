import { Component } from '@angular/core'

const crisisPassEntries: string[] = [
  'USE_YOUR_RESOURCES',
  'CONTACT_TRUSTED_PERSON',
  'CALL_THIS_NUMBER',
  'TAKE_YOUR_ON_DEMAND_MEDICATION',
  'CONTACT_YOUR_PRACTITIONER',
  'GO_TO_THE_NEXT_RESCUE_CENTER',
  'CALL_EMERGENCY_NUMBER'
]

@Component({
    selector: 'rcc-crisis-pass-overview',
    templateUrl: './crisis-pass-overview.component.html',
    styleUrls: ['./crisis-pass-overview.component.scss'],
    standalone: false
})
export class RccCrisisPassOverviewComponent {
	protected crisisPassEntries: string[] = crisisPassEntries.map(entry => `CRISIS_PASS.${entry}`)
}
