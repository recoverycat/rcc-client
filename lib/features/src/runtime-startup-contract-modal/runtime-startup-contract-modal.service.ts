import { Injectable } from '@angular/core'
import { RccKeyValueStorageService, RccModalController, RccPublicRuntimeConfigService } from '@rcc/common'
import { assertContractModalConfig, runtimeConfigPath } from './runtime-startup-contract-modal.commons'
import { RccStartupContractModalComponent } from './startup-contract-modal/startup-contract-modal.component'
import { Router } from '@angular/router'

@Injectable()
export class RccRuntimeStartupContractModalService {
	public constructor(
		private readonly rccPublicRuntimeConfigService	: RccPublicRuntimeConfigService,
		private readonly rccModalController				: RccModalController,
		private readonly rccKeyValueStorageService		: RccKeyValueStorageService,
		private readonly router							: Router,
	) {}

	public async run(): Promise<void> {
		const config: unknown = await this.rccPublicRuntimeConfigService.get(runtimeConfigPath)
		if (config == null) return

		assertContractModalConfig(config)

		if (await this.userHasAlreadyConfirmed(config.userConfirmationKey)) return

		const excludedRoutes: Set<string> = new Set(config.excludedRoutes)

		if (excludedRoutes.has(this.router.url)) return

		const result: boolean = await this.rccModalController.present(
			RccStartupContractModalComponent,
			{ content: config.content },
			{ mini: true, canDismiss: false },
		)

		if (result === true)
			await this.saveUserHasConfirmed(config.userConfirmationKey)
	}

	private async userHasAlreadyConfirmed(confirmationKey: string): Promise<boolean> {
		const confirmationKeyValue: boolean = (await this.rccKeyValueStorageService.get<boolean>(confirmationKey))?.value ?? false
		return confirmationKeyValue
	}

	private async saveUserHasConfirmed(confirmationKey: string): Promise<void> {
		await this.rccKeyValueStorageService.set(confirmationKey, true)
	}
}
