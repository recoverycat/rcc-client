import	{
			NgModule,
			inject,
		}								from '@angular/core'
import	{
			RccUsageLoggingJob,
			provideRccUsageLoggingJob,
		}								from '../usage-logging/usage-logging.commons'
import	{	Factory, provideTranslationMap					}	from '@rcc/common'
import	{
			fromEvent,
			map,
		}								from 'rxjs'
import	{	DOCUMENT				}	from '@angular/common'

import en from './i18n/en.json'
import de from './i18n/de.json'

const loggingJob: Factory<RccUsageLoggingJob> = {
	deps	: [],
	factory	: () => {
		const document: Document = inject(DOCUMENT)

		return {
			id					: 'user-engagement',
			tick$				: fromEvent(document, 'visibilitychange').pipe(
				map(() => ({
					extraValue: document.visibilityState,
				})),
			),
			description			: 'USAGE_LOGGING_ENGAGEMENT.DESCRIPTION',
			label				: 'USAGE_LOGGING_ENGAGEMENT.LABEL',
			fixedLoggingData	: {
				key				: 'focus',
				category		: 'engagement',
			},
			requiresUserConsent	: false,
			userId				: 'none',
		}
	}
}

@NgModule({
	providers: [
		provideTranslationMap('USAGE_LOGGING_ENGAGEMENT', { en, de }),
		provideRccUsageLoggingJob(loggingJob)
	]
})
export class RccUsageLoggingEngagementModule {}
