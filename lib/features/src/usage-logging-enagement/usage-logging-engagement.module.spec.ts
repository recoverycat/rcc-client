import { firstValueFrom } from 'rxjs'
import { DynamicLoggingData } from '../usage-logging/usage-logging.commons'
import { TestBed } from '@angular/core/testing'
import { RccUsageLoggingEngagementModule } from './usage-logging-engagement.module'
import { DOCUMENT } from '@angular/common'
import { UsageLoggingTestService } from '../usage-logging/usage-logging-module-test-helpers'

class MockDocument extends Document {
	private _visibilityState: DocumentVisibilityState = 'visible'
	public get visibilityState(): DocumentVisibilityState {
		return this._visibilityState
	}
	public set visibilityState(visibilityState: DocumentVisibilityState) {
		this._visibilityState = visibilityState
	}
}

describe('RccUsageLoggingEngagementModule', () => {
	let mockDocument: MockDocument = new MockDocument()

	beforeEach(() => {
		mockDocument = new MockDocument()
		TestBed.configureTestingModule({
			imports: [
				RccUsageLoggingEngagementModule,
			],
			providers: [
				UsageLoggingTestService,
				{ provide: DOCUMENT, useValue: mockDocument },
			]
		})
	})

	it('Should log when document becomes visible', async () => {
		const service: UsageLoggingTestService = TestBed.inject(UsageLoggingTestService)

		const promise: Promise<DynamicLoggingData[]> = firstValueFrom(service.jobs)

		mockDocument.visibilityState = 'visible'
		mockDocument.dispatchEvent(new Event('visibilitychange'))

		const result: DynamicLoggingData[] = await promise

		expect(result[0].extraValue).toBe('visible')
	})

	it('Should log when document becomes hidden', async () => {

		const service: UsageLoggingTestService = TestBed.inject(UsageLoggingTestService)

		const promise: Promise<DynamicLoggingData[]> = firstValueFrom(service.jobs)

		mockDocument.visibilityState = 'hidden'
		mockDocument.dispatchEvent(new Event('visibilitychange'))

		const result: DynamicLoggingData[] = await promise

		expect(result[0].extraValue).toBe('hidden')
	})
})
