import	{
			Injectable,
			Inject
		}							from '@angular/core'

import	{
			DOCUMENT
		}							from '@angular/common'

import	{
			FormControl
		}							from '@angular/forms'

import	{
			RccTranslationService,
			RccSettingsService
		}							from '@rcc/common'

import	{
			uuidv4,
			CalendarDateString,
			assert,
			Schedule
		}							from '@rcc/core'

export interface IcsReminderConfig {
	timeString			: string,
	frequency?			: Schedule,
	startDateString?	: string,
}

const iCalStandardDayCodesMap: Record<number, string> = {
	0: 'SU',
	1: 'MO',
	2: 'TU',
	3: 'WE',
	4: 'TH',
	5: 'FR',
	6: 'SA',
}

@Injectable()
export class IcsReminderService {
	public constructor(
		@Inject(DOCUMENT)
		private document				: Document,
		private rccTranslationService	: RccTranslationService,
		private rccSettingsService		: RccSettingsService
	){}

	public async getIcsReminderData(config?: IcsReminderConfig, eventName: string = 'ICS_REMINDER.EVENT_NAME'): Promise<string> {
		await this.rccSettingsService.ready

		const reminderControl	: FormControl<string>	= await this.rccSettingsService.get('day-query-reminder')
		const defaultReminder	: string				= reminderControl.value


		config					= config || {} as IcsReminderConfig
		config.timeString 		= config.timeString || defaultReminder

		assert(typeof config.timeString === 'string',  `IcsReminderService.getIcsReminderData(): bad timeString expected string, got: ${typeof config.timeString}`, config.timeString )
		assert(config.timeString.match(/^\d\d:\d\d$/), 'IcsReminderService.getIcsReminderData(): bad timeString Format, expected HH:MM, got: '+ config.timeString )

		config.frequency 		= config.frequency 			|| new Schedule([[0, 1, 2, 3, 4, 5, 6], []])
		config.startDateString	= config.startDateString 	|| CalendarDateString.today()

		const now			: string[]	=	new Date().toISOString().match(/(\d\d\d\d)-(\d\d)-(\d\d)T(\d\d):(\d\d)/)
		const date			: string	=	now.slice(1,4).join('')
		const time			: string	=	now.slice(4,6).join('')

		const creationDate 	: string	=	`${date}T${time}00Z`
		const uid			: string	= 	uuidv4()+'@recovery.cat'
		// Start of the calendar event AND the recurrence rule
		const start			: string	= 	`${config.startDateString.replaceAll('-','')}T${config.timeString.replace(/:/g,'')}00`
		// End of the calendar event
		// `start` and `end` determine the length of ONE event occurrence
		const end			: string	=	start

		// Event recurrence
		let	frequency		: string	= 	config.frequency.everyDay
											?	'DAILY'
											:	`WEEKLY;BYDAY=${config.frequency.daysOfWeek.map(number => iCalStandardDayCodesMap[number]).join(',')}`
		// End of the recurrence rule
		// Recurrence = [start, endDate[ (= last day of the rrule is the day before endDate)
		const endDate : string = config.frequency.endDate ? CalendarDateString.daysAfter(config.frequency.endDate, 1) : ''
		if (endDate) frequency += `;UNTIL=${endDate.replaceAll('-','')}T${config.timeString.replace(/:/g,'')}00`

		const reminderText	: string	= 	this.rccTranslationService.translate(eventName)

		const icsTemplate 	: string	=	`
			BEGIN:VCALENDAR

				VERSION:2.0
				PRODID:-RecoveryCat

				BEGIN:VEVENT

					DTSTAMP:${creationDate}
					UID:${uid}
					DTSTART:${start}
					RRULE:FREQ=${frequency}
					DTEND:${end}
					SUMMARY:${reminderText}

					BEGIN:VALARM
						ACTION:DISPLAY
						DESCRIPTION:${reminderText}
						TRIGGER:-PT0M
					END:VALARM

				END:VEVENT

			END:VCALENDAR
		`

		return icsTemplate.replace(/^\s*/gm,'')
	}

	public async promptDownload(config?: IcsReminderConfig, filename: string = 'recoverycat.ics', eventName?: string) : Promise<void>{

		const data 				: string			= await this.getIcsReminderData(config, eventName)
		const hiddenElement 	: HTMLAnchorElement	= this.document.createElement('a')

		hiddenElement.href 		= 'data:attachment/text,' + encodeURI(data)
		hiddenElement.target 	= '_blank'
		hiddenElement.download	= filename

		hiddenElement.click()
	}
}
