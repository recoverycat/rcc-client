import	{	NgModule				}	from '@angular/core'

import	{
			provideTranslationMap,
			provideSettingsEntry,
			SettingsEntry,
			Factory,
			RccSettingsService
		}								from '@rcc/common'

import	{
			DayQueryRemindersModule
		}								from '../day-queries/day-query-reminders'

import	{	IcsReminderService		}	from './ics-reminder.service'
import	{
			SymptomCheckMetaStoreService
		}								from '../symptom-checks'
import	{
			Schedule,
			SymptomCheck,
			PeriodicScheduleConfig,
		}								from '@rcc/core'

import en from './i18n/en.json'
import de from './i18n/de.json'

function determineSchedule(symptomChecks: SymptomCheck[]): Schedule {
	if (symptomChecks.length === 0) return undefined

	if (symptomChecks.length === 1) return new Schedule(symptomChecks[0].config.meta.defaultSchedule)

	const scheduleDays: Set<number> = symptomChecks.reduce((currentlyFoundDays, nextSymptomCheck) => {
		const scheduleConfig: PeriodicScheduleConfig = nextSymptomCheck.config.meta.defaultSchedule as PeriodicScheduleConfig

		if (!Array.isArray(scheduleConfig)) return currentlyFoundDays

		const daysToAdd: number[] = scheduleConfig[0]

		daysToAdd.forEach(day => currentlyFoundDays.add(day))
		return currentlyFoundDays
	}, new Set<number>())

	return new Schedule([[...scheduleDays], []])
}


export const icsSettingsEntry: Factory<SettingsEntry> = 	{
										deps: [IcsReminderService, SymptomCheckMetaStoreService, RccSettingsService],
										factory: (
											icsReminderService				: IcsReminderService,
											symptomCheckMetaStoreService	: SymptomCheckMetaStoreService,
											rccSettingsService				: RccSettingsService,
										) : SettingsEntry<null> => ({
											id: 			'ics-reminder',
											label:			'ICS_REMINDER.SETTINGS.DOWNLOAD.LABEL',
											description:	'ICS_REMINDER.SETTINGS.DOWNLOAD.DESCRIPTION',
											icon:			'download',
											type:			'handler' as const,
											handler:		async () => {
												await symptomCheckMetaStoreService.ready
												const symptomChecks: SymptomCheck[] = symptomCheckMetaStoreService.items

												const schedule: Schedule = determineSchedule(symptomChecks)

												const reminderTime: string = (await rccSettingsService.get('day-query-reminder')).value

												void icsReminderService.promptDownload({
													timeString: reminderTime,
													frequency: schedule
												})
											}
										})
									}

@NgModule({
	imports:[
		DayQueryRemindersModule
	],
	providers:[
		IcsReminderService,
		provideTranslationMap('ICS_REMINDER', { en,de }),
		provideSettingsEntry(icsSettingsEntry)
	]

})
export class IcsRemindersServiceModule {

}
