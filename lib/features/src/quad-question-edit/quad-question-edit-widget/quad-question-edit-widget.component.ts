import {
	Component,
}									from '@angular/core'
import {
	GenericQuestionEditWidgetComponent,
	QuestionEditControl
}									from '../../questions'
import {
	RccTranslationService
}									from '@rcc/common'
import {
	AnswerType,
	QuestionConfig,
	QuestionOptionConfig
}									from '@rcc/core'

const quadQuestionAnswerTypes: AnswerType[] = ['integer']

@Component({
    templateUrl: './quad-question-edit-widget.component.html',
    standalone: false
})
export class QuadQuestionEditWidgetComponent extends GenericQuestionEditWidgetComponent {
	public static label: string = 'BASIC_QUESTION_EDIT_WIDGETS.QUAD.LABEL'
	public static controlType: typeof QuestionEditControl = QuestionEditControl

	public answerTypes: AnswerType[] = quadQuestionAnswerTypes

	public static widgetMatch(questionEditControl: QuestionEditControl): number {
		const config: QuestionConfig 		= 	questionEditControl.questionConfig

		// New question, can still be handled:
		if(!config)			return 0

		// Made to handle questions like this:
		if (this.isQuadQuestion(config.options)) return 3

		// Can handle any other question if need be:
		return 0
	}

	/**
	 * @param questionOptionList 	The list of options that should have values of
	 * 								[0, 1, 2, 3] to be considered a quad question
	 *
	 * @returns						True if quad question false if not
	 */
	private static isQuadQuestion(questionOptionList: QuestionOptionConfig[]): boolean {
		if (questionOptionList?.length !== 4) return false

		let isQuadQuestion: boolean = true

		for (let index: number = 0; index < questionOptionList.length; index++) {
			const questionOption: QuestionOptionConfig = questionOptionList[index]
			if (questionOption.value !== index) {
				isQuadQuestion = false
				break
			}
		}

		return isQuadQuestion
	}

	public constructor(
		protected questionEditControl	: QuestionEditControl,
		protected rccTranslationService	: RccTranslationService,
	){
		super(
			questionEditControl,
			rccTranslationService,
		)

		this.setAnswerTypeAsSelectedOrDefault()
		this.limitationControl.setValue('options')
		this.tagControl.disable()

		while (this.optionControls.length > 0)
			this.removeOption(this.optionControls.controls[0])

		for (let index: number = 0; index < 4; index++)
			this.addOption({ value: index, label: '' }, { labelRequired: true })
	}
}
