import { DOCUMENT } from '@angular/common'
import { Inject, Injectable, NgZone } from '@angular/core'
import { RccKeyValueStorageService } from '@rcc/common'
import { differenceInCalendarDays, differenceInMinutes, format, isSameDay } from '@rcc/core'
import { Observable, Subject, fromEvent } from 'rxjs'

/**
 * The day on which to report the usage
 */
const REPORT_INTERVAL_DAYS					: number = 7

const TIME_TRACKING_INTERVAL_MILLISECONDS	: number = 60 * 1000 // One minute

const LAST_SUBMISSION_STORAGE_KEY			: string = 'usage-logging-last-submission'
const STORAGE_PREFIX						: string = 'usage-logging-activity'
const STORAGE_KEY_REGEX						: RegExp = new RegExp(`^${STORAGE_PREFIX}-([0-9]{4}-[0-9]{2}-[0-9]{2})`)

export interface ReportValue {
	date: Date
	minutes: number
}

@Injectable({
	providedIn: 'root'
})
export class RccUsageLoggingTimeTrackingService {
	public constructor(
		@Inject(DOCUMENT)
		private readonly document					: Document,
		private readonly rccKeyValueStorageService	: RccKeyValueStorageService,
		private readonly ngZone						: NgZone
	) {
		void this.setup()
		this.subscribeToVisibilityState()
	}

	private _report$: Subject<ReportValue[]> = new Subject<ReportValue[]>
	public report$: Observable<ReportValue[]> = this._report$.asObservable()

	private async setup(): Promise<void> {
		const lastSubmitted: Date | null = await this.getLastSubmissionDate()
		if (lastSubmitted == null)
			await this.setLastSubmissionDate(new Date())

		await this.startTracking()
	}

	// setInterval is not always an accurate tracker of time,
	// and could "drift" away from the actual time, meaning
	// we want to preserve the timestamp at the start of tracking
	// and use that to track the usage
	private timeAtStartOfTracking		: Date
	private activityAtStartOfTracking	: number = 0
	private timeout						: NodeJS.Timeout

	private async startTracking(): Promise<void> {
		this.timeAtStartOfTracking = new Date()

		if (await this.shouldSubmit()) {
			await this.submitReport()
			await this.clearAllKeys()
			await this.updateLastSubmissionDate()
		}

		const currentValue: number = await this.getStoredMinutes(this.timeAtStartOfTracking)

		if (!Number.isInteger(currentValue))
			await this.setStoredMinutes(this.timeAtStartOfTracking, 0)

		this.activityAtStartOfTracking = currentValue ?? 0

		// Because of Angular not becoming stable in due time, we need to follow the
		// recommendations from here: https://angular.dev/errors/NG0506 as
		// when normally run, it blocks other processes like the UpdateModule.
		// Issue: https://git.recoverycat.de/recoverycat/rcc-client/-/issues/1358
		this.ngZone.runOutsideAngular(() => {

			clearInterval(this.timeout)
			this.timeout = setInterval(() => {
				const currentTime: Date = new Date()

				if (this.document.visibilityState === 'hidden')
					return this.stopTracking()
				const timeToIncrement: number = differenceInMinutes(currentTime, this.timeAtStartOfTracking)

				void this.setStoredMinutes(this.timeAtStartOfTracking, this.activityAtStartOfTracking + timeToIncrement)

				// This should only happen in rare cases, where the user is actively
				// using their app when the time switches to another day (i.e. at midnight)
				if (!isSameDay(this.timeAtStartOfTracking, currentTime)) {
					this.timeAtStartOfTracking = new Date()

					return
				}

			}, TIME_TRACKING_INTERVAL_MILLISECONDS)

		})

	}

	private stopTracking(): void {
		clearInterval(this.timeout)
	}

	private subscribeToVisibilityState(): void {
		fromEvent(this.document, 'visibilitychange')
			.subscribe(() => {
				if (this.document.visibilityState === 'hidden')
					this.stopTracking()
				else
					void this.startTracking()
			})
	}

	private storageKey(date: Date): string {
		const dateString	: string = format(date, 'yyyy-MM-dd')

		return `${STORAGE_PREFIX}-${dateString}`
	}

	private async updateLastSubmissionDate(): Promise<void> {
		await this.setLastSubmissionDate(new Date())
	}

	private async submitReport(): Promise<void> {
		const today: Date = new Date()
		const allStoredDates: ReportValue[] = await this.getAllStoredDates()
		const filteredValues: ReportValue[] = allStoredDates.filter(value => {
			if (differenceInCalendarDays(today, value.date) > REPORT_INTERVAL_DAYS)
				return false
			return true
		})
		this._report$.next(filteredValues)
	}

	private async shouldSubmit(): Promise<boolean> {
		const lastSubmission: Date | undefined = await this.getLastSubmissionDate()

		if (lastSubmission == null)
			return false

		const ageOfLastSubmission: number = differenceInCalendarDays(new Date(), lastSubmission)

		return ageOfLastSubmission >= REPORT_INTERVAL_DAYS
	}

	private async getStoredMinutes(date: Date): Promise<number> {
		const key: string = this.storageKey(date)
		return (await this.rccKeyValueStorageService.get<number>(key))?.value
	}

	private async setStoredMinutes(date: Date, value: number): Promise<void> {
		const key: string = this.storageKey(date)
		await this.rccKeyValueStorageService.set(key, value)
	}

	private async getAllStoredDates(): Promise<ReportValue[]> {
		const keys: string[] = await this.getAllKeys()
		return Promise.all(keys.map(async key => {
			const value: number = await this.rccKeyValueStorageService.getValue<number>(key)
			const [, dateString] = STORAGE_KEY_REGEX.exec(key)
			const date: Date = new Date(dateString)

			return {
				date,
				minutes: value,
			}
		}))
	}

	private async getLastSubmissionDate(): Promise<Date | undefined> {
		const stringValue: string = (await this.rccKeyValueStorageService.get<string>(LAST_SUBMISSION_STORAGE_KEY))?.value
		if (stringValue == null) return undefined
		return new Date(stringValue)
	}

	private async setLastSubmissionDate(date: Date): Promise<void> {
		await this.rccKeyValueStorageService.set(LAST_SUBMISSION_STORAGE_KEY, format(date, 'yyyy-MM-dd'))
	}

	private async getAllKeys(): Promise<string[]> {
		const today: Date = new Date()
		const allKeys: string[] = await this.rccKeyValueStorageService.getAllKeys()
		return allKeys.filter(key => key.match(STORAGE_KEY_REGEX)).filter(key => {
			const [, dateString] = STORAGE_KEY_REGEX.exec(key)
			const date: Date = new Date(dateString)
			if (isSameDay(today, date))
				return false

			return true
		})
	}

	private async clearAllKeys(): Promise<void> {
		const keys: string[] = await this.getAllKeys()
		await Promise.all(keys.map((key) => this.rccKeyValueStorageService.delete(key)))
	}
}
