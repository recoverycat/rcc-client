/* eslint-disable @typescript-eslint/unbound-method */
import { KeyValuePair, RccKeyValueStorageService } from '@rcc/common'
import { RccUsageLoggingTimeTrackingService, ReportValue } from './usage-logging-time-tracking.service'
import { addDays } from 'date-fns'
import { firstValueFrom } from 'rxjs'
import { NgZone } from '@angular/core'
import { MockNgZone } from '../../../test-utils'

type Callback = (event: Event) => void

class MockDocument implements Partial<Document> {
	public visibilityState?: DocumentVisibilityState = 'visible'

	private listeners: Record<string, Callback[]> = {}

	public addEventListener(event: string, callback: Callback): void {
		if (this.listeners[event] == null)
			this.listeners[event] = []

		this.listeners[event].push(callback)
	}

	// Only needed so `fromEvent` from rxjs doesn't fail
	public removeEventListener(): void {}

	public dispatchEvent(event: Event): boolean {
		const listeners: Callback[] = this.listeners[event.type] ?? []

		listeners.forEach(listener => listener(event))

		return true
	}
}

describe('RccUsageLoggingTimeTrackingService', () => {
	let document: MockDocument | undefined = undefined
	let rccKeyValueStorageService: jasmine.SpyObj<RccKeyValueStorageService> | undefined = undefined
	const startTime: Date = new Date('2020-01-01T12:00')
	let keyValuePromises: Set<Promise<unknown>> = new Set()
	let keyValueStore: Record<string, unknown> = {}
	const mockNgZone: MockNgZone = new MockNgZone()

	/**
	 * Whilst waiting for promises from the key value service to resolve, we might
	 * be pushing more promises into the queue. This function awaits all the existing
	 * promises, and keeps looping around until there are no more left.
	 *
	 * Note, due to the weird nature of this service (it's completing asynchronous code as
	 * a result of event listeners, combined with timed intervals), this function sometimes needs
	 * to be called twice, as promises can "escape" the call stack
	 */
	async function awaitAllKeyValuePromises(): Promise<void> {
		do {
			const oldPromises: Promise<unknown>[] = [...keyValuePromises]
			await Promise.all(oldPromises)
			oldPromises.forEach(oldPromise => {
				keyValuePromises.delete(oldPromise)
			})
		} while (keyValuePromises.size > 0)
	}

	beforeEach(() => {
		jasmine.clock().install()
		jasmine.clock().mockDate(startTime)
		rccKeyValueStorageService = jasmine.createSpyObj<RccKeyValueStorageService>(
			'RccKeyValueStorageService',
			['delete', 'get', 'getAllKeys', 'set', 'getValue'],
		)
		keyValuePromises = new Set()
		keyValueStore = {}

		// @ts-expect-error foo
		rccKeyValueStorageService.get.and.callFake((key: string): Promise<KeyValuePair> => {
			const value: unknown = keyValueStore[key]
			const promise: Promise<KeyValuePair> = Promise.resolve({ key, value } as KeyValuePair)
			keyValuePromises.add(promise)
			return promise
		})
		rccKeyValueStorageService.getValue.and.callFake(<T>(key: string): Promise<T> => {
			const value: T = keyValueStore[key] as T
			const promise: Promise<T> = Promise.resolve(value)
			keyValuePromises.add(promise)
			return promise
		})
		rccKeyValueStorageService.set.and.callFake((key, value) => {
			const promise: Promise<void> = Promise.resolve()
			keyValuePromises.add(promise)
			keyValueStore[key] = value
			return promise
		})
		rccKeyValueStorageService.getAllKeys.and.callFake(() => {
			const promise: Promise<string[]> = Promise.resolve(Object.keys(keyValueStore))
			keyValuePromises.add(promise)
			return promise
		})
		rccKeyValueStorageService.delete.and.callFake((key) => {
			const promise: Promise<void> = Promise.resolve()
			keyValuePromises.add(promise)
			delete keyValueStore[key]
			return promise
		})
		document = new MockDocument()
	})

	afterEach(() => {
		jasmine.clock().uninstall()
	})

	async function appBecomesVisible(): Promise<void> {
		if (document == null) return
		document.visibilityState = 'visible'
		document.dispatchEvent(new Event('visibilitychange'))

		await awaitAllKeyValuePromises()
	}

	function appBecomesHidden(): void {
		if (document == null) return
		document.visibilityState = 'hidden'
		document.dispatchEvent(new Event('visibilitychange'))
	}

	function minutes(minutesLength: number): number {
		// Adding 100 milliseconds on to the length of minutes, just
		// to be sure that the interval gets executed
		return minutesLength * 60 * 1000 + 100
	}

	describe('Initialization', () => {
		it('Should initialize last submission to today', async () => {
			new RccUsageLoggingTimeTrackingService(
				document as unknown as Document,
				rccKeyValueStorageService as RccKeyValueStorageService,
				mockNgZone as NgZone
			)

			await awaitAllKeyValuePromises()

			expect(keyValueStore['usage-logging-last-submission']).toEqual('2020-01-01')
		})

		it('Should start tracking', async () => {
			new RccUsageLoggingTimeTrackingService(
				document as unknown as Document,
				rccKeyValueStorageService as RccKeyValueStorageService,
				mockNgZone as NgZone
			)

			await awaitAllKeyValuePromises()
			await awaitAllKeyValuePromises()

			jasmine.clock().tick(minutes(1))

			expect(keyValueStore['usage-logging-activity-2020-01-01']).toBe(1)
		})
	})

	describe('Initialization when document not visible', () => {
		beforeEach(() => {
			appBecomesHidden()
		})

		it('Should not start tracking', async () => {
			new RccUsageLoggingTimeTrackingService(
				document as unknown as Document,
				rccKeyValueStorageService as RccKeyValueStorageService,
				mockNgZone as NgZone
			)

			await awaitAllKeyValuePromises()

			jasmine.clock().tick(minutes(5))

			await awaitAllKeyValuePromises()

			expect(keyValueStore['usage-logging-activity-2020-01-01']).toBe(0)
		})
	})

	describe('Initialization when there had been previous tracking', () => {
		beforeEach(() => {
			keyValueStore['usage-logging-activity-2020-01-01'] = 25
		})

		it('Should resume tracking with the previously saved value', async () => {
			new RccUsageLoggingTimeTrackingService(
				document as unknown as Document,
				rccKeyValueStorageService as RccKeyValueStorageService,
				mockNgZone as NgZone
			)

			await awaitAllKeyValuePromises()
			await awaitAllKeyValuePromises()

			jasmine.clock().tick(minutes(5))

			expect(keyValueStore['usage-logging-activity-2020-01-01']).toBe(30)
		})
	})

	describe('When visibility state changes to hidden', () => {
		it('Should stop tracking', async () => {
			new RccUsageLoggingTimeTrackingService(
				document as unknown as Document,
				rccKeyValueStorageService as RccKeyValueStorageService,
				mockNgZone as NgZone
			)

			await awaitAllKeyValuePromises()
			await awaitAllKeyValuePromises()

			appBecomesHidden()

			jasmine.clock().tick(minutes(1))

			expect(keyValueStore['usage-logging-activity-2020-01-01']).toBe(0)
		})
	})

	describe('When visibility state changes back to visible', () => {
		it('Should restart tracking', async () => {
			new RccUsageLoggingTimeTrackingService(
				document as unknown as Document,
				rccKeyValueStorageService as RccKeyValueStorageService,
				mockNgZone as NgZone
			)

			appBecomesHidden()

			await awaitAllKeyValuePromises()
			await awaitAllKeyValuePromises()

			jasmine.clock().tick(minutes(1))

			expect(keyValueStore['usage-logging-activity-2020-01-01']).toBe(0)

			await appBecomesVisible()
			await awaitAllKeyValuePromises()

			jasmine.clock().tick(minutes(1))

			expect(keyValueStore['usage-logging-activity-2020-01-01']).toBe(1)
		})

		it('Should resume the total tracked time', async () => {
			new RccUsageLoggingTimeTrackingService(
				document as unknown as Document,
				rccKeyValueStorageService as RccKeyValueStorageService,
				mockNgZone as NgZone
			)

			await awaitAllKeyValuePromises()
			await awaitAllKeyValuePromises()

			jasmine.clock().tick(minutes(5))

			expect(keyValueStore['usage-logging-activity-2020-01-01']).toBe(5)

			appBecomesHidden()

			jasmine.clock().tick(minutes(5))

			expect(keyValueStore['usage-logging-activity-2020-01-01']).toBe(5)

			await appBecomesVisible()
			await awaitAllKeyValuePromises()

			jasmine.clock().tick(minutes(10))

			expect(keyValueStore['usage-logging-activity-2020-01-01']).toBe(15)
		})
	})

	describe('When it has been 7 days tracking', () => {
		it('Should submit a report', async () => {
			const service: RccUsageLoggingTimeTrackingService = new RccUsageLoggingTimeTrackingService(
				document as unknown as Document,
				rccKeyValueStorageService as RccKeyValueStorageService,
				mockNgZone as NgZone
			)

			await awaitAllKeyValuePromises()
			await awaitAllKeyValuePromises()
			const reportPromise: Promise<ReportValue[]> = firstValueFrom(service.report$)

			jasmine.clock().tick(minutes(5))

			appBecomesHidden()

			jasmine.clock().mockDate(addDays(startTime, 4))

			await appBecomesVisible()
			await awaitAllKeyValuePromises()

			jasmine.clock().tick(minutes(10))

			appBecomesHidden()

			jasmine.clock().mockDate(addDays(startTime, 7))

			await appBecomesVisible()
			await awaitAllKeyValuePromises()

			const reportValue: ReportValue[] = await reportPromise

			expect(reportValue.length).toBe(2)
			expect(reportValue[0].minutes).toBe(5)
			expect(reportValue[1].minutes).toBe(10)
		})
	})

	describe('When it has been more than 7 days tracking (7th day skipped)', () => {
		it('Should submit a report on the first day after', async () => {
			const service: RccUsageLoggingTimeTrackingService = new RccUsageLoggingTimeTrackingService(
				document as unknown as Document,
				rccKeyValueStorageService as RccKeyValueStorageService,
				mockNgZone as NgZone
			)

			await awaitAllKeyValuePromises()
			const reportPromise: Promise<ReportValue[]> = firstValueFrom(service.report$)

			appBecomesHidden()

			jasmine.clock().mockDate(addDays(startTime, 4))

			await appBecomesVisible()
			await awaitAllKeyValuePromises()

			jasmine.clock().tick(minutes(10))

			appBecomesHidden()

			jasmine.clock().mockDate(addDays(startTime, 8))

			await appBecomesVisible()
			await awaitAllKeyValuePromises()

			const reportValue: ReportValue[] = await reportPromise

			expect(reportValue.length).toBe(1)
			expect(reportValue[0].minutes).toBe(10)
		})

		it('Should skip days more than 7 days ago', async () => {
			const service: RccUsageLoggingTimeTrackingService = new RccUsageLoggingTimeTrackingService(
				document as unknown as Document,
				rccKeyValueStorageService as RccKeyValueStorageService,
				mockNgZone as NgZone
			)

			await awaitAllKeyValuePromises()
			const reportPromise: Promise<ReportValue[]> = firstValueFrom(service.report$)

			jasmine.clock().tick(minutes(5))

			appBecomesHidden()

			jasmine.clock().mockDate(addDays(startTime, 1))

			await appBecomesVisible()
			await awaitAllKeyValuePromises()

			jasmine.clock().tick(minutes(5))

			appBecomesHidden()

			jasmine.clock().mockDate(addDays(startTime, 4))

			await appBecomesVisible()
			await awaitAllKeyValuePromises()

			jasmine.clock().tick(minutes(10))

			appBecomesHidden()

			jasmine.clock().mockDate(addDays(startTime, 9))

			await appBecomesVisible()

			const reportValue: ReportValue[] = await reportPromise

			expect(reportValue.length).toBe(1)
			expect(reportValue[0].minutes).toBe(10)
		})
	})

	describe('When initializing after 7 days', () => {
		beforeEach(() => {
			keyValueStore['usage-logging-last-submission'] = '2019-12-25'
			keyValueStore['usage-logging-activity-2019-12-26'] = 30
			keyValueStore['usage-logging-activity-2019-12-28'] = 50
		})

		it('Should submit the report', async () => {
			const service: RccUsageLoggingTimeTrackingService = new RccUsageLoggingTimeTrackingService(
				document as unknown as Document,
				rccKeyValueStorageService as RccKeyValueStorageService,
				mockNgZone as NgZone
			)

			const reportPromise: Promise<ReportValue[]> = firstValueFrom(service.report$)
			await awaitAllKeyValuePromises()

			const reportValue: ReportValue[] = await reportPromise

			expect(reportValue.length).toBe(2)
			expect(reportValue[0].minutes).toBe(30)
			expect(reportValue[1].minutes).toBe(50)
		})
	})

	describe('When submitting a report', () => {
		beforeEach(() => {
			keyValueStore['usage-logging-last-submission'] = '2019-12-25'
			keyValueStore['usage-logging-activity-2019-12-26'] = 30
			keyValueStore['usage-logging-activity-2019-12-28'] = 50
			keyValueStore['usage-logging-activity-2020-01-01'] = 70
		})

		it('Should not include today', async () => {
			const service: RccUsageLoggingTimeTrackingService = new RccUsageLoggingTimeTrackingService(
				document as unknown as Document,
				rccKeyValueStorageService as RccKeyValueStorageService,
				mockNgZone as NgZone
			)

			const reportPromise: Promise<ReportValue[]> = firstValueFrom(service.report$)
			await awaitAllKeyValuePromises()

			const reportValue: ReportValue[] = await reportPromise

			expect(reportValue.length).toBe(2)
			expect(reportValue[0].minutes).toBe(30)
			expect(reportValue[1].minutes).toBe(50)
		})

		it('Should remove values from the key value store', async () => {
			const service: RccUsageLoggingTimeTrackingService = new RccUsageLoggingTimeTrackingService(
				document as unknown as Document,
				rccKeyValueStorageService as RccKeyValueStorageService,
				mockNgZone as NgZone
			)

			const reportPromise: Promise<ReportValue[]> = firstValueFrom(service.report$)
			await awaitAllKeyValuePromises()

			await reportPromise

			await awaitAllKeyValuePromises()

			expect(keyValueStore['usage-logging-activity-2019-12-26']).toBeUndefined()
			expect(keyValueStore['usage-logging-activity-2019-12-28']).toBeUndefined()
			expect(keyValueStore['usage-logging-activity-2020-01-01']).toBe(70)
		})

		it('Should update the last submission date', async () => {
			const service: RccUsageLoggingTimeTrackingService = new RccUsageLoggingTimeTrackingService(
				document as unknown as Document,
				rccKeyValueStorageService as RccKeyValueStorageService,
				mockNgZone as NgZone
			)

			const reportPromise: Promise<ReportValue[]> = firstValueFrom(service.report$)
			await awaitAllKeyValuePromises()

			await reportPromise

			await awaitAllKeyValuePromises()

			expect(keyValueStore['usage-logging-last-submission']).toBe('2020-01-01')
		})
	})

	describe('When the app is being used at midnight', () => {
		it('Should stop tracking the previous day, and start tracking the new day', async () => {
			new RccUsageLoggingTimeTrackingService(
				document as unknown as Document,
				rccKeyValueStorageService as RccKeyValueStorageService,
				mockNgZone as NgZone
			)

			await awaitAllKeyValuePromises()
			appBecomesHidden()

			jasmine.clock().mockDate(new Date('2020-01-01T23:57'))

			await appBecomesVisible()
			await awaitAllKeyValuePromises()

			jasmine.clock().tick(minutes(6))

			expect(keyValueStore['usage-logging-activity-2020-01-01']).toBe(3)
			expect(keyValueStore['usage-logging-activity-2020-01-02']).toBe(3)
		})
	})
})
