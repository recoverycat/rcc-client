import { NgModule } from '@angular/core'
import { RccUserManualSharedModule } from '../user-manual-shared/user-manual-shared.module'
import { USER_MANUAL_MAP } from '../user-manual-shared/user-manual-shared.common'

import deManual from './files/de-user-manual.pdf'
import enManual from './files/en-user-manual.pdf'

const languageManualMap: Record<string, string> = {
	de: deManual,
	en: enManual,
}

@NgModule({
	imports: [
		RccUserManualSharedModule,
	],
	providers: [
		{ provide: USER_MANUAL_MAP, useValue: languageManualMap },
	],
})
export class RccPatUserManualModule {}
