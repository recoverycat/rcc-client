import { NgModule } from '@angular/core'
import { SharedModule, provideTranslationMap } from '@rcc/common'
import { RccUserManualSectionComponent } from './components/user-manual-section/user-manual-section.component'

import de from './i18n/de.json'
import en from './i18n/en.json'

@NgModule({
	imports: [
		SharedModule
	],
	declarations: [
		RccUserManualSectionComponent,
	],
	exports: [
		RccUserManualSectionComponent,
	],
	providers: [
		provideTranslationMap('USER_MANUAL', { de, en })
	]
})
export class RccUserManualSharedModule {

}
