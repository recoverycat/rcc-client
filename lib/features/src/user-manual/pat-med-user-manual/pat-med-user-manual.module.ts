import { NgModule } from '@angular/core'
import { MedicalFeatureModule } from '@rcc/common/src/medical/medical-feature/medical-feature.module'
import { RccUserManualSharedModule } from '../user-manual-shared/user-manual-shared.module'
import { USER_MANUAL_MAP } from '../user-manual-shared/user-manual-shared.common'
import { provideTranslationMap } from '@rcc/common'

import deManual from './files/Gebrauchsanweisung für Patient innen.pdf'
import enManual from './files/User Manual for Patients.pdf'

import de from './i18n/de.json'
import en from './i18n/en.json'


const languageManualMap: Record<string, string> = {
	de: deManual,
	en: enManual,
}

@NgModule({
	imports: [
		MedicalFeatureModule,
		RccUserManualSharedModule
	],
	providers: [
		{ provide: USER_MANUAL_MAP, useValue: languageManualMap },
		provideTranslationMap('USER_MANUAL', { de, en }),
	],
})
export class RccPatMedUserManualModule {}
