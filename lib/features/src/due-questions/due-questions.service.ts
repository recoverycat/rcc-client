import	{
			Injectable,
		}									from '@angular/core'

import	{
			Observable,
			Subject,
			merge
		}									from 'rxjs'

import	{
			distinctUntilChanged
		}									from 'rxjs/operators'

import	{
			Schedule,
		}									from '@rcc/core'

import	{	JournalService				}	from '../entries'
import	{	QuestionnaireService		}	from '../questions'
import	{	SymptomCheckMetaStoreService }	from '../symptom-checks'
import	{	DueData						}	from './due-questions-commons'



@Injectable()
export class DueQuestionsService extends Observable<DueData> {

	protected journalService				: JournalService
	protected questionnaireService			: QuestionnaireService
	protected symptomCheckMetaStoreService	: SymptomCheckMetaStoreService

	protected mediatorSubject 		: Subject<DueData>

	public constructor(
		journalService					: JournalService,
		questionnaireService			: QuestionnaireService,
		symptomCheckMetaStoreService	: SymptomCheckMetaStoreService
	){

		const mediatorSubject = new Subject<DueData>()

		super( subscriber => {

			const subscription = 	mediatorSubject
									.pipe( distinctUntilChanged(this.isEqualDueData) )
									.subscribe(subscriber)

			this.update()

			return subscription

		})

		this.mediatorSubject 				= mediatorSubject

		this.journalService 				= journalService
		this.questionnaireService			= questionnaireService
		this.symptomCheckMetaStoreService 	= symptomCheckMetaStoreService

		merge(
			Schedule.timeOfDay.changes$,
			Schedule.timeOfDay.configChange$,
			journalService.change$,
			symptomCheckMetaStoreService.change$
		)
		.subscribe( () => this.update() )

	}

	public async getDueData(date = new Date() ) : Promise<DueData> {

		const due_questions_ids			=	await this.symptomCheckMetaStoreService.getDueQuestionsIds(date)
		const time_slot					=	Schedule.timeOfDay.at(date)
		const startDate					=	time_slot.startDate
		const endDate					=	time_slot.endDate

		const recent_entries			=	await this.journalService.filter(due_questions_ids, startDate, endDate)
		const answered_question_ids		=	recent_entries.map( entry => entry.questionId)

		const unanswered_question_ids	=	due_questions_ids.filter( qid => !answered_question_ids.includes(qid) )

		const due_symptom_checks		=	await this.symptomCheckMetaStoreService.getSymptomChecks(unanswered_question_ids)
		const unanswered_questions		=	await this.questionnaireService.get(unanswered_question_ids)

		const due_data 					=	{
												symptomChecks: 	due_symptom_checks,
												questions:		unanswered_questions,
												startDate:		startDate,
												endDate:		endDate
											}

		return due_data
	}

	public isEqualDueData(this:void, due_data_1: DueData, due_data_2: DueData) : boolean {

		if(due_data_1.startDate.getTime()	!== due_data_2.startDate.getTime())	return false
		if(due_data_1.endDate.getTime() 	!== due_data_2.endDate.getTime()) 	return false

		if(due_data_1.questions.length		!== due_data_2.questions.length)		return false
		if(due_data_1.symptomChecks.length	!== due_data_2.symptomChecks.length)	return false

		if(due_data_1.questions.some( 		question 		=> !due_data_2.questions.includes(question))) 			return false
		if(due_data_1.symptomChecks.some( 	symptomCheck 	=> !due_data_2.symptomChecks.includes(symptomCheck))) 	return false

		return true
	}

	public update() : void {

		this.getDueData()
		.then(	currentDueData 	=> this.mediatorSubject.next(currentDueData))
		.catch(	reason 			=> console.warn('DueQuestionsService.update() error: ', reason) )

	}

}
