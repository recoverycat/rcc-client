import	{
			Component,
			OnDestroy
		}								from '@angular/core'
import	{	Location				}	from '@angular/common'
import	{	Subscription			}	from 'rxjs'
import	{
			SymptomCheck,
			Question
		}								from '@rcc/core'
import	{	DueQuestionsService		}	from '../due-questions.service'


@Component({
    templateUrl: './overview-page.component.html',
    styleUrls: ['./overview-page.component.scss'],
    standalone: false
})
export class DueQuestionsOverviewPageComponent implements OnDestroy{

	public symptomChecks	: SymptomCheck[]
	public questionIds		: string[] = []

	private subscription	: Subscription

	public constructor(
		public dueQuestionsService	: DueQuestionsService,
		public location				: Location
	) {

		console.info('DueQuestionsOverviewPageComponent')

		this.subscription =	this.dueQuestionsService
			.subscribe(
				(result: { symptomChecks: SymptomCheck[], questions: Question[] }) 	=>	{
					console.info('DueQuestionsOverviewPageComponent', result)
					this.symptomChecks 	= result.symptomChecks
					this.questionIds 	= result.questions.map( (q: Question) => q.id)
				})

	}

	private goBack() : void {
		this.location.back()
	}

	public ngOnDestroy() : void {
		this.subscription.unsubscribe()
	}

}
