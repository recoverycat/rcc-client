import	{
			NgModule
		}								from '@angular/core'
import 	{
			map,
			merge,
		}								from 'rxjs'
import 	{
			Factory,
			RccTransmissionService,
			provideTranslationMap,
		}								from '@rcc/common'
import	{
			RccUsageLoggingJob,
			provideRccUsageLoggingJob
		}								from '../usage-logging/usage-logging.commons'

import en from './i18n/en.json'
import de from './i18n/de.json'

const loggingJob : Factory<RccUsageLoggingJob> = {
	deps	: [RccTransmissionService],
	factory	: (rccTransmissionService: RccTransmissionService) => ({
		id					: 'transmission-success',
		tick$				:	merge(
									rccTransmissionService.successfulIncomingTransmission$.pipe(
										map(({ service }) => ({ extraValue: 'incoming ' + service.id }))
									),
									rccTransmissionService.successfulOutgoingTransmission$.pipe(
										map(({ service }) => ({ extraValue: 'outgoing ' + service.id }))
									),
								),
		description			: 'USAGE_LOGGING_TRANSMISSION.SUCCESS.DESCRIPTION',
		label				: 'USAGE_LOGGING_TRANSMISSION.SUCCESS.LABEL',
		fixedLoggingData: {
			key				: 'success',
			category		: 'transmission',
		},
		requiresUserConsent	: false,
		userId				: 'none'
	})
}

@NgModule({
	providers: [
		provideTranslationMap('USAGE_LOGGING_TRANSMISSION', { en, de }),
		provideRccUsageLoggingJob(loggingJob),
	]

})
export class RccUsageLoggingTransmissionSuccessModule {}
