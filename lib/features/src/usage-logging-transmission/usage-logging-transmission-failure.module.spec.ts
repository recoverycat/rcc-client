import { DynamicLoggingData } from '../usage-logging/usage-logging.commons'
import { Subject, firstValueFrom } from 'rxjs'
import { TestBed } from '@angular/core/testing'
import { RccUsageLoggingTransmissionFailureModule } from './usage-logging-transmission-failure.module'
import { AbstractTransmissionService, EmissionOnTransmission, RccTransmissionService } from '@rcc/common'
import { UsageLoggingTestService } from '../usage-logging/usage-logging-module-test-helpers'

describe('RccUsageLoggingTransmissionFailureModule', () => {
	const failedIncomingTransmission$: Subject<EmissionOnTransmission> = new Subject()
	const failedOutgoingTransmission$: Subject<EmissionOnTransmission> = new Subject()

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				RccUsageLoggingTransmissionFailureModule,
			],
			providers: [
				UsageLoggingTestService,
				{ provide: RccTransmissionService, useValue: { failedIncomingTransmission$, failedOutgoingTransmission$ } },
			],
		})
	})

	describe('On failed incoming transmission', () => {
		it('should submit incoming service id', async () => {
			const service: UsageLoggingTestService = TestBed.inject(UsageLoggingTestService)

			const promise: Promise<DynamicLoggingData[]> = firstValueFrom(service.jobs)

			failedIncomingTransmission$?.next({
				service: { id: 'id-99' } as AbstractTransmissionService,
				meta: {}
			})

			const result: DynamicLoggingData[] = await promise

			expect(result[0]).toEqual({ extraValue: 'incoming id-99' })
		})
	})

	describe('On failed outgoing transmission', () => {
		it('should submit outgoing service id', async () => {
			const service: UsageLoggingTestService = TestBed.inject(UsageLoggingTestService)

			const promise: Promise<DynamicLoggingData[]> = firstValueFrom(service.jobs)

			failedOutgoingTransmission$?.next({
				service: { id: 'id-99' } as AbstractTransmissionService,
				meta: {}
			})

			const result: DynamicLoggingData[] = await promise

			expect(result[0]).toEqual({ extraValue: 'outgoing id-99' })
		})
	})
})
