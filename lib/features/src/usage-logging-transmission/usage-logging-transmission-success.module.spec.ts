import { DynamicLoggingData } from '../usage-logging/usage-logging.commons'
import { Subject, firstValueFrom } from 'rxjs'
import { TestBed } from '@angular/core/testing'
import { RccUsageLoggingTransmissionSuccessModule } from './usage-logging-transmission-success.module'
import { AbstractTransmissionService, EmissionOnTransmission, RccTransmissionService } from '@rcc/common'
import { UsageLoggingTestService } from '../usage-logging/usage-logging-module-test-helpers'

describe('RccUsageLoggingTransmissionFailureModule', () => {
	const successfulIncomingTransmission$: Subject<EmissionOnTransmission> = new Subject()
	const successfulOutgoingTransmission$: Subject<EmissionOnTransmission> = new Subject()

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				RccUsageLoggingTransmissionSuccessModule,
			],
			providers: [
				UsageLoggingTestService,
				{ provide: RccTransmissionService, useValue: { successfulIncomingTransmission$, successfulOutgoingTransmission$ } },
			],
		})
	})

	describe('On failed incoming transmission', () => {
		it('should submit incoming service id', async () => {
			const service: UsageLoggingTestService = TestBed.inject(UsageLoggingTestService)

			const promise: Promise<DynamicLoggingData[]> = firstValueFrom(service.jobs)

			successfulIncomingTransmission$?.next({
				service: { id: 'id-99' } as AbstractTransmissionService,
				meta: {}
			})

			const result: DynamicLoggingData[] = await promise

			expect(result[0]).toEqual({ extraValue: 'incoming id-99' })
		})
	})

	describe('On failed outgoing transmission', () => {
		it('should submit outgoing service id', async () => {
			const service: UsageLoggingTestService = TestBed.inject(UsageLoggingTestService)

			const promise: Promise<DynamicLoggingData[]> = firstValueFrom(service.jobs)

			successfulOutgoingTransmission$?.next({
				service: { id: 'id-99' } as AbstractTransmissionService,
				meta: {}
			})

			const result: DynamicLoggingData[] = await promise

			expect(result[0]).toEqual({ extraValue: 'outgoing id-99' })
		})
	})
})
