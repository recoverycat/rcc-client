import	{
	NgModule,
	Provider,
	ModuleWithProviders,
}											from '@angular/core'
import	{
	Action,
	getEffectivePosition,
	WithPosition,
	provideMainMenuEntry
}											from '@rcc/common'

import	{
	RccResourcesAndCrisisPlanModule,
	RESOURCES_AND_CRISIS_PASS_PATH
}											from './resources-and-crisis-pass.module'


@NgModule({
imports: 	[
				RccResourcesAndCrisisPlanModule,
			],

})

export class RccResourcesAndCrisisPlanMainMenuEntryModule {

/**
* This method can add entries to the main menu.
*
* Calling it without parameter, will add entries to the main menu
* automatically at reasonably adequate positions.
*
* If you want to determine the positions yourself provide a `config` parameter :
*
* | .position  | value                  | effect                                                    |
* |------------|------------------------|-----------------------------------------------------------|
* | .position  | true                   | adds menu entry at a reasonably adequate position         |
* |            | positive number        | adds menu entry at position counting from the top         |
* |            | negative number        | adds menu entry at position counting from the bottom      |
*
* Example:     `HomePageMainMenuEntryModule.addEntry({ position: 1 })`,
*/

	public static addEntry(config?: WithPosition): ModuleWithProviders<RccResourcesAndCrisisPlanMainMenuEntryModule> {

		const mainMenuEntry	: Action =	{
				position:		getEffectivePosition(config, .5),
				icon: 			'compass',
				label: 			'RESOURCES_AND_CRISIS_PASS.PAGE_TITLE',
				path:			RESOURCES_AND_CRISIS_PASS_PATH,
		}

		const providers : Provider[] = [provideMainMenuEntry(mainMenuEntry)]

		return {
			ngModule:	RccResourcesAndCrisisPlanMainMenuEntryModule,
			providers
		}
	}

}
