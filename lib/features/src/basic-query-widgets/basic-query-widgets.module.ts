import	{	NgModule, Type 					}	from '@angular/core'
import	{
			SharedModule,
			WidgetsModule,
			provideTranslationMap,
			provideWidget
		}									from '@rcc/common'
import	{	QuestionnaireServiceModule	}	from '../questions'
import	{
			LargeScaleQueryWidgetComponent
		}									from './large-scale/large-scale.component'
import	{	ScaleQueryWidgetComponent	}	from './scale/scale.component'
import	{	SelectQueryWidgetComponent	}	from './select/select.component'
import	{	TextQueryWidgetComponent	}	from './text/text.component'


import en from './i18n/en.json'
import de from './i18n/de.json'

const widgets: Type<unknown>[]
				=	[
						ScaleQueryWidgetComponent,
						LargeScaleQueryWidgetComponent,
						SelectQueryWidgetComponent,
						TextQueryWidgetComponent,
					]

@NgModule({
	imports: [
		SharedModule,
		QuestionnaireServiceModule,
		WidgetsModule,
	],
	providers: [
		...widgets.map(provideWidget),
		provideTranslationMap('BASIC_QUERY_WIDGETS', { en, de }),
	],
	declarations: [
		...widgets
	],
	exports: [
		...widgets
	]
})
export class BasicQueryWidgetsModule { }
