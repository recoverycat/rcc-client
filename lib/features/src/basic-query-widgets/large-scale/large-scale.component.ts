import { Component, ElementRef, Inject, OnDestroy, Optional, ViewChild } from '@angular/core'
import { QueryControl, QueryWidgetComponent } from '../../queries'
import { FormControl } from '@angular/forms'
import { Subject, take, takeUntil } from 'rxjs'
import { GREYED_OUT } from '@rcc/common'

@Component({
    templateUrl: './large-scale.component.html',
    styleUrls: ['./large-scale.component.scss'],
    standalone: false
})
export class LargeScaleQueryWidgetComponent extends QueryWidgetComponent implements OnDestroy {
	public static widgetMatch(queryControl: QueryControl): number {
		const isInteger		: boolean = queryControl.question.type === 'integer'
		const isScale		: boolean = queryControl.question.tags?.indexOf('rcc-scale') !== -1
		const hasLimits		: boolean = queryControl.question.min != null && queryControl.question.max != null
		const difference	: number = queryControl.question.max - queryControl.question.min
		const optionLength	: number = queryControl.question.options?.length ?? 0

		if (isInteger && isScale && hasLimits && difference > 11 && optionLength === 0)
			return 3

		return 0
	}

	public constructor(
		public queryControl: QueryControl,
		@Optional() @Inject(GREYED_OUT) injectedGreyedOut: boolean
	) {
		super(queryControl)
		this.greyedOut = injectedGreyedOut
	}

	private destroy$: Subject<void> = new Subject()
	public ngOnDestroy(): void {
		this.destroy$.next()
	}

	@ViewChild('fillIndicator')
	public fillIndicator: ElementRef<HTMLElement>

	public greyedOut: boolean = false

	protected get value(): number {
		const answerControl: FormControl<number> = this.queryControl.answerControl as FormControl<number>
		return answerControl.value
	}

	public onDelete(): void {
		if (this.queryControl)
			void this.queryControl.reset()
	}

	protected handleClick(): void {
		if (this.value == null)
			// This comes from a problem using the default html
			// slider (input type='range') element together with
			// Angular's Forms library. When The value is null,
			// the slider thumb gets rendered in the middle
			// of the track (which is what we want), however
			// click events directly on the thumb don't trigger
			// the input or change events. To compensate for
			// this, so that we have a value to render, we're
			// manually calling the form setValue method when the
			// thumb is clicked, but there's no value yet
			this.queryControl.answerControl.setValue(50)
	}

	private resetEventStarted: boolean = false
	private mouseDown$: Subject<void> = new Subject()
	protected handleMousedown(event: MouseEvent | PointerEvent): void {
		// It's technically possible to emit a mousedown event without having a
		// mouseup event after it, so we use this clean up the subscription if
		// another mousedown starts.
		this.mouseDown$.next()

		if (this.queryControl.answer == null) {
			this.resetEventStarted = false
			return
		}

		// Because click events on the fill indicator itself result in no click event on the input
		// element, handling the click event there prevents the default native behavior of the input.
		// As such we need to manually check whether a mouse or pointer event landed within the
		// fill indicator, and if so, treat it as a potential reset event.
		const fillIndicatorPosition: DOMRect = this.fillIndicator.nativeElement.getBoundingClientRect()

		const { clientX, clientY } = event
		const { left, right, top, bottom } = fillIndicatorPosition

		const isEventWithin: boolean =
			clientX >= left &&
			clientX <= right &&
			clientY >= top &&
			clientY <= bottom

		if (!isEventWithin) {
			this.resetEventStarted = false
			return
		}
		
		this.resetEventStarted = true

		// As the default native behavior of the input allows the user to drag the slider after
		// clicking, we need to only treat the event as a reset if the value of the underlying input
		// didn't change. So here we listen to the first emitted change, and then stop handling the
		// reset event.
		this.queryControl.change$.pipe(
			take(1),
			takeUntil(this.mouseUp$),
			takeUntil(this.mouseDown$),
			takeUntil(this.destroy$),
		).subscribe(() => {
			this.resetEventStarted = false
		})
	}

	private mouseUp$: Subject<void> = new Subject()
	protected handleMouseup(): void {
		this.mouseUp$.next()
		if (this.resetEventStarted === true)
			void this.queryControl.reset()
		this.resetEventStarted = false
	}
}
