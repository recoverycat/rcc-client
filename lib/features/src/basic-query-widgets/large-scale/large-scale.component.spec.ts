import { ComponentFixture, TestBed } from '@angular/core/testing'
import { LargeScaleQueryWidgetComponent } from './large-scale.component'
import { QueryControl } from '../../queries'
import { Question } from '@rcc/core'

describe('LargeScaleQueryWidgetComponent', () => {
    let fixture: ComponentFixture<LargeScaleQueryWidgetComponent> = undefined!
    let queryControl: QueryControl = undefined!

    beforeEach(() => {
        queryControl = new QueryControl(
            new Question({
                id: 'id',
                type: 'string',
                translations: {
                    de: 'de',
                    en: 'en'
                }
            }),
            () => undefined,
            () => undefined
        )
        fixture = TestBed.configureTestingModule({
            providers: [
                { provide: QueryControl, useValue: queryControl }
            ]
        }).createComponent(LargeScaleQueryWidgetComponent)
    })

    describe('widgetMatch', () => {
        function fakeQueryControl(question?: Partial<Question>): QueryControl {
            return {
                question: {
                    type: 'integer',
                    tags: ['rcc-scale'],
                    min: 25,
                    max: 75,
                    ...(question ?? {})
                }
            } as QueryControl
        }
        it('should return three if the control meets the requirements', () => {
            const widgetMatchQueryControl: QueryControl = fakeQueryControl()

            const result: number = LargeScaleQueryWidgetComponent.widgetMatch(widgetMatchQueryControl)

            expect(result).toBe(3)
        })

        it('should return 0 if not an integer', () => {
            const widgetMatchQueryControl: QueryControl = fakeQueryControl({
                type: 'string'
            })

            const result: number = LargeScaleQueryWidgetComponent.widgetMatch(widgetMatchQueryControl)

            expect(result).toBe(0)
        })

        it('Should return 0 if not a scale', () => {
            const widgetMatchQueryControl: QueryControl = fakeQueryControl({
                tags: []
            })

            const result: number = LargeScaleQueryWidgetComponent.widgetMatch(widgetMatchQueryControl)

            expect(result).toBe(0)
        })

        it('should return 0 if no min or max values given', () => {
            const widgetMatchQueryControl: QueryControl = fakeQueryControl({
                min: undefined,
                max: undefined
            })

            const result: number = LargeScaleQueryWidgetComponent.widgetMatch(widgetMatchQueryControl)

            expect(result).toBe(0)
        })

        it('should return 0 if it has options', () => {
            const widgetMatchQueryControl: QueryControl = fakeQueryControl({
                options: [{ value: 0 }]
            })

            const result: number = LargeScaleQueryWidgetComponent.widgetMatch(widgetMatchQueryControl)

            expect(result).toBe(0)
        })
    })

    describe('When clicking the value container', () => {
        it('should reset the queryControl', () => {
            const spy: jasmine.Spy = spyOn(queryControl, 'reset')
            const nativeElement: HTMLElement = fixture.nativeElement as HTMLElement

            const container: HTMLElement = nativeElement.querySelector('[data-testid="value-indicator"]') as unknown as HTMLElement
            container.click()

            // eslint-disable-next-line @typescript-eslint/unbound-method
            expect(spy).toHaveBeenCalledWith()
        })
    })

    describe('When clicking escape', () => {
        it('should reset the query control', () => {
            const spy: jasmine.Spy = spyOn(queryControl, 'reset')
            const nativeElement: HTMLElement = fixture.nativeElement as HTMLElement

            const input: HTMLElement = nativeElement.querySelector('input') as unknown as HTMLElement
            input.dispatchEvent(new KeyboardEvent('keydown', {
                key: 'Escape'
            }))

            // eslint-disable-next-line @typescript-eslint/unbound-method
            expect(spy).toHaveBeenCalledWith()
        })
    })
})
