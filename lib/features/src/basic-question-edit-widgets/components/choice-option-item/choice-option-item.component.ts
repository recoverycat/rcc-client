import {
	Component,
	EventEmitter,
	Input,
	Output
}					from '@angular/core'
import {
	ControlContainer,
	FormGroupDirective
}					from '@angular/forms'

@Component({
    selector: 'rcc-choice-option-item[optionValueType]',
    templateUrl: './choice-option-item.component.html',
    viewProviders: [
        { provide: ControlContainer, useExisting: FormGroupDirective }
    ],
    standalone: false
})
export class ChoiceOptionItemComponent {
	@Input()
	public optionValueType: string

	@Input()
	public showRemoveButton: boolean = true

	@Input()
	public valueReadonly: boolean = false

	@Output()
	public removeItem: EventEmitter<void> = new EventEmitter<void>()
}
