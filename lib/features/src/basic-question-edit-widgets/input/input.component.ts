import 	{
			Component,
		}										from '@angular/core'

import	{
			RccTranslationService
		}	                            		from '@rcc/common'

import	{
			takeUntil,
			startWith
		}										from 'rxjs'

import	{
			QuestionEditControl,
			GenericQuestionEditWidgetComponent
		}										from '../../questions/question-edit-widgets'
import { QuestionConfig } from '@rcc/core'


@Component({
    templateUrl: './input.component.html',
    standalone: false
})
export class InputQuestionEditWidgetComponent extends GenericQuestionEditWidgetComponent {


	// STATIC

	public static label: string = 'BASIC_QUESTION_EDIT_WIDGETS.TEXT_INPUT.LABEL'

	public static controlType: typeof QuestionEditControl = QuestionEditControl

	public placeholder: string = 'BASIC_QUESTION_EDIT_WIDGET.TEXT_INPUT.PLACEHOLDER'

	public static widgetMatch(questionEditControl: QuestionEditControl): number {

		const config: QuestionConfig			= questionEditControl.questionConfig

		// New question, can still be handled:
		if(!config)				return 2


		const hasNoOptions: boolean = ! (config.options?.length > 0)
		const typeMatch: boolean = ['string', 'integer', 'decimal'].includes(config.type)

		// Made to handle questions like this:
		if(hasNoOptions && typeMatch) return 2

		// Can handle any other question if need be:
		return 	0
	}



	// INSTANCE

	public constructor(
		protected questionEditControl	: QuestionEditControl,
		protected rccTranslationService	: RccTranslationService,
	){
		super(
			questionEditControl,
			rccTranslationService
		)

		this.answerTypes = ['string', 'integer', 'decimal']
		this.setAnswerTypeAsSelectedOrDefault()

		this.answerTypeControl.valueChanges
		.pipe(
			takeUntil(this.destroy$),
			startWith(this.answerTypeControl.value)
		)
		.subscribe(
			answerType => 	answerType === 'string'
							?	this.limitationControl.setValue('none')
							:	this.limitationControl.setValue('minmax')
		)

		this.tagControl.disable()
	}
}
