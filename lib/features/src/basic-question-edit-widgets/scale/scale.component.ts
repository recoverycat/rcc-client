import 	{
			Component,
		}										from '@angular/core'

import	{
			FormControl,
			FormGroup,
			FormArray,
			AbstractControl,
			ValidationErrors
		}										from '@angular/forms'
import	{
			takeUntil,
			startWith
		}										from 'rxjs'
import	{
			RccTranslationService
		}	                            		from '@rcc/common'

import	{
			getDecimalPlaces,
			round,
			QuestionConfig,
			QuestionOptionConfig,
		}										from '@rcc/core'

import	{
			QuestionEditControl,
			GenericQuestionEditWidgetComponent,
			OptionControl
		}										from '../../questions/question-edit-widgets'






@Component({
    templateUrl: './scale.component.html',
    standalone: false
})
export class ScaleQuestionEditWidgetComponent extends GenericQuestionEditWidgetComponent {

	// STATIC

	public static label: string = 'BASIC_QUESTION_EDIT_WIDGETS.SCALE.LABEL'

	public static controlType: typeof QuestionEditControl = QuestionEditControl


	public static widgetMatch(questionEditControl: QuestionEditControl): number {

		const config: QuestionConfig 		= 	questionEditControl.questionConfig

		// New question, can still be handled:
		if(!config)				return 0

		const noScaleTag: boolean	=	!config.tags?.includes('rcc-scale')
								&&
								!config.tags?.includes('scale') // legacy, can be removes


		// can still handle the question, if need be
		if(noScaleTag)	return 0

		const isInteger: boolean		= config.type === 'integer'
		const isDecimal: boolean		= config.type === 'decimal'


		// can still handle the question, if need be
		if(!isInteger && !isDecimal) 		return 0

		const hasMin: boolean 		= Number.isFinite(config.min)
		const hasMax: boolean 		= Number.isFinite(config.max)

		// Made to handle questions like this
		if(isInteger && hasMin && hasMax) 	return 3

		const noOptions: boolean		= !!config.options?.length

		// Can still handle the question, if need be
		if(noOptions)						return 0


		const values: (string | boolean | number)[]		= config.options.map( option => option.value)
		// eslint-disable-next-line @typescript-eslint/typedef
		const allNumbers	= values.every( (value:unknown) : value is number => typeof value !== 'number')

		// Can still handle the question, if need be; this should never happen because it implies an invalid question config:
		if(!allNumbers)						return 0

		const sortedValues: number[]	= values.sort( (a,b) => Math.sign(b-a) )
		const steps: number[]			= sortedValues.slice(1).map( (value, index) => sortedValues[index]-sortedValues[index-1])
		const uniformSteps: boolean		= (new Set(steps)).size === 1

		// Made to handle questions like this
		if(uniformSteps) return 3

		// can still handle the question, if need be
		return 0
	}

	// INSTANCE

	public stepSizeControl: FormControl<number> 			= 	new FormControl<number>(1)
	public minMaxStepSizeControl: FormGroup	=	new FormGroup({
											minMax: 	this.minMaxControls,
											stepSize: 	this.stepSizeControl
										}, this.validateStepSize() )

	public useLabelsControl: FormControl<boolean>			=	new FormControl<boolean>(false)

	public constructor(
		protected questionEditControl	: QuestionEditControl,
		protected rccTranslationService	: RccTranslationService,
	){
		super(
			questionEditControl,
			rccTranslationService,
		)

		this.updateInputsFromConfigForScale()

		this.answerTypes = ['integer', 'decimal']
		this.answerTypeControl.setValue('integer')
		this.minMaxControls.enable()
		this.enableMinControl.setValue(true)
		this.enableMaxControl.setValue(true)

		this.useLabelsControl.valueChanges
		.pipe(
			takeUntil(this.destroy$),
			startWith(this.useLabelsControl.value)
		)
		.subscribe( useLabels => {

			if (useLabels)	this.optionControls.enable()
			else 			this.optionControls.disable()

		})

		this.tags.add('rcc-scale')

		this.allForms.push(this.minMaxStepSizeControl)
		this.allForms.push(this.useLabelsControl)

		this.allForms.addValidators(
			() =>  	this.validateLimitedOption(
						this.minControl,
						this.maxControl,
						this.stepSizeControl,
						this.optionControls
					)
		)

	}

	public validateStepSize(): (ac: AbstractControl) => (ValidationErrors | null) {

		return (): ValidationErrors | null  => {

			this.minMaxControls.updateValueAndValidity({ onlySelf: true, emitEvent: false })
			this.stepSizeControl.updateValueAndValidity({ onlySelf: true, emitEvent: false })

			const min: number 	= this.minMaxControls.get('min')?.value
			const max: number 	= this.minMaxControls.get('max')?.value

			if(!Number.isFinite(min) )					return { 'BASIC_QUESTION_EDIT_WIDGETS.SCALE.STEP_SIZE.BAD_MIN' : true }
			if(!Number.isFinite(max) ) 					return { 'BASIC_QUESTION_EDIT_WIDGETS.SCALE.STEP_SIZE.BAD_MAX' : true }

			const stepSize: number 	= this.stepSizeControl.value

			if(!Number.isFinite(stepSize) ) this.stepSizeControl.setErrors({ 'BASIC_QUESTION_EDIT_WIDGETS.SCALE.STEP_SIZE.NAN' : true })

			if(stepSize <= 0){
				this.stepSizeControl.setErrors({ 'BASIC_QUESTION_EDIT_WIDGETS.SCALE.STEP_SIZE.LE_ZERO' : true })
				return null
			}

			const decimalPlaces: number = getDecimalPlaces(1/stepSize)

			if(Number.isInteger( round(max-min, decimalPlaces) / stepSize) ) return null

			this.stepSizeControl.setErrors({ 'BASIC_QUESTION_EDIT_WIDGETS.SCALE.STEP_SIZE.MIN_MAX_MISMATCH': true })

			return null
		}


	}

	/**
	 * Not actually a validator function, since it is not meant to be added to
	 * a specific control and will always return null.
	 * Will set errors on provided Controls though, if applicable.
	 */
	public validateLimitedOption(

		minControl		: FormControl<number>,
		maxControl		: FormControl<number>,
		stepSizeControl	: FormControl<number>,
		optionFormArray	: FormArray<OptionControl>

	) : null {

		const min: number 						= minControl.value
		const max: number 						= maxControl.value
		const stepSize: number					= stepSizeControl.value
		const optionControls: OptionControl[]	= optionFormArray.controls

		// These values are expected to be validated somewhere else:
		if(typeof min !== 'number') 		return null
		if(typeof max !== 'number') 		return null
		if(typeof stepSize !== 'number')	return null
		if(stepSize <= 0 )				return null


		optionControls.forEach( optionControl => {

			const valueControl: FormControl<string | boolean | number>	= optionControl.controls.value
			const labelControl: FormControl<string>						= optionControl.controls.label

			valueControl.updateValueAndValidity({ onlySelf: true })
			labelControl.updateValueAndValidity({ onlySelf: true })

			const value: string | number | boolean = valueControl.value

			if(optionControl.errors) 		return null

			// Option values are expected to be numbers; validation of this is supposed to happen earlier.
			if(typeof value !== 'number') 	return null

			if(value < min) 			valueControl.setErrors({ 'BASIC_QUESTION_EDIT_WIDGETS.SCALE.CUSTOM_LABELS.VALUE_TOO_SMALL': true })
			if(value > max) 			valueControl.setErrors({ 'BASIC_QUESTION_EDIT_WIDGETS.SCALE.CUSTOM_LABELS.VALUE_TOO_BIG': true })

			const decimalPlaces: number 		= getDecimalPlaces(stepSize)

			const valueAlignsWithSteps: boolean	= Number.isInteger(round(value-min, decimalPlaces)/stepSize)


			if(!valueAlignsWithSteps) 	valueControl.setErrors({ 'BASIC_QUESTION_EDIT_WIDGETS.SCALE.CUSTOM_LABELS.VALUE_MISMATCH': true })

		})

		return null

	}

	public addOption(data?: {value?:string|number|boolean, label?: string}) : AbstractControl {

		if(!data){
			const values: (string | number | boolean)[]	= this.optionControls.value.map( option => option.value)
			// eslint-disable-next-line @typescript-eslint/typedef
			const validValues = values.length > 0 && values.every( (v) : v is number => typeof v === 'number')
			const greatestValue: number = validValues ? Math.max(...values) : -1
			const stepSize: number = this.stepSizeControl ? this.stepSizeControl.value : 1

			data = { value: greatestValue+stepSize }
		}

		const optionControl: AbstractControl = super.addOption(data)

		return optionControl
	}


	public updateInputsFromConfigForScale() : void {

		const config: QuestionConfig = this.questionEditControl.questionConfig

		// Options:
		if(!config.options || config.options.length === 0) return

		this.useLabelsControl.setValue(true)

		// eslint-disable-next-line @typescript-eslint/typedef
		const allNumbers = config.options.every( option => typeof option.value === 'number' )

		if(!allNumbers) return

		const values: number[]		= config.options.map( option => option.value as number)
		const min: number 			= Math.min(...values)
		const max: number			= Math.max(...values)

		this.minControl.setValue(min)
		this.maxControl.setValue(max)

		const sortedValues: number[]	= values.sort( (a,b) => Math.sign(b-a) )
		const steps: number[]			= sortedValues.slice(1).map( (value, index) => sortedValues[index]-sortedValues[index-1])
		const uniformSteps: boolean		= (new Set(steps)).size === 1

		if(uniformSteps) this.stepSizeControl.setValue(steps[0])

	}

	public getConfigFromInputs() : QuestionConfig {

		const preConfig: QuestionConfig		= 	super.getConfigFromInputs()

		const min: number					=	this.minControl.value
		const max: number					=	this.maxControl.value
		const stepSize: number				=	this.stepSizeControl.value

		const minInteger: boolean			=	Number.isInteger(min)
		const maxInteger: boolean			=	Number.isInteger(max)
		const stepSizeInteger: boolean		=	Number.isInteger(stepSize)

		const noOptions: boolean			=	!preConfig.options?.length

		const allIntegers: boolean			=	minInteger && maxInteger && stepSizeInteger

		if(allIntegers && stepSize === 1 && noOptions) return preConfig

		const decimalPlaces: number		 	= 	getDecimalPlaces(1/stepSize)
		const size: number					=	1 + round(max-min, decimalPlaces) / stepSize

		const minMaxStepsAlign: boolean		=	Number.isInteger(size)


		if(!this.useLabelsControl.value) return { ...preConfig, options: undefined }

		const options: QuestionOptionConfig[] =	minMaxStepsAlign
									?	new Array(size)
										.fill(undefined)
										.map( (_, index) => {
											const value: number 	= min + (index * stepSize)
											const option: QuestionOptionConfig = preConfig.options?.find(o => o.value === value)

											return option || { value }
										})
									:	new Array(1) as QuestionOptionConfig[]

		const outOfBoundsOption: QuestionOptionConfig =
			preConfig.options.find( preOption => options.every( ({ value }) => value !== preOption.value) )

		if(outOfBoundsOption) this.allForms.markAllAsTouched()


		return { ...preConfig, options }
	}

}
