import { ApplicationRef, Injectable } from '@angular/core'
import { SwUpdate } from '@angular/service-worker'

import { RccAlertController } from '@rcc/common'
import { isApplicationStable } from '@rcc/core'
import { timer } from 'rxjs'


export const INTERVAL_CHECK_TIME_MILLISECONDS: number = 15 * 60 * 1000 // 15 minutes

@Injectable()
export class UpdateService {

	public constructor(
		protected readonly rccAlertController	: RccAlertController,
		private readonly  swUpdate				: SwUpdate,
		private readonly applicationRef			: ApplicationRef,
	){
		void this.initialize()
	}

	private async initialize(): Promise<void> {
		await isApplicationStable(this.applicationRef)

		timer(0, INTERVAL_CHECK_TIME_MILLISECONDS).subscribe(() => {
			console.info(`UpdateModule - Running update check with an interval of ${INTERVAL_CHECK_TIME_MILLISECONDS} milliseconds`)
			this.check()
		})
	}


	private showingUpdate: boolean = false
	private askToInstallUpdate(): void {
		if(this.showingUpdate)
			return

		this.showingUpdate = true
		void this.rccAlertController.present({
			message: 	'UPDATES.UPDATE_AVAILABLE_ALERT.MESSAGE',
			cssClass:	'rcc-theme',
			buttons: 	[
				{ label:'UPDATES.UPDATE_AVAILABLE_ALERT.NOT_NOW', rejectAs:	'cancel' },
				{ label:'UPDATES.UPDATE_AVAILABLE_ALERT.INSTALL', resolveAs: 'okay' }
			]
		})
			.then(() => this.installUpdate())
			.finally(() => this.showingUpdate = false)
	}

	private check(): void {
		void this.swUpdate.checkForUpdate().then((hasUpdate) => {
			if(hasUpdate) {
				console.info('UpdateModule - Update found')
				this.askToInstallUpdate()
			}
		})
	}

	public async checkManually(): Promise<void>{

		const updateAvailable: boolean = await this.swUpdate.checkForUpdate()
		if(updateAvailable)
			this.askToInstallUpdate()
		else
			void this.rccAlertController.present({
				message: 	'UPDATES.NO_UPDATE_FOUND_ALERT.MESSAGE',
				cssClass:	'rcc-theme',
				buttons: 	[
					{ label:'UPDATES.NO_UPDATE_FOUND_ALERT.NOT_NOW', rejectAs: 'cancel' },
					{ label:'UPDATES.NO_UPDATE_FOUND_ALERT.RESTART', resolveAs: 'okay' }
				]
			})
				.then(() => this.installUpdate())
	}

	private installUpdate() : void {
		void this.swUpdate.activateUpdate().then(() => location.reload())
	}

}

