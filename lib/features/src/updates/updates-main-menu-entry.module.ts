import	{
	NgModule,
	Provider,
	ModuleWithProviders,
}											from '@angular/core'
import	{
	provideMainMenuEntry,
	Factory,
	Action,
	WithPosition,
	getEffectivePosition,
}											from '@rcc/common'
import	{
	UpdateModule
}											from './updates.module'
import	{
	UpdateService
}											from './updates.service'


@NgModule({
	imports:[
		UpdateModule,
	],
	providers: [
		UpdateService,
	],
})
export class UpdatesMainMenuEntryModule {
	public static addEntry(config?: WithPosition): ModuleWithProviders<UpdatesMainMenuEntryModule> {
		const mainMenuEntry	: Factory<Action> =	{
			deps:	[UpdateService],
			factory: (updateService: UpdateService) => ({
				position: 		getEffectivePosition(config, 1),
				icon:			'refresh',
				label:			'UPDATES.MENU_ENTRY',
				handler:		() => updateService.checkManually()
			})
		}

		const providers : Provider[] = [provideMainMenuEntry(mainMenuEntry)]

		return {
			ngModule:	UpdatesMainMenuEntryModule,
			providers
		}
	}

}
