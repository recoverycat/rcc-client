import { NgModule } from '@angular/core'
import { RccDefaultResourceStoreServiceModule } from './default-resource-store/default-resource-store-service.module'
import { RccCustomResourceStoreServiceModule } from './custom-resource-store/custom-resource-store-service.module'
import { RccBasicResourceItemRepresentationModule } from './basic-resource-item-representation/basic-resource-item-representation.module'
import { RccResourceOverviewComponent } from './resource-overview/resource-overview.component'
import { SharedModule, provideTranslationMap } from '@rcc/common'

import en from './i18n/en.json'
import de from './i18n/de.json'


@NgModule({
	imports: [
		RccDefaultResourceStoreServiceModule,
		RccCustomResourceStoreServiceModule,
		RccBasicResourceItemRepresentationModule,
		SharedModule,
	],
	declarations: [
		RccResourceOverviewComponent,
	],
	exports: [
		RccResourceOverviewComponent,
	],
	providers: [
		provideTranslationMap('RESOURCES', { en, de }),
	]
})
export class RccResourcesModule {}
