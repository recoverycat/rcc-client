import { Component } from '@angular/core'
import { ExposeTemplateComponent } from '@rcc/common'
import { Resource } from '@rcc/core/src/items/resources/resource.class'

@Component({
    selector: 'rcc-basic-resource-item-label',
    templateUrl: './basic-resource-item-label.component.html',
    styleUrls: ['./basic-resource-item-label.component.scss'],
    standalone: false
})
export class RccBasicResourceItemLabelComponent extends ExposeTemplateComponent {
	public constructor(protected resource: Resource){
		super()
	}
}
