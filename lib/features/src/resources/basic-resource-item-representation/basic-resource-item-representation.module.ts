import { NgModule } from '@angular/core'
import { ItemRepresentation, SharedModule, provideItemRepresentation } from '@rcc/common'
import { Resource } from '@rcc/core/src/items/resources/resource.class'
import { RccBasicResourceItemLabelComponent } from './basic-resource-item-label/basic-resource-item-label.component'

const basicResourceItemRepresentation: ItemRepresentation = {
	itemClass		: Resource,
	name			: 'RESOURCE',
	icon			: 'resource',
	labelComponent	: RccBasicResourceItemLabelComponent,
}

@NgModule({
	imports: [
		SharedModule,
	],
	providers: [
		provideItemRepresentation(basicResourceItemRepresentation),
	],
	declarations: [
		RccBasicResourceItemLabelComponent,
	]
})
export class RccBasicResourceItemRepresentationModule {}
