import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core'
import { TranslationsModule } from '@rcc/common/src/translations/ng/translations.module'
import { RouterModule } from '@angular/router'
import { RccThemeModule } from '@rcc/themes/active'
import { FinalSlideComponent } from '../final-slide/final-slide.component'
import { CommonModule } from '@angular/common'

@Component({
    selector: 'rcc-empty-state-none-for-today',
    templateUrl: './empty-state-none-for-today.component.html',
    imports: [
        RouterModule,
        TranslationsModule,
        RccThemeModule,
        FinalSlideComponent,
        CommonModule,
    ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmptyStateNoneForTodayComponent {
    @Output()
    public viewAnswersClick: EventEmitter<void> = new EventEmitter<void>()

    @Input()
    public hasPreviousDateWithQuestions: boolean
}
