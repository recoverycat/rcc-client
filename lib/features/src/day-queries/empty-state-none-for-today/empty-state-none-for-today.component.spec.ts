import { ComponentFixture, TestBed } from '@angular/core/testing'
import { EmptyStateNoneForTodayComponent } from './empty-state-none-for-today.component'
import { provideRouter } from '@angular/router'

describe('EmptyStateNoneForTodayComponent', () => {
    it('Should render without errors', () => {
        const fixture: ComponentFixture<EmptyStateNoneForTodayComponent> = TestBed
            .configureTestingModule({
                providers: [provideRouter([])]
            })
            .createComponent(EmptyStateNoneForTodayComponent)

        expect(fixture.nativeElement).not.toBeNull()
    })
})
