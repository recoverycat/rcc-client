import	{	NgModule					}	from '@angular/core'

import	{
			provideRouter,
			Route,
			RouterModule
		}									from '@angular/router'

import	{
			SharedModule,
			TranslationsModule,
			provideTranslationMap,
			overrideRoute,
		}									from '@rcc/common'

import	{	QueriesModule				}	from '../queries'
import	{	QuestionnaireServiceModule	}	from '../questions'
import	{	JournalServiceModule		}	from '../entries'
import	{	DayQueryService				}	from './day-query.service'
import	{	TodaysOpenQuestion$			}	from './todays-open-questions.observable'
import	{	DayQueryRunPageComponent	}	from './day-query-run-page'
import	{	DayQueryPageComponent		}	from './day-query-page'
import	{	dayQueryRunPath				}	from './day-queries.commons'
import	{	HasAnAnswerForTodayPipe		}	from './day-queries.pipes'
import	{	AllDoneSlideComponent		}	from './all-done-slide'
import { EmptyStateNoneComponent } from './empty-state-none/empty-state-none.component'
import { EmptyStateNoneForTodayComponent } from './empty-state-none-for-today/empty-state-none-for-today.component'

import en from './i18n/en.json'
import de from './i18n/de.json'



const routes : Route = {

	path:		dayQueryRunPath,
	title:		'DAY_QUERIES.PAGE_TITLE',
	component:	DayQueryRunPageComponent

}

const queryPageRoute : Route = 	{

	path: 		'query/:id',
	component: 	DayQueryPageComponent

}

/**
 * This Module handles questions that are due on a target day. (No momentary assessment)
 */
@NgModule({
	imports: [
		QueriesModule,
		SharedModule,
		TranslationsModule,
		QuestionnaireServiceModule,
		JournalServiceModule,
		RouterModule,
		EmptyStateNoneComponent,
		EmptyStateNoneForTodayComponent
	],
	declarations:[
		DayQueryRunPageComponent,
		DayQueryPageComponent,
		HasAnAnswerForTodayPipe,
		AllDoneSlideComponent
	],
	exports:[
		DayQueryRunPageComponent,
		DayQueryPageComponent,
		HasAnAnswerForTodayPipe,
		AllDoneSlideComponent
	],
	providers:[
		DayQueryService,
		TodaysOpenQuestion$,
		provideTranslationMap('DAY_QUERIES', { en, de }),
		provideRouter([routes]),
		overrideRoute(queryPageRoute)
	]
})
export class DayQueryModule{}
