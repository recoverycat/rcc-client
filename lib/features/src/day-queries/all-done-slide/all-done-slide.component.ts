import 	{
			Component,
			EventEmitter,
			Input,
			Output
		}						from '@angular/core'

import	{
			Router
		}						from '@angular/router'

import	{
			Action
		}						from '@rcc/common'

@Component({
    selector: 'all-done-slide',
    templateUrl: './all-done-slide.component.html',
    styleUrls: ['./all-done-slide.component.css'],
    standalone: false
})
export class AllDoneSlideComponent{

	public constructor(
		private	router	: Router)
	{}

	@Input()
	public allDoneSelectedDate		: boolean
	@Input()
	public doneSelectedDate			: number
	@Input()
	public totalSelectedDate		: number

	@Output()
	public answerQuestions			: EventEmitter<void> = new EventEmitter<void>()
	@Output()
	public changeAnswers			: EventEmitter<void> = new EventEmitter<void>()

	public answerRemainingQuestionsAction: Action = {
		label: 			'DAY_QUERIES.QUERY_RUN.ANSWER_QUESTIONS',
		description: 	'DAY_QUERIES.QUERY_RUN.ANSWER_QUESTIONS',
		category:		'create',
		icon: 			'create',
		handler: 		() => this.answerQuestions.emit()
	}

	public changeAnswersAction: Action = {
		label: 			'DAY_QUERIES.QUERY_RUN.CHANGE_ANSWERS',
		description:	'DAY_QUERIES.QUERY_RUN.CHANGE_ANSWERS',
		category:		'create',
		icon: 			'create',
		handler: 		() => this.changeAnswers.emit()
	}

	public viewDataAction: Action = {
		label: 			'DAY_QUERIES.QUERY_RUN.VIEW_DATA',
		description: 	'DAY_QUERIES.QUERY_RUN.VIEW_DATA',
		category:		'analyze',
		icon: 			'analyze',
		path:			'/my-charts',
	}

}
