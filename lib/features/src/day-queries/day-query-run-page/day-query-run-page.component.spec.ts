import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing'
import { DayQueryRunPageComponent } from './day-query-run-page.component'
import { SymptomCheckMetaStoreService } from '../../symptom-checks/meta-store'
import { IconsModule, ModalProviderModule, QrCodeModule, QrCodeService, RccStorage, StorageProviderModule, TranslationsModule } from '@rcc/common'
import { DayQueryModule, IndexedDbModule, StarterQuestionStoreServiceModule } from '@rcc/features'
import { IonicAlertController, IonicModalsModule } from '@rcc/ionic'
import { Item, ItemStorage, SymptomCheck, SymptomCheckConfig } from '@rcc/core'
import { EmptyStateNoneComponent } from '../empty-state-none/empty-state-none.component'
import { Injectable } from '@angular/core'
import { provideRouter } from '@angular/router'

@Injectable({
    providedIn: 'root'
})
class TestingStorage implements RccStorage {
    public createItemStorage<I extends Item>(): ItemStorage<I> {
        return {
            getAll: () => Promise.resolve([]),
            store: () => Promise.resolve()
        }
    }

    public clearItemStorage(): Promise<void> {
        return Promise.resolve()
    }

    public getStorageNames(): Promise<string[]> {
        return Promise.resolve([] as string[])
    }
}

describe('DayQueryRunPageComponent', () => {

	let component: DayQueryRunPageComponent = undefined!
	let fixture: ComponentFixture<DayQueryRunPageComponent> = undefined!
	let mockSymptomCheckMetaStoreService: jasmine.SpyObj<SymptomCheckMetaStoreService> = undefined!
    let markStoreAsReady: () => void = () => undefined
    let ready: Promise<void> = undefined!
	let items: SymptomCheck[] = []

	const symptomCheckConfig: SymptomCheckConfig = {
		meta: {
			label: 'Starter Symptom Check',
			defaultSchedule: [
				[],
				['morning', 'evening'],
			],
			creationDate: '2024-08-01'

		},
		questions: [
			{
				id: 'rcc-curated-starter-questions-0001-daily-0',
				category: 'symptoms'
			},
			{
				id: 'rcc-curated-starter-questions-0002-daily-0',
				category: 'symptoms'
			},
			{
				id: 'rcc-curated-starter-questions-0003-daily-1',
				category: 'resources'
			},
			{
				id: 'rcc-curated-default-00-daily-note',
				category: 'daily_notes'
			}
		]
	}

	beforeEach(async () => {

		// Due to an error/warning saying that "Some of your tests did a full page reload!", the following fix was found
		// https://stackoverflow.com/questions/29352578/some-of-your-tests-did-a-full-page-reload-error-when-running-jasmine-tests
		window.onbeforeunload = jasmine.createSpy()

        ready = new Promise<void>((resolve) => markStoreAsReady = resolve)
		items = [new SymptomCheck(symptomCheckConfig)]
		mockSymptomCheckMetaStoreService = jasmine.createSpyObj('SymptomCheckMetaStoreService', [], {
			ready,
			items,
			stores: [],
		}) as jasmine.SpyObj<SymptomCheckMetaStoreService>

		await TestBed.configureTestingModule({
			imports: [
				StarterQuestionStoreServiceModule,
                IconsModule,
                StorageProviderModule.forRoot(TestingStorage),
				TranslationsModule,
				DayQueryModule,
				IndexedDbModule,
				ModalProviderModule.forRoot({
					alertController: IonicAlertController,
				}),
				IonicModalsModule,
			],
			declarations: [DayQueryRunPageComponent],
			providers: [
				{ provide: SymptomCheckMetaStoreService, useValue: mockSymptomCheckMetaStoreService },
				provideRouter([]),
			],
			teardown: { destroyAfterEach: false }

        }).overrideComponent(EmptyStateNoneComponent, {
            remove: { imports: [QrCodeModule] },
            add: { providers: [{ provide: QrCodeService, useValue: { } }] },
		}).compileComponents()

		fixture = TestBed.createComponent(DayQueryRunPageComponent)
		component = fixture.componentInstance

		void ready.then(() => { fixture.detectChanges() })
	})

	it('should create the component', () => {
		expect(component).toBeTruthy()
	})

	it('should run setup', fakeAsync(async () => {
		markStoreAsReady()
		await ready
		component['setup']()
		tick()

		expect(component.dateControl.value.toDateString()).toEqual((new Date()).toDateString())
		expect(component.currentQueryControls).toEqual([])
	}))

	it('should get an updated current query controls based on configured symptom check', waitForAsync(async () => {
		markStoreAsReady()
		await ready
		component['setup']()
		component.dateControl.setValue(new Date('2024-08-01'))
		await fixture.whenStable()

		expect(component.currentQueryControls).not.toEqual([])
	}))

    describe('When no symptom checks are present', () => {
        it('Should render the empty state none slide', waitForAsync(async () => {
			markStoreAsReady()
			await ready
            await fixture.whenStable()

            const nativeElement: HTMLElement = fixture.nativeElement as HTMLElement

            const slide: unknown = nativeElement.querySelector('[data-testid="no-questions"]')

            expect(slide).not.toBeNull()
        }))
    })

    describe('When symptom checks present, but none scheduled', () => {
		beforeEach(async () => {
			items.pop()
			items.push(
                new SymptomCheck({
                    meta: {},
                    questions: []
				})
			)

			markStoreAsReady()
			await ready.then(() => {
				fixture.detectChanges()
			})
		})
        
        it('Should render the empty state none for today slide', waitForAsync(async () => {
            await fixture.whenStable()
			fixture.detectChanges()

            const nativeElement: HTMLElement = fixture.nativeElement as HTMLElement

            const slide: unknown = nativeElement.querySelector('[data-testid="no-questions-today"]')

            expect(slide).not.toBeNull()
        }))
    })
})
