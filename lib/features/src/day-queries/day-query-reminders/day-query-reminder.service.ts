import	{
			Injectable,
			Inject,
			OnDestroy,
			ApplicationRef,
		}									from '@angular/core'

import	{	Router						}	from '@angular/router'

import	{
			DOCUMENT,
		}									from '@angular/common'

import	{	FormControl					}	from '@angular/forms'

import	{
			distinct,
			fromEvent,
			interval,
			map,
			merge,
			Subject,
			takeUntil,
			startWith,
			throttleTime,
			filter,
			Observable,
		}									from 'rxjs'

import	{
			CalendarDateString,
			SymptomCheck,
			assert,
			today,
			Question,
			isApplicationStable
		}									from '@rcc/core'

import	{
			RccSettingsService,
			RccSwService,
			RccTranslationService,
			ScheduledNotification,
			ScheduledNotificationService,
			SettingsEntry,
			TimingError,
		}									from '@rcc/common'

import	{
			SymptomCheckMetaStoreService,
		}									from '../../symptom-checks/meta-store'

import	{	JournalService				}	from '../../entries'

import	{	dayQueryRunPath				}	from '../day-queries.commons'
import	{	DayQueryService				}	from '../day-query.service'

declare module '@rcc/common/settings/settings.service' {
	interface SettingValueTypeMap {
		'day-query-reminder': string
	}
}

export const settingsEntry : SettingsEntry<string> =
{
	id:				'day-query-reminder',
	label: 			'DAY_QUERIES.SETTINGS.REMINDER.LABEL',
	description: 	'DAY_QUERIES.SETTINGS.REMINDER.DESCRIPTION',
	icon:			'reminder',
	type:			'time' as const,
	defaultValue:	'12:00',
}

@Injectable()
export class DayQueryReminderService implements OnDestroy{
	protected destroy$		: Subject<void>	= new Subject<void>()
	protected sourceName	: string		= 'day-query-reminder'

	public ready 			: Promise<void>

	public constructor (
		@Inject(DOCUMENT)
		protected document						: Document,
		protected dayQueryService 				: DayQueryService,
		protected symptomCheckMetaStoreService 	: SymptomCheckMetaStoreService,
		protected rccSettingsService			: RccSettingsService,
		protected scheduledNotificationService	: ScheduledNotificationService,
		protected rccTranslationService			: RccTranslationService,
		protected journalService				: JournalService,
		protected rccSwService					: RccSwService,
		protected router						: Router,
		private applicationRef									: ApplicationRef
	) {
		this.ready = this.setup()
	}

	public async setup(): Promise<void> {
		const visibilityChange$	: Observable<Event>			= 	fromEvent<Event>(this.document, 'visibilitychange')

		await isApplicationStable(this.applicationRef)

		const dateChange$		: Observable<number|Date>	= 	interval(1000*60)
									.pipe(
										startWith(null),
										map( () => today() ),
										distinct()
									)
		const reminderControl	: FormControl<string>		=	await this.rccSettingsService.get('day-query-reminder')

		merge(
			this.symptomCheckMetaStoreService.change$,
			this.journalService.change$,
			reminderControl.valueChanges,
			visibilityChange$,
			dateChange$,
		)
		.pipe(
			throttleTime(1000*20, undefined, { leading:true, trailing:true }),
			takeUntil(this.destroy$)
		)
		.subscribe( () => void this.scheduleReminders())

		this.rccSwService.incomingNotification$
		.pipe(
			takeUntil( this.destroy$ ),
			filter( notification => notification.data?.source === this.sourceName )
		)
		.subscribe( () => void this.router.navigateByUrl(dayQueryRunPath) )

		await this.symptomCheckMetaStoreService.ready


	}


	public async scheduleReminders() : Promise<void> {
		await this.symptomCheckMetaStoreService.ready

		const startDate 	: string 			= 	CalendarDateString.today()
		const endDate		: string			= 	CalendarDateString.daysAfter(startDate, 3)
		const dateRange		: string[]			= 	CalendarDateString.range(startDate, endDate)
		const symptomChecks	: SymptomCheck[]	= 	this.symptomCheckMetaStoreService.items

		const notificationPromises	: Promise<ScheduledNotification[]>[]	=	symptomChecks.map( async symptomCheck => this.getNotifications(symptomCheck, dateRange) )

		const newNotifications			: ScheduledNotification[]	=	(await Promise.all(notificationPromises)).flat()

		const scheduledNotifications	: ScheduledNotification[]	=	await this.scheduledNotificationService.getScheduledNotifications({ source: this.sourceName })

		const cancelNotifications		: ScheduledNotification[]	=	scheduledNotifications.filter( scheduledNotification => {
																			const scheduledData	: Record<string, string|number|boolean>	=	scheduledNotification.data

																			const hasUpdate		: boolean	= 	newNotifications.some(
																													({ data }) =>	data.date 			=== scheduledData.date
																																&&	data.symptomCheck 	=== scheduledData.symptomCheck
																																&&	data.time			!== scheduledData.time
																												)
																			const noLongerDue	: boolean	=	newNotifications.every(
																													({ data }) => 	data.symptomCheck 	!== scheduledData.symptomCheck
																																||	data.date 			!== scheduledData.date
																												)

																			return hasUpdate || noLongerDue
																		})

		const addNotifications			: ScheduledNotification[]	=	newNotifications.filter( newNotification => {
																			const newData		: Record<string, string|number|boolean>	= 	newNotification.data

																			const isDuplicate 	: boolean	= 	scheduledNotifications.some(
																													({ data }) =>	data.date 			=== newData.date
																																&&	data.symptomCheck 	=== newData.symptomCheck
																																&&	data.time			=== newData.time
																												)

																			return !isDuplicate
																		})

		await Promise.all( cancelNotifications.map(
			({ data }) 	=> 	this.scheduledNotificationService.cancel(data).catch(console.error)
		))

		await Promise.all( addNotifications.map(
			notification 	=> 	this.scheduledNotificationService.schedule(notification)
								.catch(e =>{
									if(e instanceof TimingError)
										return null

									throw e
								})
		))
	}

	public async getNotifications(symptomCheck: SymptomCheck, targetDays : string[]) : Promise<ScheduledNotification[]>{
		await this.rccSettingsService.ready

		targetDays.forEach( targetDay => CalendarDateString.assert(targetDay) )

		const reminderSettingControl	: FormControl<string>	= await this.rccSettingsService.get('day-query-reminder')
		const reminderDefaultSetting	: string				= reminderSettingControl.value
		const reminderTime 				: string				= symptomCheck.meta.reminder || reminderDefaultSetting

		assert(typeof reminderTime === 'string', 'DayQueryReminderService.getNotifications() unable to get valid reminder', { symptomCheck, reminderDefaultSetting } )

		const notifications	: ScheduledNotification[]	= new Array<ScheduledNotification>()
		const now			: number					= Date.now()


		await Promise.all(targetDays.map( async (targetDay: string) => {
			const dateObject	: Date		= new Date(`${targetDay}T${reminderTime}`)
			const timestamp		: number	= dateObject.getTime()

			if (timestamp < now) 			return null

			const dueQuestions	: Question[]	= await this.dayQueryService.getDueQuestions([symptomCheck], targetDay)

			if (dueQuestions.length === 0) 	return null

			const source	: string	= this.sourceName
			const date		: string 	= targetDay
			const time 		: string	= reminderTime
			const scId		: string 	= symptomCheck.id
			const id		: string	= `${source}-${scId}-${date}`
			const title		: string 	= this.rccTranslationService.translate('DAY_QUERIES.NOTIFICATION.TITLE')
			const body		: string 	= this.rccTranslationService.translate('DAY_QUERIES.NOTIFICATION.BODY')

			const data		: Record<string, string|number|boolean>	= { source, date, time, scId, id }

			notifications.push({ timestamp, data, title, body })
		}))

		return notifications
	}

	public ngOnDestroy(): void {
		this.destroy$.next()
		this.destroy$.complete()
	}
}
