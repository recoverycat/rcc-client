import { ChangeDetectionStrategy, Component, inject } from '@angular/core'
import { RouterModule } from '@angular/router'
import { TranslationsModule } from '@rcc/common/src/translations/ng/translations.module'
import { RccThemeModule } from '@rcc/themes/active'
import { QrCodeModule, QrCodeService, RccColorCategoryDirective } from '@rcc/common'
import { FinalSlideComponent } from '../final-slide/final-slide.component'

@Component({
    selector: 'rcc-empty-state-none',
    templateUrl: './empty-state-none.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    imports: [
        RouterModule,
        FinalSlideComponent,
        TranslationsModule,
        RccThemeModule,
        RccColorCategoryDirective,
        QrCodeModule,
    ]
})
export class EmptyStateNoneComponent {
    private qrCodeService: QrCodeService = inject(QrCodeService)

    protected receiveQuestions(): void {
        void this.qrCodeService.scanAndAnnounce()
    }
}
