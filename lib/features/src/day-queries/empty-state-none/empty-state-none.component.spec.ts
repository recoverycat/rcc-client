import { ComponentFixture, TestBed } from '@angular/core/testing'
import { EmptyStateNoneComponent } from './empty-state-none.component'
import { IncomingDataServiceModule, QrCodeModule, QrCodeService } from '@rcc/common'
import { provideRouter } from '@angular/router'

describe('EmptyStateNoneForTodayComponent', () => {
    let qrCodeService: jasmine.SpyObj<QrCodeService> = undefined!
    let fixture: ComponentFixture<EmptyStateNoneComponent> = undefined!

    beforeEach(() => {
        qrCodeService = jasmine.createSpyObj('QrCodeService', ['scanAndAnnounce']) as jasmine.SpyObj<QrCodeService>
        fixture = TestBed
            .configureTestingModule({
                imports: [
                    IncomingDataServiceModule,
                ],
                providers: [
                    provideRouter([])
                ]
            })
            .overrideComponent(EmptyStateNoneComponent, {
                remove: { imports: [QrCodeModule ] },
                add: { providers: [
                    { provide: QrCodeService, useValue: qrCodeService },
                ] }
            })
            .createComponent(EmptyStateNoneComponent)
    })

    it('Should render', () => {
        expect(fixture.nativeElement).not.toBeNull()
    })

    it('Should open the QR code modal when button is clicked', () => {
        const button: HTMLButtonElement = (fixture.nativeElement as HTMLElement).querySelector('button') as unknown as HTMLButtonElement

        button.click()

        // eslint-disable-next-line @typescript-eslint/unbound-method
        expect(qrCodeService.scanAndAnnounce).toHaveBeenCalledWith()
    })
})
