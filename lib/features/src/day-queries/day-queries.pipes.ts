import	{
			Pipe,
			PipeTransform
		}									from '@angular/core'
import	{	Question					}	from '@rcc/core'
import	{	DayQueryService				}	from './day-query.service'
import	{	QuestionnaireService		}	from '../questions/questionnaire'
import { QueryControl } from '../queries'
@Pipe({
    name: 'hasAnAnswerForToday',
    standalone: false
})
export class HasAnAnswerForTodayPipe implements PipeTransform{
	public constructor(
		public dayQueryService		: DayQueryService,
		public questionnaireService	: QuestionnaireService

		){}

	public async transform(questionOrId: Question | string) : Promise<boolean> {
		const question: Question			=	questionOrId instanceof Question
												?	questionOrId
												:	await this.questionnaireService.get(questionOrId)
		const query: QueryControl 			=	await this.dayQueryService.getQueryControl(question)
		const hasAnswer: boolean			=	query && !!query.entry

		return hasAnswer

	}
}
