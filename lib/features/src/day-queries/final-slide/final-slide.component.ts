import { ChangeDetectionStrategy, Component } from '@angular/core'
import { RccToIDPipeModule, TranslationsModule } from '@rcc/common'

@Component({
    selector: 'rcc-final-slide',
    templateUrl: './final-slide.component.html',
    styleUrls: ['./final-slide.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    imports: [RccToIDPipeModule, TranslationsModule]
})
export class FinalSlideComponent {}
