import { ComponentFixture, TestBed } from '@angular/core/testing'
import { FinalSlideComponent } from './final-slide.component'

describe('FinalSlideComponent', () => {
    it('Should render without errors', () => {
        const fixture: ComponentFixture<FinalSlideComponent> =  TestBed.createComponent(FinalSlideComponent)

        expect(fixture.nativeElement).not.toBeNull()
    })
})
