import { CommonModule } from '@angular/common'
import { Component, Input } from '@angular/core'
import { RccIconComponent } from '@rcc/common'
import { IconName } from '@rcc/common/ui-components/icons/icon-names'

@Component({
    templateUrl: './common-computed-value.component.html',
    styleUrls: ['./common-computed-value.component.scss'],
    selector: 'common-computed-value',
    imports: [
        RccIconComponent,
        CommonModule,
    ]
})
export class CommonComputedValueComponent {
	@Input()
	public icon: IconName

	@Input()
	public value: unknown

	@Input()
	public displayValue: string
}
