import { NgModule } from '@angular/core'
import { MedicalFeatureModule } from '@rcc/common/medical/medical-feature/medical-feature.module'
import { provideComputedValue } from '../computed-values.commons'
import { AnswerCountComputedValueComponent } from './answer-count-computed-value.component'
import { provideTranslationMap } from '@rcc/common'

const de: Record<string, string> = {
  DESCRIPTION: 'Die Anzahl an Antworten in diesem Zeitraum vs. im letzten Zeitraum'
}

const en: Record<string, string> = {
  DESCRIPTION: 'This timeframe\'s answer count vs. last timeframe\'s answer count'
}

@NgModule({
	imports: [
		MedicalFeatureModule,
	],
	providers: [
		provideComputedValue(AnswerCountComputedValueComponent),
		provideTranslationMap('COMPUTED_ANSWER_COUNT', { de, en })
	]
})
export class AnswerCountComputedValueModule {}
