import	{	NgModule								}	from '@angular/core'
import	{	BooleanCountComputedValueComponent		}	from './boolean-count-computed-value.component'
import	{
			provideComputedValue
		}												from '../computed-values.commons'
import	{
			MedicalFeatureModule,
			provideTranslationMap,
		}												from '@rcc/common'

const en : Record<string, string> =  {
	'DESCRIPTION':     'This timeframe\'s yes count vs. last timeframe\'s yes count'
}

const de : Record<string, string> =  {
	'DESCRIPTION':     'Anzahl \'Ja\'-Antworten in diesem Zeitraum vs. im vorherigen Zeitraum'
}


@NgModule({
	imports: [
		MedicalFeatureModule
	],
	providers: [
		provideComputedValue(BooleanCountComputedValueComponent),
		provideTranslationMap('COMPUTED_BOOLEAN_COUNT', { en, de })
	]
})
export class BooleanCountComputedValueModule {}
