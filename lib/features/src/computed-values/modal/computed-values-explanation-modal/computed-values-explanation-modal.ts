import { Component } from '@angular/core'
import { RccModalController, SharedModule } from '@rcc/common'
import { ComputedValuesExplanationTranslationModule } from './computed-values-explanation-translation.module'

@Component({
    templateUrl: './computed-values-explanation-modal.html',
    styleUrls: ['./computed-values-explanation-modal.scss'],
    selector: 'computed-values-explanation-modal',
    imports: [
        SharedModule,
        ComputedValuesExplanationTranslationModule,
    ]
})
export class ComputedValuesExplanationModalComponent {
	public constructor(
		private readonly rccModalController: RccModalController
	) {}

	protected closeModal(): void {
		this.rccModalController.dismiss()
	}
}
