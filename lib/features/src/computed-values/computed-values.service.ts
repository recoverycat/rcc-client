import	{
			Injectable,
			Inject,
			Optional
		}								from '@angular/core'

import	{
			DataViewControl
		}								from '../data-visualization'


import	{
			COMPUTED_VALUES
		}								from './computed-values.commons'

import	{
			ComputedValueComponent
		}								from './computed-value-component.class'


/**
 * This service manages {@link ComputedValueComponent}s.
 */
@Injectable({
	providedIn: 'root'
})
export class RccComputedValuesService {


	public constructor(
		@Optional() @Inject(COMPUTED_VALUES)
		protected computedValueComponents : (typeof ComputedValueComponent)[]

	){
		this.computedValueComponents = computedValueComponents || []
	}

	/**
	 * @returns all {@link ComputedValueComponent}s that are compatible with the
	 * provided {@link DataViewControl}, which means they have a matching score of 0 or greater,
	 * sorted by how well they match. See {@link ComputedValueComponent.match}
	 */
	public getComputedValues(dataViewControl: DataViewControl) : (typeof ComputedValueComponent)[]  {

		return 	this.computedValueComponents
				.filter( computedValueComponent => computedValueComponent.match(dataViewControl) >= 0)
				.sort( (a, b) => b.match(dataViewControl) - a.match(dataViewControl) )
	}

}
