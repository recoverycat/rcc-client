import	{
			Component,
			Input,
			OnChanges,
		}										from '@angular/core'

import	{
			NgIf,
			NgComponentOutlet,
		}										from '@angular/common'

import	{
			TranslationsModule,
			RccColorCategoryDirective,
			RccIconComponent,
			RccModalController
		}										from '@rcc/common'

import	{
			ColorCategory,
		}										from '@rcc/themes'


import	{
			DataViewControl
		}										from '../../data-visualization'

import	{
			ComputedValueComponent,
			ComputedValueCategory
		}										from '../computed-value-component.class'

import	{
			RccComputedValuesService
		}										from '../computed-values.service'
import	{
			RccComputedValuesListTranslationModule
		}										from './computed-values-list-translation.module'
import	{
			ComputedValuesExplanationModalComponent
		}										from '../modal/computed-values-explanation-modal/computed-values-explanation-modal'
import	{
			ComputedValuesExplanationTranslationModule
		}										from '../modal/computed-values-explanation-modal/computed-values-explanation-translation.module'
import	{	CalendarDateString				}	from '@rcc/core'

/**
 * This components renders a list with all compatible
 * {@link ComputedValueComponents} of a provided {@link DataViewControl}
 *
 */
@Component({
    selector: 'rcc-computed-values-list',
    templateUrl: './computed-values-list.component.html',
    styleUrls: ['./computed-values-list.component.scss'],
    imports: [
        NgIf,
        NgComponentOutlet,
        RccIconComponent,
        RccColorCategoryDirective,
        TranslationsModule,
        RccComputedValuesListTranslationModule,
        ComputedValuesExplanationTranslationModule,
    ]
})

export class RccComputedValuesListComponent implements OnChanges {

	@Input()
	public dataViewControl 	: 	DataViewControl

	/**
	 * Highlight color for possibly more prominent values. Or just any when no
	 * values need special highlighting. See {@link ComputedValueCategory}.
	 */
	@Input()
	public colorA			:	ColorCategory


	/**
	 * Another highlight color for possibly less prominent values, but in any
	 * case are different from the values for colorA.
	 * See {@link ComputedValueCategory}.
	 */
	@Input()
	public colorB			:	ColorCategory

	/**
	 * Color for values that possibly need no highlighting at all, but in any
	 * case are different from the values for colorB and colorA.
	 * See {@link ComputedValueCategory}.
	 */
	@Input()
	public colorN					: ColorCategory

	public matchingComputedValues	: (typeof ComputedValueComponent)[]
	public viableComputedValues		: (typeof ComputedValueComponent)[]
	public valueToShow				: typeof ComputedValueComponent

	public rawValues				: Map<typeof ComputedValueComponent, unknown>
									= new Map<typeof ComputedValueComponent, unknown>()

	public colorCategories			: Map<typeof ComputedValueComponent, ColorCategory>
									= new Map<typeof ComputedValueComponent, ColorCategory>()


	public constructor(
		private rccComputedValuesService	: RccComputedValuesService,
		private rccModalController			: RccModalController,
	){}

	protected open: boolean = false

	public updateComputedValues() : void {

		this.matchingComputedValues = 	this.dataViewControl
									?	this.rccComputedValuesService.getComputedValues(this.dataViewControl)
									:	[]

		this.rawValues				=	new Map<typeof ComputedValueComponent, unknown>()
		this.colorCategories		=	new Map<typeof ComputedValueComponent, ColorCategory>()


		this.matchingComputedValues.forEach( computedValue  => {

			const rawValue			: 	unknown
									= 	computedValue.getRawValue(this.dataViewControl)

			if(rawValue === null)
				console.warn(
					'RccComputedValuesListComponent.updateComputedValues(): unable to calculate raw value for ComputedValue:',
					{
						computedValue,
						dataViewControl: this.dataViewControl
					}
				)

			this.rawValues.set(computedValue,  rawValue)
		})

		this.viableComputedValues = this.matchingComputedValues
			.filter(computedValue => this.getRawValue(computedValue) !== null)

		this.valueToShow = this.viableComputedValues.length > 0 ? this.viableComputedValues[0] : undefined

		this.matchingComputedValues.forEach( computedValue  => {

			const rawValue			: 	unknown =this.getRawValue(computedValue)

			const valueCategory		: 	ComputedValueCategory
									= 	computedValue.getValueCategory(rawValue)

			const colorCategory		: 	ColorCategory
									= 		valueCategory === 'A'
										?	this.colorA
										:	valueCategory === 'B'
										?	this.colorB
										:	valueCategory === 'N'
										?	this.colorN
										:	'quinary'

			this.colorCategories.set(computedValue, colorCategory)
		})


	}


	public getRawValue(computedValue: typeof ComputedValueComponent) : unknown {
		const earliestDate: string = this.dataViewControl.datasets[0].datapoints.reduce((previousDate, nextDate) => {
			if (previousDate === '') return nextDate.date
			if (CalendarDateString.diffInDays(nextDate.date, previousDate) > 0)
				return previousDate
			return nextDate.date

		}, '')

		const differenceInDays: number = CalendarDateString.diffInDays(earliestDate, this.dataViewControl.startDate)
		const earliestDateIsBeforeCurrentRange: boolean = differenceInDays < 0

		if (!earliestDateIsBeforeCurrentRange) return undefined
		return this.rawValues.get(computedValue)
	}


	public getColorCategory(computedValue: typeof ComputedValueComponent) : ColorCategory {
		return this.colorCategories.get(computedValue)
	}

	protected openComputedValuesList(): void {
		this.open = true
	}

	protected closeComputedValuesList(): void {
		this.open = false
	}

	public ngOnChanges(): void {
		this.updateComputedValues()
	}

	protected get hasValueToShow(): boolean {
		return this.viableComputedValues.length > 0 && this.valueToShow !== undefined
	}

	protected showInfoModal(): void {
		void this.rccModalController.present(
			ComputedValuesExplanationModalComponent,
			{},
			{ mini: true },
		)
	}
}
