import { NgModule } from '@angular/core'
import { provideTranslationMap } from '@rcc/common'

const de: Record<string, string> = {
	'SHOW_TRENDS': 'Trends zeigen'
}

const en: Record<string, string> = {
	'SHOW_TRENDS': 'Show trends'
}

@NgModule({
	providers: [
		provideTranslationMap('COMPUTED_VALUES_LIST', { de, en })
	]
})
export class RccComputedValuesListTranslationModule {}
