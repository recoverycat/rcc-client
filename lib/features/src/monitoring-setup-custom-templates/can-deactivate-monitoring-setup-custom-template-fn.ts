import { CanDeactivateFn } from '@angular/router'
import { Type, inject } from '@angular/core'
import { RccAlertController } from '@rcc/common'
import { RccQuestionnaireEditorService } from '../questionnaire-editor/questionnaire-editor.service'

export const canDeactivateMonitoringSetupCustomTemplateFn: CanDeactivateFn<Type<unknown>> = () => {
    const rccAlertController        : RccAlertController
                                    = inject(RccAlertController)

    const questionnaireEditorService : RccQuestionnaireEditorService
                                    = inject(RccQuestionnaireEditorService)

    if(questionnaireEditorService.dirty)
        return rccAlertController
            .confirm(
                'MONITORING_SETUP.CONFIRM_RESET',
                'YES',
                'NO'
            )
            .then(
                () => true,
                () => false
            )

    return true
}
