
import 	{ 	Component				}		from '@angular/core'

import	{	WidgetComponent			}		from '@rcc/common'

import	{
			CalendarDateString,
			isSameDay
		} 									from '@rcc/core'

import	{
			DataViewControl,
			Dataset,
			Datapoint
		}									from '../data-visualization'


@Component({
    templateUrl: './fallback-data-view-widget.component.html',
    styleUrls: ['./fallback-data-view-widget.component.css'],
    standalone: false
})
export class FallbackDataViewWidgetComponent extends WidgetComponent<DataViewControl> {

	public static controlType 		: typeof DataViewControl
									= DataViewControl

	public static override widgetMatch(): number { return 0 }

	public combinedData 			: Record<string, Dataset[]>
	public relevantDateStrings		: string[]

	public relevantAnswerCount		: number[]

	public constructor(
		public 	dataViewControl	: DataViewControl,
	){

		super(dataViewControl)


		const combinedDataPoints 	:	Datapoint[]
									=	dataViewControl.datasets
										.map( dataSet => dataSet.datapoints)
										.flat()

		const allDateStrings		:	string[]
									= 	combinedDataPoints
										.map( datapoint => datapoint.date)

		const today					:	string
									=	CalendarDateString.today()

		const earliestDate			:	string
									=	allDateStrings.sort()[0]

		const latestDate			:	string
									=	allDateStrings.reverse()[0]

		const startDate				:	string
									=	this.dataViewControl.startDate || earliestDate || today

		const endDate				:	string
									=	this.dataViewControl.endDate || latestDate || today

		this.relevantDateStrings	=	CalendarDateString.range(startDate, endDate)

		this.combinedData 			=	Object.fromEntries(
											this.relevantDateStrings
											.map( dateStr => [
												dateStr,
												dataViewControl.datasets.map( dataset => ({
													question: 	dataset.question,
													datapoints:	dataset.datapoints.filter( datapoint => isSameDay(datapoint.date, dateStr))
												}))

											])
										)

		function inRange(datapoint: Datapoint) : boolean {
			return (startDate <= datapoint.date) &&  (datapoint.date <= endDate)
		}

		this.relevantAnswerCount	=	dataViewControl.datasets
										.map( (dataset 		: Dataset)		=> 	dataset.datapoints)
										.map( (datapoints 	: Datapoint[]) 	=> 	datapoints.filter( inRange ).length )

	}

	public hasAnswers(day: string) : boolean {
		return this.combinedData[day].some( dataset => dataset.datapoints.length !== 0)
	}



}
