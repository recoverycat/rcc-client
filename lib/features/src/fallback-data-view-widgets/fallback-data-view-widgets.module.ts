import 	{
			NgModule
		}											from '@angular/core'

import 	{
			SharedModule,
			WidgetsModule,
			provideTranslationMap,
			provideWidget,
		}											from '@rcc/common'

import	{	QuestionnaireServiceModule			}	from '../questions'
import	{	ScheduleModule						}	from '../schedules'

import	{	FallbackDataViewWidgetComponent 	}	from './fallback-data-view-widget.component'


const dataViewWidgetComponents = 	[
										FallbackDataViewWidgetComponent,
									]

import	en from './i18n/en.json'
import	de from './i18n/de.json'


@NgModule({
	providers:[
		provideTranslationMap('FALLBACK_DATA_VIEW_WIDGET', { en,de }),
		provideWidget(FallbackDataViewWidgetComponent),
	],
	imports: [
		SharedModule,
		WidgetsModule,
		QuestionnaireServiceModule,
		ScheduleModule,
	],
	declarations:[
		...dataViewWidgetComponents
	],
	exports: [
		...dataViewWidgetComponents
	]
})
export class FallbackDataViewWidgetsModule {}
