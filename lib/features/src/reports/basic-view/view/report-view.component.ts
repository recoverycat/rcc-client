import	{
			Component,
			Input,
			ChangeDetectorRef,
			OnDestroy
		}								from '@angular/core'
import	{
			FormControl,
			FormGroup
		}								from '@angular/forms'

import	{
			RccModalController
		}								from '@rcc/common'

import	{	Report					}	from '@rcc/core'

import	{
			Subject,
			takeUntil
		}								from 'rxjs'

import	{
			DatasetsService,
			Dataset,
			DataViewControl
		}								from '../../../data-visualization'


@Component({
    selector: 'rcc-report-view',
    styleUrls: ['./report-view.component.css'],
    templateUrl: './report-view.component.html',
    standalone: false
})
export class ReportViewComponent  implements OnDestroy {

	@Input()
	public set report(report: Report){ void this.setup(report) }
	public get report() : Report { return this._report }
	protected _report 					: Report

	@Input()
	public onClose						: () => void

	public dataViewControl				: DataViewControl
	public datasets						: Dataset[] 		= []

	public primaryDatasetFormControl: FormControl<Dataset> = new FormControl<Dataset>(null)
	public secondaryDatasetFormControl: FormControl<Dataset> = new FormControl<Dataset>(null)

	public dataSetControls: FormGroup<{ primary: FormControl<Dataset>, secondary: FormControl<Dataset> }> =
		new FormGroup({
			primary: 	this.primaryDatasetFormControl,
			secondary: 	this.secondaryDatasetFormControl
		})

	public destroy$: Subject<void> = new Subject<void>()

	public constructor(
		protected datasetsService		: DatasetsService,
		protected changeDetectorRef		: ChangeDetectorRef,
		protected rccModalController	: RccModalController
	){}




	public async setup(report: Report): Promise<void>{

		this._report = report

		// get all datasets for this report; some of them may be empty:
		const preliminary_datasets: Dataset[] = await this.datasetsService.getDatasets(report)

		// reduce datasets to only those, that have content
		this.datasets 	=	preliminary_datasets
							.filter(	dataset => 	dataset.datapoints.length > 0	)

		this.dataSetControls.valueChanges
		.pipe( takeUntil(this.destroy$) )
		.subscribe( () => this.updateDataViewControls() )

		this.primaryDatasetFormControl.setValue(this.datasets[0])
		this.secondaryDatasetFormControl.setValue(null)

		this.changeDetectorRef.markForCheck()
	}


	public close(): void {

		if (this.onClose)	this.close()
		else				this.rccModalController.dismiss()

	}


	public updateDataViewControls(): void{

		const datasets: Dataset[] = [
										this.primaryDatasetFormControl.value,
										this.secondaryDatasetFormControl.value
									]
									.filter( x => !!x)


		this.dataViewControl	 = new DataViewControl(datasets)

	}

	public ngOnDestroy() : void {
		this.destroy$.next()
		this.destroy$.complete()
	}

}
