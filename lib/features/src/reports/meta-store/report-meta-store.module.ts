import 	{
			NgModule,
			ModuleWithProviders,
			Type
		} 									from '@angular/core'

import 	{
			RouterModule,
			Routes
		}									from '@angular/router'

import	{
			BaseMetaStoreModule,
			MetaAction,
			MetaStoreModule,
			SharedModule,
			provideTranslationMap,
			MetaStoreConfig,
		}										from '@rcc/common'

import	{
			Report,
			ReportConfig,
			ReportStore,
		}										from '@rcc/core'


import	{	ReportMetaStoreService			}	from './report-meta-store.service'

import	{
			REPORT_STORES,
			REPORT_META_ACTIONS,
		}										from './report-meta-store.commons'

import	{	ReportMetaStorePageComponent 	}	from './overview-page/overview-page.component'



import en from './i18n/en.json'
import de from './i18n/de.json'



const routes: Routes		=	[
									{ path: 'reports',	component: ReportMetaStorePageComponent	},
								]

const metaStoreConfig: MetaStoreConfig<ReportConfig, Report, ReportStore>
							=	{
									itemClass:			Report,
									serviceClass:		ReportMetaStoreService
								}


export class MenuEntryReportMetaStoreService {}


@NgModule({
	imports: [
		SharedModule,
		RouterModule.forChild(routes),
		MetaStoreModule.forChild(metaStoreConfig),
	],
	declarations: [
		ReportMetaStorePageComponent,
	],
	providers:[
		ReportMetaStoreService,
		provideTranslationMap('REPORT_META_STORE', { en, de })
	],
	exports: [
		ReportMetaStorePageComponent,
		// this is important so anything importing ReportMetaStoreServiceModule
		// can use the components of MetaStoreModule too
		MetaStoreModule
	]
})
export class ReportMetaStoreServiceModule extends BaseMetaStoreModule {

	public static 	forChild(
				stores?			: Type<ReportStore>[],
				metaActions?	: MetaAction<ReportConfig, Report, ReportStore>[]
			): ModuleWithProviders<ReportMetaStoreServiceModule> {

		const mwp: ModuleWithProviders<ReportMetaStoreServiceModule>
			= BaseMetaStoreModule.getModuleWithProviders<ReportMetaStoreServiceModule>(this, stores, metaActions, REPORT_STORES, REPORT_META_ACTIONS)
		return mwp
	}

}
