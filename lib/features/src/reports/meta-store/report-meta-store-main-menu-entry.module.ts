import	{
			NgModule,
			Provider,
			ModuleWithProviders,
		}											from '@angular/core'

import	{
			provideMainMenuEntry,
			Factory,
			Action,
			getEffectivePosition,
			WithPosition,
		}											from '@rcc/common'

import	{
			ReportMetaStoreServiceHomePath
		}											from './report-meta-store.commons'

import	{
			ReportMetaStoreServiceModule,
		}											from './report-meta-store.module'

@NgModule({
	imports: [
		ReportMetaStoreServiceModule,
	],

})

export class ReportMetaStoreServiceMainMenuEntryModule {

	/**
	* This method can add entries to the main menu.
	*
	* Calling it without parameter, will add entries to the main menu
	* automatically at reasonably adequate positions.
	*
	* If you want to determine the positions yourself provide a `config` parameter :
	*
	* | .position  | value                  | effect                                                    |
	* |------------|------------------------|-----------------------------------------------------------|
	* | .position  | true                   | adds menu entry at a reasonably adequate position         |
	* |            | positive number        | adds menu entry at position counting from the top         |
	* |            | negative number        | adds menu entry at position counting from the bottom      |
	*
	* Example:     `ReportMetaStoreServiceMainMenuEntryModule.addEntry({ position: 1 })`,
	*/

	public static addEntry(config?: WithPosition): ModuleWithProviders<ReportMetaStoreServiceMainMenuEntryModule> {

		const mainMenuEntry	: Factory<Action> =	{
			deps:		[],
			factory:	() => ({
				label: 			'REPORT_META_STORE.MENU_ENTRY',
				icon: 			'report',
				path:			ReportMetaStoreServiceHomePath,
				position:		getEffectivePosition(config, -3),

			})
		}

		const providers : Provider[] = []

		providers.push(provideMainMenuEntry(mainMenuEntry))

		return {
			ngModule:	ReportMetaStoreServiceMainMenuEntryModule,
			providers
		}
	}

}
