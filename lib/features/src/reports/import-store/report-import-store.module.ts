import	{	NgModule				}	from '@angular/core'
import	{	Report					}	from '@rcc/core'
import	{
			IncomingDataServiceModule,
			provideTranslationMap,
			provideItemAction,
			ItemAction
		}									from '@rcc/common'
import	{	ReportMetaStoreServiceModule	}	from '../meta-store'
import	{	ReportImportStoreService		}	from './report-import-store.service'

import en from './i18n/en.json'
import de from './i18n/de.json'

const itemAction: ItemAction<Report> = 	{
							itemClass:		Report,
							storeClass: 	ReportImportStoreService,
							role:			'destructive' as const,
							getAction:		(report: Report, store: ReportImportStoreService) => ({
								label: 			'REPORT_IMPORT_STORE.DELETE',
								icon:			'remove',
								handler: 		() => store.deleteReport(report),
								successMessage:	'REPORT_IMPORT_STORE.ACTIONS.DELETE_SUCCESS',
								failureMessage:	'REPORT_IMPORT_STORE.ACTIONS.DELETE_FAILURE'
							})

						}



@NgModule({
	declarations: [],
	imports: [
		ReportMetaStoreServiceModule.forChild([ReportImportStoreService]),
		IncomingDataServiceModule
	],
	providers: [
		provideItemAction(itemAction),
		provideTranslationMap('REPORT_IMPORT_STORE', { en,de }),
		ReportImportStoreService
	]
})
export class ReportImportStoreServiceModule {}
