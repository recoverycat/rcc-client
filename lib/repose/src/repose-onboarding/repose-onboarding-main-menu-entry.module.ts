import	{
			NgModule,
			Provider,
			ModuleWithProviders,
		}											from '@angular/core'
import	{
			provideMainMenuEntry,
			Factory,
			Action,
			WithPosition,
			getEffectivePosition,
		}											from '@rcc/common'
import	{
			ReposeOnboardingModule
		}											from './repose-onboarding.module'
import	{
			ReposeOnboardingService
		}											from './repose-onboarding.service'

@NgModule({
	imports:[
		ReposeOnboardingModule
	]
})
export class ReposeOnboardingMainMenuEntryModule {
	public static addEntry(config?: WithPosition): ModuleWithProviders<ReposeOnboardingMainMenuEntryModule> {
		const mainMenuEntry	: Factory<Action> =	{
		deps:	[ReposeOnboardingService],
		factory: (reposeOnboardingService: ReposeOnboardingService) => ({
				position: 		getEffectivePosition(config, 1),
				icon:			'open',
				label:			'REPOSE_ONBOARDING.MENU_ENTRY',
				handler:		() =>  reposeOnboardingService.setup()
			})
		}

		const providers : Provider[] = [provideMainMenuEntry(mainMenuEntry)]

		return {
			ngModule:	ReposeOnboardingMainMenuEntryModule,
			providers
		}
	}
}
