import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http'
import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { provideTranslationMap, SharedModule } from '@rcc/common'
import { ReposeExportService } from './repose-export.service'
import { ReposeHttpService } from './repose-http.service'
import { ReposePageComponent } from './repose-page/repose-page.component'
import { ReposeDataModule } from './repose-data/repose-data.module'

import en from './i18n/en.json'
import de from './i18n/de.json'

const routes : Routes = [
	{ path: 'repose', component: ReposePageComponent }
]

/**
 * This module contains anything to do with the REPOSE study
 */
@NgModule({
	declarations: [
		ReposePageComponent						// landing page
	],
	imports: [
		ReposeDataModule,
		SharedModule,							// ionic icons
		RouterModule.forChild(routes)			// routing
	],
	providers: [
		provideTranslationMap('REPOSE', { en,de }),
        provideHttpClient(withInterceptorsFromDi()),
		ReposeExportService,
		ReposeHttpService,
	]
})
export class ReposeModule {}
