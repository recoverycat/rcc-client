import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { RccAlertController, RccRetryService, RccSettingsService, RccToastController, RetryAlertConfig } from '@rcc/common'
import { zip } from '@rcc/core'
import { firstValueFrom } from 'rxjs'

@Injectable()
export class ReposeHttpService {
	// Charité Nextcloud server
	private serverUrl : string = 'https://psyshare.bihealth.org/public.php/webdav/'

	// Nextcloud-specific:
	// The username for authorization is the ID of the public FileDrop folder,
	// i.e. the last part of the shareable URL
	private headers : HttpHeaders = new HttpHeaders({
		'Authorization': 'Basic ' + btoa('2XS28dpcd6tD7oN:'),
		'X-Requested-With': 'XMLHttpRequest'
	})

	private retryAlertMessages : RetryAlertConfig = {
		successMsg		: 'REPOSE.RETRY.SUCCESS',
		retryMsg		: 'REPOSE.RETRY.FAILURE',
		confirmLabel	: 'REPOSE.RETRY.CONFIRM',
		cancelLabel		: 'REPOSE.RETRY.CANCEL'
	}

	public constructor(
		private http: HttpClient,
		private rccToastController: RccToastController,
		private rccAlertController: RccAlertController,
		private router: Router,
		private rccSettingsService: RccSettingsService,
		private rccRetryService: RccRetryService
	) {}

	/**
	 * Repose-specific wrapper for `HttpClient.put()`. Shows success/failure toasts.
	 * @param filename Filename of the newly created file, appended to `this.serverUrl`
	 * @param data Data to send
	 */
	private async put(filename: string, data: unknown): Promise<void> {
		const url : string = this.serverUrl + filename
		const headers : HttpHeaders = this.headers

		// Make PUT request and convert the resulting Observable to a Promise
		// Other functions calling put() then know whether the request was successful
		const result : Promise<unknown> = firstValueFrom(this.http.put(url, data, { headers }))
		void result.catch(() => void this.rccToastController.failure('REPOSE.HTTP_REQUEST.FAILURE'))

		await result
		void this.rccToastController.success('REPOSE.HTTP_REQUEST.SUCCESS')
	}

	/**
	 * Sends a HTTP PUT request containing data to a public folder on the Charité Nextcloud server
	 * @param data An array containing CSV and JSON data as strings
	 */
	public async sendReposeExportToServer(data: [string, string]): Promise<void> {
		// Filename of the generated file is the pseudonym
		const pseudonym : string = (await this.rccSettingsService.get('repose-pseudonym')).value
		const filenames : string[] = [pseudonym + '.csv', pseudonym + '.json']

		// Password for the zip archive is REPOSE passphrase from settings
		const passphrase : string = (await this.rccSettingsService.get('repose-passphrase')).value

		if (!pseudonym || !passphrase) {
			await this.rccAlertController.present({ message: 'REPOSE.MISSING_RESPONDENT_DATA' })
			void this.router.navigate(['/'])
			throw new Error('Missing REPOSE pseudonym or passphrase.')
		}

		const zipData : Blob = await zip(data, filenames, passphrase)

		await this.rccRetryService.retry(() => this.put(pseudonym + '.zip', zipData), this.retryAlertMessages)
	}

	/**
	 * Tests the connection to the Repose server and data flow.
	 * Zips a string, encrypts the file with the passphrase and sends it to `this.serverUrl`.
	 */
	public async testReposeConnection(): Promise<void> {
		const pseudonym		: string = (await this.rccSettingsService.get('repose-pseudonym')).value
		const passphrase	: string = (await this.rccSettingsService.get('repose-passphrase')).value

		const zipData : Blob = await zip('Data transmission successful! Pseudonym and passphrase match.', pseudonym + '.json', passphrase)

		await this.rccRetryService.retry(() => this.put(pseudonym + '-test.zip', zipData), this.retryAlertMessages, 350)
	}
}

