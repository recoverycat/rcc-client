import { Injectable } from '@angular/core'
import { RccSettingsService } from '@rcc/common'
import { Entry, Question, QuestionnaireResponse, Report, report2csv, report2json } from '@rcc/core'
import { JournalService, QuestionnaireService } from '@rcc/features'

/**
 * This service exports questions + user entries from app storage to CSV and JSON
 */
@Injectable()
export class ReposeExportService {
	public constructor(
		private journalService: JournalService,
		private questionnaireService: QuestionnaireService,
		private rccSettingsService: RccSettingsService
	) {}

	/**
	 * @returns all entries from the journal
	 */
	private async getEntries(): Promise<Entry[]> {
		await this.journalService.ready
		return this.journalService.items
	}

	/**
	 * @returns all questions from the questionnaire
	 */
	private async getQuestions(): Promise<Question[]> {
		await this.questionnaireService.ready
		return this.questionnaireService.items
	}

    /**
	 * Converts a Report to the desired REPOSE export format:
     * An array containing a CSV string and a JSON string
	 * @param {Report}	report	Report, defaults to undefined.
	 * In that case, a new Report of all answered items will be generated
	 */
	public async export(report: Report = undefined): Promise<[string, string]> {
        const questions : Question[] = await this.getQuestions()
		if (!report) report = Report.from(await this.getEntries())

		const pseudonym : string = (await this.rccSettingsService.get('repose-pseudonym')).value
		const language : string = (await this.rccSettingsService.get('activeLanguage')).value

        const csv : string = report2csv(report, questions, language, pseudonym)

        const json  : QuestionnaireResponse[] = report2json(report, questions, language, pseudonym)

        return [csv, JSON.stringify(json, null, 2)]
	}
}
