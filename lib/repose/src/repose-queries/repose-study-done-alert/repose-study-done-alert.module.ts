import	{
			NgModule,
		}									from '@angular/core'
import	{
			ReposeStudyDoneAlertService
		}									from './repose-study-done-alert.service'

@NgModule({
	providers: [
		ReposeStudyDoneAlertService
	]
})
export class ReposeStudyDoneAlertModule {
	public constructor(
		private reposeStudyDoneAlertService	: ReposeStudyDoneAlertService,
	) {
		window.addEventListener('focus', () => void this.reposeStudyDoneAlertService.alertStudyEnd())
	}
}
