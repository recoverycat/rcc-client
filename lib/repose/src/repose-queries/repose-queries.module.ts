import	{
			NgModule
		}									from '@angular/core'
import	{
			RouterModule,
			Routes
		}									from '@angular/router'
import	{
			KeyValuePairConfig,
			provideKeyValuePair,
			provideTranslationMap,
			SharedModule,
			TranslationsModule,
		}									from '@rcc/common'
import	{
			QueriesModule,
			SymptomCheckQueryModule
		}									from '@rcc/features'
import	{
			ReposeQueryRunPageComponent
		}									from './repose-query-run-page'
import	{
			ReposeNewQueryAlertModule
		}									from './repose-new-query-alert'
import	{
			ReposeStudyDoneAlertModule
		}									from './repose-study-done-alert'

import en from './i18n/en.json'
import de from './i18n/de.json'

const routes : Routes = [
	{
		path:		'repose-query-run',
		component:	ReposeQueryRunPageComponent
	}
]

export interface SymptomCheckTransmissions {
	[id: string]: string
}

const reposeServerTransmissions : KeyValuePairConfig<SymptomCheckTransmissions> = {
	key		: 'repose-last-transmission-dates',
	value	: {}
}

/**
 * This module handles the REPOSE-specific query run and everything related.
 */
@NgModule({
	imports: [
		RouterModule.forChild(routes),
		QueriesModule,
		SharedModule,
		TranslationsModule,
		SymptomCheckQueryModule,
		ReposeNewQueryAlertModule,
		ReposeStudyDoneAlertModule,
	],
	declarations: [
		ReposeQueryRunPageComponent
	],
	providers: [
		provideTranslationMap('SYMPTOM_CHECK_QUERIES', { en, de }),
		provideKeyValuePair(reposeServerTransmissions),
	]
})
export class ReposeQueryModule {}
