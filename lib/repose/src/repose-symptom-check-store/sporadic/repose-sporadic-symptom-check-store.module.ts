import 	{	NgModule 								} 	from '@angular/core'
import	{
			QuestionnaireServiceModule,
			SymptomCheckMetaStoreServiceModule
		}												from '@rcc/features'
import	{	ReposeSporadicQuestionStore				}	from './repose-sporadic-question-store.service'
import	{	ReposeSporadicSymptomCheckStore			}	from './repose-sporadic-symptom-check-store.service'

/**
 * This module provides the sporadic questions and symptom checks for the REPOSE study.
 */
@NgModule({
	imports: [
		SymptomCheckMetaStoreServiceModule.forChild([ReposeSporadicSymptomCheckStore]),
		QuestionnaireServiceModule.forChild([ReposeSporadicQuestionStore]),
	],
	providers: [
		ReposeSporadicSymptomCheckStore,
		ReposeSporadicQuestionStore,
	]
})
export class ReposeSporadicSymptomCheckStoreModule{}
