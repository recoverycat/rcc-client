import 	{
			Injectable
		}										from '@angular/core'
import	{
			RccStorage
		}										from '@rcc/common'
import	{
			CalendarDateString,
			InfoSlideConfig,
			SymptomCheck,
			SymptomCheckConfig,
			SymptomCheckStore,
			QuestionConfig,
			assertProperty,
			assertSporadicScheduleConfig,
		}										from '@rcc/core'
import	{
			ReposeAssessmentQuestionConfigs,
		}										from './repose-sporadic-question-store.service'


/**
 * This {@link SymptomCheckStore} registers the sporadic symptom checks for the REPOSE study,
 * i.e. the short and full assessments.
 */
@Injectable()
export class ReposeSporadicSymptomCheckStore extends SymptomCheckStore {
	public ready : Promise<void>
	/**
	 * Hard-coded configs of sporadic symptom checks for REPOSE
	 */
	public assessmentConfigs : SymptomCheckConfig[] = [shortAssessment, fullAssessmentBlock1, fullAssessmentBlock2, fullAssessmentBlock3, fullAssessmentBlock3NoCTS, fullAssessmentBlock4]
	
	public constructor(
		private rccStorage : RccStorage,
	) {
		super(rccStorage.createItemStorage('rcc-repose-sporadic-sc-import-store'))
		this.ready = this.setup()
	}

	public async setup(): Promise<void> {
		await this.ready
	}

	/**
	 * Populates the item storage with `this.assessmentConfigs`.
	 */
	public async populateItemStore() : Promise<void> {
		// Await ItemStore.ready
		// super.ready doesn't work since SymptomCheckStore doesn't have .ready
		await this.ready

		this.assessmentConfigs.forEach(config => this.addFromConfig(config))
		await this.storeAll()
	}

	/**
	 * Checks if `config.id` exists in {@link SymptomCheckStore}.
	 * If not, adds `config` and sets its `startDate` to today.
	 */
	private addFromConfig(config: SymptomCheckConfig) : void {
		assertProperty(config, 'id', 'Could not add SymptomCheckConfig since it doesn\'t have an id')

		const symptomCheck : SymptomCheck = this.items.find(sc => sc.id === config.id)
		if (!symptomCheck) {
			const configCopy : SymptomCheckConfig = Object.assign({}, config)
			assertSporadicScheduleConfig(configCopy.meta.defaultSchedule)

			// startDate must be today so that the scheduling is calculated correctly
			configCopy.meta.defaultSchedule.startDate = CalendarDateString.today()
			this.addConfig(configCopy)
		}
	}

	/**
	 * Checks if the REPOSE symptom checks have already been created.
	 * Can be used as a proxy to check whether the study has started.
	 */
	public isRunning() : boolean {
		const ids		: string[] = this.assessmentConfigs.map(config => config.id)
		const storedIds : string[] = this.items.map(sc => sc.id)
		return ids.every(id => storedIds.includes(id))
	}
}


/**
 * Returns the IDs of all questions whose config tags include `tag`.
 */
function filterByTag(tag: string, arrayToFilter: QuestionConfig[] = undefined) : string[] {
	const configs : QuestionConfig[] = arrayToFilter ?? ReposeAssessmentQuestionConfigs

	return	configs
			.filter(config => config.tags.includes(tag))
			.map(config => config.id)
}


const shortAssessmentConfigs : QuestionConfig[] = ReposeAssessmentQuestionConfigs.filter(config => config.tags.includes('short-assessment'))
const shortAssessmentSlides : (string | InfoSlideConfig)[] = []
shortAssessmentSlides.push({ message : { translations: { en: '', de: 'Sie finden nachstehend eine Liste von Problemen und Beschwerden, die man manchmal hat. Bitte lesen Sie jede Frage einzeln sorgfältig durch und entscheiden Sie, wie stark Sie durch diese Beschwerden gestört oder bedrängt worden sind, und zwar während der vergangenen 7 Tage bis heute. Überlegen Sie bitte nicht erst, welche Antwort „den besten Eindruck“ machen könnte, sondern antworten Sie so, wie es für Sie persönlich zutrifft. Klicken Sie bitte bei jeder Frage nur das Kästchen mit der für Sie am besten zutreffenden Antwort an. Bitte beantworten Sie jede Frage!' } } })
shortAssessmentSlides.push(...filterByTag('BSI53', shortAssessmentConfigs))
shortAssessmentSlides.push({ message : { translations: { en: '', de: 'Der folgende Fragebogen enthält eine Reihe von Feststellungen, mit denen man sich selbst beschreiben kann. Diese Feststellungen können genau auf Sie zutreffen, eher zutreffen, eher nicht oder gar nicht auf Sie zutreffen. Zur Beantwortung des Fragebogens wählen Sie den entsprechenden Kreis aus. Bitte beantworten Sie jede Feststellung, auch wenn Sie einmal nicht sicher sind, welche Antwort für Sie zutrifft. Klicken Sie dann diejenige Antwort an, die noch am ehesten auf Sie zutrifft.' } } })
shortAssessmentSlides.push(...filterByTag('BISBAS', shortAssessmentConfigs))
shortAssessmentSlides.push({ message : { translations: { en: '', de: 'Denken Sie an die schlimmste Angst, die Sie je erlebt haben oder die Sie sich vorstellen können, und geben Sie dieser die Zahl 100. Denken Sie nun an einen Zustand der absoluten Ruhe und geben Sie diesem die Zahl Null. Nun haben Sie eine Skala.' } } })
shortAssessmentSlides.push(...filterByTag('SUD', shortAssessmentConfigs))
shortAssessmentSlides.push({ message : { translations: { en: '', de: 'Dieser Fragebogen enthält eine Reihe von Wörtern, die unterschiedliche Gefühle und Empfindungen beschreiben. Lesen Sie jedes Wort und klicken dann in der Skala zu jedem Wort die Intensität an. Sie haben die Möglichkeit, zwischen fünf Abstufungen zu wählen. Geben Sie bitte an, wie Sie sich im Verlauf der letzten 12 Monate gefühlt haben.' } } })
shortAssessmentSlides.push(...filterByTag('PANAS', shortAssessmentConfigs))
shortAssessmentSlides.push({ message : { translations: { en: '', de: 'Bitte beantworten Sie die folgende Frage im Hinblick darauf, wie viele Schwierigkeiten, die aufgrund von Gesundheitsproblemen entstehen können, Sie in den letzten 30 Tagen bei dieser nachfolgenden Aktivität hatten.' } } })
shortAssessmentSlides.push(...filterByTag('WHODAS20', shortAssessmentConfigs))
shortAssessmentSlides.push({ message : { translations: { en: '', de: 'Die folgenden Aussagen fragen Sie nach Ihren Gedanken und Gefühlen in einer Vielzahl von Situationen. Geben Sie für jede Aussage an, wie gut sie Sie beschreibt, indem Sie das entsprechende Kästchen ankreuzen. LESEN SIE JEDE AUFGABE SORGFÄLTIG DURCH BEVOR SIE ANTWORTEN. Antworten Sie so ehrlich wie möglich. Vielen Dank!' } } })
shortAssessmentSlides.push(...filterByTag('IRI', shortAssessmentConfigs))
shortAssessmentSlides.push({ message : { translations: { en: '', de: 'Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt.' } } })
shortAssessmentSlides.push(...filterByTag('POMS', shortAssessmentConfigs))
shortAssessmentSlides.push({ message : { translations: { en: '', de: '1) Bitte lesen Sie jede Frage sorgfältig durch, bevor Sie antworten. 2) Beantworten Sie bitte alle Fragen, auch dann, wenn Sie sich bei einer Frage unsicher sind. 3) Beantworten Sie die Fragen in der vorgegebenen Reihenfolge. 4) Beantworten Sie die Fragen so schnell wie möglich. Es sind die ersten Reaktionen auf die Fragen, die uns mehr interessieren als eine lange überlegte Antwort. 5) Beantworten Sie jede Frage ehrlich. Es gibt keine richtige oder falsche Antwort.' } } })
shortAssessmentSlides.push(...filterByTag('RMEQ', shortAssessmentConfigs))
shortAssessmentSlides.push({ message : { translations: { en: '', de: 'Bitte klicken Sie jeweils die Antwort an, die Ihre Gedanken, Gefühle und Verhaltensweisen während der letzten vier Wochen am besten beschreibt.' } } })
shortAssessmentSlides.push(...filterByTag('AES', shortAssessmentConfigs))

/**
 * Short assessment: Contains 61 questions.
 * Answered after 2 and 4 months.
 */
 const shortAssessment : SymptomCheckConfig = {
	id:				'rcc-repose-sc-short-assessment',
	meta:			{
						label: 				'REPOSE Short Assessment',
						defaultSchedule:	{
												startDate	: '2000-01-01',
												interval	: [60, 120]
											}
					},
	questions:		filterByTag('short-assessment'),
	presentation:	{
						slides: shortAssessmentSlides
					}
}


const fullAssessmentBlock1Slides : (string | InfoSlideConfig)[] = []
fullAssessmentBlock1Slides.push({ message : { translations: { en: '', de: 'Sie finden nachstehend eine Liste von Problemen und Beschwerden, die man manchmal hat. Bitte lesen Sie jede Frage einzeln sorgfältig durch und entscheiden Sie, wie stark Sie durch diese Beschwerden gestört oder bedrängt worden sind, und zwar während der vergangenen 7 Tage bis heute. Überlegen Sie bitte nicht erst, welche Antwort „den besten Eindruck“ machen könnte, sondern antworten Sie so, wie es für Sie persönlich zutrifft. Klicken Sie bitte bei jeder Frage nur das Kästchen mit der für Sie am besten zutreffenden Antwort an. Bitte beantworten Sie jede Frage' } } })
fullAssessmentBlock1Slides.push(...filterByTag('BSI53'))

/**
 * Full assessment: Contains all questions.
 * Answered after 0 and 6 months.
 * Block 1 contains BSI53.
 */
const fullAssessmentBlock1 : SymptomCheckConfig = {
	id:				'rcc-repose-sc-full-assessment-block-1',
	meta:			{
						label: 				'REPOSE Full Assessment Block 1',
						defaultSchedule:	{
												startDate	: '2000-01-01',
												interval	: [0, 180]
											}
					},
	questions:		filterByTag('block1'),
	presentation:	{
						slides: fullAssessmentBlock1Slides
					}
}


const fullAssessmentBlock2Slides : (string | InfoSlideConfig)[] = []
fullAssessmentBlock2Slides.push({ message : { translations: { en: '', de: 'Der folgende Fragebogen enthält eine Reihe von Feststellungen, mit denen man sich selbst beschreiben kann. Diese Feststellungen können genau auf Sie zutreffen, eher zutreffen, eher nicht oder gar nicht auf Sie zutreffen. Zur Beantwortung des Fragebogens wählen Sie den entsprechenden Kreis aus. Bitte beantworten Sie jede Feststellung, auch wenn Sie einmal nicht sicher sind, welche Antwort für Sie zutrifft. Klicken Sie dann diejenige Antwort an, die noch am ehesten auf Sie zutrifft.' } } })
fullAssessmentBlock2Slides.push(...filterByTag('BISBAS'))
fullAssessmentBlock2Slides.push({ message : { translations: { en: '', de: 'Entscheiden Sie für jedes dieser Lebensereignisse, ob es bei Ihnen in den letzten 12 Monaten eingetreten ist, und klicken Sie bitte Ja oder Nein an.' } } })
fullAssessmentBlock2Slides.push(...filterByTag('SRRS'))

/**
 * Full assessment: Contains all questions.
 * Answered after 0 and 6 months.
 * Block 2 contains BISBAS and SRRS.
 */
 const fullAssessmentBlock2 : SymptomCheckConfig = {
	id:				'rcc-repose-sc-full-assessment-block-2',
	meta:			{
						label: 				'REPOSE Full Assessment Block 2',
						defaultSchedule:	{
												startDate	: '2000-01-01',
												interval	: [0, 180]
											}
					},
	questions:		filterByTag('block2'),
	presentation:	{
						slides: fullAssessmentBlock2Slides
					}
}


const block3Questions : string[] = filterByTag('block3')

const beforeCts : (string | InfoSlideConfig)[] = [{ message : { translations: { en: '', de: 'Denken Sie an die schlimmste Angst, die Sie je erlebt haben oder die Sie sich vorstellen können, und geben Sie dieser die Zahl 100. Denken Sie nun an einen Zustand der absoluten Ruhe und geben Sie diesem die Zahl Null. Nun haben Sie eine Skala.' } } }]
beforeCts.push(...filterByTag('SUD'))

const cts : (string | InfoSlideConfig)[] = [{ message : { translations: { en: '', de: 'Die folgenden Fragen beziehen sich auf Ihre Kindheit und Jugend. Auch wenn die Fragen sehr persönlich sind, möchten wir Sie bitten, so ehrlich wie möglich zu antworten. Es gibt keine richtigen oder falschen Antworten. Bitte klicken Sie hinter jeder Frage die Antwort an, die auf Sie am besten zutrifft.' } } }]
cts.push(...filterByTag('CTS'))

const afterCts : (string | InfoSlideConfig)[] = []
afterCts.push({ message : { translations: { en: '', de: 'Dieser Fragebogen enthält eine Reihe von Wörtern, die unterschiedliche Gefühle und Empfindungen beschreiben. Lesen Sie jedes Wort und klicken dann in der Skala zu jedem Wort die Intensität an. Sie haben die Möglichkeit, zwischen fünf Abstufungen zu wählen. Geben Sie bitte an, wie Sie sich im Verlauf der letzten 12 Monate gefühlt haben.' } } })
afterCts.push(...filterByTag('PANAS'))
afterCts.push({ message : { translations: { en: '', de: 'Geben Sie bitte an, inwieweit die folgenden Aussagen Ihre Reaktionen in den letzten zwei Wochen (einschließlich heute) beschreiben. Wenn eines der Erlebnisse NICHT auf Sie in den letzten zwei Wochen zutraf: kein Problem. Geben Sie in dem Fall bitte an, wie Sie in den letzten zwei Wochen reagiert hätten, wenn Sie die Situation erlebt hätten. Bitte berücksichtigen Sie dabei nur den in der Frage beschriebenen Aspekt der Situation. Wenn die Aussage beispielweise besagt „Ich wollte neue Menschen kennenlernen“, bewerten Sie bitte wie sehr Sie in den letzten zwei Wochen neue Menschen kennen lernen wollten oder es gewollt hätten, wenn sich die Gelegenheit ergeben hätte. Berücksichtigen Sie dabei nicht, was die Situation Ihnen abverlangt hätte oder, ob es für Sie möglich gewesen wäre neue Menschen kennen zu lernen.' } } })
afterCts.push(...filterByTag('PVSS21'))
afterCts.push({ message : { translations: { en: '', de: 'Bitte beantworten Sie die folgende Frage im Hinblick darauf, wie viele Schwierigkeiten, die aufgrund von Gesundheitsproblemen entstehen können, Sie in den letzten 30 Tagen bei dieser nachfolgenden Aktivität hatten.' } } })
afterCts.push(...filterByTag('WHODAS20'))

/**
 * Full assessment: Contains all questions.
 * Answered after 0 months. (See below for the 6 months version)
 * Block 3 contains SUD, CTS, PANAS, PVSS21 and WHODAS20.
 */
 const fullAssessmentBlock3 : SymptomCheckConfig = {
	id:				'rcc-repose-sc-full-assessment-block-3',
	meta:			{
						label: 				'REPOSE Full Assessment Block 3',
						defaultSchedule:	{
												startDate	: '2000-01-01',
												interval	: [0]
											}
					},
	questions:		block3Questions,
	presentation:	{
						slides: beforeCts.concat(cts, afterCts)
					}
}

/**
 * Alternative version of block 3 for the 6 months assessment.
 * Doesn't contain CTS; only SUD, PANAS, PVSS21, WHODAS20.
 * = 5 questions less than the 0 months version.
 */
const fullAssessmentBlock3NoCTS : SymptomCheckConfig = {
	id:				'rcc-repose-sc-full-assessment-block-3-no-cts',
	meta:			{
						label: 				'REPOSE Full Assessment Block 3',
						defaultSchedule:	{
												startDate	: '2000-01-01',
												interval	: [180]
											}
					},
	questions:		block3Questions.filter(id => !filterByTag('CTS').includes(id)),
	presentation:	{
						slides: beforeCts.concat(afterCts)
					}
}


const fullAssessmentBlock4Slides : (string | InfoSlideConfig)[] = []
fullAssessmentBlock4Slides.push({ message : { translations: { en: '', de: 'Bitte schätzen Sie ein, inwieweit folgende Aussagen auf Sie zutreffen.' } } })
fullAssessmentBlock4Slides.push(...filterByTag('TAS20'))
fullAssessmentBlock4Slides.push({ message : { translations: { en: '', de: 'Die folgenden Aussagen fragen Sie nach Ihren Gedanken und Gefühlen in einer Vielzahl von Situationen. Geben Sie für jede Aussage an, wie gut sie Sie beschreibt, indem Sie das entsprechende Kästchen anklicken. LESEN SIE JEDE AUFGABE SORGFÄLTIG DURCH BEVOR SIE ANTWORTEN. Antworten Sie so ehrlich wie möglich. Vielen Dank!' } } })
fullAssessmentBlock4Slides.push(...filterByTag('IRI'))
fullAssessmentBlock4Slides.push({ message : { translations: { en: '', de: 'Geben Sie bitte an, wie sich Ihre Befindlichkeit in den letzten 24 Stunden am besten beschreiben lässt.' } } })
fullAssessmentBlock4Slides.push(...filterByTag('POMS'))
fullAssessmentBlock4Slides.push({ message : { translations: { en: '', de: '1) Bitte lesen Sie jede Frage sorgfältig durch, bevor Sie antworten. 2) Beantworten Sie bitte alle Fragen, auch dann, wenn Sie sich bei einer Frage unsicher sind. 3) Beantworten Sie die Fragen in der vorgegebenen Reihenfolge. 4) Beantworten Sie die Fragen so schnell wie möglich. Es sind die ersten Reaktionen auf die Fragen, die uns mehr interessieren als eine lange überlegte Antwort. 5) Beantworten Sie jede Frage ehrlich. Es gibt keine richtige oder falsche Antwort.' } } })
fullAssessmentBlock4Slides.push(...filterByTag('RMEQ'))
fullAssessmentBlock4Slides.push({ message : { translations: { en: '', de: 'Bitte klicken Sie jeweils die Antwort an, die Ihre Gedanken, Gefühle und Verhaltensweisen während der letzten vier Wochen am besten beschreibt.' } } })
fullAssessmentBlock4Slides.push(...filterByTag('AES'))

/**
 * Full assessment: Contains all questions.
 * Answered after 0 and 6 months.
 * Block 4 contains TAS20, IRI, POMS, RMEQ and AES.
 */
 const fullAssessmentBlock4 : SymptomCheckConfig = {
	id:				'rcc-repose-sc-full-assessment-block-4',
	meta:			{
						label: 				'REPOSE Full Assessment Block 4',
						defaultSchedule:	{
												startDate	: '2000-01-01',
												interval	: [0, 180]
											}
					},
	questions:		filterByTag('block4'),
	presentation:	{
						slides: fullAssessmentBlock4Slides
					}
}
