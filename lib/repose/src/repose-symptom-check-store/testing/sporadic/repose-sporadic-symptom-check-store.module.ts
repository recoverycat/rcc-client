import 	{	NgModule 								} 	from '@angular/core'
import	{
			QuestionnaireServiceModule,
			SymptomCheckMetaStoreServiceModule
		}												from '@rcc/features'
import	{	ReposeSporadicSymptomCheckStore			}	from '../../sporadic'
import	{	ReposeSporadicQuestionStore				}	from '../../sporadic/repose-sporadic-question-store.service'
import	{	ReposeTestSporadicSymptomCheckStore		}	from './repose-sporadic-symptom-check-store.service'

/**
 * This module provides the sporadic questions and test symptom check for the REPOSE study.
 */
@NgModule({
	imports: [
		SymptomCheckMetaStoreServiceModule.forChild([ReposeTestSporadicSymptomCheckStore]),
		QuestionnaireServiceModule.forChild([ReposeSporadicQuestionStore]),
	],
	providers: [
		ReposeTestSporadicSymptomCheckStore,
		{ provide: ReposeSporadicSymptomCheckStore, useExisting: ReposeTestSporadicSymptomCheckStore },
		ReposeSporadicQuestionStore
	]
})
export class ReposeTestSporadicSymptomCheckStoreModule{}
