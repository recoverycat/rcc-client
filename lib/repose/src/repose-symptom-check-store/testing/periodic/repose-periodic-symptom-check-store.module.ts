import 	{	NgModule 								} 	from '@angular/core'
import	{
			QuestionnaireServiceModule,
			SymptomCheckMetaStoreServiceModule
		}												from '@rcc/features'
import	{	ReposePeriodicSymptomCheckStore			}	from '../../periodic'
import	{	ReposePeriodicQuestionStore				}	from '../../periodic/repose-periodic-question-store.service'
import	{	ReposeTestPeriodicSymptomCheckStore		}	from './repose-periodic-symptom-check-store.service'

/**
 * This module provides the periodic questions and test symptom check for the REPOSE study.
 */
@NgModule({
	imports: [
		SymptomCheckMetaStoreServiceModule.forChild([ReposeTestPeriodicSymptomCheckStore]),
		QuestionnaireServiceModule.forChild([ReposePeriodicQuestionStore]),
	],
	providers: [
		ReposeTestPeriodicSymptomCheckStore,
		{ provide: ReposePeriodicSymptomCheckStore, useExisting: ReposeTestPeriodicSymptomCheckStore },
		ReposePeriodicQuestionStore,
	]
})
export class ReposeTestPeriodicSymptomCheckStoreModule{}
