import 	{	NgModule 								} 	from '@angular/core'
import	{
			QuestionnaireServiceModule,
			SymptomCheckMetaStoreServiceModule
		}												from '@rcc/features'
import	{	ReposePeriodicQuestionStore				}	from './repose-periodic-question-store.service'
import	{	ReposePeriodicSymptomCheckStore			}	from './repose-periodic-symptom-check-store.service'

/**
 * This module provides the periodic questions and symptom check for the REPOSE study.
 */
@NgModule({
	imports: [
		SymptomCheckMetaStoreServiceModule.forChild([ReposePeriodicSymptomCheckStore]),
		QuestionnaireServiceModule.forChild([ReposePeriodicQuestionStore]),
	],
	providers: [
		ReposePeriodicSymptomCheckStore,
		ReposePeriodicQuestionStore,
	]
})
export class ReposePeriodicSymptomCheckStoreModule{}
