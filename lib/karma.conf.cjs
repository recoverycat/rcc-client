// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

module.exports = function (config) {
	config.set({
		basePath: '',
		frameworks: ['jasmine', '@angular-devkit/build-angular'],
		plugins: [
			require('karma-jasmine'),
			require('karma-chrome-launcher'),
			require('karma-jasmine-html-reporter'),
			require('karma-junit-reporter'),
			require('@angular-devkit/build-angular/plugins/karma'),
			require('karma-spec-reporter'),
			require('karma-coverage'),
		],
		client: {
			jasmine: {
			// you can add configuration options for Jasmine here
			// the possible options are listed at https://jasmine.github.io/api/edge/Configuration.html
			// for example, you can disable the random execution with `random: false`
			// or set a specific seed with `seed: 4321`
			},
			clearContext: false // leave Jasmine Spec Runner output visible in browser
		},
		jasmineHtmlReporter: {
			suppressAll: true // removes the duplicated traces
		},
		// https://github.com/karma-runner/karma-junit-reporter
		junitReporter: {
			outputDir: '../', // results will be saved as $outputDir/$browserName.xml
			outputFile: 'unit-tests-results.xml', // if included, results will be saved as $outputDir/$browserName/$outputFile
			suite: '', // suite will become the package name attribute in xml testsuite element
			useBrowserName: false, // add browser name to report and classes names
			nameFormatter: undefined, // function (browser, result) to customize the name attribute in xml testcase element
			classNameFormatter: undefined, // function (browser, result) to customize the classname attribute in xml testcase element
			properties: {}, // key value pair of properties to add to the <properties> section of the report
			xmlVersion: null // use '1' if reporting to be per SonarQube 6.2 XML format
		},
		// config for reporter:
		// https://github.com/karma-runner/karma-coverage/blob/master/docs/configuration.md#check
		coverageReporter: {
			dir: require('path').join(__dirname, '../coverage'),
			subdir: '.',
			reporters: [
				{ type: 'html' },
				{ type: 'text-summary' },
				{ type: 'cobertura' }
			],
			check: {
				emitWarning: false,
				global: {
					statements: 27.06,
					branches: 16.63,
					functions: 18.30,
					lines: 26.89
				}
			}
		},
		reporters: ['spec', 'junit'],
		specReporter: {
			suppressSkipped: true,
		},
		port: 9876,
		colors: true,
		logLevel: config.LOG_INFO,
		autoWatch: false,
		singleRun: true,
		restartOnFileChange: true,
		browsers: ['ChromeHeadlessNoSandbox'],
		customLaunchers: {
			ChromeHeadlessNoSandbox: {
				base: 'ChromeHeadless',
				flags: ['--no-sandbox']
			}
		},
	});
};
