import  {
            Component,
            forwardRef
        }                                       from '@angular/core'

import  {
            NG_VALUE_ACCESSOR,
            ControlValueAccessor
        }                                       from '@angular/forms'

import  {
            RccDaysInWeekSelectorBaseComponent
        }                                       from '@rcc/themes/theming-mechanics'

/**
 * This is just a placeholder. It's only purpose is to provide a
 * component with an interface to avoid errors and have type hinting
 * while writing templates. This is supposed to be overwritten by
 * Bengal Themes RccDaysInWeekSelectorComponent.
 */
@Component({
    selector: 'rcc-days-in-week-selector',
    template: '',
    standalone: true,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => RccDaysInWeekSelectorComponent),
            multi: true
        }
    ],
})
export class RccDaysInWeekSelectorComponent extends RccDaysInWeekSelectorBaseComponent implements ControlValueAccessor {
    
    public writeValue(): void {
        throw new Error('Method not implemented.')
    }
    public registerOnChange(): void {
        throw new Error('Method not implemented.')
    }
    public registerOnTouched(): void {
        throw new Error('Method not implemented.')
    }
    public setDisabledState(): void {
        throw new Error('Method not implemented.')
    }
}
