import	{	Component						}	from '@angular/core'
import	{	CommonModule					}	from '@angular/common'
import	{	RccButtonBaseComponent			}	from '@rcc/themes'
import	{	TranslationsModule				}	from '@rcc/common/translations'
import	{	UiComponentsModule				}	from '@rcc/common/ui-components'

@Component({
    selector: 'rcc-button',
    templateUrl: './button.component.html',
    styleUrls: ['./button.component.css'],
    imports: [
        CommonModule,
        UiComponentsModule,
        TranslationsModule,
    ]
})
export class RccButtonComponent extends RccButtonBaseComponent {
}
