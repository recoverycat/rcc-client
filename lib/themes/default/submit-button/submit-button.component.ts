import	{
			Component,
			EventEmitter,
			HostBinding,
			Output,
			Input
		}										from '@angular/core'

import	{	CommonModule					}	from '@angular/common'
import	{
			RccButtonBaseComponent,
		}										from '@rcc/themes'
import	{	TranslationsModule				}	from '@rcc/common/translations'
import	{	UiComponentsModule				}	from '@rcc/common/ui-components'

@Component({
    selector: 'rcc-submit-button',
    templateUrl: './submit-button.component.html',
    styleUrls: ['./submit-button.component.css'],
    imports: [
        CommonModule,
        UiComponentsModule,
        TranslationsModule,
    ]
})
export class RccSubmitButtonComponent extends RccButtonBaseComponent {

	@Output()
	public clickEvent: EventEmitter<void> = new EventEmitter<void>()

	@HostBinding('class.disabled')
	public get isDisabled(): boolean { return true}

	@Input() @HostBinding('class.disabled')
	public disabled: boolean = false

}
