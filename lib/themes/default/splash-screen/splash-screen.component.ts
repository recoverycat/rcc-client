import { CommonModule } from '@angular/common'
import { ChangeDetectorRef, Component } from '@angular/core'
import { RccBuildInfoService } from '@rcc/common'
import { RccSplashScreenBaseComponent } from '../../theming-mechanics/components/splash-screen/splash-screen-base.component'
import { RccSplashScreenService } from '@rcc/themes/theming-mechanics/components/splash-screen'

@Component({
    selector: 'rcc-splash-screen',
    templateUrl: './splash-screen.component.html',
    styleUrls: ['./splash-screen.component.scss'],
    imports: [CommonModule],
    providers: [
        Document,
    ]
})

export class RccSplashScreenComponent extends RccSplashScreenBaseComponent {
	public constructor(
		rccBuildInfoService: RccBuildInfoService,
		changeDetectorRef: ChangeDetectorRef,
		document: Document,
		splashScreenService: RccSplashScreenService
	) {
		super(rccBuildInfoService, changeDetectorRef, document, splashScreenService)
	}

}
