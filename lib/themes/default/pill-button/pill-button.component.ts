import	{
			Component,
		}										from '@angular/core'

import	{	CommonModule					}	from '@angular/common'

import	{
			RccPillButtonBaseComponent
		}										from '@rcc/themes'

import	{	TranslationsModule				}	from '@rcc/common/translations'
import	{
			UiComponentsModule,
		}										from '@rcc/common/ui-components'

@Component({
    selector: 'button [pill-button], a[pill-button]',
    template: '<ng-content></ng-content>',
    styleUrls: ['./pill-button.component.css'],
    imports: [
        CommonModule,
        UiComponentsModule,
        TranslationsModule,
    ]
})
export class RccPillButtonComponent extends RccPillButtonBaseComponent {}
