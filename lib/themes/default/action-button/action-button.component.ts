import	{
			Component,
			ViewEncapsulation,
		}										from '@angular/core'


import	{
			RccActionButtonBaseComponent,
		}										from '@rcc/themes'

import	{
			DefaultThemeCommonModule
		}										from '../default-theme-common.module'

@Component({
    selector: 'rcc-action-button',
    templateUrl: './action-button.component.html',
    styleUrls: ['./action-button.component.css'],
    encapsulation: ViewEncapsulation.None,
    imports: [
        DefaultThemeCommonModule
    ]
})
export class RccActionButtonComponent extends RccActionButtonBaseComponent {}
