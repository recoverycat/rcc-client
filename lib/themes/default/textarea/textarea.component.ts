import { Component, ElementRef, OnInit, ViewChild, forwardRef } from '@angular/core'
import { NG_VALUE_ACCESSOR, ReactiveFormsModule } from '@angular/forms'
import { RccTextareaBaseComponent } from '@rcc/themes/theming-mechanics/components/textarea/textarea-base.component'

@Component({
    selector: 'rcc-textarea',
    templateUrl: './textarea.component.html',
    imports: [ReactiveFormsModule],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => RccTextareaComponent),
            multi: true
        }
    ]
})
export class RccTextareaComponent extends RccTextareaBaseComponent implements OnInit {
	public ngOnInit(): void {
		this.formControl.valueChanges.subscribe(this.onChange)
	}

	@ViewChild('textInput')
	private textInput: ElementRef<HTMLTextAreaElement>

	protected focusOnInput(): void {
		this.textInput.nativeElement.focus()
	}
}
