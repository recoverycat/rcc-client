import { Component, ViewChild } from '@angular/core'
import { RccPullDownSelectBaseComponent } from '../../theming-mechanics'
import { DefaultThemeCommonModule } from '../default-theme-common.module'

@Component({
    templateUrl: './pull-down-select.component.html',
    selector: 'rcc-pull-down-select',
    imports: [DefaultThemeCommonModule]
})
export class RccPullDownSelectComponent<T> extends RccPullDownSelectBaseComponent<T> {
	@ViewChild('#ionSelect')
	private ionSelect: HTMLIonSelectElement

	protected onInputChange(): void {
		this.onChange(this.ionSelect.value as T)
		this.onTouched()
	}
}
