import	{
			Component,
			forwardRef,
		}									from '@angular/core'
import	{	RccTextInputBaseComponent	}	from '../../theming-mechanics'
import	{	DefaultThemeCommonModule	}	from '../default-theme-common.module'
import	{	NG_VALUE_ACCESSOR			}	from '@angular/forms'

@Component({
    selector: 'rcc-text-input',
    templateUrl: './text-input.component.html',
    imports: [DefaultThemeCommonModule],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => RccTextInputComponent),
            multi: true
        }
    ]
})
export class RccTextInputComponent extends RccTextInputBaseComponent {
	protected onInput(event: Event): void {
		const input: HTMLInputElement = event.target as HTMLInputElement
		this.onChange(input.value)
	}
}
