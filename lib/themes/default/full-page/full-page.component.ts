import	{
			Component,
			ViewEncapsulation,
			Output,
			EventEmitter,
			ElementRef
		}								from '@angular/core'
import	{
			RccFullPageBaseComponent,
		}								from '../../theming-mechanics/components/full-page/full-page-base.component'
import	{
			Router,
			RouterModule,
		}								from '@angular/router'
import	{	DefaultThemeCommonModule }	from '../default-theme-common.module'
import	{
			MainMenuModule,
			MainMenuService,
		}								from '@rcc/common/main-menu'

@Component({
    selector: 'rcc-full-page',
    templateUrl: './full-page.component.html',
    styleUrls: ['./full-page.component.css'],
    encapsulation: ViewEncapsulation.None,
    imports: [
        DefaultThemeCommonModule,
        RouterModule,
        MainMenuModule
    ]
})
export class RccFullPageComponent extends RccFullPageBaseComponent {

	@Output()
	public closeEvent: EventEmitter<unknown> = new EventEmitter()

	public constructor(
		protected router: Router,
		private readonly mainMenuService: MainMenuService,
		public readonly elementRef			: ElementRef<HTMLElement>

	){
		super()
	}

	public onMenuClick(): void {
		this.mainMenuService.setIsMenuOpen(true)
	}

	public get showHomeButton() : boolean { return this.router.url !== '/'}

}
