import { Component } from '@angular/core'
import { RccHomePageGreetingBaseComponent } from '../..'
import { DefaultThemeCommonModule } from '../default-theme-common.module'

@Component({
    selector: 'rcc-home-page-greeting',
    templateUrl: './home-page-greeting.component.html',
    imports: [
        DefaultThemeCommonModule
    ]
})
export class RccHomePageGreetingComponent extends RccHomePageGreetingBaseComponent {}
