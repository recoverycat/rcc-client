import { Component } from '@angular/core'
import { DefaultThemeCommonModule } from '../../default-theme-common.module'
import { RccCodeVerifedModalBaseComponent } from '../../../theming-mechanics'
import { RccActionButtonComponent } from '../../action-button/action-button.component'

@Component({
    templateUrl: './code-verified-modal.component.html',
    selector: 'rcc-code-verified-modal',
    imports: [DefaultThemeCommonModule, RccActionButtonComponent]
})
export class RccCodeVerifiedModalComponent extends RccCodeVerifedModalBaseComponent {
}
