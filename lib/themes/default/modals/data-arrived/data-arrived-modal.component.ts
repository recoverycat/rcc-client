import { Component } from '@angular/core'
import { RccDataArrivedModalBaseComponent } from '@rcc/themes/theming-mechanics/components/modals/data-arrived/data-arrived-modal-base.component'
import { DefaultThemeCommonModule } from '../../default-theme-common.module'

@Component({
    templateUrl: './data-arrived-modal.component.html',
    selector: 'rcc-data-arrived-modal',
    imports: [DefaultThemeCommonModule]
})
export class RccDataArrivedModalComponent extends RccDataArrivedModalBaseComponent {}
