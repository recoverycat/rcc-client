import	{	Component							}		from '@angular/core'
import	{	CommonModule						}		from '@angular/common'
import	{
			ItemSelectModalComponent,
		}												from '@rcc/common/items/select'
import	{
			Action
		}												from '@rcc/common/actions'

import	{
			TranslationsModule
		}												from '@rcc/common/translations'
import	{
			RccColorCategoryDirective,
			RccFabComponent
		}												from '@rcc/common/ui-components'
import	{
			RccReducedFullPageComponent
		}												from '../../reduced-full-page/reduced-full-page.component'
import	{
			RccItemListComponent
		}												from '../../item-list/item-list.component'
import	{
			RccListItemTagComponent
		}												from '../../item-tag'


@Component({
    templateUrl: './item-select-modal.component.html',
    styleUrls: ['./item-select-modal.component.scss'],
    imports: [
        RccReducedFullPageComponent,
        RccColorCategoryDirective,
        RccListItemTagComponent,
        RccItemListComponent,
        TranslationsModule,
        RccFabComponent,
        CommonModule
    ]
})
export class BengalItemSelectModalComponent extends ItemSelectModalComponent {

	public cancelAction	: Action = {
		label:	'CLOSE',
		icon:	'close',
		handler: () => this.cancel()
	}
}
