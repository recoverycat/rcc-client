import 	{ 	Component 							} from '@angular/core'
import 	{ 	DefaultThemeCommonModule 			} from '@rcc/themes/default/default-theme-common.module'
import 	{ 	ModalLayoutComponent 				} from '../modal-layout/modal-layout.component'
import	{	RccCodeVerifedModalBaseComponent	} from '@rcc/themes/theming-mechanics'

@Component({
    templateUrl: './code-verified-modal.component.html',
    selector: 'rcc-code-verified-modal',
    imports: [ModalLayoutComponent, DefaultThemeCommonModule]
})
export class RccCodeVerifiedModalComponent extends RccCodeVerifedModalBaseComponent {}
