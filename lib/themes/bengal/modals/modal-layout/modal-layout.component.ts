import	{
			Component,
			EventEmitter,
			Input,
			Output,
		} 										from '@angular/core'
import	{	Action							}	from '@rcc/common/actions'
import	{	CurrentDateMessageComponent		}	from '@rcc/common/current-date-message'
import	{	RccActionButtonComponent		}	from '@rcc/themes/bengal'
import	{	DefaultThemeCommonModule		}	from '@rcc/themes/default/default-theme-common.module'
import	{	RccButtonComponent				}	from '../..'

/**
 * Common layout component for modals as part of the data
 * transfer process
 *
 * To set the card background, set the css property `--card-background`
 * from the parent component's css
 */
@Component({
    selector: 'rcc-modal-layout',
    templateUrl: './modal-layout.component.html',
    styleUrls: ['./modal-layout.component.scss'],
    imports: [
        CurrentDateMessageComponent,
        RccButtonComponent,
        DefaultThemeCommonModule,
        RccActionButtonComponent,
    ]
})
export class ModalLayoutComponent {
	@Input()
	public contextText: string

	@Input()
	public mainText: string

	@Output()
	public cancelClick: EventEmitter<void> = new EventEmitter<void>()

	@Input()
	public action: Action

	@Input()
	public showButton: boolean
}
