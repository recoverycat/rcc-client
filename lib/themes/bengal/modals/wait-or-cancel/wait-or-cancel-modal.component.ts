import	{	Component				}	from '@angular/core'
import	{	Action					}	from '@rcc/common/actions'
import	{
			DefaultThemeCommonModule
		}								from '@rcc/themes/default/default-theme-common.module'
import	{
			RccWaitOrCancelModalBaseComponent
		}								from '../../../theming-mechanics/components'
import	{	ModalLayoutComponent	}	from '../modal-layout/modal-layout.component'
import	{	IconName				}	from '@rcc/common/ui-components/icons/icon-names'

@Component({
    templateUrl: './wait-or-cancel-modal.component.html',
    styleUrls: ['./wait-or-cancel-modal.component.scss'],
    imports: [
        DefaultThemeCommonModule,
        ModalLayoutComponent,
    ],
    selector: 'rcc-wait-or-cancel-modal'
})
export class RccWaitOrCancelModalComponent<T> extends RccWaitOrCancelModalBaseComponent<T> {
	private _action: Action = {
		path: '',
		label: '',
		icon: '',
	}
	protected set action(value: Action) {
		this._action = value
	}
	protected get action(): Action {
		return { ...this._action, icon: this.icon || this._action.icon }
	}

	public icon		: IconName
}
