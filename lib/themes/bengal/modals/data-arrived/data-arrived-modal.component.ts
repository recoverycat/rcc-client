import 	{ 	Component 							} from '@angular/core'
import 	{ 	RccModalController 					} from '@rcc/common'
import	{	UserCanceledError					} from '@rcc/core'
import 	{ 	DefaultThemeCommonModule 			} from '@rcc/themes/default/default-theme-common.module'
import 	{ 	RccDataArrivedModalBaseComponent 	} from '@rcc/themes/theming-mechanics/components/modals/data-arrived/data-arrived-modal-base.component'
import 	{ 	ModalLayoutComponent 				} from '../modal-layout/modal-layout.component'

@Component({
    templateUrl: './data-arrived-modal.component.html',
    selector: 'rcc-data-arrived-modal',
    imports: [ModalLayoutComponent, DefaultThemeCommonModule]
})
export class RccDataArrivedModalComponent extends RccDataArrivedModalBaseComponent {
	public constructor(private readonly modalController: RccModalController) {
		super()
	}

	protected cancel(): void {
		this.modalController.dismiss(new UserCanceledError('RccDataArrivedModalComponent'))
	}
}
