import { AfterViewInit, Component, ElementRef, forwardRef, OnDestroy, ViewChild } from '@angular/core'
import { NG_VALUE_ACCESSOR } from '@angular/forms'
import { RccTranslationService } from '@rcc/common/translations'
import { uniqueId } from '@rcc/core'
import { DefaultThemeCommonModule } from '@rcc/themes/default/default-theme-common.module'
import { fromEvent, Subject, takeUntil } from 'rxjs'
import { RccTimeSelectBaseComponent } from '../..'

const hoursOptions:		number[] = [...Array(24).keys()]
const minutesOptions:	number[] = [...Array(60).keys()]

type TimeType = 'hours' | 'minutes'

@Component({
    selector: 'rcc-time-select',
    templateUrl: './time-select.component.html',
    styleUrls: ['./time-select.component.scss'],
    imports: [DefaultThemeCommonModule],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => RccTimeSelectComponent),
            multi: true,
        },
    ]
})
export class RccTimeSelectComponent extends RccTimeSelectBaseComponent implements AfterViewInit, OnDestroy {
	public constructor(private readonly translationService: RccTranslationService) {
		super()
	}

	private destroy$: Subject<void> = new Subject()

	protected get ariaLabel(): string {
		return `${this.labelText} ${this.translationService.translate('THEME.CURRENT_VALUE')}: ${this.value}}`
	}

	public ngAfterViewInit(): void {
		this.handleScrollEvents(this.hourContainer.nativeElement, () => this.updateHourFromTimePosition())
		this.handleScrollEvents(this.minuteContainer.nativeElement, () => this.updateMinutesFromTimePosition())

		this.scrollToHours('auto')
		this.scrollToMinutes('auto')
	}

	private handleScrollEvents(container: HTMLElement, callback: () => void): void {
		fromEvent(container, 'scroll').pipe(
			takeUntil(this.destroy$),
		).subscribe(callback)
	}

	protected hoursOptions:		number[] = hoursOptions
	protected minutesOptions:	number[] = minutesOptions

	@ViewChild('hourContainer')
	private hourContainer: ElementRef<HTMLDivElement>

	@ViewChild('minuteContainer')
	private minuteContainer: ElementRef<HTMLDivElement>

	private setValue(hours: number, minutes: number): void {
		const hourString	: string = hours.toString().padStart(2, '0')
		const minutesString	: string = minutes.toString().padStart(2, '0')

		this.value = `${hourString}:${minutesString}`
		this.onChange(this.value)
	}

	protected set hours(value: number) {
		// TODO: #317 This is a quick temporary fix for Android Firefox
		this.setValue(Math.min(23, value), this.minutes)
	}

	protected get hours(): number {
		return parseInt(this.value.split(':')[0], 10)
	}

	protected set minutes(value: number) {
		// TODO: #317 This is a quick temporary fix for Android Firefox
		this.setValue(this.hours, Math.min(59, value))
	}

	protected get minutes(): number {
		return parseInt(this.value.split(':')[1], 10)
	}

	public ngOnDestroy(): void {
		this.destroy$.next()
		this.destroy$.complete()
	}

	public override writeValue(value: string): void {
		this.value = value

		if (this.hourContainer != null)
			this.scrollToHours('auto')
		if (this.minuteContainer != null)
			this.scrollToHours('auto')
	}

	private uniqueIdPart: string = uniqueId('time-select')

	protected hourId(index: number): string {
		return `${this.uniqueIdPart}-hour-${index}`
	}

	protected minuteId(index: number): string {
		return `${this.uniqueIdPart}-minute-${index}`
	}

	protected handleKeydown(event: KeyboardEvent, type: TimeType): void {
		if (['ArrowDown', 'ArrowUp', 'Home', 'End'].indexOf(event.key) !== -1) {
			event.preventDefault()
			this.handleMovementKey(event.key as 'ArrowDown' | 'ArrowUp' | 'Home' | 'End', type)
		}
	}

	private handleMovementKey(key: 'ArrowDown' | 'ArrowUp' | 'Home' | 'End', type: TimeType): void {
		let newValue: number = 0
		const currentValue: number = type === 'hours' ? this.hours : this.minutes
		const maxValue: number = type === 'hours' ? 23 : 59
		if (key === 'ArrowDown')
			newValue = currentValue < maxValue ? currentValue + 1 : maxValue

		if (key === 'ArrowUp')
			newValue = currentValue > 0 ? currentValue - 1 : 0

		if (key === 'Home')
			newValue = 0

		if (key === 'End')
			newValue = maxValue

		this.updateTime(newValue, type)
	}

	private scrollToHours(behavior: ScrollBehavior): void {
		const currentValue: number = this.hours
		const div: HTMLDivElement = this.hourContainer.nativeElement

		this.scrollToTimeElement(div, currentValue, behavior)
	}

	private updateHourFromTimePosition(): void {
		const div: HTMLDivElement = this.hourContainer.nativeElement

		this.updateTimeFromScrollPosition(div, (value) => {
			this.hours = value
		})
	}

	private updateMinutesFromTimePosition(): void {
		const div: HTMLDivElement = this.minuteContainer.nativeElement

		this.updateTimeFromScrollPosition(div, (value) => {
			this.minutes = value
		})
	}

	private updateTimeFromScrollPosition(container: HTMLDivElement, callback: (value: number) => void): void {
		const scrollTop: number = container.scrollTop

		const height: number = container.clientHeight / 3

		const index: number = Math.floor((scrollTop + height / 2) / height)
		callback(index)
	}

	private scrollToMinutes(behavior: ScrollBehavior): void {
		const currentValue: number = this.minutes
		const div: HTMLDivElement = this.minuteContainer.nativeElement

		this.scrollToTimeElement(div, currentValue, behavior)
	}

	private scrollToTimeElement(container: HTMLDivElement, value: number, behavior: ScrollBehavior): void {
		const height: number = container.clientHeight / 3

		const scrollTo: number = height * value

		container.scroll({ top: scrollTo, behavior })
	}

	protected onClick(index: number, type: TimeType): void {
		if (this.disabled)
			return
		this.updateTime(index, type)
	}

	private updateTime(value: number, type: TimeType): void {
		if (type === 'hours') {
			this.hours = value
			this.scrollToHours('smooth')
		}
		else {
			this.minutes = value
			this.scrollToMinutes('smooth')
		}
	}
}
