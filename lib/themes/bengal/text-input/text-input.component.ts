import { Component, ElementRef, ViewChild, forwardRef } from '@angular/core'
import { RccTextInputBaseComponent } from '../..'
import { NG_VALUE_ACCESSOR } from '@angular/forms'

@Component({
	templateUrl	: './text-input.component.html',
	styleUrls	: ['./text-input.component.scss'],
	selector	: 'rcc-text-input',
	standalone	: true,
	providers:	[
			{
				provide: NG_VALUE_ACCESSOR,
				useExisting: forwardRef(() => RccTextInputComponent),
				multi: true
			}
		]
})
export class RccTextInputComponent extends RccTextInputBaseComponent {
	private _input: ElementRef<HTMLInputElement>

	@ViewChild('inputControl')
	protected set input(newInput: ElementRef<HTMLInputElement>) {
		this._input = newInput
		if (this.value)
			this.setInputValue(this.value)
	}

	protected get input(): ElementRef<HTMLInputElement> {
		return this._input
	}

	public override writeValue(value: string): void {
		this.setInputValue(value)
	}

	private setInputValue(value: string): void {
		if (this.input)
			this.input.nativeElement.value = value
		this.value = value
	}

	protected handleInput(): void {
		this.value = this.input.nativeElement.value
		this.onChange(this.value)
	}

}
