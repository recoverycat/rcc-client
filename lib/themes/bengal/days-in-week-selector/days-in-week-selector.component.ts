import  {
			ChangeDetectionStrategy,
			Component,
			ElementRef,
			forwardRef,
			OnChanges,
			OnDestroy,
			OnInit,
			SimpleChanges
		}                                       from '@angular/core'

import  {
			ControlValueAccessor,
			FormControl,
			FormGroup,
			NG_VALUE_ACCESSOR,
			ReactiveFormsModule
		}
												from '@angular/forms'
import  {
			uniqueId
		}                                       from '@rcc/core'

import  {
			DefaultThemeCommonModule
		}                                       from '@rcc/themes/default/default-theme-common.module'


import  {
			map,
			SubscriptionLike,
			tap
		}                                       from 'rxjs'

import  {
			RccDaysInWeekSelectorBaseComponent
		}                                       from '@rcc/themes/theming-mechanics'

interface DaysInWeekSelectorForm {
	[key: string] : FormControl<boolean>
}

/**
 * Custom form element that can be used to edit the days in week part of a
 * {@link ScheduleConfig}. You can use it either via the {@link ControlValueAccessor}
 * or set the daysInWeek input and listen to emitted events when the selected
 * days change.
 * You can use {@link ColorCategoryDirective} to set the color for the radio
 * button selection indicator and focus-visible ring.
 * @example via ControlValueAccessor:
 * ```typescript
 * // component
 * public form: FormGroup<{ scheduleDaysInWeek: FormControl<number[]> }> = new FormGroup({
 *      scheduleDaysInWeek : new FormControl<number[]>([])
 * })
 * ```
 * ```html
 * <!-- template -->
 * <form [formGroup]="form">
 *      <rcc-days-in-week-selector
 *          formControlName="scheduleDaysInWeek"
 *      ></rcc-days-in-week-selector>
 * </form>
 * ```
 * @example via Input and EventEmitter:
 * ```typescript
 * // component
 * public scheduleDaysInWeek: number[] = [1,3,6];
 *
 * public updateScheduleDaysInWeek(daysInWeek: number[]): void {
 *      this.scheduleDaysInWeek = daysInWeek;
 * }
 * ```
 * ```html
 * <!-- template -->
 * <rcc-days-in-week-selector
 *      [daysInWeek]="scheduleDaysInWeek"
 *      (daysInWeekChange)="updateScheduleDaysInWeek($event)"
 * ></rcc-days-in-week-selector>
 * ```
 * @example setting color with ColorCategoryDirective:
 * ```html
 * <!-- template -->
 * <rcc-days-in-week-selector
 *      colorCategory="secondary"
 *      [daysinWeek]="scheduleDaysInWeek"
 *      (daysInWeekChange)="updateScheduleDaysInWeek($event)"
 * ></rcc-days-in-week-selector>
 * ```
 */
@Component({
    selector: 'rcc-days-in-week-selector',
    templateUrl: './days-in-week-selector.component.html',
    styleUrls: ['./days-in-week-selector.component.scss'],
    imports: [ReactiveFormsModule, DefaultThemeCommonModule],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => RccDaysInWeekSelectorComponent),
            multi: true
        }
    ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class RccDaysInWeekSelectorComponent
	extends RccDaysInWeekSelectorBaseComponent
	implements ControlValueAccessor, OnInit, OnChanges, OnDestroy {

	private onChange                : (daysInWeek: number[]) => void

	private onTouched               : () => void

	private days                    : string[] = ['su', 'mo', 'tu', 'we', 'th', 'fr', 'sa']

	private subscription            : SubscriptionLike

	private uniqueIdPart            : string = uniqueId('schedule-editor-item')

	public daysInWeekSelectorForm   : FormGroup<DaysInWeekSelectorForm>
									= new FormGroup<DaysInWeekSelectorForm>({
										'mo' : new FormControl<boolean>(false),
										'tu' : new FormControl<boolean>(false),
										'we' : new FormControl<boolean>(false),
										'th' : new FormControl<boolean>(false),
										'fr' : new FormControl<boolean>(false),
										'sa' : new FormControl<boolean>(false),
										'su' : new FormControl<boolean>(false)
									})

	public get formControlKeys(): string[] {
		return Object.keys(this.daysInWeekSelectorForm.controls)
	}

	public constructor(private readonly elementRef: ElementRef) {
		super();
		(this.elementRef.nativeElement as HTMLElement).role = 'group'
		}

	public ngOnInit(): void {
		this.subscription = this.daysInWeekSelectorForm.valueChanges
			.pipe(

				map((daysInWeek: { [key:string] : boolean }) =>
					Object
						.entries(daysInWeek)
						.filter(([, value]) => value)
						.map(([key]) => this.days.indexOf(key))
				),

				tap(() => {
					if(typeof this.onTouched === 'function')
						this.onTouched()

				})
			)
			.subscribe(daysInWeek => {
				this.daysInWeekChange.emit(daysInWeek)

				if(typeof this.onChange === 'function')
					this.onChange(daysInWeek)

			})
	}

	public ngOnChanges(changes: SimpleChanges): void {
		const diw   : number[] | undefined | null
					= changes['daysInWeek']?.currentValue as number[] | undefined | null

		if(diw && Array.isArray(diw))
			this.writeValue(diw)


	}

	public ngOnDestroy(): void {
		this.subscription.unsubscribe()
	}

	public id(key: string): string {
		return `${this.uniqueIdPart}-${key}`
	}

	public writeValue(daysInWeek: number[]): void {
		this.days
			.forEach((day: string, index: number) => {
				this.daysInWeekSelectorForm
					.get(day)
					.setValue(daysInWeek.includes(index), { onlySelf: true, emitEvent: false })
			})
	}

	public registerOnChange(fn: (daysInWeek: number[]) => void): void {
		this.onChange = fn
	}

	public registerOnTouched(fn: () => void): void {
		this.onTouched = fn
	}

	public setDisabledState(isDisabled: boolean): void {
		if(isDisabled)
			this.daysInWeekSelectorForm.disable()
		else
			this.daysInWeekSelectorForm.enable()

	}

}
