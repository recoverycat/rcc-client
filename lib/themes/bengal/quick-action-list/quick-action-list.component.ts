import	{
			Component,
			HostBinding
		}								from '@angular/core'
import	{	CommonModule		}		from '@angular/common'
import	{	RccIconComponent	}		from '@rcc/common/src/ui-components/icons'
import	{
			RccQuickActionListBaseComponent,
		}								from '@rcc/themes/theming-mechanics'

import	{
			TranslationsModule
		}								from '@rcc/common/translations'

@Component({
    selector: 'rcc-quick-action-list',
    templateUrl: './quick-action-list.component.html',
    styleUrls: ['quick-action-list.component.css'],
    imports: [
        RccIconComponent,
        CommonModule,
        TranslationsModule,
    ]
})

export class RccQuickActionListComponent extends RccQuickActionListBaseComponent {

	@HostBinding('style')
	protected get style() : Record<string, string> {
		return {
			'--foreground-hover':			this.colorVariants.mainHighlight,
			'--foreground-text-hover':		this.colorVariants.mainHighlightContrast,
			'--foreground-text-disabled':	this.colorVariants.suppHighlightContrast

		}
	}

}
