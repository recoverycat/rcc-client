
import	{	Component							}	from	'@angular/core'
import	{	CommonModule						}	from	'@angular/common'
import	{	TranslationsModule					} 	from '@rcc/common/translations'
import	{	RccItemListBaseComponent			}	from	'../..'
import	{	RccListItemTagComponent				}	from '../item-tag'

@Component({
    selector: 'rcc-item-list',
    templateUrl: './item-list.component.html',
    styleUrls: ['./item-list.component.scss'],
    imports: [
        RccListItemTagComponent,
        CommonModule,
        TranslationsModule,
    ]
})
export class RccItemListComponent extends RccItemListBaseComponent {

}
