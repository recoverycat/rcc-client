import { ComponentFixture, TestBed } from '@angular/core/testing'
import { RccRadioSelectComponent } from './radio-select.component'
import { DOCUMENT } from '@angular/common'

describe('RccRadioSelectComponent', () => {
	let component: RccRadioSelectComponent<string> = undefined!
	let fixture: ComponentFixture<RccRadioSelectComponent<string>> = undefined!

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [RccRadioSelectComponent],
			providers: [
				{ provide: DOCUMENT, useValue: document },
			]
		}).compileComponents()

		fixture = TestBed.createComponent(RccRadioSelectComponent<string>)
		component = fixture.componentInstance

		// Set the required groupLabel
		component.groupLabel = 'Test Group'

		component.options = [
			{ value: 'option1', label: 'Option 1' },
			{ value: 'option2', label: 'Option 2' },
			{ value: 'option3', label: 'Option 3' },
		]

		fixture.detectChanges()
	})
/* eslint-disable */

	describe('setOption', () => {
		it('should set the value and call onChange and onTouched', () => {
			spyOn(component, 'onChange')
			spyOn(component, 'onTouched')

			component.setOption('option2')

			expect(component.value).toBe('option2')
			expect(component.onChange).toHaveBeenCalledWith('option2')
			expect(component.onTouched).toHaveBeenCalledWith()
		})
	})


	describe('toggleOption', () => {
		beforeEach(() => {

			component.onChange = jasmine.createSpy('onChange')
			component.onTouched = jasmine.createSpy('onTouched')
		})

		it('should emit deselected event: KeyboardEvent when toggling a selected option', () => {
			const deselectedSpy: jasmine.Spy = spyOn(component.deselected, 'emit')

			component.value = 'option1'

			component.toggleOption('option1')

			expect(deselectedSpy).toHaveBeenCalledWith()
			expect(component.onChange).not.toHaveBeenCalled()
			expect(component.onTouched).toHaveBeenCalledWith()

		})

		it('should select a new option when toggling an unselected option', () => {
			const setOptionSpy: jasmine.Spy = spyOn(component, 'setOption').and.callThrough()

			component.value = 'option1'

			component.toggleOption('option2')

			expect(setOptionSpy).toHaveBeenCalledWith('option2')
			expect(component.onChange).toHaveBeenCalledWith('option2')
			expect(component.onTouched).toHaveBeenCalledWith()
		})
	})

	describe('handleKeydown', () => {
		it('should call setOption when space key is pressed', () => {
			const setOptionSpy: jasmine.Spy = spyOn(component, 'setOption').and.callThrough()
			const event: KeyboardEvent = new KeyboardEvent('keydown', { key: ' ' })

			component.handleKeydown(event, 1)

			expect(setOptionSpy).toHaveBeenCalledWith('option2')
		})

		it('should call moveToNext when ArrowDown key is pressed', () => {
			spyOn<any>(component, 'moveToNext')
			const event: KeyboardEvent = new KeyboardEvent('keydown', { key: 'ArrowDown' })

			component.handleKeydown(event, 1)

			expect(component['moveToNext']).toHaveBeenCalledWith(1)
		})

		it('should call moveToNext when ArrowRight key is pressed', () => {
			spyOn<any>(component, 'moveToNext')
			const event: KeyboardEvent = new KeyboardEvent('keydown', { key: 'ArrowRight' })

			component.handleKeydown(event, 1)

			expect(component['moveToNext']).toHaveBeenCalledWith(1)
		})

		it('should call moveToPrevious when ArrowUp key is pressed', () => {
			spyOn<any>(component, 'moveToPrevious')
			const event: KeyboardEvent = new KeyboardEvent('keydown', { key: 'ArrowUp' })

			component.handleKeydown(event, 1)

			expect(component['moveToPrevious']).toHaveBeenCalledWith(1)
		})

		it('should call moveToPrevious when ArrowLeft key is pressed', () => {
			spyOn<any>(component, 'moveToPrevious')
			const event: KeyboardEvent = new KeyboardEvent('keydown', { key: 'ArrowLeft' })

			component.handleKeydown(event, 1)

			expect(component['moveToPrevious']).toHaveBeenCalledWith(1)
		})
	})

	describe('moveToNext', () => {
		it('should move to the next option', () => {
			const moveToIndexSpy: jasmine.Spy = spyOn(component, 'moveToIndex')
			component.moveToNext(0)

			expect(moveToIndexSpy).toHaveBeenCalledWith(1)
		})

		it('should rollover to the first option when at the end', () => {
			const moveToIndexSpy: jasmine.Spy = spyOn(component, 'moveToIndex')
			component.moveToNext(2)

			expect(moveToIndexSpy).toHaveBeenCalledWith(0)
		})
	})

	describe('moveToPrevious', () => {
		it('should move to the previous option', () => {
			const moveToIndexSpy: jasmine.Spy = spyOn(component, 'moveToIndex')
			component.moveToPrevious(1)

			expect(moveToIndexSpy).toHaveBeenCalledWith(0)
		})

		it('should rollover to the last option when at the beginning', () => {
			const moveToIndexSpy: jasmine.Spy = spyOn(component, 'moveToIndex')
			component.moveToPrevious(0)

			expect(moveToIndexSpy).toHaveBeenCalledWith(2)
		})
	})

	describe('moveToIndex', () => {
		it('should set the option and focus the element', () => {
			const setOptionSpy: jasmine.Spy = spyOn(component, 'setOption').and.callThrough()
			const mockElement: {
				focus: jasmine.Spy<jasmine.Func>
			} = { focus: jasmine.createSpy('focus') }
			const getElementByIdSpy: jasmine.Spy = spyOn(document, 'getElementById').and.returnValue(mockElement as any)

			component.moveToIndex(1)

			expect(setOptionSpy).toHaveBeenCalledWith('option2')
			expect(getElementByIdSpy).toHaveBeenCalledWith(jasmine.stringMatching(/radio-select_\d+_1/))
			expect(mockElement.focus).toHaveBeenCalled()
		})

		it('should not throw an error if element is not found', () => {
			const setOptionSpy: jasmine.Spy = spyOn(component, 'setOption').and.callThrough()
			spyOn(document, 'getElementById').and.returnValue(null)

			expect(() => component.moveToIndex(1)).not.toThrow()
			expect(setOptionSpy).toHaveBeenCalledWith('option2')
		})
	})

	describe('isTabbable', () => {
		it('should return true for the first option when no value is selected', () => {
			component.value = ''

			expect(component['isTabbable']('option1', 0)).toBe(true)
			expect(component['isTabbable']('option2', 1)).toBe(false)
			expect(component['isTabbable']('option3', 2)).toBe(false)
		})

		it('should return true only for the selected option when a value is selected', () => {
			component.value = 'option2'

			expect(component['isTabbable']('option1', 0)).toBe(false)
			expect(component['isTabbable']('option2', 1)).toBe(true)
			expect(component['isTabbable']('option3', 2)).toBe(false)
		})
	})

	describe('isSelected', () => {
		it('should return true when the value matches the component value', () => {
			component.value = 'option2'

			expect(component['isSelected']('option1')).toBe(false)
			expect(component['isSelected']('option2')).toBe(true)
			expect(component['isSelected']('option3')).toBe(false)
		})

		it('should return false when no value is selected', () => {
			component.value = ''

			expect(component['isSelected']('option1')).toBe(false)
			expect(component['isSelected']('option2')).toBe(false)
			expect(component['isSelected']('option3')).toBe(false)
		})
	})
})
