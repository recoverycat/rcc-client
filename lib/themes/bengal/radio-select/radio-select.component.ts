import { DOCUMENT } from '@angular/common'
import { Component, EventEmitter, Inject, Optional, Output, forwardRef } from '@angular/core'
import { uniqueId } from '@rcc/core'
import { DefaultThemeCommonModule } from '@rcc/themes/default/default-theme-common.module'
import { RccRadioSelectBaseComponent } from '../..'
import { NG_VALUE_ACCESSOR, } from '@angular/forms'
import { GREYED_OUT } from '@rcc/common'

@Component({
    templateUrl: './radio-select.component.html',
    styleUrls: ['radio-select.component.scss'],
    selector: 'rcc-radio-select',
    imports: [DefaultThemeCommonModule],
    providers: [
        { provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => RccRadioSelectComponent),
            multi: true
        }
    ]
})

/**
 * This derived component can get the greyedOut state from either an injecting or inputting
 * parent component. Hence you will find the injectedGreyedOut as well as @Input() greyedOut
 * here. The former is set to be optional as there are cases where RccRadioSelectComponent is
 * dynamically and other cases where it is simply set by input of the parent component.
 */
export class RccRadioSelectComponent<T> extends RccRadioSelectBaseComponent<T> {
	public constructor(
		@Inject(DOCUMENT)
		private readonly document: Document,
		@Optional() @Inject(GREYED_OUT)
		injectedGreyedOut: boolean,

	) {
		super()
		if (injectedGreyedOut) this.greyedOut = injectedGreyedOut
	}

	/**
	 * we are allowing radio selects to be deselected by a second click. To make
	 * this work I have implemented a workaround. We emit an event when a deselection
	 * is occurring. This is noticed by the parent component that holds the
	 * queryControl and which implements all the radio select answer options.
	 * In that parent component we then reset the whole queryControl. Check
	 * SelectQueryWidgetComponent for the next step.
	*/

	@Output() public deselected: EventEmitter<void> = new EventEmitter<void>()

	private uniqueIdPart: string = uniqueId('radio-select')

	protected id(index: number): string {
		return `${this.uniqueIdPart}_${index}`
	}

	protected isTabbable(value: T, index: number): boolean {
		const isAnyValueSelected: boolean = this.options.find((option) => option.value === this.value) != null

		if (!isAnyValueSelected) return index === 0

		return this.isSelected(value)
	}

	protected isSelected(value: T): boolean {
		return this.value === value
	}

	public handleKeydown(event: KeyboardEvent, index: number): void {
		if (event.key === ' ')
			this.setOption(this.options[index].value)

		if (event.key === 'ArrowDown' || event.key === 'ArrowRight')
			this.moveToNext(index)

		if (event.key === 'ArrowUp' || event.key === 'ArrowLeft')
			this.moveToPrevious(index)
	}


	public setOption(value: T): void {
		this.value = value
		this.onChange(value)
		this.onTouched()
	}

	public toggleOption(value: T): void {
		if (this.isSelected(value)) {
			this.deselected.emit()
			this.onTouched()
		}

		else this.setOption(value)


	}

	public moveToNext(index: number): void {
		const shouldRollover: boolean = index + 1 >= this.options.length
		const nextIndex: number = shouldRollover ? 0 : index + 1

		this.moveToIndex(nextIndex)
	}

	public moveToPrevious(index: number): void {
		const shouldRollover: boolean = index === 0
		const nextIndex: number = shouldRollover ? this.options.length - 1 : index - 1

		this.moveToIndex(nextIndex)
	}

	public moveToIndex(nextIndex: number): void {
		this.setOption(this.options[nextIndex].value)
		const id: string = this.id(nextIndex)
		this.document.getElementById(id)?.focus()
	}
}
