import { Component, ElementRef, HostBinding, ViewChild, forwardRef } from '@angular/core'
import { RccCodeInputBaseComponent } from '@rcc/themes'
import { NG_VALUE_ACCESSOR } from '@angular/forms'

@Component({
	templateUrl	: './code-input.component.html',
	styleUrls	: ['./code-input.component.scss'],
	selector	: 'rcc-code-input',
	standalone	: true,
	providers:	[
			{
				provide: NG_VALUE_ACCESSOR,
				useExisting: forwardRef(() => RccCodeInputComponent),
				multi: true
			}
		]
})
export class RccCodeInputComponent extends RccCodeInputBaseComponent {
	private _input: ElementRef<HTMLInputElement>

	@HostBinding('style.--length')
	public get style(): number { return this.length }

	@ViewChild('inputControl')
	protected set input(newInput: ElementRef<HTMLInputElement>) {
		this._input = newInput
		if (this.value)
			this.setInputValue(this.value)
	}

	protected get input(): ElementRef<HTMLInputElement> {
		return this._input
	}

	public override writeValue(value: string): void {
		this.setInputValue(value)
	}

	private setInputValue(value: string): void {
		if (this.input)
			this.input.nativeElement.value = value
		this.value = value
	}

	protected handleInput(): void {
		if (this.input.nativeElement.value.length > this.length)
			this.input.nativeElement.value = this.value

		if (!this.validateInput(this.input.nativeElement.value))
			this.input.nativeElement.value = this.value

		this.value = this.input.nativeElement.value
		this.onChange(this.value)
	}

	private validateInput(value: string): boolean {
		if (value === '')
			return true
		if (this.mode === 'number')
			return !isNaN(parseInt(value, 10))
	}

	protected get minimum(): number | undefined {
		if (this.mode === 'number')
			return 0
	}

	protected get maximum(): number | undefined {
		if (this.mode === 'number' && this.length != null)
			return this.generateMaxNumberFromLength()
	}

	public generateMaxNumberFromLength(): number {
		return Math.pow(10, this.length) - 1
	}
}
