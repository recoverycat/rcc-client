import	{
			Component,
			forwardRef,
		}										from '@angular/core'

import	{	CommonModule					}	from '@angular/common'

import	{	TranslationsModule				}	from '@rcc/common/translations'
import	{	UiComponentsModule				}	from '@rcc/common/ui-components'

import	{	RccToggleBaseComponent			}	from '../../theming-mechanics'
import	{	NG_VALUE_ACCESSOR				}	from '@angular/forms'
@Component({
    selector: 'rcc-toggle',
    templateUrl: './toggle.component.html',
    styleUrls: ['./toggle.component.scss'],
    imports: [
        CommonModule,
        UiComponentsModule,
        TranslationsModule,
    ],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => RccToggleComponent),
            multi: true,
        },
    ]
})

export class RccToggleComponent extends RccToggleBaseComponent {
}
