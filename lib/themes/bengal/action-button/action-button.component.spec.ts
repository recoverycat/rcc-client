import { TestBed, ComponentFixture } from '@angular/core/testing'
import { RccActionButtonComponent } from './action-button.component'
import { HandlerAction } from '../../../common/src/actions'
import { RccAlertController, RccToastController } from '../../../common/src/modals-provider'

describe('RccActionButtonComponent', () => {
	let fixture: ComponentFixture<RccActionButtonComponent> | undefined = undefined

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports		: [RccActionButtonComponent],
			providers	: [
				RccAlertController,
				RccToastController,
			]
		}).compileComponents()
		fixture = TestBed.createComponent(RccActionButtonComponent)
	})

	describe('When action has outline property', () => {
		it('should render with an outline class', () => {
			if (fixture == null) return

			const component: RccActionButtonComponent = fixture?.componentInstance
			const action: HandlerAction = {
				handler: () => {},
				TEMP_OUTLINE_PROPERTY: true,
				icon: '',
				label: ''
			}
			component.action = action
			fixture.detectChanges()

			const element: HTMLElement = fixture?.nativeElement as HTMLElement

			const button: HTMLButtonElement | null = element.querySelector('button')

			expect(button).toHaveClass('outline')
		})
	})
})
