import { TestBed, ComponentFixture, fakeAsync, tick } from '@angular/core/testing'
import { RccPullDownSelectComponent } from './pull-down-select.component'

describe('RccPullDownSelectComponent', () => {

	let component: RccPullDownSelectComponent<unknown> = undefined!
	let fixture: ComponentFixture<RccPullDownSelectComponent<unknown>> = undefined!
	let nativeElement: HTMLElement = undefined!

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [ RccPullDownSelectComponent ]
		}).compileComponents()

		fixture = TestBed.createComponent(RccPullDownSelectComponent)
		component = fixture.componentInstance
		nativeElement = fixture.nativeElement as HTMLElement
	})

	afterAll(() => {
		TestBed.resetTestingModule()
	})

	describe('Hover behavior', () => {
		let elementToDispatchEvent: HTMLElement = undefined!

		beforeEach(() => {
			component.enableHovering = true
			fixture.detectChanges()

			elementToDispatchEvent = nativeElement.querySelector('div') as HTMLElement
		})

		it('should not open if hover time is less than MINIMUM_HOVER_TIME', fakeAsync(() => {
			elementToDispatchEvent.dispatchEvent(new MouseEvent('mouseenter'))

			tick(25)

			expect(component.expanded).toBe(false)

			elementToDispatchEvent.dispatchEvent(new MouseEvent('mouseleave'))
		}))

		it('should open if hover time is more than MINIMUM_HOVER_TIME', fakeAsync(() => {
			elementToDispatchEvent.dispatchEvent(new Event('mouseenter'))

			tick(75)

			expect(component.expanded).toBe(true)
		}))

		it('should stop timer on mouse leave', fakeAsync(() => {
			elementToDispatchEvent.dispatchEvent(new MouseEvent('mouseenter'))

			tick(25)

			elementToDispatchEvent.dispatchEvent(new MouseEvent('mouseleave'))

			tick(100)

			expect(component.expanded).toBe(false)
		}))

		it('should close on mouse leave', fakeAsync(() => {
			elementToDispatchEvent.dispatchEvent(new MouseEvent('mouseenter'))

			tick(1000)

			elementToDispatchEvent.dispatchEvent(new MouseEvent('mouseleave'))

			expect(component.expanded).toBe(false)
		}))
	})
})
