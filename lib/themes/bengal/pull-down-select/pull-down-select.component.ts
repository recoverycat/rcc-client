import 	{
			DOCUMENT
		} 										from '@angular/common'
import 	{
			Component,
			ElementRef,
			forwardRef,
			Inject,
			OnChanges,
			OnDestroy,
			SimpleChanges,
			ViewChild,
		}						 				from '@angular/core'
import 	{
			FormControl,
			FormsModule,
			NG_VALUE_ACCESSOR,
			ReactiveFormsModule
		} 										from '@angular/forms'
import 	{
			getTabbableDescendants,
			uniqueId
		} 										from '@rcc/core'
import 	{
			DefaultThemeCommonModule
		} 										from '@rcc/themes/default/default-theme-common.module'
import 	{
			RccPullDownSelectBaseComponent,
		} 										from '../../theming-mechanics'
import 	{
			debounceTime,
			distinctUntilChanged,
			filter,
			Observable,
			startWith,
			Subject,
			takeUntil,
		} 										from 'rxjs'
import 	{
			FilterFn,
			Option
		} 										from '@rcc/themes/theming-mechanics/components/pull-down-select/pull-down-select.commons'
import 	{
			RccTranslationService,
			TranslationFilterFunction
		} 										from '@rcc/common'

const observerMarginBuffer : number = 20

/**
 * Searchable/filterable combobox.
 * Accessibility implemented according to https://www.w3.org/WAI/ARIA/apg/patterns/combobox/examples/combobox-autocomplete-none/
 */
@Component({
    templateUrl: './pull-down-select.component.html',
    styleUrls: ['./pull-down-select.component.scss'],
    selector: 'rcc-pull-down-select',
    imports: [
        DefaultThemeCommonModule,
        FormsModule,
        ReactiveFormsModule
    ],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => RccPullDownSelectComponent),
            multi: true
        }
    ]
})
export class RccPullDownSelectComponent<T> extends RccPullDownSelectBaseComponent<T> implements OnChanges, OnDestroy {
	@ViewChild('combobox')
	private combobox: ElementRef<HTMLInputElement>

	@ViewChild('listbox')
	private listbox: ElementRef<HTMLDivElement>

	protected showAbove			: boolean 					= false
	protected listboxId			: string 					= uniqueId('pull-down-select__listbox')

	private uniqueIdPart		: string	 				= uniqueId('pull-down-select')
	private highlightedIndex	: number 					= undefined

	private destroy$ 			: Subject<void> 			= new Subject<void>()

	protected typeaheadControl	: FormControl<string>		= new FormControl<string>('')

	private userSelection$		: Subject<Option<T>>		= new Subject<Option<T>>()
	protected filter$			: Observable<string>		= new Observable<string>()

	protected adoptedFilterFn	: TranslationFilterFunction	= null

	public constructor(
		@Inject(DOCUMENT)
		private readonly document : Document,
		private readonly rccTranslationService : RccTranslationService
	) {
		super()

		// User selects something from the pull-down
		this.userSelection$
		.pipe(takeUntil(this.destroy$))
		.subscribe(selection => this.updateValue(selection.value))

		// Options of the pull-down change
		this.option$
		.pipe(takeUntil(this.destroy$))
		.subscribe(() => this.updateValue(this.value))

		// App language changes
		this.rccTranslationService.activeLanguageChange$
		.pipe(takeUntil(this.destroy$))
		.subscribe(() => this.updateValue(this.value))

		this.handleAllowTextInput()
	}

	public ngOnChanges(changes: SimpleChanges) : void {
		this.adoptedFilterFn = this.adoptFilterFn(this.filterFn)

		if ('allowTextInput' in changes) this.handleAllowTextInput()
	}

	/**
	 * Casts a {@link FilterFn} to a {@link TranslationFilterFunction}
	 * by checking that an Option with a certain translation exists
	*/
	public adoptFilterFn(originalFilterFn: FilterFn<T>) : TranslationFilterFunction {
		if (originalFilterFn === null) return () : boolean => true

		return (translation: string, filterBy: string) : boolean =>
			originalFilterFn(filterBy, [{ value: undefined, label: translation }]).length === 1
	}

	// #region Select logic

	/**
	 * Handles text input in the search field (= filter options) if enabled
	 */
	private handleAllowTextInput() : void {
		if (!this.allowTextInput) {
			this.filter$ = null
			return
		}

		this.filter$ =	this.typeaheadControl.valueChanges
						.pipe(
							debounceTime(200),
							startWith(this.typeaheadControl.value),
							filter(() => this.allowTextInput),
							takeUntil(this.destroy$),
							distinctUntilChanged()
						)
	}

	protected selectOption(selection: Option<T>) : void {
		this.userSelection$.next(selection)

		if(this.clearInputField)
			this.combobox.nativeElement.placeholder = this.rccTranslationService.translate('QUESTIONNAIRE_EDITOR.CHANGE_TEMPLATE')

		this.close()
	}

	private updateValue(value: T): void {
		this.updateTypeaheadControl(value)

		if (this.value === value) return

		this.writeValue(value)
		if (typeof this.onChange === 'function')
			this.onChange(value)
	}

	public writeValue(value: T) : void {
		const valueHasChanged : boolean = this.value !== value

		this.value = value

		if (valueHasChanged) this.updateTypeaheadControl()
	}

	/**
	 * Update the typeahead control with the selected value
	 * or empty it if the value is undefined
	 * if clearInputField is set on true
	 */
	public updateTypeaheadControl(value: T = this.value) : void {

		if (this.clearInputField || value === undefined) return this.typeaheadControl.setValue('')

		const currentOptions: Option<T>[] = this.option$.getValue() || []

		let selectedOption: Option<T> = currentOptions.find(option => option.value === value)

		if (typeof selectedOption === 'undefined' && typeof value === 'string')
			selectedOption = { value }

		const translatedOption: string = this.rccTranslationService.translate(selectedOption)

		this.typeaheadControl.setValue(translatedOption)

	}

	// #endregion
	// #region Display logic

	/** Opens the pull-down with waiting time to display the options only by choice, not coincidence */

	public readonly MINIMUM_HOVER_TIME	: number			= 50

	private ref: ReturnType<typeof setTimeout>
	public onMouseEnter(): void {
		this.ref = setTimeout(() => {
			this.open()
		}, this.MINIMUM_HOVER_TIME)
	}

	public onMouseLeave(): void {
		clearTimeout(this.ref)

		if (this.expanded === true) this.close()
	}

	public open() : void {
		this.expanded = true
		this.showAbove = this.shouldShowAbove()

		if (this.combobox)
			this.combobox.nativeElement.select()
	}

	protected blur() : void {
		this.updateTypeaheadControl()
		this.close()
	}

	protected close() : void {
		this.expanded = false
	}

	/** Determines whether the pull-down options should be displayed above or below the value box */
	protected shouldShowAbove() : boolean {
		const comboboxPosition	: number	= this.combobox.nativeElement.getBoundingClientRect().y
		const comboboxHeight	: number	= this.combobox.nativeElement.clientHeight
		const popupHeight		: number	= this.listbox.nativeElement.clientHeight
		const pageHeight		: number	= this.document.body.clientHeight

		const enoughSpaceBelow	: boolean
								= comboboxPosition + comboboxHeight + popupHeight + observerMarginBuffer <= pageHeight

		const enoughSpaceAbove	: boolean
								= comboboxPosition > popupHeight

		return !enoughSpaceBelow && enoughSpaceAbove
	}

	// #endregion
	// #region Keydown

	/**
	 * Pressing Escape, Enter or Tab on the input closes the options list.
	 * Pressing ArrowDown on the input enters the options list and focuses the first element,
	 * ArrowUp enters the list and focuses the last element.
	 */
	protected handleInputKeydown(event: KeyboardEvent) : void {
		if (event.key === 'Tab') this.close()
		if (event.key === 'Enter') {
			if (this.expanded) event.preventDefault() // Prevent modals from closing if pull-down is in a modal
			if (!this.allowTextInput && !this.expanded) this.open()
			else this.close()
		}
		if (event.key === 'Escape') {
			if (this.expanded) event.stopPropagation() // Prevent modals from closing
			this.close()
		}
		if (event.key !== 'ArrowDown' && event.key !== 'ArrowUp') return

		this.open()
		event.preventDefault()

		const listboxElement : HTMLElement = this.listbox?.nativeElement
		if (!listboxElement) return

		const tabbables : HTMLElement[] = getTabbableDescendants(listboxElement)
		if (!tabbables.length) return

		if (event.key === 'ArrowDown')	tabbables[0].focus()
		if (event.key === 'ArrowUp')	tabbables[tabbables.length-1].focus()
	}

	protected handleOptionKeydown(event: KeyboardEvent, option: Option<T>) : void {
		if (event.key === 'Enter') {
			event.preventDefault()
			this.selectOption(option)
			this.close()
		}

		if (event.key === 'Tab' || event.key === 'Escape') {
			const comboboxElement : HTMLElement = this.combobox?.nativeElement
			comboboxElement.focus()
			event.stopPropagation() // Prevent modals from closing
			this.close()
		}
	}

	// #endregion
	// #region Aria labels

	protected get activeDescendant() : string | undefined {
		return this.expanded ? this.optionId(this.highlightedIndex) : undefined
	}

	protected isHighlighted(index: number) : boolean {
		return this.highlightedIndex === index
	}

	protected isSelected(option: Option<T>) : boolean {
		return option.value === this.value
	}

	protected optionId(index: number) : string {
		return `${this.uniqueIdPart}_${index}`
	}

	// #endregion

	public ngOnDestroy() : void {
		this.destroy$.next()
		this.destroy$.complete()
	}
}
