
import	{
			Component,
			HostBinding,
		}											from	'@angular/core'
import	{	RccListItemTagBaseComponent			}	from	'../../../theming-mechanics/components/item-tag/list/item-tag-list-base.component'
import	{	CommonModule						}	from	'@angular/common'
import	{	RccIconComponent					}	from	'@rcc/common/ui-components/icons'
import	{	Action								}	from	'@rcc/common/actions'
import	{	TranslationsModule					}	from	'@rcc/common/translations'
import	{	RccCardComponent					}	from	'@rcc/common/ui-components/card'


@Component({
    selector: 'rcc-list-item-tag',
    templateUrl: './item-tag-list.component.html',
    styleUrls: ['./item-tag-list.component.scss'],
    imports: [
        CommonModule,
        RccIconComponent,
        RccCardComponent,
        TranslationsModule
    ]
})
export class RccListItemTagComponent extends RccListItemTagBaseComponent {

	@HostBinding('class.one-action-only')
	protected get hasExactlyOneAction(): boolean {
		return super.hasExactlyOneAction
	}

	protected complexAction: Action = {
		label:		'THEME.ACTIONS.COMPLEX',
		icon:		'next_thin',
		handler:	() => this.onClick(),
	}

	protected get primaryAction(): Action {
		switch(this.mode) {
			case 'simple':
			case 'basic':
				return this.actions[0]
			case 'complex':
				return this.complexAction
			case 'select':
				return this.checkmarkAction
		}
	}

	protected get secondaryAction(): Action {
		if (this.mode === 'basic')
			return this.actions[1]
		return undefined
	}

	private checkmarkAction: Action = {
		icon: 'check_mark',
		label: '',
		handler: () => { this.selected = !this.selected }
	}

	protected select(): void {
		this.primaryAction.handler?.()
	}

	protected handleSpace($event: Event): void {
		if (this.hasExactlyOneAction || this.mode === 'select') $event.preventDefault()

		if (this.mode === 'select' as const) this.select()

		else if (this.hasExactlyOneAction) void this.onClick()
	}


}
