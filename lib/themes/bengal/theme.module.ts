import 	{
			NgModule,
			Type
		}											from '@angular/core'
import	{	CommonModule						}	from '@angular/common'
import	{	provideTranslationMap				}	from '@rcc/common/translations'
import	{	RccMainMenuBodyComponent			}	from './main-menu-body/main-menu-body.component'
import	{	RccActionButtonComponent			}	from './action-button/action-button.component'
import	{	RccButtonComponent					}	from './button/button.component'
import	{	RccPillButtonComponent				}	from './pill-button/pill-button.component'
import	{	RccCodeInputComponent				}	from './code-input/code-input.component'
import	{	RccEditTextComponent				}	from './edit-text/edit-text.component'
import	{	RccEditTextLabelDirective			}	from './edit-text-label/edit-text-label.directive'
import	{	RccSubmitButtonComponent			}	from './submit-button/submit-button.component'
import	{	RccFullPageComponent				}	from './full-page/full-page.component'
import	{	RccHomePageGreetingComponent		}	from './home-page-greeting/home-page-greeting.component'
import	{	RccRadioSelectComponent				}	from './radio-select/radio-select.component'
import	{	RccTextareaComponent				}	from './textarea/textarea.component'
import	{	RccPullDownSelectComponent			}	from './pull-down-select/pull-down-select.component'
import	{	RccQuickActionListComponent			}	from './quick-action-list/quick-action-list.component'
import	{	RccToggleComponent					}	from './toggle/toggle.component'
import	{	RccTimeSelectComponent				}	from './time-select/time-select.component'
import 	{ 	RccDaysInWeekSelectorComponent 		} 	from './days-in-week-selector/days-in-week-selector.component'
import	{	RccColorCategoryDirective			}	from '@rcc/common/ui-components/color-category/color-category.directive'
import	{	RccSegmentComponent					}	from './segment/segment.component'
import 	{	RccDatePickerComponent 				} 	from './date-picker/date-picker.component'
import	{	RccTextInputComponent				}	from './text-input/text-input.component'
import	{	RccReducedFullPageComponent			}	from './reduced-full-page/reduced-full-page.component'
import	{	RccScrollToTopButtonComponent		}	from './scroll-to-top-button'
import	{	RccSplashScreenComponent			}	from './splash-screen'
import	{
			RccStandaloneItemTagComponent,
			RccListItemTagComponent,
		}											from './item-tag'
import	{	RccItemListComponent				}	from './item-list/item-list.component'

import	{	BengalInjectionsModule				}	from './_injections'

import en from './i18n/en.json'
import de from './i18n/de.json'

const components : Type<unknown>[] = [
	RccActionButtonComponent,
	RccButtonComponent,
	RccPillButtonComponent,
	RccCodeInputComponent,
	RccColorCategoryDirective,
	RccDatePickerComponent,
	RccDaysInWeekSelectorComponent,
	RccEditTextComponent,
	RccEditTextLabelDirective,
	RccFullPageComponent,
	RccHomePageGreetingComponent,
	RccItemListComponent,
	RccListItemTagComponent,
	RccMainMenuBodyComponent,
	RccPullDownSelectComponent,
	RccQuickActionListComponent,
	RccRadioSelectComponent,
	RccReducedFullPageComponent,
	RccSegmentComponent,
	RccSplashScreenComponent,
	RccSubmitButtonComponent,
	RccStandaloneItemTagComponent,
	RccTextareaComponent,
	RccTextInputComponent,
	RccTimeSelectComponent,
	RccToggleComponent,
	RccScrollToTopButtonComponent
]

@NgModule({
	imports: [
		...components,
		CommonModule,
		BengalInjectionsModule
	],
	exports: [
		...components
	],
	providers: [
		provideTranslationMap('THEME', { en, de })
	]
})
export class RccThemeModule {
}
