import { CommonModule } from '@angular/common'
import { Component, ElementRef, EventEmitter, Output, ViewChild, forwardRef } from '@angular/core'
import { NG_VALUE_ACCESSOR, ReactiveFormsModule } from '@angular/forms'
import { RccTextareaBaseComponent } from '@rcc/themes/theming-mechanics/components/textarea/textarea-base.component'

@Component({
    selector: 'rcc-textarea',
    templateUrl: './textarea.component.html',
    styleUrls: ['./textarea.component.scss'],
    imports: [ReactiveFormsModule, CommonModule],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => RccTextareaComponent),
            multi: true
        }
    ]
})
export class RccTextareaComponent extends RccTextareaBaseComponent {
	@ViewChild('textInput')
	private textInput: ElementRef<HTMLTextAreaElement>

	/**
	 * we are allowing the type input to be deleted by 'backspace-removing' the
	 * value. To make this work I have implemented a workaround.
	 * We emit an event when a deletion
	 * is occurring. This is noticed by the parent component that holds the
	 * queryControl.
	 * In that parent component we then reset the whole queryControl. Check
	 * FallbackQueryWidgetComponent for the next step.
	*/

	@Output() public deleted: EventEmitter<void> = new EventEmitter<void>()

	protected focusOnInput(): void {
		this.textInput.nativeElement.focus()
	}

	public onTextInputChange(event: Event): void {
		const target: HTMLTextAreaElement = event.target as HTMLTextAreaElement
		const value: string = target.value.trim() // to account for cases with only whitespaces

		if(value === '') this.deleted.emit()

		this.onChange(target.value)

	}
}
