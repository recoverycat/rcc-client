import { ComponentFixture, TestBed } from '@angular/core/testing'
import { RccTextareaComponent } from './textarea.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'


describe('RccTextareaComponent', () => {
	let component: RccTextareaComponent = undefined!
	let fixture: ComponentFixture<RccTextareaComponent> = undefined!

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [FormsModule, ReactiveFormsModule, RccTextareaComponent],
		}).compileComponents()

		fixture = TestBed.createComponent(RccTextareaComponent)
		component = fixture.componentInstance
		fixture.detectChanges()
	})

	it('should create', () => {
		expect(component).toBeTruthy()
	})

	describe('onTextInputChange', () => {
		it('should emit deleted event when input is empty or only whitespace', () => {
			const deletedEmitSpy: jasmine.Spy = spyOn(component.deleted, 'emit')
			const event: Event = new Event('input')
			Object.defineProperty(event, 'target', { value: { value: '   ' } })

			component.onTextInputChange(event)

			expect(deletedEmitSpy).toHaveBeenCalledWith()

		})

		it('should not emit deleted event when input has non-whitespace characters', () => {
			const deletedEmitSpy: jasmine.Spy = spyOn(component.deleted, 'emit')
			const event: Event = new Event('input')
			Object.defineProperty(event, 'target', { value: { value: ' hello ' } })

			component.onTextInputChange(event)

			expect(deletedEmitSpy).not.toHaveBeenCalled()
		})

		it('should call onChange with the untrimmed input value', () => {
/* eslint-disable */
			spyOn(component, 'onChange' as any)
			const event: Event = new Event('input')
			Object.defineProperty(event, 'target', { value: { value: ' hello ' } })

			component.onTextInputChange(event)

			expect(component['onChange']).toHaveBeenCalledWith(' hello ')
		})
	})
})
/* eslint-enable */
