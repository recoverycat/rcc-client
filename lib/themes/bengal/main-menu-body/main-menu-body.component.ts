import	{
			DOCUMENT
		} 											from '@angular/common'
import	{
			Component,
			Inject,
			OnDestroy,
			Optional,
			ViewEncapsulation
		} 											from '@angular/core'
import	{
			RouterModule
		} 											from '@angular/router'

import	{
			getTabbableDescendants
		}											from '@rcc/core'
import	{
			Action,
			ActionService
		} 											from '@rcc/common/actions'

import	{
			MainMenuEntry,
			MainMenuService,
			MAIN_MENU_CONFIG,
			MAIN_MENU_ENTRIES,
			MAIN_MENU_ENTRIES_META
		} 											from '@rcc/common/main-menu'
import	{
			DefaultThemeCommonModule
		} 											from '@rcc/themes/default/default-theme-common.module'
import	{
			RccMainMenuBodyBaseComponent
		} 											from '@rcc/themes/theming-mechanics'
import	{
			Observable,
			Subject,
			takeUntil
		} 											from 'rxjs'
import	{
			RccMainMenuEntryComponent
		} 											from '../main-menu-entry/main-menu-entry.component'
import	{	RccActionButtonComponent			}	from '../action-button/action-button.component'

@Component({
    selector: 'rcc-main-menu-body',
    templateUrl: './main-menu-body.component.html',
    styleUrls: ['./main-menu-body.component.scss'],
    encapsulation: ViewEncapsulation.None,
    imports: [
        DefaultThemeCommonModule,
        RouterModule,
        RccMainMenuEntryComponent,
        RccActionButtonComponent
    ]
})
export class RccMainMenuBodyComponent extends RccMainMenuBodyBaseComponent implements OnDestroy {

	private returnFocusTo	:	Element
	private destroy$		:	Subject<void> = new Subject()

	public constructor(

		@Optional() @Inject(MAIN_MENU_ENTRIES)
		public entries 					: MainMenuEntry[],

		@Optional() @Inject(MAIN_MENU_ENTRIES_META)
		public metaEntries 				: MainMenuEntry[],

		@Optional() @Inject(MAIN_MENU_CONFIG)
		public config					: Record<string, unknown>,

		@Inject(DOCUMENT)
		private document				: Document,

		protected actionService			: ActionService,

		protected mainMenuService		: MainMenuService,

	) {
		super(entries,metaEntries, config, actionService)
		this.subscribeToMainMenuService()
	}

	protected isOpen$: Observable<boolean> = this.mainMenuService.isMenuOpen$

	protected closeAction: Action = {
		label: 'CLOSE',
		icon: 'close',
		handler: () => this.closeDialog()
	}

	protected closeDialog(): void {
		this.mainMenuService.setIsMenuOpen(false)
	}

	private subscribeToMainMenuService(): void {
		this.mainMenuService.isMenuOpen$
			.pipe(takeUntil(this.destroy$))
			.subscribe((isOpen) => {

				if (isOpen) {
					this.returnFocusTo = this.document.activeElement

					setTimeout(() => {

						const closeParent: HTMLElement
							= this.document.getElementById('main-menu-dialog-close-button')
						const tabbables		: HTMLElement[]
											= getTabbableDescendants(closeParent)
						tabbables[0]?.focus()

					}, 200)
				} else {
					const returnFocusTo: HTMLElement = this.returnFocusTo as HTMLElement
					returnFocusTo?.focus()
				}
			}
		)
	}

	public ngOnDestroy(): void {
		this.destroy$.next()
		this.destroy$.complete()
	}
}
