import { Component, forwardRef } from '@angular/core'
import { Option, RccSegmentBaseComponent } from '../../theming-mechanics/components/segment/segment-base.component'
import { DefaultThemeCommonModule } from '@rcc/themes/default/default-theme-common.module'
import { uniqueId } from '@rcc/core'
import { NG_VALUE_ACCESSOR } from '@angular/forms'

@Component({
    templateUrl: './segment.component.html',
    styleUrls: ['./segment.component.scss'],
    selector: 'rcc-segment',
    imports: [DefaultThemeCommonModule],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => RccSegmentComponent),
            multi: true
        }
    ]
})
export class RccSegmentComponent extends RccSegmentBaseComponent {
	protected isSelected(option: Option, index: number): boolean {
		if (this.value == null)
			return index === 0
		
		return this.value === option.value
	}

	private uniqueIdPart: string = uniqueId('rcc-segment')

	private _selectedIndex: number = 0
	private get selectedIndex(): number {
		return this._selectedIndex
	}

	private set selectedIndex(value: number) {
		this._selectedIndex = value
		this.updateValue()
	}

	private updateValue(): void {
		this.value = this.options[this.selectedIndex].value
		this.onChange(this.value)
		this.onTouched()
	}

	protected itemId(index: number): string {
		return `${this.uniqueIdPart}-${index}`
	}

	protected get selectedId(): string {
		return this.itemId(this.selectedIndex)
	}

	protected itemSelected(index: number): boolean {
		return this.selectedIndex === index
	}

	protected moveForward(): void {
		if (this.selectedIndex + 1 >= this.options.length)
			this.selectedIndex = 0
		else
			++this.selectedIndex
	}

	protected moveBack(): void {
		if (this.selectedIndex === 0)
			this.selectedIndex = this.options.length - 1
		else
			--this.selectedIndex
	}

	protected selectItem(index: number): void {
		this.selectedIndex = index
	}
}
