import { RccBuildInfoService } from '@rcc/common'
import { RccSplashScreenComponent } from './splash-screen.component'
import { ChangeDetectorRef } from '@angular/core'
import { ComponentFixture, TestBed } from '@angular/core/testing'

/* eslint-disable */

describe('RccSplashScreenComponent', () => {
	let component: RccSplashScreenComponent = undefined!
	let rccBuildInfoService: RccBuildInfoService = undefined!
	let changeDetectorRef: ChangeDetectorRef = undefined!
	let document: Document = undefined!
	let fixture: ComponentFixture<RccSplashScreenComponent> = undefined!

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [RccSplashScreenComponent],
			providers: [
				RccBuildInfoService,
				Document,
				{ provide: ChangeDetectorRef, useValue: changeDetectorRef },
			]
		}).compileComponents()
		rccBuildInfoService = TestBed.inject(RccBuildInfoService)
		changeDetectorRef = TestBed.inject(ChangeDetectorRef)
		document = TestBed.inject(Document)
		fixture = TestBed.createComponent(RccSplashScreenComponent)
		component = fixture.componentInstance

	})

	it('should create', () => {
		expect(component).toBeTruthy()
	})

	describe('selectVariant', () => {
		it('should return hcpColors for hcp variant', () => {
			component.variant = 'hcp'
			component.hcpColors = { topColor: '#123', bottomColor: '#456' }

			expect(component.selectVariant()).toEqual(component.hcpColors)
		})

		it('should return patColors for pat variant', () => {
			component.variant = 'pat'
			component.patColors = { topColor: '#789', bottomColor: '#ABC' }

			expect(component.selectVariant()).toEqual(component.patColors)
		})
	})

	describe('selectHexColorFromTheme', () => {
		it('should convert RGB to HEX', () => {
			const mockStyle: Partial<CSSStyleDeclaration> = {
				getPropertyValue: jasmine.createSpy().and.returnValue('255, 128, 0')
			}
			spyOn(window, 'getComputedStyle').and.returnValue(mockStyle as CSSStyleDeclaration)

			const result: string = component.selectHexColorFromTheme('--test-color')

			expect(window.getComputedStyle).toHaveBeenCalled()
			expect(mockStyle.getPropertyValue).toHaveBeenCalledWith('--test-color')
			expect(result).toBe('#FF8000')
		})

		it('should handle single-digit hex values', () => {
			const mockStyle: Partial<CSSStyleDeclaration> = {
				getPropertyValue: jasmine.createSpy().and.returnValue('5, 10, 15')
			}
			spyOn(window, 'getComputedStyle').and.returnValue(mockStyle as CSSStyleDeclaration)

			const result: string = component.selectHexColorFromTheme('--test-color')

			expect(result).toBe('#050A0F')
		})
	})

	describe('showSplashScreen', () => {
		it('should get value from sessionStorage', () => {
			spyOn(sessionStorage, 'getItem').and.returnValue('true')

			expect(component.showSplashScreen).toBe('true')
		})

		it('should set value in sessionStorage', () => {
			spyOn(sessionStorage, 'setItem')
			const detectChangesSpy: jasmine.Spy = jasmine.createSpy('detectChanges')

			component['changeDetectorRef'] = { detectChanges: detectChangesSpy } as any
			component.ngAfterViewInit()

			component.showSplashScreen = true

			const splashScreenValue: string | null = sessionStorage.getItem('showSplashScreen')

			expect(splashScreenValue).toBe('true')
		})
	})

	describe('ngOnInit', () => {
		it('should initialize colors and select variant', () => {
			spyOn(component, 'selectHexColorFromTheme').and.returnValues('#123', '#456', '#789', '#ABC')
			const selectVariantSpy: jasmine.Spy = spyOn(component, 'selectVariant').and.callThrough()

			component.ngOnInit()

			expect(component.hcpColors).toEqual({ topColor: '#123', bottomColor: '#456' })
			expect(component.patColors).toEqual({ topColor: '#789', bottomColor: '#ABC' })
			expect(selectVariantSpy).toHaveBeenCalledWith()
			expect(component.selectedVariant).toEqual(component.selectVariant())
		})
	})

	describe('ngAfterViewInit', () => {
		it('should call detectChanges', () => {
			const detectChangesSpy: jasmine.Spy<() => void> = jasmine.createSpy('detectChanges')

			component['changeDetectorRef'] = { detectChanges: detectChangesSpy } as any
			component.ngAfterViewInit()

			expect(detectChangesSpy).toHaveBeenCalled()
		})
	})

})
