import { Component } from '@angular/core'
import { RouterModule } from '@angular/router'
import { CurrentDateMessageComponent } from '@rcc/common/current-date-message'
import { SettingsModule } from '@rcc/common/settings'
import { DefaultThemeCommonModule } from '@rcc/themes/default/default-theme-common.module'
import { RccHomePageGreetingBaseComponent } from '../..'
import { RccBuildInfoService } from '@rcc/common'

@Component({
    selector: 'rcc-home-page-greeting',
    styleUrls: ['./home-page-greeting.component.css'],
    templateUrl: './home-page-greeting.component.html',
    imports: [
        DefaultThemeCommonModule,
        SettingsModule,
        CurrentDateMessageComponent,
        RouterModule
    ]
})
export class RccHomePageGreetingComponent extends RccHomePageGreetingBaseComponent {

	protected tag: string = this.buildInfoService.buildInfos.tag

	public constructor(
		private readonly buildInfoService: RccBuildInfoService,
	) {
		super()
	}

}
