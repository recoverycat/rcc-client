import	{
	ChangeDetectionStrategy,
			Component,
			HostBinding,
		}										from '@angular/core'

import	{	CommonModule					}	from '@angular/common'

import	{
			RccPillButtonBaseComponent
		}										from '@rcc/themes'

import	{	TranslationsModule				}	from '@rcc/common/translations'
import	{
			UiComponentsModule,
		}										from '@rcc/common/ui-components'

@Component({
    selector: 'button[pill-button], a[pill-button]',
    template: '<ng-content></ng-content>',
    styleUrls: ['./pill-button.component.scss'],
    imports: [
        CommonModule,
        UiComponentsModule,
        TranslationsModule,
    ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class RccPillButtonComponent extends RccPillButtonBaseComponent {
	@HostBinding('disabled')
	public disabled: boolean

	@HostBinding('class.outline')
	public outline: boolean
}
