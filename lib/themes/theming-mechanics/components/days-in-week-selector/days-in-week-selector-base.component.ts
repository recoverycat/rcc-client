import  {
            Component,
            EventEmitter,
            HostBinding,
            Input,
            Output
        }                   from '@angular/core'

import  {
            Required
        }                   from '@rcc/common/decorators'

@Component({
    selector: 'rcc-days-in-week-selector',
    template: '',
    standalone: false
})
export class RccDaysInWeekSelectorBaseComponent {

    @Input() public daysInWeek?           : number[] | null

    @Input() @Required public groupLabel? : string

    @Output() public daysInWeekChange     : EventEmitter<number[]> = new EventEmitter<number[]>()

    @HostBinding('attr.aria-label') public get hostLabel() : string {
        return this.groupLabel || ''
    }
}
