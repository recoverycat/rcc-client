import	{	Type						}	from '@angular/core'
import	{	RccTextInputBaseComponent	}	from './text-input-base.component'
import 	{	ComponentUnitTests			}	from '../components.commons.spec'

function rccTextInputTests(componentClass: Type<unknown>): void {
	it('extends the base component class (RccTextInputBaseComponent)', () => {
		const proto: unknown = componentClass.prototype as unknown

		expect(proto).toBeInstanceOf(RccTextInputBaseComponent)
	})
}

export const RccTextInputComponentTests: ComponentUnitTests = {
	baseComponentClass:	RccTextInputBaseComponent,
	unitTests:			rccTextInputTests,
}
