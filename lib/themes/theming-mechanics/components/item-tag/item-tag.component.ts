import	{
			Component,
			Injector,
			Input,
			OnChanges,
			TemplateRef,
			ViewChild,
			ViewContainerRef,
			ElementRef,
			ChangeDetectorRef,
			Type,
			StaticProvider
		}								from '@angular/core'

import 	{
			ActionSheetController,
			IonItemSliding,
		}								from '@ionic/angular'

import	{
			Item,
		}								from '@rcc/core'

import	{
			IconsService
		}								from '@rcc/common/src/icons'
import	{	RccTranslationService 	}	from '@rcc/common/src/translations'
import	{
			RccToastController,
		}								from '@rcc/common/src/modals-provider'
import	{
			Action,
			ActionService
		}								from '@rcc/common/src/actions'

import	{
			ItemRepresentation,
			ItemAction,
			ItemActionRole
		}								from '@rcc/common/src/items/item.commons'

import	{	ItemService				}	from '@rcc/common/src/items/item.service'
import	{	ExposeTemplateComponent	}	from '@rcc/common/src/items/expose-template.component'


/**
 * Render actions for given {@link item}
 * Which actions are rendered and in which order is resolved by using
 * {@link ItemService["getActions"]}.
 *
 */
@Component({
    selector: 'rcc-item-tag',
    template: '',
    styleUrls: [],
    standalone: false
})
export abstract class RccItemTagComponent implements OnChanges {

	// #region viewChildren
	@ViewChild(IonItemSliding)
	private slidingItem? 		: IonItemSliding

	@ViewChild('download')
	private downloadLink? 		: ElementRef<HTMLAnchorElement>
	// #endregion

	// #region getActions parameters
	/**
	 * used to decide which items to show actions for
	 */
	@Input()
	public item? 				: Item 						= null

	/**
	 * used to determine which roles to show actions for
	 */
	@Input()
	public itemActionRoles 		: ItemActionRole[] 			= []

	/**
	 * additional itemActions to consider in {@link ItemService["getActions"]}
	 */
	@Input()
	public extraItemActions		: ItemAction[] 				= []

	/**
	 *  tell {@link ItemService["getActions"]} to skip or consider globally provided actions
	 */
	@Input()
	public skipGlobalActions	: boolean					= false

	/**
	 * shortCut for {@link extraItemActions} = [] and {@link skipGlobalActions} true
	 */
	@Input()
	public noActions			: boolean					= false
	// #endregion

	/**
	 * this forces {@link Mode} 'select'
	 * note: this renders all action related parameters useless
	 */
	@Input()
	public selected?			: boolean 		 			= null

	/**
	 * use 'complex' {@link Mode} instead if 'basic'
	 * note: this only takes effect if EXACTLY 2 actions are shown
	 */
	@Input()
	public forceComplexMode		: boolean					= false

	@Input()
	public iconColor			: string					= undefined

	public actions				: Action[]					= []

	public itemRepresentation?	: ItemRepresentation 		= null
	public itemLabelInjector?	: Injector 					= null
	public itemLabelTemplate?	: TemplateRef<unknown>		= null
	public itemLabelCssClass?	: string					= null

	public constructor(
		public 	itemService				: ItemService,
		public 	actionSheetController	: ActionSheetController, // Should be wrapped in rccActionSheetController or something
		private rccToastController		: RccToastController,
		private	rccTranslationService	: RccTranslationService,
		private	iconsService			: IconsService,
		private	injector				: Injector,
		private viewContainerRef		: ViewContainerRef,
		private changeDetectorRef		: ChangeDetectorRef,
		private actionService			: ActionService
	){}


	public update(): void {

		this.updateActions()
		this.updateItemRepresentation()

	}

	protected get mode(): Mode | null {

		if(!this.item)					return null

		if (this.noActions)				return 'none'

		if(this.selected !== null) 		return 'select'


		if(this.actions.length <= 1)	return 'simple'

		if(this.forceComplexMode)		return 'complex'
		if(this.actions.length === 2)	return 'basic'

		return 'complex'
	}

	protected get hasExactlyOneAction(): boolean {
		return this.actions.length === 1
	}

	public updateActions() : void {
		this.actions	=	[]

		if(!this.item)		return

		if(this.noActions)	return

		const actions	: Action[] 	= 	this.itemService.getActions(
											this.item,
											this.itemActionRoles,
											this.extraItemActions,
											this.skipGlobalActions,
										)

		this.actions 	= actions
	}


	public updateItemRepresentation(): void {

		if(!this.item) {
			this.itemLabelInjector 	= null
			this.itemRepresentation	= null
			this.itemLabelCssClass	= null
			return
		}

		this.itemRepresentation	= this.itemService.getItemRepresentation(this.item)

		const itemClass			: Type<Item<unknown>>	= this.itemRepresentation.itemClass
		const labelComponent	: Type<unknown>			= this.itemRepresentation.labelComponent
		const provider			: StaticProvider		= { provide: itemClass, useValue: this.item }

		this.itemLabelInjector	= Injector.create({ providers: [provider], parent: this.injector })
		this.itemLabelTemplate 	= null

		if(labelComponent.prototype instanceof ExposeTemplateComponent){

			const injector	: Injector	=	Injector.create( { providers: [provider], parent: this.injector })
			const instance	: ExposeTemplateComponent	= 	this.viewContainerRef
															.createComponent(labelComponent, { injector })
															.instance as ExposeTemplateComponent

			this.itemLabelTemplate 	=	instance.itemLabelTemplate || null

		}


	}


	public async execute(action: Action): Promise<void> {
		await this.actionService.execute(action)
		this.changeDetectorRef.markForCheck()
	}


	public async toggleSlidingItem(): Promise<void>{
		if(this.slidingItem === null) return

		const ratio : number = await this.slidingItem.getSlidingRatio()

		if (ratio === 0)	void this.slidingItem.open('end')
		else				void this.slidingItem.close()
	}


	public async showActions(): Promise<void>{

		const buttons : Record<string, unknown>[] = []

		this.actions
		.forEach( (action: Action) => {

			buttons.push({
				text: 		this.rccTranslationService.translate(action.label),
				icon: 		this.iconsService.get(action.icon || 'view'),
				handler:	(): void => void this.execute(action)
			})

		})

		buttons.push({
				text: this.rccTranslationService.translate('META_STORE.ACTIONS.CANCEL'),
				icon: 'close',
				role: 'cancel'
		})

		const actionSheet : HTMLIonActionSheetElement = await this.actionSheetController.create({
			// header: '{{}}'
			buttons
		})

		await actionSheet.present()
	}


	public fallbackMissingActions() : void {
		void this.rccToastController.failure('ITEMS.ACTIONS.MISSING')
	}

	public async onClick(): Promise<unknown> {

		this.updateActions()

		if(this.mode === 'select')		return null

		if(this.actions.length === 0)	return this.noActions || this.fallbackMissingActions()

		if(this.mode === 'simple')		return this.actions[0] && this.execute(this.actions[0])

		if(this.mode === 'basic')		return this.toggleSlidingItem()

		if(this.mode === 'complex')		return this.showActions()

		return null
	}

	public ngOnChanges(): void {
		this.update()
	}
}

type Mode = 'select' | 'simple' | 'basic' | 'complex' | 'none'
