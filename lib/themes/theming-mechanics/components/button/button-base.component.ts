import	{
			Component,
			EventEmitter,
			Input,
			Output,
		}									from '@angular/core'

/**
 * This class is meant to be a base theme component; meaning that every theme
 * should have a component extending this base class.
 *
 * Here's what any button is supposed to do:
 * - Show a label, no description
 * - Use color scheme associated with hierarchical color if provided with color
 *   input ('primary', 'secondary'...). This affects only the text color, not
 *   background
 */
@Component({
    selector: 'rcc-button',
    template: '',
    standalone: false
})
export class RccButtonBaseComponent {
	@Input()
	public type: 'submit' | 'reset' | 'button'

	@Input()
	public disabled: boolean = false

	@Output()
	public clickEvent: EventEmitter<void> = new EventEmitter<void>()
}
