import	{	Type 							}	from '@angular/core'
import	{	RccButtonBaseComponent			}	from '@rcc/themes'
import 	{	ComponentUnitTests				}	from '../components.commons.spec'


// All Units test for RccButton:
function rccButtonTests(componentClass: Type<unknown>) : void {


	it('extends the base component class (RccButtonBaseComponent)', () => {

		const proto : unknown = componentClass.prototype as unknown

		expect(proto).toBeInstanceOf(RccButtonBaseComponent)
	})

}



export const RccButtonComponentTests : ComponentUnitTests = {

	baseComponentClass: 	RccButtonBaseComponent,
	unitTests:				rccButtonTests

}
