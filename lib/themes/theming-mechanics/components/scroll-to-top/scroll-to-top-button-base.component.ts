import	{
			Component,
			ElementRef,
			OnDestroy,
			OnInit
		}								from	'@angular/core'
import	{	HandlerAction	}			from	'@rcc/common/actions'


@Component({
    selector: 'rcc-scroll-to-top-button',
    template: '',
    standalone: false
})

export class RccScrollToTopButtonBaseComponent implements OnInit, OnDestroy {

	public scrollUpAction : HandlerAction = {
		label:		'THEME.SCROLL',
		icon:		'up',
		handler:	 () => this.scrollToTop()
	}

	public scrollElement			: HTMLElement
	public scrollToTopButtonElement	: HTMLElement
	public lastScrollTop			: number = 0
	public opacity					: number = 0
	private scrollDebounceTimeout	: number | null = null

	public constructor(private elementRef: ElementRef) {}

	public ngOnInit(): void {
		this.scrollToTopButtonElement = this.elementRef.nativeElement as HTMLElement

		if (!this.scrollElement)

			setTimeout(() => {
				this.scrollElement	=	this.scrollToTopButtonElement.closest('rcc-full-page')
									||	this.scrollToTopButtonElement.closest('rcc-reduced-full-page')

				this.scrollElement.addEventListener('scroll', () => {
				if (this.scrollDebounceTimeout) clearTimeout(this.scrollDebounceTimeout)

					this.scrollDebounceTimeout = window.setTimeout(() => {
						const currentScrollTop: number = this.scrollElement.scrollTop

						if (currentScrollTop > this.lastScrollTop) this.fadeIn(this.scrollToTopButtonElement)
						else this.fadeOut(this.scrollToTopButtonElement)

						this.lastScrollTop = currentScrollTop
					}, 5)
				})
			}, 50)

	}

	public ngOnDestroy() : void {
		if (this.scrollElement) this.scrollElement.removeEventListener('scroll', () => {})
	}

	public scrollToTop(): void {
		this.scrollElement.scrollTo({ top: 0, behavior: 'smooth' })
	}

	public fadeIn(element: HTMLElement | null): void {
		if (element && this.opacity < 1) {
			this.opacity += 0.2
					element.style.opacity = this.opacity.toString()
		}
	}

	public fadeOut(element: HTMLElement | null): void {
		if (element && this.opacity > 0) {
				this.opacity -= 0.30
					element.style.opacity = this.opacity.toString()
		}
	}

}
