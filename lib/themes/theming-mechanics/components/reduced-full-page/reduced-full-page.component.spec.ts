import	{	Type 							}	from '@angular/core'
import	{	RccReducedFullPageBaseComponent	}	from './reduced-full-page-base.component'
import 	{	ComponentUnitTests				}	from '../components.commons.spec'


// All Units test for RccFullPage:
function rccReducedFullPageTests(componentClass: Type<unknown>) : void {


	it('extends the base component class (RccReducedFullPageBaseComponent)', () => {

		const proto : unknown = componentClass.prototype as unknown

		expect(proto).toBeInstanceOf(RccReducedFullPageBaseComponent)
	})

}



export const RccReducedFullPageComponentTests : ComponentUnitTests = {

	baseComponentClass: 	RccReducedFullPageBaseComponent,
	unitTests:				rccReducedFullPageTests

}
