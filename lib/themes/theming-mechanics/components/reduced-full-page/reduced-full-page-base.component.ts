import	{	Location				}	from '@angular/common'
import	{
			Component,
			Input,
			Output,
			EventEmitter,
		}								from '@angular/core'
import	{
			Router
		}								from '@angular/router'
import	{	RccModalController		}	from '@rcc/common/modals-provider'
import	{	Action, ActionService	}	from '@rcc/common/actions'
import	{	IconName				}	from '@rcc/common/ui-components/icons/icon-names'
import	{	UserCanceledError		}	from '@rcc/core'

@Component({
	selector:	'rcc-reduced-full-page',
	template:	'',
	standalone:	true,
})
export class RccReducedFullPageBaseComponent {

	/**
	 * heading and subHeading(optional) for the header bar.
	 * The icon can also be chosen.
	 * mainContentDescription is optional. This is the text in the main content area above the item/action buttons
	 * buttonPlacement can be chosen either left or right, default is right.
	 */

	@Input()
	public heading: string

	@Input()
	public subHeading?: string

	@Input()
	public exitIcon: IconName = 'close'

	@Input()
	public mainContentDescription?: string

	@Input()
	public exitAction: 'back' | 'home' | 'modal-dismiss' | Action = undefined

	@Input()
	public buttonPlacement: 'start' | 'end' = 'end'

	protected exit(): void {

		this.exitEvent.emit()

	}

	public get action(): Action {

		if(this.exitAction === 'back') return {
			icon: 'back',
			label: 'BACK',
			handler: () => this.location.back()
		}
		if(this.exitAction === 'home') return {
			icon: 'close',
			label: 'CLOSE',
			path: '/'
		}
		if(this.exitAction === 'modal-dismiss') return {
			icon: 'close',
			label: 'CANCEL',
			handler: () => this.rccModalController.dismiss(new UserCanceledError())
		}

		if(typeof this.exitAction !== 'string') return this.exitAction

	}

	@Output()
	public exitEvent: EventEmitter<unknown> = new EventEmitter()


	public constructor(
		private location: Location,
		protected router: Router,
		protected actionService: ActionService,
		protected rccModalController: RccModalController
	) {}

}
