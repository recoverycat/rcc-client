import	{	Type 							}	from '@angular/core'
import	{	RccRadioSelectBaseComponent		}	from './radio-select-base.component'
import 	{	ComponentUnitTests				}	from '../components.commons.spec'

function rccRadioSelectTests(componentClass: Type<unknown>): void {
	it('extends the base component class (RccRadioSelectBaseComponent)', () => {
		const proto : unknown = componentClass.prototype as unknown

		expect(proto).toBeInstanceOf(RccRadioSelectBaseComponent)
	})
}

export const RccRadioSelectTests: ComponentUnitTests = {
	baseComponentClass: RccRadioSelectBaseComponent,
	unitTests:			rccRadioSelectTests,
}
