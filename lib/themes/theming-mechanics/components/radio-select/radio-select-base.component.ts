import { Component, Input } from '@angular/core'
import { ControlValueAccessor, ReactiveFormsModule } from '@angular/forms'
import { Required } from '@rcc/common/decorators'

export interface RccSelectOption<T> {
	value: T,
	label?: string,
	translations?: Record<string, string>
}

export type radioButtonSize = 'small' | 'large'


@Component({
    selector: 'rcc-radio-select',
    template: '',
    imports: [ReactiveFormsModule]
})
export class RccRadioSelectBaseComponent<T> implements ControlValueAccessor {
	public value: T
	public writeValue(value: T): void {
		this.value = value
	}

	public onChange: (value: T) => void = () => {}
	public registerOnChange(fn: (value: T) => void): void {
		this.onChange = fn
	}

	public onTouched: () => void = () => {}
	public registerOnTouched(fn: () => void): void {
		this.onTouched = fn
	}

	protected isDisabled: boolean
	public setDisabledState(isDisabled: boolean): void {
		this.isDisabled = isDisabled
	}

	@Input()
	public size: radioButtonSize = 'small'

	@Input()
	public options: RccSelectOption<T>[]

	@Input() @Required
	public groupLabel: string

	@Input()
	public describedBy: string

	@Input()
	public greyedOut: boolean = false

}
