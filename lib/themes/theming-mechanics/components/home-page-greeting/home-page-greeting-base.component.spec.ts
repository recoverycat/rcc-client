import	{	Type 								}	from '@angular/core'
import	{	RccHomePageGreetingBaseComponent	}	from '@rcc/themes'
import 	{	ComponentUnitTests					}	from '../components.commons.spec'

function rccHomePageGreetingTests(componentClass: Type<unknown>) : void {
	it('extends the base component class (RccHomePageGreetingComponent)', () => {
		const proto : unknown = componentClass.prototype as unknown

		expect(proto).toBeInstanceOf(RccHomePageGreetingBaseComponent)
	})
}

export const RccHomePageGreetingTests : ComponentUnitTests = {
	baseComponentClass: 	RccHomePageGreetingBaseComponent,
	unitTests:				rccHomePageGreetingTests
}
