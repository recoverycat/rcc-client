import	{	Component, Type					}	from '@angular/core'
import	{	RccQuickActionListBaseComponent	}	from './quick-action-list-base.component'
import	{	ComponentUnitTests				}	from '../components.commons.spec'

function rccQuickActionListTests(componentClass: Type<Component>): void {
it('extends the base component class (RccQuickActionListComponent)', () => {
		const prototype: unknown = componentClass.prototype as unknown

		expect(prototype).toBeInstanceOf(RccQuickActionListBaseComponent)
	})
}

export const RccQuickActionListComponentTests: ComponentUnitTests = {
	baseComponentClass	: RccQuickActionListBaseComponent,
	unitTests			: rccQuickActionListTests,
}
