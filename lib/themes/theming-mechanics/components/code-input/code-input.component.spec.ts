import	{	Type 							}	from '@angular/core'
import	{	RccCodeInputBaseComponent		}	from './code-input-base.component'
import 	{	ComponentUnitTests				}	from '../components.commons.spec'

function rccCodeInputTests(componentClass: Type<unknown>): void {
	it('extends the base component class (RccRadioSelectBaseComponent)', () => {
		const proto : unknown = componentClass.prototype as unknown

		expect(proto).toBeInstanceOf(RccCodeInputBaseComponent)
	})
}

export const RccCodeInputComponentTests: ComponentUnitTests = {
	baseComponentClass: RccCodeInputBaseComponent,
	unitTests:			rccCodeInputTests,
}
