import	{
			Component,
			ChangeDetectorRef,
			Input,
		}										from '@angular/core'
import	{	ControlValueAccessor			}	from '@angular/forms'

import	{
			Action,
			ActionService,
		}										from '@rcc/common/actions'

import	{
			ColorVariants,
			RccColorService,
		}										from '../../colors'
import { uniqueId } from '@rcc/core'

@Component({
	selector:		'rcc-toggle',
	template:		'',
	standalone:		true,
})
export class RccToggleBaseComponent implements ControlValueAccessor {

	public onToggleChange(): void {
		this.checked = !this.checked
		this.onChange(this.checked)
		this.onTouched()
	}

	@Input()
	public usage			: string

	@Input()
	public label			: string

	@Input()
	public action			: Action | null

	@Input()
	public checked			: boolean = false

	@Input()
	public disabled			: boolean = false

	@Input()
	public toggleOrCheckbox : 'toggle' | 'checkbox' = 'toggle'		// as this component was used solely for toggle buttons, it defaults to 'toggle'

	public colorVariants	: ColorVariants	| null	= null

	protected checkboxId 	: string = uniqueId('rcc-checkbox')

	public constructor(
		protected actionService		: ActionService,
		protected changeDetectorRef	: ChangeDetectorRef,
		protected rccColorService	: RccColorService
	){
	}

	public writeValue(value: boolean): void {
		this.checked = value
	}

	protected onChange: (value: boolean) => void = () => undefined
	public registerOnChange(fn: (value: boolean) => void): void {
		this.onChange = fn
	}

	protected onTouched: () => void = () => undefined
	public registerOnTouched(fn: () => void): void {
		this.onTouched = fn
	}
}
