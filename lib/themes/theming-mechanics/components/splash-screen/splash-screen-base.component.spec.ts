import	{	Type							}	from	'@angular/core'
import	{	RccSplashScreenBaseComponent	}	from	'./splash-screen-base.component'
import	{	ComponentUnitTests				}	from	'../components.commons.spec'

function rccSplashScreenTests(componentClass: Type<unknown>): void {
	it('extends the base component class (RccSplashScreenBaseComponent)', () => {
		const prototype: unknown = componentClass.prototype as unknown

		expect(prototype).toBeInstanceOf(RccSplashScreenBaseComponent)
	})
}

export const RccSplashScreenBaseComponentTests: ComponentUnitTests = {
	baseComponentClass: RccSplashScreenBaseComponent,
	unitTests: rccSplashScreenTests,
}
