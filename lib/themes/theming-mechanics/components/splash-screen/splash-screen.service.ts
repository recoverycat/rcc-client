import { Injectable } from '@angular/core'
import { BehaviorSubject, Observable } from 'rxjs'

@Injectable({
	providedIn: 'root'
})
export class RccSplashScreenService {
	private _showSplashScreen	: BehaviorSubject<boolean>	= new BehaviorSubject<boolean>(true)
	public showSplashScreen$	: Observable<boolean>		= this._showSplashScreen.asObservable()

	public shouldShowSplashScreen(): boolean {
		return sessionStorage.getItem('showSplashScreen') !== 'false'
	}

	public setShowSplashScreen(show: boolean): void {
		sessionStorage.setItem('showSplashScreen', show.toString())
		this._showSplashScreen.next(show)
	}

	public resetSplashScreen(): void {
		sessionStorage.removeItem('showSplashScreen')
	}
}
