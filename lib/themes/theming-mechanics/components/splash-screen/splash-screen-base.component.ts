import	{
			AfterViewInit,
			ChangeDetectorRef,
			Component,
			OnInit
		}								from '@angular/core'
import	{	RccBuildInfoService		}	from '@rcc/common/build-info/build-info.service'
import	{	RccSplashScreenService	}	from './splash-screen.service'
import { Observable } from 'rxjs'

export interface variantColorConfig {
	topColor: string
	bottomColor: string
}

@Component({
	selector:	'rcc-splash-screen',
	template:	'',
	standalone:	true,

})
/**
 * This component contains the splash screen that is shown during application start.
 * To prohibit it from being shown multiple times, even when just switching to another
 * subpage of the app, it makes use of the Browser's sessionStorage with the variable
 * 'showSplashScreen'.
 * To reduce the amount of files, two SVG files (logo and background) are manipulated via
 * interpolation to show the correct theme colors of either HCP or PAT as well as
 * for the different device sizes. For this the RccBuildInfoService is used to fetch the flavor of the current build.
 * This is not yet the final version of the animation, which will need a bit more time
 * due to its slight complexity.
 */
export class RccSplashScreenBaseComponent implements AfterViewInit, OnInit {

	public constructor(
		private readonly rccBuildInfoService: RccBuildInfoService,
		private changeDetectorRef			: ChangeDetectorRef,
		private document					: Document,
		private splashScreenService			: RccSplashScreenService

	){
		this.variant = rccBuildInfoService.buildInfos.flavor

		if (this.splashScreenNotShownYet) sessionStorage.setItem('showSplashScreen', 'true')
	}


	// #region theming

	public variant			: string
	public selectedVariant	: variantColorConfig

	public hcpColors		: variantColorConfig
	public patColors		: variantColorConfig


	private initializeColors(): void {
		this.hcpColors = {
			topColor: this.selectHexColorFromTheme('--rcc-rgb-violet-default'),
			bottomColor: this.selectHexColorFromTheme('--rcc-rgb-orange-default')
		}

		this.patColors = {
			topColor: this.selectHexColorFromTheme('--rcc-rgb-green-default'),
			bottomColor: this.selectHexColorFromTheme('--rcc-rgb-yellow-default')
		}
	}

	public selectVariant(): variantColorConfig {
		return (this.variant === 'hcp' || this.variant === 'hcp-med')
			? this.hcpColors
			: this.patColors
	}

	public selectHexColorFromTheme(cssVariable: string): string {

		const rgb: string = getComputedStyle(document.documentElement)
			.getPropertyValue(cssVariable).trim()

		const [r, g, b] = rgb.split(',').map(Number)

		const toHex: (component: number) => string = (component: number) => {
			const hex: string = component.toString(16)
			return hex.length === 1 ? '0' + hex : hex
		}

		return `#${toHex(r)}${toHex(g)}${toHex(b)}`.toUpperCase()
	}


	// #endregion theming

	// #region animation

	public showSplashScreen$: Observable<boolean>

	public splashScreenNotShownYet: boolean = sessionStorage.getItem('showSplashScreen') === null

	public get showSplashScreen(): string {
		return sessionStorage.getItem('showSplashScreen')
	}
	public set showSplashScreen(boolean: boolean) {
		sessionStorage.setItem('showSplashScreen', JSON.stringify(boolean))
		this.changeDetectorRef.detectChanges()
	}

	public windowWidth	: number = window.innerWidth
	public windowHeight	: number = window.innerHeight
	public logoSizeRatio: number = 3



	private showSplashScreenAnimation(): void {

		setTimeout(() => {
			this.hideSplashScreen()
		}, 1800)
	}

	private hideSplashScreen(): void {
		const splashScreen: HTMLElement = document.querySelector('.splash-screen')
		if (splashScreen) splashScreen.classList.add('splash-screen-shown')

		this.splashScreenService.setShowSplashScreen(false)
	}


	// #endregion animation

	public ngOnInit(): void {

		this.initializeColors()

		this.selectedVariant = this.selectVariant()

		this.showSplashScreen$ = this.splashScreenService.showSplashScreen$

		if (this.splashScreenService.shouldShowSplashScreen()) this.showSplashScreenAnimation()
		else this.hideSplashScreen()


	}

	public ngAfterViewInit(): void {
		this.changeDetectorRef.detectChanges()

		if (sessionStorage.getItem('showSplashScreen') === 'false') {
			const splashScreen: HTMLElement = this.document.querySelector('.splash-screen')
			if (splashScreen) splashScreen.classList.add('splash-screen-shown')
		}
	}

}
