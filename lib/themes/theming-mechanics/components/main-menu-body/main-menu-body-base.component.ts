import 	{
			Component,
			Inject,
			Optional,
			Type
		} 										from '@angular/core'

import 	{
			Action,
			ActionService,
			CustomAction,
			DownloadAction,
			HandlerAction,
			hasDescription,
			hasNotification,
			isCustomAction,
			isDownloadAction,
			isHandlerAction,
			isPathAction,
			PathAction,
			WithDescription,
			WithNotification
		} 										from '@rcc/common/actions'

import 	{
			MainMenuEntry,
			MAIN_MENU_CONFIG,
			MAIN_MENU_ENTRIES,
			MAIN_MENU_ENTRIES_META,
		}										from '@rcc/common/main-menu'


type TypeGuard<T extends Action | CustomAction> = (action: Action | CustomAction) => action is T

@Component({
    selector: 'rcc-main-menu-body',
    template: '',
    standalone: false
})
export class RccMainMenuBodyBaseComponent {
	public isPathAction		: TypeGuard<PathAction>
							= isPathAction

	public isHandlerAction	: TypeGuard<HandlerAction>
							= isHandlerAction

	public isDownloadAction	: TypeGuard<DownloadAction>
							= isDownloadAction

	public isCustomAction	: TypeGuard<CustomAction>
							= isCustomAction

	public hasDescription	: TypeGuard<Action & WithDescription>
							= hasDescription

	public hasNotification	: TypeGuard<Action & WithNotification>
							= hasNotification

	public actions			: (Action | CustomAction)[]
	public metaActions		: (Action | CustomAction)[]
	public component		: Type<unknown>[]

	public constructor(
		@Optional() @Inject(MAIN_MENU_ENTRIES)
		public entries				: MainMenuEntry[],

		@Optional() @Inject(MAIN_MENU_ENTRIES_META)
		public metaEntries			: MainMenuEntry[],

		@Optional() @Inject(MAIN_MENU_CONFIG)
		public config				: Record<string, unknown>,

		protected actionService		: ActionService,
	) {
		this.entries 		= 	entries ?? []
		this.metaEntries 	= 	metaEntries ?? []

		this.actions 		= 	this.entries.map((entry: MainMenuEntry) =>
									typeof entry === 'function' ? { component: entry } : entry
								)

		this.metaActions 	= 	this.metaEntries.map((entry: MainMenuEntry) =>
									typeof entry === 'function' ? { component: entry } : entry
								)
	}

	protected execute(action: Action): void {
		void this.actionService.execute(action)
	}
}
