import	{	Type 							}	from '@angular/core'
import	{	RccActionButtonBaseComponent	}	from '@rcc/themes'
import 	{	ComponentUnitTests				}	from '../components.commons.spec'


// All Units test for RccActionButton:
function rccActionButtonTests(componentClass: Type<unknown>) : void {


	it('extends the base component class (RccActionButtonBaseComponent)', () => {

		const proto : unknown = componentClass.prototype as unknown

		expect(proto).toBeInstanceOf(RccActionButtonBaseComponent)
	})

}



export const RccActionButtonComponentTests : ComponentUnitTests = {

	baseComponentClass: 	RccActionButtonBaseComponent,
	unitTests:				rccActionButtonTests

}
