import	{
			Component,
			Input,
			ChangeDetectorRef,
			OnChanges,
			SimpleChanges,
			OnDestroy
		}									from '@angular/core'

import	{
			Action,
			ActionService,
			hasDescription,
			hasChildComponent,
			hasNotification,
			WithDescription,
			WithNotification,
			WithChildComponent,
			isPathAction,
			PathAction,
		}									from '@rcc/common/actions'

import	{
			Subscription
		}									from 'rxjs'

export type ButtonSize = 'full' | 'medium' | 'small' | 'none'

/**
 * This class is meant to be a base theme component; meaning that every theme
 * should have a component extending this base class.
 *
 * Here's what any action button is supposed to do:
 * - If the action has a notification Observable<boolean|number>, always show
 *   the most recent value.
 * - Show label of the action
 * - Show description of the action if present
 * - When clicked: execute the provided action {@link #action}, for all types
 *   of actions (see {@link Action}, {@link ActionService.execute)} will help).
 * - Recognize `.failureMessage`, `.successMessage` and `.confirmMessage`
 *   properties of provided action.
 *   ({@link ActionService.execute)} will help)
 * - Use color scheme associated with hierarchical color if provided with color
 *   input ('primary', 'secondary'...)
 * - Use color scheme associated with the `.category` property of the action if
 *   no other color is provided ('share', 'analyze'...')
 * - Show `.childComponent` if present on action
 */
@Component({
    selector: 'rcc-action-button',
    template: '',
    standalone: false
})
export class RccActionButtonBaseComponent implements OnChanges, OnDestroy {


	public notificationValue 	: boolean | number 		= false

	protected notificationSub	: Subscription | null	= null



	@Input()
	public action: Action | null

	@Input()
	public hideCard: boolean = false

	/**
	 * Full-sized buttons have a colored area surrounding them
	 * that contains the description.
	 * Medium-sized buttons have no colored area and no description.
	 */
	@Input()
	public buttonSize : ButtonSize = 'full'

	public isDisabled(): boolean {
		if (!this.action || typeof this.action.disabled !== 'function') return false
		return this.action.disabled()
	}

	public constructor(
		protected actionService		: ActionService,
		protected changeDetectorRef	: ChangeDetectorRef,
	){}


	public async execute(): Promise<void> {
		await this.actionService.execute(this.action)
		this.changeDetectorRef.markForCheck()
	}


	public hasDescription(action: Action) 		: action is Action & WithDescription 	{ return hasDescription(action) 	}
	public hasChildComponent(action: Action)	: action is Action & WithChildComponent { return hasChildComponent(action) 	}
	public hasNotification(action: Action) 		: action is Action & WithNotification	{ return hasNotification(action) 	}

	protected onActionChange() : void {

		this.notificationValue 	= null

		if(this.notificationSub) 			this.notificationSub.unsubscribe()

		if(!this.action) 					return undefined
		if(!hasNotification(this.action)) 	return undefined

		this.notificationSub 	= this.action.notification.subscribe( value => this.notificationValue = value)

	}

	public ngOnChanges(changes: SimpleChanges): void {

		if(changes.action)
			this.onActionChange()

	}

	public ngOnDestroy() : void {
		this.notificationSub?.unsubscribe()
	}

	protected isLink(action: Action): action is PathAction {
		return isPathAction(action)
	}
}
