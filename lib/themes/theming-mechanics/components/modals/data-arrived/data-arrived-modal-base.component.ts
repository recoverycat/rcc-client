import { Component, Input } from '@angular/core'
import { Action } from '@rcc/common'

@Component({
	selector	: 'rcc-data-arrived-modal',
	template	: '',
	standalone	: true,
})
export class RccDataArrivedModalBaseComponent {
	@Input()
	public action: Action
}
