import	{	Component, Type						}	from '@angular/core'
import	{	RccDataArrivedModalBaseComponent	}	from './data-arrived-modal-base.component'
import	{	ComponentUnitTests					}	from '../../components.commons.spec'

function rccDataArrivedModalTests(componentClass: Type<Component>): void {
	it('extends the base component class (RccDataArrivedModalBaseComponent)', () => {
		const prototype: unknown = componentClass.prototype as unknown

		expect(prototype).toBeInstanceOf(RccDataArrivedModalBaseComponent)
	})
}

export const RccDataArrivedModalTests: ComponentUnitTests = {
	baseComponentClass	: RccDataArrivedModalBaseComponent,
	unitTests			: rccDataArrivedModalTests,
}
