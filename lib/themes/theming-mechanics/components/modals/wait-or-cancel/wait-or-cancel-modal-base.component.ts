import { Component, Input 	} from '@angular/core'
import { RccModalController } from '@rcc/common/modals-provider'
import { UserCanceledError	} from '@rcc/core'

@Component({
	selector: 	'rcc-wait-or-cancel-modal',
	template: 	'',
	standalone: true
})
export class RccWaitOrCancelModalBaseComponent<T=unknown> {

	@Input()
	public message			: string

	@Input()
	public contextText		: string

	@Input()
	public icon				: string

	@Input()
	public direction		: 'share' | 'receive'

	@Input()
	public set promise(p : Promise<T> ){
		void p.then(
				result	=> this.handleResolution(result),
				e 		=> this.handleRejection(e)
			)
	}

	public constructor(
		protected rccModalController: RccModalController
	){}

	public handleRejection(e: unknown) : void {

		this.rccModalController.dismiss(
			e instanceof Error
			?	e
			:	new Error('RccWaitOrCancelModalBaseComponent awaited promise was rejected', { cause:e })
		)
	}

	public handleResolution(result: unknown) : void {
		this.rccModalController.dismiss(result)
	}



	public cancel() : void {
		this.rccModalController.dismiss(new UserCanceledError('RccWaitOrCancelModalBaseComponent'))
	}
}
