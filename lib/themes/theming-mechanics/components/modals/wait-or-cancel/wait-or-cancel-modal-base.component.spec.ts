import	{	Component, Type					}	from '@angular/core'
import	{	RccWaitOrCancelModalBaseComponent }	from './wait-or-cancel-modal-base.component'
import	{	ComponentUnitTests				}	from '../../components.commons.spec'

function rccWaitOrCancelModalTests(componentClass: Type<Component>): void {
	it('extends the base component class (RccMainMenuBodyBaseComponent)', () => {
		const prototype: unknown = componentClass.prototype as unknown

		expect(prototype).toBeInstanceOf(RccWaitOrCancelModalBaseComponent)
	})
}

export const RccWaitOrCancelModalTests: ComponentUnitTests = {
	baseComponentClass	: RccWaitOrCancelModalBaseComponent,
	unitTests			: rccWaitOrCancelModalTests,
}
