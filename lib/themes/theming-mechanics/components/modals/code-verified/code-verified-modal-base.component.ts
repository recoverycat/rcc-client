import	{	Component			}	from '@angular/core'
import	{
			Action,
		}							from '@rcc/common'
import	{	RccModalController	}	from '@rcc/common/src/modals-provider'
import	{	UserCanceledError	}	from '@rcc/core'

@Component({
	selector	: 'rcc-code-verified-modal',
	template	: '',
	standalone	: true,
})
export class RccCodeVerifedModalBaseComponent {

	public constructor(protected modalController: RccModalController) {}

	public action: Action = {
		icon: 'smiley',
		label: 'TRANSMISSION_REQUESTS.TAN.VERIFIED.LABEL',
		description: 'TRANSMISSION_REQUESTS.TAN.VERIFIED.DESCRIPTION',
		handler: () => this.modalController.dismiss()
	}

	protected cancel(): void {
		this.modalController.dismiss(new UserCanceledError('RccDataArrivedModalComponent'))
	}
}
