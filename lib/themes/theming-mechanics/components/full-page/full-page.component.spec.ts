import	{
			TestBed,
			ComponentFixture,
		}										from '@angular/core/testing'
import	{
			Type,
			Component
}												from '@angular/core'
import	{	RccFullPageBaseComponent		}	from '@rcc/themes'
import 	{	ComponentUnitTests				}	from '../components.commons.spec'
import	{	RccPublicRuntimeConfigService	}	from '@rcc/common'
import { RouterModule } from '@angular/router'


// All Units test for RccFullPage:
function rccFullPageTests(componentClass: Type<unknown>) : void {

	const mockRuntimeService: Partial<RccPublicRuntimeConfigService> = {
		get: (): Promise<object> => Promise.resolve({})
	}
	
	
	it('extends the base component class (RccFullPageBaseComponent)', () => {

		const proto : unknown = componentClass.prototype as unknown

		expect(proto).toBeInstanceOf(RccFullPageBaseComponent)
	})

	it('uses content projection for heading, main and footer', async () => {

		@Component({
    template: `
							<rcc-full-page>
								<div headingContent> 	Test heading content 	</div>
								<div mainContent> 		Test main content 		</div>
								<div footerContent> 	Test footer content 	</div>
							</rcc-full-page>
						`,
    standalone: false
})
		class TestComponent{}

		await TestBed.configureTestingModule({
			imports:[
				componentClass,
				RouterModule.forRoot([]),
			],
			declarations: [
				TestComponent
			],
			providers: [
				{ provide: RccPublicRuntimeConfigService, useValue: mockRuntimeService },
			]
		}).compileComponents()

		const fixture			: ComponentFixture<TestComponent>
								= TestBed.createComponent(TestComponent)

		fixture.detectChanges()

		await fixture.whenStable()

		const nativeElement		: HTMLElement
								= fixture.nativeElement as HTMLElement

		const textContent		: string
								= nativeElement.textContent.trim()

		expect(textContent).toContain('Test heading content')
		expect(textContent).toContain('Test main content')
		expect(textContent).toContain('Test footer content')

	})

	it('should show a banner if applicable', async () => {

		@Component({
    template: `
							<rcc-full-page
								[bannerMarkdown] = "'banner test content'"
							>
							</rcc-full-page>
						`,
    standalone: false
})
		class TestComponent{}

		await TestBed.configureTestingModule({
			imports:[
				componentClass,
				RouterModule.forRoot([]),
			],
			declarations: [
				TestComponent
			],
			providers: [
				{ provide: RccPublicRuntimeConfigService, useValue: mockRuntimeService },
			]
		}).compileComponents()

		const fixture			: ComponentFixture<TestComponent>
								= TestBed.createComponent(TestComponent)

		fixture.detectChanges()

		await fixture.whenStable()


		const nativeElement		: HTMLElement
								= fixture.nativeElement as HTMLElement


		const textContent		: string
								= nativeElement.textContent.trim()


		expect(textContent).toContain('banner test content')

	})

}



export const RccFullPageComponentTests : ComponentUnitTests = {

	baseComponentClass: 	RccFullPageBaseComponent,
	unitTests:				rccFullPageTests

}
