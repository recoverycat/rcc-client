import	{	Type							}	from '@angular/core'
import	{	RccPullDownSelectBaseComponent	}	from '@rcc/themes'
import	{	ComponentUnitTests				}	from '../components.commons.spec'
import	{
			ComponentFixture,
			TestBed,
		}										from '@angular/core/testing'
import	{
			RccTranslationService,
			TranslationsModule
		}										from '@rcc/common'
import	{	Option							}	from './pull-down-select.commons'

function rccPullDownSelectTests<C extends RccPullDownSelectBaseComponent<unknown>>(componentClass: Type<C>): void {
	it('extends the base component class (RccPullDownSelectComponent)', () => {
		const prototype: unknown = componentClass.prototype as unknown

		expect(prototype).toBeInstanceOf(RccPullDownSelectBaseComponent)
	})

	let component			: C						= undefined
	let fixture				: ComponentFixture<C>	= undefined

	let translationService	: RccTranslationService = undefined
	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [TranslationsModule],
		})

		fixture = TestBed.createComponent(componentClass)

		translationService = TestBed.inject(RccTranslationService)

		component = fixture.componentInstance
	})

	it('should be able to set the options and display them in the template', () => {
		component.expanded = true

		const newOptions : Option<string>[] = [ { 'value': 'rcc-curated-indication-adhd', 'label': 'ADHS (18)' }, { 'value': 'rcc-symptom-area-leg-restlessness', 'label': 'Beinunruhe (2)' }, { 'value': 'rcc-category-depression', 'label': 'Depression (49)' }, { 'value': 'rcc-symptom-area-heart-palpitations', 'label': 'Herzklopfen (2)' } ]
		component.options = newOptions

		fixture.detectChanges()

		// Check that the option labels appear somewhere in the template
		const element : HTMLElement = fixture.nativeElement as HTMLElement
		const textContent : string = element.textContent

		const labels : string[] = newOptions.map(option => translationService.translate(option))

		labels.forEach(label =>

			expect(textContent).toContain(label)

		)
	})

	it('should be possible to set a comboboxLabel as aria-label', () => {
		const newComboboxLabel : string = 'New combobox label'
        component.comboboxLabel = newComboboxLabel

		fixture.detectChanges()

		const element : HTMLElement = fixture.nativeElement as HTMLElement

		const ariaLabelled : NodeListOf<Element> = element.querySelectorAll('[aria-label]')
		const ariaLabels : string[] = Array.from(ariaLabelled).map(el => el.getAttribute('aria-label'))

		expect(ariaLabels).toContain(newComboboxLabel)
	})

}

export const RccPullDownSelectComponentTests: ComponentUnitTests = {
	baseComponentClass	: RccPullDownSelectBaseComponent,
	unitTests			: rccPullDownSelectTests,
}
