import	{
			Component,
			Input
		}								from '@angular/core'
import	{	ControlValueAccessor	}	from '@angular/forms'
import 	{ 	BehaviorSubject 		} 	from 'rxjs'
import 	{
			FilterFn,
			Option,
			labelFilterFn,
			OnChangeOnOption as OnChangeModeOption
		} 								from './pull-down-select.commons'


/**
 * This component contains a drop down list with optional search functionality
 */
@Component({
    selector: 'rcc-pull-down-select',
    template: '',
    standalone: false
})
export abstract class RccPullDownSelectBaseComponent<T> implements ControlValueAccessor {
	protected value: T
	public writeValue(value: T): void {
		this.value = value
	}

	protected onChange: (value: T) => void = () => undefined
	public registerOnChange(fn: (value: T) => void): void {
		this.onChange = fn
	}

	protected onTouched: () => void = () => undefined
	public registerOnTouched(fn: () => void): void {
		this.onTouched = fn
	}

	/**
	 * Disable the search field
	 */
	protected isDisabled: boolean
	public setDisabledState(isDisabled: boolean): void {
		this.isDisabled = isDisabled
	}

	protected option$: BehaviorSubject<Option<T>[]> = new BehaviorSubject<Option<T>[]>([])

	@Input()
	public set options(value: Option<T>[]) {
		this.option$.next(value)
	}

	public expanded : boolean = false


	/**
	 * Use this to set a filter function to filter options by the text fields input.
	 * It will be called whenever user enters something into the search field. See
	 * {@link FilterFn} for more info. The default filter function will return all
	 * options whose labels contain the typed in string.
	 */
	@Input()
	public filterFn: FilterFn<T> = labelFilterFn


	/**
	 * This determines when the onChange method is called. See {@link OnChangeModeOption}
	 * for more information.
	 */
	@Input()
	public onChangeMode: OnChangeModeOption = 'select'

	@Input()
	public unselectedText: string = ''

	@Input()
	public comboboxLabel: string

	@Input()
	public allowTextInput: boolean = true

	@Input()
	public enableHovering: boolean = false

	@Input()
	public clearInputField: boolean = false

}
