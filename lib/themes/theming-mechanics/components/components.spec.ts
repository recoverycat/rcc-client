import 	{	Type 							}	from '@angular/core'
import	{	getClassName					}	from '@rcc/core'
import	{	ComponentUnitTests				}	from './components.commons.spec'
import	{	RccActionButtonComponentTests	}	from './action-button/action-button.component.spec'
import	{	RccButtonComponentTests			}	from './button/button.component.spec'
import	{	RccPillButtonComponentTests		}	from './pill-button/pill-button.component.spec'
import	{	RccCodeInputComponentTests		}	from './code-input/code-input.component.spec'
import	{	RccFullPageComponentTests		}	from './full-page/full-page.component.spec'
import	{
			RccReducedFullPageComponentTests
		}										from './reduced-full-page/reduced-full-page.component.spec'
import	{	RccMainMenuBodyComponentTests	}	from './main-menu-body/main-menu-body-base.component.spec'
import	{	RccHomePageGreetingTests		}	from './home-page-greeting/home-page-greeting-base.component.spec'
import	{	RccWaitOrCancelModalTests		}	from './modals/wait-or-cancel/wait-or-cancel-modal-base.component.spec'
import	{	RccCodeVerifiedModalTests		}	from './modals/code-verified/code-verified-modal-base.component.spec'
import	{	RccDataArrivedModalTests		}	from './modals/data-arrived/data-arrived-modal.component.spec'
import	{	RccRadioSelectTests				}	from './radio-select/radio-select.component.spec'
import	{	RccTextareaComponentTests		}	from './textarea/textarea.component.spec'
import	{
			RccPullDownSelectComponentTests
		}										from './pull-down-select/pull-down-select.component.spec'
import	{
			RccQuickActionListComponentTests
		}										from './quick-action-list/quick-action-list-base.component.spec'
import  {
			RccDaysInWeekSelectorComponentTest
		} 										from './days-in-week-selector/days-in-week-selector.component.spec'
import	{
			RccToggleBaseComponentTests
		}										from './toggle/toggle-base.component.spec'
import	{
			RccTimeSelectComponentTests
		}										from './time-select/time-select.component.spec'

import	{
			RccEditTextComponentTests
		}										from './edit-text/edit-text.component.spec'
import	{
			RccEditTextLabelBaseDirectiveTests
		}										from './edit-text-label/edit-text-label.directive.spec'
import 	{
			RccDatePickerComponentTests
		}										from './date-picker/date-picker.component.spec'
import	{	RccSegmentTests					}	from './segment/segment.component.spec'
import	{	RccTextInputComponentTests		}	from './text-input/text-input.component.spec'
import	{	RccItemListComponentTests		}	from './item-list/item-list-base.component.spec'
import	{
			RccScrollToTopButtonBaseComponentTests
		}										from './scroll-to-top/scroll-to-top-button-base.component.spec'
import	{
			RccSplashScreenBaseComponentTests
		}										from './splash-screen/splash-screen-base.component.spec'


import * as baseComponents 						from './index'



const baseComponentClasses 	: unknown[]				= Object.values(baseComponents)


const componentsTestEntries	: ComponentUnitTests[]	= [
	RccActionButtonComponentTests,
	RccButtonComponentTests,
	RccPillButtonComponentTests,
	RccCodeInputComponentTests,
	RccEditTextComponentTests,
	RccEditTextLabelBaseDirectiveTests,
	RccFullPageComponentTests,
	RccMainMenuBodyComponentTests,
	RccHomePageGreetingTests,
	RccSegmentTests,
	RccWaitOrCancelModalTests,
	RccDataArrivedModalTests,
	RccCodeVerifiedModalTests,
	RccRadioSelectTests,
	RccTextareaComponentTests,
	RccPullDownSelectComponentTests,
	RccQuickActionListComponentTests,
	RccDaysInWeekSelectorComponentTest,
	RccToggleBaseComponentTests,
	RccTimeSelectComponentTests,
	RccDatePickerComponentTests,
	RccTextInputComponentTests,
	RccReducedFullPageComponentTests,
	RccItemListComponentTests,
	RccScrollToTopButtonBaseComponentTests,
	RccSplashScreenBaseComponentTests

]

/**
 * Checks if one class extends another. Returns ``false``, if one of the parameters is not a
 * class/function.
 */
function isClassExtension(x: unknown,y: unknown): boolean {
	if(typeof x !== 'function') return false
	if(typeof y !== 'function') return false

	const proto : unknown = x.prototype

	return proto instanceof y
}

/**
 * Checks if a given class/function extends on of the base component classes.
 * @returns the extended base component class or undefined.
 */
function findBaseClass(x: unknown): Type<unknown> {

	if(typeof x !== 'function') return undefined

	return baseComponentClasses.find( (baseClass: unknown) => isClassExtension(x, baseClass) ) as Type<unknown>
}




/**
 * Takes all exports of a module, looks for extensions of base component
 * classes. Checks if every base component class has an extension and runs all
 * unit test for the extensions depending on which base class they extend.
 */
export function testComponents(moduleExports: Record<string,unknown>) : void {


	const componentClasses: Type<unknown>[]	= 	Object.values(moduleExports).filter(findBaseClass) as Type<unknown>[]


	describe('Set of components', () => {

		// Check if all components are present in the theme:
		baseComponentClasses.forEach( (baseComponentClass: (...args: unknown[]) => unknown) => {

			const className	: string = getClassName(baseComponentClass)

			it(`includes an extension of ${className}`, () => {

				const matchingComponentClass : Type<unknown> = componentClasses.find( (componentClass : Type<unknown>) => isClassExtension(componentClass, baseComponentClass) )

				expect(matchingComponentClass).toBeDefined()

			})

		})
	})


	// Check if all components pass the corresponding unit test
	componentClasses.forEach( (componentClass: Type<unknown>) => {

		const componentClassName	: string 	= getClassName(componentClass)


		describe(`${componentClassName}`, () => {

			// Find the corresponding tests:
			const entry	: ComponentUnitTests 	= componentsTestEntries.find( testEntry => isClassExtension(componentClass, testEntry.baseComponentClass) )

			it('should have a set of unit tests', () => {
				expect(entry).toBeDefined()
				expect(entry.unitTests).toBeDefined()
			})

			if(!entry) return undefined

			const baseComponentClassName	: string 				= getClassName(entry.baseComponentClass)

			// This test is a bit redundant, it still remains for explicitness;
			// no further need to add an extension test to the unit tests of a
			// component.
			it(`extends ${baseComponentClassName}`, () => {
				expect(isClassExtension(componentClass, entry.baseComponentClass)).toBeTrue()
			})

			// Run the tests for the given componentClass:
			entry.unitTests(componentClass)

		})

	})

}
