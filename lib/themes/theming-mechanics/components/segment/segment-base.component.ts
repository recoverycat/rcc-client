import { Component, Input } from '@angular/core'
import { ControlValueAccessor } from '@angular/forms'

export interface Option {
	label: string
	value: string
}

@Component({
    template: '',
    standalone: false
})
export class RccSegmentBaseComponent implements ControlValueAccessor {
	public writeValue(value: string): void {
		this.value = value
	}

	protected onChange: (value: string) => void = () => undefined
	public registerOnChange(fn: (value: string) => void): void {
		this.onChange = fn
	}

	protected onTouched: () => void = () => undefined
	public registerOnTouched(fn: () => void): void {
		this.onTouched = fn
	}

	@Input()
	public options: Option[]

	@Input()
	public label: string

	protected value: string
}
