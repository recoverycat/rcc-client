import	{	Type 							}	from '@angular/core'
import	{	RccPillButtonBaseComponent		}	from '@rcc/themes'
import 	{	ComponentUnitTests				}	from '../components.commons.spec'


// All Units test for RccButton:
function rccPillButtonTests(componentClass: Type<unknown>) : void {


	it('extends the base component class (RccPillButtonBaseComponent)', () => {

		const proto : unknown = componentClass.prototype as unknown

		expect(proto).toBeInstanceOf(RccPillButtonBaseComponent)
	})

}



export const RccPillButtonComponentTests : ComponentUnitTests = {

	baseComponentClass: 	RccPillButtonBaseComponent,
	unitTests:				rccPillButtonTests

}
