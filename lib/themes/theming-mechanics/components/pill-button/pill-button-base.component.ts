import	{
			Component,
			Input,
		}								from '@angular/core'
import	{	RccButtonBaseComponent	}	from '../button/button-base.component'

/**
 * This class is meant to be a base theme component; meaning that every theme
 * should have a component extending this base class.
 *
 * Here's what any button is supposed to do:
 * - Show a label, no description
 * - Use color scheme associated with hierarchical color if provided with color
 *   input ('primary', 'secondary'...). This affects only the text color, not
 *   background
 */
@Component({
    template: '',
    standalone: false
})
export class RccPillButtonBaseComponent extends RccButtonBaseComponent {
	@Input()
	public outline: boolean = false
}
