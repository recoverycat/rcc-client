import	{
			Component,
			EventEmitter,
			Input,
			Output,
		}								from '@angular/core'
import	{
			ControlValueAccessor,
		}	from '@angular/forms'

@Component({
    selector: 'rcc-edit-text',
    template: '',
    standalone: false
})
export abstract class RccEditTextBaseComponent implements ControlValueAccessor {
	public abstract writeValue(obj: string): void
	public abstract registerOnChange(fn: (text: string) => void): void
	public abstract registerOnTouched(fn: () => void): void
	public abstract setDisabledState?(isDisabled: boolean): void

	@Input()
	public inputLabel: string

	@Input()
	public multiLine: boolean

	@Output()
	public startEdit: EventEmitter<void> = new EventEmitter<void>()

	@Input()
	public placeholder: string = ''

	@Input()
	public rows: number = 3

	@Input()
	public displayValue: string

	protected abstract focusOnInput(): void
}
