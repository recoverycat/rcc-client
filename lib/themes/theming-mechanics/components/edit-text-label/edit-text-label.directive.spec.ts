import	{	Type							}	from '@angular/core'
import	{	RccEditTextBaseLabelDirective	}	from './edit-text-label.directive'
import 	{	ComponentUnitTests				}	from '../components.commons.spec'

function rccEditTextTests(componentClass: Type<unknown>): void {
	it('extends the base component class (RccEditTextBaseComponent)', () => {
		const proto: unknown = componentClass.prototype as unknown

		expect(proto).toBeInstanceOf(RccEditTextBaseLabelDirective)
	})
}

export const RccEditTextLabelBaseDirectiveTests: ComponentUnitTests = {
	baseComponentClass:	RccEditTextBaseLabelDirective,
	unitTests:			rccEditTextTests,
}
