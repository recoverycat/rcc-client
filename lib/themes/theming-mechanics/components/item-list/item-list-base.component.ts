import	{
			Component,
			Input,
		}								from '@angular/core'
import	{	ItemActionRole			}	from '@rcc/common'
import	{
			ItemGroup,
			isItemGroup
		}								from '@rcc/common/items/select'
import	{
			Item,
		}								from '@rcc/core'

/**
 * Shows a list of provided Items or ItemGroups.
 * Can also be used as a wrapper for rcc-list-item-tag.
 */
@Component({
    selector: 'rcc-item-list',
    template: '',
    standalone: false
})
export abstract class RccItemListBaseComponent {
	@Input()
	public items?				: Item[] | ItemGroup[]

	@Input()
	public itemActionRoles?		: ItemActionRole[]

	protected isItemArray(x: unknown) : x is Item[] {
		return Array.isArray(x) && x.every(i => i instanceof Item)
	}

	protected isItemGroupArray(x: unknown) : x is ItemGroup[] {
		return Array.isArray(x) && x.every(i => isItemGroup(i))
	}
}
