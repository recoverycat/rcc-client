import	{
			Component,
			ViewEncapsulation
		} 							from '@angular/core'

// import	{	Platform } 				from '@ionic/angular'

@Component({
    selector: 'app-root',
    templateUrl: 'base-app.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./base-app.component.scss'],
    standalone: false
})
export class IonicBaseAppComponent {
	// constructor(
	// 	private platform		: Platform,
	// ) {
	// 	this.initializeApp()
	// }

	// initializeApp() : void {}
}
