# Introduction

The chart library consists of 4 main chart types (`multiple-choice-chart`, `quantities-chart` (Mengen), `slider-chart` and `yes-no-chart`.) There is a fifth chart type (`medication-chart`) that, at the moment, is identical to the `yes-no-chart`.

Charts are created by passing in pre-formatted data and configuration properties. The functions to create the pre-formatted data from raw data are also included in this library.

# Creating a chart

## 1. Transform to consumable data

Before you can pass data to the charts, you need to turn the raw data into consumable data. The function `generateConsumableDataByTimePeriod` is used to do that. Inside the function there is a switch that decides which type of consumable data should be created, depending on the given `timePeriod` and `chartType`.

Depending on the `startDate` and `daysAhead` props, a subset of the raw data is created with entries within the timeframe. Missing entries within the timeframe are filled with consumable data with `value: null` (empty data). The `notes` are cross-referenced by the date and added to the consumable data.

The `options` prop is only needed to create consumable data for some of the charts, e.g. `yes-no-chart` or `multiple-choice-chart`.

```typescript
interface GenerateConsumableDataProps {
    dataPoints: DatapointBasics[]; // The datapoints from the raw data object
    startDate: Date;        // The start date for the time range to show
    timePeriod: TimePeriod; // one of "week" | "month" | "year"
    chartType: ChartType;   // one of "yesNo" | "quantities" | "medication" | "multipleChoice" | "slider"
    notes?: RawDataString;  // A raw data object that contains the patients' notes. Must be passed for "week" and "month" views.
    daysAhead?: number;           // The number of days to show ahead from the start date
    options?: OptionInteger[];    // The options from the raw data object. Must be passed to "multipleChoice" charts.
}

const consumableData = generateConsumableDataByTimePeriod(props: GenerateConsumableDataProps);
```



## 2. Preparing the chart configuration

### TimeLocaleDefinition

Since languages should not be a fix number of options, you need to pass a [TimeLocaleDefinition](https://github.com/d3/d3-time-format#timeFormatLocale) (d3) for every language used, if you want to have the time axis show the correct formatting for that language (e.g. "Mo", "Di", "Mi", ... as abbreviated day labels in german.) From the definition object, currently only the entries `shortDays` and `shortMonths` are used in the actual charts.

English is set as default and will be used as fallback if no `TimeLocaleDefinition` is passed for a language.

```typescript
const localeDE: TimeLocaleDefinition = {
  dateTime: "%A, der %e. %B %Y, %X",
  date: "%d.%m.%Y",
  time: "%H:%M:%S",
  periods: ["AM", "PM"],
  days: [
    "Sonntag",
    "Montag",
    "Dienstag",
    "Mittwoch",
    "Donnerstag",
    "Freitag",
    "Samstag",
  ],
  shortDays: ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
  months: [
    "Januar",
    "Februar",
    "März",
    "April",
    "Mai",
    "Juni",
    "Juli",
    "August",
    "September",
    "Oktober",
    "November",
    "Dezember",
  ],
  shortMonths: ["J", "F", "M", "A", "M", "J", "J", "A", "S", "O", "N", "D"],
};
```



### Style configuration

Optional. The colors and fonts of the charts can be customized.

```typescript
// the colors used for the marks in the charts
export interface ColorProps {
  primary: string; 	// the primary active color, default is green
  inactive: string; // the inactive color, default is grey
  scale: Map<number | boolean, string>; // a color scale to use for e.g. multiple-choice charts
}

// the font props to use for the x- and y-axes
export interface FontStyleProps {
  family: string;
  size: number;
  weight: number;
  color: string;
  lineHeight: number;
}

// separate props for the notes only. This way, you can show e.g. a bold font for note bubbles,
// while the rest of the axis labels use a normal font weight
export interface NoteStyleProps {
  backgroundColor: string;
  font: FontStyleProps;
}

export interface ChartStyleProps {
  color: ColorProps;
  note: NoteStyleProps;
  font: FontStyleProps;
}
```



### Dimensions

The dimensions of the chart are set via the `width` and the `margin` props. The height is automatically calculated by the width and an aspect-ratio. The aspect-ratio changes for the `yes-no-chart`, depending on the `timePeriod` selected. For "week "and "month "view, the aspect is narrow, for year view it is wide. All other charts use only the wide aspect-ratio.

The `margin` is used to determine the space outside of the stage, where the marks are drawn. The margin on the left is used for the y-axis, the bottom margin is used for the x-axis.

```
*                             | margin.top
*                             v
*     ┌──────────────────────────────────────────────────────┐
*     │                 ┌───────────────────────────────────┐│
*     │                 │     ^                             ││
*     │                 │     |                             ││
*     │                 │                                   ││
*     │                 │                                   ││
*     │<--margin.left-->│              stage              ->││<-- margin.right
*     │                 │                                   ││
*     │                 │                                   ││
*     │                 │                                   ││
*     │                 │     |                             ││
*     │                 │     v                             ││
*     │                 ●───────────────────────────────────┘│
*     │                    Di   Mi   Do   Fr   Sa   So   Mo  │
*     └──────────────────────────────────────────────────────┘
*                             ^
*                             | margin.bottom
```



## 3. Initialize the chart

```typescript
// basic chart configuration properties
export interface BaseChartConfig<
  CONSUMABLE_DATA_TYPE extends ConsumableDataBase
> {
  data: CONSUMABLE_DATA_TYPE[]; // an array of consumable data
  startDate: Date; 				// the initial start date
  endDate: Date; 				// the initial end date
  timePeriod: TimePeriod; 		// one of "week" | "month" | "year"
  onTick: (event: Event, tickData: TickDataScaleX) => void; // a callback from clicking on a note bubble with information
  															// on the event and the tickData connected to the note
  language?: string; 	// the inital language that should be used
  dimensions?: {
    width?: number; 	// the width of the SVG
    margin?: Margin; 	// the margins to the stage within the SVG, see "Dimensions"
  };
  styles?: DeepPartial<ChartStyleProps>; 	// custom styles, see "Style configuration"
  showNotes?: boolean; 						// Default: true;
  debug?: boolean; 							// Default: false; Will show guides for dimensions and spacings used. Only use
 										 	// them when you need to finetune spacings during development
  locales?: { [language: string]: TimeLocaleDefinition }; // TimeLocaleDefinitions for languages used
}

// charts with options also need an array of options passed in
export interface BaseChartWithOptionsConfig<
  CONSUMABLE_DATA_TYPE extends ConsumableDataBase,
  OPTIONS_TYPE extends OptionBasics
> extends BaseChartConfig<CONSUMABLE_DATA_TYPE> {
  options: OPTIONS_TYPE[];
}

// create common chart props, that all charts will use
const commonChartProps = {
  startDate,
  endDate,
  timePeriod: "week",
  language: "de",
  onTick: (event: Event, tickData: TickDataScaleX) => {
    console.log(event, tickData);
  },
  dimensions: DIMENSIONS_DEFAULT,
  locales: { de: localeDE },
};

// create a new chart with the properties
chart = new YesNoChart({
  data: consumableData,
  options: rawData.question.options,
  ...commonChartProps,
});

// get the element, where the chart should be placed within and append
// the chart by the exposed property chart.svg.node()
const datavizContainer = document.querySelector<HTMLDivElement>("#dataviz")!;
datavizContainer.appendChild(chart.svg.node()!);

// initially render the chart using the passed data and props
chart.render();
```



```typescript

```



## 4. Update on state change

Every chart instance exposes the functions `updateData`, `updateLanguage` and `updateTimePeriod` , which will each update the internal property passed in and trigger a re-render of the chart.

```typescript
//... inside your state change callback

// generate new consumable data with the updated data and props
const newConsumableData = generateConsumableDataByTimePeriod({
    dataPoints: newData.datapoints,
    notes: newNotes,
    startDate: newStartDate
    daysAhead: newDaysAhead,
    timePeriod: newTimePeriod,
    chartType: "yesNo",
});

if (
    // use the utility functions to confirm that consumableData is of expected type
    // for this visualization
    isArrayOfConsumableDataForYesNo(newConsumableData) ||
    isArrayOfConsumableDataForGroupCharts(newConsumableData)
) {
    chart.updateData(newConsumableData); 	// these update functions will also trigger a render() after
    chart.updateLanguage(newLanguage);   	// updating the classes internal props.
    chart.updateTimePeriod(newTimePeriod);  // ...
} else {
    console.error(
    	"Error: consumableData for YesNo chart is not an array of ConsumableDataForYesNo or ConsumableDataForGroupCharts"
    );
}
```



# Folder structure

## 📁components

The main folder for the chart components.

### 📂chart-elements

Contains reusable visualization components like x-axis, y-axis, grouped-bars etc.

- **grouped-bars** A collection of grouped bars as a single mark. Used in year views of multiple-choice-chart and yes-no-chart.
- **single-bars** A collection of single bars. Used in slider-chart and quantities-chart.
- **x-axis** The date axis at the bottom of the visualization
- **y-axis** The axis on the left that shows the input domain of the options
- **y-legend** Used as a color legend when grouped bars are showing, e.g. year view of multiple-choice-chart

### 📂charts

Contains the different chart type. All charts extend from a `base-chart` class that renders the SVG and the groups where the visual elements will be placed. The base class also contains the x-axis, since all visualizations use it. There is a `base-chart-with-options` class which adds the `options` property that a lot of chart types use for the display of the y-axis / y-legend.



## 📁content

Holds the example data files.



## 📁models

Contains types and interfaces used for the visualization properties.

- **constants.ts** Contains constant variables that are used throughout the different components, e.g. default values for styles or spacings.
- **models.ts** Interfaces for data and properties that are used by the charts, e.g. consumable data types or style configuration props.
- **raw-data.ts** Interfaces for describing the raw data.
- **index.ts** References and exports the above mentioned files.



## 📁util

Contains calculations and utility functions, e.g. transformation function from raw data to consumable data.

- **index.ts** Mostly transformation functions from raw data to consumable data. Also some other utilities like working with dates.
- **type-guards.ts** Type guards for basic JS types, raw data types and consumable data types
