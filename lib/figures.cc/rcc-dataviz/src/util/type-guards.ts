import type {
  ConsumableDataForGroupCharts,
  ConsumableDataForSingleBars,
  ConsumableDataForYesNo,
  DatapointBoolean,
  DatapointInteger,
  DatapointIntegerArray,
} from "../models";
// TYPE GUARDS
// asserts of basic types
export function assertString(value: unknown): asserts value is string {
  if (typeof value !== "string") {
    throw new TypeError(`Expected 'string', got: '${typeof value}'`);
  }
}

export function assertDate(value: unknown): asserts value is Date {
  if (!(value instanceof Date)) {
    throw new TypeError(`Expected 'Date', got: '${typeof value}'`);
  }
}

export function assertNumber(value: unknown): asserts value is number {
  if (typeof value !== "number") {
    throw new TypeError(`Expected 'number', got: '${typeof value}'`);
  }
}

export function assertBoolean(value: unknown): asserts value is boolean {
  if (typeof value !== "boolean") {
    throw new TypeError(`Expected 'boolean', got: '${typeof value}'`);
  }
}

export function assertArrayOfDatapointBoolean(
  value: unknown
): asserts value is DatapointBoolean[] {
  if (!isArrayOfDatapointBoolean(value)) {
    throw new TypeError(
      `Expected 'DatapointBoolean[]', got: '${typeof value}'`
    );
  }
}

export function assertArrayOfDatapointInteger(
  value: unknown
): asserts value is DatapointInteger[] {
  if (!isArrayOfDatapointInteger(value)) {
    throw new TypeError(
      `Expected 'DatapointInteger[]', got: '${typeof value}'`
    );
  }
}
// type guards for basic types
export const isString = (value: unknown): value is string => {
  return typeof value === "string";
};

export const isNumber = (value: unknown): value is number => {
  return typeof value === "number";
};

export const isNumberArray = (value: unknown): value is number[] => {
  return Array.isArray(value) && value.every((v) => typeof v === "number");
};

export const isBoolean = (value: unknown): value is boolean => {
  return typeof value === "boolean";
};

// type guards for interfaces
export const isDatapointBoolean = (
  value: unknown
): value is DatapointBoolean => {
  return (
    typeof value === "object" &&
    value !== null &&
    "date" in value &&
    "value" in value &&
    "note" in value &&
    typeof value.date === "string" &&
    typeof value.value === "boolean" &&
    typeof value.note === "string"
  );
};

export const isDatapointInteger = (data: unknown): data is DatapointInteger => {
  return (
    typeof data === "object" &&
    data !== null &&
    "date" in data &&
    "value" in data &&
    "note" in data &&
    typeof data.date === "string" &&
    typeof data.value === "number" &&
    typeof data.note === "string"
  );
};

export const isDatapointIntegerArray = (
  data: unknown
): data is DatapointIntegerArray => {
  return (
    typeof data === "object" &&
    data !== null &&
    "date" in data &&
    "value" in data &&
    "note" in data &&
    typeof data.date === "string" &&
    Array.isArray(data.value) &&
    data.value.every((v) => typeof v === "number") &&
    typeof data.note === "string"
  );
};

export const isConsumbleDataForSingleBars = (
  data: unknown
): data is ConsumableDataForSingleBars => {
  return (
    typeof data === "object" &&
    data !== null &&
    "date" in data &&
    "value" in data &&
    "note" in data &&
    typeof data.date === "object" &&
    data.date !== null &&
    (typeof data.value === "number" || data.value === null) &&
    (typeof data.note === "string" || data.note === null)
  );
};

export const isConsumbleDataBoolean = (
  value: unknown
): value is ConsumableDataForYesNo => {
  return (
    typeof value === "object" &&
    value !== null &&
    "date" in value &&
    "value" in value &&
    "note" in value &&
    typeof value.date === "object" &&
    value.date !== null &&
    (typeof value.value === "boolean" || value.value === null) &&
    (typeof value.note === "string" || value.note === null)
  );
};

export const isConsumableDataForGroupCharts = (
  data: unknown
): data is ConsumableDataForGroupCharts => {
  return (
    typeof data === "object" && // data should be an object with values for "date", "values", and "note"
    data !== null &&
    "date" in data &&
    "values" in data &&
    "note" in data &&
    data.date instanceof Date && // date should be a Date object
    ((Array.isArray(data.values) && // values can either be null or an array of objects with values for "value" and "color"
      data.values.every(
        (d) =>
          "yScaleValue" in d &&
          "option" in d &&
          typeof d.yScaleValue === "number" && // yScaleValue should be a number
          (typeof d.option === "boolean" ||
            typeof d.option === "number" ||
            d.option === null) // option must be a boolean, number or null
      )) ||
      data.values === null) &&
    (typeof data.note === "string" || data.note === null) // note should be a string or null
  );
};

export const isDataWithNumericValueAndColor = (
  data: unknown
): data is { value: number; color: string } => {
  return (
    typeof data === "object" &&
    data !== null &&
    "value" in data &&
    "color" in data &&
    data.value !== null &&
    typeof data.value === "number" &&
    data.color !== null &&
    typeof data.color === "string"
  );
};

export const isDataWithNullValue = (data: unknown): data is { value: null } => {
  return (
    typeof data === "object" &&
    data !== null &&
    "value" in data &&
    data.value === null
  );
};

// type guards for arrays of interfaces
export const isArrayOfDatapointBoolean = (
  value: unknown
): value is DatapointBoolean[] => {
  return Array.isArray(value) && value.every(isDatapointBoolean);
};

export const isArrayOfDatapointInteger = (
  value: unknown
): value is DatapointInteger[] => {
  return Array.isArray(value) && value.every(isDatapointInteger);
};

export const isArrayOfDatapointIntegerArray = (
  value: unknown
): value is DatapointIntegerArray[] => {
  return (
    Array.isArray(value) &&
    (value.every(isDatapointIntegerArray) || value.length === 0)
  );
};

export const isArrayOfConsumableDataForYesNo = (
  value: unknown
): value is ConsumableDataForYesNo[] => {
  return (
    Array.isArray(value) &&
    (value.every(isConsumbleDataBoolean) || value.length === 0)
  );
};

export const isArrayOfConsumableDataForSingleBars = (
  value: unknown
): value is ConsumableDataForSingleBars[] => {
  return (
    Array.isArray(value) &&
    (value.every(isConsumbleDataForSingleBars) || value.length === 0)
  );
};

export const isArrayOfConsumableDataForGroupCharts = (
  data: unknown
): data is ConsumableDataForGroupCharts[] => {
  return (
    Array.isArray(data) &&
    (data.every(isConsumableDataForGroupCharts) || data.length === 0)
  );
};
