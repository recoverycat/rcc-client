/**
 * @fileoverview This file contains helpers for the controls
 * that change the valued multiple choice chart. The controls are not
 * a necessary part of the chart library itself, but are used to change
 * the chart's state in the preview.
 */
import {
  parseTime,
  getIsoStringWithTimeZoneOffsett,
  getOneMonthAhead,
  getOneYearAhead,
  getDaysBetweenDates,
} from "./util";
import type { ApplicationState, TimePeriod } from "./models";

interface Config {
  languages?: string[];
}
export default class Controls {
  private state: ApplicationState;
  private dateSelect: HTMLInputElement;
  private daysAheadSelect: HTMLInputElement;
  private timePeriodSelect: HTMLSelectElement;
  private languageSelect: HTMLSelectElement;
  private stateChangeCallback: (state: ApplicationState) => void;

  constructor(initialState: ApplicationState, { languages = [] }: Config = {}) {
    this.state = initialState;
    this.stateChangeCallback = (newState) => {
      console.log(newState);
    };

    const { minDate, maxDate, startDate, language, daysAhead, timePeriod } =
      this.state;

    // get the input elements
    this.dateSelect = document.querySelector<HTMLInputElement>("#date-select")!;
    this.dateSelect.min = getIsoStringWithTimeZoneOffsett(minDate);
    this.dateSelect.max = getIsoStringWithTimeZoneOffsett(maxDate);
    this.dateSelect.value = getIsoStringWithTimeZoneOffsett(startDate);

    this.daysAheadSelect =
      document.querySelector<HTMLInputElement>("#days-ahead-select")!;
    this.daysAheadSelect.value = daysAhead.toString();

    this.timePeriodSelect = document.querySelector<HTMLSelectElement>(
      "#time-period-select"
    )!;
    this.timePeriodSelect.value = timePeriod;

    this.languageSelect =
      document.querySelector<HTMLSelectElement>("#language-select")!;
    languages.forEach((language) => {
      const option = document.createElement("option");
      option.value = language;
      option.text = language;
      this.languageSelect.appendChild(option);
    });

    this.languageSelect.value = language;

    // date select event
    this.dateSelect.addEventListener("change", (e: Event) => {
      const eventTarget = e.target as HTMLInputElement;
      const newStartDate = parseTime(eventTarget.value)!;

      const newEndDate = new Date(newStartDate.getTime());
      newEndDate.setDate(newStartDate.getDate() + this.state.daysAhead);

      this.state.startDate = newStartDate;
      this.stateChangeCallback(this.state);
    });

    // days ahead select event
    this.daysAheadSelect.addEventListener("change", (e: Event) => {
      const eventTarget = e.target as HTMLInputElement;
      const newDaysAhead = parseInt(eventTarget.value);

      const newEndDate = new Date(this.state.startDate.getTime());
      newEndDate.setDate(this.state.startDate.getDate() + newDaysAhead);

      this.state.daysAhead = newDaysAhead;
      this.stateChangeCallback(this.state);
    });

    // language select event
    this.languageSelect.addEventListener("change", (e: Event) => {
      const eventTarget = e.target as HTMLSelectElement;
      const newLanguage = eventTarget.value;

      this.state.language = newLanguage;
      this.stateChangeCallback(this.state);
    });

    // time period select event
    this.timePeriodSelect.addEventListener("change", (e: Event) => {
      const eventTarget = e.target as HTMLSelectElement;
      const newTimePeriod = eventTarget.value as TimePeriod;

      this.state.timePeriod = newTimePeriod;

      let newDaysAhead = 6;
      switch (newTimePeriod) {
        case "week":
          newDaysAhead = 6;
          break;

        case "month":
          newDaysAhead = getDaysBetweenDates(
            this.state.startDate,
            getOneMonthAhead(this.state.startDate)
          );
          break;

        case "year":
          newDaysAhead = getDaysBetweenDates(
            this.state.startDate,
            getOneYearAhead(this.state.startDate)
          );
          break;

        default:
          this.state.daysAhead = 6;
          break;
      }

      this.state.daysAhead = newDaysAhead;
      this.daysAheadSelect.value = newDaysAhead.toString();

      this.stateChangeCallback(this.state);
    });
  }

  public onStateChange(callback: (state: ApplicationState) => void) {
    this.stateChangeCallback = callback;
  }
}
