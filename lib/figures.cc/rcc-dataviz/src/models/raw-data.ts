/**
 * @fileoverview This file contains interfaces for the raw data
 */

// translations
export interface Translations {
  [langShort: string]: string; // translations are not pre-defined, so we use a string index
}

// options
export interface OptionBasics {
  translations: Translations;
  value: number | boolean;
}
export interface OptionInteger extends OptionBasics {
  value: number;
}
export interface OptionBoolean extends OptionBasics {
  value: boolean;
}

// data points
export interface DatapointBasics {
  date: string;
  note: string;
  value: number | number[] | boolean | string;
}
export interface DatapointInteger extends DatapointBasics {
  value: number;
}
export interface DatapointIntegerArray extends DatapointBasics {
  value: number[];
}
export interface DatapointIntegerOrIntegerArray extends DatapointBasics {
  value: number | number[];
}
export interface DatapointBoolean extends DatapointBasics {
  value: boolean;
}
export interface DataPointString extends DatapointBasics {
  value: string;
}

// questions
export interface QuestionBasics {
  id: string;
  type: "string" | "integer" | "boolean";
  meaning: string;
  translations: Translations;
  tags: string[];
}
export interface QuestionInteger extends QuestionBasics {
  type: "integer";
  options: OptionInteger[];
}
export interface QuestionIntegerMulti extends QuestionInteger {
  multi: true;
}
export interface QuestionBoolean extends QuestionBasics {
  type: "boolean";
  options: OptionBoolean[];
}
export interface QuestionString extends QuestionBasics {
  type: "string";
}
export interface QuestionIntegerSlider extends QuestionBasics {
  type: "integer";
  min: number;
  max: number;
}

// raw data
export interface RawDataInteger {
  question: QuestionInteger;
  datapoints: DatapointInteger[];
}
export interface RawDataIntegerMulti {
  question: QuestionIntegerMulti;
  datapoints: DatapointIntegerArray[];
}
export interface RawDataIntegerSlider {
  question: QuestionIntegerSlider;
  datapoints: DatapointInteger[];
}
export interface RawDataBoolean {
  question: QuestionBoolean;
  datapoints: DatapointBoolean[];
}
export interface RawDataString {
  question: QuestionString;
  datapoints: DataPointString[];
}

export type RawData =
  | RawDataInteger
  | RawDataIntegerMulti
  | RawDataIntegerSlider
  | RawDataBoolean
  | RawDataString;
