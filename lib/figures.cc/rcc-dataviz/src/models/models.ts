import type { TimeLocaleDefinition } from "d3-time-format";
import type { OptionBasics } from "./raw-data";

export type DeepPartial<T> = {
  [P in keyof T]?: T[P] extends object ? DeepPartial<T[P]> : T[P];
};
export interface Margin {
  top: number;
  right: number;
  bottom: number;
  left: number;
}

export interface Position {
  x: number;
  y: number;
}

export interface FontStyleProps {
  family: string;
  size: number;
  weight: number;
  color: string;
  lineHeight: number;
}

export interface NoteStyleProps {
  backgroundColor: string;
  font: FontStyleProps;
}

export interface ColorProps {
  primary: string;
  inactive: string;
  scale: Map<number | boolean, string>;
}

export interface ChartStyleProps {
  color: ColorProps;
  note: NoteStyleProps;
  font: FontStyleProps;
}

// consumable data
export interface ConsumableDataBase {
  date: Date;
  note: string | null;
}

export interface ConsumableDataWithValue extends ConsumableDataBase {
  value: number | number[] | boolean | string | null;
}
export interface ConsumableDataForSingleBars extends ConsumableDataWithValue {
  value: number | null;
}
export interface ConsumableDataForYesNo extends ConsumableDataWithValue {
  value: boolean | null;
}
export interface ValueWithOption {
  yScaleValue: number;
  option: boolean | number | null;
}
export interface ConsumableDataForGroupCharts extends ConsumableDataBase {
  values: ValueWithOption[] | null;
}

export interface TickDataScaleX {
  date: Date;
  note: string | null;
}
export interface TickDataScaleY {
  value: number;
  label: {
    [language: string]: string;
  };
}

export interface TickDataLegendY {
  color: string;
  label: {
    [language: string]: string;
  };
}

export type TimePeriod = "week" | "month" | "year";
export type ChartType =
  | "yesNo"
  | "quantities"
  | "medication"
  | "multipleChoice"
  | "slider";
export interface ApplicationState {
  language: string;
  startDate: Date;
  minDate: Date;
  maxDate: Date;
  daysAhead: number;
  timePeriod: TimePeriod;
}

export type LocaleDefinitions = { [language: string]: TimeLocaleDefinition };

export interface BaseChartConfig<
  CONSUMABLE_DATA_TYPE extends ConsumableDataBase
> {
  data: CONSUMABLE_DATA_TYPE[];
  startDate: Date;
  endDate: Date;
  timePeriod: TimePeriod;
  onTick: (event: Event, tickData: TickDataScaleX) => void;
  language?: string;
  dimensions?: {
    height?: number;
    width?: number;
    margin?: Margin;
  };
  styles?: DeepPartial<ChartStyleProps>;
  showNotes?: boolean;
  debug?: boolean;
  locales?: { [language: string]: TimeLocaleDefinition };
  filterTicks?: boolean;
  aspectCompressed?: number;
}
export interface BaseChartWithOptionsConfig<
  CONSUMABLE_DATA_TYPE extends ConsumableDataBase,
  OPTIONS_TYPE extends OptionBasics
> extends BaseChartConfig<CONSUMABLE_DATA_TYPE> {
  options: OPTIONS_TYPE[];
}
