/**
 * @fileoverview Constants used throughout the application.
 */

import { scalePow } from "d3-scale";
import { reversedPowScale } from "../util/index";

export const COLORS_DEFAULT = {
  greyBack: "#EBEDED",
  greenDefault: "#22A58F",
  purpleDisabled: "#A8BAFC",
  textLight: "#444343",
};
export const COLORSCALE = ["#14796A", "#22A58F", "#9EE6D9", "#D7F5F0"];

export const COLORMAP_DEFAULT = new Map([
  [0, COLORSCALE[0]],
  [1, COLORSCALE[1]],
  [2, COLORSCALE[2]],
  [3, COLORSCALE[3]],
]);
export const DIMENSIONS_DEFAULT = {
  width: 266,
  margin: {
    top: 1,
    right: 1,
    bottom: 30,
    left: 54,
  },
};

export const FONT_DEFAULT = {
  family: "'IBM Plex Sans', monospace",
  size: 8,
  weigth: 400,
  color: COLORS_DEFAULT.textLight,
  lineHeight: 8.5,
};

export enum GRAPH_CLASSNAMES {
  CHART = "chart", // class name for the main svg container
  STAGE = "stage", // the stage (drawing area without margins and axes)

  AXIS_X = "x-axis", // the x-axis
  AXIS_X_TICK = "x-axis-tick", // the x-axis ticks, a group holding the text and possibly a circle for a note
  AXIS_Y = "y-axis", // the y-axis
  AXIS_Y_TICK = "y-axis-tick", // the x-axis ticks, a group holding the text and possibly a circle for a note
  LEGEND_Y_TICK = "y-legend-tick", // the y-axis legend ticks, a group holding a text and a colored circle
  LABEL = "label", // the label for x-axis and y-axis ticks and the legend

  BARS = "bars", // the group of individual bars (week and month view)
  BAR_DATA = "bar--data", // a group for a bar with data
  BAR_DATA_BACKGROUND = "bar--data__background", // a full stage-height background bar
  BAR_DATA_MASK = "bar--data__mask", // a full stage-height bar used as mask for the bar data
  BAR_DATA_VALUE = "bar--data__value", // a full stage-height background bar
  BAR_EMPTY = "bar--empty", // a bar without data
  BARGROUP = "bar-group", // the grouped bars (year view)
  BARGROUP_BAR_DATA = "bar-group__bar--data", // a bar with data in a group of bars
  BARGROUP_BAR_EMPTY = "bar-group__bar--empty", // a bar without data in a group of bars

  CIRCLES_GROUP = "circle-group", // a group of circles (e.g. week and month view)
  CIRCLE_GROUP_DATA = "circle-group--data", // a group of circles with data (e.g. multiple choice, week and month view)
  CIRCLE_DATA = "circle--data", // a circle with data
  CIRCLE_GROUP_EMPTY = "circle-group--empty", // a group of circles without data
  CIRCLE_EMPTY = "circle--empty", // a circle without data
}

// axes
export const AXIS_X_OFFSET_Y = 20;
export const AXIS_X_NOTE_OFFSET_Y_MONTH_VIEW = 14;
export const AXIS_X_NOTE_SIZE_WEEK_VIEW = 20;
export const AXIS_X_NOTE_SIZE_MONTH_VIEW = 6;

export const AXIS_Y_OFFSET = 10;
export const AXIS_Y_VERTICAL_PADDING = 5;

// graph aspect ratios
export const ASPECT_WIDESCREEN = 1.66;
export const ASPECT_COMPRESSED = 4.75;

// grouped bar chart
export const BARGROUP_BAR_WIDTH = 4;

// legend y
export const LEGEND_Y_DOT_SIZE = 6;
export const LEGEND_Y_DOT_MARGIN_BOTTOM = 3;

// marks
export const GAP_SIZE_SCALE = reversedPowScale(
  scalePow().exponent(10).domain([7, 30]).range([3, 12])
); // 7 days = 12px, 30 days = 2px gaps between marks
export const MARK_SIZE_MAX = 20;
export const MARK_SIZE_MIN = 2;
