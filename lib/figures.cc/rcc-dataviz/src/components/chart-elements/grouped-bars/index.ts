import type { ScaleTime, ScaleLinear } from "d3-scale";
import type { Selection } from "d3-selection";
import { GRAPH_CLASSNAMES } from "../../../models";
import type { ConsumableDataForGroupCharts } from "../../../models";

interface GroupedBarsProps {
  parent: Selection<SVGGElement, undefined, null, undefined>;
  debug?: boolean;
}
interface GroupedBarsRenderProps {
  data: ConsumableDataForGroupCharts[];
  scaleX: ScaleTime<number, number>;
  scaleY: ScaleLinear<number, number>;
  markWidth: number;
  colorMap: Map<number | boolean, string>;
}
export default class GroupedBars {
  private parent: Selection<SVGGElement, undefined, null, undefined>;
  private debug: boolean;

  constructor(props: GroupedBarsProps) {
    this.parent = props.parent;
    this.debug = props.debug ?? false;
  }

  public render({
    data,
    scaleX,
    scaleY,
    markWidth,
    colorMap,
  }: GroupedBarsRenderProps) {
    // separate data with values from data without values
    const nonNullValueData = data.filter((d) => d.values !== null);

    const barWidth = markWidth / (nonNullValueData[0].values?.length || 4);

    // get the number of bars to draw
    const barGroupMidOffset = markWidth / 2;

    // draw group to hold all bars for each date

    const barGroup = this.parent
      .selectAll(`.${GRAPH_CLASSNAMES.BARGROUP}`)
      .data(nonNullValueData)
      .join("g")
      .classed(GRAPH_CLASSNAMES.BARGROUP, true)
      .attr(
        "transform",
        (d) => `translate(${scaleX(d.date) - barGroupMidOffset}, 0)`
      );

    if (this.debug) {
      barGroup
        .selectAll(".tick--mark-left")
        .data((d) => [d])
        .join("line")
        .classed("tick--mark-left", true)
        .attr("x1", 0)
        .attr("x2", 0)
        .attr("y1", 0)
        .attr("y2", -4)
        .attr("stroke", "black")
        .attr("stroke-width", 1);
      barGroup
        .selectAll(".tick--mark-right")
        .data((d) => [d])
        .join("line")
        .classed("tick--mark-right", true)
        .attr("x1", markWidth)
        .attr("x2", markWidth)
        .attr("y1", 0)
        .attr("y2", -4)
        .attr("stroke", "black")
        .attr("stroke-width", 1);
    }

    // draw a bar for each data point with a value
    barGroup
      .selectAll(`.${GRAPH_CLASSNAMES.BARGROUP_BAR_DATA}`)
      .data((d) => {
        let barData: {
          value: number;
          color: string;
          offset: number;
        }[] = [];
        if (d.values !== null) {
          barData = d.values.map((v, i) => {
            return {
              value: v.yScaleValue!,
              color: v.option !== null ? colorMap.get(v.option)! : "black",
              offset: i * barWidth,
            };
          });
        } else {
          barData = [];
        }

        return barData;
      })
      .join("rect")
      .classed(GRAPH_CLASSNAMES.BARGROUP_BAR_DATA, true)
      .attr("x", (d) => d.offset)
      .attr("y", (d) => -scaleY(d.value))
      .attr("rx", barWidth / 2)
      .attr("ry", barWidth / 2)
      .attr("width", barWidth)
      .attr("height", (d) => scaleY(d.value))
      .attr("fill", (d) => d.color);
  }

  public clear() {
    this.parent.selectAll(`.${GRAPH_CLASSNAMES.BARGROUP}`).remove();
  }
}
