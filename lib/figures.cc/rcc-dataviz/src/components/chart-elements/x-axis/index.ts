import { timeFormatLocale } from "d3-time-format";
import type { ScaleTime } from "d3-scale";
import type { Selection } from "d3-selection";
import {
  AXIS_X_OFFSET_Y,
  AXIS_X_NOTE_OFFSET_Y_MONTH_VIEW,
  AXIS_X_NOTE_SIZE_WEEK_VIEW,
  AXIS_X_NOTE_SIZE_MONTH_VIEW,
  GRAPH_CLASSNAMES,
  ChartStyleProps,
} from "../../../models";
import type {
  TickDataScaleX,
  TimePeriod,
  LocaleDefinitions,
} from "../../../models";
import { tickDataByTimePeriod, localeEN } from "../../../util";

interface XAxisProps {
  parent: Selection<SVGGElement, undefined, null, undefined>;
  noteCallback: (event: Event, datum: TickDataScaleX) => void;
  styles: ChartStyleProps;
  locales: LocaleDefinitions;
  showNotesMonthView?: boolean;
  debug?: boolean;
}
interface XAxisRenderProps {
  ticks: TickDataScaleX[];
  scaleX: ScaleTime<number, number>;
  timePeriod: TimePeriod;
  language: string;
}
export default class XAxis {
  private parent: Selection<SVGGElement, undefined, null, undefined>;
  private noteCallback: (event: Event, datum: TickDataScaleX) => void;
  private styles: ChartStyleProps;
  private locales: LocaleDefinitions;
  private showNotesMonthView: boolean;
  private debug: boolean;

  constructor({
    parent,
    noteCallback,
    styles,
    locales,
    showNotesMonthView = true,
    debug = false,
  }: XAxisProps) {
    this.parent = parent;
    this.noteCallback = noteCallback;
    this.styles = styles;
    this.locales = locales;
    this.showNotesMonthView = showNotesMonthView;
    this.debug = debug;
  }

  public render({ ticks, scaleX, timePeriod, language }: XAxisRenderProps) {
    // ticks is an array of objects for each day within the the start and end date
    const xAxisTicks = this.parent
      .selectAll(`.${GRAPH_CLASSNAMES.AXIS_X_TICK}`)
      .data(ticks)
      .join(
        (enter) => enter.append("g"), // append on enter
        (update) => update, // update (no change)
        (exit) => exit.remove() // remove on exit
      )
      .classed(GRAPH_CLASSNAMES.AXIS_X_TICK, true)
      .attr(
        "transform",
        (d) => `translate(${scaleX(d.date)}, ${AXIS_X_OFFSET_Y})`
      )
      .on("click", (event: Event, datum: TickDataScaleX) => {
        if (datum.note) {
          this.noteCallback(event, datum);
        }
      });

    // note bubbles
    xAxisTicks
      .selectAll("circle")
      .data((d) =>
        d.note &&
        ((timePeriod === "month" && this.showNotesMonthView) ||
          timePeriod === "week")
          ? [d]
          : []
      ) // draw circle only for ticks with notes and only for week view and month view if showNotesMonthView is true
      .join(
        (enter) => enter.insert("svg:circle", ".label"), // always insert the circle before the label (if it exists)
        (update) => update, // update circle on update (no change)
        (exit) => exit.remove() // remove circle on exit
      )
      .attr("cx", 0)
      .attr("cy", () => {
        switch (timePeriod) {
          case "week":
            return 0;
          case "month":
            return -AXIS_X_NOTE_OFFSET_Y_MONTH_VIEW;
          default:
            return 0;
        }
      })
      .attr("r", () => {
        switch (timePeriod) {
          case "week":
            return AXIS_X_NOTE_SIZE_WEEK_VIEW / 2;
          case "month":
            return AXIS_X_NOTE_SIZE_MONTH_VIEW / 2;
          default:
            return AXIS_X_NOTE_SIZE_WEEK_VIEW / 2;
        }
      })
      .attr("fill", (d) =>
        d.note ? this.styles.note.backgroundColor : "transparent"
      );

    // day label
    const locale = this.locales[language] || localeEN;

    const startDate = scaleX.domain()[0];
    xAxisTicks
      .selectAll("text")
      .data((d) => tickDataByTimePeriod(timePeriod, startDate, d))
      .join(
        (enter) => enter.append("text"), // append text on enter
        (update) => update, // update text on update (no change)
        (exit) => exit.remove() // remove text on exit
      )
      .attr("x", 0)
      .attr("y", 0)
      .attr("dy", "0.075em")
      .attr("dominant-baseline", "middle")
      .attr("text-anchor", "middle")
      .attr("class", "label")
      .attr("font-family", (d) =>
        d.note && timePeriod === "week"
          ? this.styles.note.font.family
          : this.styles.font.family
      )
      .attr("font-size", (d) =>
        d.note && timePeriod === "week"
          ? this.styles.note.font.size
          : this.styles.font.size
      )
      .attr("font-weight", (d) =>
        d.note && timePeriod === "week"
          ? this.styles.note.font.weight
          : this.styles.font.weight
      )
      .attr("fill", (d) =>
        d.note && timePeriod === "week"
          ? this.styles.note.font.color
          : this.styles.font.color
      )
      .text((d) => {
        switch (timePeriod) {
          case "week":
            return timeFormatLocale(locale).format("%a")(d.date); // displays the abbreviated day name "e.g Mo for Monday"

          case "month":
            return timeFormatLocale(locale).format("%d.")(d.date); // displays the day of the month "e.g 02. for the second of the month"

          case "year":
            return timeFormatLocale(locale).format("%b")(d.date); // display the short month name "e.g J for January"

          default:
            return null;
        }
      });

    // debug
    if (this.debug) {
      xAxisTicks
        .selectAll(".debug-rect")
        .data((d) => [d])
        .join(
          (enter) => enter.append("rect"), // append rect on enter
          (update) => update, // update rect on update (no change)
          (exit) => exit.remove() // remove rect on exit
        )
        .classed("debug-rect", true)
        .attr("x", -1)
        .attr("y", -AXIS_X_OFFSET_Y)
        .attr("width", 1)
        .attr("height", AXIS_X_OFFSET_Y)
        .attr("fill", "cyan");
    }
  }
}
