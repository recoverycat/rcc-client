import { ScaleLinear, scaleLinear } from "d3-scale";
import BaseChartWithOptions from "../base-chart-with-options";
import YAxis from "../../chart-elements/y-axis";
import SingleBars from "../../chart-elements/single-bars";

import { AXIS_Y_VERTICAL_PADDING } from "../../../models";
import type {
  ConsumableDataForSingleBars,
  OptionInteger,
  BaseChartWithOptionsConfig,
} from "../../../models";

/**
 * @fileoverview The valued multiple choice chart
 *     ┌──────────────────────────────────────────────────────┐
 *     │ ja,vollkommen      ◠    ◠    ◠    ◠    ◠    ◠    ◠   │
 *     │                   |░|  |░|  |░|  | |  | |  |█|  |░|  │
 *     │                   |░|  |░|  |░|  | |  | |  |█|  |░|  │
 *     │ ja, mit           |░|  |░|  |█|  | |  | |  |█|  |░|  │
 *     │ leichten          |░|  |░|  |█|  | |  | |  |█|  |░|  │
 *     │ Einschränk-       |░|  |░|  |█|  | |  | |  |█|  |░|  │
 *     │ ungen             |░|  |░|  |█|  | |  | |  |█|  |░|  │
 *     │                   |░|  |░|  |█|  | |  | |  |█|  |░|  │
 *     │ mit               |█|  |░|  |█|  | |  | |  |█|  |░|  │
 *     │ deutlichen        |█|  |░|  |█|  | |  | |  |█|  |░|  │
 *     │ Einschränk-       |█|  |░|  |█|  | |  | |  |█|  |░|  │
 *     │ ungen             |█|  |░|  |█|  | |  | |  |█|  |░|  │
 *     │                   |█|  |░|  |█|  | |  | |  |█|  |░|  │
 *     │ kaum              |█|  |█|  |█|  | |  | |  |█|  |█|  │
 *     │                    ◡    ◡    ◡    ◡    ◡    ◡    ◡   │
 *     │                   Di   Mi   Do   Fr   Sa   So   Mo   │
 *     └──────────────────────────────────────────────────────┘
 */
interface quantitiesConfig
  extends BaseChartWithOptionsConfig<
    ConsumableDataForSingleBars,
    OptionInteger
  > {
  options: OptionInteger[];
}
export default class quantities extends BaseChartWithOptions<
  ConsumableDataForSingleBars,
  OptionInteger
> {
  private scaleY: ScaleLinear<number, number>;
  private scaleYPadded: ScaleLinear<number, number>;
  private yAxis: YAxis;
  private singleBars: SingleBars;

  constructor(props: quantitiesConfig) {
    super(props);
    const { options } = props;
    this.options = options;

    // generate y-scale from question options
    const optionValues = options.map((o) => o.value);

    // initialize the y-scale (used for the y-axis and the bars)
    this.scaleY = scaleLinear()
      .domain([Math.min(...optionValues), Math.max(...optionValues)])
		.range([0, this.stageHeight]);

    this.scaleYPadded = this.scaleY
      .copy()
      .range([
		  AXIS_Y_VERTICAL_PADDING,
		  this.stageHeight - AXIS_Y_VERTICAL_PADDING,
      ]);

    // initialize the y-axis (labels for the y-scale)
    this.yAxis = new YAxis({
      parent: this.yAxisGroup,
      fontStyles: this.styles.font,
      debug: this.debug,
    });

    this.singleBars = new SingleBars({
      parent: this.stage,
      debug: this.debug,
    });
  }

  public render(): void {
    super.render();

    this.yAxis.render({
      ticks: this.options.map((o) => ({
        label: o.translations,
        value: o.value,
      })),
      language: this.language,
      scaleY: this.scaleYPadded,
      stageHeight: this.stageHeight,
    });

    this.singleBars.render({
      data: this.data,
      scaleX: this.scaleX,
      scaleY: this.scaleY,
      markWidth: this.markWidth,
      color: this.styles.color,
    });
  }

  public updateOptions(options: OptionInteger[]) {
    this.options = options;
    this.render();
  }
  public updateData(data: ConsumableDataForSingleBars[]): void {
    super.updateData(data);
    this.render();
  }
}
