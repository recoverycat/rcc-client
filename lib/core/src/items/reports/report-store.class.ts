import	{	adHocId				}	from '../../utils'
import	{	Item				}	from '../item.class'

import	{
			ItemStore,
			ItemStorage
		}							from '../item-store.class'




import	{
			Report,
		}							from './report.class'



function identifyItemBy(item: Report): string{

	if(item.id) return item.id

	return item.id = adHocId()

}

export class ReportStore extends ItemStore<Report>{

	public constructor(
		storage? : ItemStorage<Report>,
	){
		super({
			itemClass: 		Report,
			identifyItemBy,
			storage,
		})
	}

}
