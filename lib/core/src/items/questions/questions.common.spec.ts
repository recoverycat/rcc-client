import { DAILY_NOTES_ID, isDailyNotesQuestion, isQuestionRelationDto, Question, QuestionOptionConfig, QuestionRelationDto, sortAnswerOptionsMaxToMin } from '@rcc/core'

describe('QuestionsCommons', () => {

	describe('sortAnswerOptionsMaxToMin', () => {

		it('should return undefined on boolean question on undefined options', () => {
			const res: QuestionOptionConfig[] = sortAnswerOptionsMaxToMin({
				id: 'question-id',
				translations: {},
				type: 'boolean'
			})

			expect(res).toEqual(undefined)
		})

		it('should return sorted values for boolean question on defined options', () => {
			const res: QuestionOptionConfig[] = sortAnswerOptionsMaxToMin({
				id: 'question-id',
				translations: {},
				type: 'boolean',
				options: [
					{ value: false, translations: { de: 'nein', en: 'no' } },
					{ value: true, translations: { de: 'ja', en: 'yes' } }

				]
			})

			expect(res).toEqual(
				[
					{ value: true, translations: { de: 'ja', en: 'yes' } },
					{ value: false, translations: { de: 'nein', en: 'no' } }
				]
			)
		})

		it('should return sorted values for integer question on defined options with 4 items', () => {
			const res: QuestionOptionConfig[] = sortAnswerOptionsMaxToMin({
				id: 'question-id',
				translations: {},
				type: 'integer',
				options: [
					{ value: 0, translations: { en: 'no, not at all', de: 'nein, überhaupt nicht' } },
					{ value: 1, translations: { en: 'no, less than agreed', de: 'nein, weniger' } },
					{ value: 2, translations: { en: 'yes, exactly', de: 'ja, exakt' } },
					{ value: 3, translations: { en: 'no, more than agreed', de: 'nein, mehr ' } }
				]
			})

			expect(res).toEqual(
				[
					{ value: 3, translations: { en: 'no, more than agreed', de: 'nein, mehr ' } },
					{ value: 2, translations: { en: 'yes, exactly', de: 'ja, exakt' } },
					{ value: 1, translations: { en: 'no, less than agreed', de: 'nein, weniger' } },
					{ value: 0, translations: { en: 'no, not at all', de: 'nein, überhaupt nicht' } }
				]
			)
		})

		it('should return unprocessed values for integer question on defined options with > 4 items (1)', () => {
			const res: QuestionOptionConfig[] = sortAnswerOptionsMaxToMin({
				id: 'question-id',
				translations: {},
				type: 'integer',
				options: [
					{ value: 0, translations: { en: 'very bad', de: 'sehr schlecht' } },
					{ value: 1, meaning: '' },
					{ value: 2, meaning: '' },
					{ value: 3, meaning: '' },
					{ value: 4, meaning: '' },
					{ value: 5, meaning: '' },
					{ value: 6, meaning: '' },
					{ value: 7, meaning: '' },
					{ value: 8, meaning: '' },
					{ value: 9, meaning: '' },
					{ value: 10, translations: { en: 'very good', de: 'sehr gut' } }
				]
			})

			expect(res).toEqual(
				[
					{ value: 0, translations: { en: 'very bad', de: 'sehr schlecht' } },
					{ value: 1, meaning: '' },
					{ value: 2, meaning: '' },
					{ value: 3, meaning: '' },
					{ value: 4, meaning: '' },
					{ value: 5, meaning: '' },
					{ value: 6, meaning: '' },
					{ value: 7, meaning: '' },
					{ value: 8, meaning: '' },
					{ value: 9, meaning: '' },
					{ value: 10, translations: { en: 'very good', de: 'sehr gut' } }
				]
			)
		})

		it('should return unprocessed values for integer question on defined options with > 4 items (2)', () => {
			const res: QuestionOptionConfig[] = sortAnswerOptionsMaxToMin({
				id: 'question-id',
				translations: {},
				type: 'integer',
				options: [
					{ value: 10, translations: { en: 'very good', de: 'sehr gut' } },
					{ value: 9, meaning: '' },
					{ value: 8, meaning: '' },
					{ value: 7, meaning: '' },
					{ value: 6, meaning: '' },
					{ value: 5, meaning: '' },
					{ value: 4, meaning: '' },
					{ value: 3, meaning: '' },
					{ value: 2, meaning: '' },
					{ value: 1, meaning: '' },
					{ value: 0, translations: { en: 'very bad', de: 'sehr schlecht' } }
				]
			})

			expect(res).toEqual(
				[
					{ value: 10, translations: { en: 'very good', de: 'sehr gut' } },
					{ value: 9, meaning: '' },
					{ value: 8, meaning: '' },
					{ value: 7, meaning: '' },
					{ value: 6, meaning: '' },
					{ value: 5, meaning: '' },
					{ value: 4, meaning: '' },
					{ value: 3, meaning: '' },
					{ value: 2, meaning: '' },
					{ value: 1, meaning: '' },
					{ value: 0, translations: { en: 'very bad', de: 'sehr schlecht' } }
				]
			)
		})

		it('should return undefined on integer question on undefined options', () => {
			const res: QuestionOptionConfig[] = sortAnswerOptionsMaxToMin({
				id: 'question-id',
				translations: {},
				type: 'integer'
			})

			expect(res).toEqual(undefined)
		})

		it('should return undefined on string question on undefined options', () => {
			const res: QuestionOptionConfig[] = sortAnswerOptionsMaxToMin({
				id: 'question-id',
				type: 'string',
				translations: {},
				options: [
					{ value: 'depressed', translations: { en: 'depressed', de: 'bedrückt' } },
					{ value: 'neutral', translations: { en: 'neutral', de: 'neutral' } },
					{ value: 'excited', translations: { en: 'excited', de: 'enthusiastisch' } }
				]
			},)

			expect(res).toEqual([
				{ value: 'depressed', translations: { en: 'depressed', de: 'bedrückt' } },
				{ value: 'neutral', translations: { en: 'neutral', de: 'neutral' } },
				{ value: 'excited', translations: { en: 'excited', de: 'enthusiastisch' } }
			])
		})

	})

	describe('isDailyNotesQuestion', () => {
		it('should return true for the global daily notes ID', () => {
			expect(isDailyNotesQuestion(DAILY_NOTES_ID)).toBeTrue()
			expect(isDailyNotesQuestion('some-other-id')).toBeFalse()
		})

		it('should return true for QuestionRelationDtos with the daily notes ID', () => {
			const input: QuestionRelationDto = {
                id: DAILY_NOTES_ID,
				category: 'analyze'
            }

			expect(isQuestionRelationDto(input)).toBeTrue()
            expect(isDailyNotesQuestion(input)).toBeTrue()

			input.id = 'some-other-id'

			expect(isDailyNotesQuestion(input)).toBeFalse()
		})

		it('should return true for objects containing the daily notes question', () => {
			expect(isDailyNotesQuestion({ question: new Question(DAILY_NOTES_ID) })).toBeTrue()

			expect(isDailyNotesQuestion({ question: new Question('some-other-id') })).toBeFalse()
			expect(isDailyNotesQuestion({ property: 'value' })).toBeFalse()
			expect(isDailyNotesQuestion({})).toBeFalse()
		})
	})
})
