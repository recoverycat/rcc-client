
import	{
			ItemStore,
			ItemStorage
		}								from '../item-store.class'

import	{
			Question
		}								from './question.class'
import	{
			assertQuestionConfig,
			sortAnswerOptionsMaxToMin
		} 								from './questions.commons'




export abstract class QuestionStore extends ItemStore<Question> {


	public constructor(
		storage? : ItemStorage<Question>
	){
		super({
			itemClass: 	Question,
			storage
		})

		void this.sortItems()
	}

	private async sortItems(): Promise<void> {
		await this.ready
		for (const question of this.items){
			assertQuestionConfig(question)
			sortAnswerOptionsMaxToMin(question)
		}
	}


}
