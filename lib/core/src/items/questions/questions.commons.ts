import	{
			isErrorFree,
			has,
			assert,
			assertProperty
		}									from '../../utils'
import	{
			isQuestionRelationDto
		}									from '../symptom-checks'
import { Question } from './question.class'

export interface TranslationList {
	[index:string]:	string
}

export interface StringOptionConfig{
	value:			string,
	meaning?:		string,
	boolean?:		boolean // legacy remove asap
	translations:	TranslationList
}

export interface NumberOptionConfig{
	value:			number,
	meaning?:		string,
	boolean?:		boolean // legacy remove asap
	translations?:	TranslationList
}

export interface BooleanOptionConfig{
	value:			boolean,
	meaning?:		string,
	boolean?:		boolean // legacy remove asap
	translations?:	TranslationList
}


export type QuestionOptionConfig = StringOptionConfig | NumberOptionConfig | BooleanOptionConfig


export type AnswerType = 'decimal' | 'integer' | 'string' | 'boolean' | 'unknown'

export interface QuestionConfig {
	id:				string,
	type:			AnswerType,				// string, integer, decimal...
	translations:	TranslationList,
	meaning?:		string,					// TODO: instead add extra key for TranslationList
	min?:			number,
	max?:			number,
	options?:		QuestionOptionConfig[],
	tags?:			string[],
	unit?:			string,
	// note?:			string
}

export function hasOptions(config: QuestionConfig): boolean {
	const optionsCount: number = config.options?.length ?? 0
	return optionsCount > 0
}

export function assertNumberOptionConfig(x:unknown): asserts x is NumberOptionConfig {

	assertProperty(x, 'value', 			'assertNumberOptionConfig: missing .value')
	assert(Number.isFinite(x.value),	'assertNumberOptionConfig: .value must be a finite number')

	if( has(x,'translations') && x.translations)
		assert(isTranslationList(x.translations), 'assertNumberOptionConfig: if exists, .translations must be a TranslationList')


}

export function isNumberOptionConfig(x:unknown): x is NumberOptionConfig {
	return isErrorFree( () => assertNumberOptionConfig(x) )
}


export function isStringOptionConfig(x:unknown): x is StringOptionConfig {

	if(!has(x, 'value'))					return false
	if( typeof x.value !== 'string') 		return false
	if(!has(x, 'translations'))				return false
	if(!isTranslationList(x.translations))	return false

	return true
}

export function isBooleanOptionConfig(x:unknown): x is BooleanOptionConfig {

	if(!has(x, 'value'))					return false
	if( typeof x.value !== 'boolean') 		return false
	if(!has(x, 'translations'))				return false
	if(!isTranslationList(x.translations))	return false

	return true
}


export function isAnswerType(x:unknown): x is AnswerType {
	if(typeof x !== 'string') return false
	return ['decimal', 'integer', 'string', 'boolean', 'unknown'].includes(x)
}



export function isTranslationList(x:unknown): x is TranslationList {

	if(typeof x !== 'object') 	return false

	if(Object.keys(x).length === 0) return false
	if(Object.keys(x)	.some(	(key:unknown) 	=> typeof key 	!== 'string' ) ) return false
	if(Object.values(x)	.some(	(value:unknown)	=> typeof value	!== 'string'	) ) return false
	if(Object.values(x)	.every(	(value:unknown)	=> value === '') ) 				return false

	return true
}



export function assertQuestionConfig(x:unknown): asserts x is QuestionConfig {

	assertProperty(x, ['id', 'type', 'translations'])

	const type			: unknown	= x.type
	const id			: unknown	= x.id
	const translations	: unknown 	= x.translations

	assert(typeof id 	=== 'string', 		'assertQuestionConfig: .id must be a string.', id)
	assert(isAnswerType(type),				`assertQuestionConfig: unknown answer type: ${String(type)}`, type)
	assert(isTranslationList(translations),	'assertQuestionConfig: invalid .translations: ', translations)


	if(has(x,'min')) 	assert(['number', 'undefined'].includes(typeof x.min), `assertQuestionConfig: .min must be a number or undefined, got:${typeof x.min}`, x.min)
	if(has(x,'max'))	assert(['number', 'undefined'].includes(typeof x.max), `assertQuestionConfig: .max must be a number or undefined, got:${typeof x.max}`, x.max)

	if(has(x,'min','max'))
		assert(x.min === undefined || x.max === undefined  || x.min <= x.max, 'assertQuestionConfig: .max must be greater or equal to .min', x)



	if(!has(x,'options') || x.options === null || x.options === undefined) return

	const options: unknown 		= x.options

	assert(Array.isArray(options),  		'assertQuestionConfig: if present .options must be an array.' )

	const oLength: number		= options.length
	const eLength: number		= Object.values(options).length // this will differ from options.length, if empty slots are present.

	assert( oLength === eLength,				'assertQuestionConfig: .option must not include empty slots' )

	assert( type !== 'unknown',		 		'assertQuestionConfig: config cannot have .type === \'unknown\' and have .options at the same time.' )


	const stringType: boolean 	= type === 'string'

	if(stringType) assert( options.every( isStringOptionConfig), 'assertQuestionConfig: configs with .type \'string\' must have string labels and only have string valued .options.')

	const numberType: boolean	= ['decimal', 'integer'].includes(type)

	if(numberType) options.forEach(assertNumberOptionConfig)

	const booleanType: boolean	= type === 'boolean'

	if(booleanType) assert( options.every( isBooleanOptionConfig), 'assertQuestionConfig: configs with .type \'boolean\' must have string labels and only have boolean valued .options.')

}

export function isQuestionConfig(x:unknown): x is QuestionConfig {
	return isErrorFree( () => assertQuestionConfig(x) )
}

// Basically, we should only sort on questions types: boolean and integer with 4 options.
// TODO: The comparison with 4 is a hacky, IMHO. We should rethink this, specially the options possibilities, and how they affect the rendering.
// Read more about it here: https://git.recoverycat.de/recoverycat/rcc-client/-/issues/1234
export function sortAnswerOptionsMaxToMin(question: QuestionConfig): QuestionOptionConfig[] {
	if (question.type !== 'boolean' && !(question.type === 'integer' && question.options?.length === 4)) return question.options
	if (!question.options) return question.options

	return question.options.sort((a, b) => (b.value as number) - (a.value as number))
}

export const DAILY_NOTES_ID : string = 'rcc-curated-default-00-daily-note'

export function isDailyNotesQuestion(x: unknown) : boolean {
	if (typeof x === 'string')
		return x === DAILY_NOTES_ID
	if (isQuestionRelationDto(x))
		return x.id === DAILY_NOTES_ID
	// Mimics QueryControls
	// (Not explicitly used here because @rcc/core is supposed to be independent of other workspaces)
	if (has(x, 'question') && x.question instanceof Question)
		return x.question.id === DAILY_NOTES_ID
	return false
}
