import	{	Item						}	from '../item.class'

import	{
			Schedule,
			ScheduleConfig
		}									from '../schedules'


import	{	Observable, Subject			}	from 'rxjs'

import	{
			SymptomCheckConfig,
			QuestionRelationDto,
			isSymptomCheckConfig,
			assertSymptomCheckConfig,
			isQuestionRelationDto,
			defaultSymptomCheckConfig,
			SymptomCheckMetaConfig,
			SymptomCheckPresentationConfig
		}									from './symptom-checks.commons'



// Is there duplication with the interfaces in interfaces.ts?
// Answer: No. The following interfaces are NOT Configs! they make
// use of Classes like Date or Schedule.
export interface SymptomCheckMeta {
	label			: string 	| null
	paused			: boolean 	| null
	defaultSchedule	: Schedule 	| null
	creationDate	: Date 		| null
	reminder		: string	| null // HH:MM
}

export interface QuestionRelation {
	questionId		: string,
	schedule?		: Schedule,
	category?		: string
}


export class SymptomCheck extends Item<SymptomCheckConfig> {

	declare public id				:	string

	public meta!					:	SymptomCheckMeta
	public questionRelations!		: 	QuestionRelation[]
	public presentation				:	SymptomCheckPresentationConfig


	protected 	updateSubject 		: 	Subject<string>		= new Subject<string>()
	public 		update$				: 	Observable<string>	= this.updateSubject.asObservable()


	public static acceptsAsConfig : (x: unknown) => x is SymptomCheckConfig = isSymptomCheckConfig

	public constructor(config: SymptomCheckConfig = defaultSymptomCheckConfig){ super(config) }


	public set config(config: SymptomCheckConfig){

		assertSymptomCheckConfig(config)

		this.meta = {
			label:				config.meta.label 		|| null,
			paused:				config.meta.paused 		|| false,
			reminder:			config.meta.reminder 	|| null,
			creationDate:		config.meta.creationDate
								? 	new Date(config.meta.creationDate)
								: 	null,
			defaultSchedule:	new Schedule(config.meta.defaultSchedule)
		}

		this.presentation = config.presentation

		this.questionRelations = []

		config.questions.forEach( (q: string | QuestionRelationDto) => {
			if(isQuestionRelationDto(q))
				this.addQuestionId(q.id, q.schedule, q.category)
			else
				this.addQuestionId(q)
		})

		this.id = config.id

		this.update('set config')

	}

	public get config(): SymptomCheckConfig {

		const meta : SymptomCheckMetaConfig		=	{
								label:				this.meta.label || null,
								creationDate:		this.meta.creationDate && this.meta.creationDate.toISOString() || null,
								reminder:			this.meta.reminder,
								paused:				this.meta.paused ? true : false,
								defaultSchedule:	this.meta.defaultSchedule && this.meta.defaultSchedule.config  || null
							}

		// Remove empty keys:
		for(const key in meta) if(meta[key as keyof typeof meta ] === null) delete meta[key as keyof typeof meta ]


		const	questions : (string | QuestionRelationDto)[]	=	this.questionRelations.map( item => {
			if(!item.category && !item.schedule) return item.questionId

			const q : QuestionRelationDto = { id: item.questionId }

			if(item.category) q.category = item.category
			if(item.schedule) q.schedule = item.schedule.config

			return q
		})

		const	id : string			=	this.id

		const	presentation : SymptomCheckPresentationConfig = this.presentation

		return 	{ meta, questions, id, presentation }
	}


	/**
	 * Trigger emit message on .update$ observable.
	 */
	public update(message?: string) : void {

		// update() may be called from the constructor
		// this.updateSubject will then not yet exist.
		if(this.updateSubject) this.updateSubject.next(message)
	}

	public addQuestionId(id: string, config?: ScheduleConfig, category?: string): void {

		const schedule : Schedule =	config
							?	Schedule.fromConfig(config)
							:	undefined

		this.questionRelations.push({ questionId: id, schedule, category })
	}

	/**
	 * Returns the schedule specific to a question referred to by the given id –
	 * if it has it's own schedule. If it does not have it's own schedule or
	 * if there is no question with the given id associated with this
	 * Symptomcheck returns a copy of the default schedule of this Symptomcheck.
	 *
	 * If there is no specific schedule for a question and no default schedule
	 * returns null – this should never happen.
	 */
	public getEffectiveSchedule(questionId: string) : Schedule {

		const questionRelation	: 	QuestionRelation
								= 	this.questionRelations.find( qr => qr.questionId === questionId)

		if(!questionRelation) console.warn('SymptomCheck.getEffectiveSchedule(): unknown questionId!')

		const specificSchedule	: 	Schedule
								= 	questionRelation?.schedule

		const effectiveSchedule	: 	Schedule
								= 	specificSchedule || Schedule.copy(this.meta.defaultSchedule)

		return effectiveSchedule

	}

	public togglePause(force?: boolean) : void {
		this.meta.paused = 	typeof force === 'boolean'
							?	force
							:	!this.meta.paused

		this.update('meta.paused')
	}

	public coversQuestionIds(ids: string[]) : boolean {
		return	this.questionRelations
				.map( questionSchedule => questionSchedule.questionId )
				.some( id => ids.includes(id) )
	}

	/**
	 * Returns IDs of questions that are due on a given date
	 */
	public getDueQuestionIds(date: Date): string[] {
		return	this.questionRelations
				.filter(qs => {
					// Get schedule for this question if applicable
					// Otherwise use symptom check's schedule
					const schedule	: Schedule
									= this.getEffectiveSchedule(qs.questionId)

					return schedule.matchesDate(date)
				})
				.map(qs => qs.questionId)
	}

	/**
	 * Checks if any of the symptom check's questions are due today
	 */
	public isDue(date: Date = new Date()): boolean {
		return this.getDueQuestionIds(date).length > 0
	}

	public get questionIds(): string[]	{ return this.questionRelations.map( item => item.questionId) }
}
