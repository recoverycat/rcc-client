import	{
			adHocId
		}								from '../../utils'

import	{
			ItemStore,
			ItemStorage,
		}								from '../item-store.class'


import	{
			SymptomCheck
		}								from './symptom-check.class'


function identifyItemBy(item:SymptomCheck) : string {

	if(item.id) return item.id

	return item.id = adHocId()

}



export class SymptomCheckStore extends ItemStore<SymptomCheck>{

	public constructor(
		storage?: ItemStorage<SymptomCheck>,
	){
		super({
			itemClass: 		SymptomCheck,
			identifyItemBy,
			storage,
		})
	}

	public getDueQuestionIds(date: Date = new Date() ): string[] {
		return	this.items
				.map( (symptomCheck: SymptomCheck) =>  symptomCheck.getDueQuestionIds(date) )
				.flat()
	}

}
