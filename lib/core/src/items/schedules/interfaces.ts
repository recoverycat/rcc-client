import	{
			CalendarDateString,
			assert,
			assertProperty,
			isErrorFree,
		}							from '../../utils'

/**
 * Config that specifies the days of the week and times of day.
 * Repeats indefinitely if no end date specified.
 *
 * Format: `[days of week, times of day, end date?]`
 * End date is the **last** day the schedule is active, format YYYY-MM-DD.
 *
 * @example ```[[0,6], ['morning', 'afternoon'], '2023-12-31']``` (0 = Sunday)
 */
export type PeriodicScheduleConfig = [number[], string[], string?]

/**
 * Config that specifies a schedule for a particular day(s) or finite interval.
 * For the actual scheduling for the query run see {@link Schedule.matchesDate()}
 *
 * @example { '2023-08-11', [0, 12] } schedules for 2023-08-11 and 2023-08-23.
 * @example { '2023-08-11', [12] } schedules for 2023-08-23.
 * @example { '2023-08-11', [] } doesn't do anything.
 */
export interface SporadicScheduleConfig {
	/**
	 * Start date of the schedule in YYYY-MM-DD format.
	 *
	 * Nothing is scheduled for the start date itself.
	 * If you want to target this date, set interval to `[0]`
	 */
	startDate	: string,

	/**
	 * Schedule for x days from `startDate`.
	 */
	interval	: number[],

	/**
	 * Define how long the schedule should be matching after and before
	 * each due date derived from adding interval to the `startDate`.
	 * This will default to 0 days before and 14 days after (see Schedule.matchesDate())
	 */
	matchesDaysBefore?: number,
	matchesDaysAfter?: number,
}

export type ScheduleConfig = PeriodicScheduleConfig | SporadicScheduleConfig

export interface ParsedIsoString {
	timezoneOffset:	number, // in hours
	dayOfWeek:		number, // 0-6, 0 = Sunday
	timeOfDay:		string,
	asLocalDate:	Date	// timezoneOffset in string ignored
}


export function isScheduleConfig(x: unknown): x is ScheduleConfig {
	return isErrorFree( () => assertScheduleConfig(x))
}

export function assertScheduleConfig(x: unknown): asserts x is ScheduleConfig {
	const errors : Error[] = []

	try{ assertPeriodicScheduleConfig(x) } catch(e) { errors.push(e as Error) }
	try{ assertSporadicScheduleConfig(x) } catch(e) { errors.push(e as Error) }
	
	if(errors.length === 2) errors.forEach( e => { throw e })
}

export function isPeriodicScheduleConfig(x: unknown): x is PeriodicScheduleConfig {
	return isErrorFree(() => assertPeriodicScheduleConfig(x))
}

export function assertPeriodicScheduleConfig(x: unknown): asserts x is PeriodicScheduleConfig {
	assert(Array.isArray(x) && x.length > 1 && x.length < 4, 'assertPeriodicScheduleConfig(): x must be an array of length 2 or 3.')
	assert(Array.isArray(x[0]) && x[0].length <= 7, 'assertPeriodicScheduleConfig(): x[0] must be an array of max length 7.')
	assert(Array.isArray(x[1]), 'assertPeriodicScheduleConfig(): x[1] must be an array.')

	const daysOfWeekOK : boolean	= x[0].every(i => (typeof i === 'number') && (0 <= i && i <= 6))
	assert(daysOfWeekOK, 'assertPeriodicScheduleConfig(): values of x[0] must be numbers between 0 and 6')

	const timesOfDayOK : boolean	= x[1].every(i => typeof i === 'string')
	assert(timesOfDayOK, 'assertPeriodicScheduleConfig(): values of x[1] must be strings')

	if (x.length === 3)
		assert(CalendarDateString.isValid(x[2]), 'assertPeriodicScheduleConfig(): x[2] must be a CalendarDateString.')
}

export function isSporadicScheduleConfig(x: unknown): x is SporadicScheduleConfig {
	return isErrorFree(() => assertSporadicScheduleConfig(x))
}

export function assertSporadicScheduleConfig(x: unknown): asserts x is SporadicScheduleConfig {
	assertProperty(x, 'startDate')
	// x must be a string of YYYY-MM-DD format
	CalendarDateString.assert(x.startDate, 'assertSporadicScheduleConfig')

	assertProperty(x, 'interval')
	const intervalOK : boolean	= Array.isArray(x.interval) && x.interval.every(i => typeof i === 'number')
	assert(intervalOK, 'assertSporadicScheduleConfig: x.interval must be of type number[]')

    assert(Object.keys(x).length > 1 && Object.keys(x).length < 5, 'assertSporadicScheduleConfig: Has too many properties, expected 2–4')
}
