import	{
			CalendarDateString,
			assert
		}									from '../../utils'
import	{
			ScheduleConfig,
			isScheduleConfig,
			ParsedIsoString,
			isPeriodicScheduleConfig,
			SporadicScheduleConfig,
			PeriodicScheduleConfig,
		} 									from './interfaces'
import	{
			DayConfig,
			TimeOfDay,
			TimeSlot
		}									from './time-of-day.class'
import	{
			Item
		}									from '../item.class'
import	{
			addDays,
			isAfter,
			isWithinInterval,
			subDays
		}									from 'date-fns'


export class Schedule extends Item<ScheduleConfig>{
	// STATIC:

	public 	static acceptsAsConfig : typeof isScheduleConfig	= 	isScheduleConfig
	public 	static timeOfDay? : TimeOfDay		= 	new TimeOfDay([
											// Sunday:
											{
												'morning': 		10,
												'afternoon':	13,
												'evening':		18,
												'night':		23
											},
											// weekdays:
											...Array<DayConfig>(5).fill({
												'morning': 		8,
												'afternoon':	12,
												'evening':		17,
												'night':		22
											}),
											// Saturday:
											{
												'morning': 		11,
												'afternoon':	14,
												'evening':		19,
												'night':		23
											},
										])

	public static assertData(data:unknown): asserts data is ScheduleConfig {
		assert(this.acceptsAsConfig(data), 'Schedule#assertData(): bad data')
	}

	public static parse(str: string) : ParsedIsoString | null {

		if(typeof str !== 'string') return null

		const match : RegExpMatchArray = str.match(/^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}).*([+\-Z])(\d{2})?:?(\d{2})?$/i)

		if(!match) return null

		const s : number[]	=	match.slice(1)
								.map( (x : string)  =>	['+','-','Z'].includes(x)
										?	({ '+': 1, '-': -1, 'Z': 0 })[x]
										:	parseInt( x || '0' , 10)
								)

		const local_date : Date = new Date(s[0], s[1]-1, s[2], s[3], s[4], 0, 0)

		if(isNaN(local_date.getTime())) return null

		return 	{
					timezoneOffset:		s[5]*(s[6]+s[7]/60),
					dayOfWeek:			local_date.getDay(),
					timeOfDay:			this.timeOfDay.at(local_date).key,
					asLocalDate: 		local_date
				}

	}

	public static hoursToTimestring(h:number) : string {
		const offsetSign	: number 	= Math.sign(h)
		const offsetAbs		: number	= Math.abs(h)
		const offsetHours 	: number	= Math.floor(offsetAbs)
		const offsetMinutes : number	= Math.floor( (offsetAbs-offsetHours) *60 )

		return 	(offsetSign < 0 ? '-' : '+')
				+	String(offsetHours).padStart(2, '0')
				+	':'
				+	String(offsetMinutes).padStart(2, '0')
	}

	public static getIsoStringWithTimezone(date: Date = new Date()): string {

		return 		String(date.getFullYear())
				+ 	'-'
				+	String(date.getMonth() + 1).padStart(2,'0')
				+	'-'
				+	String(date.getDate()).padStart(2,'0')
				+	'T'
				+	String(date.getHours()).padStart(2,'0')
				+ 	':'
				+	String(date.getMinutes()).padStart(2,'0')
				+	this.hoursToTimestring(-1* date.getTimezoneOffset() / 60)

	}


	// INSTANCE

	// TODO change into Sets

	public daysOfWeek		: number[]
	public timesOfDay		: string[]
	public endDate			: string
	public sporadicConfig	: SporadicScheduleConfig

	public constructor(config?: ScheduleConfig){
		super(config || [[],[]])
	}

	/**
	 * Returns a new Schedule created from a copy of the config of the passed schedule
	 */
	public static copy(schedule: Schedule): Schedule {
		const  { config } = schedule
		return Schedule.fromConfig(config)
	}

	/**
	 * Returns a new Schedule from a copy of the passed config
	 */
	public static fromConfig(config: ScheduleConfig): Schedule {
		const clone : ScheduleConfig = structuredClone(config)
		return new Schedule(clone)
	}

	/**
	 * Sets either `daysOfWeek`, `timesOfDay` and `endDate` (if {@link PeriodicScheduleConfig})
	 * or `sporadicConfig` (if {@link SporadicScheduleConfig})
	 */
	public set config(config: ScheduleConfig) {
		if (!Schedule.acceptsAsConfig(config)) throw new Error ('Schedule.set config(): Invalid Schedule config.')

		const clone : ScheduleConfig = structuredClone(config)

		if (isPeriodicScheduleConfig(clone)) {
			this.daysOfWeek = clone[0]
			this.timesOfDay = clone[1]
			if (clone.length === 3) this.endDate = clone[2]
		}

		else this.sporadicConfig = clone
	}

	/**
	 * Returns either `[daysOfWeek, timesOfDay, endDate]` (if {@link PeriodicScheduleConfig})
	 * or `sporadicConfig` (if {@link SporadicScheduleConfig})
	 */
	public get config() : ScheduleConfig {
		if (this.sporadicConfig) return structuredClone(this.sporadicConfig)

		const periodicConfig : PeriodicScheduleConfig = [
															[...this.daysOfWeek || []],
															[...this.timesOfDay || []]
														]

		if (this.endDate) periodicConfig.push(this.endDate)

		return periodicConfig
	}

	/**
	 * Checks if the schedule is daily
	 */
	public get everyDay(): boolean {
		return	[0,1,2,3,4,5,6].every( day => this.daysOfWeek.includes(day) )
				||
				[0,1,2,3,4,5,6].every( day => !this.daysOfWeek.includes(day) )
	}

	/**
	 * Checks if the schedule is valid all day long or only at a certain time
	 */
	public get allDay() : boolean {
		return	this.timesOfDay.length === 0
	}

	/**
	 * Checks if targetDate matches the schedule.
	 *
	 * If the schedule is periodic, `targetDate` must match the day of the week and time of day.
	 *
	 * If the schedule is sporadic, use {@link SporadicScheduleConfig.matchesDaysBefore}
	 * (defaults to `0`) and -After (defaults to `14`) to calculate a time interval
	 * for each of the schedule's due dates.
	 * Returns true if `targetDate` falls into any of the time intervals.
	 * Sporadic schedules don't consider the time of day.
	 *
	 * @example this.sporadicConfig = { startDate: '2023-10-01', interval: [10, 20] }
	 * => dueDates = [2023-10-11, 2023-10-21]
	 * => dueIntervals = [{ start: 2023-10-11, end: 2023-10-25 }, { start: 2023-10-21, end: 2023-11-04 }]
	 */
	public matchesDate(targetDate: Date = new Date()) : boolean {
		// Sporadic schedule
		if (this.sporadicConfig) {
			// Get the intervals in which sporadic config matches
			// = from (dueDate - matchesDaysBefore) to (dueDate + matchesDaysAfter)
			const dueIntervals : Interval[] = this.getDueIntervals()

			return dueIntervals.some(
				(dueInterval: Interval) =>
					isWithinInterval(targetDate, dueInterval)
			)
		}

		// Periodic schedule
		const time_slot 		: TimeSlot 	= Schedule.timeOfDay.at(targetDate)
		const day 				: number	= time_slot.startDate.getDay()
		const time_of_day		: string	= time_slot.key

		return 	(this.daysOfWeek.length === 0 || this.daysOfWeek.includes(day))
				&&
				(this.timesOfDay.length === 0 || this.timesOfDay.includes(time_of_day))
				&&
				(!this.hasEnded(targetDate))
	}

	/**
	 * Checks if a periodic schedule has ended, i.e. is not recurring anymore.
	 */
	public hasEnded(currentDate: Date = new Date()): boolean {
		// endDate is the last day the schedule is active
		// firstInactiveDate is the first day for which hasEnded() is true
		const firstInactiveDate : Date = addDays(CalendarDateString.toDate(this.endDate), 1)
		// isAfter() also checks for time, firstInactiveDate has 00:00 as time
		return this.endDate && isAfter(currentDate, firstInactiveDate)
	}

	/**
	 * Calculates all date intervals during which the sporadic schedule is due.
	 */
	public getDueIntervals(): Interval[] {
		if (!this.sporadicConfig) return []

		const configDate : Date = CalendarDateString.toDate(this.sporadicConfig.startDate)

		const dueDates : Date[] =  this.sporadicConfig.interval.map(
			day => addDays(configDate, day)
		)

		return dueDates.map(
			(dueDate: Date) =>
				({
					start	: subDays(dueDate, this.sporadicConfig.matchesDaysBefore ?? 0),
					end		: addDays(dueDate, this.sporadicConfig.matchesDaysAfter ?? 14)
				})
		)
	}
}
