import { assert, assertProperty, isErrorFree } from '../../utils'

export interface GlossaryEntryConfig {
	language:		string
	title:			string
	lastUpdate:		string // ISO8601, YYYY-MM-DD for now
	content:		string[]
	authors:		string[]
	tags:			string[]
}

export function assertGlossaryEntryConfig(config: unknown): asserts config is GlossaryEntryConfig {

	assertProperty(config, ['language', 'title', 'lastUpdate', 'content', 'authors', 'tags'])

	assert(typeof config.language ==='string', 		'assertGlossaryEntryConfig: .language must be a string')
	assert(typeof config.title ==='string', 		'assertGlossaryEntryConfig: .title must be a string')
	assert(typeof config.lastUpdate === 'string', 	'assertGlossaryEntryConfig: .lastUpdate must be a string')
	assert(Array.isArray(config.content), 				'assertGlossaryEntryConfig: .content must be an array ')
	assert(config.content.every(x => typeof x === 'string'),	'assertGlossaryEntryConfig: .content must be an array of strings ')
	assert(Array.isArray(config.authors), 			'assertGlossaryEntryConfig: .authors must be an array')
	assert(Array.isArray(config.tags), 				'assertGlossaryEntryConfig: .tags must be an array')

	const authorsAreStrings	: boolean	= config.authors.every(author => typeof author ==='string')
	const tagsAreStrings	: boolean	= config.tags.every(tag => typeof tag ==='string')

	assert(authorsAreStrings,						'assertGlossaryEntryConfig: .authors must only contain string elements')
	assert(tagsAreStrings,							'assertGlossaryEntryConfig:.tags must only contain string elements')
}

export function isGlossaryEntryConfig(config: unknown): config is GlossaryEntryConfig {
	return isErrorFree(() => assertGlossaryEntryConfig(config), true)
}
