import	{	adHocId				}	from '../../utils'

import	{
			ItemStore,
			ItemStorage
		}							from '../item-store.class'

import	{
			Entry,
		}							from './entry.class'



function identifyItemBy(item: Entry): string {

	if(item.id) return item.id

	return item.id = adHocId()

}

export class EntryStore extends ItemStore<Entry>{

	public constructor(
		storage: ItemStorage<Entry>,
	){
		super({
			itemClass: 		Entry,
			identifyItemBy,
			storage,
		})
	}



	public async filter(ids: string[], 	startDate?: Date, endDate?:	Date): Promise<Entry[]>
	public async filter(id: string,		startDate?: Date, endDate?:	Date): Promise<Entry[]>
	public async filter(				startDate:	Date, endDate:	Date): Promise<Entry[]>

	public async filter(x:string|string[]|Date, y?: Date, z?:Date): Promise<Entry[]> {

		await this.ready

		if(typeof x === 'string') 	return await this.filter([x],	y, z)
		if(x instanceof Date)		return await this.filter([],	x, y)

		const ids				= x
		const startDate			= y
		const endDate			= z
		const filtered_by_id 	= await this.filterByIds(this.items, ids)

		return await this.filterByDate(filtered_by_id, startDate, endDate)
	}


	protected async filterByIds(entries: Entry[], ids: string[]): Promise<Entry[]> {
		await this.ready

		return entries.filter( entry => ids.includes(entry.questionId) )
	}

	protected async filterByDate(entries: Entry[], startDate: Date, endDate: Date): Promise<Entry[]> {

		await this.ready

		const start = startDate.getTime()
		const end	= endDate.getTime()

		return entries.filter( (entry: Entry) => {

			const date = new Date(entry.date).getTime()
			return start <= date && date < end

		})
	}

}
