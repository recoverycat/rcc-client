/**
 * This file encapsulates `date-fns` so if we want to replace it we can replace it in one place only.
 *
 * This means apart from this file no other file should import `date-fns`!
 * Instead use a function here and if it does not exist add it.
 *
 */
import 	{
	add							as  fnsAdd,
	addDays						as  fnsAddDays,
	eachDayOfInterval			as  fnsEachDayOfInterval,
	formatISO					as  fnsFormatISO,
	getDay						as  fnsGetDay,
	Interval					as  FnsInterval,
	isSameDay					as  fnsIsSameDay,
	nextSunday					as  fnsNextSunday,
	parseISO					as  fnsParseISO,
	previousMonday				as  fnsPreviousMonday,
	sub							as  fnsSub,
	subDays						as  fnsSubDays,
	startOfToday				as  fnsStartOfToday,
	parse						as  fnsParse,
	format						as  fnsFormat,
	startOfMonth				as  fnsStartOfMonth,
	endOfMonth					as  fnsEndOfMonth,
	isWithinInterval			as  fnsIsWithinInterval,
	isBefore					as  fnsIsBefore,
	isAfter						as  fnsIsAfter,
	isToday						as  fnsIsToday,
	endOfDay					as  fnsEndOfDay,
	startOfDay					as  fnsStartOfDay,
	differenceInCalendarDays	as	fnsDifferenceInCalendarDays,
	differenceInMinutes			as	fnsDifferenceInMinutes,
	isDate						as  fnsIsDate,
}                                                   from 'date-fns'



/**
 * Ensure that date is a Date or a string in valid ISO8601
 */
function ensureDate(date: string | Date): Date {
	return typeof date === 'string' ? fromISO8601CalendarDate(date) : date
}

/**
 *
 * @returns	{Date[]}	An Array with dates representing the start of every day between
 *						and including the days startDate and endDate represent
 */
export function range(start: string | Date, end: string | Date): Date[] {
	const startDate : Date  = ensureDate(start)
	const endDate   : Date  = ensureDate(end)

	const interval: FnsInterval = { start: startDate, end: endDate }
	return fnsEachDayOfInterval(interval)
}

/**
 * Gets the number of the day of week of a given date string (YYYY-MM-DD). 0 for Sunday.
 */
export function getDayOfWeek(date: string | Date): number {
	return fnsGetDay(ensureDate(date))
}

/**
 * Add a specific amount of days to a date
 * @param   {string | Date}    date    Date object or string representing a date in ISO8601
 */
export function daysAfter(date: string | Date, amountOfDays: number): Date {
	return fnsAddDays(ensureDate(date), amountOfDays)
}

/**
 * Subtracts a specific amount of days from a date
 * @param   {string | Date}    date    Date object or string representing a date in ISO8601
 */

export function daysBefore(date: string | Date, amountOfDay: number): Date {
	return fnsSubDays(ensureDate(date), amountOfDay)
}

/**
 * Transform a date into an ISO8601 calendar date string (YYYY-MM-DD)
 */
export function toISO8601CalendarDate(date: Date): string {
	return fnsFormatISO(date, { representation: 'date' })
}

/**
 * Transform an ISO8601 calendar date string (YYYY-MM-DD) into a Date
 */
export function fromISO8601CalendarDate(iso8601CalendarDateString: string): Date {
	return fnsParseISO(iso8601CalendarDateString)
}

/**
 * return Date representing start of this day in local time
 */
export function today(): Date {
	return fnsStartOfToday()
}

/**
 * Get the start of the first Monday previous to given date. (If the given date is a Monday go back one week)
 */
export function previousMonday(date: string | Date): Date {
	return fnsPreviousMonday(ensureDate(date))
}

/**
 * Get the start of the next Sunday after the given date. (If the given date is a Sunday advance one week)
 * @returns {Date} the next Sunday after given date
 */
export function nextSunday(date: string | Date): Date {
	return fnsNextSunday(ensureDate(date))
}

export function isToday(date: Date | number): boolean {
	return fnsIsToday(date)
}

export function isSameDay(first: string | Date, second: string | Date) : boolean {
	return fnsIsSameDay(ensureDate(first), ensureDate(second))
}


export function parse(dateString: string, formatString: string): Date {
	return fnsParse(dateString, formatString, new Date())
}

export function format(date: Date|number, formatString: string): string {
	return fnsFormat(date, formatString)
}

export function startOfMonth(date: Date|number): Date {
	return fnsStartOfMonth(date)
}

export function endOfMonth(date: Date|number): Date {
	return fnsEndOfMonth(date)
}

export function isWithinInterval(date: Date|number, interval: FnsInterval): boolean {
	return fnsIsWithinInterval(date, interval)
}

export function parseInterval(start: Date|string|number, end: Date|string|number, formatString?: string): FnsInterval|undefined {
	if((typeof start === 'string' || typeof end === 'string') && typeof formatString !== 'string')
		return

	start = typeof start === 'string' ? parse(start, formatString) : start
	end = typeof end === 'string' ? parse(end, formatString) : end

	return { start, end } as FnsInterval
}

export function isDayBefore(day: Date, compareDay: Date): boolean {
	return fnsIsBefore(day, fnsStartOfDay(compareDay))
}

export function isDayAfter(day: Date, compareDay: Date): boolean {
	return fnsIsAfter(day, fnsEndOfDay(compareDay))
}

export function add(date: Date, duration: Duration): Date {
	return fnsAdd(date, duration)
}

export function sub(date: Date, duration: Duration): Date {
	return fnsSub(date, duration)
}

export function isDate(date: unknown): date is Date {
	return fnsIsDate(date)
}

export function differenceInCalendarDays(laterDate : Date, earlierDate: Date) : number {
	return fnsDifferenceInCalendarDays(laterDate, earlierDate)
}

export function differenceInMinutes(laterDate: Date, earlierDate: Date): number {
	return fnsDifferenceInMinutes(laterDate, earlierDate)
}
