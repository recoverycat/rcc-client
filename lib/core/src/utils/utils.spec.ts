import { generateRandomSixDigitCode, isApplicationStable } from './utils'
import { BehaviorSubject } from 'rxjs'
import { ApplicationRef } from '@angular/core'

describe('utils', () => {

	describe('generateRandomSixDigitCode', () => {

		it('should generate a valid six-digit code within the valid range', () => {
			const code : number = generateRandomSixDigitCode()

			expect(typeof code).toBe('number')

			expect(code).toBeGreaterThanOrEqual(100000)
			expect(code).toBeLessThanOrEqual(999999)
		})

	})

	describe('isApplicationStable', () => {
		let applicationRef: ApplicationRef = undefined

		beforeEach(() => {
			applicationRef = {
				isStable: new BehaviorSubject<boolean>(false) // Initial state: unstable
			} as unknown as ApplicationRef
		})

		it('should resolve when the application becomes stable', (done) => {
			const isStableSubject: BehaviorSubject<boolean> = applicationRef.isStable as BehaviorSubject<boolean>

			setTimeout(() => isStableSubject.next(true), 100)

			void isApplicationStable(applicationRef).then(() => {
				expect(isStableSubject.getValue()).toBe(true)
				done()
			}).catch(done.fail)
		})

	})

})
