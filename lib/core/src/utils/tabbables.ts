
/**
 * Array of HTML attributes that have an effect on elements regarding
 * their tabbable status. Might be incomplete.
 */
export const tabAffectingAttributes	: string[]
									= ['href', 'tabindex','disabled']

/**
 * CSS selector that matches tabbable elements. Might be incomplete.
 */
export const tabbableSelector		: string
									= 'a[href], button, input, [tabindex], textarea'

/**
 * Returns an Array of tabbable descendant elements of the provided element.
 * The selector might need an extension in the future, if I missed some tabbable
 * elements.
 *
 * There are many reasons for the list of tabbable elements to change after a
 * call of this method (.e.g, DOM changes, visibility changes, disabled state)!
 *
 * This function makes use of the expensive .getComputedStyle calls. Make sure
 * to call it sparsely.
 */
export function getTabbableDescendants(element: HTMLElement) : HTMLElement[] {

	const relevantNodeList	:	NodeList
							=	element.querySelectorAll(tabbableSelector)

	const relevantElements	:	HTMLElement[]
								// If a node matches the selector above,
								// it must be a HTMLElement
							=	Array.from(relevantNodeList) as HTMLElement[]

	// Remove elements that are disabled or specifically removed from tab order:
	const filteredElements	:	HTMLElement[]
							=	relevantElements
								.filter( (el : HTMLElement) => {

									if(el.tabIndex <= -1)					return false
									if('disabled' in el && el.disabled)		return false
										
									return true
								})

	// Remove elements that are not visible:
	const visibleElements	: 	HTMLElement[]
							=	filteredElements
								.filter( (el : HTMLElement) => {
									// Check for body or html, or if some parent
									// has display:none and element position is not fixed
									// See https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/offsetParent
									if(el.offsetParent === null) 		return false

									// This is very expensive:
									const style : CSSStyleDeclaration
												= window.getComputedStyle(el)

									if(style.display 	=== 'none') 	return false
									if(style.visibility !== 'visible')	return false

									return true
								})

	// Sort elements in tab order:
	const sortedElements	: 	HTMLElement[]
							= 	visibleElements
								.sort( (el_1 : HTMLElement, el_2 : HTMLElement) => {

									const tabIndex_1 	: number
														= el_1.tabIndex || 0

									const tabIndex_2 	: number
														= el_2.tabIndex || 0

									// Elements with same tabindex, stay in original DOM order:
									if(tabIndex_1 === tabIndex_2)	return 0

									// Tabindex 0 means last in order:
									if(tabIndex_1 === 0)			return  1
									if(tabIndex_2 === 0)			return -1

									if(tabIndex_1 < tabIndex_2)		return -1
									if(tabIndex_2 < tabIndex_1)		return  1

								})

	return sortedElements
}
