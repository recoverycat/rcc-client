import { base64URIComponentToObject, objectToBase64URIComponent } from './utils'

describe('Base64 URI Component Utilities', () => {
	const testObject : object = {
		str: 'string',
		num: 123,
		bool: true,
		arr: [1, '2', false],
		obj: { nested: 'value' }
	}

	const base64String : string = 'eyJzdHIiOiJzdHJpbmciLCJudW0iOjEyMywiYm9vbCI6dHJ1ZSwiYXJyIjpbMSwiMiIsZmFsc2VdLCJvYmoiOnsibmVzdGVkIjoidmFsdWUifX0='
	const base64URIComponent : string = encodeURIComponent(base64String)

	describe('base64URIComponentToObject()', () => {
		it('should decode a base64 URI component to an object', () => {
			expect(base64URIComponentToObject(base64URIComponent)).toEqual(testObject)
		})

		it('should throw an error for an invalid base64 URI component', () => {
			const invalidBase64URIComponent : string = '%E3%28%25%29'

			expect(() => base64URIComponentToObject(invalidBase64URIComponent)).toThrow()
			expect(base64URIComponentToObject).toThrow() // argument is undefined
		})
	})

	describe('objectToBase64URIComponent()', () => {
		it('should encode an object to a base64 URI component', () => {

			expect(objectToBase64URIComponent(testObject)).toEqual(base64URIComponent)
		})

	})

	describe('Chained function calls', () => {
		it('should return the original values', () => {

			const now : Date = new Date()

			expect(base64URIComponentToObject(objectToBase64URIComponent('ABC'))).toEqual('ABC')
			expect(base64URIComponentToObject(objectToBase64URIComponent(''))).toEqual('')
			expect(base64URIComponentToObject(objectToBase64URIComponent(123))).toEqual(123)
			expect(base64URIComponentToObject(objectToBase64URIComponent(true))).toEqual(true)
			expect(base64URIComponentToObject(objectToBase64URIComponent(false))).toEqual(false)
			expect(base64URIComponentToObject(objectToBase64URIComponent([]))).toEqual([])
			expect(base64URIComponentToObject(objectToBase64URIComponent({}))).toEqual({})
			expect(base64URIComponentToObject(objectToBase64URIComponent(now))).toEqual(now.toJSON())
			expect(base64URIComponentToObject(objectToBase64URIComponent(testObject))).toEqual(testObject)
		})
	})
})
