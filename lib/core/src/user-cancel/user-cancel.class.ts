import {
			Observable,
			Observer,
		}									from 'rxjs'

export class UserCanceledError extends Error {}

/**
 * TODO: Needs documentation; also, is this used anywhere?
 */
export class UserCancel extends Observable<string> implements PromiseLike<void> {


	protected promise			: Promise<string>
	protected resolvePromise	: (str:string) => void
	protected observers			: Set<Observer<string>> = new Set()

	public canceled = false

	public static UserCanceledError = UserCanceledError


	public constructor(
		protected reason	: string = 'UserCancel: no reason given'
	){

		super( (observer: Observer<string>) => {

			if(this.canceled){

				observer.next(this.reason)
				observer.complete()
				return () => {}

			} else {

				this.observers.add(observer)
				return () => { this.observers.delete(observer) }
			}

		})

		this.promise		=	new Promise( resolve => this.resolvePromise = resolve )

	}


	public then(
		onfulfilled?	: (() => (PromiseLike<never> | never)) | undefined | null,
		onrejected?		: ((reason: any) => never | PromiseLike<never>) | undefined | null,

	): PromiseLike<never> {
		return this.promise.then( () => Promise.reject(new UserCanceledError(this.reason) ) )
	}


	public cancel(reason?:string): void {

		this.canceled = true

		this.reason = reason || this.reason

		this.resolvePromise(this.reason)

		this.observers.forEach( observer => {

			observer.next(this.reason)
			observer.complete()
			this.observers.delete(observer)

		})


	}

}
