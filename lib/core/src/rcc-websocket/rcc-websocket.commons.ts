import	{
			EncryptionHandler
		}							from '../encryption'

import	{
			isErrorFree,
			assert,
			assertProperty
		}							from '../utils'



export interface RccWebSocketConfig {
	url					: string,
	channel				: string,
	encryptionHandler	: EncryptionHandler,
	timeout?				: number
}


export function assertRccWebSocketConfig(x : unknown) : asserts x is RccWebSocketConfig {

	assertProperty(x, 'url')

	assert(typeof x.url === 'string',		'isRccWebSocketConfig: url must be a string.')
	assert(x.url.match(/^wss:/), 			'isRccWebSocketConfig: url must start with \'wss:\'.')

	assertProperty(x, 'channel')

	assert(typeof x.channel === 'string',	'isRccWebSocketConfig: channel must be a string.')


	assert(['undefined', 'number'].includes(typeof (x as Record<string, unknown>).timeout),
											'isRccWebSocketConfig: timeout must be a number or undefined.')

	assertProperty(x, 'encryptionHandler',	'isRccWebSocketConfig: missing encryptionHandler.')

	assert(x.encryptionHandler instanceof EncryptionHandler,
											'isRccWebSocketConfig: encryptionHandler, invalid type.')

}



export function isRccWebSocketConfig(x : unknown) : x is RccWebSocketConfig {

	return isErrorFree( () => assertRccWebSocketConfig(x) )

}
