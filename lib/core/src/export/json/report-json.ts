import { Entry, Question, QuestionOptionConfig, Report } from '../../items'

/**
 * One entry/answer, its corresponding question, date and pseudonym
 */
export interface QuestionnaireResponse {
	pseudonym		: string
	date			: string					// when the answer was given
	questionId		: string					// = question.id
	questionWording	: string
	answerRaw		: string|number|boolean		// = entry.answer
	answerWording	: string
}

/**
 * Converts a report to an easily readable format containing all the important information, {@link QuestionnaireResponse}
 * @param report 		report that should be exported (= a set of answers)
 * @param questions 	array of questions from the app that are answered in the report
 * @param languageCode 	language of the report, e.g. 'en' or 'de'
 * @param pseudonym		patient ID, defaults to '0000'
 */
export function report2json(report: Report, questions: Question[], languageCode: string, pseudonym: string = '0000'): QuestionnaireResponse[] {
	return report.entries.map(entry => {
			// Find questions that were actually answered in the Report
			const question	: Question = questions.find(q => q.id === entry.questionId)
			return {
				pseudonym		: pseudonym,
				date			: entry.date,
				questionId		: question.id,
				questionWording	: question.translations[languageCode],
				answerRaw		: entry.answer,
				answerWording	: getAnswerWording(entry, question, languageCode)
			}
		}
	)
}

/**
 * Finds the human-readable wording of an entry as it was presented to the questionnaire respondent.
 * Boolean questions are answered as "true" or "false" regardless of language setting.
 * @param entry The entry (answer) to find the wording for
 * @param question The question that was answered
 * @param lang Language code, e.g. 'en' or 'de'
 */
function getAnswerWording(entry: Entry, question: Question, lang: string): string {
	const answerRaw		: string|number|boolean	= entry.answer
	
	if (!question.options) return answerRaw.toString()

	const option	: QuestionOptionConfig	= question.options.find(opt => opt.value === answerRaw)
	return option && (option.translations?.[lang] || option.meaning ) || ''
}
