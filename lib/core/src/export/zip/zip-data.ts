import  {
			BlobWriter,
			TextReader,
			ZipWriter,
			ZipWriterConstructorOptions
											}	from '@zip.js/zip.js'
import	{	assert							}	from '../../utils'

/**
 * Creates a zip archive containing data.
 * @param content The string(s) to be zipped
 * @param filename Name of the file(s) in the zip archive (NOT the zip archive itself).
 * Must correspond to `content` in order and length.
 * @param password Password for the zip archive. Empty string means no encryption
 * @returns A blob containing the zip archive
 */
export async function zip(content: string, filename: string, password: string): Promise<Blob>
export async function zip(content: string[], filename: string[], password: string): Promise<Blob>
export async function zip(content: string|string[], filename: string|string[], password: string): Promise<Blob> {
	const zipBlobWriter	: BlobWriter		= new BlobWriter()
	const zipWriter		: ZipWriter<Blob>	= new ZipWriter(zipBlobWriter)

	const options : ZipWriterConstructorOptions = {
		password:			password,	// empty string means no password
		encryptionStrength:	3,			// maximum AES encryption strength
		level: 				0,			// no compression
	}

	// Put strings in arrays for iteration
	if (typeof content === 'string' && typeof filename === 'string') {
		content = [content]
		filename = [filename]
	}

	assert(content.length === filename.length)
	// Add each element of content to the zip archive with the corresponding filename
	for (let i: number = 0; i < content.length; i++) await zipWriter.add(filename[i], new TextReader(content[i]), options)

	await zipWriter.close()
	return await zipBlobWriter.getData()
}
