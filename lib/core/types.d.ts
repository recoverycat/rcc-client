import { AbstractControl } from '@angular/forms'

export type PropertiesOf<T> = { [P in keyof T]?: T[P] }

export type FormGroupOf<FormDataType> = { [DataType in keyof FormDataType]: AbstractControl<FormDataType<DataType>> }

