This directory is meant for everything explicitly Recovery Cat related, that does not make use of external frameworks like angular or ionic or such.

Anything that is tailored to meet specific Recovery Cat needs, should go into this folder.
