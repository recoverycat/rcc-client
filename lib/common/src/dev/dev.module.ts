import 	{
			NgModule,
			ModuleWithProviders
		}								from '@angular/core'

import	{
			RouterModule,
			Routes
		}								from '@angular/router'

import	{
			TranslationsModule,
			provideTranslationMap
		}								from '../translations/ng'

import	{
			MainMenuModule,
		}								from '../main-menu'
import	{	SharedModule			}	from '../shared-module'

import	{	DevWarnings				}	from './dev.commons'
import	{	DevService				}	from './dev.service'


import	{	DevPageComponent		}	from './dev.page/dev.page'

import	{	ConsolePipe				}	from './dev.pipes'


import en from './i18n/en.json'
import de from './i18n/de.json'

const routes: Routes	=	[
								{ path: 'dev',	component: DevPageComponent	},
							]

export const DevHomePath	: string = '/dev'



@NgModule({
	declarations: [
		DevPageComponent,
		ConsolePipe,
	],
	imports: [
		SharedModule,
		RouterModule.forChild(routes),
		MainMenuModule,
		TranslationsModule

	],
	exports: [
		DevPageComponent,
		ConsolePipe,
	],
	providers: [
		DevService,
		{ provide: DevWarnings, useValue: { name: 'DevModule imported', note: 'Some other module imported DevModule' }, multi:true },
		provideTranslationMap('DEV', { en,de }),
	]
})
export class DevModule {

	public constructor(){
		console.warn('DevModule in use!')
	}

	public static note(note: string): ModuleWithProviders<DevModule>{
		return 	<ModuleWithProviders<DevModule>>{
					ngModule: 	DevModule,
					providers: 	[
									{ provide: DevWarnings, useValue: { name: 'DevModule imported', note }, multi:true }
								]
				}
	}

}
