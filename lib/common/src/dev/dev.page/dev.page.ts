import	{	Component               }	from '@angular/core'
import	{	DevWarnings				}	from '../dev.commons'

@Component({
	selector: 		'rcc-dev.page',
	templateUrl: 	'./dev.page.html',
	styleUrls: 		['./dev.page.scss'],
    standalone:     false,
})
export class DevPageComponent {

	public constructor(
		public warnings : DevWarnings
	) { }
}
