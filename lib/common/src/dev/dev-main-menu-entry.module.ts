import	{
			NgModule,
			Provider,
			ModuleWithProviders,
		}											from '@angular/core'

import	{
			Action,
			getEffectivePosition,
			WithPosition
		}											from '../actions'

import	{
			provideMainMenuEntry,
		}											from '../main-menu'

import	{
			DevModule,
			DevHomePath
		}											from './dev.module'

@NgModule({
	imports: [
		DevModule,
	],
})

export class DevMainMenuEntryModule {

	/**
	* This method can add entries to the main menu.
	*
	* Calling it without parameter, will add entries to the main menu
	* automatically at reasonably adequate positions.
	*
	* If you want to determine the positions yourself provide a `config` parameter :
	*
	* | .position  | value                  | effect                                                    |
	* |------------|------------------------|-----------------------------------------------------------|
	* | .position  | positive number        | adds menu entry at position counting from the top         |
	* |            | negative number        | adds menu entry at position counting from the bottom      |
	*
	* Example:     `DevMainMenuEntryModule.addEntry({ position: 1 })`,
	*/

	public static addEntry(config?: WithPosition): ModuleWithProviders<DevMainMenuEntryModule> {

		const mainMenuEntry	:Action =	{
				icon: 			'bug',
				label: 			'DEV.MENU_ENTRY',
				path:			DevHomePath,
				position:		getEffectivePosition(config, -1),
		}

		const providers : Provider[] = [provideMainMenuEntry(mainMenuEntry)]

		return {
			ngModule:	DevMainMenuEntryModule,
			providers
		}
	}

}
