import	{
			Pipe,
			PipeTransform
		}						from '@angular/core'


@Pipe({
    name: 'console',
    standalone: false
})
export class ConsolePipe implements PipeTransform {

	public transform(x: unknown, replace?: unknown): unknown {
		console.group('DevModule')
		// eslint-disable-next-line no-console
		console.log(typeof x, x.constructor.name)
		// eslint-disable-next-line no-console
		console.log(x)
		console.groupEnd()
		return 	replace !== undefined
				?	replace
				:	x
	}
}
