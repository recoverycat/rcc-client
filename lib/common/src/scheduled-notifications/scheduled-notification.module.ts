import	{
			NgModule,
		}											from '@angular/core'

import	{	RccServiceWorkerModule				}	from '../service-worker'

import	{	ScheduledNotificationService		}	from './scheduled-notification.service'
import	{
			RccNotificationSettingsEntryGroupModule
		}											from '@rcc/features/settings-entry-groups/notifications-settings-entry-group.module'


@NgModule({
	imports:[
		RccServiceWorkerModule,
		RccNotificationSettingsEntryGroupModule,
	],
	providers:[
		ScheduledNotificationService,
	]
})
export class ScheduledNotificationModule {}
