import	{	Component						} from '@angular/core'
import	{	ScheduledNotificationService	} from './scheduled-notification.service'
import	{	RccSwService					} from '../service-worker'


@Component({
	template: 	`
					<a
						(click)		= "showNotification()"
					>
						Trigger notification immediately
					</a>

					<br/>

					<a
						(click)		= "trigger(10)"
					>
						Trigger notification in 10 seconds
					</a>

					<br/>

					<a
						(click)		= "trigger(30)"

					>
						Trigger notification in 30 seconds
					</a>

					<br>

					<a
						(click)		= "trigger(60)"

					>
						Trigger notification in 60 seconds
					</a>

				`
})
export class ScheduledNotificationTestComponent {

		public constructor(
			private scheduledNotificationService 	: ScheduledNotificationService,
			private rccSwService					: RccSwService
		){}

		public showNotification(): void{
			void this.rccSwService.serviceWorkerRegistration.showNotification('Test notification')
		}

		public trigger(delay: number) : void {

			const now 			= new Date().getTime()
			const timestamp 	= now + 1000*delay
			const time			= new Date(timestamp).toLocaleTimeString()

			void this.scheduledNotificationService.schedule({ timestamp, title: 'Test - '+time })
		}
}

