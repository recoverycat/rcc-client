import 	{
			Injectable
		}						from '@angular/core'
import 	{
			ReplaySubject
		} 						from 'rxjs'
import	{
			BannerConfig
		} 						from '../banners.commons'


/**
 * This service hold the most recent banner data.
 */
@Injectable({
	providedIn: 'root'
})
export class RccBannerService extends ReplaySubject<BannerConfig> {
	public constructor() {
		super(1)
	}
}
