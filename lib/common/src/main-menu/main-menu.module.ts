import	{
			NgModule,
			ModuleWithProviders,
			Type,
		} 									from '@angular/core'
import	{	RouterModule				}	from '@angular/router'

import	{	CommonModule				}	from '@angular/common'
import 	{ 	IonicModule 				}	from '@ionic/angular'
import	{	TranslationsModule,
			provideTranslationMap
		}									from '../translations'
import	{	IconsModule					}	from '../icons'
import	{	MainHeaderModule			}	from '../main-header'
import	{	NotificationModule			}	from '../notifications'
import	{	UiComponentsModule			}	from '../ui-components'
import	{	MainMenuHeaderItemComponent	}	from './header-item/header-item.component'

import	{
			MAIN_MENU_CONFIG
		}									from './main-menu.commons'



import en from './i18n/en.json'
import de from './i18n/de.json'

const mainHeaderConfig: Array<{ component: Type<unknown>, position: number }> =
	[
		{ component: MainMenuHeaderItemComponent, position: 1 }
	]


@NgModule({
	declarations: [
		MainMenuHeaderItemComponent
	],

	imports: [
		RouterModule,
		CommonModule,
		IonicModule,
		TranslationsModule,
		IconsModule,
		NotificationModule.forChild(),
		MainHeaderModule.forChild(mainHeaderConfig),
		UiComponentsModule
	],

	exports: [
		MainMenuHeaderItemComponent
	],

	providers: [
		provideTranslationMap('MAIN_MENU', { en, de }),
		{ provide: MAIN_MENU_CONFIG, 	useValue: {} },
	]

})
export class MainMenuModule {
	public static forRoot(config: Record<string, unknown>): ModuleWithProviders<MainMenuModule> {

		return	{
			ngModule: 	MainMenuModule,
			providers:	[
							{ provide: MAIN_MENU_CONFIG, 	useValue:	config },
						]
		}

	}
}
