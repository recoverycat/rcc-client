import 	{ 	Component				}	from '@angular/core'
import	{	MainHeaderItemComponent }	from '../../main-header'
import	{	NotificationService		}	from '../../notifications'
import	{	MainMenuService			}	from '../main-menu.service'


@Component({
	templateUrl: 	'./header-item.component.html',
	styleUrls: 		['./header-item.component.scss'],
    standalone:		false
})
export class MainMenuHeaderItemComponent extends MainHeaderItemComponent {

	public constructor(
		public notificationService: NotificationService,
		private readonly mainMenuService: MainMenuService
	){
		super()
	}

	protected onMenuClick(): void {
		this.mainMenuService.setIsMenuOpen(true)
	}
}



