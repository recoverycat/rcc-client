import	{
			Injectable,
			Inject,
			Optional
		}										from '@angular/core'

import	{
			HttpClient
		}										from '@angular/common/http'

import	{
			Observable,
			firstValueFrom,
		}										from 'rxjs'

import	{
			deflateObject,
			inflateObject,
			mergeObjects,
			assert
		}										from '@rcc/core'

import	{
			RequestedConfigValue,
			RUNTIME_CONFIG_REQUESTED_VALUES,
			assertConfigPath
		}										from './public-runtime-config.commons'


/**
 * Path to the config file, added after deployment.
 * This might be replaced by an API call  in the future.
 */
export const pathToPublicRuntimeConfig 		: string
											= '/local/config.json'

/**
 * This service provides access to configuration data,
 * that is only available at runtime (and not at build time).
 *
 * The setup of the configuration data, varies with the modules
 * included in the build. That's why modules making use of this
 * service have to announce beforehand what kind of values they
 * expect to get from the config
 * (see {@link requestPublicRuntimeConfigValue}).
 * This way we can create and
 * and output an example configuration when the app is run.
 *
 * When testing/developing you can overwrite this service with
 * a child class replacing `.loadConfig` with a method returning
 * the desired config data. See {@link MockRccPublicRuntimeConfigModule}.
 *
 * @example
 *
 * ```ts
 * @Injectable()
 * export class ServiceMakingUseOfRuntimeConfig{
 *
 *   constructor(
 *   	private rccPublicRuntimeConfigService: RccPublicRuntimeConfigService
 *   ){
 *
 *     // The value for this path is only available,
 *     // if it was requested beforehand, see below.
 *
 *     rccPublicRuntimeConfigService
 *    .get('my.path.to.config.value')
 *    .then(
 *       // value could be retrieved from the runtime config:
 *       value => console.log(value),
 *
 *       // Value was not part of the config, config could
 *       // not be loaded or the value was not properly requested
 *       // beforehand:
 *       error => handleError(error)
 *    )
 *   }
 *
 * }
 *
 * @NgModule({
 *   provides:[
 *     ServiceMakingUseOfRuntimeConfig
 *   ],
 *   imports:[
 *      // provides RccPublicRuntimeConfigService
 * 		RccPublicRuntimeConfigService,
 *
 *      //
 *      requestPublicRuntimeConfigValue({
 *        path:         'my.path.to.config.value',
 *        description:  'Used in some popup for my example feature.',
 *        type:         'string',
 *        required:     'false'
 *      })
 *   ]
 * })
 * export class ModuleMakingUseOfRuntimeConfig{}
 * ```
 *
 */
@Injectable()
export class RccPublicRuntimeConfigService {

	/**
	 * Example config, based on all requested values.
	 */
	public 	configSchema	: unknown

	private config 			: unknown
	public	ready			: Promise<void>

	public constructor(
		@Optional() @Inject(RUNTIME_CONFIG_REQUESTED_VALUES)
		protected 	requestedValues		: RequestedConfigValue[],
		private 	httpClient			: HttpClient,

	){
		this.ready = this.initialize()

		this.requestedValues = requestedValues || []

		this.assertConsistentRequests(this.requestedValues)

		this.buildConfigSchema()

		console.groupCollapsed('RuntimeConfiguration')
		console.info('Config schema:', this.configSchema)
		console.groupEnd()
	}

	protected get requiredValues() : RequestedConfigValue[] {
		return	this.requestedValues
				.filter( requestedValue => requestedValue.required)
	}

	/**
	 * Load the configs. Overwrite this to change
	 * the source of the configuration.
	 */
	protected async loadConfig() : Promise<unknown> {

		const request	:	Observable<unknown>
						=	this.httpClient.get(pathToPublicRuntimeConfig, { responseType: 'json' })


		const result 	:	unknown
						=	await firstValueFrom(request)

		return result
	}

	public async initialize() : Promise<void> {

		const noValuesRequested	: boolean
								= !this.requestedValues || this.requestedValues.length === 0

		if(noValuesRequested) return undefined

		await 	this.loadConfig()
				.then(
					config 	=> this.handleConfig(config),
					error	=> this.handleLoadingError(error)
				)

	}


	/**
	 * Checks if the config is sound, and if so
	 * stores it with the service for upcoming use.
	 */
	protected handleConfig(config: unknown): void {

		const missingValues :	string[]
							=	this.requiredValues
								.map( requestedValue => requestedValue.path)
								.filter( path => deflateObject(config, path) === undefined )

		if(missingValues.length > 0) throw new Error(`PublicRuntimeConfig.handleConfig(): missing required values: ${String(missingValues)}.`)

		this.config = config
	}

	protected handleLoadingError(error: unknown) : void {

		error =	error instanceof Error
				?	error
				:	new Error(String(error))


		// If we do not need any values,
		// it's okay when the config file is missing:
		if(this.requiredValues.length === 0 ) return undefined

		const requiredPaths		: string[]
								= this.requiredValues.map(v => v.path)

		throw new Error(`RccPublicRuntimeConfigService: unable to load config file (from ${pathToPublicRuntimeConfig}), but some values are required! ${String(requiredPaths)}`, { cause: error } )
	}

	/**
	 * Returns a value for .configSchema containing a
	 * description of the respective requested value.
	 */
	protected getValuePlaceholder(requestedValue: RequestedConfigValue): string {

		const description	: 	string
							= 	requestedValue.description

		const type			: 	string
							= 	requestedValue.type

		const required		: 	boolean
							=	!!requestedValue.required

		const optional		:	string
							=	required
								?	''
								:	'[optional]'

		return `${optional} ${type},  ${description}`
	}

	/**
	 * @throws {Error} if there is no valid request for the
	 * provided path.
	 */
	protected assertValidRequestPath(path : string): void {

		const requestedValue	: 	RequestedConfigValue
								= 	this.requestedValues
									.find( rv => rv.path === path)

		assert(requestedValue, `RccPublicRuntimeConfigService.assertValidRequest() path was not requested: "${path}"; use requestPublicRuntimeConfigValue.`)

	}

	/**
	 * Makes sure that there are no conflicts in the requested
	 * values.
	 *
	 * @throws {Error} if some requests have conflicting paths
	 */
	protected assertConsistentRequests(requestedValues : RequestedConfigValue[]) : void {

		requestedValues.forEach( rv1 => {
			requestedValues.forEach( rv2 => {

				if(rv1 === rv2) return undefined

				if(rv1.path.includes(rv2.path))
					throw new Error(`RccPublicRuntimeConfigService: conflicting requested config values: "${rv1.path}", "${rv2.path}"`)

			})
		})
	}

	/**
	 * Creates an example config; since the structure
	 * of the configuration depends on the values requested
	 * by modules. We only know what the config should look
	 * like after build time: The example config can be
	 * logged on runtime, so that we at least then know what
	 * kind of config we can provide.
	 */
	protected buildConfigSchema() : void {

		let configSchema	: unknown
							= {}

		this.requestedValues.forEach( requestedValue => {

			const path 			: string
								= requestedValue.path

			assertConfigPath(requestedValue.path)

			const placeholder	: string
								= this.getValuePlaceholder(requestedValue)

			configSchema = mergeObjects(configSchema, inflateObject(placeholder, path) )

		})

		this.configSchema = configSchema
	}



	/**
	 * Returns the value for a given path.
	 * Waits for everything to be setup properly.
	 *
	 * @throws {Error}	if the config did not load properly
	 *
	 * @throws {Error}	if the value was not properly
	 * requested, see {@link requestPublicRuntimeConfigValue}.
	 *
	 * @throws {Error}	if no value can be retrieved
	 * for the provided path, even if it was optional.
	 */
	public async get(path: string) : Promise<unknown> {

		await this.ready

		this.assertValidRequestPath(path)

		const value	: unknown
					= deflateObject(this.config, path)

		assert(value !== undefined, `RccPublicRuntimeConfigService.get() unable to retrieve value for "${path}"`)

		return value
	}

}
