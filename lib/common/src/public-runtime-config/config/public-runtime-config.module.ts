import	{
			NgModule,
		}										from '@angular/core'

import	{ provideHttpClient, withInterceptorsFromDi }										from '@angular/common/http'

import	{
			RccPublicRuntimeConfigService
		}										from './public-runtime-config.service'


@NgModule({
	providers: [
        RccPublicRuntimeConfigService,
        provideHttpClient(withInterceptorsFromDi()),
    ]
})
export class RccPublicRuntimeConfigModule {}
