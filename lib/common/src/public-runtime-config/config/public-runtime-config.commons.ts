import	{
			InjectionToken,
			Provider
		}									from '@angular/core'

/**
 * Whenever a module requests a value from the runtime config,
 * it should describe what the value is needed for.
 *
 * Collecting requested values at build time is important,
 * because we only then know which modules will be used and with
 * it what the configuration file is supposed to look like.
 */
export interface RequestedConfigValue {
	/**
	 *  e.g. 'myConfigGroup.myConfigValue'
	 */
	path: 			string,

	/**
	 * What the requested value is needed for.
	 */
	description: 	string,

	/**
	 * Javascript type (as of typeof) or Interface.
	 */
	type: string,

	/**
	 * If truthy will throw an error when the app is loaded.
	 *
	 * If falsy the requesting module has to handle cases where
	 * the value is missing from the config file.
	 *
	 */
	required?:		boolean
}

export const RUNTIME_CONFIG_REQUESTED_VALUES			: InjectionToken<RequestedConfigValue[]>
														= new InjectionToken<RequestedConfigValue[]>('List of runtime values requested by various modules.')


export function assertConfigPath(path:string) : void {

	if(!path) 								throw new Error('assertValidPath: path must be a non empty string')
	if(!path.match(/^[a-zA-Z0-9_\-.]+$/) )		throw new Error(`assertValidPath: path must only consist of a-z, A-Z, 0-9, "_", "-", and ".", got "${path}" instead.`)

	const sections	: string[]
					= path.split('.')

	if(sections.some(s => s[0] === '_'))	throw new Error(`assertValidPath: path sections must start with "_", got "${path}" instead.`)

}


/**
 * Announce to the rest of the app, that
 * a module will need this config value during runtime.
 */
export function requestPublicRuntimeConfigValue(requestedConfigValue: RequestedConfigValue) : Provider {

	assertConfigPath(requestedConfigValue.path)

	return {
		provide:	RUNTIME_CONFIG_REQUESTED_VALUES,
					multi:		true,
					useValue:	requestedConfigValue
	}
}
