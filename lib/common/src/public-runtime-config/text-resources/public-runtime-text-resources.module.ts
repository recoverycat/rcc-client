import	{
			NgModule,
		}											from '@angular/core'

import	{ provideHttpClient, withInterceptorsFromDi } from '@angular/common/http'

import	{
			RccPublicRuntimeTextResourceService
		}											from './public-runtime-text-resources.service'


@NgModule({
	providers: [
        RccPublicRuntimeTextResourceService,
        provideHttpClient(withInterceptorsFromDi()),
    ]
})
export class RccPublicRuntimeTextResourceModule {}
