import	{
			Injectable,
			Inject,
			Optional
		}										from '@angular/core'

import	{
			HttpClient
		}										from '@angular/common/http'

import	{
			Observable,
			firstValueFrom,
		}										from 'rxjs'

import	{
			assert
		}										from '@rcc/core'

import	{
			RccTranslationService
		}										from '@rcc/common'

import	{
			RequestedTextResource,
			REQUESTED_RUNTIME_TEXT_RESOURCES,
		}										from './public-runtime-text-resources.commons'



/**
 * Path to assets that will only be available at runtime,
 * such as logos or markdown files that vary by domain.
 */
export const pathToPublicRuntimeTextResources 	: string
												= '/local'

/**
 * This service provides access to text resources,
 * that are only available at runtime (and not at build time).
 *
 * Modules making use of this
 * service have to announce beforehand what kind of resources they
 * expect to load (see {@link RequestedTextResource}).
 *
 * This way we can load them at startup and they can be cached
 * by the service worker. (If they were loaded later on, they would
 * not be pre-fetched and unavailable for offline use.)
 *
 * When testing/developing you can overwrite this service with
 * a child class replacing `.load` with a method returning
 * the desired data. See {@link MockRccPublicRuntimeTextResourceModule}.
 *
 * @example
 *
 * ```ts
 * @Injectable()
 * export class ServiceMakingUseOfRuntimeTextResources{
 *
 *   public logoSVG : string = undefined
 *
 *   constructor(
 *   	private rccPublicRuntimeTextResourceService: RccPublicRuntimeTextResourceService
 *   ){
 *
 *    this.logoSVG = rccPublicRuntimeTextResourceService.get('')
 *
 * }
 *
 * @NgModule({
 *   provides:[
 *     ServiceMakingUseOfRuntimeTextResources
 *   ],
 *   imports:[
 *      requestPublicRuntimeTextResource({
 *        name:                  logo,
 *        description:           "The logo stored as svg file, to be displayed in the header",
 *        languageIndependent?:  true,	// an svg log without text is language independent, because it is usable in all language contexts.
 *        required?:             true
 *      })
 *   ]
 * })
 * export class ModuleMakingUseOfRuntimeConfig{}
 * ```
 *
 */
@Injectable()
export class RccPublicRuntimeTextResourceService {

	public	ready			: Promise<void>

	public constructor(
		@Optional() @Inject(REQUESTED_RUNTIME_TEXT_RESOURCES)
		protected 	requestedTextResources	: RequestedTextResource[],
		private 	httpClient				: HttpClient,
		private		rccTranslationService	: RccTranslationService
	){
		this.ready = this.initialize()
	}

	protected async initialize() : Promise<void>  {

		this.assertAllResourceNamesAreUnique()
		this.logRequestedResources()

		return await this.prefetchAll()

	}

	protected logRequestedResources() : void {

		console.groupCollapsed(`Requested runtime resources (${pathToPublicRuntimeTextResources})`)

		this.requestedTextResources.forEach( resource => {

			const mode					: 	string
										= 	resource.required
											?	'required'
											:	'optional'

			const availableLanguages	:	string[]
										=	this.rccTranslationService.availableLanguages

			console.info(`[${mode}] ${resource.name}; Description: ${resource.description}`)

			if(!resource.languageIndependent)
				availableLanguages.forEach( lang => {

					const name	: string
								= this.getTranslationName(resource, lang)

					console.info(`  --> ${name}`)

				})
		})
		console.groupEnd()
	}

	/**
	 * @returns the language specific name for a
	 * language dependent resource or the normal name
	 * for a language independent resource.
	 */
	protected getTranslationName(requestedTextResource: RequestedTextResource, language: string) : string {
		return requestedTextResource.name.replace(/%lang/g, language)
	}

	/**
	 * Ensures that the set of requested Resources has no conflicting names.
	 */
	protected assertAllResourceNamesAreUnique() : void {

		const takenNames 			: 	Map<string, Set<RequestedTextResource>>
									=	new Map<string, Set<RequestedTextResource>>()

		const availableLanguages	:	string[]
									=	this.rccTranslationService.availableLanguages


		this.requestedTextResources.forEach( requestedTextResource => {

			const names		:	string[]
							=	requestedTextResource.languageIndependent
								?	[ requestedTextResource.name ]
								:	availableLanguages
									.map( lang => this.getTranslationName(requestedTextResource, lang))

			names.forEach( name => 	{

				const setOfSources	: 	Set<RequestedTextResource>
									= 	takenNames.has(name)
										?	takenNames.get(name)
										:	new Set<RequestedTextResource>()

				setOfSources.add(requestedTextResource)

				takenNames.set(name, setOfSources)

			})
		})

		takenNames.forEach( (requestedResources, name) => {
			if(requestedResources.size > 1){
				const culprits 	: 	string
								= 	[...requestedResources]
									.map( r => r.name)
									.join(', ')

				throw new Error(`RccPublicRuntimeTextResourceService.assertConsistency() multiple resources registered for the same name: ${name}. Culprits: ${culprits}`)
			}
		})
	}

	/**
	 * Used to load a single resource. Can be used to use
	 * to load a single language version of a language dependent
	 * resource. Use {@link .getTranslationName()} to get the
	 * respective name.
	 *
	 * Overwrite this to change
	 * the source of the resources.
	 */
	public async load(name: string) : Promise<string> {

		const request	:	Observable<string>
						=	this.httpClient.get(`${pathToPublicRuntimeTextResources}/${name}`, { responseType: 'text' })


		const result 	:	string
						=	await firstValueFrom(request)

		return result
	}


	/**
	 * Fetches the content of a single language independent resource
	 * – that is a resource whose content is the same for all languages.
	 */
	private async loadLanguageIndependentTextResource(requestedTextResource: RequestedTextResource): Promise<string> {

		assert(requestedTextResource.languageIndependent, 'resource is not language independent')

		return await this.load(requestedTextResource.name)
	}


	/**
	 * Fetches the content of a language dependent resource for each available language.
	 *
	 * @returns a dictionary that maps the language code to the content for that langauge.
	 */
	private async loadLanguageDependentTextResource(requestedTextResource: RequestedTextResource): Promise<Record<string,string> > {

		assert(!requestedTextResource.languageIndependent, 'resource is language independent')

		const availableLanguages	: 	string[]
									=	this.rccTranslationService.availableLanguages

		const names					:	Record<string, string>
									=	{}

		availableLanguages.forEach( lang =>  names[lang] = this.getTranslationName(requestedTextResource, lang) )

		const errors				:	Record<string, Error>  = {}
		const results				:	Record<string, string> = {}

		const promises				:	Promise<unknown>[]
									=	availableLanguages
										.map( lang =>

											this.load(names[lang])
											.then(
												content => results[lang] 	= 	content,
												error	=> errors[lang]		=	error instanceof Error
																					?	error
																					:	new Error(String(error))
											)
										)

		await Promise.all(promises)

		const allFailed				:	boolean
									=	Object.values(errors).length	=== availableLanguages.length

		const allSuccessful			:	boolean
									=	Object.values(results).length 	=== availableLanguages.length


		if(allSuccessful) return results

		console.group(`Something went wrong when loading results for ${requestedTextResource.name}`)

		Object.keys(results).forEach( (lang) => {

			const failed 	: boolean
							= !!errors[lang]

			const name		: string
							= names[lang]

			console.info(lang, failed ? '[failed] ' : '[success]', name )
			if(failed) console.error(errors[lang])
		})

		console.groupEnd()


		if(allFailed)	throw new Error('RccPublicRuntimeTextResourceService.loadLanguageDependentTextResource() unable to load required resources: see above.')

		// return partial results:
		return results
	}

	/**
	 * Pre-fetches language independent resources.
	 *
	 * @throws Error if resource cannot be loaded, but is required.
	 */
	private async prefetchLanguageIndependentTextResource(requestedTextResource: RequestedTextResource) : Promise<void>{

		assert(requestedTextResource.languageIndependent, 'resource is not language independent')

		try {
			await this.loadLanguageIndependentTextResource(requestedTextResource)
		} catch(e){
			if(requestedTextResource.required)
				throw new Error(`RccPublicRuntimeTextResourceService.prefetchLanguageIndependentTextResource() unable to prefetch ${requestedTextResource.name}`, { cause: e })
		}
	}

	/**
	 * Pre-fetches language dependent resources.
	 *
	 * @throws Error	if the app has no language restrictions
	 *
	 * @throws Error	if all language version fail to load, but resource is required
	 *
	 * @throws Error	if some language versions fail and some succeed to load, even if not required.
	 */
	private async prefetchLanguageDependentTextResource(requestedTextResource: RequestedTextResource) : Promise<void> {

		assert(!requestedTextResource.languageIndependent, 'resource is language independent')

		const availableLanguages		:	string[]
										=	this.rccTranslationService.availableLanguages

		const hasLanguageRestrictions	: boolean
										= availableLanguages !== null

		// If there are no language restrictions (i.e. the app is language independent),
		// we cannot check if all resources are available for every available languages;
		// resp. we know they are not.
		assert(hasLanguageRestrictions, `RccPublicRuntimeTextResourceService.prefetchLanguageDependentTextResource() The requested text resource introduces language restrictions (${requestedTextResource.name}, despite the app itself not having any. Restrict the app to these languages before using this resource.`)

		let result 						:	Record<string, string> | null
										=	undefined

		try {
			result = await this.loadLanguageDependentTextResource(requestedTextResource)
		} catch(e) {

			// If the resource is not required it's okay if loading fails.
			if(!requestedTextResource.required) return

			throw new Error(`RccPublicRuntimeTextResourceService.prefetchLanguageDependentTextResource() unable to load required text resource ${requestedTextResource.name}`, { cause: e } )
		}


		const partial				:	boolean
									=	Object.keys(result).length < availableLanguages.length

		// If only some of the language versions of
		// a text resource can be loaded,
		// it's likely something was misconfigured:
		if(partial) throw Error(`RccPublicRuntimeTextResourceService.prefetchLanguageDependentTextResource() only partial loading success will lead to inconsistent behavior. ${requestedTextResource.name}`)
	}

	/**
	 * For Errors see {@link .prefetchLanguageIndependentTextResource} and {@link .prefetchLanguageDependentTextResource}
	 */
	private async prefetchAll() : Promise<void> {


		const loadingAttempts	:	Promise<unknown>[]
								=	this.requestedTextResources.map(
										requestedTextResource =>	requestedTextResource.languageIndependent
																	?	this.prefetchLanguageIndependentTextResource(requestedTextResource)
																	:	this.prefetchLanguageDependentTextResource(requestedTextResource)
									)

		await Promise.all(loadingAttempts)

	}


	/**
	 * Retrieves a language independent resource.
	 */
	public async get(name: string): Promise<string> {

		await this.ready

		const requestedTextResource	: RequestedTextResource
									= this.requestedTextResources.find( rtr => rtr.name === name)

		assert(requestedTextResource, 						`RccPublicRuntimeTextResourceService.get() resource was not requested beforehand; "${name}". Try requestPublicRuntimeTextResource().`)
		assert(requestedTextResource.languageIndependent,	`RccPublicRuntimeTextResourceService.get() resource was not language independent; "${name}". Try .getTranslations instead.`)

		return this.loadLanguageIndependentTextResource(requestedTextResource)
	}

	/**
	 * Retrieves a language dependent resource.
	 */
	public async getTranslations(name:string): Promise<Record<string, string>> {

		await this.ready

		const requestedTextResource	: RequestedTextResource
									= this.requestedTextResources.find( rtr => rtr.name === name)

		assert( requestedTextResource, 						`RccPublicRuntimeTextResourceService.getTranslations() resource was not requested beforehand; "${name}". Try requestPublicRuntimeTextResource().`)
		assert(!requestedTextResource.languageIndependent,	`RccPublicRuntimeTextResourceService.getTranslations() resource was language independent; "${name}". Try .get() instead.`)

		return this.loadLanguageDependentTextResource(requestedTextResource)
	}



}
