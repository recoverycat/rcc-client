// Let child modules handle normal routes, only most basic config in here
import	{
			NgModule,
			InjectionToken,
			Provider,
			Optional,
			Inject
		} 									from '@angular/core'

import	{
			RouterModule,
			Router,
			Route
		} 												from '@angular/router'




export const OVERRIDE_ROUTES = new InjectionToken<Route[]>('overrideRoutes')

export function overrideRoute(route: Route): Provider {

	return 	{
				provide: 	OVERRIDE_ROUTES,
				multi:		true,
				useValue:	route
			}
}


const fallbackRoute = 	{ path: '**', redirectTo: ''  }

@NgModule({
	imports: [
		RouterModule.forRoot([])
	],
})
export class RccRoutingModule {

	public constructor(
		@Optional() @Inject(OVERRIDE_ROUTES)
		overrideRoutes	: Route[],
		router			: Router
	){

		overrideRoutes = overrideRoutes || []

		const old_routes 		= 	router.config
		const updated_routes 	= 	[
										...overrideRoutes.reverse(),
										...old_routes,
										fallbackRoute
									]

		router.resetConfig(updated_routes)
	}

}
