import { Component, Input } from '@angular/core'
import { SharedModule } from '../shared-module'
import { RccModalController } from '../modals-provider'

@Component({
	selector	: 'rcc-modal-header',
	templateUrl	: './modal-header.component.html',
	styleUrls	: ['./modal-header.component.scss'],
	imports		: [SharedModule]
})
export class RccModalHeaderComponent {
	@Input()
	public mainHeading: string

	@Input()
	public subHeading: string

	public constructor(
		private readonly rccModalController: RccModalController,
	) {}

	protected closeModal(): void {
		this.rccModalController.dismiss()
	}
}
