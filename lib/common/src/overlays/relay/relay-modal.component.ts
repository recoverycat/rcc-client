import {
			Component,
			OnDestroy,
			InjectionToken,
			Injector,
			OnInit
		}								from '@angular/core'

import	{
			RccModalController,
		}								from '../../modals-provider'


import	{
			takeUntil,
			Subject,
			Observable
		}								from 'rxjs'

import	{
			RelayModalConfig,
		}								from './relay-modal.commons'

export const RELAY_PROPERTIES: InjectionToken<unknown> = new InjectionToken<unknown>('Relay Component properties')

/**
 * Modal used to render components handed to RccRelayModalService
 */
@Component({
	template: 	`
					<ng-container
						*ngIf						= "component"
						[ngComponentOutlet]			= "component"
						[ngComponentOutletInjector]	= "relayInjector"
					>
					</ng-container>
				`,
	styles:		[`
					:host, :host > * {
						display:	block;
						height:		100%;
					}
				`],
	standalone: false,
})
export class RelayModalComponent implements OnDestroy, OnInit {


	public 		config$			: Observable<RelayModalConfig | null>
	public 		component		: RelayModalConfig['component']

	private		destroy$		: Subject<void>	= new Subject<void>()

	protected 	relayInjector	: Injector


	public constructor(
		private 	rccModalController	: RccModalController,
		protected 	parentInjector		: Injector,
	){}

	public ngOnInit(): void {

		// this.config$ will emit whenever there is a new component handed over:
		this.config$
		.pipe( takeUntil(this.destroy$) )
		.subscribe( (relayModalConfig : RelayModalConfig | null) => {

			// Close the whole modal when we encounter a null config.
			if(relayModalConfig === null) this.rccModalController.dismiss()

			this.component 		= 	relayModalConfig?.component

			const properties	:	RelayModalConfig['properties']
								= 	relayModalConfig?.properties

			// Update the injector with the new properties,
			// so that new new component can make use of them
			this.relayInjector 	= 	Injector.create({
										providers: [
											{
												provide: 	RELAY_PROPERTIES,
												useValue: 	properties
											}
										],
										parent: this.parentInjector,
									})

		})
	}

	public ngOnDestroy() : void {

		this.destroy$.next()
		this.destroy$.complete()

	}

}

