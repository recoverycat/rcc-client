import { Type } from '@angular/core'

export interface RelayModalConfig {

	component: 		Type<unknown>
	reject:			(reason?: unknown)	=> void
	resolve:		(value?: unknown) 	=> void
	properties?:	unknown

}
