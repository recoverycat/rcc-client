import 	{
			Injectable,
			Type
		}									from '@angular/core'

import	{
			BehaviorSubject,
			Observable,
		}									from 'rxjs'

import	{
	ModalPresentationOptions,
			RccModalController
		}									from '../../modals-provider'

import	{
			RelayModalComponent
		}									from './relay-modal.component'
import 	{
			RelayModalConfig,
		}									from './relay-modal.commons'


/**
 * This service keeps track of components supposed to be displayed in a RelayModalComponent.
 */
@Injectable()
export class RccRelayModalService {

	private configs 	: BehaviorSubject<RelayModalConfig | null> 	= new BehaviorSubject<RelayModalConfig | null>(null)
	public 	config$		: Observable<RelayModalConfig | null>		= this.configs.asObservable()

	private modalOpen	: boolean = false

	public constructor(
		private rccModalController: RccModalController
	){}


	/**
	 * Swap the component, that is meant to be displayed in the
	 * ReplayModalComponent or open a new Modal with current component.
	 */
	public handOver(component : Type<unknown>, properties?: unknown, options?: Partial<ModalPresentationOptions>): Promise<unknown> {

		return new Promise( (resolve, reject) => {

			// Resolve the previous relay promise:
			// Relay promises previously rejected at this place,
			// But it is normal an expected for a relay modal to be closed
			// by the next relay modal, so this should resolve, as any other
			// modal resolves when closed by e.g. clicking on the backdrop.
			this.configs.value?.resolve('hand-over')

			// The ReplayModalComponent subscribes to .config$
			this.configs.next({ component, properties, resolve, reject })

			// If the ReplayModalComponent is already on screen,
			// there is nothing more to do
			if(this.modalOpen) return undefined

			this.modalOpen = true

			void 	this.rccModalController
					.present(RelayModalComponent, { config$: this.config$ }, options)
					.then(
						(result:unknown) => {
							this.configs.value?.resolve(result)
						},
						(reason:unknown) =>	{
							this.configs.value?.reject(reason)
						}
					)
					.finally(() => this.cleanUp())
		})
	}

	private cleanUp() : void {
		this.configs.next(null)
		this.modalOpen = false
	}
}
