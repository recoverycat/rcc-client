import	{	NgModule						}	from '@angular/core'
import	{	CommonModule					}	from '@angular/common'
import	{	IonicModule						}	from '@ionic/angular'
import	{	CurrentDateMessageComponent		}	from '../current-date-message'
import	{	TranslationsModule				}	from '../translations'
import	{	RccOverlayService				}	from './overlay.service'
import	{
			RccRelayModalService,
			RelayModalComponent
		}										from './relay'
import	{
			CommonModalStyleLayoutComponent
		}										from './layout/common-modal-style-layout/common-modal-style-layout.component'
import	{	RccThemeModule					}	from '@rcc/themes/active'

@NgModule({
	imports:[
		IonicModule,
		TranslationsModule,
		CurrentDateMessageComponent,
		CommonModule,
		RccThemeModule
	],
	providers:[
		RccOverlayService,
		RccRelayModalService
	],
	declarations:[
		RelayModalComponent,
		CommonModalStyleLayoutComponent,
	],
	exports: [
		CommonModalStyleLayoutComponent
	]
})
export class OverlaysModule {}
