import { Component, Input } from '@angular/core'
import { RccModalController } from '@rcc/common/modals-provider'
import { RccTranslationService } from '@rcc/common/translations'
import { UserCanceledError } from '@rcc/core'

@Component({
	templateUrl		: './common-modal-style-layout.component.html',
	styleUrls		: ['./common-modal-style-layout.component.scss'],
	selector		: 'rcc-full-page-modal-style-layout',
	standalone		: false,
})
export class CommonModalStyleLayoutComponent {
	public constructor(
		private readonly modalController: RccModalController,
		private readonly rccTranslationService: RccTranslationService
	) {}

	@Input()
	public headingMessage: string

	@Input()
	public buttonText?: string

	@Input()
	public hideButton: boolean = false


	protected close(): void {
		if (this.buttonText === this.rccTranslationService.translate('CANCEL')) this.modalController.dismiss(new UserCanceledError)
		this.modalController.dismiss()
	}
}
