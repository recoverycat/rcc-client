import { RccModalController      				} from '../modals-provider'
import { ShareTransmissionDataModalComponent 	} from './modal/transmission/share-transmission-data-modal.component'
import { ShareDataService        				} from './share-data.service'

describe('ShareDataService', () => {

    it('should open an instance of ShareTransmissionDataModalComponent to display the data, if it is large enough', async() => {

        /**
         *
         * Create a spy on the method present() which is implemented instead of the actual RccModalController service.
         * Create a new instance of ShareDataService with the spy as the RccModalController service.
         * Create an object which is passed as data to the share() method.
         * Call the share() method and expect the spy to have been called with the appropriate arguments.
         *
         */

        const rccModalControllerSpy    	: 	jasmine.SpyObj<RccModalController>
										= 	jasmine.createSpyObj<RccModalController>('RccModalController', ['present'])

        const shareDataService 			: 	ShareDataService
										= 	new ShareDataService(rccModalControllerSpy as RccModalController)

        const largeData       			: 	unknown
										= 	{
												a : 'tzu touiputpi tu jz aksdjh akjdhaksjdh aksjdh akjdh aksjdh akjsdh akjsdh kas dh',
												b : 'lejukwerh urh4 ilztrg öi74tr w47rt geilzfg reilfzgzilgrilg lieurgf reugh ',
											}

        const resultPromise    			: 	Promise<unknown>
										= 	shareDataService.share(largeData, 'send')

        const result           			: 	unknown
										= 	await resultPromise

        const mostRecentArgs   			: 	Parameters<jasmine.Func>
										= 	rccModalControllerSpy.present.calls.mostRecent().args


        expect(resultPromise).toBeInstanceOf(Promise)
        expect(result).toBeUndefined()
        expect(mostRecentArgs[0]).toBe(ShareTransmissionDataModalComponent)
        expect(mostRecentArgs[1]).toEqual(jasmine.objectContaining({ data: largeData }))

    })
})
