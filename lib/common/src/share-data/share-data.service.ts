import 	{	Injectable							} from '@angular/core'
import	{	Observable							} from 'rxjs'
import	{	RccModalController					} from '../modals-provider'
import	{	ShareTransmissionDataModalComponent	} from './modal/transmission/share-transmission-data-modal.component'
import	{	SharePlainDataModalComponent		} from './modal/plain/share-plain-data-modal.component'
import { ShareMode } from './share-data.common'

@Injectable()
export class ShareDataService {

	public constructor(
		public rccModalController	: RccModalController
	){}

	/**
	 * Shares the provided data by opening a dedicated data sharing modal.
	 * This method is meant to be used for larger amounts of data, that might not
	 * fit into a QR code themselves. This method will open up a transmission
	 * channel and present a QR code containing transmission data. The provided data
	 * can then be sent through the transmission channel.
	 *
	 */
	protected async shareTransmission(
		/**
		 * Data to be shared
		 */
		data: unknown,
		mode: ShareMode,
		additionalInfoText?: string,
		/**
		 * Emitting on this observable will close the sharing modal
		 */
		stop$?: Observable<unknown>,

	) : Promise<void> {
		await this.rccModalController.present( ShareTransmissionDataModalComponent, { data, closeOn: stop$, mode, additionalInfoText }, { fitContent: true })
	}


	/**
	 * Shares the provided data encoded in a QR code. If the data is too
	 * large/complex, the method can fail to show the QR code, but will not
	 * reject. To be safe use .share instead.
	 */
	protected async sharePlain(
		/**
		 * Data to be shared
		 */
		data:unknown,
		mode: ShareMode,
		additionalInfoText?: string,
		/**
		 * Emitting on this observable will close the sharing modal
		 */
		stop$?: Observable<unknown>,

	) : Promise<void> {
		await this.rccModalController.present( SharePlainDataModalComponent, { data, closeOn: stop$, mode, additionalInfoText }, { fitContent: true })
	}


	/**
	 * If the data is simple enough, calls
	 * {@link ShareDataService.sharePlain} otherwise uses
	 * {@link ShareDataService.shareTransmission}
	 */
	public async share(data:unknown, mode: ShareMode, additionalInfoText?: string, stop$?: Observable<unknown>): Promise<void> {

		const stringData 			: string
									= JSON.stringify(data)

		// Anything that is not needed for JSON or is not
		// base64 is regarded as extra:
		const containsExtraSymbols 	: boolean
									= stringData.match(/[^\][[a-zA-Z0-9+-_.,{}"']/gi) !== null

		const isLarge				: boolean
									= stringData.length > 144

		if(containsExtraSymbols || isLarge) return await this.shareTransmission(data, mode, additionalInfoText, stop$)

		return await this.sharePlain(data, mode, additionalInfoText, stop$)
	}
}
