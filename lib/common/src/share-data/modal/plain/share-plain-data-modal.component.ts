import 	{
			Component,
			Input,
		}										from '@angular/core'

import	{
			SubscriptionLike,
			Observable,
			take
		}										from 'rxjs'

import	{
			UserCanceledError
		}										from '@rcc/core'

import	{
			RccModalController
		}										from '../../../modals-provider'
import	{	ShareMode						}	from '../../share-data.common'

@Component({
	selector:		'rcc-plain-data-share-modal',
	templateUrl:	'./share-plain-data-modal.component.html',
	styleUrls:		['./share-plain-data-modal.component.scss'],
	standalone:		false,
})
export class SharePlainDataModalComponent {

	@Input()
	public data					: unknown

	@Input()
	public mode					: ShareMode

	@Input()
	public additionalInfoText	: string

	/**
	 * Remotely close this modal by emitting something on this observable:
	 */
	@Input()
	public  set closeOn(stop$ : Observable<unknown>) {

		if(this.stopSubscription) this.stopSubscription.unsubscribe()

		if(stop$)
			this.stopSubscription 	= 	stop$
				.pipe(take(1))
				.subscribe( () => this.rccModalController.dismiss() )
	}


	public stopSubscription : SubscriptionLike

	public constructor(
		private rccModalController: RccModalController,
	){}

	public get qr_data() : string {
		if (this.data == null)
			return
		return JSON.stringify(this.data)
	}

	public cancel() : void {
		this.rccModalController.dismiss(new UserCanceledError())
	}

	protected get heading(): string {
		return `SHARE_DATA.HEADING.${this.mode === 'send' ? 'SEND' : 'RECEIVE'}`
	}
}
