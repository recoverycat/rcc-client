import	{	Injectable					}	from '@angular/core'

import	{	RccAlertController			}	from '../modals-provider'
import	{	RccSwService				}	from './service-worker.service'
import	{	RccSettingsService			}	from '../settings'

@Injectable()
export class RccPushNotificationService {


	public ready 				: Promise<void>

	public constructor(
		private rccSwService				: 	RccSwService,
		private rccAlertController			: 	RccAlertController,
		private rccSettingsService			:	RccSettingsService,
	){
		this.ready = this.setup()
	}


	protected async setup() : Promise<void> {

		await this.rccSwService.ready

	}

	private requestInProgress: boolean = false
	private requestFinished: Promise<void>

	public async askPermission(): Promise<void> {

		const notificationsAllowedViaAppSettings: boolean
			= (await this.rccSettingsService.get('notifications-enabled')).value

		if (!notificationsAllowedViaAppSettings)
			throw new Error('User declined via app settings')

		if (this.requestInProgress) return await this.requestFinished.catch()

		this.requestInProgress = true

		this.requestFinished = this.showAlertModal()
		await this.requestFinished.finally(() => {
			this.requestInProgress = false
		})
	}

	private async showAlertModal(): Promise<void> {
		await 	this.ready

		try {
			await	this.rccAlertController.present({
					header:		'SERVICE_WORKER.NOTIFICATIONS.PERMISSION.HEADER',
					message: 	'SERVICE_WORKER.NOTIFICATIONS.PERMISSION.MESSAGE',
					cssClass:	'rcc-theme',
					buttons:	[
									{ label: 'SERVICE_WORKER.NOTIFICATIONS.PERMISSION.NO', 	rejectAs: 	'user_no'	},
									{ label: 'SERVICE_WORKER.NOTIFICATIONS.PERMISSION.YES', resolveAs: 	'user_yes'	},
								]
				})

		} catch (err) {
			await this.rccSettingsService.set('notifications-enabled', false)
			throw err
		}
	}

	protected getPushSubscriptionOptionsInit(serverPublicKey: string): PushSubscriptionOptionsInit {
		return { userVisibleOnly: true, applicationServerKey: serverPublicKey }
	}

	public async getSubscription(serverPublicKey: string): Promise<PushSubscription>{

		await this.ready

		const pushManager	: PushManager		= this.rccSwService.serviceWorkerRegistration.pushManager

		let subscription	: PushSubscription	= await pushManager.getSubscription()

		if(subscription) return subscription

		const pushSubscriptionOptionsInit: PushSubscriptionOptionsInit = this.getPushSubscriptionOptionsInit(serverPublicKey)

		const permission	: PermissionState	= await pushManager.permissionState(pushSubscriptionOptionsInit)

		if(permission === 'prompt')	await this.askPermission()
		if(permission === 'denied')	throw new Error('permission denied')

		subscription 		= await pushManager.subscribe(pushSubscriptionOptionsInit)

		return subscription
	}
}
