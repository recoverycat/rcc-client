import { fakeAsync, TestBed } from '@angular/core/testing'
import { ModalProviderModule, RccPushNotificationService, RccSwService, SettingsModule } from '@rcc/common'
import { IonicAlertController, IonicModalsModule } from '@rcc/ionic'
import { IndexedDbModule } from '@rcc/features'

class RccPushNotificationServiceWrapper extends RccPushNotificationService {
	public getPushSubscriptionOptionsInit(serverPublicKey: string): PushSubscriptionOptionsInit {
		return super.getPushSubscriptionOptionsInit(serverPublicKey)
	}
}

describe('RccPushNotificationService', () => {

	beforeEach(() => {
		// Due to an error/warning saying that "Some of your tests did a full page reload!", the following fix was found
		// https://stackoverflow.com/questions/29352578/some-of-your-tests-did-a-full-page-reload-error-when-running-jasmine-tests
		window.onbeforeunload = jasmine.createSpy()
	})

	describe('Subscriptions', () => {

		let service: RccPushNotificationServiceWrapper = undefined!
		let mockRccSwService: jasmine.SpyObj<RccSwService> = undefined!
		let mockServiceWorkerRegistration: jasmine.SpyObj<ServiceWorkerRegistration> = undefined!
		let mockPushManager: jasmine.SpyObj<PushManager> = undefined!
		let mockPushSubscription: jasmine.SpyObj<PushSubscription> = undefined!

		beforeEach(() => {

			mockPushManager = jasmine.createSpyObj('PushManager', ['getSubscription', 'permissionState', 'subscribe'], {}) as jasmine.SpyObj<PushManager>

			mockServiceWorkerRegistration = jasmine.createSpyObj('ServiceWorkerRegistration', [], {
				pushManager: mockPushManager
			}) as jasmine.SpyObj<ServiceWorkerRegistration>

			mockRccSwService = jasmine.createSpyObj('RccSwService', [''], {
				ready: Promise.resolve(),
				serviceWorkerRegistration: mockServiceWorkerRegistration
			}) as jasmine.SpyObj<RccSwService>

			TestBed.configureTestingModule({
				imports: [
					IndexedDbModule,
					SettingsModule,
					ModalProviderModule.forRoot({
						alertController: IonicAlertController,
					}),
					IonicModalsModule,
				],
				providers: [
					{ provide: RccSwService, useValue: mockRccSwService },
					{ provide: ServiceWorkerRegistration, useValue: mockServiceWorkerRegistration },
					{ provide: PushManager, useValue: mockPushManager },
					RccPushNotificationServiceWrapper
				],
				teardown: { destroyAfterEach: false }
			})
			service = TestBed.inject(RccPushNotificationServiceWrapper)

		})

		it('should be created', () => {
			expect(service).toBeTruthy()
		})

		it('should return if subscription exists', fakeAsync(async () => {

			mockPushSubscription = jasmine.createSpyObj('PushSubscription', [''], {
				endpoint: 'this is an endpoint'
			}) as jasmine.SpyObj<PushSubscription>

			mockPushManager.getSubscription.and.resolveTo(mockPushSubscription)
			const pushSubscription: PushSubscription = await service.getSubscription('this is a key')

			expect(pushSubscription.endpoint).toEqual(mockPushSubscription.endpoint)
		}))

		it('should create new subscription if no subscription exists', fakeAsync(async () => {

			mockPushSubscription = jasmine.createSpyObj('PushSubscription', [''], {
				endpoint: 'this is the new endpoint'
			}) as jasmine.SpyObj<PushSubscription>

			mockPushManager.getSubscription.and.resolveTo(null)
			mockPushManager.subscribe.and.resolveTo(mockPushSubscription)
			const pushSubscription: PushSubscription = await service.getSubscription('this is a key')

			expect(pushSubscription.endpoint).toEqual(mockPushSubscription.endpoint)

		}))

		it('should create PushSubscriptionOptionsInit with expected values', () => {
			const pushSubscriptionOptionsInit: PushSubscriptionOptionsInit = service.getPushSubscriptionOptionsInit('this is a key')

			expect(pushSubscriptionOptionsInit.userVisibleOnly).toBeTrue()
			expect(pushSubscriptionOptionsInit.applicationServerKey).toEqual('this is a key')

		})

	})

})
