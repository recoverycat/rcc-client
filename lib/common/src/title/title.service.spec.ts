
import  {
            discardPeriodicTasks,
            fakeAsync,
            TestBed,
            tick
        }                           	from '@angular/core/testing'
import { Title }                        from '@angular/platform-browser'
import { RccTitleService }              from './title.service'

describe('RccTitleService', () => {
    let service : RccTitleService = undefined
    let pageTitle : Title = undefined

    beforeEach(() => {
        pageTitle = new Title(document)
        
        TestBed.configureTestingModule({ providers: [RccTitleService] })
        service = TestBed.inject(RccTitleService)
    })

    it('should be possible to change the page title', fakeAsync(() => {
        service.setTitle('New Title')

        tick()

        expect(pageTitle.getTitle()).toBe('New Title')

        discardPeriodicTasks()
    }))

    it('should set the suffix as the page title if no page title is supplied', fakeAsync(() => {
        service.setTitleSuffix('Suffix Only')

        tick()

        expect(pageTitle.getTitle()).toBe('Suffix Only')

        discardPeriodicTasks()
    }))

    it('should be possible to change the page title and suffix', fakeAsync(() => {
        service.setTitle('New Title')
        service.setTitleSuffix('New Suffix')

        tick()

        expect(pageTitle.getTitle()).toBe('New Title · New Suffix')

        discardPeriodicTasks()
    }))
})
