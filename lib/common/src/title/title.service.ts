import	{	Injectable				}	from '@angular/core'
import	{	Title					}	from '@angular/platform-browser'
import	{
			BehaviorSubject,
			combineLatest,
			of,
			throttle,
			timer
		}								from'rxjs'
import	{	RccTranslationService	}	from '../translations'

@Injectable({
	providedIn: 'root'
})

/**
 * Angular service managing the application's page titles. Prefix is now changeable
 * centrally. For example Recovery Cat: Home
 */

export class RccTitleService {
	private titleString$	: 	BehaviorSubject<string>
							=	new BehaviorSubject<string>(null)
	private suffixString$	: 	BehaviorSubject<string>
							=	new BehaviorSubject<string>(null)

	public constructor(
		private pageTitle: Title,
		private rccTranslationService: RccTranslationService
	){
		combineLatest([
			this.titleString$.pipe(
				// For nested child routes, we might end up calling
				// the `setTitle` function multiple times, with
				// potentially nullish values. We want to make sure
				// that if this happens, if `setTitle` is called
				// with a specific, non-nullish value, that this
				// takes precedent over any calls to `setTitle` with
				// null or undefined
				throttle((value) => value != null ? timer(200) : of({}))
			),
			this.suffixString$,
			this.rccTranslationService.activeLanguageChange$
		])
		.subscribe(([title, suffix]) => this.updateTitle(title, suffix))
	}

	public setTitle(titleString : string): void {
		this.titleString$.next(titleString)
	}

	public setTitleSuffix(suffixString : string): void {
		this.suffixString$.next(suffixString)
	}

	private updateTitle(titleString: string, suffixString: string): void {
		const title			: string	=	titleString
											?	this.rccTranslationService.translate(titleString)
											:	''
		const suffix		: string	=	suffixString
											?	this.rccTranslationService.translate(suffixString)
											:	''

		this.pageTitle.setTitle(`${title}${title && suffix ? ' · ' : ''}${suffix}`)
	}
}
