import	{
			RouterStateSnapshot,
			TitleStrategy
		}								from '@angular/router'
import	{	Injectable				}	from '@angular/core'
import	{	RccTranslationService	}	from '../translations'
import	{	RccTitleService			}	from './title.service'

@Injectable({ providedIn: 'root' })
export class RccTitleStrategy extends TitleStrategy {
	public constructor(
		private rccTitleService			: RccTitleService,
		private rccTranslationService	: RccTranslationService
	){
		super()
	}
	public override updateTitle(snapshot: RouterStateSnapshot): void {
		this.rccTitleService.setTitleSuffix(this.buildTitle(snapshot))
	}
}
