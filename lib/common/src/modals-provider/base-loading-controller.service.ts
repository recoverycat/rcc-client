import 	{ 	Injectable			}	from '@angular/core'



export interface LoadingConfig {
	message		: string,
	useCancel?	: boolean
}

export interface LoadingInstance {
	close	: () => Promise<void>
}

/**
 * This class is meant to be extented, but will still be used as injection token.
 */
@Injectable()
export class RccLoadingController {

	/**
	 * Displays some loading/waiting indicator and blocks user interaction.
	 */
	public async present(loadingConfig?: LoadingConfig): Promise<LoadingInstance> {
		const msg = 'ModalProviderModule: missing LodingProvider, please provide alternative LoadingControllerClass extending RccLoadingController.'
		console.warn(msg)
		throw msg
	}


}



