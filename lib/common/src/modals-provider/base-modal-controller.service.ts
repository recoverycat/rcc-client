import	{
			Type,
			Injectable,
		}					from '@angular/core'
import	{
			PropertiesOf
		}					from '@rcc/core/types'

export interface ModalPresentationOptions {
	fullScreen: boolean
	mini: boolean
	fitContent: boolean
	canDismiss: boolean
}

@Injectable()
export class RccModalController {

	public async present<T, R = unknown>(component: Type<T>, data?: PropertiesOf<T>, options?: Partial<ModalPresentationOptions>): Promise<R>
	// Should resolve with whatever data is passed into dismiss, unless an Error object is passed; in that case reject:
	public present(): Promise<unknown> {
		return Promise.resolve(this.dismiss())
	}

	public dismiss(data?: unknown): void
	public dismiss(): void{
		console.warn('ModalProviderModule: missing ModalProvider, please provide alternative modalControllerClass extending RccModalController.' )
	}
}

