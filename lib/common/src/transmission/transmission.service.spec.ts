import { Observable, Subject, firstValueFrom } from 'rxjs'
import { RccBuildInfoService } from '../build-info'
import { IncomingDataService } from '../incoming-data'
import { RccAlertController } from '../modals-provider'
import { RccOverlayService } from '../overlays'
import { AbstractTransmissionService, RccTransmission } from './transmission.commons'
import { RccTransmissionService } from './transmission.service'

describe('RccTransmissionService', () => {
    let service                 : RccTransmissionService                        = undefined
    let mockTransmission        : jasmine.SpyObj<RccTransmission>               = undefined
    let mockTransmissionService : jasmine.SpyObj<AbstractTransmissionService>   = undefined
				
    const testData : string = 'test'
    const testMeta : (string|number)[] = ['rcc-cmb', 'pLofrmQFoL5RML47mVtt', 'be7F5On/ncvSMPfagthqagtXWje99XsnqFRIxGJZ4XM=']

    /**
     * Tests if an observable emits an expected value after a certain trigger.
     */
    function shouldEmitAfter(obs: Observable<unknown>, expectedMeta: unknown, done: DoneFn, triggerCallback: () => Promise<unknown>): void {
        obs.subscribe((value) => {
            // value should be of type EmissionOnTransmission
            expect(value).toEqual(jasmine.objectContaining({ service: mockTransmissionService }))
            expect(value).toEqual(jasmine.objectContaining({ meta: expectedMeta }))
            done()
        })

        void triggerCallback()
    }

    /**
     * Tests if an observable does NOT emit between a trigger and a promise resolution.´
     *
     * @param obs The observable to test.
     * @param triggerCallback The function that triggers the emission of the observable.
     * @param promiseAfterEmission A promise that resolves only after or instead of the potential emission of the observable.
     */
    async function shouldNotEmitUntil(obs: Observable<unknown>, triggerCallback: () => Promise<unknown>, promiseAfterEmission: Promise<unknown>): Promise<void> {
        let nextCounter : number = 0
        
        obs.subscribe(() => {
            nextCounter++
        })
    
        await triggerCallback()
        // Wait until the emission would have happened
        await promiseAfterEmission
    
        expect(nextCounter).toBe(0)
    }

    beforeEach(() => {

        const incomingDataService	: IncomingDataService
                                    = new Subject<unknown>() as IncomingDataService

        const rccOverlayService	    : RccOverlayService
                                    = { } as RccOverlayService

        const rccAlertController	: RccAlertController
                                    = { } as RccAlertController

        const buildInfoService	    : RccBuildInfoService
                                    = { } as RccBuildInfoService

        mockTransmission = jasmine.createSpyObj<RccTransmission>('RccTransmission', ['start', 'cancel'], ['meta'])
        mockTransmissionService = jasmine.createSpyObj<AbstractTransmissionService>('AbstractTransmissionService', ['listen', 'validateMeta', 'setup', 'setupWithExistingMeta'], ['id'])

        mockTransmissionService.id = 'transmission-id'
        mockTransmissionService.validateMeta.and.returnValue(true)
        mockTransmissionService.setup.and.resolveTo(mockTransmission)
        mockTransmissionService.setupWithExistingMeta.and.resolveTo(mockTransmission)

		service = new RccTransmissionService(
            [mockTransmissionService],
			incomingDataService,
			rccOverlayService,
			rccAlertController,
            buildInfoService
		)

	})

    describe('.listen()', () => {

        it('should emit successfulIncomingTransmission$ after a successful listen', (done: DoneFn) => {

            mockTransmissionService.listen.and.resolveTo('mock transmission data')
            shouldEmitAfter(
                service.successfulIncomingTransmission$,
                testMeta,
                done,
                () => service.listen(testMeta)
            )

        })

        it('should emit failedIncomingTransmission$ after a failed listen', (done: DoneFn) => {

            mockTransmissionService.listen.and.rejectWith('failed mock transmission')
            shouldEmitAfter(
                service.failedIncomingTransmission$,
                testMeta,
                done,
                () => service.listen(testMeta).catch(() => undefined)
            )

        })

        it('should not emit successfulIncomingTransmission$ before failedIncomingTransmission$ in case of a failed listen', async () => {

            mockTransmissionService.listen.and.rejectWith('failed mock transmission')
            await shouldNotEmitUntil(
                service.successfulIncomingTransmission$,
                () => service.listen(testMeta).catch(() => undefined),
                firstValueFrom(service.failedIncomingTransmission$)
            )

        })

        it('should not emit failedIncomingTransmission$ before successfulIncomingTransmission$ in case of a successful listen', async () => {

            mockTransmissionService.listen.and.resolveTo('mock transmission data')
            await shouldNotEmitUntil(
                service.failedIncomingTransmission$,
                () => service.listen(testMeta),
                firstValueFrom(service.successfulIncomingTransmission$)
            )

        })

    })

    /**
     * Wrapper for `.setup()` and `.setupWithExistingMeta()`.
     */
    function testSetupMethod(setupCallback: () => Promise<RccTransmission>): void {

        describe('setup', () => {

            function resolveSetup() : Promise<unknown> {
                mockTransmission.start.and.resolveTo(undefined)
                return  setupCallback()
                        .then((transmission: RccTransmission) => transmission.start())
            }

            function rejectSetup() : Promise<unknown> {
                mockTransmission.start.and.rejectWith(undefined)
                return  setupCallback()
                        .then((transmission: RccTransmission) => {
                            transmission.start()
                            .catch(() => undefined)
                        })
            }

            it('should emit successfulOutgoingTransmission$ after a successful transmission', (done: DoneFn) => {

                shouldEmitAfter(
                    service.successfulOutgoingTransmission$,
                    mockTransmission.meta,
                    done,
                    () => resolveSetup()
                )
    
            })
    
            it('should emit failedOutgoingTransmission$ after a failed transmission', (done: DoneFn) => {
    
                shouldEmitAfter(
                    service.failedOutgoingTransmission$,
                    mockTransmission.meta,
                    done,
                    () => rejectSetup()
                )
    
            })
    
            it('should not emit successfulOutgoingTransmission$ before failedOutgoingTransmission$ in case of a failed transmission', async () => {
    
                await shouldNotEmitUntil(
                    service.successfulOutgoingTransmission$,
                    () => rejectSetup(),
                    firstValueFrom(service.failedOutgoingTransmission$)
                )
    
            })
    
            it('should not emit failedOutgoingTransmission$ before successfulOutgoingTransmission$ in case of a successful transmission', async () => {
    
                await shouldNotEmitUntil(
                    service.failedOutgoingTransmission$,
                    () => resolveSetup(),
                    firstValueFrom(service.successfulOutgoingTransmission$)
                )
    
            })

        })
    }

    testSetupMethod(() => service.setup(testData))
    testSetupMethod(() => service.setupWithExistingMeta(testData, testMeta))
          
})
