import { UserCanceledError } from '@rcc/core'
import { Observable, Subject } from 'rxjs'
import { RccTransmission } from './transmission.commons'

export type MetaType = unknown

export class RccTransmissionSpy implements RccTransmission {
	private successfulOutgoingTransmissions	: Subject<MetaType> = new Subject()
	private failedOutgoingTransmissions		: Subject<MetaType> = new Subject()

	public successfulOutgoingTransmission$	: Observable<MetaType> = this.successfulOutgoingTransmissions.asObservable()
	public failedOutgoingTransmission$		: Observable<MetaType> = this.failedOutgoingTransmissions.asObservable()

	public constructor(private rccTransmission: RccTransmission) {}

	public get meta() : (string|number)[] {
		return this.rccTransmission.meta
	}

	public async start() : Promise<unknown> {
		const startPromise : Promise<unknown> = this.rccTransmission.start()

		try {
			await startPromise
			this.successfulOutgoingTransmissions.next(this.meta)
			return startPromise
		} catch (error) {
			if (!(error instanceof UserCanceledError))
				this.failedOutgoingTransmissions.next(this.meta)
			throw error
		} finally {
			this.successfulOutgoingTransmissions.complete()
			this.failedOutgoingTransmissions.complete()
		}
				
	}

	public async cancel() : Promise<unknown> {
		return await this.rccTransmission.cancel()
	}
}
