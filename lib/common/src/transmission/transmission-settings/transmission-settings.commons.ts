import	{	Factory							}	from '../../interfaces'

import	{
			SettingsEntry,
		}										from '../../settings'

import	{
			AbstractTransmissionService
		}										from '../transmission.commons'

import	{
			TransmissionSettingsInformationComponent
		}										from './transmission-settings-information/transmission-settings-information.component'

import	{
			RccTransmissionService,
		}										from '../transmission.service'
import	{
			dataTransmissionPreferenceSettingsEntryId
		}										from '@rcc/features/transmission-requests/session-requests/combined/combine-session-request.common'

declare module '@rcc/common/settings/settings.service' {
	interface SettingValueTypeMap {
		'transmission-send': string
	}
}

/**
 * Id used for the {@link SettingsEntry} {@link transmissionSendingServiceSettingsEntry}
 */
export const transmissionSendingServiceSettingsEntryId 	: string = 'transmission-send'


/**
 * {@link SettingsEntry} governing which transmission service will be used when sending data.
 */
export const transmissionSendingServiceSettingsEntry 	: Factory<SettingsEntry<string>> = {

	deps: [RccTransmissionService],
	factory: (rccTransmissionService: RccTransmissionService) => {

		const availableServices 	:	AbstractTransmissionService[]
									= 	rccTransmissionService.transmissionServices

		/**
		 * Looking for {@link CombinedTransmissionService}. If present use as
		 * default, if not pick the first transmission service available.
		 */
		const sendingService		:	AbstractTransmissionService
									= 	rccTransmissionService.getTransmissionService('COMBINED_TRANSMISSION_SERVICE')
										||
										availableServices[0]

		return 	{
					id:				transmissionSendingServiceSettingsEntryId,
					label:			'TRANSMISSION.SEND.LABEL',
					description:	'TRANSMISSION.SEND.DESCRIPTION',
					icon:			'transmission',
					type:			'select',
					defaultValue:	sendingService?.id,
					position:		rccTransmissionService.transmissionServices.length > 1 ? 1 : null,

					options:		availableServices
									.map( service => ({ value: service.id, label: service.label }) ),

					information:	TransmissionSettingsInformationComponent,
				}
	}

}


/**
 * Id used for the {@link SettingsEntry} {@link transmissionReceivingServiceSettingsEntry}
 */
export const transmissionReceivingServiceSettingsEntryId 	: string = 'transmission-receive'


/**
 * {@link SettingsEntry} governing which transmission service are allowed when receiving data.
 */
export const transmissionReceivingServiceSettingsEntry 		: Factory<SettingsEntry<string>> = {

	deps: [RccTransmissionService],
	factory: (rccTransmissionService: RccTransmissionService) => {

		const availableServices 	:	AbstractTransmissionService[]
									= 	rccTransmissionService.transmissionServices

		return {
					id:				transmissionReceivingServiceSettingsEntryId,
					label:			'TRANSMISSION.RECEIVE.LABEL',
					description:	'TRANSMISSION.RECEIVE.DESCRIPTION',
					icon:			'transmission',
					type:			'select',

					defaultValue:	availableServices[0].id,

					options:		availableServices
									.map( (service : AbstractTransmissionService) => ({ value: service.id, label: service.label }) ),
					information:	TransmissionSettingsInformationComponent,
				}
	}
}


/**
 * {@link SettingsEntry} grouping transmission related settings.
 */
export const transmissionSettingsGroup : SettingsEntry = {

	id:				'transmission',
	type:			'group',
	label:			'TRANSMISSION.SETTING.LABEL',
	description:	'TRANSMISSION.SETTING.DESCRIPTION',
	icon:			'transmission',
	position:		2,
	subSettingIds:	[
		transmissionSendingServiceSettingsEntryId,
		transmissionReceivingServiceSettingsEntryId,
		dataTransmissionPreferenceSettingsEntryId,
	]
}
