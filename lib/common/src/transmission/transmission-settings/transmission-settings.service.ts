import 	{	Injectable									}	from '@angular/core'


import	{
			RccSettingsService,
		}													from '../../settings'

import	{
			RccTransmissionService,
		}													from '../transmission.service'

/**
 * Synchronizes the transmission settings and {@link RccSettingsService}
 */
@Injectable()
export class RccTransmissionSettingsService{

	public constructor(
		protected rccSettingsService		: RccSettingsService,
		protected rccTransmissionService	: RccTransmissionService
	){


		// sync sending
		this.rccSettingsService.valueChange$('transmission-send')
		.subscribe( sendingServiceId 	=> this.rccTransmissionService.setAllowedSendingService(sendingServiceId))

		// sync receiving
		this.rccSettingsService.valueChange$('transmission-send')
		.subscribe( receivingServiceIds 	=> this.rccTransmissionService.setAllowedReceivingServices([receivingServiceIds]))

	}
}
