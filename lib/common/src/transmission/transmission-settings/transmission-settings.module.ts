import	{
			NgModule,
		}													from '@angular/core'

import	{
			CommonModule
		}													from '@angular/common'

import	{
			provideSettingsEntry
		}													from '../../settings'

import	{
			TranslationsModule,
			provideTranslationMap
		}													from '../../translations'

import	{
			TransmissionModule
		}													from '../transmission.module'

import	{
			transmissionSendingServiceSettingsEntry,
			transmissionReceivingServiceSettingsEntry,
			transmissionSettingsGroup,
		}													from './transmission-settings.commons'

import	{
			RccTransmissionSettingsService
		}													from './transmission-settings.service'

import	{
			TransmissionSettingsInformationComponent
		}													from './transmission-settings-information/transmission-settings-information.component'


import en from './i18n/en.json'
import de from './i18n/de.json'

/**
 * This module allows to restrict the app to specific transmission services
 * for sending and receiving data. If this module is not imported there's no
 * restriction on the available transmission services. Will also add settings
 * entries to configure the restrictions.
 */
@NgModule({
	imports:[
		CommonModule,
		TransmissionModule,
		TranslationsModule
	],
	providers: [
		RccTransmissionSettingsService,
		provideSettingsEntry(transmissionSendingServiceSettingsEntry),
		provideSettingsEntry(transmissionReceivingServiceSettingsEntry),
		provideSettingsEntry(transmissionSettingsGroup),
		provideTranslationMap('TRANSMISSION.SETTINGS', { en,de })
	],
	declarations: [
		TransmissionSettingsInformationComponent
	]

})
export class RccTransmissionSettingsModule {


	public constructor(
		/**
		 * Ensures that {@link RccTransmissionSettingsService} is initialized.
		 */
		private rccTransmissionSettingsService: RccTransmissionSettingsService
	){}

}
