import	{
			Injectable,
			Inject,
			Optional,
			OnDestroy,
		}								from '@angular/core'

import	{
			Observable,
			Subject,
			SubscriptionLike
		}								from 'rxjs'
import	{
			filter,
		}								from 'rxjs/operators'
import	{
			assert,
			UserCanceledError,
		}								from '@rcc/core'
import	{
			RccTransmission,
			AbstractTransmissionService,
			TRANSMISSION_SERVICE
		}								from './transmission.commons'

import	{
			RccOverlayService
		}								from '../overlays'

import	{	IncomingDataService		}	from '../incoming-data'

import	{
			RccAlertController,
		}								from '../modals-provider'
import	{	RccBuildInfoService		}	from '../build-info'
import	{	DefinedIconName			}	from '../ui-components/icons/icon-names'
import	{
			MetaType,
			RccTransmissionSpy
		}								from './transmission-spy.class'

export interface EmissionOnTransmission {
	service	: AbstractTransmissionService,
	meta    : MetaType
}


/**
 * This Service manages data transmission. It does not provide the actual means
 * of the transmission itself but relies on other modules to provide those means
 * by calling {@link TransmissionModule.forChild}.
 */
@Injectable()
export class RccTransmissionService extends AbstractTransmissionService implements OnDestroy {

	public override id				: string = 'RCC_TRANSMISSION_SERVICE'

	public override label			: string = 'TRANSMISSION.RCC_TRANSMISSION_SERVICE.LABEL'
	public override description		: string = 'TRANSMISSION.RCC_TRANSMISSION_SERVICE.DESCRIPTION'

	public allowedSendingService	: AbstractTransmissionService 	= null
	public allowedReceivingServices	: AbstractTransmissionService[] = []

	protected subscriptions : SubscriptionLike [] = []

	private successfulIncomingTransmissions	: Subject<EmissionOnTransmission> = new Subject()
	private failedIncomingTransmissions		: Subject<EmissionOnTransmission> = new Subject()
	private successfulOutgoingTransmissions	: Subject<EmissionOnTransmission> = new Subject()
	private failedOutgoingTransmissions		: Subject<EmissionOnTransmission> = new Subject()

	public successfulIncomingTransmission$	: Observable<EmissionOnTransmission> = this.successfulIncomingTransmissions.asObservable()
	public failedIncomingTransmission$		: Observable<EmissionOnTransmission> = this.failedIncomingTransmissions.asObservable()
	public successfulOutgoingTransmission$	: Observable<EmissionOnTransmission> = this.successfulOutgoingTransmissions.asObservable()
	public failedOutgoingTransmission$		: Observable<EmissionOnTransmission> = this.failedOutgoingTransmissions.asObservable()

	public constructor(
		@Optional() @Inject(TRANSMISSION_SERVICE)
		public 		transmissionServices	: AbstractTransmissionService[],
		protected	incomingDataService		: IncomingDataService,
		protected	rccOverlayService		: RccOverlayService,
		private 	rccAlertController		: RccAlertController,
		private		buildInfoService		: RccBuildInfoService,
	){
		super()

		assert(transmissionServices, 'RccTransmissionService.constructor: missing transmission services.')

		/**
		 * When sending data we have to pick one of the available transmission
		 * services without prompting the user to pick one, so we just pick the
		 * first available one here.
		 * Another service though should enable to user to set which service to
		 * use for sending; and indeed that is the job of
		 * {@link RccTransmissionSettingsService}
		 */

		this.setAllowedSendingService(this.transmissionServices[0].id)

		/**
		 * Initially every transmission service is allowed to be used when
		 * receiving data to maximize the chance the transmission actually
		 * works. There is little concern about being too open here, as
		 * receiving data over a potentially unwanted channel is rather harmless.
		 *
		 * However {@link RccTransmissionSettingsService} enables the user to
		 * restrict receiving services.
		 */
		this.setAllowedReceivingServices(this.transmissionServices.map( ts => ts.id))

		this.listenToIncomingDataService()



		assert(transmissionServices, 'RccTransmissionService: No transmission service registered; you probably forgot to load a module providing such a service.')
	}

	/**
	 * Returns the transmission service with the provided id.
	 */
	public getTransmissionService(id: string): AbstractTransmissionService {
		return this.transmissionServices.find( ts => ts.id === id)
	}

	/**
	 * Sets which service will be used for sending data.
	 */
	public setAllowedSendingService(id:string) : void {

		const sendingService 	: AbstractTransmissionService
								= this.getTransmissionService(id)

		this.allowedSendingService = sendingService
	}

	/**
	 * Sets which services will be used for receiving data.
	 */
	public setAllowedReceivingServices(ids: string[]) : void {

		const receivingServices	: AbstractTransmissionService[]
								= ids.map( id => this.getTransmissionService(id) )

		this.allowedReceivingServices = receivingServices
	}


	/**
	* Checks if the provided data can be used to setup a transmission with one
	* of the registered transmission service and if so returns it.
	*/
	public getMatchingTransmissionService(data: unknown) : AbstractTransmissionService {
		return this.transmissionServices.find( ts => ts.validateMeta(data) )
	}


	/**
	 * Checks if the provided data points to a registered transmission service and
	 * can therefore be used to setup a transmission.
	 *
	 */
	public validateMeta(data:unknown):boolean {
		return !!this.getMatchingTransmissionService(data)
	}

	/**
	 * Creates a new set of meta data for the allowed
	 * sending transmission service. Will throw an error if
	 * none of the available transmission service is allowed.
	 *
	 */
	public async createMeta() : Promise<unknown> {

		const sendingService 	: AbstractTransmissionService
								= this.allowedSendingService

		assert(sendingService, 'RccTransmissionService.createMeta() Requested transmission service was disabled.', { allowedSendingService: this.allowedSendingService })

		return await sendingService.createMeta()
	}



	/**
	 * Prepares a transmission with the provided data. Will throw an error if
	 * none of the available transmission service is allowed.
	 */
	public async setup(data:unknown): Promise<RccTransmission> {

		const sendingService 	: AbstractTransmissionService
								= this.allowedSendingService

		assert(sendingService, 'RccTransmissionService.setup() Requested transmission service was disabled.', { allowedSendingService: this.allowedSendingService })

		const transmission : RccTransmission = await sendingService.setup(data)

		// Create spy to see whether the transmission was successful
		const transmissionSpy : RccTransmissionSpy = new RccTransmissionSpy(transmission)

		// Listen to emissions from the spy and forward them
		// Catch errors and completes to keep the subscription alive for future transmissions
		transmissionSpy.successfulOutgoingTransmission$
			.subscribe({ next: (transmissionMeta: MetaType) =>
				this.successfulOutgoingTransmissions.next({ service: sendingService, meta: transmissionMeta })
			})
		transmissionSpy.failedOutgoingTransmission$
			.subscribe({ next: (transmissionMeta: MetaType) =>
				this.failedOutgoingTransmissions.next({ service: sendingService, meta: transmissionMeta })
			})

		return transmissionSpy

	}

	/**
	 * Looks for a registered transmission service matching the provided meta
	 * data and uses that one to setup a transmission.
	 *
	 * _Note:_ Use with caution! Unlike {@link RccTransmissionService.setup}
	 * the origin of the meta data is unknown to this method. So data could end
	 * up anywhere, if the meta data was not verified beforehand.
	 */
	public async setupWithExistingMeta(data: unknown, meta:unknown) : Promise<RccTransmission> {

		const sendingService	: AbstractTransmissionService
								= this.getMatchingTransmissionService(meta)

		assert(sendingService, 'RccTransmissionService.setupWithExistingMeta: No matching transmission service found.')

		if(sendingService !== this.allowedSendingService) await this.confirmSendingService(sendingService)

		const transmission		: RccTransmission
								= await sendingService.setupWithExistingMeta(data, meta)

		// Create spy to see whether the transmission was successful
		const transmissionSpy : RccTransmissionSpy = new RccTransmissionSpy(transmission)

		// Listen to emissions from the spy and forward them
		// Catch errors and completes to keep the subscription alive for future transmissions
		transmissionSpy.successfulOutgoingTransmission$
			.subscribe({ next: (transmissionMeta: MetaType) =>
				this.successfulOutgoingTransmissions.next({ service: sendingService, meta: transmissionMeta })
			})
		transmissionSpy.failedOutgoingTransmission$
			.subscribe({ next: (transmissionMeta: MetaType) =>
				this.failedOutgoingTransmissions.next({ service: sendingService, meta: transmissionMeta })
			})

		console.info('Handling transmission with ', sendingService.id)

		return transmissionSpy
	}



	private async confirmSendingService(transmissionService : AbstractTransmissionService): Promise<void> {


		const header	: string
						= 'TRANSMISSION.SEND_ERROR.HEADER'

		const message	: string
						= `{{${transmissionService.label}}}: {{${transmissionService.description}}}`

		await this.rccAlertController.present({

			header,
			message,
			buttons:	[
							{
								label:		'TRANSMISSION.SEND_ERROR.BUTTON.CANCEL',
								rejectAs: 	'dismiss'
							},
							{
								label:		'TRANSMISSION.SEND_ERROR.BUTTON.ENABLE',
								resolveAs: 	'enable'
							}
						]
		})
	}



	/**
	 * Should rarely be accessed from the outside because it lacks UI-interaction;
	 * use {@link #receiveAndAnnounce} instead. TODO: Add UI-interaction
	 */
	public async listen(meta: MetaType): Promise<unknown> {

		console.warn('RccTransmissionService.listen() listening cannot be stopped, only be ignored. This should be added in the future.')

		const receivingService	: AbstractTransmissionService
								= this.getMatchingTransmissionService(meta)

		assert(receivingService, 'RccTransmissionService.receive(): no matching TransmissionService found.')

		if(!this.allowedReceivingServices.includes(receivingService)) await this.confirmReceivingService(receivingService)

		const listenPromise	:	Promise<unknown>
							= 	receivingService.listen(meta)

		listenPromise
		.then(
			() => this.successfulIncomingTransmissions.next({ service: receivingService, meta: meta }),
			(error) => {
				if (!(error instanceof UserCanceledError))
					this.failedIncomingTransmissions.next({ service: receivingService, meta: meta })
			}
		)

		return await listenPromise
			
	}


	private async confirmReceivingService(transmissionService : AbstractTransmissionService): Promise<void> {

		const header				: string
									= 'TRANSMISSION.RECEIVE_ERROR.HEADER'

		const message				: string
									= `{{${transmissionService.label}}}: {{${transmissionService.description}}}`

		await this.rccAlertController.present({

			header,
			message,
			buttons:	[
							{
								label:		'TRANSMISSION.RECEIVE_ERROR.CANCEL',
								rejectAs: 	'dismiss'
							},
							{
								label:		'TRANSMISSION.RECEIVE_ERROR.ENABLE',
								resolveAs: 	'enable'
							}
						]
		})
	}

	public async receiveAndAnnounce(meta: unknown): Promise<void> {


		const transmissionResult 	:	Promise<unknown>
									=	this.listen(meta)

		const icon: DefinedIconName = this.buildInfoService.buildInfos.flavor === 'hcp' ? 'transmission_loading' : 'smiley_animated'
		await this.rccOverlayService.waitOrCancel(
			transmissionResult,
			{
				message: 		'DATA_TRANSFER_LOADING_TEXT',
				contextText: 	'DATA_TRANSFER_CONTEXT_TEXT',
				direction: 		'receive',
				icon: 			icon
			}
		)

		this.incomingDataService.next(await transmissionResult)
	}


	protected listenToIncomingDataService(): void {

		this.subscriptions.push(
			this.incomingDataService
			.pipe( filter( 	(data) 		=> this.validateMeta(data) ) )
			.subscribe( 	(config) 	=> { void this.receiveAndAnnounce(config) })
		)

	}


	public ngOnDestroy(): void {
		this.subscriptions.forEach( sub => sub.unsubscribe() )
	}

}
