import	{	InjectionToken			}	from '@angular/core'

import	{
			assert,
			assertProperty
		}								from '@rcc/core'

/**
 * Represents a one-time transmission. Objects of this interface will
 * be created by the transmission services. The payload will be baked into the
 * object upon creation and is not supposed to change. If you want to send
 * different data, create a new RccTransmission object.
 */
export interface RccTransmission {

	/**
	 * Data used by the transmission services to establish a connection on which
	 * to transfer the payload.
	 */
	meta		: (string|number)[]

	/**
	 * Sends the payload.
	 */
	start()		: Promise<unknown>

	/**
	 * Stops transmitting.
	 */
	cancel()	: Promise<unknown>
}

export function assertRccTransmission(valueToAssert: unknown) : asserts valueToAssert is RccTransmission {

	assertProperty(valueToAssert, ['meta', 'start', 'cancel'])

	assert( Array.isArray(valueToAssert.meta), 			'assertRccTransmission: .meta must be an array')

	const metaChecksOut	: 	boolean
						= 	valueToAssert.meta.every( entry => {

								if(typeof entry === 'string') return true
								if(typeof entry === 'number') return true

								return false
							})


	assert( metaChecksOut,								'assertRccTransmission: .meta must be an array of string and numbers')
	assert( typeof valueToAssert.start 	=== 'function', 'assertRccTransmission: .start must be function')
	assert( typeof valueToAssert.cancel	=== 'function', 'assertRccTransmission: .cancel must be function')
}

export abstract class AbstractTransmissionService {

	/**
	 * This id will be used when a string reference is needed.
	 */
	public abstract id : string


	/**
	 * The label is supposed to be a translation string. It is used to reference
	 * the service when addressing the user.
	 */
	public abstract label : string

	/**
	 * A description of the main properties of the service shown to the user;
	 * e.g. when selecting a preferred method of transmissions in the settings
	 * menu.
	 */
	public abstract description	: string

	/**
	 * Meta data is a piece of information determining which transmission
	 * service to use and how it should be configured. This method is meant to
	 * check if a specific piece of data can be used to setup a
	 * transmission with the service at hand.
	 */
	public abstract validateMeta(data: unknown)	: boolean

	/**
	 * Creates a set of meta data, ready to initialize a transmission.
	 */
	public abstract createMeta() : Promise<unknown>

	/**
	 * This method waits for a transmission, setup with the provided meta data.
	 * It should throw an Error if the provided meta data is incompatible with
	 * the service at hand.
	 */
	public abstract listen(meta: unknown) : Promise<unknown>


	/**
	 * Sets up a Transmission ready to send data. Creates its own meta data on
	 * the fly that can be used to listen to the transmission.
	 */
	public abstract setup(data: unknown) : Promise<RccTransmission>


	/**
	 * This one works like {@link AbstractTransmissionService.setup} but does
	 * not create its own meta data. It will use the one provided.
	 *
	 * _Note:_ Please use with caution, because this method has no way to verify
	 * the recipient of the data. Unlike
	 * {@link AbstractTransmissionService.setup} which creates the meta data
	 * itself, the meta data provided here is of unknown origin (at least to
	 * this method)! So data could end up anywhere, if the meta data was not
	 * verified beforehand.
	 */
	public abstract setupWithExistingMeta (data: unknown, meta:unknown)	: Promise<RccTransmission>

}

export const TRANSMISSION_SERVICE 	: InjectionToken<AbstractTransmissionService>
									= new InjectionToken('Some transmission service')
