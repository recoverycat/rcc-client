import { RccTransmissionSpy } from './transmission-spy.class'
import { RccTransmission } from './transmission.commons'

describe('RccTransmissionSpy', () => {
    let transmissionSpy     : RccTransmissionSpy                = undefined
    let wrappedTransmission : jasmine.SpyObj<RccTransmission>   = undefined

    beforeEach(() => {

        wrappedTransmission = jasmine.createSpyObj<RccTransmission>('RccTransmission', ['start', 'cancel'])
        wrappedTransmission.meta = ['rcc-cmb', 'pLofrmQFoL5RML47mVtt', 'be7F5On/ncvSMPfagthqagtXWje99XsnqFRIxGJZ4XM=']

        transmissionSpy = new RccTransmissionSpy(wrappedTransmission)

    })

    it('should be possible to create a spy on RccTransmission with the same metadata', () => {

        expect(transmissionSpy.meta).toBeDefined()
        expect(transmissionSpy.meta).toEqual(wrappedTransmission.meta)

    })

    describe('.start()', () => {

        it('should call RccTransmission.start()', () => {

            wrappedTransmission.start.and.resolveTo(undefined)

            void transmissionSpy.start()

            // eslint-disable-next-line @typescript-eslint/unbound-method
            expect(wrappedTransmission.start).toHaveBeenCalledTimes(1)
            
        })

    })

    describe('.cancel()', () => {

        it('should call RccTransmission.cancel()', () => {

            wrappedTransmission.cancel.and.resolveTo(undefined)

            void transmissionSpy.cancel()

            // eslint-disable-next-line @typescript-eslint/unbound-method
            expect(wrappedTransmission.cancel).toHaveBeenCalledTimes(1)

        })
        
    })

    describe('.successfulOutgoingTransmission$', () => {

        it('should emit successfulOutgoingTransmissions after a successful start', (done: DoneFn) => {

            wrappedTransmission.start.and.resolveTo(undefined)

            transmissionSpy.successfulOutgoingTransmission$.subscribe((value) => {
                expect(value).toBe(transmissionSpy.meta)
                done()
            })

            void transmissionSpy.start()

        })

        it('should not emit successfulOutgoingTransmissions after a failed start', async () => {

            wrappedTransmission.start.and.rejectWith(undefined)

            let nextCounter : number = 0

            transmissionSpy.successfulOutgoingTransmission$.subscribe(() => {
                nextCounter++
            })

            await   transmissionSpy.start()
                    .catch(() => undefined)

            expect(nextCounter).toBe(0)

        })

        it('successfulOutgoingTransmission$ should complete', (done: DoneFn) => {

            wrappedTransmission.start.and.resolveTo(undefined)

            transmissionSpy.successfulOutgoingTransmission$.subscribe({
                complete: () => done()
            })

            void transmissionSpy.start()

        })

    })

    describe('.failedOutgoingTransmission$', () => {

        it('should emit failedOutgoingTransmissions after a failed start', (done: DoneFn) => {

            wrappedTransmission.start.and.rejectWith(undefined)

            transmissionSpy.failedOutgoingTransmission$.subscribe((value) => {
                expect(value).toBe(transmissionSpy.meta)
                done()
            })

            void    transmissionSpy.start()
                    .catch(() => undefined)

        })

        it('should not emit failedOutgoingTransmissions after a successful start', async () => {

            wrappedTransmission.start.and.resolveTo(undefined)

            let nextCounter : number = 0

            transmissionSpy.failedOutgoingTransmission$.subscribe(() => {
                nextCounter++
            })

            await transmissionSpy.start()

            expect(nextCounter).toBe(0)

        })

        it('failedOutgoingTransmission$ should complete', (done: DoneFn) => {

            wrappedTransmission.start.and.resolveTo(undefined)

            transmissionSpy.failedOutgoingTransmission$.subscribe({
                complete: () => done()
            })

            void transmissionSpy.start()

        })
        
    })

})
