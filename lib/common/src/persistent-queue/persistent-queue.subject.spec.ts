import	{
			BehaviorSubject,
			firstValueFrom,
			Observable,
			skip,
			Subject,
			take,
			shareReplay,
			lastValueFrom
		}									from 'rxjs'

import	{
			PersistentQueueSubject,
		}									from './persistent-queue.subject'
import { fakeAsync, tick } from '@angular/core/testing'

describe('PersistentQueueSubject', () => {

	let 	storageSubject		: BehaviorSubject<unknown[]>
								= undefined


	const 	storeQueue			: (bs: BehaviorSubject<unknown[]>) => (queue: unknown[]) => Promise<void>
								= (bs: BehaviorSubject<unknown[]>) => (queue: unknown[]) => { bs.next(queue); return Promise.resolve() }

	const 	loadQueue			: (bs: BehaviorSubject<unknown[]>) => () => Promise<unknown[]>
								= (bs: BehaviorSubject<unknown[]>) => () => Promise.resolve(bs.value)

	let persistentQueueSubject 	: PersistentQueueSubject
								= undefined


	beforeEach( () => {
		storageSubject 			= new BehaviorSubject<unknown[]>([])
		persistentQueueSubject 	= new PersistentQueueSubject(storeQueue(storageSubject), loadQueue(storageSubject))
	})

	it('should extend Subject', () => {
		expect(persistentQueueSubject).toBeInstanceOf(Subject)
	})

	describe('push', () => {

		it('should add a new element to the end of the queue, only after all elements were loaded from the storage.',  fakeAsync(async () => {

			storageSubject 			= new BehaviorSubject<unknown[]>(['a', 'b', 'c'])

			persistentQueueSubject 	= new PersistentQueueSubject(storeQueue(storageSubject), loadQueue(storageSubject))

			persistentQueueSubject.push('d')

			expect(persistentQueueSubject.size).toBe(0)

			persistentQueueSubject.shift()

			await persistentQueueSubject.ready

			tick(100)

			expect(persistentQueueSubject.size).toBe(3)

			expect(storageSubject.value).toEqual(['b', 'c', 'd'])

		}))

		it('should update the storage, when an element was pushed to the queue', async () => {

			storageSubject 			= new BehaviorSubject<unknown[]>(['a', 'b', 'c'])
			persistentQueueSubject 	= new PersistentQueueSubject(storeQueue(storageSubject), loadQueue(storageSubject))

			await persistentQueueSubject.ready

			persistentQueueSubject.push('d')

			await firstValueFrom(storageSubject.pipe(skip(1)))

			expect(storageSubject.value).toEqual(['a', 'b', 'c', 'd'])

		})

	})

	describe('size', () => {

		it('should always equal the number of elements on the queue.', async () => {

			storageSubject 			= new BehaviorSubject<unknown[]>(['a', 'b', 'c'])
			persistentQueueSubject 	= new PersistentQueueSubject(storeQueue(storageSubject), loadQueue(storageSubject))

			expect(persistentQueueSubject.size).toBe(0)

			await persistentQueueSubject.ready

			expect(persistentQueueSubject.size).toBe(3)

			persistentQueueSubject.push('d')

			await firstValueFrom(storageSubject.pipe(skip(1)))

			expect(persistentQueueSubject.size).toBe(4)

			persistentQueueSubject.push('e')

			await firstValueFrom(storageSubject.pipe(skip(1)))

			expect(persistentQueueSubject.size).toBe(5)

			persistentQueueSubject.shift()

			await firstValueFrom(persistentQueueSubject)

			expect(persistentQueueSubject.size).toBe(4)

			expect(storageSubject.value).toEqual(['b', 'c', 'd', 'e'])

		})

	})

	describe('.shift()', () => {

		it('should remove the first element of the queue and emit it, only after the queue was loaded from the storage - even if called earlier than that.', async () => {

			storageSubject 			= 	new BehaviorSubject<unknown[]>(['a', 'b', 'c'])
			persistentQueueSubject 	= 	new PersistentQueueSubject(storeQueue(storageSubject), loadQueue(storageSubject))

			const result			:	unknown[]
									=	[]

			const nextFourElement$	: 	Observable<unknown>
									= 	persistentQueueSubject
										.pipe( take(4), shareReplay(4) )

			// Have to subscribe right away, or else shareReplay will not record anything
			nextFourElement$.subscribe( x =>  result.push(x) )

			persistentQueueSubject.push('d')

			persistentQueueSubject.shift()

			await persistentQueueSubject.ready

			persistentQueueSubject.shift()
			persistentQueueSubject.shift()
			persistentQueueSubject.shift()

			await lastValueFrom(nextFourElement$)

			expect(result).toEqual(['a', 'b', 'c', 'd'])


		})

		it('should update the storage when next value was emitted', async () => {

			storageSubject 			= new BehaviorSubject<unknown[]>(['a', 'b', 'c'])
			persistentQueueSubject 	= new PersistentQueueSubject(storeQueue(storageSubject), loadQueue(storageSubject))

			await persistentQueueSubject.ready

			persistentQueueSubject.shift()

			expect(storageSubject.value).toEqual(['a', 'b', 'c'])

			await firstValueFrom(storageSubject.pipe(skip(1)))

			expect(storageSubject.value).toEqual(['b', 'c'])
		})

		it('should not emit if the queue is empty', async () => {

			let count : number = 0

			storageSubject 			= new BehaviorSubject<unknown[]>([])
			persistentQueueSubject 	= new PersistentQueueSubject(storeQueue(storageSubject), loadQueue(storageSubject))

			persistentQueueSubject
			.subscribe( () => { count++ })

			await persistentQueueSubject.ready

			persistentQueueSubject.shift()
			persistentQueueSubject.push('x')
			persistentQueueSubject.shift()

			await firstValueFrom(storageSubject.pipe(skip(1)))

			expect(count).toBe(1)

		})

	})

	describe('.next()', () => {

		it('should call .push(), when called with a value', () => {
			const pushSpy	: jasmine.Spy
							= spyOn(persistentQueueSubject,'push')

			persistentQueueSubject.next('a')

			expect(pushSpy).toHaveBeenCalledOnceWith('a')
		})

		it('should call .shift(), when called with no value', () => {
			const shiftSpy	: jasmine.Spy
							= spyOn(persistentQueueSubject,'shift')

			persistentQueueSubject.next()

			expect(shiftSpy).toHaveBeenCalledOnceWith()
		})

	})


})
