
import 	{
			Subject,
		}								from 'rxjs'

/**
 * PersistentQueueSubjects represent a queue of objects.
 * The queue is meant to be stored somewhere in order to
 * persist after page reloads. The storing mechanism though
 * is out of scope and must be provided in the constructor.
 *
 * This class extends Subject, because the interface is
 * nearly (see below) identical and this way the parent
 * class can take care of all the subscription business.
 *
 */
export class PersistentQueueSubject<T = unknown> extends Subject<T> {

	private readonly queue				: T[]
										= []

	private	storingChain				: Promise<void>
										= undefined

	public	readonly ready				: Promise<void>
										= undefined



	/**

	 * The two callbacks in the parameters provide the
	 * minimal storage interface to keep the
	 * queue persistent.
	 */
	public constructor(
		private	storeQueue	: (queue: T[])	=> Promise<void>,
				loadQueue 	: () 			=> Promise<T[]>
	){

		super()

		this.ready 			=	loadQueue()
								.then( (queue) => { this.queue.unshift(...queue) })

	}

	/**
	 * Stores the current queue values by means of
	 * the storage interface provided at the constructor.
	 *
	 * Storing attempts are chained to prevent racing conditions,
	 * where earlier storage attempts take longer than expected
	 * and complete after later attempts, storing out-of-date values.
	 */
	private store() : void {

		if(this.storingChain === undefined) this.storingChain = this.ready

		this.storingChain 	=	this.storingChain
								.then( () => this.storeQueue(this.queue) )
								// Clean up the chain, when it terminates:
								.then( () => { if(originalChain === this.storingChain) delete this.storingChain })

		const originalChain	:	Promise<void>
							=	this.storingChain


	}

	public get size() : number {
		return this.queue.length
	}

	/**
	 * This method purely exists to complete the Subject
	 * interface. If it weren't present .next() would
	 * trigger the underlying parent Subject's .next() method,
	 * which is not wanted here.
	 *
	 * Will call .push, when called with a value.
	 *
	 * Will call .shift, when called without a value.
	 *
	 * Use .shift() and .push() instead.
	 */
	public next(x?:T) : void {
		if(x !== undefined) this.push(x)
		if(x === undefined)	this.shift()
	}

	/**
	 * Removes the first element from the Queue and emits it.
	 */
	public shift() : void	{

		void this.ready.then( () => {

			const value : T
						= this.queue.shift()

			if(value === undefined) return

			this.store()

			super.next(value)
		})

	}

	/**
	 * Adds an element to the queue.
	 */
	public push(x: T) : void 	{

		void this.ready.then( () => {

			this.queue.push(x)
			this.store()

		})
	}

}
