import	{
			Injectable
		}									from '@angular/core'

import	{
			RccStorage,
		}									from '@rcc/common'

import	{
			ItemStorage,
			Item,
			has
		}									from '@rcc/core'

import	{
			PersistentQueueSubject
		}									from './persistent-queue.subject'


export const PERSISTENT_QUEUE_STORAGE_NAME 	: string
											= 'persistent-queues'

export interface QueueConfig {
	id:		string,
	values:	unknown[]
}

export class Queue extends Item<QueueConfig>{

	public acceptsAsConfig(config: unknown): config is Queue  {

		if(!has(config, 'id', 'values')) 	return false
		if(!Array.isArray(config.values))	return false

		return true
	}
}


/**
 * Service to manage persistent queues (see {@link PersistentQueueSubject}).
 */
@Injectable({
	providedIn: 'root'
})
export class RccPersistentQueueService {

	private queues					: QueueConfig[]
									= []

	private readonly existingQueues	: Map<string, PersistentQueueSubject>
									= new Map<string, PersistentQueueSubject>()

	private readonly storage		: ItemStorage<Queue>
									= undefined

	public readonly ready			: Promise<void>
									= undefined

	public constructor (
		private readonly rccStorage	: RccStorage
	){

		this.storage 	= this.rccStorage.createItemStorage<Queue>(PERSISTENT_QUEUE_STORAGE_NAME)
		this.ready		= this.restore()

	}

	private async restore() : Promise<void> {
		this.queues = await this.storage.getAll()
	}

	/**
	 * Retrieves a specific queue from storage.
	 */
	private async load(queueId: string) : Promise<unknown[]> {

		await this. ready

		const queue	: QueueConfig
					= this.queues.find( q => q.id === queueId)

		return queue?.values || []

	}

	/**
	 * Saves a specific queue to the storage.
	 */
	private async store(queueId : string, values: unknown[]) : Promise<void> {

		await this.ready

		let queue :	QueueConfig
					=	this.queues.find( q => q.id === queueId)

		if(!queue){
			queue = { id:queueId, values }
			this.queues.push(queue)
		}

		queue.values = values

		await this.storage.store(this.queues)
	}

	/**
	 * Gets a specific PersistentQueueSubject by its id.
	 * If no PersistentQueueSubject exists for that id, creates a new one.
	 *
	 * When requesting the same id multiple times, will
	 * always return the same subject, in order to prevent
	 * storage inconsistencies, when two PersistentQueueSubjects try
	 * to update the storage for the same queue.
	 *
	 */
	public getOrCreate(queueId: string) : PersistentQueueSubject {

		const existingQueue	: PersistentQueueSubject
							= this.existingQueues.get(queueId)

		if(existingQueue) return existingQueue

		return this.createQueue(queueId)

	}

	private createQueue(queueId: string) : PersistentQueueSubject {

		const newQueue		: 	PersistentQueueSubject
							= 	new PersistentQueueSubject(
									(queue: unknown[]) 	=> this.store(queueId, queue),
									()					=> this.load(queueId)
								)

		this.existingQueues.set(queueId, newQueue)

		return newQueue
	}



}
