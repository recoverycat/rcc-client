import	{
			TestBed
		}								from '@angular/core/testing'

import	{
			BehaviorSubject,
			firstValueFrom,
			skip
		}								from 'rxjs'

import	{
			RccStorage
		}								from '@rcc/common'

import	{
			ItemStorage
		}								from '@rcc/core'

import	{
			Queue,
			QueueConfig
		}								from './persistent-queue.service'

import	{
			PersistentQueueSubject
		}								from './persistent-queue.subject'

import	{
			RccPersistentQueueService
		}								from './persistent-queue.service'

describe('RccPersistentQueueService', () => {

	let rccPersistentQueueService	:	RccPersistentQueueService
									=	undefined

	const storage					:	BehaviorSubject<QueueConfig[]>
									=	new BehaviorSubject<QueueConfig[]>([])

	const mockStorage 				:	RccStorage
									=	{

											createItemStorage: () : ItemStorage<Queue> => ({

												getAll: (): Promise<QueueConfig[]> => new Promise( resolve => requestAnimationFrame( () => resolve(storage.value) ) ),
												store: (queues: (QueueConfig | Queue)[]) : Promise<void> => {

													const configs 	: QueueConfig[]
																	= queues.map( x => ('config' in x) ? x.config : x)

													storage.next( configs )
													return new Promise( resolve => requestAnimationFrame( () => resolve() ) )
												}
											})

										} as unknown as RccStorage

	beforeEach( () => {

		TestBed
		.configureTestingModule({
			providers : [
				{
					provide: 	RccStorage,
					useValue:	mockStorage
				},
			]

		})

		rccPersistentQueueService	= TestBed.inject(RccPersistentQueueService)
	})

	describe('.getOrCreate', () => {

		it('should create a PersistentQueueSubject for each novel id, synced with the provided storage.', async () => {

			const persistentQueueSubject	: PersistentQueueSubject
											= rccPersistentQueueService.getOrCreate('my-new-queue-id')

			persistentQueueSubject.push('x')

			await firstValueFrom(storage.pipe(skip(1)))

			expect(persistentQueueSubject.size).toEqual(1)

			expect(storage.value).toContain({ id:'my-new-queue-id', values: ['x'] })

			persistentQueueSubject.push('y')

			await firstValueFrom(storage.pipe(skip(1)))

			expect(persistentQueueSubject.size).toEqual(2)

			expect(storage.value).toContain({ id:'my-new-queue-id', values: ['x', 'y'] })

			persistentQueueSubject.shift()

			await firstValueFrom(storage.pipe(skip(1)))

			expect(persistentQueueSubject.size).toEqual(1)

			expect(storage.value).toContain({ id:'my-new-queue-id', values: ['y'] })


		})

		it('should return the same PersistentQueueSubject when called with the same id repeatedly.', () => {

			const persistentQueueSubject1	: PersistentQueueSubject
											= rccPersistentQueueService.getOrCreate('my-queue-id')

			const persistentQueueSubject2	: PersistentQueueSubject
											= rccPersistentQueueService.getOrCreate('my-queue-id')

			expect(persistentQueueSubject1).toBe(persistentQueueSubject2)


		})

	})


})
