import  {   Injectable  }   from '@angular/core'
import      buildInfo       from '@rcc/build_info.json'

interface BuildInfos {
    environment :   string,
    flavor      :   string,

    commit      :   string,
    describe    :   string,
    short       :   string,
    tag         :   string,
	tagDate	 	:   string,
}


@Injectable()
export class RccBuildInfoService {

    public buildInfos: BuildInfos = buildInfo as BuildInfos

}
