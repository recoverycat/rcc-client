import	{	InjectionToken	}	from '@angular/core'

export const IS_MEDICAL_PRODUCT: InjectionToken<string> = new InjectionToken('Is this a medical product')
