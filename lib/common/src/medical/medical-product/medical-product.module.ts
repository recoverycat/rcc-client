import	{ Injector, NgModule, Type, inject, provideAppInitializer } 									from '@angular/core'
import	{
			IS_MEDICAL_PRODUCT,
		}									from '../medical.commons'

/**
 *
 * This function is used to assert, that the MedicalProductModule is imported
 * only once by querying the Angular Injector. As for now this is is
 * tailored to check for the MedicalProductModule and cannot be used for
 * anything else. It is also unclear whether 'ɵmod' and/or 'ɵinj' will be
 * present and work the same way in future Angular releases.
 */
export function assertSingleImport(injector: Injector): void {
	// Disabling eslint, because it complains about ts being ignored.

	/* eslint-disable */
	const defTypes			:	Type<unknown>[]
								// @ts-ignore
							=	Array.from(injector.injectorDefTypes)

	const importingModules	:	Array<unknown>
							=	defTypes.filter(defType => {

									// @ts-ignore
									if (!defType.ɵmod) throw new Error('Unable to access ɵmod while checking for multiple module imports of MedicalProductModule.')

									// @ts-ignore
									if (!defType.ɵinj) throw new Error('Unable to access ɵinj while checking for multiple module imports of MedicalProductModule.')

									// @ts-ignore
									return [...defType.ɵmod.imports, ...defType.ɵinj.imports].includes(MedicalProductModule)

								})


	/**
	 * Something went wrong;
	 * This code should only run after MedicalProductModule was imported,
	 * so it should show up here:
	 */
	if (importingModules.length === 0) throw new Error('Unable to check MedicalProductModule for multiple imports properly, cannot detect any import of MedicalProductModule.')


	// Everything is fine:
	if (importingModules.length === 1) return



	console.info('Multiple modules importing MedicalProductModule: ', importingModules)
	throw new Error('Multiple imports of MedicalProductModule detected, MedicalProductModule may only be imported once.')

	/* eslint-enable */

}

@NgModule({
	providers: [
		{ provide: IS_MEDICAL_PRODUCT, multi: false, useValue: true },
		provideAppInitializer(() => {
			const initializerFn: () => void = ((injector: Injector) => () => assertSingleImport(injector))(inject(Injector))
			return initializerFn()
		})
	]
})
export class MedicalProductModule {}
