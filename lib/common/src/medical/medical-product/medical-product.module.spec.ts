import { TestBed } from '@angular/core/testing'
import { MedicalProductModule, assertSingleImport } from './medical-product.module'
import { Injector, NgModule, Type } from '@angular/core'

@NgModule({
	imports: [
		MedicalProductModule
	]
})
export class MockModule{}

describe('MedicalProductModule', () => {

	describe('on import', () => {
		it('should throw an error if already imported earlier by another module', () => {
			expect(() => {
				TestBed.configureTestingModule({
					imports: [MedicalProductModule, MockModule]
				})
				TestBed.inject(Injector)
			}).toThrow()
		})
	})

	describe('assertSingleImport', () => {

		it('should assert single import on app initialization', () => {
			// eslint-disable
			const injectorDefTypes: Type<unknown>[] = [
				// @ts-expect-error, mocking for unit tests
				{ ɵmod: { imports: [MedicalProductModule] } },
				// @ts-expect-error, mocking for unit tests

				{ ɵmod: { imports: [MedicalProductModule] } }
			]
			const injector : Injector = { injectorDefTypes } as unknown as Injector

			expect(() => assertSingleImport(injector)).toThrow()


			// const spyAssertSingleImport : jasmine.Spy= spyOn(injector, 'get').and.callFake(() => assertSingleImport(injector))

			/* eslint-enable */

		})

	})

})
