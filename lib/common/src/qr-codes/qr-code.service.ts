import 	{	Injectable 					} 	from '@angular/core'
import	{	RccModalController			}	from '../modals-provider'
import	{	IncomingDataService			}	from '../incoming-data'
import	{	RccQrCodeScanner			}	from './qr-code.commons'

@Injectable()
export class QrCodeService {

	public constructor(
		public rccModalController	: RccModalController,
		public rccQrCodeScanner		: RccQrCodeScanner,
		public incomingDataService		: IncomingDataService
	){}

	public present(): Promise<unknown> {
		return Promise.reject('QrCodeService.present not yet implemented.')
	}

	/**
	 * Awaits RccQrCodeScanner's scan function
	 * @returns Scan result as JSON if possible, otherwise as it is
	 */
	public async scan(): Promise<unknown> {
		const result: unknown = await this.rccQrCodeScanner.scan()
		try{
			return JSON.parse(result as string) as object
		}catch(e) {
			return result
		}
	}

	/**
	 * Awaits a result from this.scan() and sends it to IncomingData
	 */
	public async scanAndAnnounce(): Promise<void> {
		const result: unknown = await this.scan()

		if (result) this.incomingDataService.next(result)
	}
}
