import	{
			NgModule,
			Component,

		}								from '@angular/core'

import	{	MainMenuModule			}	from '../main-menu'
import	{
			HomePageModule,
		}								from '../home'
import	{	SharedModule			}	from '../shared-module'
import	{	provideTranslationMap	}	from '../translations'

import	{	QrCodeComponent			}	from './qr-code-presentation/qr-code.component'
import	{	RccQrCodeScanner		}	from './qr-code.commons'
import	{	QrCodeService			}	from './qr-code.service'

import	en	from './i18n/en.json'
import	de	from './i18n/de.json'
export interface QrCodeModuleConfig {
	homePageEntry?: 		boolean | number
}

@Component({
	template:	`
					<ion-item [button] = "true" (click) = "scan()">
						<ion-label
							[id]	= "'QRCODE.SCAN' | toID: 'rcc-e2e'"
						>
							{{ "QRCODE.SCAN" | translate }}
						</ion-label>

						<ion-icon [name] = "'qr-code' | rccIcon" slot = "end"></ion-icon>
					</ion-item>
				`,
	standalone: false
})
export class MenuEntryQrCodeComponent {

	public constructor(
		public qrCodeService			: QrCodeService,
	){}

	public scan(): void {
		this.qrCodeService.scanAndAnnounce()
		.catch(console.error)
	}
}

@NgModule({
	declarations: [
		QrCodeComponent,
		MenuEntryQrCodeComponent
	],
	imports:[
		SharedModule,
		MainMenuModule,
		HomePageModule,
	],
	providers: [
		provideTranslationMap('QRCODE', { en, de }),
		QrCodeService,
		RccQrCodeScanner,
	],
	exports: [
		QrCodeComponent,
	]
})

export class QrCodeModule {}
