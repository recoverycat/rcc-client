import	{
			NgModule,
			Provider,
			ModuleWithProviders,
		}											from '@angular/core'
import	{
			provideMainMenuEntry,
			Factory,
			Action,
			WithPosition,
			getEffectivePosition,
		}											from '@rcc/common'
import	{
			QrCodeModule
		}											from './qr-code.module'
import	{
			QrCodeService
		}											from './qr-code.service'

@NgModule({
	imports:[
		QrCodeModule,
	],
	providers: [
		QrCodeService,
	],
})

export class QrCodeMainMenuEntryModule {
	public static addEntry(config?: WithPosition): ModuleWithProviders<QrCodeMainMenuEntryModule> {

		const mainMenuEntry	: Factory<Action> =	{
			deps:	[QrCodeService],
			factory: (qrCodeService: QrCodeService) => ({
				position: 		getEffectivePosition(config, 1),
				icon:			'scan',
				label:			'QRCODE.SCAN',
				category:		'receive',
				handler:		() =>  qrCodeService.scanAndAnnounce().catch(console.error)
			})
		}

		const providers : Provider[] = [provideMainMenuEntry(mainMenuEntry)]

		return {
			ngModule:	QrCodeMainMenuEntryModule,
			providers
		}
	}

}
