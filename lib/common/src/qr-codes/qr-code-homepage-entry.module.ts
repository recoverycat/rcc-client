import	{
			NgModule,
			ModuleWithProviders,
			Provider,
		}								from '@angular/core'
import	{
			getEffectivePosition,
			WithPosition,
		}								from '../actions'
import	{
			provideHomePageEntry
		}								from '../home'
import	{	Factory					}	from '../interfaces'
import	{	Action					}	from '../actions'
import	{	QrCodeService			}	from './qr-code.service'
import	{	QrCodeModule			}	from './qr-code.module'


@NgModule({
	imports:[
		QrCodeModule,
	],
	providers: [
		QrCodeService,
	],
})

export class QrCodeHomePageEntryModule {

	/**
	* This method can add entries to the home page.
	*
	* Calling it without parameter, will add entries to the home
	* page automatically at reasonably adequate positions.
	*
	* If you want to determine the positions yourself provide a `config` parameter :
	*
	* | .position  | value                  | effect                                                    |
	* |------------|------------------------|-----------------------------------------------------------|
	* | .position  | true                   | adds home page entry at a reasonably adequate position    |
	* |            | positive number        | adds home page entry at position counting from the top    |
	* |            | negative number        | adds home page entry at position counting from the bottom |
	* |            | undefined        		| adds home page entry at a position somewhere in between   |
	* |------------|------------------------|-----------------------------------------------------------|
	*
	* Example: 	`QrCodeHomePageEntryModule.addEntry({ position: 1 })`,
	*/

	public static addEntry(config?: WithPosition): ModuleWithProviders<QrCodeHomePageEntryModule> {

		const homePageEntry : Factory<Action> =	{
			deps:	[QrCodeService],
			factory: (qrCodeService: QrCodeService) => ({
					position: 		getEffectivePosition(config, 1),
					icon:			'receive_question',
					label:			'SCAN_QR_CODE_HOME_PAGE_LABEL',
					description:	'SCAN_QR_CODE_HOME_PAGE_DESCRIPTION',
					category:		'receive',
					handler:		() =>  qrCodeService.scanAndAnnounce().catch(console.error)
			})
		}

		const providers : Provider[] = [provideHomePageEntry(homePageEntry)]

		return {
			ngModule:	QrCodeHomePageEntryModule,
			providers
		}
	}
}
