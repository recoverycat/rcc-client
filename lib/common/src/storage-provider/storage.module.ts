import 	{
			NgModule,
			Type,
			ModuleWithProviders

		}									from '@angular/core'

import	{	RccVoidStorage				}	from './void-storage.service'
import	{	RccStorage					}	from './abstract-storage.class'

@NgModule({
	providers: [
		RccVoidStorage,
		{ provide: RccStorage, useExisting: RccVoidStorage }
	]
})
export class StorageProviderModule {

	static forRoot(storageSevice: Type<RccStorage>): ModuleWithProviders<StorageProviderModule>{
		return 	{
					ngModule: StorageProviderModule,
					providers: 	[
									{ provide: RccStorage, useExisting: storageSevice }
								]
				}
	}

}
