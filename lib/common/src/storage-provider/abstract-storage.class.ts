import	{
			Item,
			ItemStorage
		}								from '@rcc/core'

export abstract class RccStorage {

	abstract createItemStorage<I extends Item>(name:string):ItemStorage<I>

	abstract clearItemStorage(name: string):Promise<void>

	abstract getStorageNames(): Promise<string[]>
}


