import	{ 	TestBed						}	from '@angular/core/testing'
import	{
			DefaultLabelComponent,
			ItemService
		}									from './item.service'

import	{
			Item,
			ItemStore
		}									from '@rcc/core'
import	{
			ItemAction,
			provideItemAction,
			provideItemRepresentation,
			ItemRepresentation
		}									from './item.commons'

import	{
			Action,
		}									from '../actions'



interface TestConfig{}
class TestItem extends Item<TestConfig> {
	public static acceptsAsConfig(x: unknown): x is TestConfig { return true }
}

class TestItemLabelComponent{}

class TestItemStore extends ItemStore<TestItem> {
	public constructor(){
		super({ itemClass: TestItem })
	}
}


describe('ItemService without ItemActions and ItemRepresentations', () => {
	let itemService: ItemService = undefined!

	beforeEach(() => {
		TestBed.configureTestingModule({

			providers: [

				ItemService,

			]

		})
		itemService = TestBed.inject(ItemService)
	})



	it('#constructor() for ItemService without ItemActions and ItemRepresentations', () => {
		expect(itemService).toBeTruthy()
		expect(itemService).toBeInstanceOf(ItemService)
	})

	it('#getItemRepresentation()', () => {
		const item : Item = new TestItem({})
		const itemRepresentation : ItemRepresentation = itemService.getItemRepresentation(item)

		expect(itemRepresentation).toBeDefined()
		expect(itemRepresentation.itemClass).toBe(TestItem)
		expect(itemRepresentation.name).toBe('ITEMS.MISSING_REPRESENTATION')
		expect(itemRepresentation.icon).toBe('item')
		expect(itemRepresentation.labelComponent).toBe(DefaultLabelComponent)

	})

	it('#getActions()', () => {

		const item : Item = new TestItem({})
		const actions : Action[] = itemService.getActions(item)

		expect(actions).toBeDefined()
		expect(actions).toBeInstanceOf(Array)
		expect(actions.length).toBe(0)

	})

})

describe('ItemService with ItemRepresentations', () => {
	let itemService: ItemService = undefined!

	beforeEach(() => {
		const itemRepresentation : ItemRepresentation		= {
			itemClass: TestItem,
			name: 'TestItemName',
			cssClass: 'TestItemCssClass',
			icon: 'TestItemIcon',
			labelComponent: TestItemLabelComponent,
		}

		TestBed.configureTestingModule({

			providers: [

				ItemService,
				provideItemRepresentation(itemRepresentation)

			]

		})
		itemService = TestBed.inject(ItemService)
	})

	it('#constructor() for ItemService with ItemRepresentations', () => {

		expect(itemService).toBeTruthy()

		expect(itemService).toBeInstanceOf(ItemService)
	})

	it('#getItemRepresentation() for ItemService with ItemRepresentations', () => {
		const item : Item = new TestItem({})

		const itemRepresentation : ItemRepresentation = itemService.getItemRepresentation(item)

		expect(itemRepresentation).toBeDefined()
		expect(itemRepresentation.itemClass).toBe(TestItem)
		expect(itemRepresentation.name).toBe('TestItemName')
		expect(itemRepresentation.icon).toBe('TestItemIcon')
		expect(itemRepresentation.labelComponent).toBe(TestItemLabelComponent)

	})

})

describe('ItemService with ItemActions', () => {
	let itemService	: ItemService = undefined!
	const actionA		: Action = {
									icon: 'TestItemIcon',
									label: 'TestItemLabel',
									path: '/test/path'
								}
	const actionB		: Action = {
									icon: 'TestItemIconB',
									label: 'TestItemLabelB',
									path: '/test/path/b'
								}
	const actionLocal	: Action = {
									icon: 'TestItemLocalIcon',
									label: 'TestItemLocalLabel',
									path: '/test/local/path'
								}
	const itemActionA	: ItemAction<TestItem>	= {
													role: 'details' as const ,
													itemClass: TestItem,
													storeClass: undefined,
													getAction: () => actionA
												}
	const itemActionB	: ItemAction<TestItem>	= {
													role: 'destructive' as const ,
													itemClass: TestItem,
													storeClass: undefined,
													getAction: () => actionB
												}

	const itemActionLocal : ItemAction<TestItem>	= {
														role: 'details' as const ,
														itemClass: TestItem,
														storeClass: undefined,
														getAction: () => actionLocal
													}

	beforeEach(() => {
		TestBed.configureTestingModule({

			providers: [

				ItemService,
				TestItemStore,
				provideItemAction(itemActionA as ItemAction),  // TODO: Remove typecast
				provideItemAction(itemActionB as ItemAction)   // TODO: Remove typecast

			]

		})
		itemService = TestBed.inject(ItemService)
	})

	it('#constructor()', () => {
		expect(itemService).toBeTruthy()
		expect(itemService).toBeInstanceOf(ItemService)
	})


	it('#getActions() for global actions only', () => {

		const item : Item = new TestItem({})
		const actions : Action[] = itemService.getActions(item)

		expect(actions).toBeDefined()
		expect(actions).toBeInstanceOf(Array)
		expect(actions).toHaveSize(2)
		expect(actions).toContain(actionA)
		expect(actions).toContain(actionB)

	})

	it('#getActions() for global and local actions', () => {

		const item : Item = new TestItem({})
		const actions : Action[] = itemService.getActions(item, undefined, [itemActionLocal])

		expect(actions).toBeDefined()
		expect(actions).toBeInstanceOf(Array)
		expect(actions).toHaveSize(3)
		expect(actions).toContain(actionA)
		expect(actions).toContain(actionB)
		expect(actions).toContain(actionLocal)


	})

	it('#getActions() for local actions only', () => {

		const item : Item = new TestItem({})
		const actions : Action[] = itemService.getActions(item, undefined, [itemActionLocal], true)

		expect(actions).toBeDefined()
		expect(actions).toBeInstanceOf(Array)
		expect(actions).toHaveSize(1)
		expect(actions).toContain(actionLocal)


	})

	it('#getActions() for global & local actions and roles', () => {

		const item : Item = new TestItem({})
		let actions : Action[] = itemService.getActions(item, ['details'], [itemActionLocal])

		expect(actions).toBeDefined()
		expect(actions).toBeInstanceOf(Array)
		expect(actions).toHaveSize(2)
		expect(actions).toContain(actionA)
		expect(actions).toContain(actionLocal)
		expect(actions).not.toContain(actionB)

		actions = itemService.getActions(item, ['destructive'], [itemActionLocal])

		expect(actions).toBeDefined()
		expect(actions).toBeInstanceOf(Array)
		expect(actions).toHaveSize(1)
		expect(actions).toContain(actionB)
		expect(actions).not.toContain(actionA)
		expect(actions).not.toContain(actionLocal)


	})
})
