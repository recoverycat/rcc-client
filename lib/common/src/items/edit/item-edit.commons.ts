import	{	 Item 		}	from '@rcc/core'

export interface ItemEditResult<I extends Item = Item, A = never> {
	item:		I,
	artifacts?: A
}
