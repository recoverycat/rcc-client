import	{
			Injectable,
			Optional,
			Inject,
			Provider
		}									from '@angular/core'

import	{
			assert,
			Item,
			ItemStore,
			UserCanceledError
		}									from '@rcc/core'

import	{	RccModalController			}	from '../../modals-provider'

import	{
			ITEM_SELECTION_FILTERS,
			ItemSelectionFilter
		}									from '../item.commons'

import	{	ItemSelectModalComponent	}	from './item-select-modal.component'


export function provideItemSelectModal(ModalComponent: typeof ItemSelectModalComponent) : Provider {
	return 	{
				provide: 	ItemSelectModalComponent,
				useValue:	ModalComponent
			}
}

@Injectable()
export class ItemSelectService {

	public constructor(
		@Optional() @Inject(ItemSelectModalComponent)
		private itemSelectModalComponent	: typeof ItemSelectModalComponent,
		@Optional() @Inject(ITEM_SELECTION_FILTERS)
		private	itemSelectionFilters		: ItemSelectionFilter[],
		private rccModalController			: RccModalController,
	){
		if(!this.itemSelectionFilters) 		this.itemSelectionFilters		= []
		if(!this.itemSelectModalComponent)	this.itemSelectModalComponent	= ItemSelectModalComponent
	}


	/**
	 * Opens a modal to select items.
	 * TODO: Needs documentation.
	 */
	public async select<I extends Item>(
		config : {
			stores?			: ItemStore<I>[],
			items?			: I[],
			preselect?		: I[],
			filters?		: ItemSelectionFilter[],
			singleSelect?	: boolean,
			heading?		: string,
			subHeading?		: string,
			message?		: string

		}
	)	: Promise<I[]> {

		config.filters = config.filters || this.itemSelectionFilters

		const result : I[] =	 await 	this.rccModalController.present(this.itemSelectModalComponent, config )

		// If result isn't even an empty array, assume the user didn't make a selection
		if (!result && config.singleSelect) throw new UserCanceledError('ItemService.selectItems() no selection made')

		assert( Array.isArray(result), 							'ItemService.selectItems() result is not an array.', result)
		assert( result.every( item => item instanceof Item), 	'ItemService.selectItems() result is not an array of Item.', result)
		return result
	}


}
