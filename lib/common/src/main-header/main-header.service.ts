import	{
			Injectable,
			Type,
			TemplateRef,
			Injector,
			Optional,
			Inject,
			ViewContainerRef,
			ComponentRef
		}									from '@angular/core'

import	{	sortByKeyFn					}	from '@rcc/core'

import	{
			MAIN_HEADER_CONFIG,
			MainHeaderItemConfig
		}									from './main-header.commons'

@Injectable()
export class MainHeaderService {

	public	templates!			: TemplateRef<unknown>[]

	public constructor(
		@Optional() @Inject(MAIN_HEADER_CONFIG)
		public mainHeaderItemConfig		: MainHeaderItemConfig [],
		public injector					: Injector,
		private viewContainerRef		: ViewContainerRef,
	) {
		this.createTemplates()
	}

	public createTemplates(): void {

		this.templates =	(this.mainHeaderItemConfig || [])
							.sort(sortByKeyFn('position'))
							.map( item => {

								const component: Type<unknown>	=	((item).component || item) as Type<unknown>

								const componentRef: ComponentRef<unknown>
									= this.viewContainerRef.createComponent(component)

								// @ts-expect-error ignore
								// eslint-disable-next-line @typescript-eslint/no-unsafe-return
								return componentRef.instance.componentTemplate
							}) as TemplateRef<unknown>[]
	}
}
