import	{
			Component,
			Input,
			TemplateRef
		}										from '@angular/core'
import	{	MainHeaderService				}	from '../main-header.service'



@Component({
	selector: 		'rcc-common-header',
	templateUrl: 	'./common-header.component.html',
	styleUrls: 		['./common-header.component.scss'],
    standalone:		false
})
export class CommonHeaderComponent {
	@Input()
	public fab!: boolean

	public templates: TemplateRef<unknown>[]

	public constructor(
		public mainHeaderService: MainHeaderService
	) {

		this.templates = this.mainHeaderService.templates
	}
}
