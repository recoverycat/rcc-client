
import 	{
			Component,
			ViewChild,
			TemplateRef,
			AfterViewInit
		} 									from '@angular/core'

@Component({
    template: '',
    standalone: false
})
export class MainHeaderItemComponent implements AfterViewInit {
	@ViewChild(TemplateRef, { static: true })
	public componentTemplate!: TemplateRef<unknown>

	public ngAfterViewInit(): void {
		if(!this.componentTemplate) throw new Error('ComponentTemplate missing.')
	}
}
