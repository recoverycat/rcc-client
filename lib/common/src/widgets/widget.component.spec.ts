import { TestBed } from '@angular/core/testing'
import { Injector, Type } from '@angular/core'
import { RccWidgetComponent } from './widget.component'
import { WidgetsService } from './widgets.service'
import { CARD_VIEW, GREYED_OUT, WidgetComponent, WidgetControl } from './widgets.commons'

class MockControlType extends WidgetControl {
}

class MockWidgetComponent extends WidgetComponent<MockControlType> {

	public static controlType: Type<MockControlType> = MockControlType

	public static widgetMatch(): number {
		return 0
	}

	public constructor(public widgetControl: MockControlType) {
		super(widgetControl)
	}
}

describe('RccWidgetComponent', () => {
	let component: RccWidgetComponent = {} as RccWidgetComponent
	let widgetsService: jasmine.SpyObj<WidgetsService> = {} as jasmine.SpyObj<WidgetsService>
	let parentInjector: jasmine.SpyObj<Injector> = {} as jasmine.SpyObj<Injector>

	beforeEach(() => {
		widgetsService = jasmine.createSpyObj<WidgetsService>('WidgetsService', ['getWidgetMatches'])
		widgetsService.getWidgetMatches.and.returnValue([MockWidgetComponent])
		parentInjector = jasmine.createSpyObj<Injector>('Injector', ['get'])

		TestBed.configureTestingModule({
			providers: [
				RccWidgetComponent,
				{ provide: WidgetsService, useValue: widgetsService },
				{ provide: Injector, useValue: parentInjector }
			]
		})

		component = TestBed.inject(RccWidgetComponent)
	})

	it('should create injector with correct providers', () => {
		// Setup
		const mockControl: MockControlType = new MockControlType()

		// 1. Test
		// Set the control
		component.widgetControl = mockControl

		component.greyedOut = false
		component.cardView = false


		// Force the component to update its internal state
		component.setControl(mockControl)

		// Act
		component.updateInjector()

		// Assert
		expect(component.injector).toBeDefined()
		expect(component.injector.get(MockControlType)).toBe(mockControl)
		expect(component.injector.get(GREYED_OUT)).toBe(false)
		expect(component.injector.get(CARD_VIEW)).toBe(false)

		// 2. Test
		// Set new values for options
		component.greyedOut = true
		component.cardView = true


		// Force the component to update its internal state
		component.setControl(mockControl)

		// Act
		component.updateInjector()

		// Assert
		expect(component.injector).toBeDefined()
		expect(component.injector.get(MockControlType)).toBe(mockControl)
		expect(component.injector.get(GREYED_OUT)).toBe(true)
		expect(component.injector.get(CARD_VIEW)).toBe(true)


	})
})
