import	{
				Component,
				Injector,
				Input,
				OnChanges,
				Type
		}							from '@angular/core'

import	{
				assert
		}							from '@rcc/core'

import	{
				CARD_VIEW,
				GREYED_OUT,
				WidgetComponentType,
				WidgetControl,
		}							from './widgets.commons'

import	{
				WidgetsService
		}							from './widgets.service'


/**
 * This component will be replaced by the best matching registered {@link WidgetComponent} at runtime.
 * {@link WidgetsModule#readme|Tutorial: How to use Widgets}
 */
@Component({
    selector: 'rcc-widget',
    styleUrls: ['./widget.component.css'],
    template: `
					<ng-container
						*ngIf						= "component && injector"
						[ngComponentOutlet]			= "component"
						[ngComponentOutletInjector]	= "injector"
					>
					</ng-container>
				`,
    standalone: false
})
export class RccWidgetComponent implements OnChanges {

	@Input()
	public set widgetControl(control: WidgetControl) {	this.setControl(control) }

	@Input()
	public set widgetComponent(component: WidgetComponentType){ this.setWidgetComponent(component) }

	@Input()
	public greyedOut: boolean = false

	@Input()
	public cardView: boolean = false
	/**
	 * The {@link widgetComponentType} explicitly set by {@link #widgetComponent} or the component with the
	 * highest matching score with respect to {@link #widgetControl}.
	 */
	public get component()				: WidgetComponentType<unknown> { return this.explicitComponent || this.bestMatch }


	/**
	 * We need this to inject the control into the chosen
	 * {@link widgetComponentType} at {@link #component}.
	 */
	public injector!					: Injector


	/**
	 * Array of all {@link WidgetComponents} matching {@link #widgetControl}.
	 */
	protected matchingWidgetComponents 	: WidgetComponentType<unknown>[]

	/**
	 * {@link WidgetComponentType} with the highest matching score with
	 * respect to {@link #widgetControl}.
	 */
	protected bestMatch					: WidgetComponentType<unknown>

	/**
	 * {@link WidgetComponentType} explicitly set by {@link #widgetComponent}
	 */
	protected explicitComponent			: WidgetComponentType<unknown>


	/**
	 * {@link WidgetControl} explicitly set by {@link #widgetControl}
	 */
	protected control					: WidgetControl


	public constructor(

		private parentInjector		: Injector,
		public	widgetsService		: WidgetsService

	){}

	public ngOnChanges(): void {
		this.updateInjector()
	}

	/**
	 * Called when {@link #widgetControl} is set. Finds the best matching {@link WidgetComponent}
	 * for control and calls {@link #updateInjector}. Throws an error, if no match is found.
	 */
	public setControl(control: WidgetControl): void {

		this.control					= control
		this.matchingWidgetComponents 	= this.widgetsService.getWidgetMatches(control)
		this.bestMatch					= this.matchingWidgetComponents[0]

		assert(this.bestMatch, `RccWidgetComponent.setControl() unable to find a matching widget component for ${control.constructor.name}. Please provide a corresponding widget component.`, control)

		this.updateInjector()

	}


	/**
	 * Set a {@link WidgetComponentType} to use instead of the best matching {@link WidgetComponent}
	 * and calls {@link #updateInjector}.
	 */
	protected setWidgetComponent(component: WidgetComponentType): void {
		this.explicitComponent = component

		this.updateInjector()
	}


	public updateInjector() : void {

		// Ensure component is defined and has a controlType
		if (!this.component || !this.component.controlType)
			throw new Error('RccWidgetComponent.updateInjector() called without a valid component or controlType.')

		const controlType: Type<unknown> 	= this.component.controlType

		// If no control is set, the component is not yet ready;
		// once it is set this method should be called again.
		if(!this.control) return

		assert(this.control instanceof controlType, 'RccWidgetComponent.updateInjector() control type mismatch.', this.control)

		this.injector =	Injector.create({
							providers: 	[
											{
												provide: 	controlType,
												useValue: 	this.control
											},
											{
												provide:	GREYED_OUT,
												useValue: 	this.greyedOut
											},
											{
												provide:	CARD_VIEW,
												useValue: 	this.cardView
											},
										],

							parent:		this.parentInjector
						})
	}

}
