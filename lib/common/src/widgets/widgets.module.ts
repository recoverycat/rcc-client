import 	{
			NgModule,
		}							from '@angular/core'

import	{
			CommonModule,
		}							from '@angular/common'

import	{
			WidgetComponent
		}							from './widgets.commons'

import	{
			RccWidgetComponent
		}							from './widget.component'


import	{
			WidgetsService
		}							from './widgets.service'

/**
 * This Module manages widgets.
 *
 * A widget is a component that is dynamically added at runtime depending on the context.
 * Modules can register widgets to the WidgetModule. Every widget component comes with a static
 * widgetMatch method (see {@link WidgetComponent}), that tells the app how well the widget fits the context.
 *
 * This module does NOT provide any widgets by itself, it only provides the infrastructure.
 * {@link modules/WidgetsModule.html#readme | Tutorial: How to use widgets}
 */
@NgModule({

	imports: [
		CommonModule
	],
	providers:[
		WidgetsService
	],
	declarations :[
		RccWidgetComponent,
	],
	exports: [
		RccWidgetComponent,
	]

})
export class WidgetsModule { }
