import	{
				Directive,
				Injector,
				Input,
				ViewContainerRef
		}							from '@angular/core'

import	{
				assert
		}							from '@rcc/core'

import	{
				WidgetComponentType,
				WidgetControl,
		}							from './widgets.commons'

import	{
				WidgetsService
		}							from './widgets.service'


/**
 * This component will be replaced by the best matching registered {@link WidgetComponent} at runtime.
 * {@link WidgetsModule#readme|Tutorial: How to use Widgets}
 */
@Directive({
	selector: 	'[rccWidgetControl]',
})
export class RccWidgetDirective {

	@Input()
	public set rccWidgetControl(control: WidgetControl) {	this.setControl(control) }


	/**
	 * The component with the highest matching score.
	 */
	public component!	: WidgetComponentType<any>


	/**
	 * We need this to inject the controller into the chosen {@link WidgetComponent} stored in {@link #component}.
	 */
	public injector!	: Injector



	public constructor(
		private viewContainerRef	: ViewContainerRef,
		private parentInjector		: Injector,
		public	widgetsService		: WidgetsService

	){}



	/**
	 * Called when {@link #widgetControl} is set. Finds the best matching {@link WidgetComponent}
	 * for control and creates {@link #injector} providing the given control using the proper
	 * extension class of {@link WidgetControl}
	 */
	protected setControl(control: WidgetControl): void {

		this.component = this.widgetsService.getWidgetMatches(control)[0]

		assert(this.component, `RccWidgetComponent.setControl() unable to find a matching widget component for ${control.constructor.name}. Please provide a corresponding widget component.`, control)

		this.injector =	Injector.create({

							providers: 	[{
											provide: 	this.component.controlType,
											useValue: 	control
										}],

							parent:		this.parentInjector
						})

		this.viewContainerRef.createComponent(this.component, { inject: this.injector })

	}
}
