import 	{ 	NgModule                    } 		from '@angular/core'

import	{	provideTranslationMap       }		from './translations.commons'

import global_en from '../../i18n/en.json'
import global_de from '../../i18n/de.json'

/**
 * This module handles does nothing but load the default global translations map.
 */

@NgModule({
	providers: [
		provideTranslationMap(null, { en: global_en, de: global_de }),
	]
})
export class GlobalTranslationsMapModule {}
