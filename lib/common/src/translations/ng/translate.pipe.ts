import	{
			Pipe,
			PipeTransform,
			ChangeDetectorRef,
			OnDestroy
		}							from '@angular/core'

import	{
			takeUntil,
			Subject
		}							from 'rxjs'

import	{
			RccTranslationService
		}							from './translations.service'

import	{
			Translator
		}							from '../nong'


/**
 * This pipe is just the pipe version of {@link RccTranslationService.translate()}.
 * You cannot specify a language. It will always translate to {@link RccTranslationService.activeLanguage}
 */
@Pipe({
	name: 'translate',
	pure: false,
	standalone: false
})
export class RccTranslatePipe implements PipeTransform, OnDestroy {

	protected lastResult 		: string					= null
	protected lastInput			: unknown					= null
	protected lastParam			: Record<string, unknown>	= null
	protected lastTranslator	: Translator				= null
	protected lastHash			: string 					= null

	protected destroy$			: Subject<void>
								= new Subject<void>

	public constructor(
		private rccTranslationService	: RccTranslationService,
		private changeDetectorRef		: ChangeDetectorRef
	){

		// Language changes happen very rarely,
		// so we can afford to reset everything and start
		// gathering translation data from scratch:
		this.rccTranslationService.activeLanguageChange$
		.pipe( takeUntil(this.destroy$) )
		.subscribe( () => this.reset() )
	}

	/**
	 * Clears all the data stored with this instance; affects
	 * next change detection.
	 */
	private reset() : void {

		this.lastResult 	= null
		this.lastTranslator = null
		this.lastInput		= null
		this.lastParam		= null
		this.lastHash		= null

		this.changeDetectorRef.markForCheck()
	}

	/**
	 * Updates all the translation data stored with this instance.
	 */
	private update(input: unknown, param?:Record<string,unknown>, language?: string): void {

		const [text, translator] 	= 	this.rccTranslationService.getTranslationData(input, param, language)

		const hash					:	string
									= 	translator
										?	translator.hash(input, param)
										:	null	// translation failed

		this.lastTranslator 		= 	translator
		this.lastResult 			= 	text || String(input)
		this.lastInput				=	input
		this.lastHash				=	hash

	}

	public transform(input: unknown, param?:Record<string,unknown>, language?: string): string {

		// Since this pipe is NOT pure, it is probably a good idea to cache results.
		// We could do the caching in the .translate() method of RccTranslationService,
		// but then all input values would be stored there forever, whereas values stored on the
		// pipe should be deleted when the pipe gets destroyed.

		if(this.lastInput !== input) { this.update(input, param, language); return this.lastResult }

		// Translation failed last time, no reason to try again,
		// unless input changes (see above):
		if(!this.lastTranslator) 	return this.lastResult

		const hash	: string
					= this.lastTranslator.hash(input,param)

		if(this.lastHash !== hash)	{ this.update(input, param, language); return this.lastResult }

		return this.lastResult

	}

	public ngOnDestroy() : void {
		this.destroy$.next()
		this.destroy$.complete()
	}
}

