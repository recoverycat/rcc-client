
import	{ 	TestBed							}	from '@angular/core/testing'
import	{
			Observer,
			of,
		}										from 'rxjs'
import	{	RccTranslationService			}	from '../translations.service'
import	{	TranslationsModule				}	from '../translations.module'
import	{	provideTranslationMap			}	from '../translations.commons'
import	{	translationMutateComplements	}	from './translation-mutate-complements.operator'


const nonArrays 	: unknown[]
					= [undefined, 1, 'a', null, NaN, {}, Date, new Date(), true, false]

describe('translationMutateComplement operator', () => {

	let rccTranslationService		: RccTranslationService
									= undefined

	let observer					: Observer<unknown>
									= undefined

	let nextSpy						: jasmine.Spy
									= undefined


	beforeEach(() => {

		TestBed.configureTestingModule({

			imports: [
				TranslationsModule
			],
			providers : [
				provideTranslationMap('TEST', {
					xx: { A : 'xxA', B : 'xxB', C : 'xxC' },
					yy: { A : 'yyA', B : 'yyB', C : 'yyC' },
				})
			]

		})

		rccTranslationService		= 	TestBed.inject(RccTranslationService)
		observer					= 	{
											next: 		() => {/**/},
											error:		() => {/**/},
											complete:	() => {/**/}
										}


		nextSpy						= 	spyOn(observer, 'next')
	})


	it('does not emit on subscribe if source does not', () => {

		of()
		.pipe( translationMutateComplements(rccTranslationService) )
		.subscribe(observer)

		expect(nextSpy).toHaveBeenCalledTimes(0)
	})

	it('emits undefined if source emits something other than an Array', () => {

		of(...nonArrays)
		.pipe( translationMutateComplements(rccTranslationService) )
		.subscribe(observer)

		expect(nextSpy).toHaveBeenCalledTimes(1)
		expect(nextSpy).toHaveBeenCalledWith(undefined)
	})

	it('emits the appropriate TranslationComplements, if source emits an Array of translatable values', done => {

		rccTranslationService.activeLanguage = 'xx'

		of([ 'TEST.A', 'TEST.B' ])
		.pipe( translationMutateComplements(rccTranslationService) )
		.subscribe( complements => {

			expect(complements).toBeInstanceOf(Array)

			expect(complements[0]).toEqual({
				source: 		'TEST.A',
				translation: 	'xxA'
			})

			expect(complements[1]).toEqual({
				source: 		'TEST.B',
				translation: 	'xxB'
			})

			done()
		})

	})

	it('emits only once, if source emits the same translatables repeatedly.', () => {

		of([ 'TEST.A', 'TEST.B' ], [ 'TEST.A', 'TEST.B' ])
		.pipe( translationMutateComplements(rccTranslationService) )
		.subscribe(observer)

		expect(nextSpy).toHaveBeenCalledTimes(1)
	})

	it('emits only once, if source emits the same non-Array value repeatedly after emitting translatables first.', () => {

		of([ 'TEST.A', 'TEST.B' ], ...nonArrays)
		.pipe( translationMutateComplements(rccTranslationService) )
		.subscribe(observer)

		expect(nextSpy).toHaveBeenCalledTimes(1)
	})

	it('emits new TranslationComplements when the language changes', done => {

		rccTranslationService.activeLanguage = 'xx'

		let count : number = 0

		of([ 'TEST.A', 'TEST.B' ])
		.pipe( translationMutateComplements(rccTranslationService) )
		.subscribe( complements => {

			count ++

			if(count !== 2) return undefined

			expect(complements).toBeInstanceOf(Array)

			expect(complements[0]).toEqual({
				source: 		'TEST.A',
				translation: 	'yyA'
			})

			expect(complements[1]).toEqual({
				source: 		'TEST.B',
				translation: 	'yyB'
			})

			done()
		})

		rccTranslationService.activeLanguage = 'yy'

	})


})
