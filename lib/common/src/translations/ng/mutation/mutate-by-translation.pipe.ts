
import	{
			Pipe,
			PipeTransform,
			OnDestroy,
		}									from '@angular/core'

import	{
			distinctUntilChanged,
			map,
			Observable,
			Subject,
			takeUntil,
			ReplaySubject,
		}									from 'rxjs'

import	{
			RccTranslationService
		}									from '../translations.service'

import	{
			TranslationFilterObject,
			TranslationFilterFunction,
			translationMutateFilter,
			FilterParams
		}									from './translation-mutate-filter.operator'

import	{
			TranslationSortFunction,
			translationMutateSort,
			SortParams,
		}									from './translation-mutate-sort.operator'

import	{
			translationMutateComplements
		}									from './translation-mutate-complements.operator'

import	{
			translationMutateFinalize
		}									from './translation-mutate-finalize.operator'

export interface MutationOptions {

	/**
	 * Content to filter for. Can be a string to filter for
	 * or an Observable or a Formcontrol.
	 * The most common case when used in a template will
	 * probably be the FormControl. Because changes to the
	 * value of a FormControl can happen rapidly, its values
	 * will be debounced by 150ms. If that is not desired
	 * you can always hand over FormControl.valueChanges with
	 * all the pipes (or lack of) you like.
	 */
	filter?		: TranslationFilterObject | undefined,
	
	/**
	 * Function to use for filtering. If undefined,
	 * will fall back to lowercase string match
	 */
	filterFn?	: TranslationFilterFunction

	/**
	 * Function to sort with. If set to true will
	 * fall back to localized lowercase string comparison;
	 * if falsey will keep the original order.
	 */
	sort?		: TranslationSortFunction | boolean

	/**
	 * Boolean indicating whether to reverse the original order.
	 * Can be used additionally to sort, then the result will primarily
	 * be sorted and afterwards reversed.
	 */
	reverse?	: boolean
}





 /**
  * This pipe performs mutations (sort or filter) on an
  * Array of translatable elements. Filter and/or sort are
  * applied based on the translations of the elements.
  *
  * The result will again be an Array of translatable
  * values, which is important if we want to make use of
  * their properties, not just the translations.
  *
  * Requires asyncPipe to work.
  *
  * @example
  * ```
  * <input type = "text" [formControl] = "filterControl"/>
  *
  * // Filter questions whose translated wording includes
  * // whatever value filterControl holds.
  * //
  * // Also sort alphabetically
  *
  * <div *ngFor = "question of questions | mutateByTranslation : { sort: true, filter: filterControl } | async">
  *		{{ question | translate }} 							// The usual translation
  * 	Number of options: {{ question.options?.length}}	// Some other property of the translatable object
  * </div>
  * ```
  *
  */
@Pipe({
    name: 'mutateByTranslation',
    standalone: false
})
export class RccMutateByTranslationPipe<T = unknown> implements PipeTransform, OnDestroy{

	private translatable$			: ReplaySubject<T[]>
									= new ReplaySubject<T[]>(1)

	private param$					: ReplaySubject<MutationOptions>
									= new ReplaySubject<MutationOptions>(1)

	private result$					: Observable<T[]>

	private destroy$				: Subject<void>
									= new Subject<void>()


	public constructor(
		rccTranslationService	: RccTranslationService,
	) {


		const normalizedSortParam$		: 	Observable<SortParams>
										= 	this.param$
											.pipe(
												map(this.normalizeSortParams),
												distinctUntilChanged(this.compareSortParams)
											)

		const normalizedFilterParam$	: 	Observable<FilterParams>
										= 	this.param$
											.pipe(
												map(this.normalizeFilterParams),
												distinctUntilChanged(this.compareFilterParams)
											)


		this.result$					=	this.translatable$
											.pipe(
												distinctUntilChanged(this.shallowArrayMatch),
												translationMutateComplements<T>(rccTranslationService),
												/**
												 * Sort first, filter second. This might become a
												 * performance issue for very large arrays.
												 *
												 * There is
												 * a point to be made for doing it vice versa (filer first,
												 * sort second): After the translatables have been filtered,
												 * there is less to be sorted; so sorting will be more
												 * efficient.
												 *
												 * However: There's  little reason for the sorting parameters
												 * to change (e.g. language change / direction switch). Once
												 * the sorting is done, it will not happen again very often.
												 * Filtering  – on the other hand – will happen whenever the user
												 * changes the filter term, which can happen a lot (e.g. while typing).
												 *
												 * So the assumption here is that changes to sorting
												 * happen less often than changes to filtering and whatever
												 * we pick first should happen less often, as when the first thing
												 * is triggered the second one wil get a new set of translatables and
												 * with it gets triggered as well anyway.
												 */
												translationMutateSort(normalizedSortParam$),
												translationMutateFilter(normalizedFilterParam$),
												translationMutateFinalize(),
												takeUntil(this.destroy$)
											)
	}


	public transform (

		translatables	: T[],
		params?			: MutationOptions,

	) : Observable<T[]> {


		/**
		 * Every time the pipe transform is called,
		 * the new values trigger emissions on the following
		 * Observables, and with it eventually also on this.result$
		 */
		this.param$.next(params)
		this.translatable$.next(translatables)

		/**
		 * The return value is always the same
		 * – the result Observable –
		 * this way the pipe stays pure,
		 * but requires asyncPipe to work.
		 */
		return this.result$

	}

	/**
	 * This method extracts the appropriate
	 * parameters for the filtering operator from the more general
	 * {@link MutationOptions}
	 */
	private normalizeFilterParams(

		this	: void,
		params	: MutationOptions

	) : FilterParams | null {

		if(!params)				return null

		const disableFilter		: boolean
								= !('filter' in params) && !('filterFn' in params)

		if(disableFilter)		return null

		const filterObj			: 	TranslationFilterObject
								= 	params.filter

		const filterFn			: 	TranslationFilterFunction
								= 	params.filterFn


		return { filterFn, filterObj }
	}

	/**
	 * Tells if two sets of filtering parameters are practically equal.
	 */
	private compareFilterParams(

		this		: void,
		nextParams	: FilterParams | null,
		prevParams	: FilterParams | null

	) : boolean {

		if( !nextParams && ! prevParams) 	return true
		if( !nextParams )					return false
		if( !prevParams )					return false

		const equalFilterFn 	: boolean
								= nextParams.filterFn 	=== prevParams.filterFn

		const equalFilterObj	: boolean
								= nextParams.filterObj 	=== prevParams.filterObj

		return equalFilterFn && equalFilterObj
	}

	/**
	 * The parameters of the .transform() method work a bit differently
	 * from the parameters for the sorting operator above.
	 * (mostly for readability). This method extracts the appropriate
	 * parameters for the sorting operator from the more general
	 * {@link MutationOptions}
	 */
	private normalizeSortParams(

		this	: void,
		params?	: MutationOptions

	) : SortParams | null {

		let sortFn				: TranslationSortFunction
								= null // do not sort at all

		if(params?.sort 		=== true)		sortFn = undefined 		// use default sorting
		if(typeof params?.sort 	=== 'function')	sortFn = params.sort 	// use custom sort function

		const reverse 			: boolean
								= !!params?.reverse

		return { sortFn, reverse }

	}

	/**
	 * Tells if two sets of sorting parameters are practically equal.
	 */
	private compareSortParams(

		this		: void,
		nextParams	: SortParams | null,
		prevParams	: SortParams | null

	) : boolean {

		if( !nextParams && ! prevParams) 	return true
		if( !nextParams )					return false
		if( !prevParams )					return false

		const equalSortFn		: boolean
								= nextParams.sortFn		=== prevParams.sortFn

		const equalDirection	: boolean
								= nextParams.reverse 	=== prevParams.reverse

		return equalSortFn && equalDirection
	}


	private shallowArrayMatch(this:void, array_1: T[], array_2: T[]) : boolean {

		if(!array_1 && !array_2)				return true
		if(!array_1)							return false
		if(!array_2)							return false

		if(array_1.length !== array_2.length) 	return false

		return array_1.every( (item, index) => array_2[index] === item )

	}


	public ngOnDestroy() : void {

		this.destroy$.next()
		this.destroy$.complete()

	}
}
