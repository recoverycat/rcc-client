import	{
			ChangeDetectorRef,
			Component
		}										from '@angular/core'

import	{
			FormControl
		}										from '@angular/forms'

import	{
			ComponentFixture,
			fakeAsync,
			TestBed,
			tick,
		}										from '@angular/core/testing'
import	{
			Observer,
			Subject
		}										from 'rxjs'
import	{
			RccTranslationService
		}										from '../translations.service'
import	{	TranslationsModule				}	from '../translations.module'
import	{	provideTranslationMap			}	from '../translations.commons'
import	{
			RccMutateByTranslationPipe,
		}										from './mutate-by-translation.pipe'

import	{
			TranslationFilterFunction,
			TranslationFilterObject
		}										from './translation-mutate-filter.operator'

import  {
			TranslationSortFunction,
		}										from './translation-mutate-sort.operator'

@Component({
    template: '{{["TEST.C", "TEST.A", "TEST.B", "TEST.A"] | mutateByTranslation : {} | async }}',
    standalone: false
})
class NoParamsTestComponent {}



@Component({
    template: '{{["TEST.C", "TEST.A", "TEST.B", "TEST.A"] | mutateByTranslation : { filter, filterFn } | async }}',
    standalone: false
})
class OnlyFilterTestComponent {
	public filter		: TranslationFilterObject 			= undefined
	public filterFn		: TranslationFilterFunction			= undefined
}

@Component({
    template: '{{["TEST.C", "TEST.A", "TEST.B", "TEST.A"] | mutateByTranslation : { sort, reverse } | async }}',
    standalone: false
})
class OnlySortTestComponent {
	public sort			: TranslationSortFunction | boolean	= undefined
	public reverse		: boolean							= undefined
}

@Component({
    template: '{{["TEST.C", "TEST.A", "TEST.B", "TEST.A"] | mutateByTranslation : { sort, reverse, filter, filterFn } | async }}',
    standalone: false
})
class CombinedTestComponent {
	public filter		: TranslationFilterObject 			= undefined
	public filterFn		: TranslationFilterFunction			= undefined
	public sort			: TranslationSortFunction | boolean	= undefined
	public reverse		: boolean							= undefined
}




describe('RccMutateByTranslationPipe', () => {

	let observer					: Observer<unknown>
									= undefined

	// let changeDetectorRef			: ChangeDetectorRef
	// 								= undefined

	let rccTranslationService		: RccTranslationService
									= undefined

	let rccMutateByTranslationPipe	: RccMutateByTranslationPipe
									= undefined

	beforeEach( () => {


		TestBed
		.configureTestingModule({

			imports: [
				TranslationsModule
			],
			declarations:[
				RccMutateByTranslationPipe,
				NoParamsTestComponent,
				OnlyFilterTestComponent,
				OnlySortTestComponent,
				CombinedTestComponent
			],
			providers : [
				ChangeDetectorRef,
				provideTranslationMap('TEST', {
					xx: { A : 'xxA', B : 'xxB', C : 'xxC' },
					yy: { A : 'yyA', B : 'yyB', C : 'yyC' },
				})
			]

		})


		observer					= 	{
											next: 		() => {/**/},
											error:		() => {/**/},
											complete:	() => {/**/}
										}



		rccTranslationService		= TestBed.inject(RccTranslationService)
		rccMutateByTranslationPipe	= new RccMutateByTranslationPipe(rccTranslationService)

		rccTranslationService.activeLanguage = 'xx'

		spyOnAllFunctions(observer)


	})





	describe(' in template', () => {


		describe('without parameters', () => {


			it('leaves translatables as they are, when no parameters are passed', () => {

				const fixture 	: ComponentFixture<NoParamsTestComponent>
								= TestBed.createComponent(NoParamsTestComponent)

				fixture.detectChanges()

				const element	: HTMLElement
								= fixture.nativeElement as HTMLElement


				expect(element.textContent.trim()).toBe('TEST.C,TEST.A,TEST.B,TEST.A')
			})
		})

		describe('with filtering parameters only', () => {


			it('filters translatables by a provided string by matching lowercase substrings', () => {

				const fixture 	: ComponentFixture<OnlyFilterTestComponent>
								= TestBed.createComponent(OnlyFilterTestComponent)

				const component	: OnlyFilterTestComponent
								= fixture.componentInstance

				component.filter = 'xa'
				fixture.detectChanges()

				const element	: HTMLElement
								= fixture.nativeElement as HTMLElement


				expect(element.textContent.trim()).toBe('TEST.A,TEST.A')

				rccTranslationService.activeLanguage = 'yy'
				fixture.detectChanges()

				expect(element.textContent.trim()).toBe('')

				component.filter = 'yb'
				fixture.detectChanges()

				expect(element.textContent.trim()).toBe('TEST.B')

			})

			it('filters translatables by the values of a string Observable by matching lowercase substrings', () => {

				const fixture 	: ComponentFixture<OnlyFilterTestComponent>
								= TestBed.createComponent(OnlyFilterTestComponent)

				const component	: OnlyFilterTestComponent
								= fixture.componentInstance

				const filter$	: Subject<string>
								= new Subject<string>()

				component.filter = filter$
				fixture.detectChanges()

				const element	: HTMLElement
								= fixture.nativeElement as HTMLElement


				expect(element.textContent.trim()).toBe('')

				filter$.next('xa')
				fixture.detectChanges()

				expect(element.textContent.trim()).toBe('TEST.A,TEST.A')

				rccTranslationService.activeLanguage = 'yy'
				fixture.detectChanges()

				expect(element.textContent.trim()).toBe('')

				filter$.next('yb')
				fixture.detectChanges()

				expect(element.textContent.trim()).toBe('TEST.B')

			})


			it('filters translatables by the 150ms debounced values of a FormControl by matching lowercase substrings', fakeAsync( () => {

				const fixture 		: ComponentFixture<OnlyFilterTestComponent>
									= TestBed.createComponent(OnlyFilterTestComponent)

				const component		: OnlyFilterTestComponent
									= fixture.componentInstance

				const filterControl	: FormControl<string>
									= new FormControl<string>('')


				component.filter = filterControl
				fixture.detectChanges()

				const element	: HTMLElement
								= fixture.nativeElement as HTMLElement

				expect(element.textContent.trim()).toBe('')

				tick(150)
				fixture.detectChanges()

				expect(element.textContent.trim()).toBe('TEST.C,TEST.A,TEST.B,TEST.A')

				filterControl.setValue('xa')
				tick(150)
				fixture.detectChanges()

				expect(element.textContent.trim()).toBe('TEST.A,TEST.A')

				rccTranslationService.activeLanguage = 'yy'
				tick(150)

				fixture.detectChanges()

				expect(element.textContent.trim()).toBe('')

				filterControl.setValue('yb')
				tick(150)
				fixture.detectChanges()

				expect(element.textContent.trim()).toBe('TEST.B')

			}))

			it('filters translatables by a provided custom filtering function without extra filtering string', () => {

				const fixture 	: ComponentFixture<OnlyFilterTestComponent>
								= TestBed.createComponent(OnlyFilterTestComponent)

				const component	: OnlyFilterTestComponent
								= fixture.componentInstance

				const element	: HTMLElement
								= fixture.nativeElement as HTMLElement

				component.filterFn = (translation) => ['xxB', 'yyC'].includes(translation)

				rccTranslationService.activeLanguage = 'xx'
				fixture.detectChanges()

				expect(element.textContent.trim()).toBe('TEST.B')

				rccTranslationService.activeLanguage = 'yy'
				fixture.detectChanges()

				expect(element.textContent.trim()).toBe('TEST.C')

				rccTranslationService.activeLanguage = 'zz'
				fixture.detectChanges()

				expect(element.textContent.trim()).toBe('')

				rccTranslationService.activeLanguage = 'xx'
				fixture.detectChanges()

				expect(element.textContent.trim()).toBe('TEST.B')


			})

			it('filters translatables by a provided custom filtering function with extra filtering string', () => {

				const fixture 	: ComponentFixture<OnlyFilterTestComponent>
								= TestBed.createComponent(OnlyFilterTestComponent)

				const component	: OnlyFilterTestComponent
								= fixture.componentInstance

				const element	: HTMLElement
								= fixture.nativeElement as HTMLElement

				component.filterFn 	= (translation, filterStr) => !translation?.includes(filterStr)
				component.filter	= 'xxA'

				rccTranslationService.activeLanguage = 'xx'
				fixture.detectChanges()

				expect(element.textContent.trim()).toBe('TEST.C,TEST.B')

				rccTranslationService.activeLanguage = 'yy'
				fixture.detectChanges()

				expect(element.textContent.trim()).toBe('TEST.C,TEST.A,TEST.B,TEST.A')

				component.filter	= 'yyB'
				fixture.detectChanges()

				expect(element.textContent.trim()).toBe('TEST.C,TEST.A,TEST.A')

				component.filter	= 'y'
				fixture.detectChanges()

				expect(element.textContent.trim()).toBe('')


			})

		})

		describe('with sorting parameters only', () => {

			it('reverses the original order if only .reverse is set', () => {

				const fixture 	: ComponentFixture<OnlySortTestComponent>
								= TestBed.createComponent(OnlySortTestComponent)

				const component	: OnlySortTestComponent
								= fixture.componentInstance

				const element	: HTMLElement
								= fixture.nativeElement as HTMLElement

				component.reverse 	= true
				fixture.detectChanges()

				expect(element.textContent.trim()).toBe('TEST.A,TEST.B,TEST.A,TEST.C')

				component.reverse 	= false
				fixture.detectChanges()

				expect(element.textContent.trim()).toBe('TEST.C,TEST.A,TEST.B,TEST.A')
			})

			it('sorts alphabetically when .sort is set to true', () => {

				const fixture 	: ComponentFixture<OnlySortTestComponent>
								= TestBed.createComponent(OnlySortTestComponent)

				const component	: OnlySortTestComponent
								= fixture.componentInstance

				const element	: HTMLElement
								= fixture.nativeElement as HTMLElement

				component.sort		= true
				fixture.detectChanges()

				expect(element.textContent.trim()).toBe('TEST.A,TEST.A,TEST.B,TEST.C')

				component.reverse 	= true
				fixture.detectChanges()

				expect(element.textContent.trim()).toBe('TEST.C,TEST.B,TEST.A,TEST.A')
			})

			it('sorts by a custom function if supplied', () => {

				const fixture 	: ComponentFixture<OnlySortTestComponent>
								= TestBed.createComponent(OnlySortTestComponent)

				const component	: OnlySortTestComponent
								= fixture.componentInstance

				const element	: HTMLElement
								= fixture.nativeElement as HTMLElement

				const order		: string[]
								=  ['xxC','xxA','xxB']


				component.sort		= 	(t1,t2) => Math.sign( order.indexOf(t1) - order.indexOf(t2) ) as 1 | 0 | -1
				fixture.detectChanges()

				expect(element.textContent.trim()).toBe('TEST.C,TEST.A,TEST.A,TEST.B')

				component.reverse	= true
				fixture.detectChanges()

				expect(element.textContent.trim()).toBe('TEST.B,TEST.A,TEST.A,TEST.C')

				component.sort		= true
				fixture.detectChanges()

				expect(element.textContent.trim()).toBe('TEST.C,TEST.B,TEST.A,TEST.A')

			})

		})

		describe('with all parameters combined', () => {

			it('sorts and filters at the same time', () => {

				const fixture 	: ComponentFixture<CombinedTestComponent>
								= TestBed.createComponent(CombinedTestComponent)

				const component	: CombinedTestComponent
								= fixture.componentInstance

				const element	: HTMLElement
								= fixture.nativeElement as HTMLElement


				fixture.detectChanges()

				expect(element.textContent.trim()).toBe('TEST.C,TEST.A,TEST.B,TEST.A')

				fixture.detectChanges()

				component.sort 		= true
				component.reverse	= false
				component.filter	= 'xA'

				const exlusionFn	: TranslationFilterFunction
									= (translation, filter) => !translation.includes(filter)

				component.filterFn	= exlusionFn
				fixture.detectChanges()

				expect(element.textContent.trim()).toBe('TEST.B,TEST.C')

				component.filter	= 'xB'
				component.reverse	= true

				fixture.detectChanges()

				expect(element.textContent.trim()).toBe('TEST.C,TEST.A,TEST.A')

				component.reverse	= false
				fixture.detectChanges()

				expect(element.textContent.trim()).toBe('TEST.A,TEST.A,TEST.C')

				component.sort		= false
				fixture.detectChanges()

				expect(element.textContent.trim()).toBe('TEST.C,TEST.A,TEST.A')

				component.filterFn	= undefined
				fixture.detectChanges()

				expect(element.textContent.trim()).toBe('TEST.B')

				component.filterFn	= exlusionFn
				component.filter	= 'yA'
				fixture.detectChanges()

				expect(element.textContent.trim()).toBe('TEST.C,TEST.A,TEST.B,TEST.A')

				rccTranslationService.activeLanguage = 'yy'
				fixture.detectChanges()

				expect(element.textContent.trim()).toBe('TEST.C,TEST.B')


			})

		})

	})


	/**
	 * Some aspects are not testable on the template. Those
	 * aspects are tested directly on the .transform() method:
	 */
	describe('.transform()', () => {

		describe('without parameters beyond the translatables', () => {

			it('emit only when translatables or langugage changes', () => {


				rccMutateByTranslationPipe
				.transform(undefined)
				.subscribe(observer)

				rccMutateByTranslationPipe.transform(['TEST.A'])

				expect(observer.next).toHaveBeenCalledTimes(2)

				rccMutateByTranslationPipe.transform(['TEST.B'])

				expect(observer.next).toHaveBeenCalledTimes(3)

				rccMutateByTranslationPipe.transform(['TEST.B'])

				expect(observer.next).toHaveBeenCalledTimes(3)

				rccMutateByTranslationPipe.transform(['TEST.C'])

				expect(observer.next).toHaveBeenCalledTimes(4)

				rccMutateByTranslationPipe.transform([])

				expect(observer.next).toHaveBeenCalledTimes(5)

				rccMutateByTranslationPipe.transform(undefined)

				expect(observer.next).toHaveBeenCalledTimes(6)

				rccMutateByTranslationPipe.transform(null)

				expect(observer.next).toHaveBeenCalledTimes(6)

				rccTranslationService.activeLanguage = 'yy'

				expect(observer.next).toHaveBeenCalledTimes(7)

				rccMutateByTranslationPipe.transform(undefined)

				expect(observer.next).toHaveBeenCalledTimes(7)

			})

			it('preserves original translatables', done => {

				const originalTranslatables	: unknown[]
											= [{}, Date, new Date(), 'TEST.A']

				rccMutateByTranslationPipe
				.transform(originalTranslatables)
				.subscribe( translatables => {
					expect(translatables).toEqual(originalTranslatables)
					done()
				})

			})

		})


		describe('with sorting parameters', () => {

			it('preserves original translatables when sorting', done => {

				const originalTranslatables	: unknown[]
											= [{}, Date, new Date(), 'TEST.A']

				rccMutateByTranslationPipe
				.transform(originalTranslatables, { sort: true })
				.subscribe( translatables => {
					expect(translatables.every(t => originalTranslatables.includes(t) )).toBeTrue()
					done()
				})

			})
		})

		describe('with filtering parameters', () => {

			it('preserves original translatables when filtering', done => {

				const originalTranslatables	: unknown[]
											= [{}, Date, new Date(), 'TEST.A']

				rccMutateByTranslationPipe
				.transform(originalTranslatables, { sort: true })
				.subscribe( translatables => {
					expect(translatables.every(t => originalTranslatables.includes(t) )).toBeTrue()
					done()
				})

			})
		})

	})

})
