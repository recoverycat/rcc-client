
/**
 * Stores a translatable value alongside its translation
 * for further processing. This is useful, if you want to
 * process the original value – possibly in multiple steps
 * without having to re-translate it on every step.
 */
export interface TranslationComplement<T=unknown> {
	source		: T,
	translation	: string
}
