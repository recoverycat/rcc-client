
import	{
			Observer,
			of
		}											from 'rxjs'
import	{	translationMutateFinalize			}	from './translation-mutate-finalize.operator'
import	{	TranslationComplement				}	from './translation-mutate.commons'

const translationComplementA	: 	TranslationComplement
								= 	{
										source:			'TEST.A',
										translation:	'xxA'
									}

const translationComplementB	: 	TranslationComplement
								= 	{
										source:			'TEST.B',
										translation:	'xxB'
									}

const translationComplementC	: 	TranslationComplement
								= 	{
										source:			'TEST.C',
										translation:	'xxC'
									}



describe('translationMutateFinalize operator', () => {


	let observer					: Observer<unknown>
									= undefined

	let nextSpy						: jasmine.Spy
									= undefined


	beforeEach(() => {

		observer					= 	{
											next: 		() => {/**/},
											error:		() => {/**/},
											complete:	() => {/**/}
										}

		nextSpy						= 	spyOn(observer, 'next')
	})

	it('does not emit on subscribe if source does not', () => {

		of()
		.pipe( translationMutateFinalize() )
		.subscribe(observer)

		expect(nextSpy).toHaveBeenCalledTimes(0)


	})

	
	it('emits undefined if source emits undefined', () => {

		of(undefined)
		.pipe( translationMutateFinalize() )
		.subscribe(observer)

		expect(nextSpy).toHaveBeenCalledTimes(1)
		expect(nextSpy).toHaveBeenCalledWith(undefined)

	})

	it('emits [] if source emits []', () => {

		of([])
		.pipe( translationMutateFinalize() )
		.subscribe(observer)

		expect(nextSpy).toHaveBeenCalledTimes(1)
		expect(nextSpy).toHaveBeenCalledWith([])

	})


	it('emits original values, when the source emits TranslationComplements', done => {

		of([translationComplementA, translationComplementB, translationComplementC])
		.pipe( translationMutateFinalize() )
		.subscribe( sources => {
			expect(sources).toEqual([ 'TEST.A','TEST.B','TEST.C' ])
			done()
		})

	})

})
