import { Translator } from './translator.class'


// Generic test for newly created extensions of Translator class to see whether it was extended correctly.

export function genericTranslatorClassTests(translatorClass: typeof Translator): void {

    describe('TranslatorClass', () => {

        it('should extend Translator.', () => {
            expect(translatorClass.prototype).toBeInstanceOf(Translator)
        })

    })

}
