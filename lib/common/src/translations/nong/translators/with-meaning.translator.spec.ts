import { WithMeaningTranslator } from './with-meaning.translator'
import { genericTranslatorClassTests } from '../translator.class.spec'


describe('WithMeaningTranslator', () => {

    let withMeaningTranslator = new WithMeaningTranslator

    beforeEach(() => {
        withMeaningTranslator = new WithMeaningTranslator()
    })

    genericTranslatorClassTests(WithMeaningTranslator)

    it('WithMeaningTranslator.match() should return -1, if called for anything lacking the meaning property of type string.', () => {



        expect(withMeaningTranslator.match({ meaning: 'blub' })             ).toBe(1)
        expect(withMeaningTranslator.match({ meaning: 'abcdefghijkl mnop' })).toBe(1)

        expect(withMeaningTranslator.match({ meaning: '' })                 ).toBe(0)

        expect(withMeaningTranslator.match({ meaning: 3 })                  ).toBe(-1)
        expect(withMeaningTranslator.match({ meaning: null })               ).toBe(-1)
        expect(withMeaningTranslator.match({ meaning: {} })                 ).toBe(-1)
        expect(withMeaningTranslator.match(undefined)                       ).toBe(-1)
        expect(withMeaningTranslator.match(5)                               ).toBe(-1)
        expect(withMeaningTranslator.match(9999)                            ).toBe(-1)
        expect(withMeaningTranslator.match(0)                               ).toBe(-1)
        expect(withMeaningTranslator.match(-10)                             ).toBe(-1)
        expect(withMeaningTranslator.match(NaN)                             ).toBe(-1)
        expect(withMeaningTranslator.match(null)                            ).toBe(-1)
        expect(withMeaningTranslator.match({ translations: {} })            ).toBe(-1)
        expect(withMeaningTranslator.match(new Date())                      ).toBe(-1)
        expect(withMeaningTranslator.match(true)                            ).toBe(-1)
        expect(withMeaningTranslator.match(false)                           ).toBe(-1)
        expect(withMeaningTranslator.match('abc')                           ).toBe(-1)
        expect(withMeaningTranslator.match({})                              ).toBe(-1)
        expect(withMeaningTranslator.match([])                              ).toBe(-1)


    })


    it('WithMeaningTranslator.translate() should return null, if tried on anything lacking the meaning property of type string.', () => {

        expect(withMeaningTranslator.translate({ meaning: 99 }, 'xx')  ).toBeNull()
        expect(withMeaningTranslator.translate({ meaning: null }, 'xx')).toBeNull()
        expect(withMeaningTranslator.translate({ meaning: {} }, 'xx')  ).toBeNull()
        expect(withMeaningTranslator.translate(true, 'xx')             ).toBeNull()
        expect(withMeaningTranslator.translate('5', 'xx')              ).toBeNull()
        expect(withMeaningTranslator.translate('abc', 'xx')            ).toBeNull()
        expect(withMeaningTranslator.translate({}, 'xx')               ).toBeNull()
        expect(withMeaningTranslator.translate([], 'xx')               ).toBeNull()
        expect(withMeaningTranslator.translate(new Date, 'xx')         ).toBeNull()
        expect(withMeaningTranslator.translate(null, 'xx')             ).toBeNull()
        expect(withMeaningTranslator.translate(NaN, 'xx')              ).toBeNull()

    })

    it('WithMeaningTranslator.translate() should return a final string value that equals the value of the present meaning property in square brackets.', () => {

        expect(withMeaningTranslator.translate({ meaning: 'abc' }, 'en').final).toBe('[abc]')
        expect(withMeaningTranslator.translate({ meaning: 'abc' }, 'de').final).toBe('[abc]')
        expect(withMeaningTranslator.translate({ meaning: 'abc' }, 'xx').final).toBe('[abc]')

    })
})
