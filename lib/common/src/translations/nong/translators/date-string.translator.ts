import	{	CalendarDateString	} from '@rcc/core'
import	{ 	Translator 		} from '../translator.class'
import	{ 	TranslationResult	} from '../interfaces'

/**
 * Converts a date string YYYY-MM-DD into a local text representation; uses toLocaleString.
 **/
export class DateStringTranslator extends Translator {

	compatibleLanguages : null 	= null // works with all languages


	match(x: unknown): number{
		return 	CalendarDateString.isValid(x)
				?	 2
				:	-1
	}

	/**
	 * Converts a date string YYYY-MM-DD into a local text representation; uses toLocaleString.
	 */
	translate(
		value		: string,
		language?	: string,
		/**
		 * Will be used as options parameter to Date.toLocaleString()
		 */
		param?		: unknown
	): TranslationResult {

		if(this.match(value) === -1) return null

		const date = new Date(value)

		return { final: date.toLocaleString(language || undefined,  param || { dateStyle: 'medium' } ) }

	}
}
