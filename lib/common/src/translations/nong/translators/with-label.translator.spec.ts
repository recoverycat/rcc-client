import { TranslationResult } from '../interfaces'
import { genericTranslatorClassTests } from '../translator.class.spec'
import { WithLabelTranslator } from './with-label.translator'

describe('WithLabelTranslator', () => {
	const translator: WithLabelTranslator = new WithLabelTranslator()

	genericTranslatorClassTests(WithLabelTranslator)

	describe('WithLabelTranslator.match()', () => {
		it('should return -1 if no label present', () => {
			expect(translator.match({})).toBe(-1)
			expect(translator.match(null as unknown)).toBe(-1)
		})

		it('should return 1 if label present', () => {
			expect(translator.match({ label: 'foo' })).toBe(1)
		})

		it('should return 0 if label empty string', () => {
			expect(translator.match({ label: '' })).toBe(0)
		})
	})

	describe('WithLabelTranslator.translate()', () => {
		it('should return label value', () => {
			const result: TranslationResult = translator.translate({ label: 'foo' })

			expect(result?.intermediate).toBe('foo')
		})
	})
})
