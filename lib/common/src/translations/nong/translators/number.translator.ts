import	{	Translator 			} from '../translator.class'
import	{ 	TranslationResult	} from '../interfaces'

/**
 * This translator does almost nothing.
 * It's only task is to ensure that numbers pass the translation, so the translation won't end up as null.
 * The translation though will be a string representation of the input, not a number.
 **/
export class NumberTranslator extends Translator {

	public compatibleLanguages	: null 		= 	null  // works with all languages

	public defaultOptions		: Record<string, unknown>	=	{
											maximumSignificantDigits:	21
										}

	public match(x: unknown): number{
		return	typeof x === 'number'
				?	0
				:	-1
	}

	public translate(input: unknown, language?: string, param?: Record<string, unknown>): TranslationResult {

		if(this.match(input) === -1)	return null
		if(typeof input!== 'number')	return null
		if(isNaN(input))				return { intermediate: 'NaN' }

		const options = Object.assign({}, this.defaultOptions, param)

		return 	input.toLocaleString
				?	{ final: input.toLocaleString(language, options) }
				:	{ final: String(input) }

	}
}
