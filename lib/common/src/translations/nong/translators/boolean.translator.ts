import	{	Translator 			}	from '../translator.class'
import	{	TranslationResult	}	from '../interfaces'


/**
 * This basic translators turn boolean values into the respective translation of 'yes' or 'no'.
 * This is more of a fallback, since in many cases, you'd rather give more specific translations
 * depending on the circumstance, like 'active''inactive' or 'on''off'.
 * In those cases better manually turn the boolean into a translation string and then translate:
 * ```
 * 	<span>{{ input ? 'SOME_SCOPE.SETTING.ACTIVE' : 'SOME_SCOPE.SETTING_INACTIVE' | translate}}</span>
 * ```
 **/
export class BooleanTranslator extends Translator {

	compatibleLanguages : null 	= null // works with all languages


	match(input: any): number{

		return	typeof input === 'boolean'
				?	0
				:	-1
	}

	translate(value:any): TranslationResult{
		if(this.match(value) === -1) return  null

		return 	value
				?	{ intermediate: 'TRUE'	}
				:	{ intermediate: 'FALSE'	}
	}
}
