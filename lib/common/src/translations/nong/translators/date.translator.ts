import	{ 	Translator 			} from '../translator.class'
import	{ 	TranslationResult	} from '../interfaces'

/**
 * Outputs a Date object as a human readable string; makes use of toLocaleString().
 **/
export class DateTranslator extends Translator {

	compatibleLanguages : null 	= null // works with all languages


	match(x: unknown): number {
		return 	x && x instanceof Date
				?	2
				:	-1
	}

	translate(value: Date, language: string, param?: unknown): TranslationResult {

		if(this.match(value) === -1) return null

		return { final: value.toLocaleString(language || undefined,  param || { dateStyle: 'full', timeStyle: 'short' } ) }
		// should lang be the locale str? if lang === 'en' the American format will be used
		// instead of the browsers local settings. Might be annoying to British users
	}
}
