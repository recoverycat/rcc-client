import { NumberTranslator } from './number.translator'
import { genericTranslatorClassTests } from '../translator.class.spec'



describe('NumberTranslator', () => {

	let numberTranslator = new NumberTranslator

	beforeEach(() => {
		numberTranslator = new NumberTranslator()
	})
	genericTranslatorClassTests(NumberTranslator)

	it('NumberTranslator.match() should return -1 if called for a non-number object value.', () => {

		console.log('Running assertions for NumberTranslator.match()')

		expect(numberTranslator.match(5)                         ).toBe(0)
		expect(numberTranslator.match(9999)                      ).toBe(0)
		expect(numberTranslator.match(0)                         ).toBe(0)
		expect(numberTranslator.match(-10)                       ).toBe(0)
		expect(numberTranslator.match(NaN)                       ).toBe(0)

		expect(numberTranslator.match({ translations: {} })      ).toBe(-1)
		expect(numberTranslator.match(new Date())                ).toBe(-1)
		expect(numberTranslator.match(true)                      ).toBe(-1)
		expect(numberTranslator.match(false)                     ).toBe(-1)
		expect(numberTranslator.match('abc')                     ).toBe(-1)
		expect(numberTranslator.match({})                        ).toBe(-1)
		expect(numberTranslator.match([])                        ).toBe(-1)

	})


	it('NumberTranslator.translate() should return null if tried on a non-number value.', () => {

		console.log('Running 1st assertions for NumberTranslator.translate()')

		expect(numberTranslator.translate(true, 'en')            ).toBeNull()
		expect(numberTranslator.translate('5', 'en')             ).toBeNull()
		expect(numberTranslator.translate('abc', 'en')           ).toBeNull()
		expect(numberTranslator.translate({}, 'en')              ).toBeNull()
		expect(numberTranslator.translate([], 'en')              ).toBeNull()
		expect(numberTranslator.translate(new Date, 'en')        ).toBeNull()
		expect(numberTranslator.translate(null, 'en')            ).toBeNull()

	})

	it('NumberTranslator.translate() should return an intermediate string value for NaN.', () => {

		console.log('Running assertions for NumberTranslator.match()')

		expect(numberTranslator.translate(NaN, 'en').intermediate).toBe('NaN')

	})


	it('NumberTranslator.translate() should return final string value with a local number representation.', () => {

		console.log('Running 2nd assertions for NumberTranslator.translate()')

		expect(numberTranslator.translate(5, 'en').final         ).toBe('5')
		expect(numberTranslator.translate(99, 'en').final        ).toBe('99')
		expect(numberTranslator.translate(-10, 'en').final       ).toBe('-10')

		expect(numberTranslator.translate(3500, 'en').final      ).toBe('3,500')
		expect(numberTranslator.translate(3500, 'de').final      ).toBe('3.500')

		expect(numberTranslator.translate(3.14159, 'en').final   ).toBe('3.14159')
		expect(numberTranslator.translate(3.14159, 'de').final   ).toBe('3,14159')

		expect(numberTranslator.translate(1000.1, 'en').final    ).toBe('1,000.1')
		expect(numberTranslator.translate(1000.1, 'de').final    ).toBe('1.000,1')

	})
})
