import	{ 	Translator 			} from '../translator.class'
import	{ 	TranslationResult	} from '../interfaces'

/**
 * Converts a time string HH:MM or HH:MM:SS into a local text representation; uses .toLocaleTimeString.
 **/
export class TimeStringTranslator extends Translator {

	public compatibleLanguages : null 	= null // works with all languages

	public match(x: unknown): number{

		if(typeof x !== 'string') return -1

		const matches = x.match(/^\d\d:\d\d(:\d\d)?$/)

		return	matches
				?	 2
				:	-1
	}

	/**
	 * Converts a date string YYYY-MM-DD into a local text representation; uses .toLocaleTimeString.
	 */
	public translate(
		value		: string,
		language?	: string,
		/**
		 * Will be used as options parameter to Date.toLocaleString()
		 */
		param?		: unknown
	): TranslationResult {

		if(this.match(value) === -1) return null

		const date 		= new Date()
		const matches 	= value.match(/^(\d\d):(\d\d)(:(\d\d))?$/)
		const hours		= parseInt(matches[1], 10)
		const minutes	= parseInt(matches[2], 10)
		const seconds	= parseInt(matches[4] || '00', 10)

		date.setHours(hours, minutes, seconds)

		return { final: date.toLocaleTimeString(language || undefined,  param || { timeStyle: 'short' } ) }

	}
}
