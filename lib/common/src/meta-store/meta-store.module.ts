import 	{
			NgModule,
			ModuleWithProviders,
		} 									from '@angular/core'

import	{
			Item,
			ItemStore
		}									from '@rcc/core'

import	{	SharedModule 				}	from '../shared-module'
import	{	provideTranslationMap		}	from '../translations'


import	{
			MetaStoreConfig,
			META_STORE_CONFIGS,
		}									from './meta-store.class'

import	{	MetaStoreService			}	from './meta-store.service'
import	{	MetaStoreModalComponent		}	from './modal/meta-store.modal'
import	{	MetaStoreComponent			}	from './content/meta-store.component'
import	{
			HeaderComponent,
			PopoverComponent
		}									from './header/header.component'


import en from './i18n/en.json'
import de from './i18n/de.json'


@NgModule({
	declarations: [
		MetaStoreComponent,
		MetaStoreModalComponent,
		HeaderComponent,
		PopoverComponent
	],

	imports: [
		SharedModule,
	],

	exports: [
		MetaStoreComponent,
		MetaStoreModalComponent,
		HeaderComponent
	],
	providers:[
		provideTranslationMap('META_STORE', { en, de }),
		MetaStoreService
	]
})
export class MetaStoreModule {

	public static 	forChild(config: MetaStoreConfig<unknown,Item<unknown>,ItemStore<Item<unknown>>>): ModuleWithProviders<MetaStoreModule> {

		return 	{
					ngModule:	MetaStoreModule,
					providers:	[
									{ provide: META_STORE_CONFIGS,	useValue: config, 	multi:true }
								]
				}
	}
}
