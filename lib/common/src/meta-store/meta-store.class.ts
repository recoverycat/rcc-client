import	{	Item,
			ItemConfig,
			ItemStore,
		}							from '@rcc/core'

import	{
			Observable,
			merge
		}							from 'rxjs'

import	{
			share
		}							from 'rxjs/operators'
import 	{
			ActionWithHandler,
			ActionWithHandlerFactory,
			ActionWithPath,
		} 							from './meta-store.commons'
import 	{
			InjectionToken,
			Type
		} 							from '@angular/core'

/**
 * I think this is just a workaround, because we already have an Action
 * interface elsewhere. TODO: We should refactor this and use the same Actions
 * here as we do in other parts of the app (@rcc/common/actions)
 */
type ActionX<H extends (...args: unknown[]) => unknown> = ActionWithPath | ActionWithHandler<H> | ActionWithHandlerFactory<H>

/**
 * Meta Actions are Actions that closely relate to {@link ItemAction}s.
 * But where an ItemAction is something you can do with a single given item.
 * Meta Actions represent things you can do with no item at hand (e.g. create a
 * new item) or with multiple/all items; e.g. creating a
 * report from all the Entries(extends Item) available to the app, regardless
 * which ItemStore handles them.
 *
 * TODO: This should be properly aligned with {@link ItemAction}s and
 * {@link Action}s
 */
export type MetaAction<
		C extends ItemConfig,
		I extends Item<C>,
		S extends ItemStore<I>
	>	= 	ActionX< (metaStore?: MetaStore<C,I,S>) => unknown >


/**
 * Unlike the name suggests, this is not configuration data for a MetaStore.
 * Instead this is the interface used for a InjectionToken collecting all the
 * MetaStores for all kinds of items in the app.
 */
export interface MetaStoreConfig<C extends ItemConfig, I extends Item<C> ,S extends ItemStore<I> > {
	itemClass			: Type<I>,
	itemLabelComponent?	: never, // legacy
	itemIcon?			: never, // legacy
	serviceClass		: Type<MetaStore<C,I,S>>,
}

export const META_STORE_CONFIGS : InjectionToken<MetaStoreConfig<unknown,Item<unknown>,ItemStore<Item<unknown>>>>
								= new InjectionToken<MetaStoreConfig<unknown,Item<unknown>,ItemStore<Item<unknown>>>>('Meta Store Module Configs')



/**
 * MetaStores are stores of stores for one type of item. All MetaStoreServices
 * should extend this base class. The MetaStoreServices are the most prominent
 * way to access all the items of one type available to the app, regardless of
 * their source or where they are stored.
 *
 * It comes with an Observable that tracks any changes of any item of any store
 * for one item type.
 *
 * MetaStores are also the place where MetaAction for a specific item type are
 * stored and accessible.
 *
 * TODO: The triplet of C,I,S variables should be reduced to one.
 *
 */
export abstract	class MetaStore
				<
					C extends ItemConfig 	= unknown,
					I extends Item<C>		= Item<C>,
					S extends ItemStore<I>	= ItemStore<I>
				> {

	/**
	 * Name used in the UI, Ideally use a translation string here.
	 */
	public abstract	name 		: string

	/**
	 * Changes to any store of the MetaStore or any item in one of them.
	 */
	public change$				: Observable<I>


	public constructor(
		public stores			: S[]					= [],
		public metaActions		: MetaAction<C,I,S>[]	= []
	) {

		this.stores 		= stores 		|| []
		this.metaActions	= metaActions	|| []

		this.change$		= 	merge(...this.stores.map( store => store.change$ ))
								.pipe( share() )
	}

	public get items() : I[] {

		const array_of_items 	: I[] 		= this.stores.map( store => store.items).flat()
		const unique_items 		: I[]		= new Array<I>()
		const ids 				: string[]	= new Array<string>()

		array_of_items.forEach( item => {
			if(!item.id) 				return unique_items.push(item)
			if(ids.includes(item.id))	return null

			ids.push(item.id)
			unique_items.push(item)
		})

		if(array_of_items.length !== unique_items.length) console.warn('MetaStore.items: duplicate item IDs!' )

		return unique_items
	}

	/**
	 * This promise resolves when all the stores are ready and all the items
	 * are retrieved and ready to access.
	 */
	public get ready() : Promise<unknown[]>{

		return 	Promise.all( this.stores.map( store => store.ready) )

	}

	/**
	 * This determines the value that is returned if the store looks up an
	 * item with a given Id, but cannot find a matching item.
	 */
	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	public handleIdWithoutItem(id: string): I | null {
		return null
	}

	public async get (id: string): Promise<I | null>
	public async get (ids: string[]): Promise<( I | null)[]>
	public async get (id_or_ids: string | string []): Promise<(I|null)[]|(I|null)>
	public async get (id_or_ids: string | string []): Promise<(I|null)[]|(I|null)> {

		if(typeof id_or_ids === 'string') return await this.get([id_or_ids]).then( items => items[0])

		const getPromises 	: Promise<I[]>[]	= 	this.stores.map( store => store.get(id_or_ids) ) // TODO: deal with doubles?
		const itemArrays 	: I[][] 			= 	await Promise.all(getPromises)  // TODO: deal with undefined!

		const result 		: I[]				= 	id_or_ids
														.map((id: string, index: number) =>
															itemArrays.reduce((item :I | null, itemArray: (I | null)[])  =>
																item || itemArray[index],
																null
															)
															|| this.handleIdWithoutItem(id)
														)
														.filter(
															(item: I | null ) => item !== null
														)

		return result

	}

	/**
	 * Get the store a given item is included in.
	 */
	public getStore(item: I) : S | null {
		return 	this.stores.find( (store: S) =>  store.items.includes(item) ) || null
	}


	public async filter( callback: (item :I, index?:number, arr?: I[]) => boolean ) : Promise<I[]> {
		await this.ready

		return 	this.items
				.filter(callback)

	}

}
