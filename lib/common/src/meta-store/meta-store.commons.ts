import 	{
			Type
		}							from '@angular/core'

import	{
			ItemStore
		}							from '@rcc/core'

import	{
			ItemActionRole
		}							from '../items'

// TODO: Align with {@link Action}s
export interface ActionBase {
	label			: string,
	role?			: ItemActionRole,
	icon?			: string,
	position?		: number,
	successMessage?	: string,
	failureMessage?	: string,
	store?			: Type<ItemStore>,
}

export interface ActionWithPath extends ActionBase {
	path			: string,			// use ":id" for item id
}

export interface ActionWithHandler<H extends (...args: unknown[]) => unknown> extends ActionBase {
	handler			: H
}

export interface ActionWithHandlerFactory<H extends (...args: unknown[]) => unknown> extends ActionBase {
	deps			: unknown[],
	handlerFactory	: (...dependencies: unknown[]) => H,
}

