import { Component, Input } from '@angular/core'
import { Setting } from '@rcc/common/settings/settings.service'

@Component({
	selector: 'rcc-boolean-setting',
	templateUrl: './boolean-setting.component.html',
    standalone: false,
})
export class BooleanSettingComponent {
	@Input()
	public setting: Setting<boolean>
}
