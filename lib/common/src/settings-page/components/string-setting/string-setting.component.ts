import { Component, Input } from '@angular/core'
import { Setting } from '@rcc/common/settings/settings.service'

@Component({
	selector	: 'rcc-string-setting',
	templateUrl	: './string-setting.component.html',
	standalone	: false,
})
export class StringSettingComponent {
	@Input()
	protected setting: Setting<unknown>
}
