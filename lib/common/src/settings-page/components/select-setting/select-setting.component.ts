import { Component, Input } from '@angular/core'
import { Setting } from '@rcc/common/settings/settings.service'
import { FilterFn } from '@rcc/themes/theming-mechanics/components/pull-down-select/pull-down-select.commons'

@Component({
	selector	: 'rcc-select-setting',
	templateUrl	: './select-setting.component.html',
	styleUrls	: ['./select-setting.component.scss'],
	standalone	: false,
})
export class SelectSettingComponent {
	@Input()
	protected setting: Setting<unknown>

	protected filterFunction: FilterFn<string> = (_, options) => options
}
