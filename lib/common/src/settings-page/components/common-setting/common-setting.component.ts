import { Component, Input } from '@angular/core'
import { RccModalController, Setting } from '@rcc/common'
import { InformationModalComponent } from '../information-modal/information-modal.component'

@Component({
	selector	: 'rcc-common-setting',
	templateUrl	: './common-setting.component.html',
	styleUrls	: ['./common-setting.component.scss'],
	standalone	: false,
})
export class CommonSettingComponent {
	public constructor(
		private readonly rccModalController: RccModalController
	) {}

	@Input()
	public setting: Setting<unknown>

	protected openModal(): void {
		void this.rccModalController.present(
			InformationModalComponent,
			{ componentOutlet: this.setting.entry.information },
			{ mini: true },
		)
	}
}
