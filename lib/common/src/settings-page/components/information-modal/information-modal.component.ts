import { Component, Input, Type } from '@angular/core'
import { RccModalController } from '@rcc/common/modals-provider'

@Component({
	templateUrl	: './information-modal.component.html',
	styleUrls	: ['./information-modal.component.scss'],
	standalone	: false,
})
export class InformationModalComponent {
	public constructor(
		private readonly rccModalController: RccModalController
	) {}

	@Input()
	public componentOutlet: Type<unknown>

	protected dismiss(): void {
		this.rccModalController.dismiss()
	}
}
