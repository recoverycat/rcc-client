import	{	NgModule					}	from '@angular/core'
import 	{
			RouterModule,
			Routes,
		}									from '@angular/router'
import 	{
			SETTINGS_PATH,
			SETTINGS_PAGE_TITLE
		} 									from './settings-page.config'
import	{	OverviewPageComponent		}	from './overview-page/overview-page.component'
import	{	SelectSettingComponent		}	from './components/select-setting/select-setting.component'
import	{	BooleanSettingComponent		}	from './components/boolean-setting/boolean-setting.component'
import	{	StringSettingComponent		} from './components/string-setting/string-setting.component'
import	{	TimeSettingComponent		}	from './components/time-setting/time-setting.component'
import	{
			TranslationsModule,
			provideTranslationMap,
		}									from '../translations'
import	{	SettingsModule				}	from '../settings/settings.module'
import	{	SharedModule				}	from '../shared-module'
import	{	SettingGroupComponent		}	from './components/setting-group/setting-group.component'
import	{	CommonSettingComponent		}	from './components/common-setting/common-setting.component'
import	{	InformationModalComponent	}	from './components/information-modal/information-modal.component'
import	{	HandlerSettingComponent		}	from './components/handler-setting/handler-setting.component'

import en from './i18n/en.json'
import de from './i18n/de.json'

const routes: Routes	=	[
	{
		path: 		SETTINGS_PATH,
		title: 		SETTINGS_PAGE_TITLE,
		component:	OverviewPageComponent
	},
	{
		path: 		SETTINGS_PATH + '/:id',
		title: 		SETTINGS_PAGE_TITLE,
		component:	OverviewPageComponent
	}
]

@NgModule({
	declarations: [
		OverviewPageComponent,
		CommonSettingComponent,
		SettingGroupComponent,
		SelectSettingComponent,
		BooleanSettingComponent,
		TimeSettingComponent,
		StringSettingComponent,
		HandlerSettingComponent,
		InformationModalComponent,
	],
	imports: [
		RouterModule.forChild(routes),
		TranslationsModule,
		SettingsModule,
		SharedModule,
	],
	providers: [
		provideTranslationMap('SETTINGS_PAGE', { en, de }),
	]
})
export class SettingsPageModule {}
