import	{
			NgModule,
			Provider,
			ModuleWithProviders,
		}											from '@angular/core'
import	{
			Action,
			WithPosition,
			getEffectivePosition
		}											from '../actions'

import	{
			provideMainMenuEntry,
		}											from '../main-menu/main-menu.commons'

import	{
			SETTINGS_PATH,
		}											from './settings-page.config'

import	{
			SettingsPageModule,
		}											from './settings-page.module'

@NgModule({
	imports: [
		SettingsPageModule,
	],
})

export class SettingsMainMenuEntryModule {

	/**
	* This method adds an entry for settings to the main menu.
	*
	* Calling it without parameter, will add the entry to the main menu
	* automatically at reasonably adequate positions.
	*
	* If you want to determine the positions yourself provide a `config` parameter :
	*
	* | .position  | value                  | effect                                                    |
	* |------------|------------------------|-----------------------------------------------------------|
	* | .position  | positive number        | adds menu entry at position counting from the top         |
	* |            | negative number        | adds menu entry at position counting from the bottom      |
	*
	* Example:     `SettingsMainMenuEntryModule.addEntry({ position: 1 })`,
	*/

	public static addEntry(config?: WithPosition): ModuleWithProviders<SettingsMainMenuEntryModule> {

		const mainMenuEntry	: Action =	{
				path:			SETTINGS_PATH,
				label:			'SETTINGS.MENU_ENTRY',
				icon:			'settings',
				position:		getEffectivePosition(config, -2)
		}

		const providers : Provider[] = [provideMainMenuEntry(mainMenuEntry)]

		return {
			ngModule:	SettingsMainMenuEntryModule,
			providers
		}
	}

}
