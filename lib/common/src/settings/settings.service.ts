import	{
			Injectable,
			Injector,
			OnDestroy
		}									from '@angular/core'

import 	{	Router						}	from '@angular/router'

import	{
			FormControl,
		}									from '@angular/forms'

import	{
			from,
			map,
			merge,
			mergeMap,
			Observable,
			Subject,
			takeUntil,
		}									from 'rxjs'

import	{
			Item,
			ItemStorage,
		}									from '@rcc/core'

import	{
			SETTING_CONFIGS,
			SettingsEntry,
			SettingsGroupEntry,
			defaultSettingsEntryGroup
		}									from './settings.commons'

import	{
			RccStorage
		}									from '../storage-provider'

/**
 * This interface is initially left empty. It should be expanded on
 * by other modules declaring what settings keys exist and what types
 * they have
 *
 * @example
 * declare module '@rcc/common/settings/settings.service' {
 *   interface SettingValueTypeMap {
 *     'new-setting-key': SettingKeyType
 *   }
 * }
 *
 * const setting: SettingKeyType = this.rccSettingService.get('new-setting-key')
 */
export interface SettingValueTypeMap {
}

export type TypeOfSetting<Key> = Key extends keyof SettingValueTypeMap ? SettingValueTypeMap[Key] : unknown

export interface SettingValue {
	id				: string
	value			: unknown
}

export interface Setting<T=unknown> {
	formControl		: FormControl<T>,
	entry			: SettingsEntry<T>
}



export interface Group {
	subSettings		: Setting[]
	groupSetting	: Setting
}


@Injectable()
export class RccSettingsService implements OnDestroy {

	protected destroy$			: Subject<void>
								= new Subject<void>()

	protected storage			: ItemStorage<Item<SettingValue>>

	public 	ready				: Promise<void>
	private allSettings			: Map<string, Setting>
	public 	rootSettings		: Setting[]	= []

	public 	activeGroupsMap		: Map<string, Group>
								= new Map<string, Group>()


	public constructor(
		protected rccStorage	: RccStorage,
		protected injector		: Injector,
		protected router		: Router
	) {
		this.ready = this.setup()
	}


	/**
	 * Prepares entry to later use in this service. Most importantly links the
	 * entry to a FormControl used in the template. That FormControl is the
	 * most direct way to access the current value of settings entry. The
	 * service provides some extra methods making it easier to access that value
	 * or its changes.
	 */
	private setupEntry(entry: SettingsEntry): Setting {

		if(entry.type === 'handler')	return { formControl: null, entry }
		if(entry.type === 'group')		return { formControl: null, entry }

		const formControl : FormControl = new FormControl<unknown>(null)

		if (entry.restriction === 'read-only') formControl.disable()

		formControl
		.valueChanges
		.pipe( takeUntil( this.destroy$ ) )
		.subscribe( () => void this.storeAll() )

		return { formControl, entry }
	}


	private async setup(): Promise<void> {

		this.storage 		= 	this.rccStorage.createItemStorage<Item<SettingValue>>('rcc-settings')

		const data			: 	SettingValue[]
							= 	await this.storage.getAll()

		const entries		: 	SettingsEntry<unknown>[]
							= 	[
									...this.injector.get(SETTING_CONFIGS, []).filter(x => !!x),
									defaultSettingsEntryGroup
								]


		this.allSettings	=	new Map(
									entries
										// SettingEntries can sensibly have a position of
										// null right from the start, if a factory provider
										// sets it on injection; e.g. after dependency
										// resolution the number of options
										// for a given settings drops to 1 or 0
										.filter( 	entry => entry.position !== null)
										.map(		entry => [ entry.id, this.setupEntry(entry) ] )
								)

		// Initialize all settings, use stored value if available or fallback
		// to default:
		this.allSettings.forEach(setting => {

			const matchingData	: SettingValue	=	data.find(d => d.id === setting.entry.id)

			const value			: unknown		=	matchingData
													&& 	matchingData.value !== null
													&& 	matchingData.value !== undefined

													?	matchingData.value
													:	setting.entry.defaultValue

			if (setting.formControl) setting.formControl.setValue(value)
		})

		this.setupEntryGroups(entries)
	}

	/**
	 * Special preparations for the group entries.
	 */
	private setupEntryGroups(providedEntries: SettingsEntry[]) : void {

		const groupEntries		: 	SettingsGroupEntry[]
								= 	providedEntries
									.filter( (entry) : entry is SettingsGroupEntry => entry.type === 'group')

		const nonGroupEntries	:	SettingsEntry[]
								=	providedEntries
									.filter( entry => entry.type !== 'group')

		// SettingEntires that are not part of a group will go into the default group:
		const lonelyEntries		:	SettingsEntry[]
								=	nonGroupEntries
									.filter( entry => groupEntries.every( groupEntry => !groupEntry.subSettingIds.includes(entry.id) ) )

		// This is a special group that is always provided by the module this
		// service is part of
		defaultSettingsEntryGroup.subSettingIds = 	lonelyEntries
													.map( entry => entry.id )
		/**
		 * Filter for active groups (groups with actual sub settings) and and
		 * keep track of them as {@link Group}s for easier access rather than
		 * {@link SettingsEntry} objects.
		 *
		 * Some of the provided group entries might actually turn out to be
		 * empty even though they do have subSettingIds – that's because those
		 * ids can point to entries that were not provided.
		 */
		groupEntries.forEach( groupEntry => {

			const groupId 		: 	string
								= 	groupEntry.id

			const subSettingIds : 	string[]
								=	groupEntry.subSettingIds || []

			const subSettings	:	Setting[]
								= 	subSettingIds
									.map( 	id => this.allSettings.get(id))
									// If a SettingsEntry is referenced in a group entry, but is not actually provided,
									// elements of this array can be undefined:
									.filter(  x => !!x)


			const groupSetting	:	Setting
								=	this.allSettings.get(groupId)

			const groupEmpty 	: boolean
								= subSettings.length === 0
			const allSubsHidden : boolean
								= subSettings.every(subSetting => subSetting.entry.restriction === 'hidden')

			if(!groupEmpty && !allSubsHidden)
				this.activeGroupsMap.set(groupId, { subSettings, groupSetting })
		})


		const activeGroups		:	Group[]
								=	Array.from(this.activeGroupsMap.values())

		// Groups that are not sub groups of other groups:
		const rootGroups		:	Group[]
								=	activeGroups
									.filter( ({ groupSetting }) => activeGroups.every( ({ subSettings }) => subSettings.every( setting => setting.entry.id !== groupSetting.entry.id) ) )

		this.rootSettings		= 	rootGroups.length === 1
									// In most cases this means, only the default group exists
									?	activeGroups[0].subSettings
									// At least one active group other than the default group exists:
									:	activeGroups.map( group => group.groupSetting )

	}


	public async storeAll() : Promise<void> {

		await this.ready

		const settingValues : SettingValue[] = Array.from(this.allSettings, ([id, setting]) => ({ id, value: setting.formControl?.value }) )

		const mergedSettings: SettingValue[] = await this.mergeWithLocalStorage(settingValues)

		await this.storage.store(mergedSettings)
	}

	private async mergeWithLocalStorage(settingValues: SettingValue[]): Promise<SettingValue[]> {
		const allExistingSettingsFoundInLocalStorage: SettingValue[] = await this.storage.getAll()

		for (const setting of allExistingSettingsFoundInLocalStorage) {
			if (this.allSettings.has(setting.id))
				continue

			settingValues.push(setting)
		}

		return settingValues
	}


	/**
	 * Access a specific setting through its ID.
	 * @template									T	The expected type of the setting value (FormControl.value).
	 *			 										TS will cast the found Setting
	 * @param										id	{@link SettingsEntry} identifier, types should be provided via {@link SettingValueTypeMap}
	 * @returns											The FormControl of the Setting with ID id or undefined if Setting with given ID does not exist
	 */
	public async get<Key extends string>(id: Key): Promise<FormControl<TypeOfSetting<Key>>> {

		await this.ready

		const settings 	: Setting<TypeOfSetting<Key>>
						= this.allSettings.get(id) as Setting<TypeOfSetting<Key>>


		if(
			!this.valueInOptions(settings?.formControl?.value, settings?.entry?.options)
			&& settings?.formControl?.value !== settings?.entry?.defaultValue
		)
			void this.set(id, settings?.entry?.defaultValue as TypeOfSetting<Key>)

		return settings?.formControl
	}

	/**
	 * Access value changes as an Observable for a specific setting given by
	 * its ID. Emits the current value on subscription.
	 *
	 * @param	id	{@link SettingsEntry} identifier, types should be provided via {@link SettingValueTypeMap}
	 */
	public valueChange$<Key extends string>(id: Key): Observable<TypeOfSetting<Key>>{

		const control$: Observable<FormControl<TypeOfSetting<Key>>> = from(this.get(id))

		return merge(
			control$.pipe(map(control => control.value)),
			control$.pipe(mergeMap(control => control.valueChanges)),
		)
	}

	/**
	 * @param	id		{@link SettingsEntry} identifier, types should be provided via {@link SettingValueTypeMap}
	 * @param	value	The value to set for the settings key
	 */
	public async set<Key extends string>(id: Key, value: TypeOfSetting<Key>): Promise<void>
	{
		const formControl 	: FormControl<TypeOfSetting<Key>>
							= await this.get(id)

		if(this.valueInOptions(value, this.allSettings.get(id).entry.options))
			formControl.setValue(value)
	}

	/**
	 * checks whether a value is in the options
	 * @returns true if the value is in the options or no options are provided
	 */
	private valueInOptions<T=unknown>(value: T, options?: {value: T, label:string}[]) : boolean {
		const optionValues: T[] = options?.map(
			(setting: {value: T, label:string}) => setting.value
		)
		return optionValues?.includes(value) ?? true
	}

	public ngOnDestroy(): void {
		this.destroy$.next()
		this.destroy$.complete()
	}
}
