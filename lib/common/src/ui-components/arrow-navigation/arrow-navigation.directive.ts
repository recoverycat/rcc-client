import	{
			Directive,
			AfterViewInit,
			OnDestroy,
			ElementRef,
			Inject,
		}							from '@angular/core'

import	{
			DOCUMENT,
		}							from '@angular/common'
import	{
			Subject,
			takeUntil,
			fromEvent,
			filter,
			Observable,
			merge
		}							from 'rxjs'

import	{
			getTabbableDescendants,
		}							from '@rcc/core'

/**
 * Enables navigation with arrow keys to focus child elements of the host element.
 */
@Directive({
	selector: 	'[arrowNavigation]',
	standalone: true

})
export class RccArrowNavigationDirective implements AfterViewInit, OnDestroy{

	protected 	destroy$			: Subject<void>
									= new Subject<void>()


	protected 	element 			: HTMLElement
									= undefined

	public constructor(
		@Inject(DOCUMENT)
		private document	: Document,
		elementRef	: ElementRef<HTMLElement>
	){
		this.element = elementRef.nativeElement
	}

	public ngAfterViewInit() : void {

		const keydownEvent$		:	Observable<KeyboardEvent>
								=	fromEvent<KeyboardEvent>(this.element, 'keydown')

		const escEvent$			:	Observable<KeyboardEvent>
								=	keydownEvent$
									.pipe( filter( event => event.key === 'Escape') )

		const upEvent$			:	Observable<KeyboardEvent>
								=	keydownEvent$
									.pipe( filter( event => event.key === 'ArrowUp') )
										
		const downEvent$		:	Observable<KeyboardEvent>
								=	keydownEvent$
									.pipe( filter( event => event.key === 'ArrowDown') )

		merge(upEvent$, downEvent$)
		.pipe( takeUntil(this.destroy$) )
		.subscribe( (event: KeyboardEvent) => this.navigate(event) )

		escEvent$
		.pipe( takeUntil(this.destroy$) )
		.subscribe( () => this.clearFocus() )
	}

	protected clearFocus() : void {
		if(!(this.document.activeElement instanceof HTMLElement)) 	return undefined
		if(!this.element.contains(this.document.activeElement))		return undefined

		this.document.activeElement.blur()
	}

	protected navigate(event: KeyboardEvent): void {
		const focusedElement	:	HTMLElement
								=	this.document.activeElement as HTMLElement

		const tabbables 		:	HTMLElement[]
								=	getTabbableDescendants(this.element)

		const focusedElementPos	:	number
								=	tabbables.indexOf(focusedElement)

		const direction			:	number = event.key === 'ArrowUp' ? -1 : 1

		const nextPos			:	number
								=	focusedElementPos === -1
									? 0
									: (focusedElementPos + direction + tabbables.length) % tabbables.length

		const nextElement		:	HTMLElement
								=	tabbables[nextPos]

		nextElement.focus()
		event.preventDefault()
	}

	public ngOnDestroy(): void{
		this.destroy$.next()
		this.destroy$.complete()
	}

}
