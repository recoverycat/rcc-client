import { CommonModule } from '@angular/common'
import { Component, Input } from '@angular/core'

@Component({
    selector: 'rcc-markdown-template',
    template: '<div [innerHtml]="template"></div>',
    imports: [
        CommonModule,
    ]
})
export class RccMarkdownTemplateComponent {
	@Input()
	public template: string
}
