import { Pipe, PipeTransform } from '@angular/core'
import { marked } from 'marked'

@Pipe({
	name		: 'markdown',
	standalone	: true
})
export class MarkdownPipe implements PipeTransform {
	public transform(input: string, inline : boolean = false): string {
		return 	inline
				?	marked.parseInline(input) as string
				:	marked.parse(input)

	}
}
