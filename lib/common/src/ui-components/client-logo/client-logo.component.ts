import { Component, OnInit, inject } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ReplaySubject } from 'rxjs'
import { RccPublicRuntimeConfigService, RccPublicRuntimeConfigModule } from '../../public-runtime-config/config'
import { assertClientLogoConfig } from './client-logo.commons'
import { RccColorCategoryDirective } from '../color-category'

@Component({
    templateUrl: './client-logo.component.html',
    styleUrls: ['./client-logo.component.scss'],
    selector: 'rcc-client-logo',
    imports: [
        CommonModule,
        RccPublicRuntimeConfigModule,
        RccColorCategoryDirective,
    ]
})
export class RccClientLogoComponent implements OnInit {
	private rccPublicRuntimeConfigService: RccPublicRuntimeConfigService = inject(RccPublicRuntimeConfigService)

	protected imgSrc$: ReplaySubject<string> = new ReplaySubject<string>()
	protected altText$: ReplaySubject<string> = new ReplaySubject<string>()

	public ngOnInit(): void {
		void this.rccPublicRuntimeConfigService.get('client_logo').then((config) => {
			assertClientLogoConfig(config)
			
			this.imgSrc$.next(config.src)
			this.altText$.next(config.altText)
		})
	}
}
