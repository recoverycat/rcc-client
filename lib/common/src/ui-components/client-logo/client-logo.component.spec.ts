import { 	ComponentFixture, TestBed 		} 	from '@angular/core/testing'
import { 	RccClientLogoComponent 			} 	from './client-logo.component'
import {
			RccPublicRuntimeConfigService,
			RccPublicRuntimeConfigModule,
		}										from '../../public-runtime-config/config'
import {	 waitUntil } from '../../../../test-utils'

describe('RccClientLogoComponent', () => {
	const mockRuntimeConfigService: Partial<RccPublicRuntimeConfigService> = {
		get: () => Promise.resolve({ src: 'src-path', altText: 'Alt text' })
	}

	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				{ provide: RccPublicRuntimeConfigService, useValue: mockRuntimeConfigService }
			],
		})
		// Removing RccPublicRuntimeConfigModule, because its only purpose is
		// to import RccPublicRuntimeConfigService, which would overwrite the mock
		// from above:
		.overrideComponent(RccClientLogoComponent, { remove: { imports: [ RccPublicRuntimeConfigModule ] } })
	})

	it('should render image provided from runtime config', async () => {
		const fixture: ComponentFixture<RccClientLogoComponent> = TestBed.createComponent(RccClientLogoComponent)
		const component: HTMLElement = fixture.nativeElement as HTMLElement
		
		await waitUntil(() => {
			const src: string | null | undefined = component.querySelector('img')?.getAttribute('src')
			return src != null && src !== ''
		}, fixture)

		expect(component.querySelector('img')?.getAttribute('src')).toBe('src-path')
	})

	it('should include alt text provided from runtime config', async () => {
		const fixture: ComponentFixture<RccClientLogoComponent> = TestBed.createComponent(RccClientLogoComponent)
		const component: HTMLElement = fixture.nativeElement as HTMLElement
		
		await waitUntil(() => {
			const alt: string | null | undefined = component.querySelector('img')?.getAttribute('alt')
			return alt != null && alt !== 'null'
		}, fixture)

		expect(component.querySelector('img')?.getAttribute('alt')).toBe('Alt text')
	})
})
