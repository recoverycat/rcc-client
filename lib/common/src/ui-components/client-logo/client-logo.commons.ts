import { assertProperty, assertType } from '@rcc/core'

interface ClientLogoConfig {
	src: string
	altText: string
}

export function assertClientLogoConfig(clientConfig: unknown): asserts clientConfig is ClientLogoConfig {
	assertType(clientConfig, 'object', 'clientConfig')
	assertProperty(clientConfig, ['src', 'altText'])
	assertType(clientConfig.src, 'string', 'clientConfig.src')
	assertType(clientConfig.altText, 'string', 'clientConfig.altText')
}
