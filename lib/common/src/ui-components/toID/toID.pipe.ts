import	{
			Pipe, PipeTransform
		}								from '@angular/core'

@Pipe({
    name: 'toID',
    standalone: false
})
export class RccToIDPipe implements PipeTransform {

	public transform(x: string, prefix: string = ''): string {

		if(typeof x !== 'string') return x

		const noCamelCaseX: string      = x.replace(
			/([a-z])([A-Z]+)/g,
			(match, s1: string, s2: string) => this.resolveCamelCase(match, s1, s2)
		)
		const lowerCaseX: string        = noCamelCaseX.toLowerCase()
		const ID: string                = lowerCaseX.replace(/[^a-z0-9]/g,'-')

		return	prefix
				? prefix + '-' + ID
				: ID
	}

	public resolveCamelCase(match: string, s1: string, s2: string) : string {
		return s1+'-'+s2.toLowerCase()
	}
}
