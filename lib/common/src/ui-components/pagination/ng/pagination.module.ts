import	{	NgModule					}	from '@angular/core'
import	{	CommonModule				}	from '@angular/common'
import	{	IonicModule					}	from '@ionic/angular'
import	{	IconsModule					}	from '../../../icons'
import	{	RccPaginationComponent		}	from './pagination.component'

@NgModule({
	imports:[
		CommonModule,
		IconsModule,
		IonicModule,
	],
	declarations:[
		RccPaginationComponent
	],
	exports: [
		RccPaginationComponent
	]
})
export class RccPaginationModule {

}
