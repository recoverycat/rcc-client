import	{
			Directive,
			AfterViewInit,
			ElementRef
		}							from '@angular/core'


@Directive({
    selector: '[autofocus]',
    standalone: false
})
export class RccAutofocusDirective implements AfterViewInit{

	public constructor(private host: ElementRef<HTMLElement>){}

	public ngAfterViewInit() : void {
		this.host.nativeElement.focus()
  }

}
