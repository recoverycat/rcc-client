import 	{	NgModule				} from '@angular/core'
import	{	RccAutofocusDirective	} from './autofocus.directive'

@NgModule({
	declarations:[
		RccAutofocusDirective
	],
	exports:[
		RccAutofocusDirective
	]
})
export class RccAutofocusModule {}
