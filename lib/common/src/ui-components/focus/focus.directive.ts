import	{
			Directive,
			ElementRef,
			Inject,
			Input,
			OnDestroy,
		}							from '@angular/core'

import	{
			Subject,
			Observable,
			takeUntil,
			Subscription
		}							from 'rxjs'

import	{
			DOCUMENT
		}							from '@angular/common'

/**
 * Listens to an Observable and focuses the first focusable child element,
 * whenever the Observable emits. If the element itself is focusable,
 * it focuses on itself, instead.
 */
@Directive({
    selector: '[rccFocus]',
    standalone: false
})
export class RccFocusDirective implements OnDestroy{

	@Input()
	protected set rccFocus(obs: Observable<unknown>){ this.subscribe(obs) }

	private destroy$		: Subject<void> = new Subject<void>()
	private subscription	: Subscription	= undefined

	public constructor(
		private host: ElementRef<HTMLElement>,
		@Inject(DOCUMENT)
		private document: Document
	){}

	/**
	 * Find all focusable children of an element. If no element is provided
	 * starts the search from document root.
	 */
	private getFocusableElements(element: HTMLElement|Document = this.document) : Array<HTMLElement> {
		return Array.from(element.querySelectorAll('button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])'))
	}

	private subscribe(observable: Observable<unknown>) : void {
		if(this.subscription) this.subscription.unsubscribe()

		this.subscription 	= 	observable
								.pipe( takeUntil(this.destroy$) )
								.subscribe( () => this.focus())
	}

	private focus() : void {
		const element			: HTMLElement 	=	this.host.nativeElement
		const isFocusable 		: boolean 		=	this.getFocusableElements(element.parentElement).includes(element)

		const focusableElement 	: HTMLElement	=	isFocusable
													?	element
													:	this.getFocusableElements(element)[0]

		focusableElement?.focus()
	}

	public ngOnDestroy() : void {
		this.destroy$.next()
		this.destroy$.complete()
	}
}
