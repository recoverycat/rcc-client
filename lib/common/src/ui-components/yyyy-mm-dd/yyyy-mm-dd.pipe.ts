import	{
			Pipe, PipeTransform
		}								from '@angular/core'

import	{
			toISO8601CalendarDate
		}								from '@rcc/core'

/**
 * This pipe converts a Date object into a YYYY-MM-DD formatted string
 * according to local time.
 *
 * Handing data into components inputs can be tricky (in many ways, ugh!),
 * specifically because angular will not detect changes when the Date object
 * itself is updated.
 *
 * This pipe is impure and should therefor only rarely be used.
 */
@Pipe({
	name : 		'YYYY_MM_DD',
	pure :		false,		// impure because Dates can change.
	standalone: true

})
export class RccYyyyMmDdPipe implements PipeTransform {

	public transform(date: Date): string {
		return toISO8601CalendarDate(date)
	}


}
