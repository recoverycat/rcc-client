import 	{
			Pipe,
			PipeTransform,
		}								from '@angular/core'

import	{
			biEndSortFn,
			mapSort,
			has
		}								from '@rcc/core'


/**
 * Sort input by their `.position` property. ( see {@link biEndSortFn})
 */
@Pipe({
    name: 'sortByPosition',
    pure: false,
    standalone: false
})
export class SortByPositionPipe implements PipeTransform {


	private positions 	: Map<unknown, number|null> = new Map<unknown, number|null>()

	private lastValue	: (unknown)[] = null


	private getPosition(x: unknown, property?: string): number | null | undefined {

			let entry : unknown = x

			if( has(x,property) ){
				const sub : unknown = x[property]
				if( has(sub, 'position') ) entry = sub
			}

			let position : number | null = undefined

			if( has(entry,'position') ){

				const value : unknown	=	typeof entry.position === 'function'
											?	entry.position() 	as unknown
											:	entry.position

				if(value === null) 				position = null
				if(typeof value === 'number')	position = value

			}

			return position

	}

	public transform<T>(entries?: T[], property?: string): T[] {

		if(!entries) return entries

		let change_detected : boolean = this.lastValue === null

		entries.forEach( (entry : T) => {

			const current_position 	: number | null | undefined = 	this.getPosition(entry, property)
			const previous_position	: number | null | undefined	= 	this.positions.get(entry)

			if(!this.positions.has(entry))				change_detected = true
			if(previous_position !== current_position) 	change_detected = true

			this.positions.set(entry, current_position )
		})

		if(change_detected) this.lastValue = 	[...entries]
												.filter( entry => this.positions.get(entry) !== null)
												.sort( mapSort( (entry : unknown) => this.positions.get(entry), biEndSortFn) )

		return this.lastValue as T[]


	}

}
