import {
			Directive,
			HostBinding,
			Input,
		} 					from '@angular/core'
import {
			ColorCategory,
			ColorVariants,
			RccColorService
		} 					from '@rcc/themes/theming-mechanics/colors'

/**
 * This directive will add css variables for colors to its host's
 * style declaration. Colors can be either set by {@link HierarchicalColor}
 * (like 'primary', 'secondary' and so on), or by {@link CategoricalColor}
 * (like 'create', 'share'). The host will then have css variables
 * set for main and supplementary color variants that can be referenced
 * by it and its descendants.
 * @example
 * ```html
 * <div colorCategory="primary">
 * 		<p style="color: var(--main)">
 * 			This text will be in whatever is
 * 			defined as the primary main color
 * 		</p>
 * </div>
 * ```
 * You can also change the name of the css variable by adding a suffix:
 *  * @example
 * ```html
 * <div colorCategory="primary" colorCategorySuffix="background">
 * 		<p style="background: var(--main-background)">
 * 			The background of this paragraph will be in whatever is
 * 			defined as the primary main color
 * 		</p>
 * </div>
 * ```
 * Instead of providing a single color category and a single color category suffix,
 * you can also provide a list of color categories and a list of color category suffixes.
 * Suffixes and color categories will be aligned according to their position in their respective arrays.
 */

@Directive({
	selector: '[colorCategory]',
	standalone: true
})
export class RccColorCategoryDirective {

	public colorCategories		:	ColorCategory[]	=	[]

	@Input()
	public set colorCategory(c: ColorCategory | ColorCategory[]) {
		const colorCategories: ColorCategory[] = Array.isArray(c)? c : [c]
		this.colorCategories = colorCategories.filter((colorCategory: ColorCategory) => !!colorCategory)

		this.updateColorRecord()
	}
	public colorCategoriesSuffixes		:	string[]	=	[]

	@Input()
	public set colorCategorySuffix(c: string | string[]) {
		this.colorCategoriesSuffixes = Array.isArray(c)? c : [c]
		this.updateColorRecord()
	}


	@HostBinding('style')
	public get style() : Record<string, string> {
		return this.colorRecord
	}

	public colorRecord: Record<string, string> = undefined

	public updateColorRecord(): void {

		this.colorRecord = {}

		if(this.colorCategories.length === 0) return undefined

		const colorVariantsArray: ColorVariants[] = []

		this.colorCategories.forEach((category: ColorCategory, index: number) => {
			colorVariantsArray[index] = this.rccColorService.getColorVariants(category)
		})

		const suffixes				:	string[]
									=	this.colorCategoriesSuffixes.length
										?	this.colorCategoriesSuffixes
										:	['']

		suffixes.forEach((suffix: string, index: number) => {

			const colorVariants	: ColorVariants
								= colorVariantsArray[index%colorVariantsArray.length]


			this.colorRecord[this.addSuffix('--main', suffix)]						=	colorVariants.main
			this.colorRecord[this.addSuffix('--main-highlight', suffix)]			=	colorVariants.mainHighlight
			this.colorRecord[this.addSuffix('--main-contrast', suffix)]				=	colorVariants.mainContrast
			this.colorRecord[this.addSuffix('--main-highlight-contrast', suffix)]	=	colorVariants.mainHighlightContrast
			this.colorRecord[this.addSuffix('--supp', suffix)]						=	colorVariants.supp
			this.colorRecord[this.addSuffix('--supp-highlight', suffix)]			=	colorVariants.suppHighlight
			this.colorRecord[this.addSuffix('--supp-contrast', suffix)]				=	colorVariants.suppContrast
			this.colorRecord[this.addSuffix('--supp-highlight-contrast', suffix)]	=	colorVariants.suppHighlightContrast
		})

	}

	private addSuffix(value: string, suffix: string): string {
		return suffix ? `${value}-${suffix}` : value
	}


	private colorVariants : Record<string, ColorVariants> | null = null

	public constructor(
		private readonly rccColorService	:	RccColorService
	) {}

}
