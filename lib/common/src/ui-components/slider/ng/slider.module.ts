import	{	NgModule			}	from '@angular/core'

import	{
			RccSliderComponent,
			RccSlideComponent
		}							from './slider.component'


@NgModule({
	declarations: [
		RccSliderComponent,
		RccSlideComponent,
	],
	exports: [
		RccSliderComponent,
		RccSlideComponent
	]
})
export class RccSliderModule {}
