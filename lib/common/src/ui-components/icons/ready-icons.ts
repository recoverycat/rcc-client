import	{
			IconConfig,
			IconAlias,
		}							from './icon.commons'

export const rccIconAdd: IconConfig = {
	name: 'add',
	iconVariants: {
		small: '',
		medium: `
			<svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M9 5V13" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M13 9L5 9" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M9 17C13.4182 17 17 13.4182 17 9C17 4.58172 13.4182 1 9 1C4.58172 1 1 4.58172 1 9C1 13.4182 4.58172 17 9 17Z" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
			</svg>
		`,
		large: ''
	}
}

export const rccIconQR: IconConfig = {

	name: 'scan',
	iconVariants: {
		'small': '',
		'medium': `
			<svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M11.6611 8.99512V11.6601" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M8.99512 1V3.66502" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M14.3262 8.99512V11.6601" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M8.99512 14.3252H16.9907" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M14.3262 16.9902H16.9914" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M3.66504 8.99512H6.33022" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M3.66504 3.67491L3.67392 3.66504" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M8.99512 9.00498L9.004 8.99512" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M1 9.00498L1.00888 8.99512" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M8.99512 6.33995L9.004 6.33008" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M8.99512 11.67L9.004 11.6602" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M11.6611 17.0001L11.67 16.9902" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M8.99512 17.0001L9.004 16.9902" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M16.9912 9.00498L17.0001 8.99512" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M16.9912 11.67L17.0001 11.6602" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M14.3262 3.67491L14.3351 3.66504" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M6.33037 1.533V5.79704C6.33037 6.09141 6.09172 6.33005 5.79733 6.33005H1.53304C1.23865 6.33005 1 6.09141 1 5.79704V1.533C1 1.23863 1.23865 1 1.53304 1H5.79733C6.09172 1 6.33037 1.23863 6.33037 1.533Z" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M16.9915 1.533V5.79704C16.9915 6.09141 16.7529 6.33005 16.4585 6.33005H12.1942C11.8998 6.33005 11.6611 6.09141 11.6611 5.79704V1.533C11.6611 1.23863 11.8998 1 12.1942 1H16.4585C16.7529 1 16.9915 1.23863 16.9915 1.533Z" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M3.66504 14.3351L3.67392 14.3252" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M6.33037 12.1932V16.4572C6.33037 16.7516 6.09172 16.9902 5.79733 16.9902H1.53304C1.23865 16.9902 1 16.7516 1 16.4572V12.1932C1 11.8988 1.23865 11.6602 1.53304 11.6602H5.79733C6.09172 11.6602 6.33037 11.8988 6.33037 12.1932Z" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
			</svg>`,
		'large': `
			<svg width="54" height="54" viewBox="0 0 54 54" fill="none" stroke='#000' xmlns="http://www.w3.org/2000/svg">
				<rect x="38.5" y="0.5" width="15" height="15"/>
				<rect x="42.5" y="4.5" width="7" height="7"/>
				<rect x="38.5" y="38.5" width="15" height="15"/>
				<rect x="42.5" y="42.5" width="7" height="7"/>
				<rect x="0.5" y="0.5" width="15" height="15"/>
				<rect x="4.5" y="4.5" width="7" height="7"/>
				<line x1="3" y1="19.5" x2="19" y2="19.5"/>
				<line x1="8" y1="23.5" x2="24" y2="23.5"/>
				<line x1="34" y1="23.5" x2="54" y2="23.5"/>
				<line x1="19.5" y1="20" x2="19.5" y2="4"/>
				<line x1="24.5" y1="24" x2="24.5" y2="8"/>
				<line x1="33.5" y1="24" x2="33.5" y2="16"/>
				<line x1="33.5" y1="50" x2="33.5" y2="42"/>
				<line x1="0.5" y1="54" x2="0.5" y2="46"/>
				<line x1="2" y1="28.5" x2="16" y2="28.5"/>
				<line x1="2" y1="38.5" x2="16" y2="38.5"/>
				<line x1="20" y1="28.5" x2="34" y2="28.5"/>
				<line x1="20" y1="38.5" x2="34" y2="38.5"/>
				<line x1="29.5" y1="14" x2="29.5" y2="2.18557e-08"/>
				<line x1="38" y1="28.5" x2="52" y2="28.5"/>
				<line y1="33.5" x2="42" y2="33.5"/>
				<line x1="8" y1="44.5" x2="28" y2="44.5"/>
				<line x1="1" y1="53.5" x2="16" y2="53.5"/>
				<line x1="4" y1="49.5" x2="34" y2="49.5"/>
			</svg>`
	} ,
}

export const rccCalendar: IconConfig = {
	name: 'calendar',
	iconVariants: {
		small: '',
		medium: `
		<svg width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path d="M14 1V4" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
			<path d="M2 1V4" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
			<path d="M5 1V4" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
			<path d="M8 1V4" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
			<path d="M11 1V4" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
			<rect x="0.5" y="2.5" width="15" height="14" rx="1.5" stroke="#0E0E0E"/>
			<line x1="1" y1="8.5" x2="15" y2="8.5" stroke="black"/>
		</svg>
		`,
		large: ''
	}
}

export const rccIconCircle: IconConfig = {
	name: 'circle',
	iconVariants: {
		small: '',
		medium: `
			<svg viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
				<circle cx="8" cy="8" r="7" fill="none" stroke-width="1" stroke="black"/>
				<circle class="fill-indicator" cx="8" cy="8" r="4" />
			</svg>
		`,
		large: '',
	}
}

export const rccIconCheckbox: IconConfig = {
	name: 'checkbox',
	iconVariants: {
		small: '',
		medium: `
		<svg width="16" height="15" viewBox="0 0 17 12" fill="none" xmlns="http://www.w3.org/2000/svg">
			<rect x="0.5" y="-1.5" width="15" height="15" stroke="#0E0E0E"/>
		</svg>
		`,
		large: ''
	}
}

export const rccIconCheckboxChecked: IconConfig = {
	name: 'checkbox_checked',
	iconVariants: {
		small: '',
		medium: `
		<svg width="16" height="15" viewBox="0 0 17 12" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path d="M5.52558 10.0509H5.49584M5.49584 10.0509L14.5173 1M5.49584 10.0509L3.2479 7.78818L0.999951 5.52544" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
			<rect x="0.5" y="-1.5" width="15" height="15" stroke="#0E0E0E"/>
		</svg>
		`,
		large: ''
	}
}

export const rccIconCheckMark: IconConfig = {
	name: 'check_mark',
	iconVariants: {
		small: '',
		medium: `
			<svg width="16" height="11" viewBox="0 0 16 11" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M5.52558 10.0509H5.49584M5.49584 10.0509L14.5173 1M5.49584 10.0509L3.2479 7.78818L0.999951 5.52544" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
			</svg>
		`,
		large: '',
	}
}

export const rccIconClose: IconConfig = {
	name: 'close',
	iconVariants: {
		small: '',
		medium: `
		<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
			<line x1="2" y1="22" x2="22" y2="2" stroke="#000" stroke-width="4" stroke-linecap="round"/>
			<line x1="22" y1="22" x2="2" y2="2" stroke="#000" stroke-width="4" stroke-linecap="round" />
		</svg>
		`,
		large: ''
	}
}

export const rccIconData: IconConfig = {
	name: 'data',
	iconVariants: {
		'small': '',
		'medium': '',
		'large': `
			<svg width="54" height="54" viewBox="0 0 54 54" fill="none" stroke='#000' xmlns="http://www.w3.org/2000/svg">
				<path d="M53.5 27V53.5H0.5V0.5H27H37.7929L45.6464 8.35355L53.5 16.2071V27Z"/>
				<path d="M9.5 25.5H12.5V48.5H9.5V25.5Z"/>
				<path d="M25.5 22.5H28.5V48.5H25.5V22.5Z"/>
				<path d="M41.5 17.5H44.5V48.5H41.5V17.5Z"/>
				<path d="M17.5 28.5H20.5V48.5H17.5V28.5Z"/>
				<path d="M33.5 25.5H36.5V48.5H33.5V25.5Z"/>
				<path d="M17 16C18.1046 16 19 15.1046 19 14C19 12.8954 18.1046 12 17 12C15.8954 12 15 12.8954 15 14C15 15.1046 15.8954 16 17 16Z" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M25 14C23.3212 16.991 20.3052 20 17 20C13.6948 20 10.6787 16.991 9 14C11.0432 11.1583 13.437 8 17 8C20.563 8 22.9569 11.1582 25 14Z" stroke-linecap="round" stroke='#fff'/>
			</svg>`,
	} ,
}

export const rccIconDownloadSession: IconConfig = {
	name: 'download_session',
	iconVariants: {
		small: '',
		medium: `
			<svg width="54" height="54" viewBox="0 0 54 54" fill="none" xmlns="http://www.w3.org/2000/svg">
				<g clip-path="url(#clip0_5828_61278)">
				<path d="M0.5 53.5V0.5H27H37.7929L53.5 16.2071V27V53.5H0.5Z" stroke="#0E0E0E"/>
				<line x1="20" y1="27.5" x2="34" y2="27.5" stroke="#0E0E0E"/>
				<line x1="10" y1="33.5" x2="44" y2="33.5" stroke="#0E0E0E"/>
				<line x1="17" y1="39.5" x2="37" y2="39.5" stroke="#0E0E0E"/>
				<line x1="14" y1="45.5" x2="40" y2="45.5" stroke="#0E0E0E"/>
				<path d="M22 11.5C18.5 11.5 16.5 11.5 13 11.5C12.8385 11.5 9 11.5 9 15.5C9 20 12.7024 20 13 20C16 20 18 20 21 20" stroke="#0E0E0E" stroke-linecap="square"/>
				<path d="M19.5 15C20.8668 13.6332 21.6332 12.8668 23 11.5C21.6332 10.1332 20.8668 9.36683 19.5 8" stroke="#0E0E0E" stroke-linecap="square"/>
				</g>
				<defs>
				<clipPath id="clip0_5828_61278">
				<rect width="54" height="54" fill="white"/>
				</clipPath>
				</defs>
			</svg>
		`,
		large: ''
	}
}

export const rccIconError: IconConfig = {
	name: 'error',
	iconVariants: {
		'small': '',
		'medium': `
			<svg width="54" height="54" viewBox="0 0 54 54" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M53.5 27V53.5H0.5V0.5H27H37.7929L53.5 16.2071V27Z"/>
				<path d="M19 38L27.0002 30.0002M27.0002 30.0002L35 22M27.0002 30.0002L19 22M27.0002 30.0002L35 38" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
			</svg>`,
		'large': ''
	} ,
}

export const rccIconHelp: IconConfig = {
	name: 'help',
	iconVariants: {
		medium: `
			<svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M9 5V10" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M9 13.0101L9.01 12.999" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M9 17C13.4182 17 17 13.4182 17 9C17 4.58172 13.4182 1 9 1C4.58172 1 1 4.58172 1 9C1 10.4571 1.38958 11.8233 2.07026 13L1.4 16.6L5 15.9298C6.1767 16.6104 7.54288 17 9 17Z" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
			</svg>
		`
	}
}

export const rccIconInformation: IconConfig = {
	name: 'information',
	iconVariants: {
		small: '',
		medium: `
		<svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path d="M9 17C13.4182 17 17 13.4182 17 9C17 4.58172 13.4182 1 9 1C4.58172 1 1 4.58172 1 9C1 13.4182 4.58172 17 9 17Z" stroke="#000000"/>
			<path d="M9 8.00195V13.002" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"/>
			<path d="M9 5.01111L9.01 5" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"/>
		</svg>
		`,
		large: ''
	}
}

export const rccIconLoad: IconConfig = {
	name: 'load',
	iconVariants: {
		'small': '',
		'medium': '',
		'large': `
			<svg width="54" height="54" viewBox="0 0 54 54" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M53.5 27V53.5H0.5V0.5H27H37.7929L53.5 16.2071V27Z"/>
				<path d="M41.6651 25.5999C39.1899 19.9491 33.5374 16 26.9602 16C18.6432 16 11.8047 22.3146 11 30.3999" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M34 25.9999H41.0573C41.5889 25.9999 42.0197 25.5701 42.0197 25.0399V18" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
			</svg>`,
	},
}

export const rccIconMedicalProduct: IconConfig = {
	name: 'medical-device',
	iconVariants: {
		medium: `
			<svg width="32" height="21" viewBox="0 0 32 21" fill="none" xmlns="http://www.w3.org/2000/svg">
				<g clip-path="url(#clip0_7329_83248)">
				<path d="M31.012 19.0145H1.00819V1.00819H31.012V19.0145ZM0 20.0227H32V0H0V20.0227Z" fill="#231F20"/>
				<path d="M3.46875 16.2521V3.75049H7.23939L9.5179 12.2798L11.7561 3.75049H15.5469V16.2521H13.1877V6.41211L10.7076 16.2521H8.28791L5.80775 6.41211V16.2521H3.46875Z" fill="#231F20"/>
				<path d="M20.5661 5.86782V14.1148H22.4615C22.964 14.173 23.4714 14.173 23.9738 14.1148C24.3562 14.0321 24.7106 13.8514 25.0022 13.5906C25.3214 13.2491 25.5501 12.8332 25.6676 12.3807C25.8522 11.629 25.9336 10.8557 25.9096 10.0821C25.9339 9.32835 25.8525 8.57495 25.6676 7.84387C25.528 7.38739 25.2788 6.97202 24.9417 6.63404C24.6141 6.33373 24.209 6.13117 23.7722 6.04929C23.105 5.9488 22.4302 5.90832 21.7558 5.92831L20.5661 5.86782ZM18.1465 3.75061H22.764C23.5325 3.74653 24.2983 3.84141 25.0425 4.03291C25.7564 4.24761 26.4024 4.64359 26.9178 5.18225C27.4591 5.76128 27.8714 6.44852 28.1276 7.19863C28.4069 8.13387 28.5429 9.10606 28.5309 10.0821C28.5837 10.9933 28.4814 11.907 28.2284 12.784C27.9565 13.6183 27.494 14.3777 26.8774 15.002C26.3712 15.4654 25.7635 15.8037 25.103 15.9901C24.3814 16.1904 23.6334 16.2788 22.885 16.2522H18.1465V3.75061Z" fill="#231F20"/>
				</g>
				<defs>
				<clipPath id="clip0_7329_83248">
				<rect width="32" height="20.0227" fill="white"/>
				</clipPath>
				</defs>
			</svg>
		`
	}
}

export const rccIconMonitoringSetup: IconConfig = {
	name: 'monitoring_setup',
	iconVariants: {
		'small': '',
		'medium': `
					<svg width="54" height="54" fill="none" viewBox="0 0 54 54" stroke='#000' xmlns="http://www.w3.org/2000/svg">
						<path d="M0.5 53.5V0.5H27H37.7929L53.5 16.2071V27V53.5H0.5Z"/>
							<line x1="14" y1="21.5" x2="40" y2="21.5"/>
							<line x1="20" y1="27.5" x2="34" y2="27.5"/>
							<line x1="10" y1="33.5" x2="44" y2="33.5"/>
							<line x1="17" y1="39.5" x2="37" y2="39.5"/>
							<line x1="14" y1="45.5" x2="40" y2="45.5"/>
					</svg>`,
		'large': ''
	}
}
export const rccIconEdit: IconConfig = {
	name: 'edit',
	iconVariants: {
		'small': `
					<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path d="M8.90005 2.90001L11.3 0.5L15.5 4.70001L13.1 7.09998M8.90005 2.90001L0.748526 11.0515C0.589401 11.2106 0.5 11.4265 0.5 11.6515V15.5H4.34853C4.57358 15.5 4.7894 15.4106 4.94854 15.2515L13.1 7.09998M8.90005 2.90001L13.1 7.09998" stroke-linecap="round" stroke-linejoin="round" stroke='#fff'/>
					</svg>
		`,
		'medium': undefined,
		'large': undefined
	}
}
export const rccIconDelete: IconConfig = {
	name: 'delete',
	iconVariants: {
		'small': '',
		'medium': `
		<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
			<line x1="2" y1="22" x2="22" y2="2" stroke="#000" stroke-width="2" stroke-linecap="round"/>
			<line x1="22" y1="22" x2="2" y2="2" stroke="#000" stroke-width="2" stroke-linecap="round" />
		</svg>
		`,
		'large': ''
	}
}

export const rccIconDeleteCircle: IconConfig = {
	name: 'delete_circle',
	iconVariants: {
		small: '',
		medium: `
			<svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
				<circle cx="16" cy="16" r="15.5" />
				<path d="M11.5 20.5509L15.9959 16.0255M20.4918 11.5L15.9959 16.0255M15.9959 16.0255L11.5 11.5M15.9959 16.0255L20.4918 20.5509" stroke-linecap="round" stroke-linejoin="round"/>
			</svg>
		`,
		large: ''
	}
}

export const rccIconMedication: IconConfig = {
	name: 'medication',
	iconVariants: {
		'small': '',
		'medium': `
					<svg width="54" height="54" viewBox="0 0 54 54" fill="none" xmlns="http://www.w3.org/2000/svg">
						<circle cx="27" cy="27" r="26.5" stroke="#0E0E0E" />
						<rect x="15.5" y="9.5" width="23" height="35" rx="11.5" stroke="#0E0E0E"/>
						<line x1="15" y1="26.5" x2="39" y2="26.5" stroke="black"/>
					</svg>
				`,
		'large': ''
	}
}

export const rccIconPin: IconConfig = {
	name: 'pin',
	iconVariants: {
		small: '',
		medium: `
			<svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M6.77571 11.3203L1 16.9947" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M2.7771 6.94834L10.9452 14.9731L12.4531 13.4916L12.1047 10.1863L17 6.10663L11.8021 1L7.64943 5.80916L4.28506 5.46686L2.7771 6.94834Z" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
			</svg>
		`,
		large: ''
	}
}

export const rccIconSearch: IconConfig = {
	name: 'search',
	iconVariants: {
		small: '',
		medium: `
			<svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M13 13L17 17" stroke="#0E0E0E" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M1 7.85714C1 11.6442 4.07005 14.7143 7.85714 14.7143C9.75394 14.7143 11.471 13.9441 12.7123 12.6994C13.9495 11.4591 14.7143 9.74743 14.7143 7.85714C14.7143 4.07005 11.6442 1 7.85714 1C4.07005 1 1 4.07005 1 7.85714Z" stroke="#0E0E0E" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
			</svg>
		`,
		large: ''
	}
}

export const rccIconSymptoms: IconConfig = {
	name: 'symptoms',
	iconVariants: {
		'small': '',
		'medium': `
					<svg width="54" height="54" viewBox="0 0 54 54" fill="none" xmlns="http://www.w3.org/2000/svg">
						<circle cx="27" cy="27" r="26.5" stroke="#0E0E0E" />
						<circle cx="27" cy="27" r="18.5" stroke="#0E0E0E" />
						<path d="M35.0132 33.0525L27.0132 16.9473L19.0132 33.0525Z" stroke="#0E0E0E" stroke-linecap="square"/>
					</svg>
				`,
		'large': ''
	}
}
export const rccIconSideEffect: IconConfig = {
	name: 'side_effects',
	iconVariants: {
		'small': '',
		'medium': `
					<svg width="54" height="54" viewBox="0 0 54 54" fill="none" xmlns="http://www.w3.org/2000/svg">
						<circle cx="27" cy="27" r="26.5" stroke="#0E0E0E" />
						<circle cx="27" cy="27" r="18.5" stroke="#0E0E0E" />
						<line x1="27" y1="15" x2="27" y2="31" stroke="#0E0E0E" stroke-linecap="square"/>
						<circle cx="27" cy="38" r="1" stroke="#0E0E0E" />
					</svg>
				`,
		'large': ''
	}
}

export const rccIconRefresh: IconConfig = {
	name: 'refresh',
	iconVariants: {
		small: '',
		medium: `
		<svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path d="M16.3332 6.33242C15.0956 3.1936 12.2693 1 8.9804 1C5.46369 1 2.47539 3.50854 1.39355 6.9989" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
			<path d="M12.9897 6.33275H16.3325C16.7011 6.33275 16.9999 6.034 16.9999 5.66547V1.88916" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
			<path d="M1.6665 11.6675C2.90414 14.8063 5.73043 16.9999 9.01934 16.9999C12.5361 16.9999 15.5243 14.4913 16.6062 11.001" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
			<path d="M5.01013 11.668H1.66742C1.29881 11.668 1 11.9667 1 12.3352V16.1116" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
		</svg>
		`,
		large: ''
	}
}

export const rccIconResources: IconConfig = {
	name: 'resources',
	iconVariants: {
		'small': '',
		'medium': `
					<svg width="54" height="54" viewBox="0 0 54 54" fill="none" xmlns="http://www.w3.org/2000/svg">
						<circle cx="27" cy="27" r="26.5" stroke="#0E0E0E" />
						<circle cx="27" cy="27" r="18.5" stroke="#0E0E0E" />
						<circle cx="27" cy="27" r="7.5" stroke="#0E0E0E" />
					</svg>
				`,
		'large': ''
	}
}
export const rccIconWarningSigns: IconConfig = {
	name: 'early_warning_signs',
	iconVariants: {
		'small': '',
		'medium': `
					<svg width="54" height="54" viewBox="0 0 54 54" fill="none" xmlns="http://www.w3.org/2000/svg">
						<circle cx="27" cy="27" r="26.5" stroke="#0E0E0E" />
						<circle cx="32.25" cy="22.5" r="12.5" stroke="#0E0E0E" />
						<circle cx="22.25" cy="32.5" r="12.5" stroke="#0E0E0E" />
					</svg>
				`,
		'large': ''
	}
}

export const rccIconViewData: IconConfig = {
	name: 'view_data',
	iconVariants: {
		small: '',
		medium: `
			<svg width="54" height="54" viewBox="0 0 54 54" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M53.5 27V53.5H0.5V0.5H27H37.7929L45.6464 8.35355L53.5 16.2071V27Z" stroke="#0E0E0E"/>
				<path d="M9.5 25.5H12.5V48.5H9.5V25.5Z" stroke="#0E0E0E"/>
				<path d="M25.5 22.5H28.5V48.5H25.5V22.5Z" stroke="#0E0E0E"/>
				<path d="M41.5 17.5H44.5V48.5H41.5V17.5Z" stroke="#0E0E0E"/>
				<path d="M17.5 28.5H20.5V48.5H17.5V28.5Z" stroke="#0E0E0E"/>
				<path d="M33.5 25.5H36.5V48.5H33.5V25.5Z" stroke="#0E0E0E"/>
				<path d="M17 16C18.1046 16 19 15.1046 19 14C19 12.8954 18.1046 12 17 12C15.8954 12 15 12.8954 15 14C15 15.1046 15.8954 16 17 16Z" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M25 14C23.3212 16.991 20.3052 20 17 20C13.6948 20 10.6787 16.991 9 14C11.0432 11.1583 13.437 8 17 8C20.563 8 22.9569 11.1582 25 14Z" stroke="#0E0E0E" stroke-linecap="round"/>
			</svg>
		`,
		large: ''
	}
}

export const rccIconWithdrawalSymptoms: IconConfig = {
	name: 'withdrawal_symptoms',
	iconVariants: {
		'small': '',
		'medium': `
					<svg width="54" height="54" viewBox="0 0 54 54" fill="none" xmlns="http://www.w3.org/2000/svg">
						<circle cx="27" cy="27" r="26.5" stroke="#0E0E0E" />
						<circle cx="27" cy="27" r="18.5" stroke="#0E0E0E" />
						<path d="M27.817 27H44.7059M44.7059 27L40.8085 23M44.7059 27L40.8085 31" stroke="#0E0E0E" stroke-linecap="square" stroke-linejoin="round"/>
						<path d="M27.817 27L8.9412 27M8.9412 27L13.2972 31M8.9412 27L13.2972 23" stroke="#0E0E0E" stroke-linecap="square" stroke-linejoin="round"/>
					</svg>
				`,
		'large': ''
	}
}
export const rccIconHome: IconConfig = {
	name: 'home',
	iconVariants: {
		'small': '',
		'medium': `
					<svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M1 6.71801L9 1.20447L17 6.71801" stroke="black" stroke-linecap="square" stroke-linejoin="round"/>
						<path d="M16 6.2168V16.2987C16 16.8208 15.7314 17.2439 15.4 17.2439H2.6C2.26863 17.2439 2 16.8208 2 16.2987V6.2168" stroke="black" stroke-linecap="square" stroke-linejoin="round"/>
					</svg>
				`,
		'large': ''
	}
}
export const rccIconQuestion: IconConfig = {
	name: 'question',
	iconVariants: {
		'small': '',
		'medium': `
					<svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M9 17C13.4182 17 17 13.4182 17 9C17 4.58172 13.4182 1 9 1C4.58172 1 1 4.58172 1 9C1 13.4182 4.58172 17 9 17Z" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
						<path d="M6.6001 6.60001C6.6001 3.79998 11.0001 3.80001 11.0001 6.60001C11.0001 8.60001 9.0001 8.19993 9.0001 10.5999" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
						<path d="M9 13.8081L9.008 13.7992" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
					</svg>
				`,
		'large': ''
	}
}

export const rccIconReceiveQuestion: IconConfig = {
	name: 'receive_question',
	iconVariants: {
		'small': '',
		'medium': `
					<svg width="54" height="54" viewBox="0 0 54 54" fill="none" xmlns="http://www.w3.org/2000/svg">
						<g clip-path="url(#clip0_10296_1034)">
							<path d="M53.5 27V53.5H0.5V0.5H27H38H53.5V16V27Z" stroke="#0E0E0E"/>
							<line x1="26" y1="30.5" x2="42" y2="30.5" stroke="#0E0E0E"/>
							<line x1="26" y1="18.5" x2="42" y2="18.5" stroke="#0E0E0E"/>
							<line x1="26" y1="42.5" x2="42" y2="42.5" stroke="#0E0E0E"/>
							<circle cx="14" cy="30" r="3.5" stroke="#0E0E0E"/>
							<circle cx="14" cy="42" r="3.5" stroke="#0E0E0E"/>
							<path d="M8 15.5C11.5 15.5 13.5 15.5 17 15.5C17.1615 15.5 21 15.5 21 11.5C21 7 17.2976 7 17 7C14 7 12 7 9 7" stroke="#0E0E0E" stroke-linecap="square"/>
							<path d="M10.5 12C9.1332 13.3668 8.3668 14.1332 7 15.5C8.3668 16.8668 9.1332 17.6332 10.5 19" stroke="#0E0E0E" stroke-linecap="square"/>
						</g>
						<defs>
							<clipPath id="clip0_10296_1034">
							<rect width="54" height="54" fill="white"/>
							</clipPath>
						</defs>
					</svg>

				`,
		'large': ''
	}
}
export const rccIconSheet: IconConfig = {
	name: 'sheet',
	iconVariants: {
		'small': '',
		'medium': `
					<svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M15.2222 1H2.77778C1.79594 1 1 1.79594 1 2.77778V15.2222C1 16.2041 1.79594 17 2.77778 17H15.2222C16.2041 17 17 16.2041 17 15.2222V2.77778C17 1.79594 16.2041 1 15.2222 1Z" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
						<path d="M4 5H14" stroke="black" stroke-linecap="square" stroke-linejoin="round"/>
						<path d="M4 9H14" stroke="black" stroke-linecap="square" stroke-linejoin="round"/>
						<path d="M4 13H10" stroke="black" stroke-linecap="square" stroke-linejoin="round"/>
					</svg>
				`,
		'large': ''
	}
}

export const rccIconSettings: IconConfig = {
	name: 'settings',
	iconVariants: {
		'small': '',
		'medium': `
					<svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M9 11C10.1046 11 11 10.1046 11 9C11 7.8954 10.1046 7 9 7C7.8954 7 7 7.8954 7 9C7 10.1046 7.8954 11 9 11Z" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
						<path d="M15.0979 7.71632L14.2198 5.59584L15.4 4.2L13.8 2.6L12.4118 3.78636L10.2462 2.89579L9.74824 1H8.1848L7.67928 2.9209L5.56353 3.81277L4.2 2.6L2.6 4.2L3.7627 5.63108L2.898 7.75704L1 8.2V9.8L2.92089 10.3244L3.8126 12.4398L2.6 13.8L4.2 15.4L5.63293 14.2322L7.7176 15.0898L8.2 17H9.8L10.2836 15.0906L12.4041 14.2124C12.7575 14.465 13.8 15.4 13.8 15.4L15.4 13.8L14.2127 12.3995L15.0911 10.2784L16.9999 9.78176L17 8.2L15.0979 7.71632Z" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
					</svg>
				`,
		'large': ''
	}
}

export const rccIconBackup: IconConfig = {
	name: 'backup',
	iconVariants: {
		'small': '',
		'medium': `
					<svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M1 3.52637V8.579C1 8.579 1 11.1053 6.34307 11.1053C6.79532 11.1053 7.20934 11.0872 7.58824 11.0541" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
						<path d="M14.1763 3.52637V8.579" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
						<path d="M7.58824 1C14.1765 1 14.1765 3.52632 14.1765 3.52632C14.1765 3.52632 14.1765 6.05263 7.58824 6.05263C1 6.05263 1 3.52632 1 3.52632C1 3.52632 1 1 7.58824 1Z" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
						<path d="M7.58824 16.1579C1 16.1579 1 13.6316 1 13.6316V8.57898" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
						<path d="M16.6076 11.386C15.8796 9.73372 14.2171 8.57898 12.2826 8.57898C10.214 8.57898 8.45621 9.89951 7.81985 11.7369" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
						<path d="M14.6411 11.3859H16.6073C16.8242 11.3859 16.9999 11.2286 16.9999 11.0346V9.04675" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
						<path d="M7.98047 14.1929C8.70847 15.8452 10.3709 16.9999 12.3055 16.9999C14.3741 16.9999 16.1319 15.6794 16.7682 13.842" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
						<path d="M9.9472 14.1923H7.98097C7.76414 14.1923 7.58838 14.3495 7.58838 14.5435V16.5314" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
					</svg>
				`,
		'large': ''
	}
}

export const rccIconBackupDownload: IconConfig = {
	name: 'backup_download',
	iconVariants: {
		small: '',
		medium: `
			<svg width="56" height="56" viewBox="0 0 56 56" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M1 10V27.9998C1 27.9998 1 36.9997 22.897 36.9997C24.7505 36.9997 26.4472 36.9352 28 36.8173" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M54.9971 10V27.9998" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M28 1C55 1 55 9.99991 55 9.99991C55 9.99991 55 18.9998 28 18.9998C1 18.9998 1 9.99991 1 9.99991C1 9.99991 1 1 28 1Z" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M28 55.0002C1 55.0002 1 46.0003 1 46.0003V28.0005" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M42.5 32V55M42.5 55L54 43.5M42.5 55L31 43.5" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
			</svg>
		`,
		large: ''
	}
}

export const rccIconBackupRestore: IconConfig = {
	name: 'backup_restore',
	iconVariants: {
		'small': '',
		'medium': `
			<svg width="56" height="56" viewBox="0 0 56 56" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M53.4107 38.2306C51.5543 33.5215 47.315 30.2305 42.3819 30.2305C37.107 30.2305 32.6247 33.994 31.002 39.2305" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M48.3975 38.2301H53.4114C53.9643 38.2301 54.4125 37.7819 54.4125 37.229V31.5635" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M31.4131 46.2304C33.2695 50.9395 37.5088 54.2305 42.442 54.2305C47.7169 54.2305 52.1992 50.467 53.8219 45.2305" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M36.4281 46.2285H31.4142C30.8613 46.2285 30.4131 46.6767 30.4131 47.2296V52.8951" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M1 10V27.9998C1 27.9998 1 36.9997 22.897 36.9997C24.7505 36.9997 26.4472 36.9352 28 36.8173" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M54.9971 10V27.9998" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M28 1C55 1 55 9.99991 55 9.99991C55 9.99991 55 18.9998 28 18.9998C1 18.9998 1 9.99991 1 9.99991C1 9.99991 1 1 28 1Z" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M28 55.0002C1 55.0002 1 46.0003 1 46.0003V28.0005" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
			</svg>
		`,
		'large': ''
	}
}

export const rccIconBackupTransfer: IconConfig = {
	name: 'backup_transfer',
	iconVariants: {
		small: '',
		medium: `
			<svg width="56" height="56" viewBox="0 0 56 56" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M1 10V27.9998C1 27.9998 1 36.9997 22.897 36.9997C24.7505 36.9997 26.4472 36.9352 28 36.8173" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M54.9961 10V27.9998" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M28 1C55 1 55 9.99991 55 9.99991C55 9.99991 55 18.9998 28 18.9998C1 18.9998 1 9.99991 1 9.99991C1 9.99991 1 1 28 1Z" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M28 54.9997C1 54.9997 1 45.9998 1 45.9998V28" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				<path d="M30 43L54 43M54 43L42 31M54 43L42 55" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
			</svg>
		`,
		large: ''
	}
}

export const rccIconAnswerQuestion: IconConfig = {
	name: 'answer_question',
	iconVariants: {
		'small': '',
		'medium': `
					<svg width="54" height="54" viewBox="0 0 54 54" fill="none" stroke = "#000" xmlns="http://www.w3.org/2000/svg">
						<rect x="0.5" y="0.5" height="53.5" width="53.5" stroke="#0E0E0E"/>
						<line x1="11" y1="7.5" x2="43" y2="7.5" stroke="#0E0E0E"/>
						<line x1="26" y1="30.5" x2="42" y2="30.5" stroke="#0E0E0E"/>
						<line x1="26" y1="18.5" x2="42" y2="18.5" stroke="#0E0E0E"/>
						<line x1="26" y1="42.5" x2="42" y2="42.5" stroke="#0E0E0E"/>
						<circle cx="14" cy="30" r="3.5" stroke="#0E0E0E"/>
						<circle cx="14" cy="18" r="3.5" stroke="#0E0E0E"/>
						<circle cx="14" cy="42" r="3.5" stroke="#0E0E0E"/>
					</svg>
				`,
		'large': ''
	}
}

export const rccIconShareData: IconConfig = {
	name: 'share_data',
	iconVariants: {
		'small': '',
		'medium': '',
		'large': `
					<svg width="54" height="54" viewBox="0 0 54 54" fill="none" stroke = "#000" xmlns="http://www.w3.org/2000/svg">
						<path id="page" d="M37.5 0.5L53.5 16.5V53.5H0.5V0.5Z" stroke="#0E0E0E"/>
						<path d="M9.5 25.5H12.5V48.5H9.5V25.5Z" stroke="#0E0E0E"/>
						<path d="M25.5 22.5H28.5V48.5H25.5V22.5Z" stroke="#0E0E0E"/>
						<path d="M41.5 17.5H44.5V48.5H41.5V17.5Z" stroke="#0E0E0E"/>
						<path d="M17.5 28.5H20.5V48.5H17.5V28.5Z" stroke="#0E0E0E"/>
						<path d="M33.5 25.5H36.5V48.5H33.5V25.5Z" stroke="#0E0E0E"/>
						<path d="M22 11.5C18.5 11.5 16.5 11.5 13 11.5C12.8385 11.5 9 11.5 9 15.5C9 20 12.7024 20 13 20C16 20 18 20 21 20" stroke="#0E0E0E" stroke-linecap="square"/>
						<path d="M19.5 15C20.8668 13.6332 21.6332 12.8668 23 11.5C21.6332 10.1332 20.8668 9.36683 19.5 8" stroke="#0E0E0E" stroke-linecap="square"/>
					</svg>
				`
	}
}

export const rccIconReceiveData: IconConfig = {
	name: 'receive_data',
	iconVariants: {
		'small': '',
		'medium': '',
		'large': `
		<svg width="54" height="54" viewBox="0 0 54 54" fill="none" xmlns="http://www.w3.org/2000/svg">
			<g clip-path="url(#clip0_4141_53062)">
				<path d="M53.5 27V53.5H0.5V0.5H27H37.7929L45.6464 8.35355L53.5 16.2071V27Z" stroke="#0E0E0E"/>
				<path d="M9.5 25.5H12.5V48.5H9.5V25.5Z" stroke="#0E0E0E"/>
				<path d="M25.5 22.5H28.5V48.5H25.5V22.5Z" stroke="#0E0E0E"/>
				<path d="M41.5 17.5H44.5V48.5H41.5V17.5Z" stroke="#0E0E0E"/>
				<path d="M17.5 28.5H20.5V48.5H17.5V28.5Z" stroke="#0E0E0E"/>
				<path d="M33.5 25.5H36.5V48.5H33.5V25.5Z" stroke="#0E0E0E"/>
				<path d="M10 16.5C13.5 16.5 15.5 16.5 19 16.5C19.1615 16.5 23 16.5 23 12.5C23 8 19.2976 8 19 8C16 8 14 8 11 8" stroke="#0E0E0E" stroke-linecap="square"/>
				<path d="M12.5 13C11.1332 14.3668 10.3668 15.1332 9 16.5C10.3668 17.8668 11.1332 18.6332 12.5 20" stroke="#0E0E0E" stroke-linecap="square"/>
			</g>
			<defs>
			<clipPath id="clip0_4141_53062">
				<rect width="54" height="54" fill="white"/>
			</clipPath>
			</defs>
		</svg>`
	}
}

export const rccIconChevron: IconConfig = {
	name: 'chevron',
	iconVariants: {
		small: '',
		medium: `
				<svg width="22" height="14" viewBox="0 0 22 14" fill="none" xmlns="http://www.w3.org/2000/svg">
					<line x1="2.82843" y1="2.59375" x2="11.3137" y2="11.079" stroke="#fff" stroke-width="4" stroke-linecap="round"/>
					<line x1="11.3135" y1="11.0778" x2="19.7988" y2="2.59254" stroke="#fff" stroke-width="4" stroke-linecap="round"/>
				</svg>
				`,
		large: '',
	}
}


export const rccIconChevronThin: IconConfig = {
	name: 'chevron_thin',
	iconVariants: {
		small: '',
		medium: `
		<svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path d="M1 16.6547L9.00019 8.74257L1 0.830078" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
		</svg>
		`,
		large: '',
	}
}

export const rccIconConfirm: IconConfig = {
	name: 'confirm',
	iconVariants: {
		small: '',
		medium: `
				<svg width="54" height="54" viewBox="0 0 54 54" fill="none" xmlns="http://www.w3.org/2000/svg">
					<rect width="54" height="54" rx="27" fill="none"/>
					<path fill-rule="evenodd" clip-rule="evenodd" d="M14.4144 24.383C13.6334 25.164 13.6334 26.4303 14.4144 27.2114L22.8997 35.6967C23.6807 36.4777 24.9471 36.4777 25.7281 35.6967V35.6967C26.5092 34.9156 26.5092 33.6493 25.7281 32.8682L17.2428 24.383C16.4618 23.6019 15.1954 23.6019 14.4144 24.383V24.383Z" fill="#FDFDFD"/>
					<path fill-rule="evenodd" clip-rule="evenodd" d="M23.4147 32.5545C22.6337 33.3356 22.6337 34.6019 23.4147 35.383V35.383C24.1958 36.164 25.4621 36.164 26.2431 35.383L40.3853 21.2408C41.1663 20.4598 41.1663 19.1934 40.3853 18.4124V18.4124C39.6042 17.6314 38.3379 17.6314 37.5568 18.4124L23.4147 32.5545Z" fill="#FDFDFD"/>
				</svg>
				`,
		large: '',
	}
}
export const rccIconDeleteAll: IconConfig = {
	name: 'delete_all',
	iconVariants: {
		small: '',
		medium: `
				<svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path d="M1 3.53259V8.59767C1 8.59767 2 11 8.5 11C8.95225 11 9.36626 10.9819 9.74516 10.9487" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
					<path d="M14.1763 3.53259V10" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
					<path d="M7.58824 1C14.1765 1 14.1765 3.53254 14.1765 3.53254C14.1765 3.53254 14.1765 6.06508 7.58824 6.06508C1 6.06508 1 3.53254 1 3.53254C1 3.53254 1 1 7.58824 1Z" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
					<path d="M9 16C2 16.5 1 13.6627 1 13.6627V8.59766" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
					<path d="M8 17L11.9999 13M11.9999 13L16 9M11.9999 13L8 9M11.9999 13L16 17" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
				</svg>

				`,
		large: '',
	}
}

export const rccIconChangeDevice: IconConfig = {
	name: 'change_device',
	iconVariants: {
		small: '',
		medium: `
				<svg width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path d="M15 5.66667C13.87 2.9197 11.2894 1 8.28653 1C4.48947 1 1.3674 4.06964 1 8" stroke="#FDFDFD" stroke-linecap="round" stroke-linejoin="round"/>
					<path d="M12 6H15.52C15.7851 6 16 5.7851 16 5.52V2" stroke="#FDFDFD" stroke-linecap="round" stroke-linejoin="round"/>
					<path d="M2 11.3333C3.12999 14.0803 5.71064 16 8.71344 16C12.5105 16 15.6326 12.9303 16 9" stroke="#FDFDFD" stroke-linecap="round" stroke-linejoin="round"/>
					<path d="M5 11H1.48C1.2149 11 1 11.2149 1 11.48V15" stroke="#FDFDFD" stroke-linecap="round" stroke-linejoin="round"/>
				</svg>

				`,
		large: '',
	}
}

export const rccIconTransmissionLoading: IconConfig = {
	name: 'transmission_loading',
	iconVariants: {
		small: '',
		medium: `
				<svg width="54" height="54" viewBox="0 0 54 54" fill="none" xmlns="http://www.w3.org/2000/svg">
					<style type="text/css">
					g#arrow {
						animation-name: spin;
						animation-duration: 2s;
						animation-iteration-count: infinite;
						transform-origin: center;
						animation-timing-function: linear;
					}

					@keyframes spin {
						0% {
						transform: rotate(0deg);
						}

						100% {
						transform: rotate(360deg);
						}
					}
					</style>
					<path id="page" d="M53.5 27V53.5H0.5V0.5H27H37.7929L53.5 16.2071V27Z" stroke="#0E0E0E"/>
					<g id="arrow">
						<path id="arrow-line" d="M41.6651 25.5999C39.1899 19.9491 33.5374 16 26.9602 16C18.6432 16 11.8047 22.3146 11 30.3999" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
						<path id="arrow-head" d="M34 25.9999H41.0573C41.5889 25.9999 42.0197 25.5701 42.0197 25.0399V18" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
					</g>
				</svg>
				`,
		large: '',
	}
}


export const rccIconTransmissionComplete: IconConfig = {
	name: 'transmission_complete',
	iconVariants: {
		small: '',
		medium: `
				<svg width="56" height="56" viewBox="0 0 56 56" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path d="M28 55C42.9116 55 55 42.9116 55 28C55 13.0883 42.9116 1 28 1C13.0883 1 1 13.0883 1 28C1 42.9116 13.0883 55 28 55Z" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
					<path d="M40.9199 36.9999C40.9199 36.9999 36.5999 42.3999 27.9599 42.3999C19.3199 42.3999 14.9999 36.9999 14.9999 36.9999" stroke="#0E0E0E" stroke-linecap="square" stroke-linejoin="round"/>
					<path d="M18 21C17.4477 21 17 20.1046 17 19C17 17.8954 17.4477 17 18 17C18.5523 17 19 17.8954 19 19C19 20.1046 18.5523 21 18 21Z" fill="#0E0E0E" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
					<path d="M36.3599 21C35.8077 21 35.3599 20.1046 35.3599 19C35.3599 17.8954 35.8077 17 36.3599 17C36.9121 17 37.3599 17.8954 37.3599 19C37.3599 20.1046 36.9121 21 36.3599 21Z" fill="#0E0E0E" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				</svg>
				`,
		large: '',
	}
}

export const rccIconTrash: IconConfig = {
	name: 'trash',
	iconVariants: {
		small: '',
		medium: `
				<svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path d="M16 7L14.2544 15.7279C14.1073 16.4635 13.3804 17 12.5309 17H5.46915C4.61957 17 3.89271 16.4635 3.74559 15.7279L2 7" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
					<path d="M16 5H14.25H11.625M11.625 5V3C11.625 1.89543 10.9286 1 10.0694 1H7.93056C7.07145 1 6.375 1.89543 6.375 3V5M11.625 5H9H6.375M2 5H6.375" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
					<rect x="1" y="5" width="16" height="2" stroke="#0E0E0E" stroke-linejoin="round"/>
					<path d="M9 9L9 15" stroke="#0E0E0E" stroke-linecap="round"/>
					<path d="M12 9L12 15" stroke="#0E0E0E" stroke-linecap="round"/>
					<path d="M6 9L6 15" stroke="#0E0E0E" stroke-linecap="round"/>
				</svg>
				`,
		large: ''
	}
}

export const rccIconDownload: IconConfig = {
	name: 'download',
	iconVariants: {
		small: '',
		medium: `
				<svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path d="M17 16.8555L17 8.94317" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
					<path d="M1 16.8555H17" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
					<path d="M1 16.8555L1 8.94317" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
					<path d="M9 1.03125V12.8997M9 12.8997L13 9.43807M9 12.8997L5 9.43807" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				</svg>`,
		large: ''
	}
}

export const rccSmileyAnimated: IconConfig = {
	name: 'smiley_animated',
	iconVariants: {
		small: '',
		medium: `
		<svg width="56" height="56" viewBox="0 0 56 56" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path id="head" d="M28 55C42.9116 55 55 42.9116 55 28C55 13.0883 42.9116 1 28 1C13.0883 1 1 13.0883 1 28C1 42.9116 13.0883 55 28 55Z" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
			<g id="eyes">
			<path d="M18 21.2C17.4477 21.2 17 20.3045 17 19.2C17 18.0954 17.4477 17.2 18 17.2C18.5523 17.2 19 18.0954 19 19.2C19 20.3045 18.5523 21.2 18 21.2Z" fill="#0E0E0E" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
			<path d="M36.36 21.2C35.8078 21.2 35.36 20.3045 35.36 19.2C35.36 18.0954 35.8078 17.2 36.36 17.2C36.9122 17.2 37.36 18.0954 37.36 19.2C37.36 20.3045 36.9122 21.2 36.36 21.2Z" fill="#0E0E0E" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
			</g>
			<path id="mouth" d="M 25 38 C 25 38, 26 38, 28 38 C 30 38, 31 38, 31 38" stroke="#0E0E0E" stroke-linecap="square" stroke-linejoin="round">
			<animate
				attributeName="d"
				values="M 25 38 C 25 38, 26 38, 28 38 C 30 38, 31 38, 31 38;M23 37 C23 37 24.6667  40 28 40 C 31.3333 40 33 37 33 37"
				dur="3s"
				repeatCount="indefinite"
			/>
			</path>
		</svg>
		`,
		large: ''
	}
}

export const rccSmiley: IconConfig = {
	name: 'smiley',
	iconVariants: {
		small: '',
		medium: `
		<svg width="56" height="56" viewBox="0 0 56 56" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path d="M28 55C42.9116 55 55 42.9116 55 28C55 13.0883 42.9116 1 28 1C13.0883 1 1 13.0883 1 28C1 42.9116 13.0883 55 28 55Z" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
			<path d="M40.9199 36.9999C40.9199 36.9999 36.5999 42.3999 27.9599 42.3999C19.3199 42.3999 14.9999 36.9999 14.9999 36.9999" stroke="#0E0E0E" stroke-linecap="square" stroke-linejoin="round"/>
			<path d="M18 21C17.4477 21 17 20.1046 17 19C17 17.8954 17.4477 17 18 17C18.5523 17 19 17.8954 19 19C19 20.1046 18.5523 21 18 21Z" fill="#0E0E0E" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
			<path d="M36.3599 21C35.8077 21 35.3599 20.1046 35.3599 19C35.3599 17.8954 35.8077 17 36.3599 17C36.9121 17 37.3599 17.8954 37.3599 19C37.3599 20.1046 36.9121 21 36.3599 21Z" fill="#0E0E0E" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
		</svg>
		`,
		large: ''
	}
}
export const rccHamburger: IconConfig = {
	name: 'hamburger',
	iconVariants: {
		small: '',
		medium: `
			<svg width="32" height="24" viewBox="0 0 32 24" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path fill-rule="evenodd" clip-rule="evenodd" d="M0 22C0 23.1046 0.895431 24 2 24H30C31.1046 24 32 23.1046 32 22C32 20.8954 31.1046 20 30 20H2C0.89543 20 0 20.8954 0 22Z" />
				<path fill-rule="evenodd" clip-rule="evenodd" d="M0 12C0 13.1046 0.89543 14 2 14H30C31.1046 14 32 13.1046 32 12C32 10.8954 31.1046 10 30 10H2C0.895431 10 0 10.8954 0 12Z" />
				<path fill-rule="evenodd" clip-rule="evenodd" d="M2 0C0.895431 0 0 0.89543 0 2C0 3.10457 0.895431 4 2 4H30C31.1046 4 32 3.10457 32 2C32 0.89543 31.1046 0 30 0H2Z" />
			</svg>

		`,
		large: ''
	}
}
export const rccCompass: IconConfig = {
	name: 'compass',
	iconVariants: {
		small: '',
		medium: `
				<svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path d="M7.86826 7.86893L12.9594 5.04053L10.131 10.1317M7.86826 7.86893L5.03979 12.9601L10.131 10.1317M7.86826 7.86893L10.131 10.1317" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
					<path d="M9 17C13.4182 17 17 13.4182 17 9C17 4.58172 13.4182 1 9 1C4.58172 1 1 4.58172 1 9C1 13.4182 4.58172 17 9 17Z" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				</svg>
		`,
		large: ''
	}
}
export const rccGlossary: IconConfig = {
	name: 'glossary',
	iconVariants: {
		small: '',
		medium: `
				<svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path d="M6.33331 8.9987H8.99998M8.99998 8.9987H11.6666M8.99998 8.9987V6.33203M8.99998 8.9987V11.6654" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
					<path d="M17 1.53333V16.4667C17 16.7612 16.7612 17 16.4667 17H1.53333C1.23878 17 1 16.7612 1 16.4667V1.53333C1 1.23878 1.23878 1 1.53333 1H16.4667C16.7612 1 17 1.23878 17 1.53333Z" stroke="#0E0E0E" stroke-linecap="round" stroke-linejoin="round"/>
				</svg>

		`,
		large: ''
	}
}

export const rccManual: IconConfig = {
	name: 'manual',
	iconVariants: {
		small: '',
		medium: `<svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path fill-rule="evenodd" clip-rule="evenodd" d="M20.636 14.5396V15.5843H22.2505V21.0904H20.6309V22.1068H27.6583V21.0904H26.0491V15.1924V14.5396L20.636 14.5396Z" fill="black"/>
					<path d="M24.1845 13.367C25.2334 13.367 26.0838 12.5166 26.0838 11.4677C26.0838 10.4187 25.2334 9.56836 24.1845 9.56836C23.1355 9.56836 22.2852 10.4187 22.2852 11.4677C22.2852 12.5166 23.1355 13.367 24.1845 13.367Z" fill="black"/>
					<path fill-rule="evenodd" clip-rule="evenodd" d="M13.0846 4.00153C12.7689 4.00096 12.4709 4.0192 12.2128 4.05015C11.4164 4.14588 10.7437 4.40704 10.269 4.64445C9.78868 4.88471 9.16001 5.28091 8.84605 5.43798C8.65365 5.53428 8.08329 5.82905 7.38567 6.06855C6.66944 6.31452 5.80601 6.47007 5.32244 6.52192C4.28808 6.63265 4.0708 6.70615 2.16085 6.45032C1.71546 6.39068 1.32382 6.27691 0.949656 6.15763C0.788215 6.10616 0.629052 6.05393 0.471789 6.00398C0.306359 5.95137 0.150236 5.89857 0 5.84596V26.9207C0.0619176 26.9412 0.120796 26.9618 0.184803 26.9819C0.412341 27.0541 0.668178 27.1412 0.949656 27.2244C1.27273 27.3198 1.63056 27.41 2.03473 27.4641C3.98286 27.7251 4.41419 27.6468 5.42349 27.5388C5.98246 27.4789 6.87703 27.32 7.69412 27.0394C8.46847 26.7735 9.10569 26.4427 9.27074 26.3602C9.68668 26.1523 10.2986 25.764 10.6941 25.5663C11.0952 25.3658 11.6722 25.1445 12.3267 25.0659C12.8912 24.9981 13.8133 25.0132 14.479 25.1823C14.5508 25.2005 14.6635 25.2398 14.7907 25.2869C14.9991 25.3641 15.2589 25.4689 15.5341 25.5867C15.6841 25.6509 15.8329 25.7162 15.9804 25.7825C16.1457 25.7084 16.3159 25.6347 16.4838 25.5633C16.7381 25.4552 16.9761 25.3593 17.17 25.2873C17.2978 25.2398 17.4102 25.2007 17.4824 25.1823C18.1481 25.0132 19.0706 24.9981 19.6351 25.0659C20.2896 25.1445 20.8666 25.3658 21.2677 25.5663C21.6632 25.764 22.275 26.1523 22.6907 26.3602C22.8558 26.4429 23.493 26.7735 24.2673 27.0394C25.0844 27.32 25.979 27.4789 26.538 27.5388C27.5471 27.6468 27.9786 27.7253 29.9267 27.4641C30.3476 27.4077 30.7172 27.3124 31.0503 27.2127C31.3159 27.1331 31.5599 27.0506 31.7766 26.9819C31.8539 26.9574 31.9255 26.9325 32 26.9076V5.83095C31.838 5.88812 31.6697 5.94548 31.4895 6.00265C31.3447 6.04861 31.1985 6.09647 31.0503 6.14396C30.6646 6.26779 30.2612 6.38726 29.8006 6.4488C27.8906 6.70463 27.6736 6.63113 26.6392 6.5204C26.1556 6.46855 25.2918 6.313 24.5756 6.06703C23.8778 5.82753 23.308 5.53257 23.1154 5.43646C22.8014 5.27958 22.1724 4.88319 21.692 4.64293C21.2174 4.40552 20.5447 4.14436 19.7483 4.04864C19.4902 4.01768 19.1924 3.99944 18.8765 4.00001C18.7713 4.0002 18.664 4.00248 18.5557 4.00704C18.1219 4.02546 17.6691 4.08092 17.2488 4.18766C16.9732 4.25756 16.7121 4.35063 16.4839 4.44293C16.2822 4.5246 16.1098 4.60494 15.9795 4.66914C15.8636 4.61159 15.7085 4.54018 15.5343 4.46743C15.2963 4.36791 15.0184 4.26535 14.713 4.18766C14.2927 4.08092 13.8399 4.02546 13.4061 4.00704C13.2977 4.00248 13.1905 4.0002 13.0853 4.00001L13.0846 4.00153ZM13.2211 4.95784C13.6485 4.96809 14.1041 5.01444 14.4787 5.10959C14.8952 5.21538 15.2914 5.39335 15.5337 5.51168V24.5561C15.2312 24.4319 14.9419 24.3213 14.7125 24.263C14.2921 24.1563 13.8393 24.1008 13.4055 24.0824C13.2971 24.0778 13.19 24.0756 13.0847 24.0754C12.7691 24.0748 12.4711 24.0928 12.213 24.124C11.4166 24.2197 10.7438 24.4809 10.2692 24.7183C9.78887 24.9585 9.15982 25.3547 8.84586 25.5118C8.65346 25.6081 8.08348 25.9025 7.38567 26.142C6.66944 26.388 5.80601 26.5439 5.32244 26.5958C4.28808 26.7065 4.0708 26.78 2.16085 26.5242C1.71546 26.4645 1.32382 26.3507 0.949656 26.2315V7.15097C1.27273 7.24632 1.63056 7.33654 2.03473 7.39067C3.98286 7.65163 4.41419 7.57338 5.42349 7.46531C5.98246 7.40548 6.87703 7.24651 7.69412 6.96598C8.46847 6.70008 9.10569 6.36922 9.27074 6.28679C9.68668 6.07881 10.2986 5.69097 10.6941 5.49325C11.0952 5.29269 11.6722 5.07104 12.3267 4.9924C12.4678 4.9755 12.6312 4.96372 12.8074 4.95822C12.9396 4.95404 13.079 4.95366 13.2215 4.95708L13.2211 4.95784ZM19.1538 4.95898C19.3301 4.96448 19.4934 4.97607 19.6345 4.99316C20.289 5.0718 20.866 5.29345 21.2672 5.49401C21.6626 5.69173 22.2744 6.07957 22.6901 6.28754C22.8552 6.37017 23.4924 6.70084 24.2668 6.96674C25.0838 7.24727 25.9784 7.40624 26.5374 7.46607C27.5465 7.57414 27.978 7.65239 29.9261 7.39143C30.347 7.33502 30.7166 7.23967 31.0498 7.13996V26.2186C30.664 26.3424 30.2606 26.4619 29.8 26.5234C27.8901 26.7792 27.673 26.7057 26.6386 26.595C26.1551 26.5431 25.2912 26.3872 24.575 26.1413C23.8772 25.9017 23.3074 25.6072 23.1148 25.5111C22.8009 25.3542 22.1718 24.9578 21.6915 24.7175C21.2168 24.4801 20.5441 24.219 19.7477 24.1232C19.4896 24.0923 19.1918 24.074 18.8759 24.0746C18.7707 24.0748 18.6634 24.0771 18.5551 24.0816C18.1213 24.1001 17.6685 24.1555 17.2482 24.2623C17.0321 24.3172 16.7662 24.4144 16.4834 24.5289V5.48452C16.7307 5.36657 17.0967 5.20722 17.482 5.1094C17.9814 4.98272 18.6252 4.94245 19.154 4.95879L19.1538 4.95898Z" fill="black"/>
				</svg>
		`,
		large: ''
	}
}

export const rccCE: IconConfig = {
	name: 'ce',
	iconVariants: {
		small: '',
		medium: `<svg width="33" height="32" viewBox="0 0 33 32" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path fill-rule="evenodd" clip-rule="evenodd" d="M12.5719 26.9515C12.1922 26.99 11.8107 27.0092 11.429 27.0092C8.39787 27.0092 5.49085 25.7971 3.34749 23.6396C1.20413 21.4821 0 18.5558 0 15.5046C0 12.4534 1.20413 9.52715 3.34749 7.36962C5.49085 5.21209 8.39787 4 11.429 4C11.8107 4 12.1922 4.01925 12.5719 4.05767V7.50905C12.1928 7.46239 11.8109 7.44312 11.429 7.45138C9.30722 7.45138 7.27231 8.29984 5.77195 9.81011C4.2716 11.3204 3.42871 13.3688 3.42871 15.5046C3.42871 17.6404 4.2716 19.6888 5.77195 21.1991C7.27231 22.7094 9.30722 23.5578 11.429 23.5578C11.8109 23.5661 12.1928 23.5468 12.5719 23.5002V26.9515Z" fill="black"/>
					<path fill-rule="evenodd" clip-rule="evenodd" d="M32.0016 26.9515C31.6219 26.99 31.2404 27.0092 30.8587 27.0092C27.8276 27.0092 24.9205 25.7971 22.7772 23.6396C20.6338 21.4821 19.4297 18.5558 19.4297 15.5046C19.4297 12.4534 20.6338 9.52715 22.7772 7.36962C24.9205 5.21209 27.8276 4 30.8587 4C31.2404 4 31.6219 4.01925 32.0016 4.05767V7.50905C31.6225 7.46239 31.2406 7.44312 30.8587 7.45138C29.0346 7.45123 27.2652 8.07856 25.8439 9.22938C24.4225 10.3802 23.4345 11.9854 23.0436 13.7789H29.7158V17.2303H23.0436C23.4345 19.0238 24.4225 20.629 25.8439 21.7798C27.2652 22.9306 29.0346 23.558 30.8587 23.5578C31.2406 23.5661 31.6225 23.5468 32.0016 23.5002V26.9515Z" fill="black"/>
				</svg>
		`,
		large: ''
	}
}

export const rccManufacturer: IconConfig = {
	name: 'manufacturer',
	iconVariants: {
		small: '',
		medium: `<svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path d="M0 19.9118L4.84432 11.5212L8.57652 17.9856L12.3087 11.5212L16.0409 17.9856L19.7731 11.5212L23.5237 18.0175V1H32V30.6193H0.000449708L0 19.9118Z" fill="black"/>
				</svg>
		`,
		large: ''
	}
}
export const rccMenuDots: IconConfig = {
	name: 'menu_dots',
	iconVariants: {
		small: '',
		medium: `
				<svg width="28" height="4" viewBox="0 0 28 4" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path fill-rule="evenodd" clip-rule="evenodd" d="M-8.74228e-08 2.00195C-3.91405e-08 3.10652 0.895431 4.00195 2 4.00195C3.10457 4.00195 4 3.10652 4 2.00195C4 0.897384 3.10457 0.00195299 2 0.00195304C0.89543 0.00195309 -1.35705e-07 0.897384 -8.74228e-08 2.00195Z" fill="none"/>
				<path fill-rule="evenodd" clip-rule="evenodd" d="M14 4C15.1046 4 16 3.10457 16 2C16 0.89543 15.1046 -1.35705e-07 14 -8.74228e-08C12.8954 -3.91405e-08 12 0.895431 12 2C12 3.10457 12.8954 4 14 4Z" fill="none"/>
				<path fill-rule="evenodd" clip-rule="evenodd" d="M26 4.00195C27.1046 4.00195 28 3.10652 28 2.00195C28 0.897384 27.1046 0.00195299 26 0.00195304C24.8954 0.00195309 24 0.897384 24 2.00195C24 3.10652 24.8954 4.00195 26 4.00195Z" fill="none"/>
				</svg>
				`,
		large: ''
	}
}

export const rccIconUDI: IconConfig = {
	name: 'UDI',
	iconVariants: {
		medium: `
				<svg width="32" height="27" viewBox="0 0 32 27" fill="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
				<rect width="32" height="26.6667" fill="url(#pattern0)"/>
				<defs>
				<pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
				<use xlink:href="#image0_7329_83254" transform="matrix(0.00555556 0 0 0.00666667 -0.0777778 0)"/>
				</pattern>
				<image id="image0_7329_83254" width="208" height="150" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANAAAACWCAYAAACvpQYFAAAACXBIWXMAABYlAAAWJQFJUiTwAAAa70lEQVR4nO1dB5RURbNmSYsgCJgQxcSvRAMmZImCigHFo+Iv6iNIVBEwHlRQSSpGzBETJkzwDGA6IvpEQBGVDIooUWWXxQU2T73z1U5depp7Z+7OnZ0ZlvrO6TNhd+7t7ltfV3V1dXUVUigUcaNK/D9VKBRKIIUiAJRACkUAKIEUigBQAikUAaAEUigCQAmkUASAEkihCAAlkEIRAEogxV6JkpISKi0tdT4XFxfzd2lBoFAoxEWhSDeIbIIwaUMgqZSSRpHOgHyCNKac2poo6QSyiYPKSKVMYmnRPgiluA9MmRXSmASCFkJJGoGkIiZZUCGplJJISRNK0z4Q2TQ1kshu0glkVky1T+qFQ0soKnEiyEPB5upxE+jff/+lZcuW0ffff0/fffcdzZ8/nxYsWEDz5s3jgu/xGd9r0T6YnyZ9AFmFfEI2Fy9eTNlbcwLNhcpNIFF3S5cupfHjx9PQIUPof666ivr06UP9+vWjvn37cpH3+F6L9kGfNOkDkcurr76axo4dSwsXLoyYelQ4gYSls2fPpg4dOtC+++5LGRkZWrQPKF36oGrVqhGvZqlevTq/1qhRg7LataMPPvggwsGQNAJ99dVX1LFjR6pZsyZVqVKFK1WtWjWuND5r0T6okoI+gAyiiEyiyHv7/9q1a0effPJJ3OQJRKA5c+ZEEAjEAatRMWG/Fu2DqknsA8ge5BFaxu1vJolSTqCvv/6aOnfuTJmZmaptVNvQntYHKScQm3CdOjoEEtV57LHHUu/eval///48YdOifdAviX3Qt0+ZE0scWP2v7k9XXXUVNWvWLMKkSxsC1apVy2E1yDRw4EBatnw55eXlsat727ZtWrQPKFl9kLstl3JzywpkMH9nPv2+di3LJWQ1bQgEL1z7rPYRlYL9OXToUNqwcUNESI8W7YPSJPSBuKKlSFhOdk4OXXvttZRZKzN9CAQnQqdOnSI0EAg0ZMgQ2rhpI/9PEPegQlFemFHWsrYDbM3dSoMGDXJc2GljwoFAMgeCpwNkAtM3b97sNEihSCWZAJhysIzgJU5bAokGAoH+/vtvpxEKRSpgBomCQNdcc40SSKHwCyWQQhEASiCFIgCUQApFACiBFIoAUAIpFAGgBFIoAkAJpFAEgBJIoQgAJZBCEQBKIIUiAJRACkUAKIEUigBQAikUAaAEUigCQAkUhltO42jJws3fJRJyD2zSsuuEnY+FRYURnwsKCnxn+jdPD4iWy7mii3lGjrTDrFvQ/vPKp+71LO1+KU/mUCVQGOi0oqKiiAz6hYWFzsOVTi0p3bUHXh5+IiCkMffcRxCJQkyWHTt3UHFJMdcVnwsK/RMIQJvy8/P5t0Xh9plHcAQtbgOMLaBch4J8p23Sbvu38SAUJoY8TyEr5y4oKeaCvsRzzNu+nfvBbWDxCyUQEXfiN998Q/fffz9NnDiRy7hx4+juu+/mgvfIbyzfT5kyhdauXcu/S9SDxzWQ4eWLL76ge++917nXXXfdxbnBJ0yYwHVBPfD9nXfeSa+88gpt2LDBIXqs6//222/0/PPP05gxY5zrouB6uJ+8D1ImTZpEjz32GD3zzDM0depUmjFjBuf2W7VqFWdPEtKgviCyfMbAgB2dQQekgoICmj59OrdP+kraaX7Ge/TD/37wAeXk5EQkfFcClQPoLDy4++67L2byRkknfMopp3D2fdFOGOmCnD4mo95ff/1FN954Y8y0xZJSFnvtf/rpp6gZ/x2NWlrK5Gzbtq1rCtpEF/RlgwYN6PDDD6cTTzyRzjnnHE60PnrMaHr33Xfpjz/+oO3btzP5Abzu2LEjUD8C2dnZ1KtXr4jn5Vak/cgr+Ouvv0Y8y/Jo9L1eAzGBtufxyFm3bl3OqIKcC0gMIdlV8Cqf0elINfzjjz+6Hp4UF0Jlgr4lO5vGjB5NderU4cQpKBBE1EdSxqIO++yzD9ejW7dutHLlSl+mBwj0f99+S2eccYZDQmmfvHdLkh5PsQccs+CQgFNPPZVGjRrFiTbWrV9f7tPavIDnAAIhwaHc361+aC+epxDo999/dwhU3mepBAoTCGZT7dq1nc6VjrazTOI1KyuLj6tge7o0MU4E1AOmBMw0EERyLZv3lrqIpuzSpQstWbLEeZCxHjTMVKRJluuJgEtbE6F5TCJKFlnJGS05pVEOPPBAOvfcc9mkgjaH9gnqSCgtLaUtW7ZEEMjLkpDBEFpxzZo1u+a45Tx+ZK8nEDpg586dPP8xE9jLq5mB3zSdoIEkqZ7Y8YnSQLUyyxLzCYlMAuG9jJ449uWXX37x/aAlUb9twtmaIyiBTDLa1xYSyXcYtC6++GKeK63fsMGX8IY8+loIdMUVV0Q1U8062QQSE87v89y7CRQW3H/z/qV77rnHSd5oJ8aTDhfBPf3001kDobNNT10QMIG2bGECuc3FTKGU9zDHcHpftD4wvYpy1pIpXF7mV6KKmIem8IqWku/Q72jLo5Mn89woVj+VephZfglkkrx/v360avXquJ0XezeBwr+F+QANJHMLe3Q2Vb8QaNGiRQldu3AINGaMUw/ThLNNELyiX3CCn58+QH/CG+amgSq6mGQ1ByezYP6JudGzzz7Luae9+qkiNBC8k/GuRSmBQiH2BpXHCwcC/fDDDwlxX0eacGUaSEzJWPUAgZYvXx5VqMwHPccngewBROYMfh0Ibt/Zpp3XPY877jiaNm0au/RlPlJQWMCaPpazwS+BzL+BQOJEiAdKICXQboIlpqqXcyGa2WfPd4SA4kn0o7HOPPNMPhIRJIKA5ocXfpVA5WR2skw41UCRcxax4Rs1akQnnXQSa4XWrVvR8ccfz+WEE07gtR0U/K158+Z8XlPT/zSlJk2aUIOGDalG2FEQS/O4lQMOOIDnEtDyJmlC4flPNJlRDaQESqkJB4FHwYB1ySWXcLQDFj7feecdeu+99yIKvn/rrbc44uDFF1/k6AO4pYcMHcq/zcpqR40bN3bMPz/n2oq2atGiBd0zcSJt2rTJifaI1U4lkNUZgGqg5BLIdC/fdtttPC8ru4H7dRFXhmsXFRexVxLmFrQABB8Ojqeeeop69uzJEQleDgS7beKlg4b76KOP2MljxikqgXxACZQaDSTzGkRDwCMoHjFTC8S6H/4C0wvrayDT3LlzOUQJJIJ2iaWJZP1tv/32o9tvv52v4WeBUzWQEijlXjj5Hq700aNHOwSKFq1sbg+QzxJJjvklroH1qttuv52at2xBmTHahyKODDgU5s2bx2SMtd6mBFICpZxAUjAHggnnh0DRFjtFc8ENDXfxfZPuo2OaHRtVC5nxfnBKjB07lqPfY7VTCaQESjsCYQuCn+vHgpAJkQa33nor1a5Tx2mLXRe0X6JC8Pfu3buzFioNlXrOxZRALp0BqBOhchBIrgGTDgulLVq2jGiL+V68gPJ9mzZt2AsIMy5aXVQDKYEqrQYyrwFT7vLLL/cM1pXIbalL48aNeT72zz//KIH8QjVQ5SOQAM4ABO/Wr1/ftR72d5mZmRyxjZg1Uzbc2qgLqUqgSquBTGABFtELonHMODqTSPK5Q3gTY7S6KIGszgB0DlQ5CYQwnbPOOosyLEeCHZQqbW3ZqhV9/PHH6oXzCyVQ5dZAiFTAdmpzDuRWD/m+yWGH0ZNPPsmRDl71UQ1kdQagGqhyEUiug+vedNNNnCPBTz6Fgw46iMaNG0vbtnnXRwmkBKrUXjgTcEkj8BTE8LO5r27dujR8+HBOGqIE8gHVQJWbQAjzefDBB+nII4+MGRuXkZHBgwpMvmiubNVAVmcAasJVLgLJdUAgJGBsxmE91XwR6Morr6TNmzcrgfxACVQ5NZDEyBUXl/D+oeNat4654S4j7I27tNeltH79es/6qAZSAlX6OZCZGB+b8LDj1Q+BMjIy6IILLnCy9iiBfHQ0oCZc5SKQmWZr6quv0mmnnRaTQFXD7UUiRiWQTyiBKqcGEoBAr732Gufq9kug7t2707p16zzroyac1Rl7qwbyW4/KQCAkHYxFoOrhxIxnn302n0bhVR8lkNUZySAQ9pgIgWQTl18CJVKoUI/snGw+lsNvPZDNU3Jj72kEwga7l156KaoJZy+w9ujRQ50I6aqBcDqDX8GF2bFgwQI+pCnhBLrzTtqnVi1fSQERS7Z48WJ/108zAiFJCLxwSJNl5gC3+1vi5KpWUy/cHpPa1y+Bgp5lY7cX5wMhiYbsxoxFIJg0ooH2RAI98sgjdPTRRzv7gqLFw9WoWYMzjuo6UJqezvDQQw9xKie/BJIDthIByTMA4bj55pt9E+i8885zcmPvaQSCCQdzFYdw2cetuLW3Tp06/Mw1lCcNCYSH+fjjj/NDiiZUYqvDbsdZOxhFEwGp/8aNG+mGG0bGdCKI8F900UW0YsWKPYpAZjDpyJEjncHC63QI1k5Vq9EBBx7AZyfZSU7sNuqGOqMzkmXCQZPAHkcesmjCJd4gHPH45Zdfcr7mhGkgIvpz3Tq6ftiwmASSOl522WV8PKGfPkgXAslzhTcNid39DBYZGRmcnQeDHMxtr/oogVJBoPB9Xn31Vd5i7IdASHIxa9Ys2hFOcpEoAuGgp8GDBkWcp+OlfTAy4yQ2/MYP0oVAAngxYYKaZ+lEa2/Lli3p/fffZ2tBd6T6fODJMuGAt6ZN46TmfgiEhOo4PUCEK1FzIJhjffr08UUg/M+AAQPozz//9NUH6UYgkKFVq1a+zylq3749zZ8/P2qCedVAVmckk0B4oMj+4iawZrolGQ3ffvtt2rp1a0QSwXgh18BxjchWI/exz2gVzSPRyeiDaMGViU6smAgSyYniTzzxBB188MG+yFOzZk268MILadWqVU5bvNqocyCjM5JJIJhkOKbD7bAomBmmlwiu1wceeMARXmwz3hVlXLxbDmmz2N/Le/wGrnHEewlJJMm66Z0SR4Ykf4fr2287gxAo3kHC/g2ugz09w4YN83VaA0qjRo0i2qomnM8HnkwC4T44YlBGfzNnmQicfIbrFWsSEo1gnhxgE0VyRJtHCIrjQg6zlYJzNGEe2gJuCpp8D4fHxIkTWRj9aId4cmPfcccdux23GOteNtEk+lp+x/WYM4eXAmLVQ9rdpk0bPkoF8x+pg9e9VQOlwAsHYPTHpBZ79EVozWPgI02KTDrllJN53pSfX0D5BflUWFToqW3MNsmhxCAPiCfCBe/SCy+8QIcccshupHETcJg/Tz/9tGNGVgSBMOrb1492L3yP9glpZNuCtBHv4aqfMGEC7b///hEmqXlvM7EitP8ll1zsK2RJCWR1RjIJhAl83759ee+9LVS2NsJDhQBfd911nGUTRPCzJiTEkfaZ9UbydBwBYh4w7CbcUg8QDaMyzEc/7fdLILkv6oGoCHsO5HYvU7vIMfEyUIT/g0pKStkh8eGHH1LXrl2dg7zM+4pHTs4IwveYl06YMIGz+JjPy6uNqoGMzkgmgdDxSHoODWQnPDcJJHMh/P2YY47hE9xycnJ2c6/aZowpfBAwCH6ZWVf20HEdmJB2TjQ70aAsPCKXAE7djuaVCqKB0N+jRo2KmAOZGtOtSFQHDgUWjSN1w7OaMWMG9e7dm81P+/xVrzS/iPebP3++s/4Tq41KIKMzkkEgAYQDC3WND20cYUp42eUoGCU7d+7MydIxQgqJxFSTuZE4F2QeBJMPBd9hYgzydOvaNSKUCPdBm+3k6/gO9eCtDOEoBFnLSiSBQFQx4eT3Mp9D26TYpJKDsPAdn1aXnU2rV62mF154nk4/va3rwqldF/mMjD2IUdwWdqXHcmQogazOSCaBgOnTp1NWVpZzr2j7VEAemBxwKFx66aU07a1ptHr16oiVclMzmCesyZwHazgYlRGSU69evQizxiSLLVwQ7sGDB0fdGxPUC4ewJj7iccsWjrgQkwyEsQkk7/F/eXl5HNMHDyXM4vffe49GjBhBJ7ZpE/EMo+WAEw9khw4daPbs2azVdjcL3duoGsjojGQRSEZXrMNgHiILqk4ovcd8RP4Gk6R169Y0cOBAzvm8aNEi3jkJDxkeKAreQ9tA6CFYOK5j5IgRvLdIHBdeaW3tV4zMOHs0b3seb6moCAJBgGFu4YxSuPjxOnPmTJ7D4D3S7KLgM9bQsC6GDXIIicKc5frrr2enDPJfi8lmXj9a9DXKUUcdxbFvCB4tNsijGqgcQp1MDYSHBFPhs88+412SttkUa9OXzBsOO+wwOvnkkzkBxtChQ+mWW27huQTmV6jzf/97GQsx5jAw2WwPlNdpBVKgGTAyI5iV9zLt2B5xFHyiCIQCrQhBRkF9UY444gjnvXxGnNqhhx7KE36QG1rZbJvdT25aCNoczxXfwUOHY+6x18n0VpYYcyqvNqoGShGBZN6C2DKYLhACrwhhNwG3CYb6NmzYkA5udDB7zFAgGH4Ey8vMQcGIjnnBps2b+GTsaLFhQQkUtLg5QkwTDX0mryggEQaIXr16sYMEGx2xPFASNn/1kOFyCnSyCIQHA0EUUwGR1tBCctCtCK+fDJoiMLEWCN0EC0Ik7RRSitcPBaYeAkhxzAd7xYqLqLik2Hd/lpdAZl3LU2K1H89PXNYSbSH9jB2qb775Zhl5CgqppLjMbLMjPLzaqBooBQSSCb/cEybDc889Ry2at2AzxhTiWJrCFDrb/IoltOZvIWDyXg7d7dmzJ0+qc3NzHYHyuwaVCg3k1h9CFDOqHP2LfsaghVxxOVtz2HGAYrvKS9SJkL5eOAiiHJ8BDxni3fBQ/SQ69GuWxbqG+SqjM8iDedXrr7/Oi5Egjzk3kNd0JJBoVnOOJ+Zu1YyqVK16NWrQsAE7LOBY+evvv5gkHN1hLAUA6sZOYwKxjV1c5qaFOxYF0b9YG0IsVnWYc8ao6VcATe1j7v23fy+EMd3YMidA9h3UA0kFzUgGCZtJZChPMgueY5PDm1C/fv3Ywwf3twwOYhGI960khgdO2qgmXIoIhNVzkEYWC+X+MJfgnj27e3f2OMXSRl7mi1vx0kRiMsKrBdf45599ztEOgLlQKXO2REYiVGSR/sCzbNasGe9IRfzfypUrnUBReZ724nOxzoHSeA5UGmICSVCo3F/qgEXP7+bOpYcffpi3G8CjBrMq2vqQXcwQIPN/zffQPnAUwNOG+Q72zEALom5CHDNY04wE8CKRdA1i0RAFDRd4RRNI2iuuafQVPJBNmzalLl260A033MCL1mt+W8N9DrBJWlxmrpnBp0KckpISHuCiPWv8LR4NBM9rvAliTLMSi8hwv5u7bNEPmAYg0j4IqsRTsWQ6EcxRz45pk7AUrK5jEj9+/Hje4IVYOAgH6oMicxYvEtlFtA06vG69etS8RQt++IiyRtYf5ImTFX5zHmDWy6xnrP4EgZAKC3WGO13qjsgGKdKWeAucAVgLgrY+/oTjqVu3buw5xDH1U6ZM4XU25HGQPVTS36aZZscRAvJsYgGxe9jVi/5F+8y2mUXW9hDRgYBgJVAFwvT+4D3i3ubOncvuVhw7OHjwIDr//PN5jwtME5heWH3HgwI5kNMMr/iMOY2MxggelQVXkBKmIlzUGMkA0TiYmyWiDYiOwER98uTJrN1QHn7kYZ5f4awe5GnD3+Itjz76KF/zuWef4/g+RIt//vnntHDhQh58TDOtolBUXMSjPdKUoU2ok1tBPbGehkgKkM5rYIoF1UA+4QizQSR+YEVFTKjFS5bQt99+S59++imHtrzxxhucqOTll1+ml15+iV+nTp3KJEHYy8xZs/j/l69YTv9k/+PqipaHaW6+25MRdOt7RaA0rNlgStrP1tfv1YTzByxYsolRumvNKFZoSXlg2vz2hFlc1UGFD20w51DiCpd2JKqYm+jsNZxkEChkrOu5FWmzW+S81N8vlEDleCi2YLhFMthCg+/gBDCDIU1Pmnk924VbESM2rinR1XJtc6NfIq5v9lO6otgIUPWKmvcDJZAPQBBkxd9U89L59shqahO3UdeM7XL+t8Rd88gDSoSmE5LKiGveJ5Emoj2ImN+nA0Lh5wmPq6z5xQslkE9ECHvY9DEFXrSHPCARSrf1CzcNJg8URHIbvRMxorvd1x5tvXabxlOi3buiEYpSL1vj2N5M1UBJ0ESm1wbFDjsx/2YjlnmWKI0TrR1mW9zqnAjSuF0vWQhFqaPpLrefo86BkoxoRIi2wOn3GolGeQUkKJJNnPLA3iUcz6ClJpxCEQBKIIUiAJRACkUAKIEUigBQAikUAaAEUigCQAmkUFRmAmH/DTaBmfuBsC0A58tEO/JcoUj2OhcyCeHAAclvlxYEkhRTII3sAK1dpzZnvkRuNGmEQpFMmDInsrotbxvLpWz7l525KSGQVBAZOJFUQ04lQIE2gqqUIy/cQue1aB+EKqgPeAsE4hatANytuVt5hysS0KSNBkKGShBITDhoIWxHhqqEBvIbLqNQJBJmvgaJbMcc6NrrrqV9au/Kl5EWORGQSca0KyUnAuZASK6uBFIkG3LGkxmFL06EtJoDwYmAXAPQQJJqFxWEralOBEWqAK2DzKlmYC4IBMsImZXMA9lS7kTAmT1mpUAmVBTZQ9N956OiciNkyF5u7jbWQMgGlDYaSAhkOhHq16/PkzXJ9oJsM3jVon2wPkl9gHOeUJAtFp9xeDISywwYMIAzL6UVgdq3b+8cvouKQQMhJxtSSSEBYY8ePbRoH1Ay+wByh5MJkZYMn5EnEK+QS+QHNAmEQ9Q+mTWrTLDjtJYCOxFEA8U6YkSL9kGVNOoDJlDbtjRr5kyW53g3MwZ2YwuBhN14lXNmtGgf1EhBH8hhAOZnOzMtvoMJh3yBplwnjUBIR2um9nU7ElGL9kFGGvVBhAaqXp2y2rXjc2ZTGsqDOZCc2GYfDahF+6BamvWBaKTMWpnUsVNHTiFsJzCpUALJDVauWMEHXY0YOYLXflAQSDp8+HB2ZaPgsxbtg2Fp1AeQU7i0kfd80qRJ9PPPPzsyHc/SS7kJJNixcye7CpcvX87Hw+MVBWfKLF26lIv8TYv2wYo06AOR0WVLl9GSxUv4NAossoI0XrkCK4xA0aCLqIo9CXZ226QTSJLiCXuVQIp0hT3PCZqPL2EEMkmjBFLsLagQE06h2FugBFIoAkAJpFAEgBJIoQgAJZBCEQBKIIUiAJRACkUAKIEUigBQAikUAaAEUigCQAmkUASAEkihCAAlkEIRAEoghSIAlEAKBcWP/wdSuboDickFrgAAAABJRU5ErkJggg=="/>
				</defs>
			</svg>
		`
	}
}

export const rccBack: IconAlias = {
	name: 		'back',
	from: 		'chevron',
	transform: 	'rotate(90deg)'
}

export const rccPrevious: IconAlias = {
	name: 		'previous',
	from: 		'chevron',
	transform: 	'rotate(90deg)'
}

export const rccNext: IconAlias = {
	name: 		'next',
	from: 		'previous',
	transform: 	'scaleX(-1)'
}
export const rccNextThin: IconAlias = {
	name: 		'next_thin',
	from: 		'chevron_thin',
}

export const rccOpen: IconAlias = {
	name: 		'open',
	from: 		'next',
}

export const rccDown: IconAlias = {
	name: 		'down',
	from: 		'chevron',
}

export const rccUp: IconAlias = {
	name: 		'up',
	from: 		'chevron',
	transform: 	'rotate(180deg)'
}


export const rccReport: IconAlias = {
	name:		'report',
	from:		'data',
}

export const rccSession: IconAlias = {
	name:		'session',
	from:		'share_data'
}

export const rccSymptomCheck: IconAlias = {
	name:		'symptom_check',
	from:		'monitoring_setup'
}

export const rccCreateFrom: IconAlias = {
	name:		'create-from',
	from:		'edit'
}
