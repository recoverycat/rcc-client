import	{
			NgModule
		}								from '@angular/core'
import	{	provideIcon				}	from './icon.commons'

import	* as allIcons					from './ready-icons'

@NgModule({
	providers: [
		...Object.values(allIcons).map(provideIcon)
	]
})

export class AllIconsModule{}
