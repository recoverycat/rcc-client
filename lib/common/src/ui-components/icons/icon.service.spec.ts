import 	{
			RccIconService,
			ResolvedIconName
		}							from './icon.service'
import	{
			IconConfig,
			IconAlias,
		}							from './icon.commons'



describe('RccIconService', () => {
	let rccIconService : RccIconService | undefined = undefined

	beforeEach(() => {
		rccIconService = new RccIconService([])
	})

	it('should be instantiated', () => {
		expect(rccIconService).toBeDefined()
	})

	it('should not throw an error if initial icons/aliases come in a bad order (alias before icon)', () => {

		const iconConfig 	: IconConfig
							= { name: 'test', iconVariants: { large: 'largeSVG', medium: 'mediumSVG', small: 'smallSVG' } }

		const iconAlias		: IconAlias
							= { name: 'alias', from: 'test' }

		expect( () => new RccIconService([iconAlias, iconConfig]) ).not.toThrow()
	})


	describe('.addIcon()', () => {

		it('should throw an error when duplicate names are used', () => {

			const iconConfig 	: IconConfig
								= { name: 'test', iconVariants: { large: 'largeSVG', medium: 'mediumSVG', small: 'smallSVG' } }

			const iconAlias		: IconAlias
								= { name: 'test', from: 'unknown' }

			rccIconService.addIcon(iconConfig)

			expect( () => rccIconService.addIcon(iconConfig) ).toThrow()
			expect( () => rccIconService.addIcon(iconAlias) ).toThrow()
		})


		it('should not throw an error when duplicate names are used and "forceReplace" is set to true', () => {

			const iconConfig 	: IconConfig
								= { name: 'test', iconVariants: { large: 'largeSVG', medium: 'mediumSVG', small: 'smallSVG' } }

			const iconAlias		: IconAlias
								= { name: 'alias', from: 'test' }

			rccIconService.addIcon(iconConfig)
			rccIconService.addIcon(iconAlias)

			expect( () => rccIconService.addIcon(iconConfig, true) ).not.toThrow()
			expect( () => rccIconService.addIcon(iconAlias, true) ).not.toThrow()
		})

		it('should throw an error when alias .from points to nothing', () => {

			const iconConfig 	: IconConfig
								= { name: 'test', iconVariants: { large: 'largeSVG', medium: 'mediumSVG', small: 'smallSVG' } }

			const iconAlias		: IconAlias
								= { name: 'alias', from: 'unknown' }

			rccIconService.addIcon(iconConfig)

			expect( () => rccIconService.addIcon(iconAlias) ).toThrow()
		})

		it('should throw an error when alias .from equals .name, even though forceReplace == true', () => {

			const iconConfig 	: IconConfig
								= { name: 'test', iconVariants: { large: 'largeSVG', medium: 'mediumSVG', small: 'smallSVG' } }

			const iconAlias1	: IconAlias
								= { name: 'alias', from: 'test' }

			const iconAlias2	: IconAlias
								= { name: 'alias', from: 'alias' }

			rccIconService.addIcon(iconConfig)
			rccIconService.addIcon(iconAlias1)
		
			expect( () => rccIconService.addIcon(iconAlias2, true) ).toThrow()
		})

	})

	describe('getIconVariants', ( ) => {

		it('should be able to retrieve the missing-icon when on a pristine instance', () => {
			expect(rccIconService.getIconVariants('missing')).toBeDefined()
		})

		it('should return the icon variants for a given icon name', () => {
			rccIconService.addIcon({ name: 'test', iconVariants: { large: 'largeSVG', medium: 'mediumSVG', small: 'smallSVG' } })

			expect(rccIconService.getIconVariants('test')).toEqual({ large: 'largeSVG', medium: 'mediumSVG', small: 'smallSVG' })
		})

		it('should return the icon variants for a given icon name and process aliases', () => {

			rccIconService.addIcon({ name: 'test', iconVariants: { large: 'largeSVG', medium: 'mediumSVG', small: 'smallSVG' } })
			rccIconService.addIcon({ name: 'test-alias', from: 'test' })

			expect(rccIconService.getIconVariants('test-alias')).toEqual({ large: 'largeSVG', medium: 'mediumSVG', small: 'smallSVG' })
		})

		it('should return undefined for an icon not registered', () => {
			expect(rccIconService?.getIconVariants('nonExistent')).toBeUndefined()
		})


	})

	describe('.resolveName()', () => {

		it('should resolve an icon name into record of traversal', () => {
			const iconConfig 	: IconConfig
								= { name: 'test', iconVariants: { large: 'largeSVG', medium: 'mediumSVG', small: 'smallSVG' } }

			const iconAlias1	: IconAlias
								= { name: 'alias1', from: 'test', transform: 'translateX(0.5rem)' }

			const iconAlias2	: IconAlias
								= { name: 'alias2', from: 'alias1', transform: 'scale(2)' }

			const iconAlias3	: IconAlias
								= { name: 'alias3', from: 'alias2', transform: 'rotate(40deg)' }

					
			rccIconService.addIcon(iconConfig)
			rccIconService.addIcon(iconAlias1)
			rccIconService.addIcon(iconAlias2)
			rccIconService.addIcon(iconAlias3)

			const resolvedIconName 	: ResolvedIconName
									= rccIconService.resolveName('alias3')

			expect(resolvedIconName).toEqual({
				icon: 		'test',
				aliases:	['alias3', 'alias2', 'alias1']
			})

		})
	})

	describe('.getTransformation()', () => {

		it('should get stacked transformation of alias chains', () => {

			const iconConfig 	: IconConfig
								= { name: 'test', iconVariants: { large: 'largeSVG', medium: 'mediumSVG', small: 'smallSVG' } }

			const iconAlias1	: IconAlias
								= { name: 'alias1', from: 'test', transform: 'translateX(0.5rem)' }

			const iconAlias2	: IconAlias
								= { name: 'alias2', from: 'alias1', transform: 'scale(2)' }

			const iconAlias3	: IconAlias
								= { name: 'alias3', from: 'alias2', transform: 'rotate(40deg)' }

					
			rccIconService.addIcon(iconConfig)
			rccIconService.addIcon(iconAlias1)
			rccIconService.addIcon(iconAlias2)
			rccIconService.addIcon(iconAlias3)

			const mergedTransform 	: string
									= rccIconService.getTransformation('alias3')

			expect(mergedTransform).toBe('rotate(40deg)scale(2)translateX(0.5rem)')

		})
	})

})
