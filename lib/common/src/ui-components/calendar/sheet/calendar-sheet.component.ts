import	{
			Component,
			Input,
			HostBinding
		}								from '@angular/core'

import	{
			RccTranslationService
		}								from '../../../translations'


/**
 * TODO
 */
@Component({
    selector: 'rcc-calendar-sheet',
    templateUrl: './calendar-sheet.component.html',
    styleUrls: ['./calendar-sheet.component.css'],
    standalone: false
})
export class RccCalendarSheetComponent {

	@Input()
	public set dateString (str: string) { this.setDate(str) }

	@Input()
	public set ticks(ticks:[number, number]){ this.setTicks(ticks)}

	@Input()
	public color		: string = 'medium'

	@HostBinding('class')
	public get themeClass()	: Record<string, boolean> { return { [`ion-color-${this.color}`]: true } }

	@HostBinding('class.weekend')
	public weekend 		: boolean

	@HostBinding('class.has-ticks')
	public get hasTicks():boolean { return !!this.startTicks }

	public day 			: string
	public monthNumber	: string
	public monthName	: string
	public dayOfWeek	: string

	public startTicks		: boolean[]
	public endTicks			: boolean[]
	public ellipsis: boolean	= false
	public allDone: boolean		= false

	public constructor(
		protected rccTranslationService: RccTranslationService
	){}


	public setDate(dateStr: string): void {

		this.day			= '00'
		this.monthNumber	= '00'
		this.monthName		= '--'
		this.dayOfWeek		= '--'
		this.weekend		= false

		if(typeof dateStr !== 'string') return

		const date: Date	= new Date(dateStr)
		const parts: string[]	= dateStr.split('-')

		this.day			= parts[2]
		this.monthNumber	= parts[1]
		this.monthName		= this.rccTranslationService.translate(dateStr, { month:'short' })
		this.dayOfWeek		= this.rccTranslationService.translate(dateStr, { weekday:'short' })
		this.weekend		= [0,6].includes(date.getDay())

	}

	public setTicks(ticks: [number, number] = [0,0]): void {

		const actual: number		= ticks[0]
		const max: number			= ticks[1]

		const allTicks: boolean[]	= new Array(max).fill(true).map( (x, index) => index < actual)

		if(actual === max && actual > 0) this.allDone = true

		if(max > 5){
			this.ellipsis	= true
			this.startTicks = allTicks.slice(0,2)
			this.endTicks	= allTicks.slice(-2)
		}else {
			this.ellipsis	= false
			this.startTicks = allTicks
			this.endTicks	= []
		}

	}

}
