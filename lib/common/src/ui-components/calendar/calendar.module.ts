import	{	NgModule					}	from '@angular/core'
import	{	CommonModule				}	from '@angular/common'
import	{	RccCalendarSheetComponent	}	from './sheet'
import	{	RccCalendarComponent		}	from './calendar'
import	{	RccSliderModule				}	from '../slider'

@NgModule({

	imports:[
		CommonModule,
		RccSliderModule,
	],

	declarations:[
		RccCalendarSheetComponent,
		RccCalendarComponent
	],

	exports:[
		RccCalendarSheetComponent,
		RccCalendarComponent
	]
})
export class RccCalendarModule {

}
