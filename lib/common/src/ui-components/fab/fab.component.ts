import	{
			Component,
			Input,
		}									from '@angular/core'
import	{	IconName					}	from '../icons/icon-names'
import	{	RccIconComponent			}	from '../icons'
import	{	RccColorCategoryDirective	}	from '../color-category'
import	{	TranslationsModule			}	from '@rcc/common/src/translations'
import	{	ColorCategory				}	from '@rcc/themes/theming-mechanics'

@Component({
    selector: 'rcc-fab',
    templateUrl: 'fab.component.html',
    styleUrls: ['fab.component.scss'],
    imports: [
        RccIconComponent,
        RccColorCategoryDirective,
        TranslationsModule,
    ]
})

export class RccFabComponent {

	@Input()
	public name: IconName

	@Input()
	public label: string

	@Input()
	public colorCategory: ColorCategory = 'primary'

	@Input()
	public disabled: boolean = false

}
