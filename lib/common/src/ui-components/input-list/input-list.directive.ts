import	{
			Directive,
			Input
		}								from '@angular/core'

import	{
			IonInput
		}								from '@ionic/angular'


/**
 * Adds autocomplete/data-list support to ion-input
 */
@Directive({
    selector: 'ion-input[list]',
    standalone: false
})
export class RccInputListDirective {

	@Input()
	public set list(list : string){

		if(!this.ionInput) return

		void 	this.ionInput.getInputElement()
				.then( element => element.setAttribute('list', list) )

	}

	public constructor(
		public ionInput:IonInput
	){}


}
