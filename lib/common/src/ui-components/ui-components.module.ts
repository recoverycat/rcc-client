import	{
			NgModule,
			Type
		}											from '@angular/core'

import	{ provideHttpClient, withInterceptorsFromDi }	from '@angular/common/http'

import	{	RccSliderModule						}	from './slider'
import	{	RccPaginationModule					}	from './pagination'
import	{	RccCalendarModule					}	from './calendar'
import	{	RccClickOutsideModule				}	from './click-outside'
import	{	RccToIDPipeModule					}	from './toID'
import	{	RccLogoComponent					}	from './logo'
import	{	IconsModule							}	from '../icons'
import	{	RccIconComponent					}	from './icons'
import	{	RccAutofocusModule					}	from './autofocus'
import	{	RccFocusModule						}	from './focus'
import	{	RccInputListModule					}	from './input-list'
import	{	SortByPositionPipeModule			}	from './sort-by-position'
import	{	AllIconsModule						}	from './icons'
import	{	RccCardComponent					}	from './card'
import	{	RccYyyyMmDdPipe						}	from './yyyy-mm-dd'
import	{	RccTabTrapDirective					}	from './tab-trap'
import	{ 	RccColorCategoryDirective 			}	from './color-category'
import	{	RccFabComponent						}	from './fab'
import	{	RccClientLogoComponent				}	from './client-logo/client-logo.component'
import	{	RccArrowNavigationDirective			}	from './arrow-navigation'
import	{	requestPublicRuntimeConfigValue		}	from '../public-runtime-config/config/public-runtime-config.commons'


const modules : Type<unknown> [] = [
	RccAutofocusModule,
	RccFocusModule,
	RccCalendarModule,
	RccClickOutsideModule,
	RccPaginationModule,
	RccSliderModule,
	RccToIDPipeModule,
	SortByPositionPipeModule,
	RccInputListModule,
	IconsModule,
	RccColorCategoryDirective,
	AllIconsModule,
]

const standaloneComponents : Type<unknown>[] = [
	RccArrowNavigationDirective,
	RccCardComponent,
	RccClientLogoComponent,
	RccFabComponent,
	RccIconComponent,
	RccLogoComponent,
	RccTabTrapDirective,
	RccYyyyMmDdPipe,
]
@NgModule({
	imports: [...modules, ...standaloneComponents],
	exports: [...modules, ...standaloneComponents],
	providers: [
		requestPublicRuntimeConfigValue({
			description: 'Client image',
			path: 'client_logo',
			type: 'ClientLogoConfig',
		}),
		provideHttpClient(withInterceptorsFromDi())
	]
})

export class UiComponentsModule{}
