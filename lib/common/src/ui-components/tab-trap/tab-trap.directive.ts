import	{
			Directive,
			AfterViewInit,
			OnDestroy,
			ElementRef,
			Inject,
			Input
		}							from '@angular/core'

import	{
			DOCUMENT,
		}							from '@angular/common'
import	{
			Subject,
			takeUntil,
			fromEvent,
			filter,
			Observable,
			merge
		}							from 'rxjs'

import	{
			getTabbableDescendants,
		}							from '@rcc/core'

/**
 * Prevents focus leaving the host element on TAB key presses. Instead
 * using TAB will loop through all the tabbable child elements of the host
 * element.
 *
 * How to exit the loop (incomplete):
 * * Press ESC
 * * Blur the focused element
 * * Focus another element outside the host programmatically
 * * Hide the host element (e.g. set visibility:hidden or display:none)
 * * Detach host element
 * * Destroy host component
 */
@Directive({
	selector: 	'[tabTrap]',
	standalone: true

})
export class RccTabTrapDirective implements AfterViewInit, OnDestroy{

	protected 	destroy$			: Subject<void>
									= new Subject<void>()


	protected 	element 			: HTMLElement
									= undefined

	/**
	 * Allow using the up and down arrow keys to navigate in addition to the TAB key.
	 */
	@Input()
	public		allowArrowKeys		: boolean
									= false


	public constructor(
		@Inject(DOCUMENT)
		private document	: Document,
		private elementRef	: ElementRef<HTMLElement>
	){
		this.element = elementRef.nativeElement
	}

	public ngAfterViewInit() : void {

		const keydownEvent$		:	Observable<KeyboardEvent>
								=	fromEvent<KeyboardEvent>(this.element, 'keydown')

		const tabEvent$			:	Observable<KeyboardEvent>
								=	keydownEvent$
									.pipe( filter( event => event.key === 'Tab') )

		const escEvent$			:	Observable<KeyboardEvent>
								=	keydownEvent$
									.pipe( filter( event => event.key === 'Escape') )

		const upEvent$			:	Observable<KeyboardEvent>
								=	keydownEvent$
									.pipe( filter( event => event.key === 'ArrowUp') )
										
		const downEvent$		:	Observable<KeyboardEvent>
								=	keydownEvent$
									.pipe( filter( event => event.key === 'ArrowDown') )

		merge(tabEvent$, upEvent$, downEvent$)
		.pipe( takeUntil(this.destroy$) )
		.subscribe( (event:KeyboardEvent) => this.trapFocus(event) )

		escEvent$
		.pipe( takeUntil(this.destroy$) )
		.subscribe( () => this.clearFocus() )
						
	}

	protected clearFocus() : void {

		if(!(this.document.activeElement instanceof HTMLElement)) 	return undefined
		if(!this.element.contains(this.document.activeElement))		return undefined

		this.document.activeElement.blur()
	}

	protected trapFocus(event: KeyboardEvent): void {

		const focusedElement	:	HTMLElement
								=	this.document.activeElement as HTMLElement

		const tabbables 		:	HTMLElement[]
								=	getTabbableDescendants(this.element)

		const focusedElementPos	:	number
								=	tabbables.indexOf(focusedElement)

		const isArrowEvent		:	boolean
								=	event.key === 'ArrowUp' || event.key === 'ArrowDown'

		// Navigation with arrow keys
		if (this.allowArrowKeys && isArrowEvent) {
			const direction		:	number = event.key === 'ArrowUp' ? -1 : 1

			const nextPos		:	number
								=	focusedElementPos === -1
									? 0
									: (focusedElementPos + direction + tabbables.length) % tabbables.length

			const nextElement	:	HTMLElement
								=	tabbables[nextPos]

			nextElement.focus()
			event.preventDefault()
			return undefined
		}

		// If focus does not lie within the host, leave things as they are:
		if( !this.element.contains(focusedElement) )
			return undefined

		// Do not try to trap anything if there are no tabbable child elements
		if(tabbables.length === 0)
			return undefined

		// If there is only one tabbable child element, then keep it focused:
		if(tabbables.length === 1) {
			event.preventDefault()
			return undefined
		}

		const firstTabbable		: HTMLElement
								= tabbables[0]

		const lastTabbable		: HTMLElement
								= tabbables[tabbables.length-1]

		const tabbingForward	: boolean
								= !event.shiftKey

		const tabbingBackward	: boolean
								= event.shiftKey

		const exitFromFirst		: boolean
								= focusedElement === firstTabbable 	&& tabbingBackward

		const exitFromLast		: boolean
								= focusedElement === lastTabbable 	&& tabbingForward

		const exitImminent		: boolean
								= exitFromFirst || exitFromLast

		if(exitImminent) 	event.preventDefault()

		if(exitFromFirst)	lastTabbable.focus()
		if(exitFromLast)	firstTabbable.focus()

	}

	public ngOnDestroy(): void{
		this.destroy$.next()
		this.destroy$.complete()
	}

}
