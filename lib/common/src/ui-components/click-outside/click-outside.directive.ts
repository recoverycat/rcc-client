import	{
			Directive,
			Output,
			ElementRef,
			HostListener,
			EventEmitter
		}								from '@angular/core'

@Directive({
    selector: '[clickOutside]',
    standalone: false
})
export class RccClickOutsideDirective {


	@Output()
	public clickOutside: EventEmitter<void> = new EventEmitter<void>()

	@HostListener('document:click', ['$event'])
	public onClick(event: Event): void {

		if( this.element.contains(event.target as HTMLElement) ) return

		this.clickOutside.emit()

	}

	protected element!: HTMLElement

	public constructor(
		public elementRef:	ElementRef<HTMLElement>
	){
		this.element = elementRef.nativeElement
	}


}
