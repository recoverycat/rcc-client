import	{
			Directive,
			HostListener,
		}								from '@angular/core'

@Directive({
    selector: '[clickPropagationStop]',
    standalone: false
})
export class RccClickPropagationStopDirective {

	@HostListener('click', ['$event'])
	public onClick(event: Event): void {
		event.stopPropagation()
	}

}
