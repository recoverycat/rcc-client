import	{	NgModule							} from '@angular/core'
import	{	RccClickOutsideDirective			} from './click-outside.directive'
import	{	RccClickPropagationStopDirective	} from './click-propagation-stop.directive'

@NgModule({
	declarations:[
		RccClickOutsideDirective,
		RccClickPropagationStopDirective
	],
	exports: [
		RccClickOutsideDirective,
		RccClickPropagationStopDirective
	]
})
export class RccClickOutsideModule {

}
