import	{	NgModule					}	from '@angular/core'

import	{	Item						}	from '@rcc/core'

import	{	ExportConfig				}	from './export.commons'

import	{	RccExportService			}	from './export.service'

import	{	ExportModalComponent		}	from './modal/export-modal.component'

import	{	provideTranslationMap		}	from '../translations'

import	{	SharedModule				}	from '../shared-module'

import	{	MetaStoreModule				}	from '../meta-store'

import	{	Factory						}	from '../interfaces'

import	{
			ItemAction,
			provideItemAction
		}									from '../items'

import	{	HandlerAction				}	from '../actions'

import en from './i18n/en.json'
import de from './i18n/de.json'


/**
 * HandlerAction factory for items.
 * Handler function checks if export services are available for an item
 * and opens a selection modal
 */
const exportItemActionFactory : Factory<ItemAction<Item<unknown>>> = {
	deps: [RccExportService],

	factory: (rccExportService: RccExportService) => ({
		itemClass: null,
		role: 'share',
		getAction(item: Item): HandlerAction {
			const config : ExportConfig = {
				data: item
			}

			return {
				label: 'EXPORT.ACTION.LABEL',
				icon: 'export',
				description: 'EXPORT.ACTION.DESCRIPTION',
				handler: () => void rccExportService.runExport(config)
			}
		}
	})
}

@NgModule({
	imports: [
		SharedModule,
		MetaStoreModule,
	],
	providers: [
		RccExportService,
		provideTranslationMap('EXPORT', { en, de }),
		provideItemAction(exportItemActionFactory)
	],
	declarations: [
		ExportModalComponent
	]
})
export class ExportModule {}
