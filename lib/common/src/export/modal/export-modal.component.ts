import	{	Component			}	from '@angular/core'
import 	{
			Item,
			UserCanceledError
		}							from '@rcc/core'
import	{	RccModalController	}	from '../../modals-provider'
import	{	DownloadAction		}	from '../..'


@Component({
	templateUrl:	'./export-modal.component.html',
	styleUrls:		['./export-modal.component.scss'],
	standalone:		false,
})
export class ExportModalComponent {
	public actions			: DownloadAction[]
	public data				: unknown
	public label?			: string | undefined
	public description?		: string | undefined

	public constructor(
		public rccModalController:	RccModalController
	) {}

	public isItem(x: unknown): x is Item<unknown> {
		return x instanceof Item
	}

	public cancel(): void {
		this.rccModalController.dismiss(new UserCanceledError())
	}
}
