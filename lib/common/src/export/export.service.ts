import	{
			Injectable,
			Inject,
			Optional,
		}								from '@angular/core'

import	{
			assert,
			Item
		}								from '@rcc/core'

import	{	RccModalController		}	from '../modals-provider'

import	{	DownloadAction			}	from '../actions'

import	{
			ExportConfig,
			ExportMetadata,
			ExportResult,
			ExportService,
			EXPORT_SERVICES,
			toBlobPart
		}								from './export.commons'

import	{	ExportModalComponent	}	from './modal/export-modal.component'


@Injectable()
export class RccExportService {

	public constructor(
		@Optional()@Inject(EXPORT_SERVICES)
		public exportServices: 		ExportService<Item<unknown>, unknown>[],
		public rccModalController:	RccModalController

	) {
		this.exportServices = exportServices || []
	}

	/**
	 * Returns a list of ExportServices that can export data
	 */
	private getApplicableExportServices(data: unknown, ...context: unknown[]): ExportService<Item<unknown>, unknown>[] {
		return this.exportServices.filter( exportService => exportService.canExport(data, context) )
	}

	/**
	 * Builds a DownloadAction from an {@link ExportConfig} (= input data) and an applicable {@link ExportService}.
	 * `DownloadAction.data()` will return a BlobPart of the ExportResult.
	 */
	private async buildActionFromExportService<F,T>(config: ExportConfig<F>, exportService: ExportService<F,T>): Promise<DownloadAction> {
		assert(exportService.canExport(config.data), 'buildActionFromExportService: ExportService can\'t export this ExportConfig')

		const metadata : ExportMetadata	= await exportService.getMetadata(config)
		
		return {
			label			: exportService.label,
			description		: exportService.description,
			icon			: 'export',
			filename		: metadata.suggestedFilename,
			mimeType		: metadata.mimeType,
			data: async () => {
				const result : ExportResult = await exportService.export(config)
				return toBlobPart(result)
			},
		}
	}

	/**
	 * In charge of the export process for a piece of data ({@link ExportConfig}).
	 * - Gets a list of applicable {@link ExportService}s and constructs a DownloadAction for each of them.
	 * - Presents a modal with the ExportService selection that also triggers the DownloadAction.
	 */
	public async runExport(config: ExportConfig): Promise<void> {
		const applicableExportServices : ExportService[] = this.getApplicableExportServices(config.data)

		const actions : DownloadAction[] =	await Promise.all(
												applicableExportServices.map(async exportService =>
													await this.buildActionFromExportService(config, exportService)
											))

		const data			: unknown	= config.data
		// Optional label/description of the input data
		// Will be displayed before the available export services
		const label 		: string	= config.label
		const description	: string	= config.description

		await this.rccModalController.present(ExportModalComponent, { actions, data, label, description })
	}

}
