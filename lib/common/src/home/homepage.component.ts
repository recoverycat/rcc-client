import 	{
			Component,

		}											from '@angular/core'
import	{	map, Observable						}	from 'rxjs'
import	{
			BackupRestoreHelper,
		}											from '@rcc/features/backup/backup-restore.helper'
import	{	NotificationService 				}	from '../notifications'
import	{	Action								}	from '../actions'
import	{
			HomePageEntry,
			ItemEntry,
		}											from './homepage.commons'
import	{	RccHomePageEntryService				}	from './homepage-entry.service'
import	{	ColorCategory						}	from '@rcc/themes/theming-mechanics'
import	{	MarkdownConfig,	RccBannerService	}	from '../banners'

@Component({
				selector: 		'rcc-home',
				templateUrl: 	'homepage.component.html',
				styleUrls:		['homepage.component.scss'],
				standalone:		false,
			})
export class HomePageComponent {

	/**
	 * Markdown to be rendered on the full page as a banner.
	 */
	protected bannerMarkdown: Observable<MarkdownConfig> = this.rccBannerService.pipe(
		map((value) => value.markdown)
	)

	/**
	 * Background color of the banner.
	 */

	protected bannerColor: Observable<ColorCategory> = this.rccBannerService.pipe(
		map((value) => value.color)
	)


	public constructor(
		public notificationService		: NotificationService,
		backupRestoreHelper				: BackupRestoreHelper,
		public rccHomePageEntryService	: RccHomePageEntryService,
		public rccBannerService			: RccBannerService
	) {
		backupRestoreHelper.showToastAfterReload()
	}

	public isItemEntry(entry: HomePageEntry) : entry is ItemEntry {
		return 'item' in entry
	}
	public isAction(entry: HomePageEntry) : entry is Action {
		return !('item' in entry)
	}

	public get activeEntries$(): Observable<HomePageEntry[]> {
		return 	this.rccHomePageEntryService.activeEntries$
	}

}
