import	{
			Inject,
			Injectable,
			Optional
		}							from '@angular/core'
import	{
			Subject,
			Observable
		}							from 'rxjs'
import	{
			throttleTime,
			map,
			shareReplay,
			startWith
		}							from 'rxjs/operators'
import	{	Action				}	from '../actions'
import	{
			HOMEPAGE_ENTRIES,
			HomePageEntry
		}							from './homepage.commons'
import	{	sortByKeyFn			}	from '@rcc/core'



/** The `RccHomePageEntryService` is a service in an Angular
application that manages the active entries on a homepage.
It provides functionality to add and remove homepage entries, as well
as update the list of active entries. The service uses RxJS
observables to notify subscribers when the active entries have been
updated. The `getActiveEntries` function filters out null entries
and sorts the remaining entries based on their `position` property.
*
*/

@Injectable()
export class RccHomePageEntryService {

	private updateRequests	: Subject<void>
							= new Subject<void>()

	public activeEntries$	: Observable<HomePageEntry[]>

	public constructor(
		@Optional()@Inject(HOMEPAGE_ENTRIES)
		public entries		: HomePageEntry[],
	)
	{
		this.entries = entries || []
		this.activeEntries$ = this.updateRequests
								.pipe(
									startWith(undefined),
									throttleTime(500, undefined, { leading:true, trailing:true }),
										map(() => this.getActiveEntries()),
									shareReplay(1)
								)
		this.updateRequests.next()
	}


	/**
	 * The function `getActiveEntries` filters out null and sorts an array of `HomePageEntry`
	 * objects based on their `position` property.
	 * @returns an array of active HomePageEntry objects.
	 */

	private getActiveEntries(): HomePageEntry[] {
		const activeEntries	:	HomePageEntry[]
							=	this.entries
									.filter(
										(action: Action) => typeof action.position === 'function'
															?	['number', 'undefined'].includes(typeof action.position() )
															:	['number', 'undefined'].includes(typeof action.position )
									)
									.sort( sortByKeyFn('position') )

		return activeEntries
	}

	public requestUpdateActiveEntries(): void {
		this.updateRequests.next()
	}

	public addHomePageEntry(homePageEntry: HomePageEntry) : void {
		this.entries.push(homePageEntry)
		this.requestUpdateActiveEntries()
	}

	public removeHomePageEntry(homePageEntry: HomePageEntry): void {
		this.entries = this.entries.filter((entry) => entry !== homePageEntry)
		this.requestUpdateActiveEntries()
	}

}
