
import	{
	InjectionToken,
			Provider
		} 								from '@angular/core'

import	{
			Factory,
			getProvider
		}								from '../interfaces'

import	{	Action, WithPosition	}	from '../actions'
import	{	ItemActionRole			}	from '../items'
import	{	Item					}	from '@rcc/core'


export type HomePageEntry = Action | ItemEntry

export interface ItemEntry extends WithPosition {
	item: 				Item,
	/**
	 * Only use action with these roles (see {@link RccItemTagComponent}):
	 */
	itemActionRoles?:	ItemActionRole[]
}

export const HOMEPAGE_ENTRIES: InjectionToken<HomePageEntry[]> = new InjectionToken<HomePageEntry[]>('HOMEPAGE_ENTRIES')

export function provideHomePageEntry( config : (Action | Factory<Action> | ItemEntry) ): Provider {

	return 	getProvider(HOMEPAGE_ENTRIES, config, true)


}
