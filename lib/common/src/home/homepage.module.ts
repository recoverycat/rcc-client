import	{
			NgModule,
		} 								from '@angular/core'
import 	{ 	RouterModule,
			Routes
		}								from '@angular/router'
import	{
			MainMenuModule,
									}	from '../main-menu'
import	{	provideTranslationMap	}	from '../translations'
import	{	SharedModule 			}	from '../shared-module'
import	{	NotificationModule		}	from '../notifications'

import	{	HomePageComponent 		}	from './homepage.component'
import	{	BuildInfoModule			}	from '../build-info'
import	{	RccHomePageEntryService	}	from './homepage-entry.service'

import	de from './i18n/de.json'
import	en from './i18n/en.json'
import	{	RccSplashScreenComponent	}	from '@rcc/themes/active'

const routes: Routes 		=	[
									{
										path: '',
										title: 'HOMEPAGE.PAGE_TITLE',
										component: HomePageComponent,
									},
]

export class MenuEntryHomeComponent {}

export const HomePageHomePath : string = '/'


@NgModule({
	imports: [
		RouterModule.forChild(routes),
		MainMenuModule,
		NotificationModule.forChild(),
		SharedModule,
		BuildInfoModule,
		RccSplashScreenComponent
	],
	providers: [
		provideTranslationMap('HOMEPAGE', { en, de }),
		RccHomePageEntryService

	],
	declarations: [
		HomePageComponent,
	],

})
export class HomePageModule {}
