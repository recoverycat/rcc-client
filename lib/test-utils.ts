import { ComponentFixture } from '@angular/core/testing'
import { NgZone } from '@angular/core'

interface WaitUntilOptions {
	/**
	 * Time (ms) until test can be considered failed. Default: 5000
	 */
	timeout: number
	/**
	 * Time (ms) between tests, before checking again if the condition has passed.
	 *
	 * Default: 200
	 */
	changeDetection: number
}

export async function waitUntil(callback: () => boolean, fixture: ComponentFixture<unknown>, options?: WaitUntilOptions): Promise<void> {
	const timeout: number = options?.timeout ?? 5000
	const changeDetection: number = options?.changeDetection ?? 200

	const firstAttempt: boolean = callback()

	if (firstAttempt)
		return
	

	return new Promise((resolve, reject) => {
		let timeElapsed: number = 0
		const timer: NodeJS.Timeout = setInterval(() => {
			fixture.detectChanges()
				const result: boolean = callback()
				
				if (result) {
					clearInterval(timer)
					resolve()
					return
				}

				timeElapsed += changeDetection
				if (timeElapsed >= timeout) {
					reject(new Error(`Condition not met after ${timeout}ms`))
					clearInterval(timer)
				}
			})
	})
}

export class MockNgZone implements Partial<NgZone> {
	public run<T>(fn: () => T): T {
		return fn()
	}

	public runOutsideAngular<T>(fn: () => T): T {
		return fn()
	}
}
