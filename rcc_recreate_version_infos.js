import { execSync } from 'child_process';

// This script runs the `npm run generate-info` command every 500ms
// We need this to update the `build_info.json` when using `ng serve`
//
// environment and flavor need to be provided as args


/**
 * Execute a command and log error if one occurs
 * @param {*} command to be executed
 * @returns a string with the stdout of the command
 */
function execute(command) {
  try {
    return execSync(command).toString().trim();
  } catch (error) {
    console.error(error);
  }
};


const args        = process.argv;
const environment = args[2] || 'development';
const flavor      = args[3] || 'unknown';

let describe = ''
setInterval(rebuildIfNecessary, 2000)

/**
 * if `git describe --dirty` differs from the last iteration
 * regenerate `build_info.json`
 */
function rebuildIfNecessary() {
  let new_describe    = execute('git describe --dirty');
  if(new_describe !== describe) {
    console.log('regenerating build infos')
    execute(`npm run generate-info ${environment} ${flavor}`)
    describe = new_describe
  }
}

