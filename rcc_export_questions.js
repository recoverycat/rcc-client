import yargs from 'yargs'
import { hideBin } from 'yargs/helpers'
import { writeFile } from 'fs/promises'

const argv	=	yargs(hideBin(process.argv))
				.options({
					input: {
						describe:		'Path to JS file with a variable `configs` that contains QuestionConfigs',
						demandOption:	true,
					},
					output: {
						describe:		'Target path of the generated TSV file',
						default:		'./questions.tsv'
					},
				})
                .check(argv => {
                    if (!argv.input.endsWith('.js'))    throw new Error('Input path must end with .js')
                    if (!argv.output.endsWith('.tsv'))  throw new Error('Output path must end with .tsv')
                    return true
                })
				.argv

const configFile = argv.input
const targetPath = argv.output

let configs
configs = (await import(configFile)).configs

// TSV headers
const headers = 'ID\tWording Deutsch\tWording Englisch\tAntwort Deutsch\tAntwort Englisch\tAntwort Values\tTags\n'

function createRows(configs) {
    let data = ''

    for (let config of configs) {
        let row = config.id + '\t' + config.translations.de + '\t' + config.translations.en + '\t'

        row += getAnswers(config) + '\t'

        if (!config.tags) console.warn(`No tags found for ${config.meaning}`)
        else config.tags.forEach(tag => row += `${tag}\t`)

        data += row + '\n'
    }

    return data
}

function getAnswers(config) {
    if (config.type === 'boolean') return 'ja, nein\tyes, no'

    if (!config.options) {
        console.warn(`No answer options found for ${config.meaning}`)
        return ''
    }

    let answerValues    = ''
    let answersGerman   = ''
    let answersEnglish  = ''
    
    config.options.forEach(option => {
        answerValues += option.value + ', '
        if (option.translations) {
            answersGerman   += option.translations.de + ', '
            answersEnglish  += option.translations.en + ', '
        }
    })

    // Remove trailing comma + space
    return answersGerman.slice(0, -2) + '\t' + answersEnglish.slice(0, -2) + '\t' + answerValues.slice(0, -2)
}

async function exportQuestions(filename) {
    const data = headers + createRows(configs)
    await writeFile(filename, data, 'utf8')
}

await exportQuestions(targetPath)
