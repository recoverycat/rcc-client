import { parse } from 'marked'

export default function markdownLoader(markdown) {
	const options = this.getOptions();

	return parse(markdown, options);
}
