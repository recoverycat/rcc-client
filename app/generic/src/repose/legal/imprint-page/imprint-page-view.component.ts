import 	{ 	Component 			} 					from '@angular/core'
import	{
			TranslationsModule,
			RccMarkdownComponent,
			RccColorCategoryDirective
		} 											from '@rcc/common'

import	{
			RccFullPageComponent
		}											from '@rcc/themes/active'

import de from './i18n/imprint_de.md'
import en from './i18n/imprint_en.md'

@Component({
	templateUrl	:	'./imprint-page-view.component.html',
	selector	:	'imprint-markdown',
	styleUrls	:	['../legal-common.scss'],
	standalone	:	true,
	imports		:	[
						TranslationsModule,
						RccMarkdownComponent,
						RccColorCategoryDirective,
						RccFullPageComponent
					]
})
export class ImprintPageViewComponent{

	// prefixing 'translation' in the record allows for the use of TranslatePipe in the template:
	public translations			: Record<string, Record<string, string>>
								= { translations: { en, de } }

}
