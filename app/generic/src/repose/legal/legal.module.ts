import	{	NgModule										}	from '@angular/core'
import	{	RouterModule, Routes							}	from '@angular/router'
import	{
			MainMenuEntry,
			provideMainMenuMetaEntry,
			provideTranslationMap,
			SharedModule
		}														from '@rcc/common'
import	{	RccUserManualSharedModule						}	from '@rcc/features/user-manual/user-manual-shared/user-manual-shared.module'
import	{	HelpAndUserManualPageComponent					}	from './help-and-user-manual-page/help-and-user-manual-page.component'
import	{	ImprintPageViewComponent						}	from './imprint-page/imprint-page-view.component'

import en from './i18n/en.json'
import de from './i18n/de.json'

const mainMenuMetaEntries 	: 	MainMenuEntry[]
							=	[
									{
										label: 	'LEGAL.IMPRINT.MENU_ENTRY',
										path:	'imprint',
										icon:	undefined
									},
									{
										label:	'LEGAL.SUPPORT.MENU_ENTRY',
										path:	'support',
										icon:	undefined
									}

								]

const routes				: Routes
							=	[
									{
										path: 		'support',
										component: 	HelpAndUserManualPageComponent,
										title:		'LEGAL.SUPPORT.MENU_ENTRY'

									},
									{
										path: 		'imprint',
										component: 	ImprintPageViewComponent,
										title:		'LEGAL.IMPRINT.MENU_ENTRY'
									}
								]



@NgModule({
	declarations: [
		HelpAndUserManualPageComponent
	],
	imports: [
		SharedModule,
		RouterModule.forChild(routes),
		RccUserManualSharedModule,
	],
	providers:[
		provideTranslationMap('LEGAL', { en, de }),
		...mainMenuMetaEntries.map(provideMainMenuMetaEntry)
	]
})
export class RccLegalModule {}
