import	{
			NgModule
		}													from '@angular/core'

import	{	RccBaseFeaturesModule as HcpBaseFeatureModule	}	from '../hcp/base-features.module'

import	{	HcpMedicalFeaturesModule					}	from '../hcp-med/medical-features.module'

@NgModule({
	imports: [
		HcpBaseFeatureModule,
		HcpMedicalFeaturesModule,
	],
})

export class RccFeaturesModule { }
