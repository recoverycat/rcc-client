import	{
			NgModule
		}											from '@angular/core'

import	{
			MedicalProductModule,
		}											from '@rcc/common'

import	{
			GlossaryMainMenuEntryModule,
			BasicGlossaryEntryRepresentationModule,
			RegulatoryInfoPageMetaMenuEntryModule,
			RccHcpMedUserManualModule,
			InclusionCriteriaModalModule
		}											from '@rcc/features'
import	{	RccLegalModule						}	from '../hcp-med/legal'

@NgModule({
	imports: [
		MedicalProductModule,
		GlossaryMainMenuEntryModule.addEntry({ position: 3.5 }),
		BasicGlossaryEntryRepresentationModule,
		RegulatoryInfoPageMetaMenuEntryModule.addEntry({ position: 1 }),
		RccHcpMedUserManualModule,
		InclusionCriteriaModalModule,
		RccLegalModule,
	]
})
export class HcpMedicalFeaturesModule{}
