export interface MatomoEnvironment {
	url			: string
	disabled	: boolean
}
