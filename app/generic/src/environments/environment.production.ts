import { MatomoEnvironment } from './environment.common'

export const environment: { matomo: MatomoEnvironment } = {
	matomo: {
		url			: 'https://matomo.recoverycat.de',
		disabled	: false,
	}
}
