# Impressum

**Recovery Cat GmbH**\
Invalidenstr. 153\
10115 Berlin\
[+49 30 209 654 055](tel:+4930209654055)\
[hello@recovery.cat](mailto:hello@recovery.cat)


**Geschäftsführer**: Dr. Jakob Kaminski\
**Handelsregister**: Amtsgericht Berlin-Charlottenburg, HRB 246357 B, VAT ID: DE355718464
