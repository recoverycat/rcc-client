# Privacy Policy | Recovery Cat Medical

The following is a translated version. Only the German language version is legally binding.

Recovery Cat GmbH ("Recovery Cat", "we", "us", "our") has developed the browser-based web application Recovery Cat Medical ("Service") (accessible via the URL specified in the Agreement) to support therapy planning and symptom diary creation for patients undergoing psychiatric treatment and their caregivers. Recovery Cat protects your privacy and your personal data. We will inform you in detail below about the processing of your data.


## 1. Responsible Persons

Insofar as we do not act as a processor for the institution where you work as a practitioner, the "controller" for the collection and processing of your personal data in accordance with Art. 4 No. 7 of the EU General Data Protection Regulation (Regulation (EU) 2016/679) ("GDPR") is

Recovery Cat GmbH\
Invalidenstr. 153\
10115 Berlin\
[hello@recovery.cat](mailto:hello@recovery.cat)\
[+49 (0) 30 209 654 055](tel:+49030209654055)

You can find more information about us in the [imprint](/imprint).


## 2. Data Protection Officer
If you have any questions about the processing of personal data by Recovery Cat, you can contact our data protection officer:

[datenschutz@recovery.cat](mailto:datenschutz@recovery.cat)


## 3. Data Processing
### 3.1. Processing for Billing Purposes (Patients)
We process your personal data (first name, surname, health insurance information) and your health data (information on hospitalization, proof of diagnosis) for the purpose of transmitting it to your health insurance company for billing our services. We also need your contact details (e-mail address, telephone number) for possible product recalls.

#### Legal basis: 
The processing is based on your consent in accordance with Art. 6 para. 1 lit. a) GDPR in conjunction with Art. 9 para. 2 lit. a) GDPR. You can revoke your consent at any time without giving reasons by e-mail to [support@recovery.cat](mailto:support@recovery.cat) with effect for the future.

#### Categories of personal data: 
Contact data, health data, insurance data.

#### Recipient: 
Your data will be stored by us. Billing data will be transmitted to your health insurance company.

#### Obligation to provide data: 
The provision of your personal data is necessary for the billing of our services. Without this data, we cannot bill your health insurance company for our services.

### 3.2. Processing of the Data Entered by You
#### 3.2.1. Patients
You can receive therapy follow-ups from your therapists, answer questions from the therapy follow-up, view your therapy progress and share it with your therapists. The information you provide contains health data.

##### Legal basis: 
The processing of your health data is based on your consent in accordance with Art. 6 para. 1 lit. a) GDPR in conjunction with Art. 9 para. 2 lit. a) GDPR.

##### Categories of personal data: 
Health data, usage data.

##### Storage duration: 
The data is initially only stored locally on your end device. Processing by us only takes place when you share the data with your practitioners.

##### Recipients: 
Your practitioners can view the data on your device in plain text, evaluate it with you and store it locally for documentation purposes.

##### Obligation to provide data: 
The provision of data is voluntary. However, without this data you will not be able to make full use of the functions of the service.

#### 3.2.2. Healthcare Professionals
You can create questions for the follow-up questionnaire (therapy follow-up) for your patients and share these with them, receive data from your patients and export the data received. If you provide information about yourself when creating therapy follow-ups, e.g. your name, we will process this data.

##### Legal basis: 
Art. 6 para. 1 lit. b) GDPR (fulfillment of contract).

##### Categories of personal data: 
Contact details, professional information.

##### Recipient: 
The processing of this encrypted data is carried out by us as part of order processing for your institution.

##### Obligation to provide the data: 
The provision of your data is necessary for the use of the service.


### 3.3. Processing of Contact Requests 
When contacting us (e.g. by e-mail or telephone), your details will be stored for the purpose of processing the request and in the event of follow-up questions.

#### Legal basis: 
Art. 6 para. 1 lit. b) GDPR (contract fulfillment or pre-contractual measures).

#### Categories of personal data: 
Contact details, communication content.

#### Storage period: 
We delete the requests if they are no longer required.

#### Obligation to provide the data: 
The provision of data is voluntary and only takes place if you contact Recovery Cat. 


### 3.4. Logfiles
When you visit our service, information is automatically collected and stored in so-called server log files, which your browser automatically transmits to us.

#### Processed data:
- Browser type and version
- Operating system
- Anonymous IP address
- Access date and time
- Amount of data transferred
- Referrer URL
- Connection data and sources

#### Purpose: 
To ensure a smooth connection setup, system security and stability and for administrative purposes.

#### Legal basis: 
Art. 6 para. 1 lit. f) GDPR. Our legitimate interest follows from the purposes mentioned.

#### Recipients: 
Our hosting service providers.

#### Storage duration: 
The log files are stored for a maximum of 9 months and then deleted.

#### Right to object: 
Data processing is necessary for the operation of the service. There is therefore no right to object.

#### Obligation to provide data: 
The provision of data is neither legally nor contractually required, but is technically necessary for the operation of the service.


### 3.5. Usage Analysis (Matomo)
We use Matomo, an open source software for the statistical analysis of user access. Matomo analyzes your use of our service without using cookies.

#### Processed data:
- Anonymous IP address
- Referrer URL
- Pages accessed
- Length of stay
- Device information (browser, operating system, settings)

#### Purpose: 
Analysis of usage behavior to optimize our service.

#### Legal basis: 
Art. 6 para. 1 lit. f) GDPR. Our legitimate interest lies in the improvement of our offer.

#### Recipient: 
The data is stored on our servers and not passed on to third parties.

#### Storage duration: 
The data is stored for up to two years.

#### Objection option: 
You can prevent the storage of data by setting your browser software accordingly. This may limit the functionality of the service.

#### Obligation to provide data: 
The provision of data is voluntary. Without processing, however, the functionality of the service may be restricted.


### 3.6. Automated Decision-Making
We do not use automated decision-making or profiling in accordance with Art. 22 GDPR.


## 4. Recipients or Categories of Recipients of the Personal Data 
Your personal data will be passed on to the following recipients:

- Health insurance companies (for billing purposes)
- Healthcare Professional (in the context of therapy)
- Hosting service provider (as part of order processing)
- Further service providers, if applicable, insofar as this is necessary for the fulfillment of the contract


## 5. Transfer to Third Countries
Your data will not be transferred to countries outside the European Economic Area.


## 6. Security
We take appropriate technical and organizational security measures to protect your data from manipulation, loss, destruction or unauthorized access by third parties. Our security measures are continuously improved in line with technological developments.


## 7. Use of Cookies
Cookies can be used to facilitate and improve the use of our service. These are small text files that are stored on your end device. No cookies are currently used and accordingly no data from cookies is stored. 


## 8. How Long We Store Your Personal Data
Your personal data will be deleted or blocked as soon as the purpose of storage no longer applies. Data may also be stored if this is provided for by European or national laws, regulations or other provisions to which we are subject.

## 9. Your Rights
You have the following rights with regard to your personal data:


### 9.1. Right to Information (Art. 15 GDPR)
You have the right to request confirmation from us as to whether we are processing personal data concerning you.


### 9.2. Right to Rectification (Art. 16 GDPR)
You have the right to obtain without undue delay the rectification of inaccurate personal data or the completion of incomplete personal data.


### 9.3. Right to Erasure (Art. 17 GDPR)
You have the right to obtain from us the erasure of personal data concerning you without undue delay where one of the grounds specified in Art. 17 GDPR applies.


### 9.4. Right to Restriction of Processing (Art. 18 GDPR)
You have the right to request the restriction of processing if one of the conditions of Art. 18 GDPR is met.


### 9.5. Right to Data Portability (Art. 20 GDPR)
You have the right to receive the personal data concerning you, which you have provided to us, in a structured, commonly used and machine-readable format and to transmit this data to another controller.


### 9.6. Right to Object (Art. 21 GDPR)
You have the right to object to the processing of your data at any time on grounds relating to your particular situation.


### 9.7. Revocation of Granted Consent
You can withdraw your consent at any time with effect for the future.


### 9.8. Right to Lodge a Complaint with a Supervisory Authority (Art. 77 GDPR)
You have the right to complain to a data protection supervisory authority about the processing of your personal data by us.


## 10. Links to Third Party Websites
Please note that our service may contain links to content from other providers to which this data protection notice does not apply. We have no influence on these websites and whether they comply with the applicable data protection regulations.


## 11. Up-to-dateness and Amendment of this Privacy Policy
This privacy policy was last updated on 23.01.2025. Due to the further development of our service or due to changes in legal or official requirements, it may become necessary to amend this privacy policy. The current privacy policy can be accessed and printed out on our website at any time.
 
Last update: 23.01.2025
