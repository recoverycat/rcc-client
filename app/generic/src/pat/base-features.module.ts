import	{	NgModule										}	from '@angular/core'

import	{

			RccServiceWorkerModule,
			QrCodeModule,
			ScheduledNotificationModule,
			RccTitleModule,

			// Settings
			RccTransmissionSettingsModule,

		}														from '@rcc/common'

import	{	environment										} 	from '../environments/environment'

import	{


			QrCodeScannerServiceModule,
			StaticQrCodeModule,
			IndexedDbModule,

			SymptomCheckViewModule,
			SymptomCheckQueryModule,
			PreConfiguredSymptomChecksModule,

			ImportSymptomCheckStoreServiceModule,
			ImportQuestionStoreServiceModule,

			CombinedTransmissionModule,

			JournalServiceModule,

			BasicQueryWidgetsModule,

			DayQueryModule,
			DayQueryRemindersModule,


			FulFillSessionRequestModule,

			// Entries
			EntryShareServiceModule,

			// Basics:
			BasicReportRepresentationModule,
			BasicReportPreparatorModule,
			BasicDataViewWidgetsModule,
			BasicSymptomCheckRepresentationModule,

			ComplexDataViewWidgetsModule,

			// Fallbacks:
			FallbackDataViewWidgetsModule,


			// Setting entry groups
			RccReminderSettingsEntryGroupModule,
			RccLanguageSettingsEntryGroupModule,
			RtcTransmissionModule,
			IcsRemindersServiceModule,

			ReportStackedViewModule,
			RccUserSupportDefaultDebugInfoModule,
			RccZammadModule,

			RccRuntimeStartupBannerModule,

			// Logging
			RccUsageLoggingModule,
			RccUsageLoggingUsertypeModule,
			RccUsageLoggingEngagementModule,
			RccUsageLoggingTransmissionFailureModule,
			RccUsageLoggingTransmissionSuccessModule,
			MatomoModule,
			StarterSymptomCheckStoreServiceModule,

		}										from '@rcc/features'

import	{	BasePatModule					}	from '../pat/base-pat.module'

import	{	MainMenuModule					}	from '../pat/main-menu.module'
import	{	HomePageModule					}	from '../pat/home-page.module'

import	{
			UpdateModule,
		}										from '@rcc/features/updates'
import	{
			RccUsageLoggingAveragePerDayModule
		}										from '@rcc/features/usage-logging-average-per-day'
import	{	RccUsageLoggingTotalTimeModule	}	from '@rcc/features/usage-logging-total-time'
import  {   SyncDataModule                  }   from '@rcc/features/sync-data'

@NgModule({
	imports: [
		BasePatModule,

		MainMenuModule,
		HomePageModule,

		IndexedDbModule,
		RccTitleModule,

		ScheduledNotificationModule,
		RccZammadModule.forRoot(
			{ url: 'https://zammad.recoverycat.de' }
		),
		RccUserSupportDefaultDebugInfoModule,


		RccServiceWorkerModule,
		QrCodeModule,
		StaticQrCodeModule,
		UpdateModule,

		QrCodeScannerServiceModule,

		JournalServiceModule,

		BasicQueryWidgetsModule,
		DayQueryModule,
		DayQueryRemindersModule,

		FulFillSessionRequestModule,

		SymptomCheckViewModule,
		SymptomCheckQueryModule,
		PreConfiguredSymptomChecksModule,

		StarterSymptomCheckStoreServiceModule,

		ImportSymptomCheckStoreServiceModule,
		ImportQuestionStoreServiceModule,
		SyncDataModule,

		// should use a different way to configure:
		CombinedTransmissionModule.forRoot(
			'wss://signaling.recoverycat.de',
			[
				'stun:stun.recoverycat.de'
			]
		),
		RtcTransmissionModule.forRoot(
			'wss://signaling.recoverycat.de',
			[
				'stun:stun.recoverycat.de'
			]
		),


		// BackupModule,

		// Entries
		EntryShareServiceModule,


		// Basics:
		BasicReportRepresentationModule,
		BasicReportPreparatorModule,
		BasicDataViewWidgetsModule,
		BasicSymptomCheckRepresentationModule,

		ComplexDataViewWidgetsModule,


		// Fallbacks:

		FallbackDataViewWidgetsModule,

		// Settings
		RccTransmissionSettingsModule,
		IcsRemindersServiceModule,

		// Setting entry groups
		RccReminderSettingsEntryGroupModule,
		RccLanguageSettingsEntryGroupModule,

		// statistics

		ReportStackedViewModule,

		RccRuntimeStartupBannerModule,

		// Logging
		RccUsageLoggingTransmissionFailureModule,
		RccUsageLoggingTransmissionSuccessModule,
		RccUsageLoggingModule,
		RccUsageLoggingUsertypeModule,
		RccUsageLoggingEngagementModule,
		RccUsageLoggingAveragePerDayModule,
		RccUsageLoggingTotalTimeModule,

		MatomoModule.forRoot({
			url: environment.matomo.url,
		}),

		// example data:

	],
})
export class RccBaseFeaturesModule{}
