# Allgemeine Geschäftsbedingungen (Mai 2023)


## Recovery Cat



### 1. Allgemeines, Geltungsbereich
1.1 Die Recovery Cat GmbH ("**Recovery Cat**"), Invalidenstraße 153, 10115 Berlin, hat die gleichnamige Browser-basierte Web-Applikation ("**Dienst**") zur Unterstützung der Therapieplanung und Symptom-Tagebuch-Erstellung für Patient:innen in psychiatrischer Behandlung ("**Patienten**") und deren Behandler:innen ("**Behandler**") entwickelt. Der Dienst besteht aus Elementen zur Präsentation von relevanten Fragen im Hinblick auf die psychiatrische Behandlung sowie zur Erfassung und Darstellung der Symptomverläufe.


1.2 Der Dienst ermöglicht es Behandlern, ihre Patienten zur Nutzung des Dienstes einzuladen. In einer für beide Seiten ähnlichen Benutzeransicht können Fragen zur Verlaufsdokumentation notiert und die Antworten visualisiert werden. Behandler und Patienten nutzen den Dienst per Aufrufen einer URL.


1.3 Zweck der von Recovery Cat angebotenen Leistungen ist ausschließlich die Bereitstellung von etablierten Ansätzen, konkret dem Führen eines Symptomtagebuchs in digitaler Form zur Verlaufsdokumentation und kurzfristigen Speicherung von Informationen. Bei sämtlichen von Recovery Cat angebotenen Leistungen handelt es sich nicht um eine medizinische Leistung, insbesondere nicht eine ärztliche Beratung, Diagnostik oder Therapie. Die Verwendung des Dienstes und die Erstellung von Fragen im Dienst verbleiben in der Verantwortung des Behandlers. Der Dienst stellt keinen Ersatz für eine Untersuchung, Diagnose oder Behandlung durch einen Arzt oder Therapeuten dar. Recovery Cat trägt keine Verantwortung für eine fehlerhafte oder unvollständige ärztliche oder therapeutische Leistung. Recovery Cat übernimmt keine Garantie für den Therapieerfolg.


1.4 Es handelt sich bei dem Dienst ausschließlich um ein passives Werkzeug zur Verlaufsdokumentation von Selbstbeobachtungen. Der Dienst ist nicht dazu bestimmt und auch nicht geeignet, Krankheiten zu diagnostizieren, zu verhüten, zu überwachen oder vorherzusagen. Auch kann der Dienst weder Prognosen erstellen, noch Krankheiten behandeln oder lindern. Eingaben zur Verlaufsdokumentation werden unverändert im Dienst abgebildet. Es werden keine inhaltlichen Zusammenfassungen erstellt. Ferner erfolgt keine Plausibilitätsprüfung von Benutzereingaben. Es gibt keine Einschränkung oder inhaltsbezogene Lenkung der Eingabemöglichkeiten innerhalb des Dienstes. Bei sämtlichen im Angebot von Recovery Cat enthaltenen Leistungen handelt es sich nicht um ein Medizinprodukt im Sinne der Verordnung (EU) 2017/745 über Medizinprodukte. Die Angebote von Recovery Cat sind nicht CE-zertifiziert.


1.5 Der Dienst wurde für angestellte oder selbstständig tätige Behandler im ambulanten, stationären oder teilstationären Behandlungssetting (online- und vor-Ort, alle Patientengruppen: Kinder, Jugendliche, Erwachsene – alle psychotherapeutischen Fachrichtungen) entwickelt und richtet sich an Fachpersonal aus psychiatrischen oder psychosomatischen Fachkliniken, spezialisierten Stationen in Krankenhäusern und Facheinrichtungen, Medizinische Versorgungszentren (MVZ), sozialpsychiatrische Einrichtungen, therapeutische/soziale Einrichtungen der Jugendhilfe, Wiedereingliederungshilfe und Rehaeinrichtungen.


1.6 Der Dienst ist nur auf der Basis einer tragfähigen, vertrauensvollen und stabilen Arbeitsbeziehung mit ausreichender Behandlungsmotivation sowie Einsichtsfähigkeit beim Patienten zu verwenden. Der Dienst ist ausdrücklich nicht für den Einsatz bei akuten Krisen konzipiert. Es ist nicht vorgesehen, dass mit dem Einsatz des Dienstes in Situationen mit extrem hoher Erregung oder Anspannung begonnen wird. Der Dienst dient nicht als asynchrones Kommunikationstool für die Zeit zwischen den Behandlungsterminen. Der Behandler hat außerhalb der jeweiligen Sitzung keinen Zugriff auf die Daten seiner Patienten.


1.7 Der Kunde ("Kunde") möchte den bei ihm tätigen Behandelnden die Nutzung des Dienstes im Rahmen der Behandlung ihrer Patienten ermöglichen.


1.8 Diese Allgemeinen Geschäftsbedingungen ("**AGB**") in ihrer zum Zeitpunkt des Vertragsschlusses gültigen Fassung gelten für die von Recovery Cat im Zusammenhang mit dem Dienst gegenüber dem Kunden erbrachten Leistungen. Der Vertrag über die Leistungen der Recovery Cat ("**Vertrag**") kommt durch Bestätigung des von Recovery Cat unterbreiteten Angebots ("**Angebot**") innerhalb der im Angebot genannten Frist durch den Kunden zustande. Indem der Kunde das Angebot annimmt, erklärt er sich mit der Geltung dieser AGB einverstanden und die AGB werden Bestandteil des Vertrags. Die Zustimmung zu diesen AGB ist Voraussetzung für die Inanspruchnahme der Leistungen. Es gelten ausschließlich die AGB von Recovery Cat. Allgemeine Geschäftsbedingungen des Kunden werden auch dann nicht Vertragsbestandteil, wenn der Kunde darauf Bezug nimmt und Recovery Cat deren Geltung nicht widerspricht.



### 2. Leistungen der Recovery Cat
2.1 Recovery Cat hält den Dienst über eine URL für Behandler ("**Behandleransicht**") sowie eine URL für Patienten ("**Patientenansicht**") bereit.


2.2 Der Dienst wird über eine browserbasierte Web-Applikation angeboten, auf die über das Internet zugegriffen werden kann.


2.3 Der Umfang des Dienstes richtet sich danach, welche Funktionen Recovery Cat verfügbar macht. Zur Verfügung stehende Funktionen können sein:


2.3.1 Behandleransicht:

2.3.1.2 Fragen für die Verlaufserhebung (Therapie Follow-up) mit dem Patienten teilen,

2.3.1.3 Daten des Patienten empfangen,

2.3.1.4 Daten (als Report) exportieren.

2.3.2 Patientenansicht:

2.3.2.1 Therapie Follow-up empfangen,

2.3.2.2 Fragen (aus dem Therapie Follow-up) beantworten,

2.3.2.3 Therapieverlauf / Daten aus dem Therapie Follow-up mit dem Behandler teilen,

2.3.2.4 Therapieverlauf / Daten aus dem Therapie Follow-up ansehen.

Der konkrete Leistungsumfang ergibt sich aus der dem Angebot beigefügten Leistungsbeschreibung.


2.4 Recovery Cat entwickelt den Dienst und die zugrundeliegende Software sowie deren Inhalte kontinuierlich weiter. Recovery Cat hat das Recht, jederzeit Aktualisierungen und neue Versionen zur Verbesserung des Dienstes zu implementieren. Recovery Cat kann Änderungen jeglicher Art an dem Dienst oder an der Art und Weise, wie der Dienst bereitgestellt wird, vornehmen. Recovery Cat behält sich vor, den Funktionsumfang des Dienstes jederzeit zu ändern oder zu ergänzen oder ganz oder teilweise zu beenden oder den Zugang zum Dienst zu beschränken. Über nicht unerhebliche Beeinträchtigungen der Zugriffsmöglichkeit oder der Nutzbarkeit des Dienstes wird Recovery Cat den Kunden innerhalb einer angemessenen Frist vor dem Zeitpunkt der Änderung informieren.



### 3. Zugangsvoraussetzungen
3.1 Der Dienst ist als Web-Applikation programmiert. Der Zugang zum Dienst erfordert den Einsatz von Hard- und Software, die die jeweils mitgeteilten technischen Mindestanforderungen erfüllen, d.h. ein internetfähiges Endgerät (Smartphone, Tablet oder Computer) mit Kamera (an Patient:innen Smartphone), ausreichend Speicherplatz und kompatibler Webbrowser-Software (Chrome, Firefox, Edge, Opera). Unter Umständen muss zusätzliche Software heruntergeladen oder installiert werden (z. B. bestimmte Software-Plug-ins oder Software-Anwendungen), um auf den Dienst oder bestimmte Funktionen zugreifen zu können. Der Dienst ist während des Teilens oder Empfangens von Daten nur mit bestehender Internetverbindung verwendbar, außerhalb des Teilens oder Empfangens von Daten ist eine Nutzung des Dienstes auch offline möglich.


3.2 Die Erfüllung der Zugangsvoraussetzungen obliegt dem Kunden.



### 4. Mitwirkungs- und Aufklärungspflichten des Kunden
4.1 Der Kunde wird Recovery Cat bei der Erbringung der Leistungen nach diesem Vertrag in dem erforderlichen Maße unterstützen.


4.2 Zur Dokumentation der Daten (Reports) im Patientenverwaltungssystem hat der Kunde Sorge dafür zu tragen, dass die technischen Voraussetzungen für einen Import zur Verfügung stehen.


4.3 Der Dienst darf ausschließlich zu dem Zweck genutzt, zu dem der Zugang zum Dienst gewährt wurde. Jede darüber hinaus gehende Nutzung des Dienstes ist untersagt.

Es ist Recovery Cat nicht möglich, von Behandlern erstellte Fragen oder von Patienten gemachte Angaben zu kontrollieren. Dies gilt insbesondere hinsichtlich der Richtigkeit, Rechtmäßigkeit, Aktualität und Qualität sowie der Eignung für einen bestimmten Zweck. Der Kunde hat insbesondere dafür Sorge zu tragen, dass keine Inhalte im Dienst eingestellt werden, die

4.4.1 strafbar sein können (insbesondere Volksverhetzung, Beleidigung, Verleumdung, Bedrohung),

4.4.2 als pornographisch, vulgär oder obszön, belästigend oder in sonstiger Weise anstößig anzusehen sind,

4.4.3 verfassungsfeindlich, extremistisch, rassistisch oder fremdenfeindlich sind oder Inhalte darstellen, die von verbotenen Gruppierungen stammen,

4.4.4 in Rechte Dritter (insbesondere Persönlichkeitsrechte, Urheberrechte, Markenrechte, Patentrechte oder sonstige Rechte Dritter) eingreifen.


4.5 Der Kunde hat dafür Sorge zu tragen, dass Behandler und Patienten den Dienst so nutzen, dass keine Beeinträchtigungen, Überlastungen oder Schäden auftreten und der mit dem Dienst verfolgte Zweck weder gefährdet noch umgangen wird. Der Kunde wird weder selbst noch durch Dritte Sicherheitsvorkehrungen des Dienstes umgehen oder verändern.


4.6 Recovery Cat weist darauf hin, dass die Nutzung des Dienstes Behandler nicht von der Einhaltung der berufsrechtlichen und sonstiger gesetzlicher Vorschriften entbindet, einschließlich der nachvollziehbaren Dokumentation der Behandlung nach §§ 630 ff. BGB. Insbesondere kann der Dienst das von der ärztlichen Sorgfaltspflicht bei der Therapieplanung zu führende Aufklärungsgespräch nicht ersetzen, sondern dient lediglich zur Unterstützung der Therapieplanung und -dokumentation. Auch bei der Verwendung und Auswertung der Daten verbleibt die Verantwortung für eine vollständige und zutreffende Therapieplanung und deren rechtskonformer Dokumentation, Ablage und Archivierung beim Behandler, einschließlich der Anpassung an neue wissenschaftliche Erkenntnisse oder rechtliche Anforderungen. Eine Haftung der Recovery Cat für die Folgen von Dokumentationsmängeln beim Kunden wird ausgeschlossen.


4.7 Der Kunde hat dafür Sorge zu tragen, dass die Behandler die vorstehenden Pflichten einhalten und insbesondere ihre Patienten über die Nutzung des Dienstes aufklären. Recovery Cat stellt zur Unterstützung eine Informationsbroschüre mit Nutzungshinweisen bereit.



### 5. Rechteeinräumung durch Recovery Cat
5.1 Alle Rechte am Dienst und seinen Funktionen sowie an der zugrunde liegenden Software, einschließlich damit zusammenhängender Software, Datenbanken, Grafiken, Benutzeroberflächen, Designs sowie an anderen Inhalten, Bezeichnungen, Namen und Marken verbleiben bei Recovery Cat und bei etwaigen Dritten, die Recovery Cat Rechte im Zusammenhang mit dem Betrieb des Dienstes eingeräumt haben, soweit sich aus den nachstehenden Ziffern nichts Abweichendes ergibt.


5.2 Der Kunde erhält ein entgeltliches, nicht ausschließliches, nicht übertragbares, zeitlich auf die Laufzeit des Vertrags beschränktes Recht, den Dienst bestimmungsgemäß in seinem Klinikbetrieb zu nutzen. Der Kunde ist berechtigt, an die bei ihm tätigen Behandler und deren Patienten Unterlizenzen zu vergeben und ihnen den Dienst zur Nutzung zur Verfügung zu stellen. Rechte, die dem Kunden nicht ausdrücklich eingeräumt werden, stehen ihm vorbehaltlich der Ziffer 5.3 nicht zu.


5.3 Teilen des Dienstes liegt die sog. MIT-Lizenz zugrunde, eine freizügige Open Source Software Lizenz. Sofern und soweit die MIT-Lizenz eine über Ziffer 5.2 hinausgehende Rechteeinräumung vorsieht, stehen dem Kunden die gemäß der MIT-Lizenz vorgesehenen Rechte zu.



### 6. Service-Level-Agreement
6.1 Die Parteien vereinbaren für die Verfügbarkeit des Dienstes das Service Level Agreement in [Anlage 1](terms-and-conditions/).


6.2 Eine etwaige verschuldensunabhängige Haftung von Recovery Cat für einen bei Vertragsschluss vorhandenen Mangel des Dienstes gemäß § 536a Abs. 1 Alt. 1 BGB wird ausgeschlossen, soweit es sich nicht um eine ausdrücklich zugesicherte Eigenschaft handelt oder Recovery Cat den Mangel arglistig verschwiegen hat.



### 7. Vertragsbeginn, Laufzeit, Kündigung
7.1 Der Vertrag beginnt mit Vertragsschluss durch Bestätigung des Angebots durch den Kunden, sofern im Angebot kein abweichendes Datum angegeben ist. Die Mindestvertragslaufzeit ist im Angebot angegeben. Der Vertrag verlängert sich nach Ablauf der Mindestvertragslaufzeit um jeweils weitere 12 Monate ("Vertragsjahr"), wenn er nicht spätestens drei Monate vor Ablauf des jeweils laufenden Vertragsjahrs von einer Partei gekündigt wird.


7.2 Das Recht der Parteien zur außerordentlichen Kündigung aus wichtigem Grund bleibt unberührt.


7.3 Im Falle der Kündigung wird Recovery Cat die gekündigten Leistungen einstellen. Der Kunde hat dafür Sorge zu tragen, dass die bei ihm tätigen Behandler die Nutzung des Dienstes zum Vertragsende einstellen.



### 8. Vergütung
8.1 Für die von Recovery Cat erbrachten Leistungen nach diesem Vertrag zahlt der Kunde für die Laufzeit des Vertrags eine jährliche Vergütung in Höhe des im Angebot ausgewiesenen Netto-Betrags zzgl. der gesetzlichen Umsatzsteuer, soweit diese anfällt.


8.2 Die Vergütung gemäß Ziffer 8.1 ist innerhalb von 14 Tagen nach Beginn des laufenden Vertragsjahrs gemäß Ziffer 7.1 auf das im Angebot angegebene Bankkonto von Recovery zu zahlen. Recovery Cat wird dem Kunden die zu zahlenden Beträge in Rechnung stellen. Recovery Cat ist berechtigt, die jährliche Vergütung nach Ablauf der Mindestvertragslaufzeit gemäß Ziffer 7.1 in angemessener Weise zu erhöhen. Über eine beabsichtigte Erhöhung der Vergütung informiert Recovery Cat den Kunden vor Beginn des neuen Vertragsjahrs. Dem Kunden steht in diesem Fall das Recht zu, den Vertrag auch nach Ablauf der Kündigungsfrist gemäß Ziffer 7.1 vor Beginn des neuen Vertragsjahrs zu kündigen; kündigt der Kunde den Vertrag nicht, gilt für das neue Vertragsjahr die angekündigte erhöhte Vergütung als vereinbart.


8.3 Als Zeitpunkt der Zahlung gilt der Tag, an dem der fällige Vergütungsbetrag auf dem Bankkonto von Recovery Cat gutgeschrieben ist. Etwaige Kosten der Banküberweisung trägt der Kunde.


8.4 Gerät der Kunde mit der Zahlung der Vergütung ganz oder teilweise mehr als 14 (vierzehn) Tage in Verzug, so ist Recovery Cat nach vorheriger Aufforderung an den Kunden per E-Mail und erfolglosem Verstreichen einer Zahlungsfrist von 5 (fünf) Tagen, gerechnet ab dem Datum der Absendung der E-Mail, berechtigt, die Erbringung der Leistungen nach diesem Vertrag auszusetzen, bis alle ausstehenden Beträge bezahlt sind. Weitere Rechte der Recovery Cat bleiben unberührt.



### 9. Haftung und Freistellung
9.1 Recovery Cat haftet unbeschadet der nachfolgenden Haftungsbeschränkungen uneingeschränkt für Schäden an Leben, Körper und Gesundheit, die auf einer fahrlässigen oder vorsätzlichen Pflichtverletzung von Recovery Cat, ihrer gesetzlichen Vertreter oder Erfüllungsgehilfen beruhen, sowie für Schäden, die auf vorsätzlichen oder grob fahrlässigen Vertragsverletzungen sowie Arglist von Recovery Cat, ihrer gesetzlichen Vertreter oder Erfüllungsgehilfen beruhen, sowie für Schäden, die von der Haftung nach dem Produkthaftungsgesetz umfasst werden.


9.2 Bei Fahrlässigkeit beschränkt sich die Haftung von Recovery Cat auf den Ersatz des typischen vorhersehbaren Schadens. Bei einfacher Fahrlässigkeit haftet Recovery Cat nur, wenn sie eine Pflicht verletzt hat, deren Erfüllung die Erreichung des Vertragszwecks überhaupt erst ermöglicht und auf deren Einhaltung der Kunde vertrauen durfte. Bei fahrlässiger Verletzung nicht vertragswesentlicher Nebenpflichten haftet Recovery Cat nicht.


9.3 Eine weitergehende Haftung von Recovery Cat ist ohne Rücksicht auf die Rechtsnatur des geltend gemachten Anspruchs ausgeschlossen. Soweit die Haftung von Recovery Cat ausgeschlossen oder beschränkt ist, gilt dies auch für die persönliche Haftung ihrer Angestellten, Arbeitnehmer, Mitarbeiter, Vertreter und Erfüllungsgehilfen.


9.4 Recovery Cat übernimmt keine Garantie für den Therapieerfolg. Recovery Cat haftet nicht für fehlerhafte oder unvollständige ärztliche oder therapeutische Leistungen und haftet nicht für Schäden, die aus der nicht bestimmungsgemäßen Nutzung des Dienstes entstehen.


9.5 Der Kunde hat Recovery Cat von allen angemessenen Kosten und Aufwendungen (einschließlich der Aufwendungen für die Rechtsverfolgung und Rechtsverteidigung) freizustellen, die Recovery Cat dadurch entstehen, dass Dritte gegen Recovery Cat Ansprüche aufgrund der schuldhaften Verletzung der dem Kunden aufgrund dieses Vertrages oder von Gesetzes wegen obliegenden Verpflichtungen geltend machen.



### 10. Datenschutz
10.1 Die Parteien verpflichten sich, die für sie geltenden datenschutzrechtlichen Bestimmungen einzuhalten.


10.2 Die Parteien gehen davon aus, dass der Kunde datenschutzrechtlich Verantwortlicher für die von Behandlern und Patienten im Dienst angegebenen personenbezogenen Daten ist. Sofern und soweit Recovery Cat personenbezogene Daten im Auftrag des Kunden verarbeitet, gilt der Auftragsverarbeitungsvertrag (Anlage 2).



### 11. Schlussbestimmungen
11.1 Es gilt das Recht der Bundesrepublik Deutschland unter Ausschluss des UN-Kaufrechts (CISG).


11.2 Als Gerichtsstand für sämtliche Streitigkeiten aus oder im Zusammenhang mit dem Vertrag vereinbaren die Parteien Berlin.


11.3 Sollten einzelne Bestimmungen dieser AGB unwirksam oder undurchführbar sein oder werden, so wird dadurch die Wirksamkeit des Vertrags im Übrigen nicht berührt. An die Stelle der undurchführbaren oder unwirksamen Bestimmung tritt die jeweilige gesetzliche Vorschrift. Sofern Gesetzesrecht im jeweiligen Fall nicht zur Verfügung steht, soll diejenige wirksame oder durchführbare Regelung gelten, deren Wirkungen dem ursprünglich von Recovery Cat und dem Kunden verfolgten Zweck am nächsten kommt. Entsprechendes gilt für den Fall einer Regelungslücke.


11.4 Recovery Cat stellt dem Kunden die AGB in elektronischer Form zur Verfügung.


11.5 Recovery Cat behält sich das Recht vor, diese AGB jederzeit ohne Angabe von Gründen mit Wirkung für die Zukunft zu ändern oder zu ergänzen, wenn

11.5.1 dies zur Anpassung an geänderte, von Recovery Cat nicht zu vertretende Umstände (insbesondere Gesetzesänderungen und bei Vertragsschluss nicht vorhersehbare Schwierigkeiten bei der Durchführung des Nutzungsvertrages) erforderlich und die Änderung für den Kunden zumutbar ist;

11.5.2 die Änderung lediglich vorteilhaft für den Kunden ist;

11.5.3 wenn die Änderungen oder Anpassungen ohne wesentlichen Einfluss auf Funktionen des Dienstes oder rein technischer oder organisatorischer Art sind;

11.5.4 Recovery Cat Neuerungen am Dienst vornimmt, zusätzliche, gänzlich neue Dienste oder Leistungen einführt, die einer Beschreibung in den Bedingungen bedürfen, es sei denn, dass dies für das laufende Vertragsverhältnis nachteilig wäre.

Eine beabsichtigte Änderung wird dem Kunden an die der Recovery Cat zuletzt überlassene E-Mail-Adresse zugesandt. Die Änderung tritt spätestens 14 Tage, nachdem der Kunde auf die Änderung aufmerksam gemacht wurde, in Kraft, wenn der Kunde der Änderung nicht innerhalb von 14 Tagen widerspricht. Ein Widerspruch wird durch den Kunden an die zuletzt von Recovery Cat überlassene E-Mail-Adresse zugesandt.


11.6 Im Falle eines Widerspruchs innerhalb der in Ziffer 11.5 genannten Frist kann Recovery Cat das Vertragsverhältnis mit dem Kunden fristlos kündigen. Sollte Recovery Cat das bisherige Vertragsverhältnis nach dem wirksamen Widerspruch nicht kündigen, so behalten die bisherigen AGB im Verhältnis zum Kunden unverändert ihre Geltung.
