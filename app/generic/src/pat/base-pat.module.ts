import	{	NgModule							}	from '@angular/core'
import	{
			provideTranslationMap,
			provideLogoVariants,
			LogoVariants,
		}											from '@rcc/common'

import	{	RccTitleService						}	from '@rcc/common'

import en from './i18n/en.json'
import de from './i18n/de.json'



export const PATLogoVariants: LogoVariants = {
	full: 'assets/svg/rcc-logo-pat-full.svg',
	small: 'assets/svg/rcc-logo-pat-small.svg',
	minimal: 'assets/svg/rcc-logo-pat-minimal.svg'
}


@NgModule({

	providers: [
		provideTranslationMap(null, { en,de }),
		provideLogoVariants(PATLogoVariants),
	]
})

export class BasePatModule {
	public constructor(private rccTitleService: RccTitleService) {
		this.rccTitleService.setTitle('PAGE_TITLE_PREFIX')
	}
}
