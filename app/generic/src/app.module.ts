import	{	NgModule						}	from '@angular/core'
import	{
			IonicBaseAppComponent,
			IonicBaseAppModule
		}										from '@rcc/ionic'

import	{	RccRoutingModule				}	from '@rcc/common'

import	{	RccFeaturesModule				}	from './features/features.module'
import { StaticQrCodeModule } from '@rcc/features/static-qr-code'


@NgModule({
	imports:	[
					IonicBaseAppModule,
					RccFeaturesModule,
					RccRoutingModule,
		StaticQrCodeModule
				],
	bootstrap: [IonicBaseAppComponent]
})
export class AppModule { }
