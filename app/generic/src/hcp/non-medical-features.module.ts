import	{	NgModule									}	from '@angular/core'
import	{	RccHcpUserManualModule,
			CuratedAdhdSymptomCheckStoreServiceModule	}	from '@rcc/features'
import	{	RccLegalModule								}	from '../hcp/legal'

@NgModule({
	imports: [
		RccHcpUserManualModule,
		RccLegalModule,

		// Template which is only available in base but not in hcp-med
		CuratedAdhdSymptomCheckStoreServiceModule,

	]
})
export class NonMedicalFeaturesModule {}
