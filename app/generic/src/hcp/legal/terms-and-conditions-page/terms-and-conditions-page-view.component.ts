import 	{ 	Component 			} 					from '@angular/core'
import	{
			TranslationsModule,
			RccMarkdownComponent,
			RccColorCategoryDirective
		} 											from '@rcc/common'

import	{
			RccFullPageComponent,
			RccScrollToTopButtonComponent
		}											from '@rcc/themes/active'

import de from './i18n/terms_de.md'
import en from './i18n/terms_en.md'


@Component({
	templateUrl	: './terms-and-conditions-page-view.component.html',
	selector	: 'terms-and-conditions-markdown',
	styleUrls	:	['../legal-common.scss'],
	standalone	:	true,
	imports		:	[
						TranslationsModule,
						RccMarkdownComponent,
						RccColorCategoryDirective,
						RccFullPageComponent,
						RccScrollToTopButtonComponent
					]


})
export class TermsAndConditionsPageViewComponent {
	public translations					: Record<string, Record<string, string>>
										= { translations: { en, de } }
}
