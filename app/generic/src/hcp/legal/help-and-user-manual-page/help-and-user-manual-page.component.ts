import	{	Component				}	from '@angular/core'
import	{	gotoSupportFormAction	}	from '@rcc/features/user-support'
import	{	Action					}	from '@rcc/common'

@Component({
	templateUrl:	'./help-and-user-manual-page.component.html',
	styleUrls: ['../legal-common.scss', './help-and-user-manual-page.component.scss'],
	standalone: false,
})
export class HelpAndUserManualPageComponent {
	public gotoSupportFormAction : Action = gotoSupportFormAction
}
