import 	{ 	Component 			} 					from '@angular/core'
import	{
			TranslationsModule,
			RccMarkdownComponent,
			RccColorCategoryDirective
		} 											from '@rcc/common'

import	{
			RccFullPageComponent,
			RccScrollToTopButtonComponent
		}											from '@rcc/themes/active'

import de from './i18n/privacy_de.md'
import en from './i18n/privacy_en.md'

@Component({
	templateUrl	: './privacy-policy-page-view.component.html',
	selector	: 'privacy-policy-markdown',
	styleUrls	:	['../legal-common.scss'],
	standalone	:	true,
	imports		:	[
						TranslationsModule,
						RccMarkdownComponent,
						RccColorCategoryDirective,
						RccFullPageComponent,
						RccScrollToTopButtonComponent
					]

})
export class PrivacyPolicyPageViewComponent {
	public translations					: Record<string, Record<string, string>>
										= { translations: { en, de } }

}
