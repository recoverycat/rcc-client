
# Datenschutzerklärung | Recovery Cat Base

Die Recovery Cat GmbH ("Recovery Cat", "wir", "uns", "unser") hat die Browser-basierte Web-Applikation Recovery Cat Base ("Dienst") (abrufbar über die im Vertrag angegebene URL) zur Erstellung von Symptomtagebüchern für Patient:innen in psychiatrischer Behandlung und deren Behandler:innen entwickelt. Recovery Cat schützt Ihre Privatsphäre und Ihre personenbezogenen Daten. Nachfolgend informieren wir Sie ausführlich über die Verarbeitung Ihrer Daten.


## 1. Verantwortliche
Soweit wir nicht als Auftragsverarbeiter für die Einrichtung agieren, bei der Sie als Behandler:in tätig sind, ist „Verantwortliche“ für die Erhebung und Verarbeitung Ihrer personenbezogenen Daten im Sinne des Art. 4 Nr. 7 der EU-Datenschutzgrundverordnung (Verordnung (EU) 2016/679) ("DSGVO") die

Recovery Cat GmbH\
Invalidenstr. 153\
10115 Berlin\
[hello@recovery.cat](mailto:hello@recovery.cat)\
[+49 (0) 30 209 654 055](tel:+49030209654055)

Weitere Informationen zu uns können Sie dem [Impressum](/imprint) entnehmen.


## 2. Datenschutzbeauftragter
Bei Fragen zu der Verarbeitung von personenbezogenen Daten durch Recovery Cat können Sie unseren Datenschutzbeauftragten kontaktieren:

[datenschutz@recovery.cat](mailto:datenschutz@recovery.cat)


## 3. Datenverarbeitungen
### 3.1. Verarbeitung der von Ihnen eingegebenen Daten
#### 3.1.1. Patient:innen
Sie können Therapie-Follow-ups von Ihren Behandler:innen empfangen, Fragen aus dem Therapie-Follow-up beantworten, Ihren Therapieverlauf einsehen und mit Ihren Behandler:innen teilen. Die von Ihnen gemachten Angaben enthalten Gesundheitsdaten.

##### Rechtsgrundlage: 
Die Verarbeitung Ihrer Gesundheitsdaten erfolgt auf Grundlage Ihrer Einwilligung gemäß Art. 6 Abs. 1 lit. a) DSGVO in Verbindung mit Art. 9 Abs. 2 lit. a) DSGVO.

##### Kategorien personenbezogener Daten: 
Gesundheitsdaten, Nutzungsdaten.

##### Speicherdauer: 
Die Daten werden zunächst ausschließlich lokal auf Ihrem Endgerät gespeichert. Eine Verarbeitung durch uns erfolgt erst, wenn Sie die Daten mit Ihren Behandler:innen teilen.

##### Empfänger: 
Ihre Behandler:innen können die Daten auf Ihrem Endgerät im Klartext einsehen, mit Ihnen auswerten und zu Dokumentationszwecken lokal speichern.

##### Pflicht zur Bereitstellung der Daten: 
Die Bereitstellung der Daten ist freiwillig. Ohne diese Daten können Sie jedoch die Funktionen des Dienstes nicht vollumfänglich nutzen.


#### 3.1.2. Behandler:innen
Sie können Fragen für die Verlaufserhebung (Therapie-Follow-up) für Ihre Patient:innen erstellen und diese mit ihnen teilen, Daten Ihrer Patient:innen empfangen sowie die empfangenen Daten exportieren. Sofern Sie bei der Erstellung von Therapie-Follow-ups Angaben zu Ihrer Person machen, z.B. Ihren Namen vermerken, verarbeiten wir diese Daten.

##### Rechtsgrundlage:
Art. 6 Abs. 1 lit. b) DSGVO (Vertragserfüllung).
Kategorien personenbezogener Daten: 
Kontaktdaten, berufliche Informationen.

##### Empfänger: 
Die Verarbeitung dieser verschlüsselten Daten erfolgt durch uns im Rahmen einer Auftragsverarbeitung für Ihre Einrichtung.

##### Pflicht zur Bereitstellung der Daten: 
Die Bereitstellung Ihrer Daten ist für die Nutzung 
des Dienstes erforderlich.


### 3.2. Verarbeitung im Falle Ihrer Kontaktaufnahme
Bei der Kontaktaufnahme mit uns (z.B. per E-Mail oder Telefon) werden Ihre Angaben zwecks Bearbeitung der Anfrage und für den Fall von Anschlussfragen gespeichert.

#### Rechtsgrundlage: 
Art. 6 Abs. 1 lit. b) DSGVO (Vertragserfüllung bzw. vorvertragliche Maßnahmen).

#### Kategorien personenbezogener Daten: 
Kontaktdaten, Kommunikationsinhalte.

#### Speicherdauer: 
Wir löschen die Anfragen, sofern diese nicht mehr erforderlich sind.

#### Pflicht zur Bereitstellung der Daten: 
Die Bereitstellung der Daten ist freiwillig und erfolgt nur, wenn Sie mit Recovery Cat in Kontakt treten. 


### 3.3. Logfiles
Beim Besuch unseres Dienstes werden automatisch Informationen in sogenannten Server-Logfiles erhoben und gespeichert, die Ihr Browser automatisch an uns übermittelt.

#### Verarbeitete Daten:
- Browsertyp und -version
- Betriebssystem
- Anonymisierte IP-Adresse
- Zugriffsdatum und -uhrzeit
- Übertragene Datenmenge
- Referrer-URL
- Verbindungsdaten und -quellen

#### Zweck: 
Gewährleistung eines reibungslosen Verbindungsaufbaus, Systemsicherheit und -stabilität sowie zu administrativen Zwecken.

#### Rechtsgrundlage: 
Art. 6 Abs. 1 lit. f) DSGVO. Unser berechtigtes Interesse folgt aus den genannten Zwecken.

#### Empfänger: 
Unsere Hosting-Dienstleister.

#### Speicherdauer: 
Die Logfiles werden für maximal 9 Monate gespeichert und danach gelöscht.

#### Widerspruchsmöglichkeit: 
Die Datenverarbeitung ist für den Betrieb des Dienstes erforderlich. Ein Widerspruchsrecht besteht daher nicht.

#### Pflicht zur Bereitstellung der Daten: 
Die Bereitstellung ist weder gesetzlich noch vertraglich vorgeschrieben, jedoch für den Betrieb des Dienstes technisch erforderlich.


### 3.4. Nutzungsanalyse (Matomo)
Wir nutzen Matomo, eine Open-Source-Software zur statistischen Auswertung der Nutzungszugriffe. Matomo analysiert Ihre Nutzung unseres Dienstes, ohne dabei Cookies zu verwenden.

#### Verarbeitete Daten:
- Anonymisierte IP-Adresse
- Referrer-URL
- Aufgerufene Seiten
- Aufenthaltsdauer
- Geräteinformationen (Browser, Betriebssystem, Einstellungen)

#### Zweck: 
Analyse des Nutzungsverhaltens zur Optimierung unseres Dienstes.

#### Rechtsgrundlage: 
Art. 6 Abs. 1 lit. f) DSGVO. Unser berechtigtes Interesse liegt in der Verbesserung unseres Angebotes.

#### Empfänger: 
Die Daten werden auf unseren Servern gespeichert und nicht an Dritte weitergegeben.

#### Speicherdauer: 
Die Daten werden bis zu zwei Jahre gespeichert.

#### Widerspruchsmöglichkeit: 
Sie können die Speicherung der Daten durch eine entsprechende Einstellung Ihrer Browser-Software verhindern. Dadurch kann die Funktionalität des Dienstes eingeschränkt sein.

#### Pflicht zur Bereitstellung der Daten: 
Die Bereitstellung ist freiwillig. Ohne die Verarbeitung kann es jedoch zu Einschränkungen in der Funktionalität des Dienstes kommen.


### 3.5. Automatisierte Entscheidungsfindung
Wir setzen keine automatisierte Entscheidungsfindung oder Profiling gemäß Art. 22 DSGVO ein.


## 4. Empfänger oder Kategorien von Empfängern der personenbezogenen Daten 
Ihre personenbezogenen Daten werden an folgende Empfänger weitergegeben:

- Behandler:in (im Rahmen der Therapie)
- Hosting-Dienstleister (im Rahmen einer Auftragsverarbeitung)
- Ggf. weitere Dienstleister, soweit dies zur Vertragserfüllung erforderlich ist


## 5. Übermittlung in Drittländer
Eine Übermittlung Ihrer Daten in Länder außerhalb des Europäischen Wirtschaftsraums findet nicht statt.


## 6. Sicherheit
Wir treffen angemessene technische und organisatorische Sicherheitsmaßnahmen, um Ihre Daten vor Manipulation, Verlust, Zerstörung oder unberechtigtem Zugriff Dritter zu schützen. Unsere Sicherheitsmaßnahmen werden entsprechend der technologischen Entwicklung fortlaufend verbessert.


## 7. Verwendung von Cookies
Cookies können verwendet werden, um die Nutzung unseres Dienstes zu erleichtern und zu verbessern. Es handelt sich um kleine Textdateien, die auf Ihrem Endgerät gespeichert werden. Aktuell werden keine Cookies genutzt und entsprechend auch keine Daten aus Cookies gespeichert. 


## 8. Wie lange wir Ihre personenbezogenen Daten aufbewahren
Ihre personenbezogenen Daten werden gelöscht oder gesperrt, sobald der Zweck der Speicherung entfällt. Eine Speicherung kann darüber hinaus erfolgen, wenn dies durch europäische oder nationale Gesetze, Verordnungen oder sonstige Vorschriften, denen wir unterliegen, vorgesehen wurde.


## 9. Ihre Rechte
Sie haben folgende Rechte hinsichtlich der Sie betreffenden personenbezogenen Daten:


### 9.1. Recht auf Auskunft (Art. 15 DSGVO)
Sie haben das Recht, von uns eine Bestätigung darüber zu verlangen, ob wir personenbezogene Daten verarbeiten, die Sie betreffen.


### 9.2. Recht auf Berichtigung (Art. 16 DSGVO)
Sie haben das Recht, unverzüglich die Berichtigung unrichtiger oder die Vervollständigung unvollständiger personenbezogener Daten zu verlangen.


### 9.3. Recht auf Löschung (Art. 17 DSGVO)
Sie können von uns verlangen, dass die Sie betreffenden personenbezogenen Daten unverzüglich gelöscht werden, sofern einer der in Art. 17 DSGVO genannten Gründe zutrifft.


### 9.4. Recht auf Einschränkung der Verarbeitung (Art. 18 DSGVO)
Sie haben das Recht, die Einschränkung der Verarbeitung zu verlangen, wenn eine der Voraussetzungen des Art. 18 DSGVO gegeben ist.


### 9.5. Recht auf Datenübertragbarkeit (Art. 20 DSGVO)
Sie haben das Recht, die Sie betreffenden personenbezogenen Daten, die Sie uns bereitgestellt haben, in einem strukturierten, gängigen und maschinenlesbaren Format zu erhalten und diese Daten einem anderen Verantwortlichen zu übermitteln.


### 9.6. Widerspruchsrecht (Art. 21 DSGVO)
Sie haben das Recht, aus Gründen, die sich aus Ihrer besonderen Situation ergeben, jederzeit gegen die Verarbeitung Ihrer Daten Widerspruch einzulegen.


### 9.7. Widerruf erteilter Einwilligungen
Sie können eine erteilte Einwilligung jederzeit mit Wirkung für die Zukunft widerrufen.


### 9.8. Recht auf Beschwerde bei einer Aufsichtsbehörde (Art. 77 DSGVO)
Sie haben das Recht, sich bei einer Datenschutzaufsichtsbehörde über die Verarbeitung Ihrer personenbezogenen Daten durch uns zu beschweren.


## 10. Links zu Webseiten Dritter
Bitte beachten Sie, dass unser Dienst Verlinkungen zu Inhalten anderer Anbieter enthalten kann, für die diese Datenschutzerklärung nicht gelten. Wir haben keinen Einfluss auf diese Webseiten und darauf, ob diese die geltenden Datenschutzbestimmungen einhalten.


## 11. Aktualität und Änderung dieser Datenschutzerklärung
Diese Datenschutzerklärung hat den Stand vom 23.01.2025. Durch die Weiterentwicklung unseres Dienstes oder aufgrund geänderter gesetzlicher bzw. behördlicher Vorgaben kann es notwendig werden, diese Datenschutzerklärung zu ändern. Die jeweils aktuelle Datenschutzerklärung kann jederzeit auf unserer Website abgerufen und ausgedruckt werden.
Soweit in diesen Datenschutzhinweisen nicht anderweitig ausgeführt, übermitteln wir Ihre Daten nicht an Länder außerhalb des Europäischen Wirtschaftsraums.

Letzte Aktualisierung: 23.01.2025 
