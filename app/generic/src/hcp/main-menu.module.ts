import	{	NgModule							}	from '@angular/core'

import	{
			HomePageMainMenuEntryModule,
			SettingsMainMenuEntryModule,
			QrCodeMainMenuEntryModule,
			SupportMainMenuEntryModule,
		}											from '@rcc/common'

import	{
			HCPSymptomCheckMetaStoreServiceMainMenuEntryModule,
			UpdatesMainMenuEntryModule,
			BackupMainMenuEntryModule,
			DeleteStorageMainMenuEntryModule
		}											from '@rcc/features'

@NgModule({
	imports : [
		HomePageMainMenuEntryModule
		.addEntry({ position: 1 }),
		
		SupportMainMenuEntryModule
		.addEntry({ position: 2 }),
		
		HCPSymptomCheckMetaStoreServiceMainMenuEntryModule
		.addEntry({ position: 3 }),
		
		QrCodeMainMenuEntryModule
		.addEntry({ position: 5 }),
		
		SettingsMainMenuEntryModule
		.addEntry({ position: 6 }),
		
		UpdatesMainMenuEntryModule
		.addEntry({ position: 7 }),
		
		BackupMainMenuEntryModule
		.addEntry({ position: 8 }),

		DeleteStorageMainMenuEntryModule
		.addEntry({ position: -1 }),
	]
})

export class MainMenuModule {}
