import { NgModule } from '@angular/core'
import { DAILY_NOTES_ID } from '@rcc/core'
import { RccMonitoringSetupModule } from '@rcc/features'

@NgModule({
	imports: [
		RccMonitoringSetupModule.forRoot({
			questionCategories: [
				'symptoms',
				'medication',
				'resources',
				'early_warning_signs',
				'side_effects',
				'withdrawal_symptoms',
				'daily_notes'
			],
			hiddenOnEditCategories: [
				'daily_notes'
			],
			hiddenInCatalogue: [
				DAILY_NOTES_ID
			],
			defaultQuestionsByCategory: new Map(
				[['daily_notes', [DAILY_NOTES_ID]]]
			),
			addTemplateQuestionsTo: 'symptoms',
			/**
			 * The `catalogRestrictionsByCategory` property in the
			 * configuration object is used to specify restrictions
			 * on the catalog items displayed for a particular category.
			 **/
			catalogRestrictionsByCategory: {
				'resources': {
					includeOnly: ['rcc-category-resources']
				},
				'side_effects': {
					includeOnly: ['rcc-category-use']
				},
				'withdrawal_symptoms': {
					includeOnly: ['rcc-category-dess']
				},
			},
		}),
	]
})
export class RccMonitoringSetupHcpModule {}
