import	{	NgModule						}	from '@angular/core'
import	{	RccBaseFeaturesModule			}	from '../hcp/base-features.module'
import	{	NonMedicalFeaturesModule		}	from '../hcp/non-medical-features.module'
import	{	StaticQrCodeModule				}	from '@rcc/features/static-qr-code'


@NgModule({
	imports: [
		RccBaseFeaturesModule,
		NonMedicalFeaturesModule,
		StaticQrCodeModule
	],
})

export class RccFeaturesModule { }
