#!/bin/sh

set -e

if [ -n "$FLAVOR" ]; then
    cp -r /app/"$FLAVOR"/* /usr/share/nginx/html/
else
    echo "Expecting to find the flavor we're supposed to run in the FLAVOR env variable." 1>&2
    exit 1
fi
