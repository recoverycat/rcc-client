import fs 	from 'fs'
import path from 'path'

/**
 * extract_translations: The directory to extract the translations from.
 * Usage in terminal: `node rcc_merge_translations.js csv=translations.csv extract=./lib`
 *
 * output: Is a directory to output the translations to as individual JSON files.
 * Usage in terminal: `node rcc_merge_translations.js extract=./lib output=./mydir`
 *
 * import_translations: Read translation from an external JSON file.
 * Usage in terminal: `node rcc_merge_translations.js import=./translations.json`
 *
 * languages: Sets the languages that will be recognized for this call. Default: `lang=de,en`
 *
 * spread_translations: Takes the current translations and places them into the given directory.
 * Usage in terminal: `node rcc_merge_translations.js import=./translations.json spread=./lib`
 *
 * csv_export: The directory to export current translations as CSV to.
 * Usage in terminal: `node rcc_merge_translations.js extract=./lib csv-export=translations.csv`
 *
 * csv_import: The CSV file to import the current translations from.
 * Usage in terminal: `node rcc_merge_translations.js csv-import=translations.csv`
 */
const extract_translations 	= 	getParam('extract','')
const output				= 	getParam('output', '')
const import_translations	=	getParam('import', '')
const languages 		    = 	getParam('lang', 'de,en', true)
const spread_translations	=	getParam('spread','')
const csv_export			=	getParam('csv-export','')
const csv_import			=	getParam('csv-import','')
const help 					= 	getParam('help', false)

const help_text = `
	Usage: node rcc_merge_translations.js [options]

	Options:
		--help		  	this help text

		extract=		the directory to extract the translations from
				Command: node rcc_merge_translations.js csv=translations.csv extract=./lib
		output=		  	the directory to output the translations to
				Command: node rcc_merge_translations.js extract=./lib output=./mydir
		import=			the JSON file to import the translations from
				Command: node rcc_merge_translations.js import=./translations.json
		lang=			the languages to recognize for this call
		spread=			the directory to spread the translations to
				Command: node rcc_merge_translations.js import=./translations.json spread=./lib
		csv-export=		the directory to export the translations as CSV to
				Command: node rcc_merge_translations.js extract=./lib csv-export=translations.csv
		csv-import=		the CSV file to import the translations from
				Command: node rcc_merge_translations.js csv-import=translations.csv
`
/**
 * Gets the user input for the given parameter.
 *
 * Please check source code to see line for line in-depth explanations of the code.
 *
 * Variables eq and dd refer to two different regular expressions. The former is looking for the '=' symbol, the latter for the '--' symbol.
 *
 * @param {string} param
 * @param {string} defaultValue
 * @param {boolean} array			Whether or not the result is supposed to be an array. (split the result at commata)
 * @returns {string[] | string}
 */
function getParam(param, defaultValue = '', array = false){

	const regex_eq	= 	new RegExp('^'+param+'=')						// Create a regex to match anything that contains an queal sign.

	const match_eq	= 	process.argv.find( x => regex_eq.test(x) )		// Get first process argument that matches the regex.
	const result_eq	= 	match_eq && match_eq.split('=')[1]				// Extract everything before the equal sign.

	const regex_dd	= 	new RegExp('^--'+param+'$')						// Create a regex looking for anything that starts with a double dash.
	const match_dd	= 	process.argv.find( x => regex_dd.test(x) )		// Get first process argument that matches the regex.
	const result_dd	= 	!!match_dd										// Get true or false. The double exclamation mark converts a truthy or falsy value to “true” or “false”.

	const result 	= 	result_eq || result_dd || defaultValue 			// If no eq or dd match is found, the default value is returned

	if(!result) return null 											// If no result is found, return null

	return 	array 														// Whether or not the result is supposed to be an array
			?	result.split(',') 										// If so, split it by commas
			:	result 													// Else if the result is a string, return it

}



/**
 * This function takes a string and returns an array of all the translations i.e.
 * language files found referred to in the string (thought of as content of a file).
 *
 * Please check source code to see line for line in-depth explanations of the code.
 */
function getLanguageFiles(str = ''){									// Get all references to language files from the given string
	const regex = new RegExp('(?<=[\'`"])[^\'`"]*('+languages.join('|')+')\\.json','mgi')
	const matches = str.match(regex) 									// Match all language files ending with .json. Get all matches and save them in an array
	return  matches || [] 												// If no matches are found, return an empty array
}



/**
 * This function looks for the scope of a translation map.
 * It searches for the function call that adds the translation map with a scope to the overall dictionary.
 * Through this mechanism it is prevented that a translation value is used wrongfully in different files because of sharing the same key name.
 *
 * Two match systems are used. The first one is legacy.
 */
function getScope(sourceCode = ''){

	const legacy_match 	= sourceCode.match(/TranslationsModule\w*\.\w*forChild\([\'`"]?([^,\'`"]*)[\'`"]?,[^)]*\)/)
	const recent_match 	= sourceCode.match(/provideTranslationMap\([\'`"]?([^,\'`"]*)[\'`"]?,[^)]*\)/)

	const legacy_scope	= legacy_match && legacy_match[1]
	const recent_scope	= recent_match && recent_match[1]

	return recent_scope || legacy_scope
}

/**
 * Gets the language code from the given path. (That is the name of a language file, without the path and extension.)
 * First it checks if the path ends with .json.
 * It then returns the array and the second element of the array
 * @param {string} filename optionally including path.
 */

 function getLanguageCode(path_to_file){
	const match = path_to_file.match(/([^\/]+)\.json$/)
	return match && match[1]
}

/**
 * This function checks if the given file includes translation data..
 */
function isTranslationFile(path_to_file){

	// Check if the corresponding language is one given in the proceess parameters, if not false;
	if(!languages.includes(getLanguageCode(path_to_file))) return false

	// Try to parse the contents as JSON.
	try{
		const json = require('./'+path_to_file)
		//import json from './'+path_to_file
		return true

	} catch(e) {
		console.log(e)
		return false
	}
}


/**
 * This function retrieves the scope and paths for all translation files referenced in a given file.
 * Breaks if multiple language files for the same language are loaded in the same module.
 * Please check source code to see line for line in-depth explanations of the code.
 *
 * @param 	{string} path_to_file path to module file to be checked
 * @returns	{{scope: string, map: Object}}
 *
 */
function scopeAndPath(path_to_file) {

	if(!path_to_file.match(/\.ts$|.js$/)) return false 					// If the file is not a .ts or .js file, return false


	const contents 			= fs.readFileSync(path_to_file, 'utf-8') 	// Read the file

	const language_files 	= getLanguageFiles(contents) 				// Get all language files from the file

	if(language_files.length === 0) return null 							// If no language files are found, return null

	var map = {}

	language_files.forEach( file => { 									// For each language file found

		const lang 	= getLanguageCode(file) 							// Get the language code from the file

		map[lang]	= path.join(path.dirname(path_to_file), file)		// Add the language code and the path to the map

	})

	const scope	= getScope(contents)									// Get the scope from the file
	console.log(path_to_file, typeof scope, scope)
	if(!scope) return null 												// If no scope is found, return null

	return {scope, map} 												// Return the scope and the map
}


/**
 * This function returns all filename including path in a given directory and all sub directories.
 *
 * Please check source code to see line for line in-depth explanations of the code.
 */
function readRdirSync(base = '.') {
	return	fs.readdirSync(base)										// Synchronously reads the contents of a given directory.
			.map( filename => {
				const path_to_file = path.join(base, filename)
				return 	fs.statSync(path_to_file).isDirectory() 		// Synchronously returns information about the given file path.
						?	readRdirSync(path_to_file)
						:	path_to_file

			})
			.flat()
}

/**
 * This function reads the given directory and returns an array of filenames including path corresponding to modules with translations.
 *
 * Please check source code to see line for line in-depth explanations of the code.
 */
function getModulesWithTranslations(dir) {

	return 	readRdirSync(dir)											// Synchronously reads the contents of a given directory.
			.map(scopeAndPath)											// Map each file to the scopeAndPath function
			.filter( x => !! x) 										// Filter out null values

}

/**
 * Extracts all the translation tables from the given directory and returns a translations JSON object.
 *
 * Please check source code to see line for line in-depth explanations of the code.
 */
function extractTranslationTables(dir){

	var translations = {} 												    // Create an empty object

	getModulesWithTranslations(dir) 									    // Get all modules with translations
	.forEach( x => {													    // For each module
		Object.keys(x.map).forEach( lang => {							    // For each language
			translations[lang]  =   translations[lang] || {}				// If the language is not in the translations object, create it
			const map           =   JSON.parse(fs.readFileSync(x.map[lang], 'utf-8'))
			const globalScope   =   ["null", "undefined"].includes(x.scope)
			const scopedMap     =   globalScope
									? map
									: inflateObject(map, x.scope)
			translations[lang]  = mergeObjects(translations[lang], scopedMap)
		})																// Add the translation table to the translations JSON object
	})

	return translations
}

/**
 * This function spreads the translations in the given src folder.
 *
 * Get all modules with translations for each module.
 * For each language write the translation table to the file.
 */
function spreadTranslations(translations, src){
	getModulesWithTranslations(src)
	.forEach( ({scope, map }) => {

		Object.keys(translations).forEach( lang => {
			fs.writeFileSync(map[lang], JSON.stringify(translations[lang][scope], null, 4), 'utf-8')
		})
	})
}

/**
 * This function aligns the given translations.
 *
 * Add translation key (with value null) to all translations that are lacking that specific key.
 *
 * @param {Object} translations Partial translation map with language prefix, e.g. {'en': ..., 'de': ...}
 */
function alignTranslations(translations){

	const languages				= Object.keys(translations)
	const translation_tables 	= Object.values(translations) 	// strip the languages keys
	const skeleton 				= mergeKeys(translation_tables)	// merge all object properties (see mergeKeys())

	return languages.reduce( (result, lang) => ({ ...result, [lang]: fillKeys(translations[lang], skeleton)}), {} )
}

/**
 * This function merges the keys of the given JSON objects. All values are set to null.
 *
 * @param {Object[]} objects Array of objects to be merged.
 */
function mergeKeys(objects){

	var 	skeleton 	= 	{}
	const	all_keys	=	objects.map( o => typeof o === 'object' ? (o && Object.keys(o)) : [] )
							.flat()
	const	unique_keys	= 	Array.from(new Set(all_keys) )

	if(unique_keys.length === 0) return null

	unique_keys.forEach( key => skeleton[key] = mergeKeys(objects.map( o => o[key] || {}) ) )

	return skeleton
}


/**
 * This function fills the values of the given object into the given skeleton at
 * the appropriate location.
 *
 * Please check source code to see line for line in-depth explanations of the code.
 *
 * @param {Object} obj
 * @param {Object} skeleton
 */
function fillKeys(obj, skeleton){

	obj = obj || {}

	if(!skeleton) return null 										// If no skeleton is given, return null

	const 	keys 	= Object.keys(skeleton) 						// Get the keys of the skeleton
	var 	result 	= {}


	keys.forEach( key => {											// For each key in the skeleton check
		result[key] = 	(typeof skeleton[key] === 'object' && skeleton[key])
						?	fillKeys(obj[key], skeleton[key]) 		// if the key is an object, and the skeleton is not null. Fill the key with the skeleton
						:	obj[key] || null 						// Else, set the key to the value of the object or null
	})

	return result
}





/**
 * TODO: Check for errors
 * This function imports translations from a given path. *
 *
 * 1. Synchronously reads the contents of a given directory and returns an array of all the files and directories within it.
 * 2. If there is only one file, return the translation table
 * 3. If there are more than one file, merge everything into one object and return the result.
 *
 * @param {string} path Path to translation files (e.g. /translations, including en.json, de.json)
 */
function importTranslations(path){
	const files = readRdirSync(path)	//


	// If only one file is present, assumes that the file content looks like
	// {'en':..., 'de': ...} or similar; including the languages.
	// And try to parse it as JSON.
	// TODO: Check if proper translation file.
	if(files.length === 1) return JSON.parse(fs.readFileSync(path, 'utf-8'))

	// If multiple files, treat every file as translation for one language indicated by its filename.
	// Merge JSON content of each file into one result object indexed by the language keys. (e.g. {'en':..., 'de': ...})
	return 	files.reduce( (result, path ) => {
				return {...result, [getLanguageCode(path)] : JSON.parse(fs.readFileSync(path, 'utf-8')) }
			}, {})

}




/*** CSV ****/



/**
 * Recursive function to flatten an object.
 * Returns a value/sub object matching the given path: {a:{b:{c:1}}} 'a.b' -> {c:1}
 *
 * Please check source code to see line for line in-depth explanations of the code.
 */
function deflateObject(obj, path = '') {

	if(path === '') 				return obj 									// If no path is given, return the object
	if(typeof obj !== 'object')	return obj 									// If the object is not an object, return it

	const parts 		= path.split('.') 									// Split the path into parts
	const first_part 	= parts.shift() 									// Get the first part of the path
	const rest			= parts.join('.') 									// Get the rest of the path

	const def_obj 		= obj[first_part] 									// Get the object matching the first part of the path
	const def_path		= rest 											 	// Get the path matching the rest of the path

	return deflateObject( def_obj, def_path) 								// Recursively call the function with the new path

}

/**
 * Recursive function that takes an object of the type NestedRecord<T> and an empty string as a path. For full TypeScript function check utils.ts.
 *
 * Please check source code to see line for line in-depth explanations of the code.
 *
 * obj, 'PROP1.PROP2' -> { PROP1: { PROP2: obj }}
 */
function inflateObject(obj, path) {
	const parts 		= path.split('.')							// Splits the path into parts
	const last_part 	= parts.pop()								// Gets the last part of the path and saves it in the variable last_part
	const rest			= parts.join('.')							// Gets the rest of the path and saves it in the variable rest

	const inflated		= { [last_part]: obj }						// Creates a new object with the last part as the key and the value of the object as the value

	return	rest === ''												// If the rest is empty
			?	inflated 											// Return the inflated object
			:	inflateObject(inflated , rest) 						// Else, recursively call the function with the inflated object and the rest of the path

}

/**
 * Recursive function that akes two objects and merges them into one. For full TypeScript function check utils.ts.
 *
 * Please check source code to see line for line in-depth explanations of the code.
 */
function mergeObjects(obj1, obj2) {

	const keys1  	= obj1 && typeof obj1 === 'object' && Object.keys(obj1)	// Gets the keys of the first object
	const keys2  	= obj2 && typeof obj2 === 'object' && Object.keys(obj2)	// Gets the keys of the second object

	if(!keys1) return obj2													// If the first object is not an object or is empty, return the second object
	if(!keys2) return obj2 													// If the second object is not an object or is empty, return the second object

	const keys 	 	= new Set([...keys1, ...keys2]) 						// Creates a new set with the keys of both objects

	const merged = {}														// Creates a new object

	keys.forEach( key => {													// For each key in the set

		const o1 	= (key in obj1) ? obj1[key] : undefined 				// Gets the value of the key in the first object
		const o2 	= (key in obj2) ? obj2[key] : undefined 				// Gets the value of the key in the second object

		if(o1 === undefined)	return merged[key] = o2 					// If the key is not in the first object, return the value of the key in the second object
		if(o2 === undefined)	return merged[key] = o1 					// If the key is not in the second object, return the value of the key in the first object

		merged[key] = mergeObjects( o1, o2) 								// Else, recursively call the function with the values of the key in the first and second object

	})

	return merged

}


/**
 * Converts nested object keys into path strings in an array: {a:{b:{c:1}, d:2}} -> ['a.b.c', 'a.d'].
 *
 * Please check source code to see line for line in-depth explanations of the code.
 */
function getPathsFromJson(obj){

	if(obj 	=== null)			return []									// If the object is null, return an empty array
	if(typeof obj !==  'object')	return []									// If the object is not an object, return an empty array

	const keys	=	Object.keys(obj)										// Gets the keys of the object
	const paths = 	keys													// Creates a new array with the keys of the object
					.map(													// Maps the keys to strings
						key => 	{ 											// For each key recursively call the function with the value of the key
									const sub_paths = getPathsFromJson( obj[key] )
									const prefix	= key.toUpperCase() 	// Creates a prefix with the key in uppercase

									return 	sub_paths.length !== 0			// If the sub_paths array is not empty
											? 	sub_paths.map( path => `${prefix}.${path}`)
											:	prefix						// Map the sub_paths array to paths with the prefix. Else, return the prefix
								}
					)
					.flat()													// Flatten the array

	return paths

}

const missingValueReplacement = '[MISSING!]'

/**
 * Wraps a string into double quotes and escapes quotes within,
 * preparing it to be used as a value in a CSV line.
 *
 * Please check source code to see line for line in-depth explanations of the code.
 */
function sanitizeStringForCsv(str){
	if(typeof str !== 'string') return `"${missingValueReplacement}"`  		// if not a string, return a placeholder in quotes

	const san_str = str.replace(/"/, '""')							// replace all double quotes with double double quotes

	return `"${san_str}"`

}

/**
 * Function for importing CSV files with previously escaped characters
 * by replacing double double quotes with double quotes globally.
 *
 * Please check source code to see line for line in-depth explanations of the code.
 */
function desanitizeStringForJson(str){
	if(str === missingValueReplacement) return null

	const desan_str = 	str
						.replace(/^"/, '')									// remove first "
						.replace(/"$/, '')									// remove last "
						.replace(/""/g, '"')  								// global replace

	if(str === missingValueReplacement) return null 							// if missing value replacement is left, return null

	return desan_str

}



/**
 * Converts a JSON object into CSV.
 * The first column will include the path (see getPathsFromJson) of a value.
 * First level keys will be used as column headers.
 *
 * ```js
 * 	{a:{b:{c:'ABC'}, x: { y : {z: 'ZZZ'} } }
 *
 * 	// will go to:
 *
 *  //		|	a		|	x
 *  // b.c	|	1		|	[Missing!]
 *  // y.z	|[Missing!]	|	ZZZ
 *
 * ```
 */

/**
 * Function for exporting JSON to CSV.
 *
 * Please check source code to see line for line in-depth explanations of the code.
 */
function json2Csv(obj){																		// function for exporting JSON to CSV
																							// Example string:: en: {EXPORT : { MISSING_APPLICABLE_EXPORTERS : 'There are no applicable exporters.' }}
	const languages 	= 	Object.keys(obj) 												// Retrieve the keys of the JSON Object (i.e. obj) and place into an array -> e.g. ['en', 'de']

	const paths			=	languages 														// Iterate with map through the paths array.
							.map( language => getPathsFromJson(obj[language]) )				// Retrieve each path of each value in the JSON object (obj) through the its key (language)
							.flat()															// Flatten the array of arrays into a single array of paths

	const unique_paths 	= 	Array.from( new Set( paths ) )									// Remove duplicates from the paths array

	const lines			=	unique_paths.map( path => {										// Assign the lines variable ( = lines in the CSV file) Iterate through the unique paths array.
																							// Retrieve for each path the individual values in the values array.
								const values 	=	languages.map( language => {
														const raw_value = deflateObject(obj[language], path)
														const san_value = sanitizeStringForCsv(raw_value)

														return san_value 					// raw_value is a variable where the sub hash is simplified and stored as a single string.
													})										// san_value is a cleaned string with escaped quotes to avoid formatting issues in the CSV file.

								const line		= 	[sanitizeStringForCsv(path), ...values].join(',') 			// Join the path and all the values with a comma

								return line
							})

	const header		=	['', ...languages].map( v => sanitizeStringForCsv(v)) 												// Create the header line. The first column will be empty. The rest will be the languages.

	const csv			=	[																// Join the header with all the gathered lines with a linebreak after each line. -> Saved in a CSV variable
								header,
								...lines
							]
							.join('\n') 													// Join the lines with a linebreak

	return csv


}

	/* CSV to JSON */

/**
 * Function for retrieving values from a single CSV line.
 *
 * Please check source code to see line for line in-depth explanations of the code.
 */
	function csvLine2Json(line, headers) {
		const columns 			= csvStringToArray(line)[0]																	// Split the line into columns
		const path 				= columns.shift() 																			// Retrieve the path from the first column
		const relevantHeaders 	= headers.slice(1) 																			// Retrieve the headers from the second column onwards
		const midResult 		= columns.map( column =>	inflateObject(desanitizeStringForJson(column), path))			// Take the values of a row and use the headers to turn them into a JSON object

		const result 			= {}

		relevantHeaders.forEach((header,index) => {											// Iterate through the relevant headers
			result[header] = midResult[index].value											// Assign the value to the result hash
		})
		return result
	}


// Thanks to Ben Nadel,  https://www.bennadel.com/blog/1504-ask-ben-parsing-csv-strings-with-javascript-exec-regular-expression-command.htm and Jezternz
// https://gist.github.com/Jezternz/c8e9fafc2c114e079829974e3764db75

function csvStringToArray(strData){

	const 	objPattern = new RegExp(/(\,|\r?\n|\r|^)(?:"([^"]*(?:""[^"]*)*)"|([^\,\r\n]*))/gi)
	let 	arrMatches = null, arrData = [[]]

	if (strData.trim() === "") throw "csvStringToArray input must not be empty."
	while (arrMatches = objPattern.exec(strData)){
		if (arrMatches[1].length && arrMatches[1] !== ",")arrData.push([])
		arrData[arrData.length - 1].push(
			arrMatches[2]
			?	arrMatches[2].replace(new RegExp( "\"\"", "g" ), "\"")
			:	arrMatches[3]
		)
	}
	return arrData
}

/**
 * Function for turning a CSV file into a JSON object.
 *
 * Please check source code to see line for line in-depth explanations of the code.
 */
 function csv2Json(csv){

	const lines 			=	csv.split("\n").filter(x => x.trim() !== "")			// Split the CSV into lines
	const headers 			=	csvStringToArray(lines.shift())[0]					// Retrieve the first line (header) and split it into columns
	const partial_objects  	= 	lines.map( line => csvLine2Json(line, headers))		// Iterate through the lines and retrieve the values for each line.

	let result 				= 	{}													// Create an empty object to store the result

	partial_objects.forEach( obj => result = mergeObjects(result, obj))				// Iterate through the partial objects and merge them into the result object
	return result
}


/**
 * Function for importing a CSV file.
 * UTF-8 (Unicode Transformation Format)is an encoding system for Unicode.
 * It can translate any Unicode character to a matching unique binary string.
 * It can also translate the binary string back to a Unicode character.
 */
	function importCsv(filename) {
		const csv = fs.readFileSync(filename, 'utf8')
		return csv2Json(csv)

	}

/* END CSV to JSON */


var translations = {} 															// Create an empty object to store the translations


/* If conditions */

/**
 * If the import_translations variable is set to true, import the translations from the CSV file.
 *
 * Please check source code to see line for line in-depth explanations of the code.
 */
if(import_translations){
	process.stdout.write('\n')																// Print a linebreak to the console
	process.stdout.write('Importing from '+import_translations+' ... ') 					// Print the filename to the console
	translations = importTranslations(import_translations) 									// Import the translations from the CSV file
	process.stdout.write(Object.keys(importTranslations(import_translations)).join(', ')) 	// Print the languages to the console
	process.stdout.write(' [ok]\n') 														// Print [ok] to the console
}

/**
 * If the extract_translations variable is set to true,
 * extract all the translation tables from the given directory and returns a translations JSON object
 *
 * Please check source code to see line for line in-depth explanations of the code.
 */
if(extract_translations){ //
	process.stdout.write('\n')

	process.stdout.write('Extracting from '+extract_translations+' ... ') 					// Print the filename to the console
	translations = (alignTranslations(extractTranslationTables(extract_translations))) 		// Extract the translations from the HTML file
	process.stdout.write(Object.keys(translations).join(', ')) 								// Print the languages to the console

	process.stdout.write(' [ok]\n')
}

/**
 * If the csv_export variable is set to true, export the translations from a JSON file to a CSV file.
 *
 * Please check source code to see line for line in-depth explanations of the code.
 */
if(csv_export){
	process.stdout.write('\n')

	if (Object.keys(translations).length === 0) { 											// If the translations object is empty, print an error message
		process.stdout.write('CSV: Don\'t know which translations to use, maybe add parameter "extract=./something"\n')
		process.exit(1) 																	// Exit the script with an error message
	}

	const content 			= json2Csv(translations) 										// Create a CSV file from the translations object
	const matches 			= content.match(/\n/g) 											// Count the number of linebreaks in the CSV file
	const number_of_lines	= matches && matches.length || 0 								// Count the number of lines in the CSV file

	process.stdout.write(`CSV: got ${number_of_lines} lines.\n`) 							// Print the number of lines to the console

	fs.mkdirSync('./translations/', { recursive: true }) 									// Create the translations folder if it doesn't exist
	fs.writeFileSync('./translations/'+csv_export, content) 								// Write the CSV file to the translations folder

	process.stdout.write('\nCSV: result written to '+'./translations/'+csv_export) 			// Print the filename to the console

	process.stdout.write(' [ok]\n')
}

/**
 * If the csv_import variable is set to true, import the translations from a CSV file.
 *
 * Please check source code to see line for line in-depth explanations of the code.
 */
if(csv_import){
	process.stdout.write('\n')

	process.stdout.write(`CSV: Reading from ${csv_import} ... `) 						// Print the filename to the console
	translations = importCsv(csv_import) 												// Import the CSV file to the translations object

	process.stdout.write(' [ok]\n')

}

/**
 * If the spread_translations variable is set to true, spread the translation JSON files in the given src folder.
 *
 * Please check source code to see line for line in-depth explanations of the code.
 */
if(spread_translations){
	process.stdout.write('\n') 																// Print a linebreak to the console

	process.stdout.write('Spreading to '+spread_translations+' ... ') 						// Print the filename to the console
	process.stdout.write(Object.keys(translations).join(', ')) 								// Print the languages to the console
	spreadTranslations(translations, spread_translations)									// Spread the translations to the HTML file

	process.stdout.write(' [ok]\n')
}

/**
 * If the output variable is set to true, set a directory to output the translations to as individual JSON files.
 *
 * Please check source code to see line for line in-depth explanations of the code.
 */
if(output){ 																				// If the output variable is set to true, export the translations to the HTML file
	const dir = output 													// Set output directory
	process.stdout.write('\n') 																// Print a linebreak to the console

	process.stdout.write('Exporting to '+dir+' ... ') 										// Print the filename to the console
	process.stdout.write(Object.keys(translations).join(', ')) 								// Print the languages to the console
	process.stdout.write(' [ok]\n\n')
	fs.mkdirSync(dir, { recursive: true }) 													// Create the translations folder if it doesn't exist

	Object.keys(translations).forEach( lang => { 											// Loop through the languages
		fs.writeFileSync(dir+'/'+lang+'.json', JSON.stringify(translations[lang], null, 4)) // Write the translations to the HTML file
	})

}

/**
 * If the help variable is set to true, show the help message on the console.
 * */
if(help){
	console.log("##############################################################################################")
	console.log('\x1b[33m'+help_text)
	console.log('\x1b[37m'+"##############################################################################################")

}

/**
 * Print the translations object to the console (for debugging) with a maximum depth of 20 nestings.
 */
console.dir( translations, {depth:20})
